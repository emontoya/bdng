<%@page import="org.bdng.harvmgr.dlConfig"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"  %>
    <%
    
    //verificamos session como "admin" sino, lo redirijimos al index.jsp
	HttpSession sesion = request.getSession();
    String user ="";
    String type = "";
    if(sesion.getAttribute("username")== null && sesion.getAttribute("type")== null) {
    	response.sendRedirect("index.jsp");
    	
	}else{
		user = (String)sesion.getAttribute("username");
    	type = (String)sesion.getAttribute("type");
    		if(!user.equals("admin") || !type.equals("admin"))
    			response.sendRedirect("index.jsp");
	}
    
    dlConfig doa = new dlConfig(this.getServletContext().getRealPath("/"));
     
    String sysName = doa.getString("sysName");
    String sysNameFull = doa.getString("sysNameFull");
    String dl_home = doa.getString("dl_home");
    String dl_host = doa.getString("dl_host");
    String dl_port = doa.getString("dl_port");
    String dl_metadata = doa.getString("dl_metadata");
    String dl_data = doa.getString("dl_data");
    String dl_name = doa.getString("dl_name");
    String dl_col = doa.getString("dl_col");
    String admin_email = doa.getString("admin_email");
    String exist_user = doa.getString("exist_user");
    String exist_pass = doa.getString("exist_pass");
    String record_xml = doa.getString("record_xml");
    String proxy = doa.getString("proxy");
    String proxy_host = doa.getString("proxy_host");
    String proxy_port = doa.getString("proxy_port");
    String smtp_host = doa.getString("smtp_host");
    
    
    String harvest_count_incremental = doa.getString("harvest_count_incremental");
    String harvest_frecuency = doa.getString("harvest_frecuency");
    String number_threads = doa.getString("number_threads");
    
	    if(proxy.equalsIgnoreCase("true")){
	    	proxy = "checked";	
	 	    }
	    else{
	    	proxy = "";
	    }
    


%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Configuración del ServerConf.xml</title>

<link type="text/css" href="css/styles.css" rel="stylesheet"/>
<script language="JavaScript" src="gen_validatorv4.js"
    type="text/javascript" xml:space="preserve"></script>

</head>
<body class="container-windows">

<div id=title_section>Configuracion del Sistema</div> 
</br>
<form name="form_ServerConf" method="post" action="servlet/ServerConf">
  <table width="503" border="0" align="center">
    
     <tr>
      <td width="255"><em>Nombre Corto del Sistema:</em></td>
      <td width="144"><label>
        <input type="text" name="sysName" id="sysName" value="<%=sysName%>" >
      </label></td>
      <td width="90">&nbsp;</td>
    </tr>
    <tr>
      <td width="255"><em>Nombre Largo del Sistema:</em></td>
      <td width="144"><label>
        <input type="text" name="sysNameFull" id="sysNameFull" value="<%=sysNameFull%>" >
      </label></td>
      <td width="90">&nbsp;</td>
    </tr>
    <tr>
      <td width="255"><em>Directorio de Instalacion:</em></td>
      <td width="144"><label>
        <input type="text" name="dl_home" id="dl_home" value="<%=dl_home%>" >
      </label></td>
      <td width="90">&nbsp;</td>
    </tr>
        <tr>
      <td width="255"><em>Direccion Host Servidor:</em></td>
      <td width="144"><label>
        <input type="text" name="dl_host" id="dl_host" value="<%=dl_host%>" >
      </label></td>
      <td width="90">&nbsp;</td>
    </tr>
     <tr>
      <td width="255"><em>Puerto:</em></td>
      <td width="144"><label>
        <input type="text" name="dl_port" id="dl_port" value="<%=dl_port%>" >
      </label></td>
      <td width="90">&nbsp;</td>
    </tr>
     <tr>
      <td width="255"><em>Direccion Host SMTP:</em></td>
      <td width="144"><label>
        <input type="text" name="smtp_host" id="smtp_host" value="<%=smtp_host%>" >
      </label></td>
      <td width="90">&nbsp;</td>
    </tr>
    <tr> 
      <td><em>Directorio de Metadatos:</em></td>
      <td><input type="text" name="dl_metadata" id="dl_metadata" value="<%=dl_metadata%>"></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><em>Exist user:</em></td>
      <td><input type="text" name="exist_user" id="exist_user" readonly="readonly" value="<%=exist_user%>"></td>
      <td>&nbsp;</td>
    </tr>
     <tr>
      <td><em>Exist password:</em></td>
      <td><input type="password" name="exist_pass" id="exist_pass"  value="<%=exist_pass%>"></td>
      <td>&nbsp;</td>
    </tr>
        </tr>
     <tr>
      <td><em>Admin Email:</em></td>
      <td><input type="text" name=admin_email id="admin_email"  value="<%=admin_email%>"></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><em>Directorio de Datos:</em></td>
      <td><input type="text" name="dl_data" id="dl_data" value="<%=dl_data%>"></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><em>Nombre:</em></td>
      <td><input type="text" name="dl_name" id="dl_name" value="<%=dl_name%>"></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><em>Colecci&oacute;n :</em></td>
      <td><input type="text" name="dl_col" id="dl_col" value="<%=dl_col%>"></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><em>Formato XML:</em></td>
      <td><input type="text" name="record_xml" id="record_xml" value="<%=record_xml%>"></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><em>Direccion Proxy:</em></td>
      <td><input type="text" name="proxy_host" id="proxy_host" value="<%=proxy_host%>"></td>
      <td><em>
        <input name="proxy" type="checkbox" id="proxy" value="true" <%=proxy%>>
Proxy?</em></td>
    </tr>
    <tr>
      <td><em>Puerto Proxy:</em></td>
      <td><input type="text" name="proxy_port" id="proxy_port" value="<%=proxy_port%>"></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><em>Numero de Recolecciones Incrementales:</em></td>
      <td><input type="text" name="harvest_count_incremental" id="harvest_count_incremental" value="<%=harvest_count_incremental%>"></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><em>Frecuencia de Recoleccion:</em></td>
      <td><input type="text" name="harvest_frecuency" id="harvest_frecuency" value="<%=harvest_frecuency%>"></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
    <tr>
      <td><em>Numero de Hilos:</em></td>
      <td><input type="text" name="number_threads" id="number_threads" value="<%=number_threads%>"></td>
      <td>&nbsp;</td>
    </tr>
      <td>
      <p>
        <label>
          <input type="submit" name="send" id="send" value="Guardar">
        </label>
      </p></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
  </table>
</form>
<script  type="text/javascript">
//validaciones req = requerido, numeric = solo numeros


 var frmvalidator = new Validator("form_ServerConf");

 frmvalidator.addValidation("dl_home","req","Por favor ingrese el directorio de instalacion");
 frmvalidator.addValidation("dl_host","req","Por favor ingrese la direccion del host");
 frmvalidator.addValidation("dl_port","req","Por favor ingrese el puerto del host");
 frmvalidator.addValidation("dl_metadata","req","Por favor ingrese el directorio de instalacion");
 frmvalidator.addValidation("dl_name","req","Por favor ingrese un Nombre");
 frmvalidator.addValidation("dl_col","req","Por favor ingrese una Coleccion");
 frmvalidator.addValidation("admin_email","req","Por favor ingrese el email del administrador");
 frmvalidator.addValidation("exist_user","req","Por favor ingrese el usuario administrador");
 frmvalidator.addValidation("exist_pass","req","Por favor ingrese la clave del administrador");
 frmvalidator.addValidation("record_xml","req","Por favor ingrese el formato XML");

 
//	 frmvalidator.addValidation("proxy_port","req","Por favor ingrese el puerto para el proxy");		 
//	 frmvalidator.addValidation("proxy_host","req","Por favor ingrese la direccion host para el proxy");
	 frmvalidator.addValidation("proxy_port","numeric","Por favor ingrese un puerto valido para el proxy");
	 
 
 frmvalidator.addValidation("harvest_count_incremental","numeric","Por favor ingrese un numero valido para el campo 'Cuenta Incremental de Recolector'");
 frmvalidator.addValidation("harvest_frecuency","numeric","Por favor ingrese un numero valido para el campo 'Frecuencia de Recolector'");
 frmvalidator.addValidation("number_threads","numeric","Por favor ingrese un numero valido para el campo 'Numero de Hilos'");
 
 

 
 </script>


<h1></h1>


</body>
</html>