$(document).ready(function(){	
	

	
	$("#loginForm").hide();
	
	$("#loginForm").slideDown('slow');
	
	$("#loginForm").bind("keypress", function(e) {
		 
		   if (e.keyCode == 13) {
			   login_admin();
		      return false; // ignore default event
		   }
		});
	

		$('a[rel*=facebox]').facebox() ;
	 
});


function Clear() {
	
	$('#wrongLogin').hide();
	$('#inactive').hide();
	
}


function recuperar(){
	
		var mail = $("#facebox #txtMail").val();
	
	
		$.ajax({
			type: "POST",
	        data:  { txtMail: mail, fromJsp : "si"  },
	        url: "/bdcol/search/user/retrievePass.xq",
	        timeout: 25000,
	        dataType: "html",
	        beforeSend: function(){        	
	        },
	        complete: function(){
	        },
	        success: function( data ) {
	        	
	        	//console.log("data "+data);
	        	
	        	var resp  = $(data).find("#resp").html();
	        	//console.log("resp "+resp);
	    
	        	if(resp)
					humane.error("El email ingresado no se encuentra registrado en el sistema");
				else
					humane.success("La clave ha sido enviada al correo: "+mail);
	        },
	        error: function(result){
	        	if(result.statusText != "abort"){
	        		humane.error("Problemas en el servidor, Intentelo otra vez en unos minutos.");
	        	}
	        }
	    });
		
		
}




var loggeando = null;
function login_admin(){
	if(loggeando){
		loggeando.abort();
	}
	var username = $("#username").val();
	var password = $("#password").val();
	var url = "/admin/servlet/LoginUser";
	if(username!="admin")
		url = "/bdcol/search/user/controllerLogin.xq";
	

	
	loggeando  = $.ajax({
		type: "POST",
        data: {pass: password, user : username},
        url: url,
        timeout: 25000,
        dataType: "html",
        beforeSend: function(){        	
        },
        complete: function(){
        },
        success: function( data ) {
        	//alert("data "+data)
        	
        	var loginsuccess  = $(data).find("#loginsuccess").html();
        	var type  = $(data).find("#type").html();
        	var username  = $(data).find("#username").html();
        	
        	//alert("loginsuccess "+loginsuccess);
           	if(loginsuccess=="Inactivo"){
        		
        		$('#inactive').show();
        	}else
        	if(loginsuccess=="no"){
        		
        		$('#inactive').hide();
        		$('#wrongLogin').show();
        	}else if(loginsuccess=="yes"){
        		
        		
        		$('#inactive').hide();
        		$('#loginForm').slideUp('slow');
        		
        		//$("#redirect").trigger("click");
        		//window.location = "main.jsp";
        		//aki
        		
        		//console.log("type"+type);
        		if(type=="adminRepo"){
        			
        			$.ajax({
        				type: "POST",
        		        data: {name: name, type: type, username: username },
        		        url: "../../../../admin/Session.jsp",
        		        timeout: 25000,
        		        dataType: "html",
        		        beforeSend: function(){        	
        		        },
        		        complete: function(){
        		        },
        		        success: function( data ) {
        		        //	console.log("submit");
        		        	$('#loginForm').slideUp('slow');
        		        	$('#loginForm').submit();
        		        
        		        },
        		        error: function(result){
        		        	if(result.statusText != "abort"){
        		        		humane.error("Problemas en el servidor, Intentelo otra vez en unos minutos.");
        		        	}
        		        }
        		    });
        			
        			
        		}else
        		$('#loginForm').submit();
        		//aki deberia de redireccionar hacia index.xq

        		
        	}

        	
        	
        	
        	
        },
        error: function(result){
        	if(result.statusText != "abort"){
        		humane.error("Problemas en el servidor, Intentelo otra vez en unos minutos.");
        	}
        }
    });
	return false;
}  