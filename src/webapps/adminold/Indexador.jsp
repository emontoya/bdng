
<%@page import="java.util.Iterator"%>
<%@page import="org.bdng.harvmgr.dlConfig"%>
<%@page import="org.jdom2.Document"%>
<%@page import="java.util.List" %>
<%@page import="org.jdom2.Element"%>
<%@page import="org.jdom2.input.SAXBuilder" %>
<%@page import="org.bdng.harvmgr.harvester.DataProvider"%>
<%@page import="org.bdng.webold.ProviderWeb"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
            
    <%    //verificamos session como "admin" sino, lo redirijimos al index.jsp
	HttpSession sesion = request.getSession();
    String user ="";
    String type = "";
    if(sesion.getAttribute("username")== null && sesion.getAttribute("type")== null) {
    	response.sendRedirect("index.jsp");
    	
	}else{
		user = (String)sesion.getAttribute("username");
    	type = (String)sesion.getAttribute("type");
    		if(!user.equals("admin") || !type.equals("admin"))
    			response.sendRedirect("index.jsp");
	} %>
    
    <%
    
    	
    	dlConfig doa = new dlConfig(this.getServletContext().getRealPath("/"));
    	List l = doa.GetVisualProviders();
    	//out.print("registros "+l.size());
    	
		
		
		
	Element oai_provider = null;
		
	Element shortName = null;
	Element repositoryName = null;
	Element owner = null;
	 
  	
  	
  	
  	    %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Indexador</title>

		<title>DataTables example</title>
		<style type="text/css" title="currentStyle">
			
			@import "DataTables/media/css/demo_table.css";
		</style>
		<link type="text/css" href="css/styles.css" rel="stylesheet"/>
		<script type="text/javascript">
		
		function Validate(form){ 
			
			if(form.listProviders.checked){//este if es para cuando el usuario hace un filtrado de tal forma que solo queda un elemento  y no podemos tratarlo con un array
				Index(form);
				return true;
			}
			
			for(var i = 0; i < form.listProviders.length; i++){ 
				if(form.listProviders[i].checked){
					Index(form);
				return true;
				}
			}
			alert('Debes seleccionar al menos un Repositorio'); 
			return false; 
			}
		
		function Index(form){
				
				form.action = 'servlet/IndexarWeb?index_opcion=indexar_seleccionados';
				form.submit();				
		}
		function checkAll(){
			
			$(".checkbox").attr('checked', !$(".checkbox").attr('checked'));
			
		}
		function callAll (){
		
			checkAll();
		
		}
		
		</script>
		<script type="text/javascript" language="javascript" src="DataTables/media/js/jquery.js"></script>
		<script type="text/javascript" language="javascript" src="DataTables/media/js/jquery.dataTables.js"></script>
		<script type="text/javascript" charset="utf-8">
			$(document).ready(function() {
				$('#example').dataTable();
			} );
		</script>

</head>
	<body  id="dt_exampleX">
		<div  id="container">
			<div class="full_width big"></div>
			<div id=title_section>Indexacion de metadatos recolectados</div>
			</br>
				<div  id="demo">
<form name="control" method="post" action="">
<div align="right" > <input type="checkbox" id="checkAll" onchange="callAll()" /> Seleccionar todos</div>
  <table  cellpadding="0" cellspacing="0" border="0" class="display" id="example">
    <thead>
    <tr>
    
    		<th>Repositorio</th>
			<th>Nombre Corto</th>
			<th>Acci�n</th>
			
			

    </tr>
    </thead>
    	<tbody>
    <%
    
for(int j = 0; j<l.size();j++){
		  		
		
		oai_provider=(Element)l.get(j);
	
		owner=oai_provider.getChild("owner");
		if(	!owner.getText().equals("admin")) continue;

  			shortName=oai_provider.getChild("shortName");
  			repositoryName=oai_provider.getChild("repositoryName");
			
  				    			
          out.print("<tr class=\"gradeU\">");
          out.print("<td align=\"center\">"+repositoryName.getText()+"</td>");
          out.print("<td align=\"center\">"+shortName.getText()+"</td>");
          out.print("<td align=\"center\">"+"<input class=\"checkbox\" type=\"checkbox\"name=\"listProviders\" value=\""+shortName.getText()+"\">"+"</td>");
          out.print("</tr>");
		  
          
      }    
    
    %>
    </tbody>
	<tfoot>
		<tr>
      		
      		<th>Repositorio</th>
			<th>Nombre Corto</th>
			<th>Acci�n</th>
					
		</tr>
	</tfoot>
</table>
  
    <p>&nbsp;</p>
    <table align="center" width="300" border="0">
      <tr>
        <td><input type="button" name="Actualizar" id="Actualizar" value="Actualizar" onClick="window.location.reload(true);"></td>
        <td><input type="button" name="Indexar" id="Indexar" value="Indexar Todos" onClick="window.location='servlet/IndexarWeb?index_opcion=indexar_todos'"></td>
        <td><input type="button" name="Indexar_sel" id="Indexar_sel" value="Indexar Seleccionados" onClick="Validate(this.form)"></td>
        
        
      </tr>
    </table>
   
  </form>
</body>
</html>