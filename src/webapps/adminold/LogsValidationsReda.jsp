<%@page import="org.bdng.harvmgr.validator.ValidadorLog"%>
<%@page import="java.io.File"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.bdng.harvmgr.dlConfig"%>
<%@page import="org.jdom2.Document"%>
<%@page import="java.util.List"%>
<%@page import="org.jdom2.Element"%>
<%@page import="org.jdom2.input.SAXBuilder"%>
<%@page import="org.bdng.harvmgr.harvester.DataProvider"%>
<%@page import="org.bdng.webold.ProviderWeb"%>
<%@ page language="java" contentType="text/html; charset=US-ASCII"
    pageEncoding="US-ASCII"%>
    
    <%    //verificamos session como "admin" o como "adminRepo" sino, lo redirijimos al index.jsp
	HttpSession sesion = request.getSession();
    String user ="";
    String type = "";
    if(sesion.getAttribute("username")== null && sesion.getAttribute("type")== null) {
    	response.sendRedirect("index.jsp");
    	
	}else{
		user = (String)sesion.getAttribute("username");
    	type = (String)sesion.getAttribute("type");
    		if(!type.equals("adminRepo") && !type.equals("admin"))
    		
    			response.sendRedirect("index.jsp");
	} %>
    
<%

    if(sesion.getAttribute("username")!= null)
    	user = (String)sesion.getAttribute("username");
	
    dlConfig doa = new dlConfig(this.getServletContext().getRealPath("/"));
   	List l = doa.GetVisualProviders();
	//	out.print("registros "+l.size());	
	
	ValidadorLog validadorLog = new ValidadorLog();
	List<File> listaLogs = validadorLog.retornarLogs();
    
	Element oai_provider = null;
	
	Element shortName = null;
	Element repositoryName = null;
	Element baseURL = null;
	Element collection = null;
	Element metadataPrefix = null;
	Element protocol = null;
	Element owner = null;
  	
  	
  	
  	    %>
    
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
<title>Listar Logs</title>

<title>DataTables example</title>
<style type="text/css" title="currentStyle">
@import "DataTables/media/css/demo_table.css";
</style>
<link type="text/css" href="css/styles.css" rel="stylesheet"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript" src="js/app/appJSP.js"></script>
<script type="text/javascript" src="js/spin.min.js"></script>
<script type="text/javascript" language="javascript"
	src="DataTables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="js/jquery.tipTip.js"></script>
<link type="text/css" href="css/tipTip.css" rel="stylesheet"/>

<script type="text/javascript" charset="utf-8">

//Opciones del spinner (loading)
var opts = {
		lines: 16, // The number of lines to draw
		length: 1, // The length of each line
		width: 5, // The line thickness
		radius: 16, // The radius of the inner circle
		color: '#fff', // #rgb or #rrggbb
		speed: 1, // Rounds per second
		trail: 73, // Afterglow percentage
		shadow: false // Whether to render a shadow
};

	$(document).ready(function() {
		
		$(".toltip").tipTip();
		$(document).on("click",".elements img",function(e){
			//console.log("oeee");
			$("#cargando").show();
		});
		
		var target = document.getElementById('cargando');
		var spinner = new Spinner(opts).spin(target);
		$("#cargando").hide();
		$('#example').dataTable();
		
	});
</script>

</head>
<body>

	<div id="cargando" class="loadingDiv"></div>
	<div id="container">
		<div class="full_width big"></div>
		<div id="demo">
			<div id=title_section>Logs de Validaci&oacute;n - REDA</div>
			<table cellpadding="0" cellspacing="0" border="0" class="display"
				id="example">
				<thead>
					<tr>
<!-- 						<th>Repositorio</th> -->
						<th>Log</th>
					</tr>
				</thead>
				<tbody>
					<%
						for(int i = 0; i<listaLogs.size();i++){
							out.print("<tr class=\"gradeU\">");
							out.print("<td><a href=\"/logs/"+listaLogs.get(i).getName()+"\" target=\"_blank\">" + listaLogs.get(i).getName() + "</a></td>");
							out.print("</tr>");
						}
					%>
				</tbody>
			</table>
		</div>
	</div>
</body>
</html>