<html> 
<head>
<title>BDNG OAI</title>
   <META HTTP-EQUIV="Content-Type" content="text/html; charset=iso-8859-1">
  <META HTTP-EQUIV="Cache-Control"  CONTENT="no-cache">
  <META HTTP-EQUIV="Cache-Control"  CONTENT="no-store">
  <META HTTP-EQUIV="Pragma"  CONTENT="no-cache">
  <META HTTP-EQUIV="Expires" CONTENT="0">  
  <META HTTP-EQUIV="Content-Encoding" CONTENT="gzip"> 
<style type="text/css"> 
<!-- 
body  {
	font: 100% Verdana, Arial, Helvetica, sans-serif;
	background: #666666;
	margin: 0; /* it's good practice to zero the margin and padding of the body element to account for differing browser defaults */
	padding: 0;
	text-align: center; /* this centers the container in IE 5* browsers. The text is then set to the left aligned default in the #container selector */
	color: #000000;
}
.twoColElsLtHdr #container { 
	width: 56em;  /* this width will create a container that will fit in an 800px browser window if text is left at browser default font sizes */
	background: #FFFFFF;
	margin: 0 auto; /* the auto margins (in conjunction with a width) center the page */
	border: 1px solid #000000;
	text-align: left; /* this overrides the text-align: center on the body element. */
} 
.twoColElsLtHdr #header { 
	background: ##FFFFFF; 
	padding: 0 0px;  /* this padding matches the left alignment of the elements in the divs that appear beneath it. If an image is used in the #header instead of text, you may want to remove the padding. */
} 
.twoColElsLtHdr #header h1 {
	margin: 0; /* zeroing the margin of the last element in the #header div will avoid margin collapse - an unexplainable space between divs. If the div has a border around it, this is not necessary as that also avoids the margin collapse */
	padding: 10px 0; /* using padding instead of margin will allow you to keep the element away from the edges of the div */
}

.twoColElsLtHdr #sidebar1 {
	float: left; 
	width: 12em; /* since this element is floated, a width must be given */
	background: #EBEBEB; /* the background color will be displayed for the length of the content in the column, but no further */
	padding: 15px 0; /* top and bottom padding create visual space within this div */
}
.twoColElsLtHdr #sidebar1 h3, .twoColElsLtHdr #sidebar1 p {
	margin-left: 10px; /* the left and right margin should be given to every element that will be placed in the side columns */
	margin-right: 10px;
}
.twoColElsLtHdr #mainContent {
	margin: 0 1.5em 0 13em; /* the right margin can be given in ems or pixels. It creates the space down the right side of the page. */
} 
.twoColElsLtHdr #footer { 
	padding: 0 10px; /* this padding matches the left alignment of the elements in the divs that appear above it. */
	background:#DDDDDD;
} 
.twoColElsLtHdr #footer p {
	margin: 0; /* zeroing the margins of the first element in the footer will avoid the possibility of margin collapse - a space between divs */
	padding: 10px 0; /* padding on this element will create space, just as the the margin would have, without the margin collapse issue */
}

/* Miscellaneous classes for reuse */
.fltrt { /* this class can be used to float an element right in your page. The floated element must precede the element it should be next to on the page. */
	float: right;
	margin-left: 8px;
}
.fltlft { /* this class can be used to float an element left in your page */
	float: left;
	margin-right: 8px;
}
.clearfloat { /* this class should be placed on a div or break element and should be the final element before the close of a container that should fully contain a float */
	clear:both;
    height:0;
    font-size: 1px;
    line-height: 0px;
}
--> 
</style>
</head>

<body class="twoColElsLtHdr">

<div id="container">
  <div id="header">
    <h1><tbody><tr>
					<td>
						<table border="0" cellpadding="0" cellspacing="0" width="100%">  <tbody><tr>    <td bgcolor="#b6c589" width="100%"><table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">      <tbody><tr>        <td><a href="http://www.eafit.edu.co/biblioteca.shtm" target="_blank"><img src="img/eafit/titulo.gif" alt="titulo" border="0" height="15" width="505"></a></td>      </tr>    </tbody></table></td>  </tr></tbody></table><table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">  
						  <tbody><tr>        <td width="795"><table border="0" cellpadding="0" cellspacing="0" width="100%">      <tbody><tr>        <td align="left"><img src="img/eafit/libro.jpg" height="90" width="55"></td>        <td align="center"><img src="img/eafit/titleDigital.gif" height="39" width="274"></td>        
						<td align="center"><img src="img/eafit/img.jpg" height="90" width="348"></td>        <td align="right"><a href="http://www.eafit.edu.co/" target="_blank"><img src="img/eafit/logoVerde.gif" alt="Regreso a página principal" border="0" height="89" width="118"></a></td>      </tr>    </tbody></table></td>      
						  </tr></tbody></table><a name="subir"></a></td>
				</tr>
				<tr>
					<td> <table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">
  </table>
<map name="Map" id="Map"><area shape="rect" coords="740,2,762,19" href="http://www.eafit.edu.co/biblioteca.shtm" alt="Volver al Home"><area shape="rect" coords="6,3,87,19" href="http://www.eafit.edu.co/EafitCn/Biblioteca/servicios/"><area shape="rect" coords="105,2,241,19" href="http://www.eafit.edu.co/EafitCn/Biblioteca/digital/"><area shape="rect" coords="264,3,379,19" href="http://www.eafit.edu.co/EafitCn/Biblioteca/recomendados/"><area shape="rect" coords="402,2,468,20" href="http://www.eafit.edu.co/EafitCn/Biblioteca/enlaces/"><area shape="rect" coords="486,3,602,18" href="http://www.eafit.edu.co/EafitCn/Biblioteca/generalidades/"><area shape="rect" coords="622,2,727,18" href="http://www.eafit.edu.co/Eafit/Includes/biblioteca/contactenos/contactenos.shtm">
  






</map>

<map name="Map2" id="Map2"><area shape="rect" coords="371,1,455,22" href="http://www.eafit.edu.co/buscador/"><area shape="rect" coords="196,1,340,24" href="http://www.eafit.edu.co/biblioteca/metabiblioteca/index.htm" target="_blank"><area shape="rect" coords="-1,1,157,22" href="http://zeta.eafit.edu.co:8080/sinbad/">


</map>
<map name="Map3" id="Map3"><area shape="rect" coords="2,1,96,23" href="http://www.eafit.edu.co/EafitCn/Biblioteca/english/services">
</map></td>
				</tr>
	</tbody></h1>
  <!-- end #header --></div>
  <div id="sidebar1">
    <h3>OAI-PMH </h3>
    <p>The OAI Protocol for Metadata Harvesting has
become popular as a mechanism for publicizing and sharing
digital collections. In this approach, servers that implement OAI-PMH
are built for each collection and added to a central registry
as data providers. Applications and value-added service providers
thus can query OAI data providers in a uniform fashion regardless
of the varying internal structure of the underlying digital
collections. </p>
  <!-- end #sidebar1 --></div>
  <div id="mainContent">
    <h2>What is BDNG OAI ?</h2>   
    <p>BDNG OAI is a project that  allows the generation and harvesting of metadata servers under the OAI protocol  for collections stored in XML based databases or XMLs files.</p>
    <p>Given the variety and lack  of standards of the collections for which OAI servers are generated, we decided  to work on the servers' generation for documents collections stored in XML  based databases.</p>
    <p><br>
      The architecture of each  OAI-XML server is defined by several components, where the communication takes  place through interfaces, in such a way that a component can be substituted by  another, as long as the substitute implements the corresponding interface.</p>
    <p>&nbsp;</p>
      <tr bgcolor="#eaeac8">
        <td align="left"><a href="harvester.jsp">1. Harvesting BDNG Exist</a></td>
        <br/>
        <td align="left"><a href="harvesterAll.jsp">2. Harvesting URLs</a></td>        
      </tr>
  <!-- end #mainContent --></div>
 <!-- This clearing element should immediately follow the #mainContent div in order to force the #container div to contain all child floats --><br class="clearfloat" />
   <div id="footer">
    <p align="center">� Biblioteca Luis Echavarr�a Villegas - Universidad EAFIT - Medell�n Colombia �
Carrera 49 No. 7 Sur-50 - Avenida Las Vegas - Tel�fono (57) (4) 261 95 00 - Ext. 261 AA 3300 - Fax (57) (4) 266 42 84</p>
  <!-- end #footer --></div>
<!-- end #container --></div>
</body>
</html>
