<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
    <%
    HttpSession sesion = request.getSession();
    boolean login = false;
    
    String type = "";
    String name = "";
    

    type = (String)sesion.getAttribute("type");
    name = (String)sesion.getAttribute("name");
	//out.print("type"+type);
    //ok = (String)sesion.getAttribute("login");
	if (sesion.getAttribute("type") != null){
   		login = true;
   		
   	}
        
    %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Bdcol Admin Menu</title>
<style type="text/css">
<!--
#login {
	position:absolute;
	left:7px;
	top:80px;
	width:148px;
	height:190px;
	z-index:1;
}
-->
</style>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js">a</script>
<script type="text/javascript" src="js/jquery.treeview.js"></script>

<link rel="stylesheet" href="css/jquery.treeview.css" />


<script type="text/javascript" >
$(document).ready(function(){		
	
	$("#main").hide();
	$("#main").slideDown('slow');
	$("#browser").treeview();
	
	
	//verificamos session
	$('li a').click(function(e){
		//e.preventDefault();
		$.ajax({
			  url: 'check_session.jsp',
			  success: function(data) {
				var resp  = $(data).find("session").text();
				if(resp == 'true'){
					
				}
				else{
					window.top.location.href = "index.jsp"; //recarga todo el iframe contenedor 
				}
					
			    
			  }
			});
		
	});
	
	
});


function logout(){
	
	
	setTimeout(function() {
		//window.location.reload(true);
		$("#main").show();
		$("#main").slideUp('slow');
	},200);

}

</script>
    



</head>
<body>

<div id="main" style="display: none">
		<ul id="browser" class="filetree treeview">
		
		<ul>

  <%
	if (login && type.equals("admin")){
		//menu si la sesion existe
		
		%>
		<li class="closed"><span class="folder"><a href="ServerConf.jsp" target="main_frame">1.Configuracion</a> </span></li>
		<li class="closed"><span class="folder">2.Repositorios</span>
		
			<ul>
				<li><span class="file"><a  href="Provider.jsp" target="main_frame">Agregar</a> </span></li>
				<li><span class="file"><a  href="ListProviders.jsp" target="main_frame">Editar</a> </span></li>
				<li><span class="file"><a  href="ListRepositories.jsp" target="main_frame">Listar Sencillo</a> </span></li>
				<li><span class="file"><a  href="ListRepositoriesDetails.jsp" target="main_frame">Listar Detallado</a> </span></li>
				<li><span class="file"><a  href="TreeRepositories.jsp" target="main_frame">Navegar</a> </span></li>
			</ul>
		</li>
		<li class="closed"><span class="folder">3.Recolector</span>
			<ul>
				<li><span class="file"><a href="recolector.jsp" target="main_frame">Recolectar</a> </span>
				<li><span class="file"><a href="StatsForHarvester.jsp" target="main_frame">Estadisticas</a> </span></li>
			</ul>
		</li>
		<li class="closed"><span class="folder">4.Validador</span>
			<ul>
				<li><span class="file"><a href="ListValidations.jsp" target="main_frame">Validador BDCOL</a> </span></li>
				<li class="closed"><span class="folder">Validador REDA</span>
					<ul>
						<li><span class="file"><a href="ListValidationsReda.jsp" target="main_frame">Validar REDA</a> </span>
						<li><span class="file"><a href="LogsValidationsReda.jsp" target="main_frame">Ver Logs</a> </span>
					</ul>
				</li>
			</ul>
		</li>
		<li class="closed"><span class="folder"><a href="ListToPublish.jsp" target="main_frame">5.Aprobador</a> </span></li>
		<!--  <li class="closed"><span class="folder"><a href="Scheduler.jsp" target="main_frame">Programador</a> </span></li>-->
		<li class="closed"><span class="folder"><a href="Indexador.jsp" target="main_frame">6.Indexador</a> </span></li>
		<li class="closed"><span class="folder"><a onclick="logout()" href="logout.jsp" target="main_frame">7.Salir</a> </span></li>
		
		<%
		}
		
	else
		
		if (login && type.equals("adminRepo")){
			
			%>
		
			<li class="closed"><span class="folder"><a href="../bdcol/search/user/profile.xql" target="_blank">1.Mi Perfil</a> </span></li>
			<li class="closed"><span class="folder">2.Repositorios</span>
			
				<ul>
					<li><span class="file"><a  href="Provider.jsp" target="main_frame">Agregar</a> </span></li>
					<li><span class="file"><a  href="ListProviders.jsp" target="main_frame">Editar</a> </span></li>
				</ul>
			</li>
			<li class="closed"><span class="folder">3.Validador</span>
			<ul>
				<li><span class="file"><a href="ListValidations.jsp" target="main_frame">Validador BDCOL</a> </span></li>
				<li><span class="file"><a href="ListValidationsReda.jsp" target="main_frame">Validador REDA</a> </span></li>
			</ul>
		</li>
			<li class="closed"><span class="folder"><a href="ListToPublishUser.jsp" target="main_frame">4.Publicador</a> </span></li>
			<li class="closed"><span class="folder"><a onclick="logout()" href="logout.jsp" target="main_frame">5.Salir</a> </span></li>
			
			<%
		
			
			
		
			}else{
				// form de login si no existe la sesion.. 
				//out.print("form de login");
			}
	
%>
     
</ul>
</ul>
</div>


</body>
</html>