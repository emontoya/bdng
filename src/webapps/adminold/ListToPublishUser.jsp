<%@page import="java.util.Iterator"%>
<%@page import="org.bdng.harvmgr.dlConfig"%>
<%@page import="org.jdom2.Document"%>
<%@page import="java.util.List"%>
<%@page import="org.jdom2.Element"%>
<%@page import="org.jdom2.input.SAXBuilder"%>
<%@page import="org.bdng.harvmgr.harvester.DataProvider"%>
<%@page import="org.bdng.webold.ProviderWeb"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	
	        
    <%    //verificamos session como "admin" o como "adminRepo" sino, lo redirijimos al index.jsp
	HttpSession sesion = request.getSession();
    String user ="";
    String type = "";
    if(sesion.getAttribute("username")== null && sesion.getAttribute("type")== null) {
    	response.sendRedirect("index.jsp");
    	
	}else{
		user = (String)sesion.getAttribute("username");
    	type = (String)sesion.getAttribute("type");
    		if(!type.equals("adminRepo") && !type.equals("admin"))
    		
    			response.sendRedirect("index.jsp");
	} %>
    
<%

    if(sesion.getAttribute("username")!= null)
    	user = (String)sesion.getAttribute("username");
	
    dlConfig doa = new dlConfig(this.getServletContext().getRealPath("/"));
   	List l = doa.GetVisualProviders();
	
   	
	DataProvider dp = new DataProvider(this.getServletContext().getRealPath("/"));
	
	
	
	Element oai_provider = null;
	
	Element shortName = null;
	Element repositoryName = null;
	Element baseURL = null;
	Element collection = null;
	Element metadataPrefix = null;
	Element protocol = null;
	Element owner = null;
  	
  	
  	
  	    %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Listar Proveedores</title>

<title>DataTables example</title>
<style type="text/css" title="currentStyle">

@import "DataTables/media/css/demo_table.css";
</style>
<link type="text/css" href="css/styles.css" rel="stylesheet"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript" src="js/app/appJSP.js"></script>
<script type="text/javascript" src="js/spin.min.js"></script>
<script type="text/javascript" language="javascript"
	src="DataTables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="js/jquery.tipTip.js"></script>
<link type="text/css" href="css/tipTip.css" rel="stylesheet"/>
<script type="text/javascript" charset="utf-8">

//Opciones del spinner (loading)
var opts = {
		
		
		lines: 16, // The number of lines to draw
		length: 1, // The length of each line
		width: 5, // The line thickness
		radius: 16, // The radius of the inner circle
		color: '#fff', // #rgb or #rrggbb
		speed: 1, // Rounds per second
		trail: 73, // Afterglow percentage
		shadow: false // Whether to render a shadow
};

	$(document).ready(function() {
		$(".toltip").tipTip();
		$(document).on("click",".elements img",function(e){
// 			console.log("oeee");
			$("#cargando").show();
		});
		
		var target = document.getElementById('cargando');
		var spinner = new Spinner(opts).spin(target);
		$("#cargando").hide();
		$('#example').dataTable();
		
	});
</script>

</head>
<body id="dt_exampleX">
<div id="cargando" class="loadingDiv"></div>
	<div id="container">
		<div class="full_width big"></div>
		<div id="demo">
		<div id=title_section> Publicacion de proveedores </div>
		</br>

			<table cellpadding="0" cellspacing="0" border="0" class="display"
				id="example">
				<thead>
					<tr>
						<th>Publicar</th>
						<th>ID-rep</th>
						<th>Repositorio</th>
						<th>Resultado Validacion</th>
				</tr>
				</thead>
				<tbody>
					<%
			String active = "5";
    
for(int j = 0; j<l.size();j++){
		  		
  			oai_provider=(Element)l.get(j);
  			
			shortName=oai_provider.getChild("shortName");
			owner=oai_provider.getChild("owner");

			active = dp.getActiveStatus(shortName.getText());
		
				if(active.equals("1")) continue;
  		
				if(	!owner.getText().equals(user)) continue;

  			shortName=oai_provider.getChild("shortName");
  			repositoryName=oai_provider.getChild("repositoryName");
  		//	baseURL=oai_provider.getChild("baseURL");
  			collection=oai_provider.getChild("collection");
  			metadataPrefix=oai_provider.getChild("metadataPrefix");
  			protocol=oai_provider.getChild("protocol");
			
  		    			
          out.print("<tr class=\"gradeU\">");
          out.print("<td class='elements'><A class='toltip' title='Click aqui para publicar este repositorio' HREF=\"servlet/PublishWeb?action=publish&shortname="+shortName.getText()+"&owner="+owner.getText()+"\"><img style='cursor:pointer;' src=\"images/btn_publish.png\" width=\"20\" height=\"20\" border=\"0\"></A></td>");
          
          out.print("<td>"+shortName.getText()+"</td>");
          out.print("<td>"+repositoryName.getText()+"</td>");
        //  out.print("<td>"+baseURL.getText()+"</td>");
          out.print("<td> "+doa.GetScoreValidation(shortName.getText())+"</td>");
          out.print("</tr>");
          
      }    
    
    %>
				</tbody>
				<tfoot>
					<tr>
						<th>Publicar</th>						
						<th>ID-rep</th>
						<th>Repositorio</th>
						<th>Resultado Validacion</th>
					</tr>
				</tfoot>
			</table>
</body>
</html>
