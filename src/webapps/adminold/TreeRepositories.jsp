<%@page import="java.util.TreeMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.bdng.harvmgr.dlConfig"%>
<%@page import="org.jdom2.Document"%>
<%@page import="java.util.List"%>
<%@page import="org.jdom2.Element"%>
<%@page import="org.jdom2.input.SAXBuilder"%>
<%@page import="org.bdng.harvmgr.harvester.DataProvider"%>
<%@page import="org.bdng.webold.ProviderWeb"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%

	// this file is public, dont have session -luis
	dlConfig doa = new dlConfig(this.getServletContext().getRealPath("/"));
	List l = doa.GetVisualProviders();
	
   	DataProvider dp = new DataProvider(this.getServletContext().getRealPath("/"));

	Element oai_provider = null;
	
	Element shortName = null;

	Element departamento = null;
	Element municipio = null;
	Element institution = null;
	Element repositoryName = null;
	Element urlhome = null;
	
	Element owner = null;
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
                    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	
	<link rel="stylesheet" href="http://jquery.bassistance.de/treeview/demo/screen.css" type="text/css" />
	<link rel="stylesheet" href="http://jquery.bassistance.de/treeview/jquery.treeview.css" type="text/css" />
	<link type="text/css" href="./styles/repositoriesTrees.css" rel="stylesheet"/>
	<link type="text/css" href="./styles/nanoscroller.css" rel="stylesheet"/>
	<link type="text/css" href="css/styles.css" rel="stylesheet"/>
	
	<script src="http://code.jquery.com/jquery-latest.js"></script>
	<script type="text/javascript" src="http://jquery.bassistance.de/treeview/jquery.treeview.js"></script>
	<script type="text/javascript" src="./js/jquery.nanoscroller.min.js"></script>
		
	
	
	<script type="text/javascript">
		$(document).ready(function(){		
			$("#browser").treeview();
		});
	</script>



</head>
<body>
	<div class="containerTree">
		<div class=title_section><label>Repositorios</label></div></br>
		<div class="treeListContainer "> 
			<div class="nano">
				<div class="content">
					<ul id="browser" class="filetree treeview">
			
			
			
					<%
						TreeMap<String, List<Element>> SortByDepto = new TreeMap<String, List<Element>>();
						TreeMap<String, List<Element>> SortByInst = new TreeMap<String, List<Element>>();
			
						
			    		String active = "5";
			    	    
			    	    
			for(int j = 0; j<l.size();j++){
					  		
			  		oai_provider=(Element)l.get(j);
			  		
						owner=oai_provider.getChild("owner");
						if(	!owner.getText().equals("admin")) continue;
						
						shortName=oai_provider.getChild("shortName");
			  			active = dp.getActiveStatus(shortName.getText());
			  	  		
						if(!active.equals("0")) continue;
			  		
			
							departamento = oai_provider.getChild("departamento");
							municipio = oai_provider.getChild("municipio");
							institution = oai_provider.getChild("institution");
							repositoryName = oai_provider.getChild("repositoryName");
							urlhome = oai_provider.getChild("urlhome");
			
							if (SortByDepto.containsKey(departamento.getText())) {
								List actual = SortByDepto.get(departamento.getText());
								actual.add(oai_provider);
								SortByDepto.put(departamento.getText(), actual);
			
							} else {
			
								List actual = new ArrayList();
								actual.add(oai_provider);
								SortByDepto.put(departamento.getText(), actual);
			
							}
							
							
							if (SortByInst.containsKey(institution.getText())) {
								List actual = SortByInst.get(institution.getText());
								actual.add(oai_provider);
								SortByInst.put(institution.getText(), actual);
			
							} else {
			
								List actual = new ArrayList();
								actual.add(oai_provider);
								SortByInst.put(institution.getText(), actual);
			
							}
			
						}
						
						
						
					%>
						 <!-- Ordenamiento por Departamento -->
					
					<li class="closed"><span class="folder"> Departamentos </span> <ul><%
							
							
						for (String depto : SortByDepto.keySet()) {
					%>
					
					
					<li class="closed"><span class="folder"> <%out.print(depto); %></span> <%
					
			 		List actual = SortByDepto.get(depto);
			 		for (int j = 0; j < actual.size(); j++) {
			
			 			oai_provider = (Element) actual.get(j);
			
			 			departamento = oai_provider.getChild("departamento");
			 			municipio = oai_provider.getChild("municipio");
			 			institution = oai_provider.getChild("institution");
			 			repositoryName = oai_provider.getChild("repositoryName");
			 			urlhome = oai_provider.getChild("urlhome");
			 %>
						<ul>
							<li><span class="file"> <%out.print(institution.getText()+ " -- <A target=\"_blank\" HREF=\""+ urlhome.getText() + "\">"+ repositoryName.getText() + "  </A>"); %></span></li>
						</ul> <%
			 	}
			 %></li>
			
					<%
						}
					%>
				</ul>
				</li>
				
				 <!-- Ordenamiento por Institucion -->
				 
				 <li class="closed"><span class="folder"> Instituciones </span> <ul><%
							
							
						for (String inst : SortByInst.keySet()) {
					%>
					
					
					<li class="closed"><span class="folder"> <%out.print(inst); %></span> <%
					
			 		List actual = SortByInst.get(inst);
			 		for (int j = 0; j < actual.size(); j++) {
			
			 			oai_provider = (Element) actual.get(j);
			
			 			departamento = oai_provider.getChild("departamento");
			 			municipio = oai_provider.getChild("municipio");
			 			institution = oai_provider.getChild("institution");
			 			repositoryName = oai_provider.getChild("repositoryName");
			 			urlhome = oai_provider.getChild("urlhome");
			 %>
						<ul>
							<li><span class="file"> <%out.print("<A target=\"_blank\" HREF=\""+ urlhome.getText() + "\">"+ repositoryName.getText() + "  </A>"); %></span></li>
						</ul> <%
			 	}
			 %></li>
			
					<%
						}
					%>
				</ul>
				</li>
					
				</ul>
				 </div> 
			</div>		
		</div>
				
	</div>	
	<script type="text/javascript">
		$(".nano").nanoScroller();
	</script>
</body>
</html>
