<%@page import="java.util.Iterator"%>
<%@page import="org.bdng.harvmgr.dlConfig"%>
<%@page import="org.jdom2.Document"%>
<%@page import="java.util.List"%>
<%@page import="org.jdom2.Element"%>
<%@page import="org.jdom2.input.SAXBuilder"%>
<%@page import="org.bdng.harvmgr.harvester.DataProvider"%>
<%@page import="org.bdng.webold.ProviderWeb"%>
 <%@page import="org.bdng.webold.Utils"%>   
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	
	        
    <%    //verificamos session como "admin" o como "adminRepo" sino, lo redirijimos al index.jsp
	HttpSession sesion = request.getSession();
    String user ="";
    String type = "";
    if(sesion.getAttribute("username")== null && sesion.getAttribute("type")== null) {
    	response.sendRedirect("index.jsp");
    	
	}else{
		user = (String)sesion.getAttribute("username");
    	type = (String)sesion.getAttribute("type");
    		if(!type.equals("adminRepo") && !type.equals("admin"))
    		
    			response.sendRedirect("index.jsp");
	} %>
    
<%

    if(sesion.getAttribute("username")!= null)
    	user = (String)sesion.getAttribute("username");
	
	dlConfig doa = new dlConfig(this.getServletContext().getRealPath("/"));
	List l = doa.GetVisualProviders();

	DataProvider dp = new DataProvider(this.getServletContext().getRealPath("/"));
	
	
	Element oai_provider = null;
	
	Element shortName = null;
	Element repositoryName = null;
	Element baseURL = null;
	Element collection = null;
	Element metadataPrefix = null;
	Element owner = null;
	
	Utils u = new Utils(this.getServletContext().getRealPath("/"));
	u.uploadConf("oai_providers"); //se sube el archivo ServerConfig.xml a eXist
  	
  	
  	
  	    %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Listar Proveedores</title>

<title>DataTables example</title>
<link type="text/css" href="css/styles.css" rel="stylesheet"/>
<style type="text/css" title="currentStyle">
@import "DataTables/media/css/demo_table.css";
</style>
<script type="text/javascript" language="javascript"
	src="DataTables/media/js/jquery.js"></script>
<script type="text/javascript" language="javascript"
	src="DataTables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="js/jquery.tipTip.js"></script>
<link type="text/css" href="css/tipTip.css" rel="stylesheet"/>
<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		$(".toltip").tipTip();
		$('#example').dataTable();
	});
</script>

</head>
<body id="dt_exampleX">
	<div id="container">
		<div class="full_width big"></div>
		<div id=title_section>Edicion de Repositorios</div>
		</br>
		<div id="demo">

			<table cellpadding="0" cellspacing="0" border="0" class="display"
				id="example">
				<thead>
					<tr>
						<th>Borrar</th>
						<th>Editar</th>
						<th>ID-rep</th>
						<th>Repositorio</th>
						<th>URL</th>
						<th>Coleccion</th>
						<th>Metadata</th>


					</tr>
				</thead>
				<tbody>
					<%
					String active = "5";
    
for(int j = 0; j<l.size();j++){
		  		
  		oai_provider=(Element)l.get(j);
  			
			shortName=oai_provider.getChild("shortName");
			owner=oai_provider.getChild("owner");

			active = dp.getActiveStatus(shortName.getText());
  			
  			if( !user.equals("admin") && active.equals("1")) continue; // aqui verificamos que un usuario no pueda editar un provider que esta en proceso de aceptacion (active=1)
  			
			if(	!owner.getText().equals(user)) continue; //si no le pertenece se lo salta
			
  			shortName=oai_provider.getChild("shortName");
  			repositoryName=oai_provider.getChild("repositoryName");
  			baseURL=oai_provider.getChild("baseURL");
  			collection=oai_provider.getChild("collection");
  			metadataPrefix=oai_provider.getChild("metadataPrefix");
  			
			
  		    			
          out.print("<tr class=\"gradeU\">");
          out.print("<td><A class='toltip' title='Click aqui para eliminar este repositorio' HREF=\"servlet/ProviderWeb?shortname="+shortName.getText()+"&prov_opcion=opt_borrar_rep\"><img src=\"images/btn_del.png\" width=\"18\" height=\"18\" border=\"0\"></A></td>");
          out.print("<td><A class='toltip' title='Click aqui para editar este repositorio' HREF=\"Provider.jsp?shortName="+shortName.getText()+"\"><img src=\"images/btn_edit.png\" width=\"18\" height=\"18\" border=\"0\"></A></td>");
          out.print("<td>"+shortName.getText()+"</td>");
          out.print("<td>"+repositoryName.getText()+"</td>");
          out.print("<td>"+baseURL.getText()+"</td>");
          out.print("<td>"+collection.getText()+"</td>");
          out.print("<td>"+metadataPrefix.getText()+"</td>");
    	  out.print("<input name=\"shortname\" type=\"hidden\" id=\"shortname\" value=\""+shortName.getText()+"\">");
          out.print("</tr>");
          
      }    
    
    %>
				</tbody>
				<tfoot>
					<tr>
						<th>Borrar</th>
						<th>Editar</th>
						<th>ID-rep</th>
						<th>Repositorio</th>
						<th>URL</th>
						<th>Coleccion</th>
						<th>Metadata</th>

					</tr>
				</tfoot>
			</table>
			<br><br>
</body>
</html>
