<%@ page import  = "org.bdng.oai.db.DBExist,java.util.*"%>


<%
	try {
		DBExist db = new DBExist();
		Vector sets = new Vector(db.loadSets());



%>


<!doctype html public "-//w3c//dtd html 3.2 final//en">
<html>
<head>
<script language='JavaScript'>
<!--

function trim(str){
	return str.replace(/^\s*|\s*$/g,"");
}


function disableSet(){
	var form = document.formBusqueda;
	form.set.disabled = true;
}
function enableSet(){
	var form = document.formBusqueda;
	form.set.disabled = false;
}

function disableGetRecord(){
	var form = document.formBusqueda;
	form.identifier.disabled = true;
}
function enableGetRecord(){
	var form = document.formBusqueda;
	form.identifier.disabled = false;
}

function disableFromUntil(){
	var form = document.formBusqueda;
	form.from_year.disabled = true;
	form.from_month.disabled = true;
	form.from_day.disabled = true;
	form.until_year.disabled = true;
	form.until_month.disabled = true;
	form.until_day.disabled = true;
	form.from.disabled = true;
	form.until.disabled = true;
}
function enableFromUntil(){
	var form = document.formBusqueda;
	form.from_year.disabled = false;
	form.from_month.disabled = false;
	form.from_day.disabled = false;
	form.until_year.disabled = false;
	form.until_month.disabled = false;
	form.until_day.disabled = false;
	form.from.disabled = false;
	form.until.disabled = false;
}
function enableFrom() {
	var form = document.formBusqueda;
	form.from_year.disabled = true;
	form.from_month.disabled = true;
	form.from_day.disabled = true;
	form.from.disabled = false;
}
function enableUntil() {
	var form = document.formBusqueda;
	form.until_year.disabled = true;
	form.until_month.disabled = true;
	form.until_day.disabled = true;
	form.until.disabled = false;
}


function disableRT(){ //ResumptionToken
	var form = document.formBusqueda;
	form.resumptionToken.disabled = true;
}
function enableRT(){
	var form = document.formBusqueda;
	form.resumptionToken.disabled = false;
}



function enableDisable() {

	var forma = document.formBusqueda;
	var value = forma.verb.value;
	disableGetRecord();
	disableFromUntil();
	disableSet();
	disableRT();//Nuevo

	if(value == 'ListRecords' || value == 'ListIdentifiers') {
		//var 
		
		enableFromUntil();
		enableSet();
		enableRT();
		
		

	}else if(value == 'GetRecord') {
		enableGetRecord();
	}

}

function validateDateYear(date) {
	var temp = new Number(date);

	if(temp=='NaN' || temp==null) {
		return false;
	}

	if (temp > 1900 && temp < 2020) {
		return true;
	}

	return false;

}

function validateDateMonth(date) {
	var temp = new Number(date);

	if(temp=='NaN' || temp==null) {
		return false;
	}

	if (temp > 0 && temp < 13) {
		return true;
	}

	return false;

}

function validateDateDay(date) {
	var temp = new Number(date);

	if(temp=='NaN' || temp==null) {
		return false;
	}

	if (temp > 0 && temp < 32) {
		return true;
	}

	return false;

}

function validateFrom() {
	var forma = document.formBusqueda;

	var day = forma.from_day.value;
	var month = forma.from_month.value;
	var year = forma.from_year.value;

	if(validateDateDay(day) && validateDateMonth(month) && validateDateYear(year)) {
		return true;
	}

	return false;

}

function validateUntil() {

	var forma = document.formBusqueda;

	var day = forma.until_day.value;
	var month = forma.until_month.value;
	var year = forma.until_year.value;

	if(validateDateDay(day) && validateDateMonth(month) && validateDateYear(year)) {
		return true;
	}

	return false;

}

function isNullFrom() {
	var forma = document.formBusqueda;
	var day = forma.from_day.value;
	var month = forma.from_month.value;
	var year = forma.from_year.value;

	if(trim(day) == "" && trim(month) == "" && trim(year)=="") {
		return true;
	}

	return false;

}

function isNullUntil(){
	var forma = document.formBusqueda;
	var day = forma.until_day.value;
	var month = forma.until_month.value;
	var year = forma.until_year.value;

	if(trim(day) == "" && trim(month) == "" && trim(year)=="") {
		return true;
	}

	return false;

}

function getFrom() {
	var forma = document.formBusqueda;
	//var from = forma.from_year.value + "-" + forma.from_month.value + "-" + forma.from_day.value;
	var from = forma.from_year.value + "" + forma.from_month.value + "" + forma.from_day.value;
	//alert(from);
	return from;
}

function getUntil() {
	var forma = document.formBusqueda;
	//var until = forma.until_year.value + "-" + forma.until_month.value + "-" + forma.until_day.value;
	var until = forma.until_year.value + "" + forma.until_month.value + "" + forma.until_day.value;
	//alert(until);
	return until;
}

function sendSearch() {
	var enviar1 = false;
	var enviar2 = false;
	var form = document.formBusqueda;

	var value = form.verb.value;
	
				disableFromUntil();	
	
	if (value == 'ListRecords' || value == 'ListIdentifiers') {
		if(form.resumptionToken.value != '') {
				disableFromUntil();
				disableSet();
				enviar1 = true;
				enviar2= true;
				
		
		} else {
			form.resumptionToken.disabled = true;
		
			if(!isNullFrom()) {
				if(validateFrom()) {
					enviar1=true;
					enableFrom();
					form.from.value = getFrom();
				}
			}else {
				enviar1 = true;
			}

			if(!isNullUntil()) {
				if(validateUntil()) {
					enableUntil();
					enviar2=true;
					form.until.value = getUntil();
				}
			}else {
				enviar2 = true;
			}
		
		}

		if(enviar1 && enviar2) {
			form.submit();
			form.reset();
		}else {
			if (!enviar1 && !enviar2){
				alert('from datestamp and until datestamp invalid');
			}else if(enviar2){
				alert('from datestamp invalid');
			}else{
				alert('until datestamp invalid');
			}
		}

	}else {
		form.submit();
		form.reset();
	}

}



-->
</script>

</head>

<body bgcolor="#ffffff" text="#000000" link="#ff0000" vlink="#800000" alink="#ff00ff" background="?">
<form name="formBusqueda"   action="../servlet/co.edu.eafit.bdng.oai.servlet.OAI" method="get" target="_blank">

<table width="100%" border="0" cellpadding="0" cellspacing="0" bordercolor="#CCCCCC">
<input  type="hidden"  name="from" disabled>
<input  type="hidden"  name="until" disabled>
   <tr>
      <td colspan="4" align="center"><strong>BUSQUEDAS EN LINEA OAI - BDEAFIT</strong></td>
  </tr>
  <tr>
        <td colspan="4">&nbsp;</td>
  </tr>

  <tr>
    <td>Action (verb):</td>
    <td>
    	<select name="verb" onChange="enableDisable()">
			<option value="Identify">Identify</option>
			<option value="GetRecord">GetRecord</option>
			<option value="ListIdentifiers">ListIdentifiers</option>
			<option value="ListRecords">ListRecords</option>
			<option value="ListSets">ListSets</option>
			<option value="ListMetadataformats">ListMetadataformats</option>
		</select>
	</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
        <td colspan="4">&nbsp;</td>
  </tr>

  <tr>

      <td colspan="4"><b>Options for GetRecord</b></td>
  </tr>
  <tr>
      <td>Set:</td>
      <td>
      	<select  name="set" disabled>
	  		<%for (Iterator iter = sets.iterator(); iter.hasNext();) {
	  			String element = (String) iter.next();
	  		%>
	     		<option value="<%=element%>"><%=element%></option>
	     	<%}%>
	  	</select>
	  </td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
  </tr>
  <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
  </tr>
  <tr>
      <td>Record Id:</td>
      <td><input type="text" name="identifier" size="10" maxlength="25" disabled></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
  </tr>
  <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
  </tr>
  <tr>
      <td colspan="4"><b>Options for Listrecords and ListIdentifiers</b></td>
  </tr>
   <tr>
      <td>from (YYYY-MM-DD):</td>
      <td><input type="text" name="from_year" size="4" maxlength="4"  disabled>&nbsp;/&nbsp;<input type="text" name="from_month" size="2" maxlength="2" disabled>&nbsp;/&nbsp;<input type="text" name="from_day" size="2" maxlength="2" disabled>
	  </td>
      <td>until (YYYY-MM-DD):</td>
      <td><input type="text" name="until_year" size="4" maxlength="4" disabled>&nbsp;/&nbsp;<input type="text" name="until_month" size="2" maxlength="2" disabled>&nbsp;/&nbsp;<input type="text" name="until_day" size="2" maxlength="2" disabled></td>
  </tr>
  <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
  </tr>
  <tr>
      <td>ResumptionToken:</td>
      <td><input type="text" name="resumptionToken" size="10" disabled></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
  </tr>


  <tr>
        <td><input type="button" name="submitname"  value="Search" onClick="sendSearch()">
		</td>
		<td><input type="reset" name="submitname"  value="Reset" ></td>
  </tr>

</table>
</form>

</body>
</html>
<%}catch(Exception e) {%>
Error
<%
}
%>