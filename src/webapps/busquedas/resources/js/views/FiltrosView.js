(function (views) {

  views.Filtros = Backbone.View.extend({

	el : '#accordionFiltros',
	
	events: {
		'click .checkboxFiltro': 'filtrar',
		'click .checkboxFiltroAvanzada': 'filtrarAvanzada',
		'click #btnLimpiarFiltros': 'limpiarFiltros',
		'click input[name=optionsRadios]:checked': 'onRadioClick'
	},
	
	refineData : new Object(),
	
	refineDataString : '',
	
	preSelected : '',
	
	filtrosPreSelected : new Array(),
	
	tipoFiltrado : 'and',

    template: _.template($('#tmpListarFiltros').html()),

    initialize: function () {

      this.collection.on('sync', this.render, this);

    },

    render: function () {

    	if((this.collection.totalRecords != 0)){
    		
    		//Variables para refrescar las categorias de los filtros donde no se ha seleccionado ninguna opcion
    		var limpiarFiltrosLenguajes = true;
    		var limpiarFiltrosContinentes = true;
    		var limpiarFiltrosDepartamentos = true;
    		var limpiarFiltrosCiudades = true;
    		var limpiarFiltrosPalabrasClave = true;
    		var limpiarFiltrosAutores = true;
    		var limpiarFiltrosExtensiones = true;
    		var limpiarFiltrosTipos = true;
    		var limpiarFiltrosAKN = true;
    		var limpiarFiltrosNBC = true;
    		var limpiarFiltrosAnios = true;
    		var limpiarFiltrosInstituciones = true;
    		
    		//Con este switch se revisa cuales categorias de filtrado tienen opciones seleccionada para que no se refresquen. 
    		for(var i=0; i < this.filtrosPreSelected.length; i++){
    			switch (this.filtrosPreSelected[i]) {
				case 'filtrosLenguajes':
					limpiarFiltrosLenguajes = false;
					break;
	    		case 'filtrosContinentes':
	    			limpiarFiltrosContinentes = false;
	    			break;
	    		case 'filtrosDepartamentos':
	    			limpiarFiltrosDepartamentos = false;
	    			break;
	    		case 'filtrosCiudades':
	    			limpiarFiltrosCiudades = false;
	    			break;
	    		case 'filtrosPalabrasClave':
	    			limpiarFiltrosPalabrasClave = false;
	    			break;
	    		case 'filtrosAutores':
	    			limpiarFiltrosAutores = false;
	    			break;
	    		case 'filtrosExtensiones':
	    			limpiarFiltrosExtensiones = false;
	    			break;
	    		case 'filtrosTipos':
	    			limpiarFiltrosTipos = false;
	    			break;
	    		case 'filtrosAKN':
	    			limpiarFiltrosAKN = false;
	    			break;
	    		case 'filtrosNBC':
	    			limpiarFiltrosNBC = false;
	    			break;
	    		case 'filtrosAnios':
	    			limpiarFiltrosAnios = false;
	    			break;
	    		case 'filtrosInstituciones':
	    			limpiarFiltrosInstituciones = false;
	    			break;
				}
        	}
    		
    		
    		if(this.refineDataString == ''){
				limpiarFiltrosLenguajes = true;
				limpiarFiltrosContinentes = true;
				limpiarFiltrosDepartamentos = true;
				limpiarFiltrosCiudades = true;
				limpiarFiltrosPalabrasClave = true;
				limpiarFiltrosAutores = true;
				limpiarFiltrosExtensiones = true;
				limpiarFiltrosTipos = true;
				limpiarFiltrosAKN = true;
				limpiarFiltrosNBC = true;
				limpiarFiltrosAnios = true;
				limpiarFiltrosInstituciones = true;
    		}
    		
			if (limpiarFiltrosLenguajes) {
				this.$('#filtrosLenguajes').empty();
			}
			if (limpiarFiltrosContinentes) {
				this.$('#filtrosContinentes').empty();
			}
			if (limpiarFiltrosDepartamentos) {
				this.$('#filtrosDepartamentos').empty();
			}
			if (limpiarFiltrosCiudades) {
				this.$('#filtrosCiudades').empty();
			}
			if (limpiarFiltrosPalabrasClave) {
				this.$('#filtrosPalabrasClave').empty();
			}
			if (limpiarFiltrosAutores) {
				this.$('#filtrosAutores').empty();
			}
			if (limpiarFiltrosExtensiones) {
				this.$('#filtrosExtensiones').empty();
			}
			if (limpiarFiltrosTipos) {
				this.$('#filtrosTipos').empty();
			}
			if (limpiarFiltrosAKN) {
				this.$('#filtrosAKN').empty();
			}
			if (limpiarFiltrosNBC) {
				this.$('#filtrosNBC').empty();
			}
			if (limpiarFiltrosAnios) {
				this.$('#filtrosAnios').empty();
			}
			if (limpiarFiltrosInstituciones) {
				this.$('#filtrosInstituciones').empty();
			}
    		
			//En la variable that se almacena el objeto asociado a this debido a que dentro del ciclo this ya no hace referencia al objeto Filtros.
    		var that = this;
    		
    		//El objeto JSON ademas de retonar el record set, tambien retorna datos de información de la consulta, entre ellos, los diferentes filtros
    		//asociados al resultado. Estos filtros se almacenan en un objeto facets que hace parte de la coleccion. 
    		//Con este ciclo se recorre cada facet, es decir, filtro, para que sea mostrado en la columna de filtros.
	    	$.each(this.collection.facets,function(k,v){

	    		for(var i=0; i < v.length; i++){
	    			var html = '';
	    			//Con esta condicion se controla si el resultado solo tiene una opcion de filtrado para una categoria. Esto porque ya no se trataria como un arreglo.
	    			if(v[i].term.length > 1){
		    			$.each(v[i].term,function(k2,v2){
		    				if(v2['#text'] == that.preSelected){
		    					html = that.template({valor : v2['#text'], freq : v2.freq, parent: v[i].label, completed : true, text : that.getTexto(v2['#text'],v[i].label)});
		    				}else{
		    					html = that.template({valor : v2['#text'], freq : v2.freq, parent: v[i].label, completed : false, text : that.getTexto(v2['#text'],v[i].label)});
		    				}
			    			switch (v[i].label) {
			    			case 'general.language':
			    				if(limpiarFiltrosLenguajes){
			    					that.$('#filtrosLenguajes').append(html);
			    				}
								break;
			    			case 'general.coverage.hierarchical.@continent':
			    				if(limpiarFiltrosContinentes){
			    					that.$('#filtrosContinentes').append(html);
			    				}
			    				break;
			    			case 'general.coverage.hierarchical.@state':
			    				if(limpiarFiltrosDepartamentos){
			    					that.$('#filtrosDepartamentos').append(html);
			    				}
			    				break;
			    			case 'general.coverage.hierarchical.@city':
			    				if(limpiarFiltrosCiudades){
			    					that.$('#filtrosCiudades').append(html);
			    				}
			    				break;
			    			case 'general.titlePack.keyword.li':
								if(limpiarFiltrosPalabrasClave){
									that.$('#filtrosPalabrasClave').append(html);
								}
			    				break;
			    			case 'lifeCycle.controlVersion.contribute.entity':
								if(limpiarFiltrosAutores){
									that.$('#filtrosAutores').append(html);
								}
			    				break;
			    			case 'technical.properties.file.@extension':
								if(limpiarFiltrosExtensiones){
									that.$('#filtrosExtensiones').append(html);
								}
			    				break;
			    			case 'educational.resourceType.@main':
								if(limpiarFiltrosTipos){
									that.$('#filtrosTipos').append(html);
								}
			    				break;
			    			case 'educational.eduProperties.specificContext.@hsKnowAreas':
								if(limpiarFiltrosAKN){
									that.$('#filtrosAKN').append(html);
								}
			    				break;
			    			case 'educational.eduProperties.specificContext.@hsBasicCore':
								if(limpiarFiltrosNBC){
									that.$('#filtrosNBC').append(html);
								}
			    				break;
			    			case 'rights.copyrightStackHolder.@year':
								if(limpiarFiltrosAnios){
									that.$('#filtrosAnios').append(html);
								}
			    				break;
							case 'rights.copyrightStackHolder.entity':
								if(limpiarFiltrosInstituciones){
									that.$('#filtrosInstituciones').append(html);
								}
								break;
							}
			    		});
	    			}else{
	    				if(v[i].term['#text'] == that.preSelected){
	    					html = that.template({valor : v[i].term['#text'], freq : v[i].term.freq, parent: v[i].label, completed : true, text : that.getTexto(v[i].term['#text'],v[i].label)});
	    				}else{
	    					html = that.template({valor : v[i].term['#text'], freq : v[i].term.freq, parent: v[i].label, completed : false, text : that.getTexto(v[i].term['#text'],v[i].label)});
	    				}
	    				switch (v[i].label) {
	    					case 'general.language':
			    				if(limpiarFiltrosLenguajes){
			    					that.$('#filtrosLenguajes').append(html);
			    				}
								break;
			    			case 'general.coverage.hierarchical.@continent':
			    				if(limpiarFiltrosContinentes){
			    					that.$('#filtrosContinentes').append(html);
			    				}
			    				break;
			    			case 'general.coverage.hierarchical.@state':
			    				if(limpiarFiltrosDepartamentos){
			    					that.$('#filtrosDepartamentos').append(html);
			    				}
			    				break;
			    			case 'general.coverage.hierarchical.@city':
			    				if(limpiarFiltrosCiudades){
			    					that.$('#filtrosCiudades').append(html);
			    				}
			    				break;
			    			case 'general.titlePack.keyword.li':
								if(limpiarFiltrosPalabrasClave){
									that.$('#filtrosPalabrasClave').append(html);
								}
			    				break;
			    			case 'lifeCycle.controlVersion.contribute.entity':
								if(limpiarFiltrosAutores){
									that.$('#filtrosAutores').append(html);
								}
			    				break;
			    			case 'technical.properties.file.@extension':
								if(limpiarFiltrosExtensiones){
									that.$('#filtrosExtensiones').append(html);
								}
			    				break;
			    			case 'educational.resourceType.@main':
								if(limpiarFiltrosTipos){
									that.$('#filtrosTipos').append(html);
								}
			    				break;
			    			case 'educational.eduProperties.specificContext.@hsKnowAreas':
								if(limpiarFiltrosAKN){
									that.$('#filtrosAKN').append(html);
								}
			    				break;
			    			case 'educational.eduProperties.specificContext.@hsBasicCore':
								if(limpiarFiltrosNBC){
									that.$('#filtrosNBC').append(html);
								}
			    				break;
			    			case 'rights.copyrightStackHolder.@year':
								if(limpiarFiltrosAnios){
									that.$('#filtrosAnios').append(html);
								}
			    				break;
							case 'rights.copyrightStackHolder.entity':
								if(limpiarFiltrosInstituciones){
									that.$('#filtrosInstituciones').append(html);
								}
								break;
						}
	    			}
	    		}
	    	});
    	}
    },
    
    //Con esta funcion se cambian los valores que vienen de la base de datos, en un formato de codigo, al verdadero valor para mostrar en la vista. 
    //Si se desea agregar una nueva categoria de filtrado en la cual se necesiten transformas los valores textuales, se debe agregar el respectivo caso.
    getTexto: function(textoDefault, label){
    	switch (label) {
			case 'general.language':
				switch (textoDefault) {
					case 'es':
						return 'Espa&ntilde;ol';
						break;
					case 'en':
						return 'Ingl&eacute;es';
						break;
					default:
						return textoDefault;
						break;
				}
				break;
			case 'general.coverage.hierarchical.@continent':
				switch (textoDefault) {
					case 'SA':
						return 'Sur Am&eacute;rica';
						break;
					case 'NA':
						return 'Norte Am&eacute;rica';
						break;
					case 'CA':
						return 'Centro Am&eacute;rica';
						break;
					case 'EU':
						return 'Europa';
						break;
					case 'AF':
						return '&Aacute;frica';
						break;
					case 'AO':
						return 'Ocean&iacute;a';
						break;
					case 'AS':
						return 'Asia';
						break;
					case 'CR':
						return 'Caribe';
						break;
					case 'AN':
						return 'Antarctica';
						break;
					default:
						return textoDefault;
						break;
				}
				break;
			case 'general.coverage.hierarchical.@state':
				return textoDefault;
				break;
			case 'general.coverage.hierarchical.@city':
				return textoDefault;
				break;
			case 'general.titlePack.keyword.li':
				return textoDefault;
				break;
			case 'lifeCycle.controlVersion.contribute.entity':
				return textoDefault;
				break;
			case 'technical.properties.file.@extension':
				return textoDefault;
				break;
			case 'educational.resourceType.@main':
				switch (textoDefault) {
					case 'CW':
						return 'Curso Virtual';
						break;
					case 'LO':
						return 'Objeto de Aprendizaje';
						break;
					case 'EA':
						return 'Aplicaci&oacute;n Educativa';
						break;
					case 'CA':
						return 'Art&iacute;culo Cient&iacute;fico';
						break;
					case 'SM':
						return 'Material de Estudio';
						break;
					case 'DA':
						return 'Medios';
						break;
					case 'NI':
						return 'Asuntos Nacionales';
						break;
					default:
						return textoDefault;
						break;
					}
				break;
			case 'educational.eduProperties.specificContext.@hsKnowAreas':
				switch (textoDefault) {
					case 'mtakn1':
						return 'Agronom&iacute;a, Veterinaria y Afines';
						break;
					case 'mtakn2':
						return 'Bellas Artes';
						break;
					case 'mtakn3':
						return 'Ciencias de la Educaci&oacute;n';
						break;
					case 'mtakn4':
						return 'Ciencias de la Salud';
						break;
					case 'mtakn5':
						return 'Ciencias Sociales y Humanas';
						break;
					case 'mtakn6':
						return 'Econom&iacute;a, Contabilidad, Administraci&oacute;n y Afines';
						break;
					case 'mtakn7':
						return 'Ingenier&iacute;a, Arquitectura, Urbanismo y Afines';
						break;
					case 'mtakn8':
						return 'Matem&aacute;ticas y Ciencias Naturales';
						break;
					default:
						return textoDefault;
						break;
					}
				break;
			case 'educational.eduProperties.specificContext.@hsBasicCore':
				switch (textoDefault) {
					case 'mtnbc1':
						return 'Agronom&iacute;a';
						break;
					case 'mtnbc2':
						return 'Medicina Veterinaria';
						break;
					case 'mtnbc3':
						return 'Zootecnia';
						break;
					case 'mtnbc4':
						return 'Artes Pl&aacute;sticas, Visuales y Afines';
						break;
					case 'mtnbc5':
						return 'Artes Representativas';
						break;
					case 'mtnbc6':
						return 'Diseño';
						break;
					case 'mtnbc7':
						return 'M&uacute;sica';
						break;
					case 'mtnbc8':
						return 'Otros Programas Asociados a Bellas Artes';
						break;
					case 'mtnbc9':
						return 'Publicidad y Afines';
						break;
					case 'mtnbc10':
						return 'Educaci&oacute;n';
						break;
					case 'mtnbc11':
						return 'Bacteriolog&iacute;a';
						break;
					case 'mtnbc12':
						return 'Enfermer&iacute;a';
						break;
					case 'mtnbc13':
						return 'Instrumentaci&oacute;n Quir&uacute;rgica';
						break;
					case 'mtnbc14':
						return 'Medicina';
						break;
					case 'mtnbc15':
						return 'Nutrici&oacute;n y Diet&eacute;tica';
						break;
					case 'mtnbc16':
						return 'Odontolog&iacute;a';
						break;
					case 'mtnbc17':
						return 'Optometr&iacute;a, Otros Programas de Ciencias de la Salud';
						break;
					case 'mtnbc18':
						return 'Salud P&uacute;blica';
						break;
					case 'mtnbc19':
						return 'Terapias';
						break;
					case 'mtnbc20':
						return 'Antropolog&iacute;a, Artes Liberales';
						break;
					case 'mtnbc21':
						return 'Bibliotecolog&iacute;a, Otros de Ciencias Sociales y Humanas';
						break;
					case 'mtnbc22':
						return 'Ciencia Pol&iacute;tica, Relaciones Internacionales';
						break;
					case 'mtnbc23':
						return 'Comunicaci&oacute;n Social, Periodismo y Afines';
						break;
					case 'mtnbc24':
						return 'Deportes, Educaci&oacute;n F&iacute;sica y Recreaci&oacute;n';
						break;
					case 'mtnbc25':
						return 'Derecho y Afines';
						break;
					case 'mtnbc26':
						return 'Filosof&iacute;a, Teolog&iacute;a y Afines';
						break;
					case 'mtnbc27':
						return 'Formaci&oacute;n Relacionada con el Campo Militar o Policial';
						break;
					case 'mtnbc28':
						return 'Geograf&iacute;a, Historia';
						break;
					case 'mtnbc29':
						return 'Lenguas Modernas, Literatura, Lingü&iacute;stica y Afines';
						break;
					case 'mtnbc30':
						return 'Psicolog&iacute;a';
						break;
					case 'mtnbc31':
						return 'Sociolog&iacute;a, Trabajo Social y Afines';
						break;
					case 'mtnbc32':
						return 'Administraci&oacute;n';
						break;
					case 'mtnbc33':
						return 'Contadur&iacute;a P&uacute;blica';
						break;
					case 'mtnbc34':
						return 'Econom&iacute;a';
						break;
					case 'mtnbc35':
						return 'Arquitectura';
						break;
					case 'mtnbc36':
						return 'Ingenier&iacute;a Administrativa y Afines';
						break;
					case 'mtnbc37':
						return 'Ingenier&iacute;a Agr&iacute;cola, Forestal y Afines';
						break;
					case 'mtnbc38':
						return 'Ingenier&iacute;a Agron&oacute;mica, Pecuaria y Afines';
						break;
					case 'mtnbc39':
						return 'Ingenier&iacute;a Agroindustrial, Alimentos y Afines';
						break;
					case 'mtnbc40':
						return 'Ingenier&iacute;a Ambiental, Sanitaria y Afines';
						break;
					case 'mtnbc41':
						return 'Ingenier&iacute;a Biom&eacute;dica y Afines';
						break;
					case 'mtnbc42':
						return 'Ingenier&iacute;a Civil y Afines';
						break;
					case 'mtnbc43':
						return 'Ingenier&iacute;a de Minas, Metalurgia y Afines';
						break;
					case 'mtnbc44':
						return 'Ingenier&iacute;a de Sistemas, Telem&aacute;tica y Afines';
						break;
					case 'mtnbc45':
						return 'Ingenier&iacute;a El&eacute;ctrica y Afines';
						break;
					case 'mtnbc46':
						return 'Ingenier&iacute;a Electr&oacute;nica, Telecomunicaciones y Afines';
						break;
					case 'mtnbc47':
						return 'Ingenier&iacute;a Industrial y Afines';
						break;
					case 'mtnbc48':
						return 'Ingenier&iacute;a Mec&aacute;nica y Afines';
						break;
					case 'mtnbc49':
						return 'Ingenier&iacute;a Qu&iacute;mica y Afines';
						break;
					case 'mtnbc50':
						return 'Otras Ingenier&iacute;as';
						break;
					case 'mtnbc51':
						return 'Biolog&iacute;a, Microbiolog&iacute;a y Afines';
						break;
					case 'mtnbc52':
						return 'F&iacute;sica';
						break;
					case 'mtnbc53':
						return 'Geolog&iacute;a, Otros Programas de Ciencias Naturales';
						break;
					case 'mtnbc54':
						return 'Matem&aacute;ticas, Estad&iacute;stica y Afines';
						break;
					case 'mtnbc55':
						return 'Qu&iacute;mica y Afines';
						break;
					default:
						return textoDefault;
						break;
					}
				break;
			case 'rights.copyrightStackHolder.@year':
				return textoDefault;
				break;
			case 'rights.copyrightStackHolder.entity':
				return textoDefault;
				break;
    	}
    },
    
    filtrar: function(e){
		//Se valida si la busqueda es basica o avanzada para que el evento no se ejecute dos veces
    	if(this.collection.tipoBusqueda == 'basica'){
    		console.log("hola");
	    	this.busquedaRefinada(e);
    	}
    	
		
	},
	
	filtrarAvanzada: function(e){
		//Se valida si la busqueda es basica o avanzada para que el evento no se ejecute dos veces
    	if(this.collection.tipoBusqueda == 'avanzada'){
    		console.log("hola");
	    	this.busquedaRefinada(e);
    	}
    	
		
	},
	
	//Esta funcion se ejecuta cada que se selecciona o deselecciona un filtro. Si se selecciona se agrega a un arreglo con los filtros seleccionados,
	//Si se deselecciona se elimina del arreglo. Luego se recorre este arreglo y se arma la consulta que se debe hacer en la base de datos segun los
	//filtros seleccionados. 
	busquedaRefinada: function(e){
		$('#pagination').empty();
		$('#paginationResults').empty();
		$('#searchContent').empty();
		$('#cargando').show();
		this.refineDataString = '';
		if(e.currentTarget.checked){
			var that = this;
			this.refineData[$(e.currentTarget).data("parent")] = new Array();
			console.log(e.currentTarget);
			
			//Con esta condicion se verifica si se esta haciendo un filtrado en OR o en AND. Si es en AND al seleccionar un filtro
			//se borran el resto de posibles opciones en la categoria. Si es en OR las opciones no se borran y quedan disponibles,
			//para sumarse a los resultados de cada filtrado. 
			if(this.tipoFiltrado == 'and'){
				that.refineData[$(e.currentTarget).data("parent")].push(e.currentTarget.value);
				this.$(e.currentTarget).closest('.accordion-inner').find("label").each(function(){
					if((e.currentTarget)!==($(this)[0].children[0])){
						console.log("entro");
						$(this).remove();
					}
				});
			}else if(this.tipoFiltrado == 'or'){
				this.$(e.currentTarget).closest('.accordion-inner').find("input[type=checkbox]:checked").each(function(){
					that.refineData[$(e.currentTarget).data("parent")].push(this.value);
				});
			}
			console.log(this.refineData);
			this.filtrosPreSelected.push(this.$(e.currentTarget).closest('.accordion-inner')[0].id);
		}else{
			var position = jQuery.inArray(e.currentTarget.value, this.refineData[$(e.currentTarget).data("parent")]);
			this.refineData[$(e.currentTarget).data("parent")].splice(position,1);
			if(this.refineData[$(e.currentTarget).data("parent")].length == 0){
				delete this.refineData[$(e.currentTarget).data("parent")];
			}
			console.log(this.refineData);
			
			var positionSeleccionados = jQuery.inArray(this.$(e.currentTarget).closest('.accordion-inner')[0].id, this.filtrosPreSelected);
			this.filtrosPreSelected.splice(positionSeleccionados,1);
			
		}
		
		//Con este for se recorre el arreglo de los filtros seleccionados y se va armando un string con la consulta a realizar
		for(var i in this.refineData){
			var node = '';
			var ancestors = '';
			var ns = 'dmp:';
			switch (i) {
			case 'general.language':
				node = 'language';
				ancestors = '[dmp:doer/dmp:general';
				break;
			case 'general.coverage.hierarchical.@continent':
				node = '@continent';
				ns = '';
				ancestors = '[dmp:doer/dmp:general/dmp:coverage/dmp:hierarchical';
				break;
			case 'general.coverage.hierarchical.@state':
				node = '@state';
				ns = '';
				ancestors = '[dmp:doer/dmp:general/dmp:coverage/dmp:hierarchical';
				break;
			case 'general.coverage.hierarchical.@city':
				node = '@city';
				ns = '';
				ancestors = '[dmp:doer/dmp:general/dmp:coverage/dmp:hierarchical';
				break;
			case 'general.titlePack.keyword.li':
				node = 'li';
				ancestors = '[dmp:doer/dmp:general/dmp:titlePack/dmp:keyword';
				break;
			case 'lifeCycle.controlVersion.contribute.entity':
				node = 'entity';
				ancestors = '[dmp:doer/dmp:lifeCycle/dmp:controlVersion/dmp:contribute';
				break;
			case 'technical.properties.file.@extension':
				node = '@extension';
				ns = '';
				ancestors = '[dmp:doer/dmp:technical/dmp:properties/dmp:file';
				break;
			case 'educational.resourceType.@main':
				node = '@main';
				ns = '';
				ancestors = '[dmp:doer/dmp:educational/dmp:resourceType';
				break;
			case 'educational.eduProperties.specificContext.@hsKnowAreas':
				node = '@hsKnowAreas';
				ns = '';
				ancestors = '[dmp:doer/dmp:educational/dmp:eduProperties/dmp:specificContext';
				break;
			case 'educational.eduProperties.specificContext.@hsBasicCore':
				node = '@hsBasicCore';
				ns = '';
				ancestors = '[dmp:doer/dmp:educational/dmp:eduProperties/dmp:specificContext';
				break;
			case 'rights.copyrightStackHolder.@year':
				node = '@year';
				ns = '';
				ancestors = '[dmp:doer/dmp:rights/dmp:copyrightStackHolder';
				break;
			case 'rights.copyrightStackHolder.entity':
				node = 'entity';
				ancestors = '[dmp:doer/dmp:rights/dmp:copyrightStackHolder';
				break;
			}
			this.refineDataString += ancestors + "[ft:query(" + ns + node + ",<query>";
			for(var j in this.refineData[i]){
				this.refineDataString += "<phrase>"+this.refineData[i][j]+"</phrase>";
			}
			this.refineDataString +="</query>)]]";
		}
		
		//Al final se ejecuta el metodo pager() de la coleccion el cual vuelve a invocar el servicio ya sea de busqueda basica o avanzada
		//y esta vez se envía el parametro refineBusqueda con el query de los filtros. El servicio valida esto e invoca las funciones en
		//xquery para efectuar los filtros. 
		this.collection.refineBusqueda = this.refineDataString;
		this.collection.currentPage = 1;
		this.collection.pager();
		console.log(this.refineDataString);
	},
	
	limpiarFiltros: function(){
		if(this.filtrosPreSelected.length != 0){
			console.log(this.filtrosPreSelected.length);
			$('#pagination').empty();
			$('#paginationResults').empty();
			$('#searchContent').empty();
			$('#cargando').show();
			
			this.refineData = new Object();
			this.filtrosPreSelected = new Array();
			this.refineDataString = '';
			this.collection.refineBusqueda = '';
			this.collection.currentPage = 1;
			this.collection.pager();
		}
	},
	
	//Con esta funcion se evalua si los filtros se realizan en OR o en AND.
	onRadioClick: function(){
		console.log($('input[name=optionsRadios]:checked').val());
		this.tipoFiltrado = $('input[name=optionsRadios]:checked').val();
		this.limpiarFiltros();
	},

  });

})( app.views );
