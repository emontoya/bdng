(function (views) {

  views.PaginatedViewAvanzada = Backbone.View.extend({

    events: {
      'click a.servernextavanzada': 'nextResultPage',
      'click a.serverpreviousavanzada': 'previousResultPage',
      'click a.orderUpdate': 'updateSortBy',
      'click a.serverlastavanzada': 'gotoLast',
      'click a.pageavanzada': 'gotoPage',
      'click a.serverfirstavanzada': 'gotoFirst',
      'click a.serverpage': 'gotoPage',
      'click .serverhowmanyavanzada a': 'changeCount'

    },

    //tagName: 'aside',
	el : '#pagination',

    template: _.template($('#tmpServerPaginationAvanzada').html()),

    initialize: function () {

      this.collection.on('reset', this.render, this);
      this.collection.on('sync', this.render, this);
      //console.log("entro");

      //this.$el.appendTo('#pagination');

    },

    render: function () {
      var html = this.template(this.collection.info());
      this.$el.html(html);
      //console.log(this.collection);
		$(".modal").on("show", function () {
		  $("body").addClass("modal-open");
		}).on("hidden", function () {
		  $("body").removeClass("modal-open");
		});

    },

    updateSortBy: function (e) {
      e.preventDefault();
      var currentSort = $('#sortByField').val();
      this.collection.updateOrder(currentSort);
    },

    nextResultPage: function (e) {
      e.preventDefault();
      this.collection.requestNextPage();
    },

    previousResultPage: function (e) {
      e.preventDefault();
      this.collection.requestPreviousPage();
    },

    gotoFirst: function (e) {
      e.preventDefault();
      this.collection.goTo(this.collection.information.firstPage);
    },

    gotoLast: function (e) {
      e.preventDefault();
      this.collection.goTo(this.collection.information.lastPage);
    },

    gotoPage: function (e) {
      e.preventDefault();
      var page = $(e.target).text();
      this.collection.goTo(page);
    },

    changeCount: function (e) {
      e.preventDefault();
      var per = $(e.target).text();
      this.collection.howManyPer(per);
    }

  });

})( app.views );
