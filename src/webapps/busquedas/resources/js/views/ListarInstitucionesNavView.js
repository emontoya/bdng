(function ( views ) {

	views.InstitucionNavViewList = Backbone.View.extend({
//		el : '#selectInstitucion',
		
//		el : '#instituciones-nav',

		initialize : function () {

			var tags = this.collection;
			
			tags.on('add', this.addOne, this);
			tags.on('all', this.render, this);

		},
		


		addOne : function ( Institucion ) {
			var view = new views.InstitucionNavView({model:Institucion});
			$(this.el).append(view.render().el);
//			$('#cargando').hide();
			$('.tooltiplink').tooltip();
		},

		render: function(){
		}
	});

})( app.views );
