(function ( views ) {

	views.ResultadosAvanzada = Backbone.View.extend({
		el : '#searchContent',

		initialize : function () {

			var tags = this.collection;
			
			tags.on('add', this.addOne, this);
			tags.on('all', this.render, this);
//
//			tags.pager();

		},
		
		buscar: function() {
			var tags = this.collection;
			
			//tags.on('add', this.addOne, this);
			//tags.on('all', this.render, this);

			tags.pager();
		},

		addOne : function ( record ) {
			var view = new views.ResultView({model:record});
			$('#searchContent').append(view.render().el);
			$('#cargando').hide();
		},

		render: function(){
		}
	});

})( app.views );

