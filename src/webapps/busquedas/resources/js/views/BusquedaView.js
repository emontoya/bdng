( function ( views ){

	views.BusquedaView = Backbone.View.extend({
		
		el : '#containerPrincipal',
		
		//En este objeto se crean todos los eventos principales de la aplicación 
		events: {
			'click #btnBusquedaBasica': 'busquedaBasica',
			'keypress #query': 'findOnEnter',
			'click #btnBusquedaAvanzada': 'busquedaAvanzada',
			'click #btnBusquedaAvanzadaToggle': 'btnToggleAvanzada',
			'click #btnBusquedaBasicaToogle': 'btnToggleBasica',
			'click .buscarItemHistorial': 'buscarHistorial',
			'click .buscarItemHistorialAvanzada': 'buscarHistorialAvanzada',
			'click #verNavegacionDepartamentos': 'verNavegacionDepartamentos',
			'click .depIcon': 'mostrarInstPorDepto',
			'click .instIcon': 'busquedaNavegacion',
			'click .imgtipo': 'busquedaTipo',
			'click #verNavegacionInstituciones': 'verNavegacionInstituciones',
			
		},
		
		initialize : function () {
			//En este metodo se inicializan todos los objetos necesarios al cargar la aplicación.
			this.$('#validacionBusquedaBasica').hide();
			this.$('#validacionBusquedaAvanzada').hide();
			this.$('#cargando').hide();
		    this.$('#contenedorAvanzada').collapse({
		        toggle: false
	        });
		    this.$('#contenedorBusqueda').collapse({
		        toggle: false
	        });
		    this.$('#accordionFiltros').hide();
		    
		    this.$('#inputFechaPublicacion').datepicker();
			
		    //Se crean lo objetos necesarios para listar los departamentos
			app.collections.departamentosNavegacion = new app.collections.Departamentos();
			app.views.departamentosNavegacionView = new app.views.DepartamentosViewList({collection: app.collections.departamentosNavegacion});
			
			//Se crean los objetos necesarios para listar las instituciones dependiendo del departamento seleccionado
			app.collections.institucionesNavegacion = new app.collections.InstitucionesNav();
			app.views.institucionesNavegacionView = new app.views.InstitucionNavViewList({
				el : '#instituciones-nav',
				collection: app.collections.institucionesNavegacion
			});
			
			//Se crean los objetos necesarios para listar las instituciones en la navegacion por instituciones
			app.collections.allInstitucionesNavegacion = new app.collections.InstitucionesNav();
			app.views.allInstitucionesNavegacionView = new app.views.InstitucionNavViewList({
				el : '#solo-instituciones-nav',
				collection: app.collections.allInstitucionesNavegacion
			});
			
			this.$('#navegacionContent').hide();
			this.$('#navegacionInstContent').hide();
			
			//Se inicializan los objetos para mostrar las cantidades por tipos en el div principal
			app.models.cantidadCursosVirtuales = new app.models.CantidadTipo();
			app.models.cantidadCursosVirtuales.url = '/bdcol/servicios/ReporteCantidades.xql?tipo=CW';
			app.models.cantidadCursosVirtuales.fetch();
			app.views.cantidadCursosVirtuales = new app.views.CantidadGeneral({
				el : '#cantidadCursosVirtuales',
				model: app.models.cantidadCursosVirtuales
			});
			
			app.models.cantidadObjetosAprendizaje = new app.models.CantidadTipo();
			app.models.cantidadObjetosAprendizaje.url = '/bdcol/servicios/ReporteCantidades.xql?tipo=LO';
			app.models.cantidadObjetosAprendizaje.fetch();
			app.views.cantidadObjetosAprendizaje = new app.views.CantidadGeneral({
				el : '#cantidadObjetosAprendizaje',
				model: app.models.cantidadObjetosAprendizaje
			});
			
			app.models.cantidadAplicacionesEducacion = new app.models.CantidadTipo();
			app.models.cantidadAplicacionesEducacion.url = '/bdcol/servicios/ReporteCantidades.xql?tipo=EA';
			app.models.cantidadAplicacionesEducacion.fetch();
			app.views.cantidadAplicacionesEducacion = new app.views.CantidadGeneral({
				el : '#cantidadAplicacionesEducacion',
				model: app.models.cantidadAplicacionesEducacion
			});
			
			//Se crean dos colecciones, una para almacenar los registros de la busqueda basica y otra para almacenar los registros de la busqueda avanzada.
			//Para estos dos tipos de busquedas se crean lo objetos necesarios, colecciones, vistas y modelos. Los modelos se definen en la respectivas colecciones.
			app.collections.paginatedItems = new app.collections.Repository();
			app.collections.paginatedItemsAvanzada = new app.collections.RepositoryAvanzada();
			app.collections.institucionesList = new app.collections.Instituciones();
			app.collections.institucionesList.fetch();
			app.views.viewsPagination = new app.views.PaginatedView({collection: app.collections.paginatedItems});
			app.views.viewsPaginationResults = new app.views.PaginatedResults({collection: app.collections.paginatedItems});
			app.views.viewsPaginationResultsAvanzada = new app.views.PaginatedResultsAvanzada({collection: app.collections.paginatedItemsAvanzada});
			app.views.viewsPaginationAvanzada = new app.views.PaginatedViewAvanzada({collection: app.collections.paginatedItemsAvanzada});
			
			//Se inicializan los objetos necesarios para listar las instituciones en el div de la busqueda avanzada
			app.views.viewsInstitucionesList = new app.views.InstitucionViewList({
				el : '#selectInstitucion',
				collection: app.collections.institucionesList
			});
			app.views.viewsInstitucionesList.navegacion = false;
			
			//Se crean los ojetos necesarios para mostrar los resultados de las busquedas. Igual que las colecciones, se crea uno para cada tipo de busqueda.
			app.views.resultadosBasica = new app.views.AppView({collection: app.collections.paginatedItems});
			app.views.resultadosAvanzada = new app.views.ResultadosAvanzada({collection: app.collections.paginatedItemsAvanzada});
			
			//Se crean los objetos encargados de manejar el historial para cada tipo de busqueda
			app.views.historialBusqueda = new app.views.HistorialBusqueda({collection: app.collections.paginatedItems});
			app.views.historialBusquedaAvanzada = new app.views.HistorialBusquedaAvanzada({collection: app.collections.paginatedItemsAvanzada});
			
			//Se crean los objetos para manejar los filtros de cada tipo de busqueda.
			app.views.filtrosBusquedaBasica = new app.views.Filtros({collection: app.collections.paginatedItems});
			app.views.filtrosBusquedaAvanzada = new app.views.Filtros({collection: app.collections.paginatedItemsAvanzada});
			
			btnPresionado = false;
			
		},
		
		render: function(){
		},
		
		//Funcion empleada para realizar la busqueda basica. Se invoca precionando el boton de busqueda basica.
		busquedaBasica: function() {
			
			//En la variable value se almacena el valor obtenido del input #query, el cual es el campo de texto de la busqueda basica
			var value = this.$('#query').val().trim();
			
			//Con la variable regex se prueba que no se ingresen caracteres extranos en la
			//busqueda. Esta permitido todo lo que esta dentro de los corchetes. Si se desea
			//permitir un nuevo carecter se debe ingresar en los corchetes. Consultar la
			//validacion con RegExp en javascript.
			var regex = new RegExp("^[a-zA-Z0-9äöüßÄÖÜáéíóúñÑ ]*$");
			if(regex.test(value) == false) {
				this.$('#validacionBusquedaBasica').show();
				this.$('#validacionBusquedaAvanzada').hide();
				this.$('#navegacionContent').hide();
				this.$('#accordionFiltros').hide();
				this.$('#pagination').empty();
				this.$('#paginationResults').empty();
				this.$('#searchContent').empty();
				this.$('#contenedorAvanzada').collapse('hide');
			}else{
				if(value != ""){
					this.$('#pagination').empty();
					this.$('#paginationResults').empty();
					this.$('#searchContent').empty();
					this.$('#validacionBusquedaBasica').hide();
					this.$('#validacionBusquedaAvanzada').hide();
					this.$('#divInstitucionNav').hide();
					this.$('#navegacionContent').hide();
					
					//Se resetean las variables utilizadas para la facetacion, es decir los filtros
					app.views.filtrosBusquedaBasica.refineData = new Object();
					app.views.filtrosBusquedaBasica.filtrosPreSelected = new Array();
					app.views.filtrosBusquedaAvanzada.filtrosPreSelected = new Array();
					app.views.filtrosBusquedaBasica.refineDataString = '';
					app.collections.paginatedItems.refineBusqueda = '';

					this.$('#cargando').show();
					
					//Se setea en el objeto coleccion de la busqueda vasica el atributo valueBusquedaBasica, el cual se envia al servicio de busqueda basica. 
					//y se establece que la pagina a mostrar es la numero 1, es decir, el primer conjunto del record set. 
					app.collections.paginatedItems.valueBusquedaBasica = value;
					app.collections.paginatedItems.setCurrentPage(1);
					
					//Se ejecuta el metodo Buscar() de la vista asociada al resultado de la busqueda basica. Este metodo hace que se llene la coleccion (fetch)
					//con el objeto JSON retornado por el servicio, es decir, invoca el servicio web y llena la coleccion. 
					app.views.resultadosBasica.buscar();
					
					this.$('#query').val('');
					this.$('#contenedorAvanzada').collapse('hide');
					this.$('#accordionFiltros').show();
					
				}
				else{
					this.$('#validacionBusquedaBasica').show();
					this.$('#validacionBusquedaAvanzada').hide();
					this.$('#divInstitucionNav').hide();
					this.$('#navegacionContent').hide();
					this.$('#accordionFiltros').hide();
					this.$('#pagination').empty();
					this.$('#paginationResults').empty();
					this.$('#searchContent').empty();
					this.$('#contenedorAvanzada').collapse('hide');
				}
			}
		},
		
		//Funcion empleada para realizar la busqueda avanzada.
		busquedaAvanzada: function() {
			//Con la variable regex se prueba que no se ingresen caracteres extranos en la
			//busqueda. Esta permitido todo lo que esta dentro de los corchetes. Si se desea
			//permitir un nuevo carecter se debe ingresar en los corchetes. Consultar la
			//validacion con RegExp en javascript.
			var regex = new RegExp("^[a-zA-Z0-9äöüßÄÖÜáéíóúñÑ ]*$");
			
			//Se almacenan en variables los valores obtenidos de los campos de texto de la busqueda avanzada
			var title = this.$('#inputTitulo').val().trim();
			var author = this.$('#inputAutor').val().trim();
			var nbc = this.$('#selectNBC').val().trim();
			var tipoRecurso = this.$('#selectTipoRecurso').val().trim();
			var institution = this.$('#selectInstitucion').val().trim();
			var fechaPublicacion = this.$('#inputFechaPublicacion').val().trim();
			if((regex.test(title) == false) || (regex.test(author) == false)){
				this.$('#validacionBusquedaAvanzada').show();
				this.$('#validacionBusquedaBasica').hide();
				this.$('#accordionFiltros').hide();
				this.$('#navegacionContent').hide();
				this.$('#pagination').empty();
				this.$('#paginationResults').empty();
				this.$('#searchContent').empty();
			}else{
				if((title != "") || (author != "") || (nbc != "") || (tipoRecurso != "") || (institution != "") || (fechaPublicacion != "")){
					this.$('#pagination').empty();
					this.$('#paginationResults').empty();
					this.$('#searchContent').empty();
					this.$('#validacionBusquedaAvanzada').hide();
					this.$('#validacionBusquedaBasica').hide();
					this.$('#navegacionContent').hide();
					
					//Se resetean las variables utilizadas para la facetacion, es decir, los filtros
					app.views.filtrosBusquedaAvanzada.refineData = new Object();
					app.views.filtrosBusquedaAvanzada.filtrosPreSelected = new Array();
					app.views.filtrosBusquedaBasica.filtrosPreSelected = new Array();
					app.views.filtrosBusquedaAvanzada.refineDataString = '';
					app.collections.paginatedItemsAvanzada.refineBusqueda = '';
					app.collections.paginatedItemsAvanzada.navegacion = false;
					
					this.$('#cargando').show();
					
					app.collections.paginatedItemsAvanzada.title = title;
					app.collections.paginatedItemsAvanzada.author = author;
					app.collections.paginatedItemsAvanzada.nbc = nbc;
					app.collections.paginatedItemsAvanzada.tipoRecurso = tipoRecurso;
					app.collections.paginatedItemsAvanzada.tipo = '';
					app.collections.paginatedItemsAvanzada.institution = institution;
					app.collections.paginatedItemsAvanzada.fechaPublicacion = fechaPublicacion;
					
					//Igual que en la busqueda basica. Se setea la pagina 1 para retornar el primer conjunto de resultados de la consulta
					//y se ejecuta el metodo buscar para poblar la colección con el objeto JSON, luego de invocar el servicio de busqueda avanzada
					app.collections.paginatedItemsAvanzada.setCurrentPage(1);
					app.views.resultadosAvanzada.buscar();
					this.$('#accordionFiltros').show();
				}
				else{
					this.$('#validacionBusquedaAvanzada').show();
					this.$('#validacionBusquedaBasica').hide();
					this.$('#accordionFiltros').hide();
					this.$('#divInstitucionNav').hide();
					this.$('#navegacionContent').hide();
					this.$('#pagination').empty();
					this.$('#paginationResults').empty();
					this.$('#searchContent').empty();
				}
			}
		},
		
		//Funcion utilizada para ejecutar la busqueda basica al presionar el boton ENTER
		findOnEnter: function(event){
			if ( event.which === 13 ) {
				this.busquedaBasica();
			}
		},
		
		//Funcion utilizada para mostrar el panel de busqueda avanzada y ocultar el de busqueda basica
		btnToggleAvanzada: function(){
				this.$('#divInstitucionNav').hide();
				this.$('#navegacionContent').hide();

				this.$('#validacionBusquedaBasica').hide();
				this.$('#contenedorAvanzada').collapse('show');
				this.$('#contenedorBusquedaBasica').collapse('hide');
				
		},
		
		//Funcion utilizada para mostrar el panel de busqueda basica y ocultar el de busqueda avanzada
		btnToggleBasica: function(){
			this.$('#validacionBusquedaAvanzada').hide();
			this.$('#contenedorAvanzada').collapse('hide');
			this.$('#contenedorBusquedaBasica').collapse('show');
			this.$('#inputTitulo').val('');
			this.$('#inputAutor').val('');
			this.$('#selectNBC').val('');
			this.$('#selectTipoRecurso').val('');
			this.$('#selectInstitucion').val('');
			this.$('#inputFechaPublicacion').val('');
		},
		
		//Funcion utilizada para ejecutar el metodo de busqueda basica al dar click en un elemento del historial que pertenece a la busqueda basica
		buscarHistorial: function(e){
			
			this.$('#query').val(e.currentTarget.name);
			this.busquedaBasica();
			
		},
		
		//Funcion utilizada para ejecutar el metodo de busqueda avanzada al dar click en un elemento del historial que pertenece a la busqueda avanzada
		//Como la navegacion y la busqueda por tipos utilizan el servicio de busqueda avanzada, este tipo de eventos tambien son controlados en esta funcion
		buscarHistorialAvanzada: function(e){
			console.log($(e.currentTarget).data("titulo"));
			console.log($(e.currentTarget).data("author"));
			console.log($(e.currentTarget).data("nbc"));
			console.log($(e.currentTarget).data("tiporecurso"));
			console.log($(e.currentTarget).data("tipo"));
			console.log($(e.currentTarget).data("institution"));
			console.log($(e.currentTarget).data("fechapublicacion"));
			
			if(($(e.currentTarget).data("tipo")) == ""){
				console.log($(e.currentTarget).data("navegacion"));
				if(!($(e.currentTarget).data("navegacion"))){
					this.$('#inputTitulo').val($(e.currentTarget).data("titulo"));
					this.$('#inputAutor').val($(e.currentTarget).data("author"));
					this.$('#selectNBC').val($(e.currentTarget).data("nbc"));
					this.$('#selectTipoRecurso').val($(e.currentTarget).data("tiporecurso"));
					this.$('#selectInstitucion').val($(e.currentTarget).data("institution"));
					this.$('#inputFechaPublicacion').val($(e.currentTarget).data("fechapublicacion"));
					
					this.btnToggleAvanzada();
					this.busquedaAvanzada();
				}else if(($(e.currentTarget).data("navegacion"))){
					console.log("entro aca");
					this.busquedaNavegacion(e);
				}
			}else{
				this.busquedaTipo(e);
			}
		},
		
		//Funcion utilizada para mostrar la navegacion por departamentos, es decir, invocar el servicio que lista los departamentos y pintarlos en el panel de navegacion
		verNavegacionDepartamentos: function(){
			this.$('#validacionBusquedaAvanzada').hide();
			this.$('#validacionBusquedaBasica').hide();
			this.$('#accordionFiltros').hide();
			this.$('#contenedorAvanzada').collapse('hide');
			this.$('#contenedorBusqueda').collapse('show');

			this.$('#pagination').empty();
			this.$('#paginationResults').empty();
			this.$('#searchContent').empty();
			this.$('#departamentos-nav').empty();
			this.$('#instituciones-nav').empty();
			this.$('#solo-instituciones-nav').empty();
			this.$('#navegacionInstContent').hide();
			this.$('#navegacionContent').show();
			
			//El metodo fetch() se utiliza para ejecutar el servicio y poblar la coleccion con los departamentos.
			//Es similar al metodo buscar() de las vistas, ya que el metodo buscar en la vista, ejecuta el metodo fetch de la respectiva coleccion, por medio
			//del metodo pager() que hace parte de las librerias del paginador. 
			app.collections.departamentosNavegacion.fetch();
			
		},
		
		//Funcion utilizada para mostrar las instituciones en la navegacion por instituciones
		verNavegacionInstituciones : function(){
			this.$('#validacionBusquedaAvanzada').hide();
			this.$('#validacionBusquedaBasica').hide();
			this.$('#accordionFiltros').hide();
			this.$('#contenedorAvanzada').collapse('hide');
			this.$('#contenedorBusqueda').collapse('show');

			this.$('#pagination').empty();
			this.$('#paginationResults').empty();
			this.$('#searchContent').empty();
			this.$('#solo-instituciones-nav').empty();
			this.$('#departamentos-nav').empty();
			this.$('#instituciones-nav').empty();
			this.$('#navegacionContent').hide();
			this.$('#navegacionInstContent').show();
			
			app.collections.allInstitucionesNavegacion.url = '/bdcol/servicios/ListarAllInstitucionesNav.xql';
			app.collections.allInstitucionesNavegacion.fetch();
		},
		
		//Funcion utilizada para mostrar las instituciones segun el departamento que se seleccione en la navegacion por departamentos.
		mostrarInstPorDepto: function(e){
			this.$('#instituciones-nav').empty();
			console.log($(e.currentTarget).data("name"));
			var departamento = $(e.currentTarget).data("name");
			app.collections.institucionesNavegacion.url = '/bdcol/servicios/ListarInstitucionesNav.xql?departamento=' + departamento;
			app.collections.institucionesNavegacion.fetch();
		},
		
		//Funcion utilizada para efectuar la busqueda segun la institucion que se seleccione en cualquiera de las opciones de navegacion
		//Esta funcion utiliza el servicio de busqueda avanzada, por lo tanto utiliza el objeto coleccion de la busqueda avanzada
		busquedaNavegacion: function(e){
			$('#pagination').empty();
			$('#paginationResults').empty();
			$('#searchContent').empty();
			$('#validacionBusquedaAvanzada').hide();
			$('#validacionBusquedaBasica').hide();
			$('#cargando').show();
			
        	app.views.filtrosBusquedaAvanzada.refineData = new Object();
        	app.views.filtrosBusquedaAvanzada.filtrosPreSelected = new Array();
        	app.views.filtrosBusquedaBasica.filtrosPreSelected = new Array();
			app.views.filtrosBusquedaAvanzada.refineDataString = '';
			app.collections.paginatedItemsAvanzada.refineBusqueda = '';
			app.collections.paginatedItemsAvanzada.navegacion = true;
			
			app.collections.paginatedItemsAvanzada.title = '';
			app.collections.paginatedItemsAvanzada.author = '';
			app.collections.paginatedItemsAvanzada.nbc = '';
			app.collections.paginatedItemsAvanzada.tipoRecurso = '';
			app.collections.paginatedItemsAvanzada.tipo = '';
			app.collections.paginatedItemsAvanzada.institution = $(e.currentTarget).data("name");
			app.collections.paginatedItemsAvanzada.repositorio = '';
			app.collections.paginatedItemsAvanzada.fechaPublicacion = '';
			
			app.collections.paginatedItemsAvanzada.setCurrentPage(1);
			
			app.collections.paginatedItemsAvanzada.pager();
			
			$('#accordionFiltros').show();
		},
		
		//Funcion utilizada para efectuar la busqueda segun el tipo seleccionado el div principal donde se pintan los tres tipos de objetos principales. 
		//Esta funcion utiliza el servicio de busqueda avanzada, por lo tanto utiliza el objeto coleccion de la busqueda avanzada
		busquedaTipo: function(e){
			console.log($(e.currentTarget).data("tipo"));
			$('#pagination').empty();
			$('#paginationResults').empty();
			$('#searchContent').empty();
			$('#validacionBusquedaAvanzada').hide();
			$('#validacionBusquedaBasica').hide();
			$('#cargando').show();
			
			app.views.filtrosBusquedaAvanzada.refineData = new Object();
        	app.views.filtrosBusquedaAvanzada.filtrosPreSelected = new Array();
        	app.views.filtrosBusquedaBasica.filtrosPreSelected = new Array();
			app.views.filtrosBusquedaAvanzada.refineDataString = '';
			app.collections.paginatedItemsAvanzada.refineBusqueda = '';
			app.collections.paginatedItemsAvanzada.navegacion = false;
			
			app.collections.paginatedItemsAvanzada.title = '';
			app.collections.paginatedItemsAvanzada.author = '';
			app.collections.paginatedItemsAvanzada.nbc = '';
			app.collections.paginatedItemsAvanzada.tipoRecurso = '';
			app.collections.paginatedItemsAvanzada.tipo = $(e.currentTarget).data("tipo");
			app.collections.paginatedItemsAvanzada.institution = '';
			app.collections.paginatedItemsAvanzada.repositorio = '';
			app.collections.paginatedItemsAvanzada.fechaPublicacion = '';
			
			app.collections.paginatedItemsAvanzada.setCurrentPage(1);
			
			app.collections.paginatedItemsAvanzada.pager();
			
			$('#accordionFiltros').show();
		}
		
	});
	
})( app.views );