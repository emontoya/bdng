( function ( views ){

  views.DepartamentoView = Backbone.View.extend({
    tagName : 'li',
    template: _.template($('#tmpDepartamentoIcon').html()),
    className: 'depIcon',
    attributes: function(){
    	return{
    		'data-name': this.model.get('name')
    	};
    },
    render : function () {
      this.$el.html(this.template(this.model.toJSON()));
      return this;
    }
  });

})( app.views );