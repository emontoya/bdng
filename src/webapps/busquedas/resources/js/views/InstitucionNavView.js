( function ( views ){

  views.InstitucionNavView = Backbone.View.extend({
    tagName : 'li',
    template: _.template($('#tmpInstitucionIcon').html()),
    className: 'instIcon',
    attributes: function(){
    	return{
    		'data-name': this.model.get('name')
    	};
    },
    render : function () {
      this.$el.html(this.template(this.model.toJSON()));
      return this;
    }
  });

})( app.views );