(function ( views ) {

	views.DepartamentosViewList = Backbone.View.extend({
//		el : '#selectInstitucion',
		
		el : '#departamentos-nav',

		initialize : function () {

			var tags = this.collection;
			
			tags.on('add', this.addOne, this);
			tags.on('all', this.render, this);

		},
		


		addOne : function ( Departamento ) {
			var view = new views.DepartamentoView({model:Departamento});
			$('#departamentos-nav').append(view.render().el);
//			$('#cargando').hide();
			$('.tooltiplink').tooltip();
		},

		render: function(){
		}
	});

})( app.views );
