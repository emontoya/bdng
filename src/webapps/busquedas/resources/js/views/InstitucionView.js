( function ( views ){

  views.InstitucionView = Backbone.View.extend({
    tagName : 'option',
//    template: _.template($('#tmpListarInstituciones').html()),
    attributes: function(){
    	return{
    		'value': this.model.get('name')
    	};
    },
    render : function () {
      this.$el.html(this.template(this.model.toJSON()));
      return this;
    }
  });

})( app.views );