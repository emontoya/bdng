(function (views) {

  views.PaginatedResults = Backbone.View.extend({

    events: {
      'click .serverhowmany a': 'changeCount'
    },

    //tagName: 'aside',
	el : '#paginationResults',

    template: _.template($('#tmpServerResults').html()),

    initialize: function () {

      //this.collection.on('reset', this.render, this);
      this.collection.on('sync', this.render, this);
      //console.log("entro");

      //this.$el.appendTo('#pagination');

    },

    render: function () {
    	var valueBusqueda = this.collection.valueBusquedaBasica;
    	
    	var html = this.template({valorTitle : valueBusqueda, totalRecords : this.collection.totalRecords});
    	this.$el.html(html);
    	$('.tooltiplink').tooltip();
    	//console.log(this.collection);
    	//console.log(this.collection.info());
    },
    
    getTotalRecords: function () {
    	return this.collection.info();
    },

    changeCount: function (e) {
      e.preventDefault();
      var per = $(e.target).text();
      $('#searchContent').empty();
      this.collection.howManyPer(per);
    }

  });

})( app.views );
