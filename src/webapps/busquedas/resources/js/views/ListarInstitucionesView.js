(function ( views ) {

	views.InstitucionViewList = Backbone.View.extend({
//		el : '#selectInstitucion',
		
		navegacion : '',

		initialize : function () {

			var tags = this.collection;
			
			tags.on('add', this.addOne, this);
			tags.on('all', this.render, this);

		},
		


		addOne : function ( Institucion ) {
			var view = new views.InstitucionView({model:Institucion});
			if(this.navegacion){
				view.template = _.template($('#tmpListarInstitucionesNavegacion').html());
				$('#selectInstitucionNav').append(view.render().el);
			}else{
				view.template = _.template($('#tmpListarInstituciones').html());
				$('#selectInstitucion').append(view.render().el);
			}
		},

		render: function(){
		}
	});

})( app.views );
