//(function ( views ) {
//
//	views.AppView = Backbone.View.extend({
//
//		el : '#searchContent',
//
//		initialize : function () {
//
//			var tags = this.collection;
//			
//			//console.log(tags);
//
//			tags.on('add', this.addOne, this);
//			tags.on('all', this.render, this);
//			//console.log("entro");
//			
//			/*tags.fetch({
//				success: function(){
//					tags.pager();
//				},
//				silent:true
//			});
//*/
//			tags.pager();
//
//		},
//		addAll : function () {
//			this.$el.empty();
//			this.collection.each (this.addOne);
//			console.log("entro a addAll");
//		},
//		
//		addOne : function ( record ) {
//			var view = new views.ResultView({model:record});
//			$('#searchContent').append(view.render().el);
//		},
//
//		render: function(){
//			//return this;
//			console.log("entro a render AppView");
//		}
//	});
//
//})( app.views );

(function ( views ) {

	views.AppView = Backbone.View.extend({
		el : '#searchContent',

		initialize : function () {

			var tags = this.collection;
			
			tags.on('add', this.addOne, this);
			tags.on('all', this.render, this);
//
//			tags.pager();

		},
		
		buscar: function() {
			var tags = this.collection;
			
			//tags.on('add', this.addOne, this);
			//tags.on('all', this.render, this);
			tags.pager();
		},

		addOne : function ( record ) {
			var view = new views.ResultView({model:record});
			$('#searchContent').append(view.render().el);
			$('#cargando').hide();
		},

		render: function(){
		}
	});

})( app.views );

