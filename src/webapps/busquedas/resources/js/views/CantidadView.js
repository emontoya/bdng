( function ( views ){
	
	views.CantidadGeneral = Backbone.View.extend({
		
		template: _.template($('#tmpMostrarCantidades').html()),
		
		initialize: function () {

	      this.model.on('sync', this.render, this);

	    },
	    render: function(){
	    	var html = this.template(this.model.toJSON());
	    	this.$el.html(html);
//	    	console.log(this.$el);
		}
	});
	
})( app.views );