(function ( views ) {

	views.RepositorioViewList = Backbone.View.extend({
		el : '#selectRepositorio',

		initialize : function () {

			var tags = this.collection;
			
			tags.on('add', this.addOne, this);
			tags.on('all', this.render, this);

		},
		


		addOne : function ( Repositorio ) {
			var view = new views.RepositorioView({model:Repositorio});
			$('#selectRepositorio').append(view.render().el);
		},

		render: function(){
		}
	});

})( app.views );
