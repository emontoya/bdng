( function ( views ){

  views.ResultView = Backbone.View.extend({
    tagName : 'li',
    className: function() {
    	if($(window).width() > 1024){
    		return 'span4 result-box';
    	}else{
    		return 'span5 result-box';
    	}
	},
    template: _.template($('#resultItemTemplate').html()),

    initialize: function() {
      this.model.bind('change', this.render, this);
      this.model.bind('destroy', this.remove, this);
    },

    render : function () {
      this.$el.html(this.template(this.model.toJSON()));
      return this;
    },
    
  });

})( app.views );
