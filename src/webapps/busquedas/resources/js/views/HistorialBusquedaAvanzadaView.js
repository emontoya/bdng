(function (views) {

  views.HistorialBusquedaAvanzada = Backbone.View.extend({

	el : '#historialBusqueda',

    template: _.template($('#tmpListarHistorialBusquedaAvanzada').html()),
    
    historialBusquedasAvanzadas : new Array(),

    initialize: function () {

      this.collection.on('sync', this.render, this);

    },

    render: function () {
    	if((this.collection.totalRecords != 0)){
    		var yaEstaEnHistorial = false;
    		
    		var title = this.collection.title;
			var author = this.collection.author;
			var nbc = this.collection.nbc;
			var tipoRecurso = this.collection.tipoRecurso;
			var tipo = this.collection.tipo;
			var institution = this.collection.institution;
			var fechaPublicacion = this.collection.fechaPublicacion;
			var navegacion = this.collection.navegacion;
			
			if(tipo === ""){
				
				if(!navegacion){
					var consultaCompleta = title + author + nbc + tipoRecurso + institution + fechaPublicacion;
					
					for(var i=0; i < this.historialBusquedasAvanzadas.length; i++){
						if(consultaCompleta === this.historialBusquedasAvanzadas[i]){
							yaEstaEnHistorial = true;
							console.log("ya esta en el historial");
						}
					}
					
					if(!yaEstaEnHistorial){
						this.historialBusquedasAvanzadas.push(consultaCompleta);
						
						var consultaTitle = "";
						if(title != ""){
							consultaTitle = consultaTitle + title;
						}
						if(author != ""){
							if(consultaTitle != ""){
								consultaTitle = consultaTitle + ", " + author;
							}else{
								consultaTitle = consultaTitle + author;
							}
						}
						if(nbc != ""){
							if(consultaTitle != ""){
								consultaTitle = consultaTitle + ", " + this.getNucleoBasicoConocimiento(nbc);
							}else{
								consultaTitle = consultaTitle + this.getNucleoBasicoConocimiento(nbc);
							}
						}
						if(tipoRecurso != ""){
							if(consultaTitle != ""){
								consultaTitle = consultaTitle + ", " + tipoRecurso;
							}else{
								consultaTitle = consultaTitle + tipoRecurso;
							}
						}
						if(institution != ""){
							if(consultaTitle != ""){
								consultaTitle = consultaTitle + ", " + institution;
							}else{
								consultaTitle = consultaTitle + institution;
							}
						}
						if(fechaPublicacion != ""){
							if(consultaTitle != ""){
								consultaTitle = consultaTitle + ", " + fechaPublicacion;
							}else{
								consultaTitle = consultaTitle + fechaPublicacion;
							}
						}
						
				    	var html = this.template({valorTitle : consultaTitle, labeltype : 'label-info', esNavegacion : 'false', name : ''});
				    	this.$el.append(html);
				    	
				    	console.log("se inserto un nuevo registro en el historial");
					}
				}else{
					var consultaCompleta = institution + "navegacion";
					
					for(var i=0; i < this.historialBusquedasAvanzadas.length; i++){
						if(consultaCompleta === this.historialBusquedasAvanzadas[i]){
							yaEstaEnHistorial = true;
							console.log("ya esta en el historial");
						}
					}
					
					if(!yaEstaEnHistorial){
						this.historialBusquedasAvanzadas.push(consultaCompleta);
						var consultaTitle = institution;
						var html = this.template({valorTitle : consultaTitle, labeltype : 'label-inverse', esNavegacion : 'true', name : institution});
				    	this.$el.append(html);
				    	
				    	console.log("se inserto un nuevo registro en el historial");
					}
				}
    		}else{
    			var consultaCompleta = tipo;
				
				for(var i=0; i < this.historialBusquedasAvanzadas.length; i++){
					if(consultaCompleta === this.historialBusquedasAvanzadas[i]){
						yaEstaEnHistorial = true;
						console.log("ya esta en el historial");
					}
				}
				
				if(!yaEstaEnHistorial){
					this.historialBusquedasAvanzadas.push(consultaCompleta);
					var consultaTitle = this.getTipoRecurso(tipo);
					var html = this.template({valorTitle : consultaTitle, labeltype : 'label-inverse', esNavegacion : 'false', name : ''});
			    	this.$el.append(html);
			    	
			    	console.log("se inserto un nuevo registro en el historial");
				}
				
    		}
    	}
    },
    
    getNucleoBasicoConocimiento : function(nbc) {
		try {
			switch (nbc) {
			case 'mtnbc1':
				return 'Agronom&iacute;a';
				break;
			case 'mtnbc2':
				return 'Medicina Veterinaria';
				break;
			case 'mtnbc3':
				return 'Zootecnia';
				break;
			case 'mtnbc4':
				return 'Artes Pl&aacute;sticas, Visuales y Afines';
				break;
			case 'mtnbc5':
				return 'Artes Representativas';
				break;
			case 'mtnbc6':
				return 'Diseño';
				break;
			case 'mtnbc7':
				return 'M&uacute;sica';
				break;
			case 'mtnbc8':
				return 'Otros Programas Asociados a Bellas Artes';
				break;
			case 'mtnbc9':
				return 'Publicidad y Afines';
				break;
			case 'mtnbc10':
				return 'Educaci&oacute;n';
				break;
			case 'mtnbc11':
				return 'Bacteriolog&iacute;a';
				break;
			case 'mtnbc12':
				return 'Enfermer&iacute;a';
				break;
			case 'mtnbc13':
				return 'Instrumentaci&oacute;n Quir&uacute;rgica';
				break;
			case 'mtnbc14':
				return 'Medicina';
				break;
			case 'mtnbc15':
				return 'Nutrici&oacute;n y Diet&eacute;tica';
				break;
			case 'mtnbc16':
				return 'Odontolog&iacute;a';
				break;
			case 'mtnbc17':
				return 'Optometr&iacute;a, Otros Programas de Ciencias de la Salud';
				break;
			case 'mtnbc18':
				return 'Salud P&uacute;blica';
				break;
			case 'mtnbc19':
				return 'Terapias';
				break;
			case 'mtnbc20':
				return 'Antropolog&iacute;a, Artes Liberales';
				break;
			case 'mtnbc21':
				return 'Bibliotecolog&iacute;a, Otros de Ciencias Sociales y Humanas';
				break;
			case 'mtnbc22':
				return 'Ciencia Pol&iacute;tica, Relaciones Internacionales';
				break;
			case 'mtnbc23':
				return 'Comunicaci&oacute;n Social, Periodismo y Afines';
				break;
			case 'mtnbc24':
				return 'Deportes, Educaci&oacute;n F&iacute;sica y Recreaci&oacute;n';
				break;
			case 'mtnbc25':
				return 'Derecho y Afines';
				break;
			case 'mtnbc26':
				return 'Filosof&iacute;a, Teolog&iacute;a y Afines';
				break;
			case 'mtnbc27':
				return 'Formaci&oacute;n Relacionada con el Campo Militar o Policial';
				break;
			case 'mtnbc28':
				return 'Geograf&iacute;a, Historia';
				break;
			case 'mtnbc29':
				return 'Lenguas Modernas, Literatura, Lingü&iacute;stica y Afines';
				break;
			case 'mtnbc30':
				return 'Psicolog&iacute;a';
				break;
			case 'mtnbc31':
				return 'Sociolog&iacute;a, Trabajo Social y Afines';
				break;
			case 'mtnbc32':
				return 'Administraci&oacute;n';
				break;
			case 'mtnbc33':
				return 'Contadur&iacute;a P&uacute;blica';
				break;
			case 'mtnbc34':
				return 'Econom&iacute;a';
				break;
			case 'mtnbc35':
				return 'Arquitectura';
				break;
			case 'mtnbc36':
				return 'Ingenier&iacute;a Administrativa y Afines';
				break;
			case 'mtnbc37':
				return 'Ingenier&iacute;a Agr&iacute;cola, Forestal y Afines';
				break;
			case 'mtnbc38':
				return 'Ingenier&iacute;a Agron&oacute;mica, Pecuaria y Afines';
				break;
			case 'mtnbc39':
				return 'Ingenier&iacute;a Agroindustrial, Alimentos y Afines';
				break;
			case 'mtnbc40':
				return 'Ingenier&iacute;a Ambiental, Sanitaria y Afines';
				break;
			case 'mtnbc41':
				return 'Ingenier&iacute;a Biom&eacute;dica y Afines';
				break;
			case 'mtnbc42':
				return 'Ingenier&iacute;a Civil y Afines';
				break;
			case 'mtnbc43':
				return 'Ingenier&iacute;a de Minas, Metalurgia y Afines';
				break;
			case 'mtnbc44':
				return 'Ingenier&iacute;a de Sistemas, Telem&aacute;tica y Afines';
				break;
			case 'mtnbc45':
				return 'Ingenier&iacute;a El&eacute;ctrica y Afines';
				break;
			case 'mtnbc46':
				return 'Ingenier&iacute;a Electr&oacute;nica, Telecomunicaciones y Afines';
				break;
			case 'mtnbc47':
				return 'Ingenier&iacute;a Industrial y Afines';
				break;
			case 'mtnbc48':
				return 'Ingenier&iacute;a Mec&aacute;nica y Afines';
				break;
			case 'mtnbc49':
				return 'Ingenier&iacute;a Qu&iacute;mica y Afines';
				break;
			case 'mtnbc50':
				return 'Otras Ingenier&iacute;as';
				break;
			case 'mtnbc51':
				return 'Biolog&iacute;a, Microbiolog&iacute;a y Afines';
				break;
			case 'mtnbc52':
				return 'F&iacute;sica';
				break;
			case 'mtnbc53':
				return 'Geolog&iacute;a, Otros Programas de Ciencias Naturales';
				break;
			case 'mtnbc54':
				return 'Matem&aacute;ticas, Estad&iacute;stica y Afines';
				break;
			case 'mtnbc55':
				return 'Qu&iacute;mica y Afines';
				break;
			default:
				return 'Campo no definido';
				break;
			}
		} catch (err) {
			return 'Campo no definido';
		}
	},
	
	getTipoRecurso: function(typeR){
		try {
			switch (typeR) {
				case 'CW':
					return 'Curso Virtual';
					break;
				case 'LO':
					return 'Objeto de Aprendizaje';
					break;
				case 'EA':
					return 'Aplicaci&oacute;n Educativa';
					break;
				case 'CA':
					return 'Art&iacute;culo Cient&iacute;fico';
					break;
				case 'SM':
					return 'Material de Estudio';
					break;
				case 'DA':
					return 'Medios';
					break;
				case 'NI':
					return 'Asuntos Nacionales';
					break;
				default:
					return 'Campo no definido';
					break;
			}
		} catch (err) {
			return 'Campo no definido';
		}
	},

  });

})( app.views );
