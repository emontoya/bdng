(function (views) {

  views.HistorialBusqueda = Backbone.View.extend({

	el : '#historialBusqueda',

    template: _.template($('#tmpListarHistorialBusqueda').html()),
    
    historialBusquedasBasicas : new Array(),

    initialize: function () {

      this.collection.on('sync', this.render, this);

    },

    render: function () {
    	if((this.collection.totalRecords != 0)){
    		var yaEstaEnHistorial = false;
    		var valueBusqueda = this.collection.valueBusquedaBasica;
    		
    		for(var i=0; i < this.historialBusquedasBasicas.length; i++){
    			if(valueBusqueda === this.historialBusquedasBasicas[i]){
    				yaEstaEnHistorial = true;
					console.log("ya esta en el historial");
    			}
    		}
    		
    		if(!yaEstaEnHistorial){
    			this.historialBusquedasBasicas.push(valueBusqueda);
		    	var html = this.template(this.collection.info());
		    	this.$el.append(html);
		    	console.log("se inserto un nuevo registro en el historial");
    		}
    	}
    },

  });

})( app.views );
