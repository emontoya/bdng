(function (collections, model) {
	
	collections.InstitucionesNav = Backbone.Collection.extend({
		model: model,
		
//		url: 'http://localhost:8080/exist/rest/db/servicios/ListarInstituciones.xql',
		
//		url:'http://localhost:8081/bdcol/servicios/ListarInstituciones.xql',
		
		parse: function (response) {
			var tags = response.institucion;
			return tags;
		}
	});
	
})( app.collections, app.models.Institucion);