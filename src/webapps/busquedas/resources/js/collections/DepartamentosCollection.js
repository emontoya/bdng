(function (collections, model) {
	
	collections.Departamentos = Backbone.Collection.extend({
		model: model,
		
//		url: 'http://localhost:8080/exist/rest/db/servicios/ListarInstituciones.xql',
		
//		url:'http://localhost:8081/bdcol/servicios/ListarInstituciones.xql',
		
		url:'/bdcol/servicios/ListarDepartamentos.xql',
		
		parse: function (response) {
			var tags = response.departamento;
			console.log(tags);
			return tags;
		}
	});
	
})( app.collections, app.models.Departamento);