(function (collections, model) {
	
	collections.Repositorios = Backbone.Collection.extend({
		model: model,
		
//		url: 'http://localhost:8080/exist/rest/db/servicios/ListarRepositorios.xql',
		
//		url:'http://localhost:8081/bdcol/servicios/ListarRepositorios.xql',
		
		url:'/bdcol/servicios/ListarRepositorios.xql',
		
		parse: function (response) {
			var tags = response.repname;
			return tags;
		}
	});
	
})( app.collections, app.models.Repositorio);