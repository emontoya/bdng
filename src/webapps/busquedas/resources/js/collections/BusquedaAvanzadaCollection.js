(function (collections, model, paginator) {

	collections.RepositoryAvanzada = paginator.requestPager.extend({

		model: model,
		
		tipoBusqueda: 'avanzada',
		
		navegacion: false,
		
		//Campos por los cuales se realizara la busqueda avanzada
		title: '',
		
		author: '',
		
		nbc: '',
		
		tipoRecurso: '',
		
		tipo: '',
		
		institution: '',
		
		repositorio: '',
		
		fechaPublicacion: '',
		
		//Campo en el que se almacena el tiempo de la consulta
		tiempoBusqueda: '',
		
		//Campo en el que se almacena el inicio del conjunto de datos a mostrar, es decir, el principio de la paginacion
		start: '',
		
		//Campo en el que se almacena el fin del conjunto de datos a mostrar, es decir, el fin de la paginacion
		end: '',
		
		//En este campo se almacenan los posibles filtros del conjunto de resultados
		facets: '',
		
		//En este campo se agregan los query de los filtros si la operacion que se esta realizando es de filtrado
		refineBusqueda: '',
		
		terminado: false,

		paginator_core: {
			
			type: 'GET',
			
			dataType: 'json',
			
			url:'/bdcol/servicios/BusquedaAvanzada.xql'
		},
		
		paginator_ui: {
			// the lowest page index your API allows to be accessed
			firstPage: 1,
		
			// which page should the paginator start from 
			// (also, the actual page the paginator is on)
			currentPage: 1,
			
			// how many items per page should be shown
			perPage: 21,
			
			// a default number of total pages to query in case the API or 
			// service you are using does not support providing the total 
			// number of pages for us.
			// 10 as a default in case your service doesn't return the total
			totalPages: 10
		},
		
		//Variable donde se mapean los parametros que se enviaran al servidor en la consulta
		server_api: {
			
			'title': function() { return this.title; },
			
			'author': function() { return this.author; },
			
			'nbc': function() { return this.nbc; },
			
			'tipo': function() { return this.tipo; },
			
			'fechaPublicacion': function() { return this.fechaPublicacion; },
			
			'institution': function() { return this.institution; },
			
			'repositorio': function() { return this.repositorio; },
			
			'start': function() {
				if(this.currentPage === 1){
					return 1;
				}else{
					return ((this.currentPage - 1) * this.perPage) +1;
				}
			},
			
			'perPage': function() { return this.perPage; },
			
			'refineBusqueda': function(){ return this.refineBusqueda; }
		
		},
		
		setCurrentPage: function(current){
			this.currentPage= current;
		},

		parse: function (response) {
			
			var tags = response.record;
			//Normally this.totalPages would equal response.d.__count
			//but as this particular NetFlix request only returns a
			//total count of items for the search, we divide.
			this.totalPages = Math.ceil(response.total / this.perPage);
			console.log(response);
			this.tiempoBusqueda = response.time;
			this.start = response.start;
			this.end = response.end;
			this.facets = response.facets;
			this.terminado = true;

			this.totalRecords = parseInt(response.total);
			return tags;
		}

	});

})( app.collections, app.models.Record, Backbone.Paginator);
