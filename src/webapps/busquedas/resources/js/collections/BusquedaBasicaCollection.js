(function (collections, model, paginator) {

	collections.Repository = paginator.requestPager.extend({

		model: model,
		
		tipoBusqueda: 'basica',
		
		//Campo donde se almacena el texto que se quiere consultar en la base de datos
		valueBusquedaBasica: '',
		
		//Campo en el que se almacena el tiempo de la consulta
		tiempoBusqueda: '',
		
		//Campo en el que se almacena el inicio del conjunto de datos a mostrar, es decir, el principio de la paginacion
		start: '',
		
		//Campo en el que se almacena el fin del conjunto de datos a mostrar, es decir, el fin de la paginacion
		end: '',
		
		//En este campo se almacenan los posibles filtros del conjunto de resultados
		facets: '',
		
		//En este campo se agregan los query de los filtros si la operacion que se esta realizando es de filtrado
		refineBusqueda: '',

		paginator_core: {
			
			type: 'POST',
			
			dataType: 'json',
			
			url:'/bdcol/servicios/BusquedaBasica.xql'
			
		},
		
		paginator_ui: {
			// the lowest page index your API allows to be accessed
			firstPage: 1,
		
			// which page should the paginator start from 
			// (also, the actual page the paginator is on)
			currentPage: 1,
			
			// how many items per page should be shown
			perPage: 21,
			
			// a default number of total pages to query in case the API or 
			// service you are using does not support providing the total 
			// number of pages for us.
			// 10 as a default in case your service doesn't return the total
			totalPages: 10
		},
		
		server_api: {
			
			'query': function() { return this.valueBusquedaBasica; },
			
			'start': function() {
				if(this.currentPage === 1){
					return 1;
				}else{
					return ((this.currentPage - 1) * this.perPage) +1;
				}
			},
			
			'perPage': function() { return this.perPage; },
			
			'refineBusqueda': function(){ return this.refineBusqueda; }
			 
		},
		
		setCurrentPage: function(current){
			this.currentPage= current;
		},

		parse: function (response) {
			
			var tags = response.record;
			//Normally this.totalPages would equal response.d.__count
			//but as this particular NetFlix request only returns a
			//total count of items for the search, we divide.
			this.totalPages = Math.ceil(response.total / this.perPage);
			console.log(response);
			this.tiempoBusqueda = response.time;
			this.start = response.start;
			this.end = response.end;
			this.facets = response.facets;
			this.totalRecords = parseInt(response.total);
			
			return tags;
		}

	});

})( app.collections, app.models.Record, Backbone.Paginator);
