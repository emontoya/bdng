(function ( models ) {
	models.Departamento = Backbone.Model.extend({
		
		getImgDepartamento : function() {
			try{
				return "resources/img/departamentos/" + (this.omitirAcentos(this.get("name"))).toLowerCase() + ".png";
			}catch(err){
				return "campo no definido";
			}
		},
		
		getDepartamento : function() {
			try{
				return this.get("name");
			}catch(err){
				return "campo no definido";
			}
		},
		
		omitirAcentos: function(text) {
			var acentos = "ÃÀÁÄÂÈÉËÊÌÍÏÎÒÓÖÔÙÚÜÛãàáäâèéëêìíïîòóöôùúüûÇç";
			var original = "AAAAAEEEEIIIIOOOOUUUUaaaaaeeeeiiiioooouuuucc";
			for (var i=0; i<acentos.length; i++) {
				text = text.replace(acentos.charAt(i), original.charAt(i));
			}
			return text;
		}
		
	});
})( app.models );