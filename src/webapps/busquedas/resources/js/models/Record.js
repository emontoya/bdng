// js/models/record.js

(function(models) {
	models.Record = Backbone.Model
			.extend({
				imgTipoRecurso : function() {
					try {
						// console.log(this.get("doer").educational.resourceType.main);
						// if((this.get("doer").educational.resourceType.main)
						// == 'DA'){
						// console.log("El recurso es Digital Asset");
						// }
						switch (this.get("doer").educational.resourceType.main) {
						case 'CW':
							return 'tprcv';
							break;
						case 'LO':
							return 'tproa';
							break;
						case 'EA':
							return 'tprae';
							break;
						default:
							return 'tprcv';
							break;
						}
					} catch (err) {
						return 'tprcv';
					}
				},
				
				getTipoRecurso: function(){
					try {
						switch (this.get("doer").educational.resourceType.main) {
							case 'CW':
								return 'Curso Virtual';
								break;
							case 'LO':
								return 'Objeto de Aprendizaje';
								break;
							case 'EA':
								return 'Aplicaci&oacute;n Educativa';
								break;
							case 'CA':
								return 'Art&iacute;culo Cient&iacute;fico';
								break;
							case 'SM':
								return 'Material de Estudio';
								break;
							case 'DA':
								return 'Medios';
								break;
							case 'NI':
								return 'Asuntos Nacionales';
								break;
							default:
								return 'Campo no definido';
								break;
						}
					} catch (err) {
						return 'Campo no definido';
					}
				},

				colorAreaConocimientoMeta : function() {
					try {
						switch (this.get("doer").educational.eduProperties.specificContext.hsKnowAreas) {
						case 'mtakn1':
							return 'tm_avva';
							break;
						case 'mtakn2':
							return 'tm_bbaa';
							break;
						case 'mtakn3':
							return 'tm_ccee';
							break;
						case 'mtakn4':
							return 'tm_ccss';
							break;
						case 'mtakn5':
							return 'tm_cchh';
							break;
						case 'mtakn6':
							return 'tm_eaca';
							break;
						case 'mtakn7':
							return 'tm_iaua';
							break;
						case 'mtakn8':
							return 'tm_mycn';
							break;
						default:
							return 'tm_iaua';
							break;
						}
					} catch (err) {
						return 'tm_iaua';
					}

				},

				colorAreaConocimientoIco : function() {
					try {
						return this.get("doer").educational.eduProperties.specificContext.hsKnowAreas;
					} catch (err) {
						return 'mtakn7';
					}
				},

				colorNucleoBasicoConocimientoIco : function() {
					try {
						return this.get("doer").educational.eduProperties.specificContext.hsBasicCore;
					} catch (err) {
						return 'mtnbc38';
					}
				},

				iconoAreaConocimiento : function() {
					try {
						switch (this.get("doer").educational.eduProperties.specificContext.hsKnowAreas) {
						case 'mtakn1':
							return 'ico1_2_0';
							break;
						case 'mtakn2':
							return 'ico1_2_1';
							break;
						case 'mtakn3':
							return 'ico1_2_2';
							break;
						case 'mtakn4':
							return 'ico1_2_3';
							break;
						case 'mtakn5':
							return 'ico1_2_4';
							break;
						case 'mtakn6':
							return 'ico1_2_5';
							break;
						case 'mtakn7':
							return 'ico1_2_6';
							break;
						case 'mtakn8':
							return 'ico1_2_7';
							break;
						default:
							return 'ico1_2_6';
							break;
						}
					} catch (err) {
						return 'ico1_2_6';
					}
				},

				iconoNucleoBasicoConocimiento : function() {
					try {
						switch (this.get("doer").educational.eduProperties.specificContext.hsBasicCore) {
						case 'mtnbc1':
							return 'ico1_2_0_0';
							break;
						case 'mtnbc2':
							return 'ico1_2_0_1';
							break;
						case 'mtnbc3':
							return 'ico1_2_0_2';
							break;
						case 'mtnbc4':
							return 'ico1_2_1_0';
							break;
						case 'mtnbc5':
							return 'ico1_2_1_1';
							break;
						case 'mtnbc6':
							return 'ico1_2_1_2';
							break;
						case 'mtnbc7':
							return 'ico1_2_1_3';
							break;
						case 'mtnbc8':
							return 'ico1_2_1_4';
							break;
						case 'mtnbc9':
							return 'ico1_2_1_5';
							break;
						case 'mtnbc10':
							return 'ico1_2_2_0';
							break;
						case 'mtnbc11':
							return 'ico1_2_3_0';
							break;
						case 'mtnbc12':
							return 'ico1_2_3_1';
							break;
						case 'mtnbc13':
							return 'ico1_2_3_2';
							break;
						case 'mtnbc14':
							return 'ico1_2_3_3';
							break;
						case 'mtnbc15':
							return 'ico1_2_3_4';
							break;
						case 'mtnbc16':
							return 'ico1_2_3_5';
							break;
						case 'mtnbc17':
							return 'ico1_2_3_6';
							break;
						case 'mtnbc18':
							return 'ico1_2_3_7';
							break;
						case 'mtnbc19':
							return 'ico1_2_3_8';
							break;
						case 'mtnbc20':
							return 'ico1_2_4_0';
							break;
						case 'mtnbc21':
							return 'ico1_2_4_1';
							break;
						case 'mtnbc22':
							return 'ico1_2_4_2';
							break;
						case 'mtnbc23':
							return 'ico1_2_4_3';
							break;
						case 'mtnbc24':
							return 'ico1_2_4_4';
							break;
						case 'mtnbc25':
							return 'ico1_2_4_5';
							break;
						case 'mtnbc26':
							return 'ico1_2_4_6';
							break;
						case 'mtnbc27':
							return 'ico1_2_4_7';
							break;
						case 'mtnbc28':
							return 'ico1_2_4_8';
							break;
						case 'mtnbc29':
							return 'ico1_2_4_9';
							break;
						case 'mtnbc30':
							return 'ico1_2_4_10';
							break;
						case 'mtnbc31':
							return 'ico1_2_4_11';
							break;
						case 'mtnbc32':
							return 'ico1_2_5_0';
							break;
						case 'mtnbc33':
							return 'ico1_2_5_1';
							break;
						case 'mtnbc34':
							return 'ico1_2_5_2';
							break;
						case 'mtnbc35':
							return 'ico1_2_6_0';
							break;
						case 'mtnbc36':
							return 'ico1_2_6_1';
							break;
						case 'mtnbc37':
							return 'ico1_2_6_2';
							break;
						case 'mtnbc38':
							return 'ico1_2_6_3';
							break;
						case 'mtnbc39':
							return 'ico1_2_6_4';
							break;
						case 'mtnbc40':
							return 'ico1_2_6_5';
							break;
						case 'mtnbc41':
							return 'ico1_2_6_6';
							break;
						case 'mtnbc42':
							return 'ico1_2_6_7';
							break;
						case 'mtnbc43':
							return 'ico1_2_6_8';
							break;
						case 'mtnbc44':
							return 'ico1_2_6_9';
							break;
						case 'mtnbc45':
							return 'ico1_2_6_10';
							break;
						case 'mtnbc46':
							return 'ico1_2_6_11';
							break;
						case 'mtnbc47':
							return 'ico1_2_6_12';
							break;
						case 'mtnbc48':
							return 'ico1_2_6_13';
							break;
						case 'mtnbc49':
							return 'ico1_2_6_14';
							break;
						case 'mtnbc50':
							return 'ico1_2_6_15';
							break;
						case 'mtnbc51':
							return 'ico1_2_7_0';
							break;
						case 'mtnbc52':
							return 'ico1_2_7_1';
							break;
						case 'mtnbc53':
							return 'ico1_2_7_2';
							break;
						case 'mtnbc54':
							return 'ico1_2_7_3';
							break;
						case 'mtnbc55':
							return 'ico1_2_7_4';
							break;
						default:
							return 'ico1_2_6_3';
							break;
						}
					} catch (err) {
						return 'ico1_2_6_3';
					}
				},

				iconoSubTipoRecurso : function() {
					try {

					} catch (err) {

					}
				},

				getTitulo : function() {
					try {
						return this.get('doer').general.titlePack.title["#text"];
					} catch (err) {
						return 'Campo no definido';
					}
				},
				
				getTituloVistaDetallada : function() {
					try {
						var tituloArray = (this.get('doer').general.titlePack.title["#text"]).split(":");
						var titulo = "";
						if(tituloArray.length > 1){
							titulo = "<h3>" + tituloArray[0] + ":</h3>" + tituloArray[1];
						}else{
							titulo = "<h3>" + tituloArray[0] + "</h3>";
						}
						return titulo;
					} catch (err) {
						return 'Campo no definido';
					}
				},

				getDescripcion : function() {
					try {
						return this.get('doer').general.titlePack.description["#text"];
					} catch (err) {
						return 'Campo no definido';
					}
				},

				getAreaConocimiento : function() {
					try {
						switch (this.get("doer").educational.eduProperties.specificContext.hsKnowAreas) {
						case 'mtakn1':
							return 'Agronom&iacute;a, Veterinaria y Afines';
							break;
						case 'mtakn2':
							return 'Bellas Artes';
							break;
						case 'mtakn3':
							return 'Ciencias de la Educaci&oacute;n';
							break;
						case 'mtakn4':
							return 'Ciencias de la Salud';
							break;
						case 'mtakn5':
							return 'Ciencias Sociales y Humanas';
							break;
						case 'mtakn6':
							return 'Econom&iacute;a, Contabilidad, Administraci&oacute;n y Afines';
							break;
						case 'mtakn7':
							return 'Ingenier&iacute;a, Arquitectura, Urbanismo y Afines';
							break;
						case 'mtakn8':
							return 'Matem&aacute;ticas y Ciencias Naturales';
							break;
						default:
							return 'Campo no definido';
							break;
						}
					} catch (err) {
						return 'Campo no definido';
					}
				},

				getNucleoBasicoConocimiento : function() {
					try {
						switch (this.get("doer").educational.eduProperties.specificContext.hsBasicCore) {
						case 'mtnbc1':
							return 'Agronom&iacute;a';
							break;
						case 'mtnbc2':
							return 'Medicina Veterinaria';
							break;
						case 'mtnbc3':
							return 'Zootecnia';
							break;
						case 'mtnbc4':
							return 'Artes Pl&aacute;sticas, Visuales y Afines';
							break;
						case 'mtnbc5':
							return 'Artes Representativas';
							break;
						case 'mtnbc6':
							return 'Diseño';
							break;
						case 'mtnbc7':
							return 'M&uacute;sica';
							break;
						case 'mtnbc8':
							return 'Otros Programas Asociados a Bellas Artes';
							break;
						case 'mtnbc9':
							return 'Publicidad y Afines';
							break;
						case 'mtnbc10':
							return 'Educaci&oacute;n';
							break;
						case 'mtnbc11':
							return 'Bacteriolog&iacute;a';
							break;
						case 'mtnbc12':
							return 'Enfermer&iacute;a';
							break;
						case 'mtnbc13':
							return 'Instrumentaci&oacute;n Quir&uacute;rgica';
							break;
						case 'mtnbc14':
							return 'Medicina';
							break;
						case 'mtnbc15':
							return 'Nutrici&oacute;n y Diet&eacute;tica';
							break;
						case 'mtnbc16':
							return 'Odontolog&iacute;a';
							break;
						case 'mtnbc17':
							return 'Optometr&iacute;a, Otros Programas de Ciencias de la Salud';
							break;
						case 'mtnbc18':
							return 'Salud P&uacute;blica';
							break;
						case 'mtnbc19':
							return 'Terapias';
							break;
						case 'mtnbc20':
							return 'Antropolog&iacute;a, Artes Liberales';
							break;
						case 'mtnbc21':
							return 'Bibliotecolog&iacute;a, Otros de Ciencias Sociales y Humanas';
							break;
						case 'mtnbc22':
							return 'Ciencia Pol&iacute;tica, Relaciones Internacionales';
							break;
						case 'mtnbc23':
							return 'Comunicaci&oacute;n Social, Periodismo y Afines';
							break;
						case 'mtnbc24':
							return 'Deportes, Educaci&oacute;n F&iacute;sica y Recreaci&oacute;n';
							break;
						case 'mtnbc25':
							return 'Derecho y Afines';
							break;
						case 'mtnbc26':
							return 'Filosof&iacute;a, Teolog&iacute;a y Afines';
							break;
						case 'mtnbc27':
							return 'Formaci&oacute;n Relacionada con el Campo Militar o Policial';
							break;
						case 'mtnbc28':
							return 'Geograf&iacute;a, Historia';
							break;
						case 'mtnbc29':
							return 'Lenguas Modernas, Literatura, Lingü&iacute;stica y Afines';
							break;
						case 'mtnbc30':
							return 'Psicolog&iacute;a';
							break;
						case 'mtnbc31':
							return 'Sociolog&iacute;a, Trabajo Social y Afines';
							break;
						case 'mtnbc32':
							return 'Administraci&oacute;n';
							break;
						case 'mtnbc33':
							return 'Contadur&iacute;a P&uacute;blica';
							break;
						case 'mtnbc34':
							return 'Econom&iacute;a';
							break;
						case 'mtnbc35':
							return 'Arquitectura';
							break;
						case 'mtnbc36':
							return 'Ingenier&iacute;a Administrativa y Afines';
							break;
						case 'mtnbc37':
							return 'Ingenier&iacute;a Agr&iacute;cola, Forestal y Afines';
							break;
						case 'mtnbc38':
							return 'Ingenier&iacute;a Agron&oacute;mica, Pecuaria y Afines';
							break;
						case 'mtnbc39':
							return 'Ingenier&iacute;a Agroindustrial, Alimentos y Afines';
							break;
						case 'mtnbc40':
							return 'Ingenier&iacute;a Ambiental, Sanitaria y Afines';
							break;
						case 'mtnbc41':
							return 'Ingenier&iacute;a Biom&eacute;dica y Afines';
							break;
						case 'mtnbc42':
							return 'Ingenier&iacute;a Civil y Afines';
							break;
						case 'mtnbc43':
							return 'Ingenier&iacute;a de Minas, Metalurgia y Afines';
							break;
						case 'mtnbc44':
							return 'Ingenier&iacute;a de Sistemas, Telem&aacute;tica y Afines';
							break;
						case 'mtnbc45':
							return 'Ingenier&iacute;a El&eacute;ctrica y Afines';
							break;
						case 'mtnbc46':
							return 'Ingenier&iacute;a Electr&oacute;nica, Telecomunicaciones y Afines';
							break;
						case 'mtnbc47':
							return 'Ingenier&iacute;a Industrial y Afines';
							break;
						case 'mtnbc48':
							return 'Ingenier&iacute;a Mec&aacute;nica y Afines';
							break;
						case 'mtnbc49':
							return 'Ingenier&iacute;a Qu&iacute;mica y Afines';
							break;
						case 'mtnbc50':
							return 'Otras Ingenier&iacute;as';
							break;
						case 'mtnbc51':
							return 'Biolog&iacute;a, Microbiolog&iacute;a y Afines';
							break;
						case 'mtnbc52':
							return 'F&iacute;sica';
							break;
						case 'mtnbc53':
							return 'Geolog&iacute;a, Otros Programas de Ciencias Naturales';
							break;
						case 'mtnbc54':
							return 'Matem&aacute;ticas, Estad&iacute;stica y Afines';
							break;
						case 'mtnbc55':
							return 'Qu&iacute;mica y Afines';
							break;
						default:
							return 'Campo no definido';
							break;
						}
					} catch (err) {
						return 'Campo no definido';
					}
				},

				getNucleoBasicoConocimientoCorto : function() {
					try {
						switch (this.get("doer").educational.eduProperties.specificContext.hsBasicCore) {
						case 'mtnbc1':
							return 'Agronom&iacute;a';
							break;
						case 'mtnbc2':
							return 'Medicina Veterinaria';
							break;
						case 'mtnbc3':
							return 'Zootecnia';
							break;
						case 'mtnbc4':
							return 'Artes Pl&aacute;sticas, Visuales y Afines';
							break;
						case 'mtnbc5':
							return 'Artes Representativas';
							break;
						case 'mtnbc6':
							return 'Diseño';
							break;
						case 'mtnbc7':
							return 'M&uacute;sica';
							break;
						case 'mtnbc8':
							return 'Otros Asociados a Bellas Artes';
							break;
						case 'mtnbc9':
							return 'Publicidad y Afines';
							break;
						case 'mtnbc10':
							return 'Educaci&oacute;n';
							break;
						case 'mtnbc11':
							return 'Bacteriolog&iacute;a';
							break;
						case 'mtnbc12':
							return 'Enfermer&iacute;a';
							break;
						case 'mtnbc13':
							return 'Instrumentaci&oacute;n Quir&uacute;rgica';
							break;
						case 'mtnbc14':
							return 'Medicina';
							break;
						case 'mtnbc15':
							return 'Nutrici&oacute;n y Diet&eacute;tica';
							break;
						case 'mtnbc16':
							return 'Odontolog&iacute;a';
							break;
						case 'mtnbc17':
							return 'Optometr&iacute;a, Otros C. de la Salud';
							break;
						case 'mtnbc18':
							return 'Salud P&uacute;blica';
							break;
						case 'mtnbc19':
							return 'Terapias';
							break;
						case 'mtnbc20':
							return 'Antropolog&iacute;a, Artes Liberales';
							break;
						case 'mtnbc21':
							return 'Bibliotecolog&iacute;a, Otros C. Sociales';
							break;
						case 'mtnbc22':
							return 'Ciencia Pol&iacute;tica, Relaciones Intern.';
							break;
						case 'mtnbc23':
							return 'Com. Social, Periodismo y Afines';
							break;
						case 'mtnbc24':
							return 'Deportes, Ed. F&iacute;sica y Recreaci&oacute;n';
							break;
						case 'mtnbc25':
							return 'Derecho y Afines';
							break;
						case 'mtnbc26':
							return 'Filosof&iacute;a, Teolog&iacute;a y Afines';
							break;
						case 'mtnbc27':
							return 'Formaci&oacute;n Campo Militar o Policial';
							break;
						case 'mtnbc28':
							return 'Geograf&iacute;a, Historia';
							break;
						case 'mtnbc29':
							return 'Lenguas Modernas, Lit. y Afines';
							break;
						case 'mtnbc30':
							return 'Psicolog&iacute;a';
							break;
						case 'mtnbc31':
							return 'Sociolog&iacute;a, Trabajo Social y Afines';
							break;
						case 'mtnbc32':
							return 'Administraci&oacute;n';
							break;
						case 'mtnbc33':
							return 'Contadur&iacute;a P&uacute;blica';
							break;
						case 'mtnbc34':
							return 'Econom&iacute;a';
							break;
						case 'mtnbc35':
							return 'Arquitectura';
							break;
						case 'mtnbc36':
							return 'Ingenier&iacute;a Administrativa y Afines';
							break;
						case 'mtnbc37':
							return 'Ing. Agr&iacute;cola, Forestal y Afines';
							break;
						case 'mtnbc38':
							return 'Ing. Agron&oacute;mica, Pecuaria y Afines';
							break;
						case 'mtnbc39':
							return 'Ing. Agroind., Alimentos y Afines';
							break;
						case 'mtnbc40':
							return 'Ing. Ambiental, Sanitaria y Afines';
							break;
						case 'mtnbc41':
							return 'Ingenier&iacute;a Biom&eacute;dica y Afines';
							break;
						case 'mtnbc42':
							return 'Ingenier&iacute;a Civil y Afines';
							break;
						case 'mtnbc43':
							return 'Ing. de Minas, Metalurgia y Afines';
							break;
						case 'mtnbc44':
							return 'Ing. Sistemas, Telem&aacute;tica y Afines';
							break;
						case 'mtnbc45':
							return 'Ingenier&iacute;a El&eacute;ctrica y Afines';
							break;
						case 'mtnbc46':
							return 'Ing. Electr&oacute;nica, Telecom. y Afines';
							break;
						case 'mtnbc47':
							return 'Ingenier&iacute;a Industrial y Afines';
							break;
						case 'mtnbc48':
							return 'Ingenier&iacute;a Mec&aacute;nica y Afines';
							break;
						case 'mtnbc49':
							return 'Ingenier&iacute;a Qu&iacute;mica y Afines';
							break;
						case 'mtnbc50':
							return 'Otras Ingenier&iacute;as';
							break;
						case 'mtnbc51':
							return 'Biolog&iacute;a, Microbiolog&iacute;a y Afines';
							break;
						case 'mtnbc52':
							return 'F&iacute;sica';
							break;
						case 'mtnbc53':
							return 'Geolog&iacute;a, Otros de C. Naturales';
							break;
						case 'mtnbc54':
							return 'Matem&aacute;ticas, Estad&iacute;stica y Afines';
							break;
						case 'mtnbc55':
							return 'Qu&iacute;mica y Afines';
							break;
						}
					} catch (err) {

					}
				},

				getImgThumbnail : function() {
					try {
						if((this.get("doer").technical.thumbnails.image.length) > 1){
							for ( var i = 0; i < (this.get("doer").technical.thumbnails.image.length); i++) {
								if (this.get("doer").technical.thumbnails.image[i].size == 'TS') {
									 return (this.get("doer").technical.thumbnails.src + this.get("doer").technical.thumbnails.image[i].fileName);
//									return 'resources/img/repo/3wwF4.jpg';
								}
							}
						}else{
							if (this.get("doer").technical.thumbnails.image.size == 'TS') {
								 return (this.get("doer").technical.thumbnails.src + this.get("doer").technical.thumbnails.image.fileName);
//								return 'resources/img/repo/3wwF4.jpg';
							}else{
								return 'resources/img/repo/3wwF4.jpg';
							}
						}
						// return
						// this.get("doer").technical.thumbnails.image.length;
					} catch (err) {
						return 'resources/img/repo/3wwF4.jpg';
					}
				},

				getImgInstitucion : function() {
					try {
						if ((this.get("doer").rights.copyrightStackHolder.entity.length) > 1) {
							for ( var i = 0; i < (this.get("doer").rights.copyrightStackHolder.entity.length); i++) {
								if (this.get("doer").rights.copyrightStackHolder.entity[i].type == 'ORG') {
									// return
									// "resources/img/institutions/" + this.get("doer").rights.copyrightStackHolder.entity[i].entityForm;
									return 'resources/img/institutions/default.png';
								}
							}
						} else {
							// return
							// "resources/img/institutions/" + this.get("doer").rights.copyrightStackHolder.entity.entityForm;
							return 'resources/img/institutions/default.png';
						}
					} catch (err) {
						return 'resources/img/institutions/default.png';
					}
				},

				getInstitucion : function() {
					try {
						if ((this.get("doer").rights.copyrightStackHolder.entity.length) > 1) {
							for ( var i = 0; i < (this.get("doer").rights.copyrightStackHolder.entity.length); i++) {
								if (this.get("doer").rights.copyrightStackHolder.entity[i].type == 'ORG') {
									return this.get("doer").rights.copyrightStackHolder.entity[i]["#text"];
								}
							}
						} else {
							return this.get("doer").rights.copyrightStackHolder.entity["#text"];
						}
					} catch (err) {
						return 'Campo no definido';
					}
				},

//				getAutoresCompletos : function() {
//					try {
//						var autores = '';
//						if ((this.get("doer").lifeCycle.controlVersion.length) > 1) {
//							for ( var i = 0; i < (this.get("doer").lifeCycle.controlVersion.length); i++) {
//								if ((this.get("doer").lifeCycle.controlVersion[i].contribute.length) > 1) {
//									for ( var j = 0; j < (this.get("doer").lifeCycle.controlVersion[i].contribute.length); j++) {
//										if ((this.get("doer").lifeCycle.controlVersion[i].contribute[j].entity.length) > 1) {
//											for ( var k = 0; k < (this
//													.get("doer").lifeCycle.controlVersion[i].contribute[j].entity.length); k++) {
//												if (k != ((this.get("doer").lifeCycle.controlVersion[i].contribute[j].entity.length) - 1)) {
//													autores += (this
//															.get("doer").lifeCycle.controlVersion[i].contribute[j].entity[k]["#text"])
//															+ '; ';
//												} else {
//													autores += (this
//															.get("doer").lifeCycle.controlVersion[i].contribute[j].entity[k]["#text"]);
//												}
//											}
//										} else {
//											autores += (this.get("doer").lifeCycle.controlVersion[i].contribute[j].entity["#text"]);
//										}
//									}
//								} else {
//									if ((this.get("doer").lifeCycle.controlVersion[i].contribute.entity.length) > 1) {
//										for ( var k = 0; k < (this.get("doer").lifeCycle.controlVersion[i].contribute.entity.length); k++) {
//											if (k != ((this.get("doer").lifeCycle.controlVersion[i].contribute.entity.length) - 1)) {
//												autores += (this.get("doer").lifeCycle.controlVersion[i].contribute.entity[k]["#text"])
//														+ '; ';
//											} else {
//												autores += (this.get("doer").lifeCycle.controlVersion[i].contribute.entity[k]["#text"]);
//											}
//										}
//									} else {
//										autores += (this.get("doer").lifeCycle.controlVersion[i].contribute.entity["#text"]);
//									}
//								}
//							}
//						} else {
//							if ((this.get("doer").lifeCycle.controlVersion.contribute.length) > 1) {
//								for ( var j = 0; j < (this.get("doer").lifeCycle.controlVersion.contribute.length); j++) {
//									if ((this.get("doer").lifeCycle.controlVersion.contribute[j].entity.length) > 1) {
//										for ( var k = 0; k < (this.get("doer").lifeCycle.controlVersion.contribute[j].entity.length); k++) {
//											if (k != ((this.get("doer").lifeCycle.controlVersion.contribute[j].entity.length) - 1)) {
//												autores += (this.get("doer").lifeCycle.controlVersion.contribute[j].entity[k]["#text"])
//														+ '; ';
//											} else {
//												autores += (this.get("doer").lifeCycle.controlVersion.contribute[j].entity[k]["#text"]);
//											}
//										}
//									} else {
//										autores += (this.get("doer").lifeCycle.controlVersion.contribute[j].entity["#text"]);
//									}
//								}
//							} else {
//								if ((this.get("doer").lifeCycle.controlVersion.contribute.entity.length) > 1) {
//									for ( var k = 0; k < (this.get("doer").lifeCycle.controlVersion.contribute.entity.length); k++) {
//										if (k != ((this.get("doer").lifeCycle.controlVersion.contribute.entity.length) - 1)) {
//											autores += (this.get("doer").lifeCycle.controlVersion.contribute.entity[k]["#text"])
//													+ '; ';
//										} else {
//											autores += (this.get("doer").lifeCycle.controlVersion.contribute.entity[k]["#text"]);
//										}
//									}
//								} else {
//									autores += (this.get("doer").lifeCycle.controlVersion.contribute.entity["#text"]);
//								}
//							}
//						}
//						return autores;
//					} catch (err) {
//						return 'Campo no definido';
//					}
//				},
				
				getAutoresCompletos : function() {
					try{
						var autores = '';
						if ((this.get("doer").lifeCycle.controlVersion.length) > 1) {
							if ((this.get("doer").lifeCycle.controlVersion[0].contribute.length) > 1) {
								for ( var j = 0; j < (this.get("doer").lifeCycle.controlVersion[0].contribute.length); j++) {
									if ((this.get("doer").lifeCycle.controlVersion[0].contribute[j].role) == 'AU') {
										if ((this.get("doer").lifeCycle.controlVersion[0].contribute[j].entity.length) > 1) {
											for ( var k = 0; k < (this.get("doer").lifeCycle.controlVersion[0].contribute[j].entity.length); k++) {
												if (k != ((this.get("doer").lifeCycle.controlVersion[0].contribute[j].entity.length) - 1)) {
													autores += (this.get("doer").lifeCycle.controlVersion[0].contribute[j].entity[k]["#text"]) + '; ';
												} else {
													autores += (this.get("doer").lifeCycle.controlVersion[0].contribute[j].entity[k]["#text"]);
												}
											}
										}else{
											autores = (this.get("doer").lifeCycle.controlVersion[0].contribute[j].entity["#text"]);
										}
									}
								}
							}else{
								if ((this.get("doer").lifeCycle.controlVersion[0].contribute.role) == 'AU') {
									if ((this.get("doer").lifeCycle.controlVersion[0].contribute.entity.length) > 1) {
										for ( var k = 0; k < (this.get("doer").lifeCycle.controlVersion[0].contribute.entity.length); k++) {
											if (k != ((this.get("doer").lifeCycle.controlVersion[0].contribute.entity.length) - 1)) {
												autores += (this.get("doer").lifeCycle.controlVersion[0].contribute.entity[k]["#text"]) + '; ';
											} else {
												autores += (this.get("doer").lifeCycle.controlVersion[0].contribute.entity[k]["#text"]);
											}
										}
									}else{
										autores = (this.get("doer").lifeCycle.controlVersion[0].contribute.entity["#text"]);
									}
								}
							}
						}else{
							if ((this.get("doer").lifeCycle.controlVersion.contribute.length) > 1) {
								for ( var j = 0; j < (this.get("doer").lifeCycle.controlVersion.contribute.length); j++) {
									if ((this.get("doer").lifeCycle.controlVersion.contribute[j].role) == 'AU') {
										if ((this.get("doer").lifeCycle.controlVersion.contribute[j].entity.length) > 1) {
											for ( var k = 0; k < (this.get("doer").lifeCycle.controlVersion.contribute[j].entity.length); k++) {
												if (k != ((this.get("doer").lifeCycle.controlVersion.contribute[j].entity.length) - 1)) {
													autores += (this.get("doer").lifeCycle.controlVersion.contribute[j].entity[k]["#text"]) + '; ';
												} else {
													autores += (this.get("doer").lifeCycle.controlVersion.contribute[j].entity[k]["#text"]);
												}
											}
										}else{
											autores = (this.get("doer").lifeCycle.controlVersion.contribute[j].entity["#text"]);
										}
									}
								}
							}else{
								if ((this.get("doer").lifeCycle.controlVersion.contribute.role) == 'AU') {
									if ((this.get("doer").lifeCycle.controlVersion.contribute.entity.length) > 1) {
										for ( var k = 0; k < (this.get("doer").lifeCycle.controlVersion.contribute.entity.length); k++) {
											if (k != ((this.get("doer").lifeCycle.controlVersion.contribute.entity.length) - 1)) {
												autores += (this.get("doer").lifeCycle.controlVersion.contribute.entity[k]["#text"]) + '; ';
											} else {
												autores += (this.get("doer").lifeCycle.controlVersion.contribute.entity[k]["#text"]);
											}
										}
									}else{
										autores = (this.get("doer").lifeCycle.controlVersion.contribute.entity["#text"]);
									}
								}
							}
						}
						return autores;
					}catch(err){
						return 'Campo no definido';
					}
				},
				
				getAutores : function() {
					try{
						var autores = '';
						if ((this.get("doer").lifeCycle.controlVersion.length) > 1) {
							if ((this.get("doer").lifeCycle.controlVersion[0].contribute.length) > 1) {
								for ( var j = 0; j < (this.get("doer").lifeCycle.controlVersion[0].contribute.length); j++) {
									if ((this.get("doer").lifeCycle.controlVersion[0].contribute[j].role) == 'AU') {
										if ((this.get("doer").lifeCycle.controlVersion[0].contribute[j].entity.length) <= 6) {
											if ((this.get("doer").lifeCycle.controlVersion[0].contribute[j].entity.length) > 4) {
												for ( var k = 0; k < 4; k++) {
													if (k != 3) {
														autores += (this.get("doer").lifeCycle.controlVersion[0].contribute[j].entity[k]["#text"]) + '; ';
													} else {
														autores += (this.get("doer").lifeCycle.controlVersion[0].contribute[j].entity[k]["#text"]) + ' ...';
													}
												}
											}else{
												for ( var k = 0; k < (this.get("doer").lifeCycle.controlVersion[0].contribute[j].entity.length); k++) {
													if (k != ((this.get("doer").lifeCycle.controlVersion[0].contribute[j].entity.length) - 1)) {
														autores += (this.get("doer").lifeCycle.controlVersion[0].contribute[j].entity[k]["#text"]) + '; ';
													} else {
														autores += (this.get("doer").lifeCycle.controlVersion[0].contribute[j].entity[k]["#text"]);
													}
												}
											}
										}else{
											if ((this.get("doer").lifeCycle.controlVersion[0].contribute[j].entity.length) > 1) {
												autores = (this.get("doer").lifeCycle.controlVersion[0].contribute[j].entity[0]["#text"]) + " Et.Al";
											}else{
												autores = (this.get("doer").lifeCycle.controlVersion[0].contribute[j].entity["#text"]) + " Et.Al";
											}
										}
									}
								}
							}else{
								if ((this.get("doer").lifeCycle.controlVersion[0].contribute.role) == 'AU') {
									if ((this.get("doer").lifeCycle.controlVersion[0].contribute.entity.length) <= 6) {
										if ((this.get("doer").lifeCycle.controlVersion[0].contribute.entity.length) > 4) {
											for ( var k = 0; k < 4; k++) {
												if (k != 3) {
													autores += (this.get("doer").lifeCycle.controlVersion[0].contribute.entity[k]["#text"]) + '; ';
												} else {
													autores += (this.get("doer").lifeCycle.controlVersion[0].contribute.entity[k]["#text"]) + ' ...';
												}
											}
										}else{
											for ( var k = 0; k < (this.get("doer").lifeCycle.controlVersion[0].contribute.entity.length); k++) {
												if (k != ((this.get("doer").lifeCycle.controlVersion[0].contribute.entity.length) - 1)) {
													autores += (this.get("doer").lifeCycle.controlVersion[0].contribute.entity[k]["#text"]) + '; ';
												} else {
													autores += (this.get("doer").lifeCycle.controlVersion[0].contribute.entity[k]["#text"]);
												}
											}
										}
									}else{
										if ((this.get("doer").lifeCycle.controlVersion[0].contribute.entity.length) > 1) {
											autores = (this.get("doer").lifeCycle.controlVersion[0].contribute.entity[0]["#text"]) + " Et.Al";
										}else{
											autores = (this.get("doer").lifeCycle.controlVersion[0].contribute.entity["#text"]) + " Et.Al";
										}
									}
								}
							}
						}else{
							if ((this.get("doer").lifeCycle.controlVersion.contribute.length) > 1) {
								for ( var j = 0; j < (this.get("doer").lifeCycle.controlVersion.contribute.length); j++) {
									if ((this.get("doer").lifeCycle.controlVersion.contribute[j].role) == 'AU') {
										if ((this.get("doer").lifeCycle.controlVersion.contribute[j].entity.length) <= 6) {
											if ((this.get("doer").lifeCycle.controlVersion.contribute[j].entity.length) > 4) {
												for ( var k = 0; k < 4; k++) {
													if (k != 3) {
														autores += (this.get("doer").lifeCycle.controlVersion.contribute[j].entity[k]["#text"]) + '; ';
													} else {
														autores += (this.get("doer").lifeCycle.controlVersion.contribute[j].entity[k]["#text"]) + ' ...';
													}
												}
											}else{
												for ( var k = 0; k < (this.get("doer").lifeCycle.controlVersion.contribute[j].entity.length); k++) {
													if (k != ((this.get("doer").lifeCycle.controlVersion.contribute[j].entity.length) - 1)) {
														autores += (this.get("doer").lifeCycle.controlVersion.contribute[j].entity[k]["#text"]) + '; ';
													} else {
														autores += (this.get("doer").lifeCycle.controlVersion.contribute[j].entity[k]["#text"]);
													}
												}
											}
										}else{
											if ((this.get("doer").lifeCycle.controlVersion.contribute[j].entity.length) > 1) {
												autores = (this.get("doer").lifeCycle.controlVersion.contribute[j].entity[0]["#text"]) + " Et.Al";
											}else{
												autores = (this.get("doer").lifeCycle.controlVersion.contribute[j].entity["#text"]) + " Et.Al";
											}
										}
									}
								}
							}else{
								if ((this.get("doer").lifeCycle.controlVersion.contribute.role) == 'AU') {
									if ((this.get("doer").lifeCycle.controlVersion.contribute.entity.length) <= 6) {
										if ((this.get("doer").lifeCycle.controlVersion.contribute.entity.length) > 4) {
											for ( var k = 0; k < 4; k++) {
												if (k != 3) {
													autores += (this.get("doer").lifeCycle.controlVersion.contribute.entity[k]["#text"]) + '; ';
												} else {
													autores += (this.get("doer").lifeCycle.controlVersion.contribute.entity[k]["#text"]) + ' ...';
												}
											}
										}else{
											for ( var k = 0; k < (this.get("doer").lifeCycle.controlVersion.contribute.entity.length); k++) {
												if (k != ((this.get("doer").lifeCycle.controlVersion.contribute.entity.length) - 1)) {
													autores += (this.get("doer").lifeCycle.controlVersion.contribute.entity[k]["#text"]) + '; ';
												} else {
													autores += (this.get("doer").lifeCycle.controlVersion.contribute.entity[k]["#text"]);
												}
											}
										}
									}else{
										if ((this.get("doer").lifeCycle.controlVersion.contribute.entity.length) > 1) {
											autores = (this.get("doer").lifeCycle.controlVersion.contribute.entity[0]["#text"]) + " Et.Al";
										}else{
											autores = (this.get("doer").lifeCycle.controlVersion.contribute.entity["#text"]) + " Et.Al";
										}
									}
								}
							}
						}
						return autores;
					}catch(err){
						return 'Campo no definido';
					}
				},

				getPalabrasClave : function() {
					// if((this.get("doer").general.titlePack.keyword) !=
					// undefined){
					try {
						return this.get("doer").general.titlePack.keyword.li;
					} catch (err) {
						return 'Campo no definido';
					}
					// }else{
					// return 'Campo no definido';
					// }
				},

				getImgLicencia : function() {
					try {
						// return this.get("doer").rights.licence.type;
						return 'default.png';
					} catch (err) {
						return 'default.png';
					}
				},

				getTipoLicencia : function() {
					try {
						return this.get("doer").rights.license.type;
					} catch (err) {
						return 'Campo no definido';
					}
				},

				getExtension : function() {
					try {
						if((this.get("doer").technical.properties.length) > 1){
							return this.get("doer").technical.properties[0].file.extension;
						}else{
							return this.get("doer").technical.properties.file.extension;
						}
					} catch (err) {
						return 'Campo no definido';
					}
				},

				tamBloque : function() {
					var tamanio = '';
					if (($(window).width() > 1493)) {
						tamanio = 'metablocklarge';
					} else {
						tamanio = 'metablocksmall';
					}
					return tamanio;
				},

				dominioPublico : function() {
					if ((this.get("doer").rights.publicDomain)) {
						return 'Si';
					} else {
						return 'No';
					}
				},
				
				getLifeCycle : function(){
					if ((this.get("doer").lifeCycle.controlVersion.length) > 1) {
						return '<li title="Ciclo de Vida"><a href="#lifeCycle' + this.cid + '" class="liset2" data-toggle="tab"><span class="icon-oxp-time"></span></a></li>';
					}else{
						return '';
					}
				},
				
				getLifeCycleContenido : function(){
					try{
						var autores = '';
						if ((this.get("doer").lifeCycle.controlVersion.length) > 1) {
							for ( var i = 0; i < (this.get("doer").lifeCycle.controlVersion.length); i++) {
								if ((this.get("doer").lifeCycle.controlVersion[i].contribute.length) > 1) {
									for ( var j = 0; j < (this.get("doer").lifeCycle.controlVersion[i].contribute.length); j++) {
										if ((this.get("doer").lifeCycle.controlVersion[i].contribute[j].role) != 'AU') {
											autores += '<strong>' + this.get("doer").lifeCycle.controlVersion[i].contribute[j].role + '</strong><br />';
											if ((this.get("doer").lifeCycle.controlVersion[i].contribute[j].entity.length) > 1) {
												for ( var k = 0; k < (this.get("doer").lifeCycle.controlVersion[i].contribute[j].entity.length); k++) {
													if (k != ((this.get("doer").lifeCycle.controlVersion[i].contribute[j].entity.length) - 1)) {
														autores += (this.get("doer").lifeCycle.controlVersion[i].contribute[j].entity[k]["#text"]) + '<br />';
													}else{
														autores += (this.get("doer").lifeCycle.controlVersion[i].contribute[j].entity[k]["#text"]) + '<br /><br />';
													}
												}
											}else{
												autores += (this.get("doer").lifeCycle.controlVersion[i].contribute[j].entity["#text"]) + '<br /><br />';
											}
										}
									}
								}else{
									if ((this.get("doer").lifeCycle.controlVersion[i].contribute.role) != 'AU') {
										autores += '<strong>' + this.get("doer").lifeCycle.controlVersion[i].contribute.role + '</strong><br />';
										if ((this.get("doer").lifeCycle.controlVersion[i].contribute.entity.length) > 1) {
											for ( var k = 0; k < (this.get("doer").lifeCycle.controlVersion[i].contribute.entity.length); k++) {
												if (k != ((this.get("doer").lifeCycle.controlVersion[i].contribute.entity.length) - 1)) {
													autores += (this.get("doer").lifeCycle.controlVersion[i].contribute.entity[k]["#text"]) + '<br />';
												}else{
													autores += (this.get("doer").lifeCycle.controlVersion[i].contribute.entity[k]["#text"]) + '<br /><br />';
												}
											}
										}else{
											autores += (this.get("doer").lifeCycle.controlVersion[i].contribute.entity["#text"]) + '<br /><br />';
										}
									}
								}
							}
						}else{
							if ((this.get("doer").lifeCycle.controlVersion.contribute.length) > 1) {
								for ( var j = 0; j < (this.get("doer").lifeCycle.controlVersion.contribute.length); j++) {
									if ((this.get("doer").lifeCycle.controlVersion.contribute[j].role) != 'AU') {
										autores += '<strong>' + this.get("doer").lifeCycle.controlVersion.contribute[j].role + '</strong><br />';
										if ((this.get("doer").lifeCycle.controlVersion.contribute[j].entity.length) > 1) {
											for ( var k = 0; k < (this.get("doer").lifeCycle.controlVersion.contribute[j].entity.length); k++) {
												if (k != ((this.get("doer").lifeCycle.controlVersion.contribute[j].entity.length) - 1)) {
													autores += (this.get("doer").lifeCycle.controlVersion.contribute[j].entity[k]["#text"]) + '<br />';
												}else{
													autores += (this.get("doer").lifeCycle.controlVersion.contribute[j].entity[k]["#text"]) + '<br /><br />';
												}
											}
										}else{
											autores += (this.get("doer").lifeCycle.controlVersion.contribute[j].entity["#text"]) + '<br /><br />';
										}
									}
								}
							}else{
								if ((this.get("doer").lifeCycle.controlVersion.contribute.role) != 'AU') {
									autores += '<strong>' + this.get("doer").lifeCycle.controlVersion.contribute.role + '</strong><br />';
									if ((this.get("doer").lifeCycle.controlVersion.contribute.entity.length) > 1) {
										for ( var k = 0; k < (this.get("doer").lifeCycle.controlVersion.contribute.entity.length); k++) {
											if (k != ((this.get("doer").lifeCycle.controlVersion.contribute.entity.length) - 1)) {
												autores += (this.get("doer").lifeCycle.controlVersion.contribute.entity[k]["#text"]) + '<br />';
											}else{
												autores += (this.get("doer").lifeCycle.controlVersion.contribute.entity[k]["#text"]) + '<br /><br />';
											}
										}
									}else{
										autores += (this.get("doer").lifeCycle.controlVersion.contribute.entity["#text"]) + '<br /><br />';
									}
								}
							}
						}
						return autores;
					}catch(err){
						return 'Campo no definido';
					}
				},
				
				getTechnicalContenido : function(){
					try {
						var result = '';
						result += '<strong>Nombre del archivo:</strong><br />';
						if((this.get("doer").technical.properties.length) > 1){
							result += this.get("doer").technical.properties[0].file.name + '<br /><br />';
						}else{
							result += this.get("doer").technical.properties.file.name + '<br /><br />';
						}
						result += '<strong>Extensi&oacute;n del archivo:</strong><br />';
						if((this.get("doer").technical.properties.length) > 1){
							result += this.get("doer").technical.properties[0].file.extension + '<br /><br />';
						}else{
							result += this.get("doer").technical.properties.file.extension + '<br /><br />';
						}
						result += '<strong>Formato del archivo:</strong><br />';
						if((this.get("doer").technical.properties.length) > 1){
							result += this.get("doer").technical.properties[0].file.format + '<br /><br />';
						}else{
							result += this.get("doer").technical.properties.file.format + '<br /><br />';
						}
						result += '<strong>Peso del archivo:</strong><br />';
						if((this.get("doer").technical.properties.length) > 1){
							result += this.get("doer").technical.properties[0].file.size + '<br /><br />';
						}else{
							result += this.get("doer").technical.properties.file.size + ' ' + this.get("doer").technical.properties.file.unit + '<br /><br />';
						}
						result += '<strong>Enlace del archivo:</strong><br />';
						if((this.get("doer").technical.properties.length) > 1){
							if((this.get("doer").technical.properties[0].file.src)!=undefined){
								result += '<a href="' + this.get("doer").technical.properties[0].location.src + '" target="_blank">' + this.get("doer").technical.properties[0].location.src + '</a><br /><br />';
							}else{
								result += '<a href="' + this.get("doer").technical.properties[0].location.asVar + '" target="_blank">' + this.get("doer").technical.properties[0].location.asVar + '</a><br /><br />';
							}
						}else{
							if((this.get("doer").technical.properties.file.src)!=undefined){
								result += '<a href="' + this.get("doer").technical.properties.location.src + '" target="_blank">' + this.get("doer").technical.properties.location.src + '</a><br /><br />';
							}else{
								result += '<a href="' + this.get("doer").technical.properties.location.asVar + '" target="_blank">' + this.get("doer").technical.properties.location.asVar + '</a><br /><br />';
							}
						}
						return result;
					} catch (err) {
						return 'Campo no definido';
					}
				},
				
				getEducationalContenido : function(){
					try {
						var result = '';
						result += '<strong>Tipo de recurso:</strong><br />';
						result += this.getTipoRecurso() + '<br /><br />';
						result += '<strong>&Aacute;rea del Conocimiento:</strong><br />';
						result += this.getAreaConocimiento() + '<br /><br />';
						result += '<strong>N&uacute;cleo B&aacute;sico del Conocimiento:</strong><br />';
						result += this.getNucleoBasicoConocimiento() + '<br /><br />';
						return result;
					} catch (err) {
						return 'Campo no definido';
					}
				},
				
				getAnnotation : function(){
					if ((this.get("doer").annotations != undefined)) {
						return '<li title="Anotaciones"><a href="#annotation' + this.cid + '" class="liset6" data-toggle="tab"><span class="icon-oxp-check"></span></a></li>';
					}else{
						return '';
					}
				},
				
				getAnnotationContenido: function(){
					try {
						return 'Campo no definido';
					} catch (err) {
						return 'Campo no definido';
					}
				},
				
				getRelations : function(){
					if ((this.get("doer").relations != undefined)) {
						return '<li title="Relaciones"><a href="#relations' + this.cid + '" class="liset5" data-toggle="tab"><span class="icon-dbs-neo4j"></span></a></li>';
					}else{
						return '';
					}
				},
				
				getRelationsContenido: function(){
					try {
						return 'Campo no definido';
					} catch (err) {
						return 'Campo no definido';
					}
				},
				
				getClassification : function(){
					if ((this.get("doer").classification != undefined)) {
						return '<li title="Clasificacion"><a href="#classification' + this.cid + '" class="liset7" data-toggle="tab"><span class="icon-dbs-sqlite"></span></a></li>';
					}else{
						return '';
					}
				},
				
				getClassificationContenido: function(){
					try {
						return 'Campo no definido';
					} catch (err) {
						return 'Campo no definido';
					}
				},
				
			});
})(app.models);