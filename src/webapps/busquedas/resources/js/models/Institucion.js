(function ( models ) {
	models.Institucion = Backbone.Model.extend({
		getInstitucion : function() {
			try{
				return this.get("name");
			}catch(err){
				return "campo no definido";
			}
		},
		
		getImgInstitucion : function() {
			try {
				return "resources/img/institutions/" + this.get("entity") + ".png";
			} catch (err) {
				return 'resources/img/institutions/default.png';
			}
		}
		
	});
})( app.models );