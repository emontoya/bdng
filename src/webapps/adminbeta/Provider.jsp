<%@page import="com.sun.tools.javac.code.Type.ForAll"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashSet"%>
<%@page import="org.bdng.harvmgr.harvester.DataProvider"%>
<%@page import="org.bdng.harvmgr.dlConfig"%>
<%@page import="org.jdom2.Document"%>
<%@page import="java.util.List" %>
<%@page import="org.jdom2.Element"%>
<%@page import="org.jdom2.input.SAXBuilder" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
    <%    //verificamos session como "admin" o como "adminRepo" sino, lo redirijimos al index.jsp
	HttpSession sesion = request.getSession();
    String user ="";
    String type = "";
    String shortNameAttr = "";
    if(sesion.getAttribute("username")== null && sesion.getAttribute("type")== null) {
    	response.sendRedirect("errorSession.jsp");
    	
	}else{
		user = (String)sesion.getAttribute("username");
    	type = (String)sesion.getAttribute("type");
    	shortNameAttr = (String)sesion.getAttribute("stShortName");
    		if(!type.equals("adminRepo") && !type.equals("admin"))
    		
    			response.sendRedirect("errorSession.jsp");
	} %>
    
    
    <%
    

    if(sesion.getAttribute("user")!= null)
    	user = (String)sesion.getAttribute("user");
    
    
    
    //lista de variables inicializadas con vacio
	String shortName="";
	String RepositoryName="";
	String description="";
	String urlhome="";
	String protocol=""; 
	String baseURL ="";
	String emailadmin="";
	String nameAdmin="";
	String phoneAdmin="";
	String reptype="";
	String collection_sel="";
	String metadataPrefix="";
	String institution="";
	String shortInst="";
	String staticCollection="";
	String Municipio="";
	String Departamento="";
	String bdcol="";

    
    shortName = shortNameAttr;

		if(shortName!=null){    
		    DataProvider dp = new DataProvider(this.getServletContext().getRealPath("/"));
		    
		    dp.setValues(shortName); //setea todos los valores de del provider para poder utilizar los get
		    RepositoryName = dp.getRepositoryName();
		    description = dp.getDescription();
		    urlhome = dp.getUrlhome();
		    protocol = dp.getProtocol();
		    baseURL = dp.getBaseURL();
		    emailadmin = dp.getEmailadmin();
		    nameAdmin = dp.getNameAdmin();
		    phoneAdmin = dp.getPhoneAdmin();
		    reptype = dp.getReptype();
			collection_sel = dp.getCollection();
		    metadataPrefix = dp.getMetadataPrefix();
		    institution = dp.getInstitution2();
		    shortInst = dp.getShortInst2();
		    staticCollection = dp.getStaticCollection2();
		    Departamento = dp.getDepartamento();
		    Municipio = dp.getMunicipio();
		    bdcol = dp.getBdcol();
		    
		    
		    
		    
		}else{			
			shortName = "";
		}    
		    String OAI="", HTTP="";
		    String oai_dc="", oai_etd="", dc ="";
		    
		    if (protocol.equals("OAI")){ //control de listmenu protocol
		    	OAI="selected";
		    }else{
		    	HTTP="selected";
		    }
		    
		    //lo siguiente hay que cambiarlo pues ya se genera lista apartir de una consulta

		    if (metadataPrefix.equals("oai_dc")){//control de listmenu metadatos
		    	oai_dc="selected";
		    }else
		    	if(metadataPrefix.equals("oai_etd")){
		    	oai_etd="selected";	
		    	}
		    	else{
		    		dc = "selected";
		    		
		    	}
		    
		    //control del checkbox bdcol
	        if(bdcol.equalsIgnoreCase("true")){
	        	bdcol = "checked";	
		 	    }
		    else{
		    	bdcol = "";
		    }
		    
		    
		    //control del checkbox reptype
		        if(reptype.equalsIgnoreCase("true")){
		        	reptype = "checked";	
			 	    }
			    else{
			    	reptype = "";
			    }
		    
		      //control del checkbox staticCollection  
		        if(staticCollection.equalsIgnoreCase("true")){
		        	staticCollection = "checked";	
			 	    }
			    else{
			    	staticCollection = "";
			    }
		    //control de la lista desplegagle de collections (Select)
		    
		        dlConfig doa = new dlConfig(this.getServletContext().getRealPath("/"));
		    	List l = doa.GetVisualCollections();
		    
				
		    	Element collections = null;
				Element collection= null;
				
				
			//control de la lista de metadaPrefix
				
				List ListMetadataPrefix = doa.GetVisualListMetadataPrefix();
			
		    	Element eListMetadataPrefix = null;
				Element eMetadataPrefix= null;
				ArrayList<String> ArrayMetadataPrefix = new ArrayList<String>();
				
				
				for(int j = 0; j<ListMetadataPrefix.size();j++){
					
					eListMetadataPrefix=(Element)ListMetadataPrefix.get(j);
					eMetadataPrefix=eListMetadataPrefix.getChild("name");
					ArrayMetadataPrefix.add(eMetadataPrefix.getText());
				
		      	}    
		    	
				
    %>


<div id="title_section" class="text-center">
	<h2>Agregar/Editar Repositorio</h2>
</div> 

<br />
  <form name="form1" id="form1" method="post" action="servlet/ProviderWeb?prov_opcion=crear_provider">
    <table width="767" border="0" align="center">
      <tr>
        <td width="197"><p align="right"><em>Institucion:</em></p></td>
        <td width="459"><label>
          <input type="text" name="institution" id="institution" value="<%=institution%>" size="70">
        </label></td>
        <td width="97">&nbsp;</td>
      </tr>
      <tr>
        <td><p align="right"><em>Short Institucion:</em></p></td>
        <td><label>
          <input type="text" name="shortInst" id="shortInst" value="<%=shortInst%>" size="70">
        </label></td>
        <td width="97">&nbsp;</td>
      </tr>
      <tr>
        <td><p align="right"><em>Departamento:</em></p></td>
        <td><label>
          <select  class="chosen" name="Departamento" id="Departamento" onchange="cargarMunicipios()">
          <option value="0" selected>Seleccione... 
          <%
          
          String Departamentos [] = {"amazonas","antioquia","arauca","atlantico","bolivar","boyaca","caldas","caqueta","casanaere","cauca","cesar","choco","cordoba","cundinamarca","guainia","guaviare","huila","la guajira","magdalena","meta","narino","norte de santander","putumayo","quindio","risaralda","san andres y providencia","santander","sucre","tolima","valle del cauca","vaupes","vichada"};
		for(int j = 0; j<Departamentos.length;j++){
			if(Departamentos[j].equalsIgnoreCase(Departamento.trim())){  
			//cuando se entrar por edit compara el vector , con el valor guardado (departament) para agregarle la selected y la muestre seleccionada en el jsp 
				out.print("<option value=\""+Departamentos[j]+"\" selected >"+Departamentos[j]+"</option>");
			}else{
				out.print("<option value=\""+Departamentos[j]+"\">"+Departamentos[j]+"</option>");	
			}
      	}    
      %>      
          </select>
        </label></td>
        <td width="97">&nbsp;</td>
      </tr>
      <tr>
      <tr>
        <td><p align="right"><em>Municipio:</em></p></td>
        <td><label>
          <select class="chosen" name="Municipio" id="Municipio" style="width:200px;">
			
			<%
			out.print("<option value=\""+Municipio+"\" selected >"+Municipio+"</option>");
			%>
			 
			      
          </select>
        </label></td>
        <td width="97">&nbsp;</td>
      </tr>
      <tr>
        <td><p align="right"><em>Nombre del Repositorio:</em></p></td>
        <td><label>
          <input type="text" name="repname" id="repname" value="<%=RepositoryName%>" size="70">
        </label></td>
        <td width="97">&nbsp;</td>
      </tr>
      
      <tr>
        <td><p align="right"><em>Descripcion</em></p></td>
        <td><label>
          <textarea name="descripcion" id="descripcion" cols="51" rows="5" ><%=description%></textarea>
        </label></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td><p align="right"><em>URL Home:</em></p></td>
        <td><input type="text" name="urlhome" id="urlhome"value="<%=urlhome%>" size="70"></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td><p align="right"><em>(*)Nombre Corto (ID)</em></p></td>
        <td><input type="text" name="shortname" id="shortname" value="<%=shortName%>" size="70" onBlur="check_provider()"><span id="Result" Style="position:absolute"></span></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td><p align="right"><em>Protocolo:</em></p></td>
        <td><label>
          <select name="protocol" id="protocol" >
          	<option value="HTTP"<%=HTTP%> >HTTP</option>
            <option value="OAI" <%=OAI%>>OAI</option>
           </select>
        </label></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td><p align="right"><em>(*) URL Base:</em></p></td>
        <td><input type="text" name="urlbase" id="urlbase" value="<%=baseURL%>" size="70">
        <input type="button" name="Consultar" id="Consultar" value="Consultar" onClick="cargarContenido()"></td>
        <td>&nbsp;</td>
      </tr>
       <tr>
        <td><p align="right"><em>Nombre Admin:</em></p></td>
        <td><input type="text" name="nameAdmin" id="nameAdmin" value="<%=nameAdmin%>" size="70"></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
       <tr>
        <td><p align="right"><em>Admin Email:</em></p></td>
        <td><input type="text" name="emailadmin" id="emailadmin" value="<%=emailadmin%>" size="70"></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
      <tr>
        <td><p align="right"><em>Telefono Admin:</em></p></td>
        <td><input type="text" name="phoneAdmin" id="phoneAdmin" value="<%=phoneAdmin%>" size="70"></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td><p>&nbsp;</p></td>
        <td><label>
          <input type="checkbox" name="bdcol" id="bdcol" value="true" <%=bdcol%>>
          <em>Repositorio Oficial</em></label></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td><p>&nbsp;</p></td>
        <td><label>
          <input type="checkbox" name="reptype" id="reptype" value="true" <%=reptype%>>
          <em>Repositorio Estatico </em></label></td>
        <td>&nbsp;</td>
      </tr>
      
        <tr>
        <td><p>&nbsp;</p></td>
        <td><label>
          <input type="checkbox" name="staticCollection" id="staticCollection" value="true" <%=staticCollection%>">
          <em>Colecci�n Estatica? </em></label></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td><p align="right"><em>(*) Coleccion</em></p></td>
        <td>  <select name="collection" id="collection" >
        
              	
       
     <%
    
		for(int j = 0; j<l.size();j++){
			
			collections=(Element)l.get(j);
			collection=collections.getChild("name");
			if(collection_sel.equalsIgnoreCase(collection.getText().toString().trim())){  
			//cuando se entrar por edit compara la lista, con el valor guardado (collection_sel) para agregarle la selected y la muestre seleccionada en el jsp 
				out.print("<option value=\""+collection.getText()+"\" selected >"+collection.getText()+"</option>");
			}else{
				out.print("<option value=\""+collection.getText()+"\">"+collection.getText()+"</option>");	
				
			}
      	}    
    
    %>
            
            
            </select></td>
        <td>&nbsp;</td>
      </tr>
      <tr id=metadata_oai>
        <td><p align="right"><em>Metadatos</em></p></td>
        <td><label>
          <select name="metadataprefix_oai" id="metadataprefix_oai">
         
          	<option value="<%=metadataPrefix%>" selected="selected"><%=metadataPrefix%></option>
            
            </select>
          <input type="button" name="Consultar2" id="Consultar2" value="Consultar" onClick="List(this.form)" >
        </label></td>
        <td>&nbsp;</td>
      </tr>
      
         <tr id=metadata_http style="display: none">
        <td><p align="right"><em>Metadatos</em></p></td>
        <td><label>
          <select name="metadataprefix_http" id="metadataprefix_http">
          <option value="<%=metadataPrefix%>" selected="selected"><%=metadataPrefix%></option>
			<%
			for(String value : ArrayMetadataPrefix){ %>
	  		
	  			<option value="<%=value.trim()%>"><%=value.trim()%></option>
	  			
			<%} %>

            
            </select>
      
        </label></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td></td>
        <td></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td><label>
        <input type="hidden" name="owner" id="owner" value=<%=user %>>
          <input type="submit" name="Grabar" id="Grabar" value="Grabar">
          <input  type="reset" name="Clear" id="Clear" value="Cancelar">
        </label></td>
        <td>&nbsp;</td>
      </tr>
    </table>
  </form>
  
  <% sesion.setAttribute("stShortName", null); %>
  

