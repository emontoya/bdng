<%@ page import="javax.xml.parsers.DocumentBuilderFactory,javax.xml.parsers.DocumentBuilder,org.w3c.dom.*"%>

<html><head>
<title>Biblioteca Digital Colombiana - Configuracion del servidor</title>
<body>
    <center><img src="bdcol.jpg" width="200" height="87" alt="bdcol"/></center>
      <br><br><br><p><p><p><p>

    <center><table border="2">
<%
DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
DocumentBuilder db = dbf.newDocumentBuilder();
Document doc = db.parse("../conf/ServerConfig.xml");

NodeList nl= doc.getElementsByTagName("dl_home");
NodeList n2= doc.getElementsByTagName("dl_metadata");
NodeList n3= doc.getElementsByTagName("dl_data");
NodeList n4= doc.getElementsByTagName("dl_name");
NodeList n5= doc.getElementsByTagName("dl_col");
NodeList n6= doc.getElementsByTagName("record_xml");
NodeList n7= doc.getElementsByTagName("exist_user");
NodeList n8= doc.getElementsByTagName("exist_pass");
NodeList n9= doc.getElementsByTagName("exist_uri");
NodeList n10= doc.getElementsByTagName("exist_driver");
NodeList n11= doc.getElementsByTagName("proxy_host");
NodeList n12= doc.getElementsByTagName("proxy_port");
NodeList n13= doc.getElementsByTagName("proxy");
NodeList n14= doc.getElementsByTagName("harvest_count_incremental");
NodeList n15= doc.getElementsByTagName("harvest_frecuency");
NodeList n16= doc.getElementsByTagName("number_threads");
%>
<tr>
    <td colspan="2" align ="center"><h4><font color='green'>Parametros de Configuracion del Servidor</font></h4></td></tr>
<tr><td><font color='blue'>dl_home</font></td>
<td><%= nl.item(0).getFirstChild().getNodeValue() %> </td>
</tr><tr><td><font color='blue'>dl_metadata</font></td>
<td><%= n2.item(0).getFirstChild().getNodeValue() %> </td></tr><tr>
<td><font color='blue'>dl_data</font></td>
<td><%= n3.item(0).getFirstChild().getNodeValue() %> </td></tr>
<tr><td><font color='blue'>dl_name</font></td>
<td><%= n4.item(0).getFirstChild().getNodeValue() %> </td></tr>
<tr><td><font color='blue'>dl_col</font></td>
<td><%= n5.item(0).getFirstChild().getNodeValue() %> </td></tr>
<tr><td><font color='blue'>record_xml</font></td>
<td><%= n6.item(0).getFirstChild().getNodeValue() %> </td></tr>
<tr><td><font color='blue'>exist_user</font></td>
<td><%= n7.item(0).getFirstChild().getNodeValue() %> </td></tr>
<tr><td><font color='blue'>exist_pass</font></font></td>
<td><%= n8.item(0).getFirstChild().getNodeValue() %> </td></tr>
<tr><td><font color='blue'>exist_uri</font></td>
<td><%= n9.item(0).getFirstChild().getNodeValue() %> </td></tr>
<tr><td><font color='blue'>exist_driver</font></td>
<td><%= n10.item(0).getFirstChild().getNodeValue() %> </td></tr>
<tr><td><font color='blue'>proxy_host</font></td>
<td><%= n11.item(0).getFirstChild().getNodeValue() %> </td></tr>
<tr><td><font color='blue'>proxy_port</font></td>
<td><%= n12.item(0).getFirstChild().getNodeValue() %> </td></tr>
<tr><td><font color='blue'>proxy</font></td>
<td><%= n13.item(0).getFirstChild().getNodeValue() %> </td></tr>
<tr><td><font color='blue'>harvest_count_incremental</font></td>
<td><%= n14.item(0).getFirstChild().getNodeValue() %> </td></tr>
<tr><td><font color='blue'>harvest_frecuency</font></td>
<td><%= n15.item(0).getFirstChild().getNodeValue() %> </td></tr>
<tr><td><font color='blue'>number_threads</font></td>
<td><%= n16.item(0).getFirstChild().getNodeValue() %> </td></tr>
<tr><td><font color='blue'>exist_uri</font></td>
<td><%= n16.item(0).getFirstChild().getNodeValue() %></td></tr>
</table>
</center><br><p><p>
<center> <A HREF="index.html">Menu de gestion de configuracion del servidor</A></center>
</body>
</html>