<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Vector"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.bdng.harvmgr.validator.Validator"%>
<%@page import="java.util.HashMap"%>
<%@page import="org.bdng.harvmgr.harvester.DataProvider"%>
<%@page import="org.bdng.harvmgr.dlConfig"%>
<%@page import="org.jdom2.Document"%>
<%@page import="java.util.List" %>
<%@page import="org.jdom2.Element"%>
<%@page import="org.jdom2.input.SAXBuilder" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%
 	HttpSession sesion = request.getSession();
    String shortNameAttr = "";

	String user = "";
    if(sesion.getAttribute("username")!= null)
    	user = (String)sesion.getAttribute("username");
    
    shortNameAttr = (String)sesion.getAttribute("stShortName");
    dlConfig doa = new dlConfig(this.getServletContext().getRealPath("/"));
    List l = doa.GetVisualValidation(shortNameAttr);
    
    
    
	Element verbo = null;	
	Element codeMessage = null;
	Element details = null;
	Element total = null;



	Element protocol = (Element)l.get(0);
	Element metadataPrefix = (Element)l.get(1);



	String protocolType = "";
	protocolType=protocol.getValue();
	
	String metadataPrefixType = "";
	metadataPrefixType = metadataPrefix.getValue();
	

	

		
    %>


<div id="accordion" style="height:auto;">

	<h3><a href="#">Resumen</a></h3>
	<div>
		<p>Resumen Validacion Repositorio: <%=shortNameAttr%>  </p>
		<%
		
		
		total =(Element)l.get(l.size()-1);
		Element totalTime = total.getChild("totalTime");
		String totalTimelbl = totalTime.getValue();
		Element score = total.getChild("score");
		String scorelbl = score.getValue();
		%>
		
		
		<p>Protocolo: <%=protocolType%>  </p>
		<p>Metadatos: <%=metadataPrefixType%>  </p>
	
		<p>Score: <%=scorelbl%>  </p>
		<p>Tiempo total: <%=totalTimelbl%>  </p>
		
		
		<%
	
		details = total.getChild("details");
		List detailsList = details.getChildren();
		Element detail = null;
			%><ul><% 
			//out.print("size afuera "+detailsList.size());
			for (int j = 0; j < detailsList.size(); j++) {
				
				
				Element validation = (Element)detailsList.get(j); //protocol.. semantic... etc
				
				
				if(validation.getName().equals("syntax")) continue; //esto lo hago para mostrar la validacion de sintaxis en otro desplegable
				//out.println("validacion de "+validation.getName());
				
				List detailsValidation = validation.getChildren();
				
				//out.print("size adentro "+detailsValidation.size());
				for (int k = 0; k < detailsValidation.size(); k++) {
					
					detail = (Element)detailsValidation.get(k);
					String lbldetail = detail.getValue();

					%>
					<li> <%= lbldetail%> </li>
					<%
					
				}
				
				
			}
			%></ul><%
			
	
			/// esta es la lista de mapeo que se hacer a partir del textpatterns.xml
			Validator validator = new Validator(this.getServletContext().getRealPath("/"));
			HashMap<String,Vector<String>> map = validator.identificarPatrones(shortNameAttr); // Key: Tag, Content: oldText1,oldtext2....
			
			if(map.size()!=0){ //si el mapa llega vacio es porque no hay nada que ofrecer para mapear
				%><div>Lista de Mapeo</div> </br></br><%
					%><div class="ListMap" ><%
						%><form  method="post" action="servlet/ValidatorWeb?option=updateTextPatterns&shortname=<%=shortNameAttr%>"><%
							%><table><%
							
									%><th>Tag</th><%
									%><th>Old Text</th><%
									%><th>New Text</th><%
							
							for(String tag : map.keySet()){
								
								Vector<String> oldTexts  =map.get(tag);
								
								ArrayList<String> posibleVocabulario = validator.obtenerVocabulario(tag, metadataPrefixType); 
									String oldTextsShow ="";
									for(String old: oldTexts ){
										oldTextsShow= old.replace(" ","�?"); //este par de caracteres se le pusieron para que no hubiera espeacios en el name del select, aqui se los ponemos
										
										%>
										<tr> 
										
											
												<td>  <%=tag%></td>
												<td> <%=old%></td>
												
												<td><select class="chosen"  name="<%=tag%>___<%=oldTextsShow%>" style="min-width: 200px"> <!-- el ___ sirve para enviar estos 2 parametros relacionados y luego separarlos en el servlet -->
														<%
														for(String posible: posibleVocabulario){
															%><option value="<%=posible%>"><%=posible%></option><%	
														}
														%>
													</select>
												</td>
											 
										</tr>
									
									<%
									}
								
							
								
							}
							
						%></table><%
					%></br> <input type="submit" name="Grabar" id="Grabar" value="Grabar"><%
					%></form><%
				%></div><%
				
				
			}
		List ListAutoMap = Validator.identificarAutoCorrecciones(shortNameAttr);
//		out.print("size "+ListAutoMap.size());
		if(ListAutoMap.size()>0){
			%></br></br><%
			%><div>Lista de Auto Correcciones</div> </br></br><%
			%><div class="ListMap" ><%
					%><table><%
					
						%><th>Tag</th><%
						%><th>Old Text</th><%
						%><th>New Text</th><%
						
						for (Object i : ListAutoMap) {
							Element pattern = (Element)i;
							
							
							String tag = pattern.getChildText("tag");
							String oldText = pattern.getChildText("oldText");
							String newText = pattern.getChildText("newText");
							
							%>
							<tr>
							 	<td>  <%=tag%></td>
							 	<td>  <%=oldText%></td>
							 	<td>  <%=newText%></td>
							</tr> 
							<%
							
							//System.out.println(tag + " "+ oldText+ " "+ newText);
						}
						
						
					%></table><%
				
			
			%></div><%
			
			
		}
		
		%>
		
		
		
	</div>
	
	
	<%
	
	//Aquie empieza la validacion sintactica
	Element syntax = details.getChild("syntax");
	List Tags = syntax.getChildren("Tag");
	String scoreSintaxis = syntax.getAttributeValue("score");
	String baseUrl = syntax.getAttributeValue("baseUrl");
	String success = syntax.getAttributeValue("success");
	

	
	
	%>
	<h3><a href="#">Validacion Sintactica</a></h3>
	<div>
	<%if(success==null){//si no llega succes fue porque no se pudo hacer la validacion%>
	
	<p>La Validacion Sintatica no se ha podido realizar</p>
	<p>Score: 0%  </p>
	
	<%}
	else
	if(Tags.size()==0 && success.equals("success")){//si Tags.size ==0 quiere decir que no hubo errores y con succes controlamos de que se hizo la validacion %>
	
	<p>Validacion Sintatica Completamente Correcta:</p>
	<p>Score: 100%  </p>
	<%}else{ %>		 
		
		<p>Lista de Errores Y Recomendaciones</p>
		<p>Resumen</p>
		<p>Score: <%=scoreSintaxis%> %  </p>
		<div class="ListMap">
		<div id ="popUp" class="modal">
		<br>
			Muestra de registros no exitosos 
			<div class="modal-header">
			<div id="cboxClose">close</div>
			</div>
			<div class="modal-body">
					<div id="contentPopup" class="contentPopup">
					
					
					</div>
				
			</div>
			
		</div>
		<%
		
		
		%><table><%
			
			%><th>Tag</th><%
			%><th>Descripcion</th><%
			%><th>Exitosos</th><%
			%><th>Lista</th><%
		
		
		
		
		
		Iterator iterator = Tags.iterator();
			while(iterator.hasNext()){
				Element Tag = (Element) iterator.next();
				String nameTag = Tag.getAttributeValue("name");		
				
				Element ListIdentifier = Tag.getChild("ListIdentifier");
		
		
				
				String size = ListIdentifier.getAttributeValue("size");
				String error = String.valueOf(Integer.parseInt(size)-Integer.parseInt(ListIdentifier.getAttributeValue("error")));
				double sizeInt = Double.parseDouble(size);
				double errorInt = Double.parseDouble(error);
			
				double porcentaje = (errorInt/sizeInt*100);
				String porcentajeString = (porcentaje+"").substring(0, 4)+"%";
				String failType = ListIdentifier.getAttributeValue("failType");
				String mensaje = "";
				if(failType.equals("Recommended")) mensaje="Recomendado No Presente";
				if(failType.equals("Mandatory")) mensaje="Obligatorio No Presente";
				if(failType.equals("Multiple")) mensaje="Multiples Veces";
				
				
				%><tr><%
		
				%><td><%=nameTag%></td> <%
						
						
				List ListIdentifiers = ListIdentifier.getChildren("identifier");
				Iterator ItListIdentifiers  = ListIdentifiers.iterator();
		
				
				
					%>
					
					<td> <%=mensaje%></td> <td> <%=porcentajeString%></td> <td> <a class="moreInfo"> [Ver Muestra] </a><%
							
						%><div class="ListLinks" style="display: none">
							<ul> <%	
								while(ItListIdentifiers.hasNext()){
									Element identifier = (Element) ItListIdentifiers.next();
									
									String identifierText = identifier.getText();
									String link ="";
									if(protocol.getValue().equals("OAI")){
										link = baseUrl+"?verb=GetRecord&identifier="+identifierText+"&metadataPrefix=oai_dc";
										%> <li><a href="<%=link%>" target="_blank"><%=identifierText %></a></li> <%
									}
									else{
										%> <li><a href="<%=identifierText%>" target="_blank"><%=identifierText %></a></li> <%
									}
									
								}
							%></ul>
						</div>
						</td>
					
				<tr><%
			
				
			}
			%></table><%
			%>
			</div>
	<% }//aqui se se cierra el if de si existen Tags para msotrar %>			
		
		</div>
		
	 

<%	//aki empieza la lista de los verbos cuando se valida OAI_DC
	if(protocol.getValue().equals("OAI"))	
		
	for (int i = 2; i < l.size()-1; i++) { // OJO AKI es un 2 porque primero hay 2 elementos (Protocol y MetadataPrefix)
		
		verbo =(Element)l.get(i);
		String lblVerbo =verbo.getName();
		%>
		<h3><a href="#"><%= lblVerbo%> </a></h3>
		
		<% 
		codeMessage = verbo.getChild("codeMessage");
		String lblcodeMessage = codeMessage.getValue();
		%><div><% 
		%>
		<p> <%= lblcodeMessage%> </p>
		<% 
		
		details = verbo.getChild("details");
		 detailsList = details.getChildren();
		 detail = null;
			%><ul><% 
			
			for (int j = 0; j < detailsList.size(); j++) {
				detail = (Element)detailsList.get(j);
				String lbldetail = detail.getValue();
				
				%>
				<li> <%= lbldetail%> </li>
				<% 
				
				
			}
			%></ul><% 
		%></div><% 
	}
	

%>

</div>

