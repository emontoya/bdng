<%@page import="org.bdng.harvmgr.dlConfig"%>
<%@ page language="java" contentType="text/html; charset=US-ASCII" pageEncoding="US-ASCII"%>
    <%    //verificamos session como "admin" o como "adminRepo" sino, lo redirijimos al index.jsp
	HttpSession sesion = request.getSession();
    String user ="";
    String type = "";
    if(sesion.getAttribute("username")== null && sesion.getAttribute("type")== null) {
    	response.sendRedirect("main.jsp");
    	
	}else{
		user = (String)sesion.getAttribute("username");
    	type = (String)sesion.getAttribute("type");
    		if(!type.equals("adminRepo") && !type.equals("admin"))
    		
    			response.sendRedirect("main.jsp");
	} %>
    
    <%
    if(sesion.getAttribute("username")!= null)
    	user = (String)sesion.getAttribute("username");
    
    	dlConfig doa = new dlConfig(this.getServletContext().getRealPath("/"));
    
    	String sysName =(String) doa.getSysName();
    	String sysNameFull=(String) doa.getSysNameFull();
    	
    
    
    %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
	<title><%=sysName%> - <%=sysNameFull%></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<link type="text/css" href="resources/css/app/basic.css" rel="stylesheet"/>
	
	<link type="text/css" href="resources/bootstrap/css/bootstrap.css" rel="stylesheet">
	<link type="text/css" href="resources/bootstrap/css/bootstrap-responsive.css" rel="stylesheet">
	
</head>
<body>
	<div class="container">
		<div class="row">
			<div id="header-top"></div>
		</div>
		
		<div class="row">
			<div class="span2 breadcrumb" id="menuAdmin">
			    <ul class="nav nav-list sidenav">
			    	<li class="nav-header">General</li>
			    	<li><a href="javascript:;" onclick="loadPage('ServerConf.jsp')">Configuracion</a></li>
				    <li class="nav-header">Repositorios</li>
				    <li><a href="javascript:;" onclick="loadPage('Provider.jsp')">Agregar</a></li>
					<li><a href="javascript:;" onclick="loadPage('ListProviders.jsp')">Editar</a></li>
					<li><a href="javascript:;" onclick="loadPage('ListRepositories.jsp')">Listar Sencillo</a></li>
					<li><a href="javascript:;" onclick="loadPage('ListRepositoriesDetails.jsp')">Listar Detallado</a></li>
					<li><a href="javascript:;" onclick="loadPage('TreeRepositories.jsp')">Navegar</a></li>
					<li><a href="javascript:;" onclick="loadPage('ListToPublish.jsp')">Aprobador</a></li>
					<li class="nav-header">Recolector</li>
					<li><a href="javascript:;" onclick="loadPage('recolector.jsp')">Recolectar</a></li>
					<li><a href="javascript:;" onclick="loadPage('StatsForHarvester.jsp')">Estad&iacute;sticas</a></li>
					<li class="nav-header">Validador</li>
				    <li><a href="javascript:;" onclick="loadPage('ListValidations.jsp')">Validar BDCOL</a></li>
				    <li><a href="javascript:;" onclick="loadPage('ListValidationsReda.jsp')">Validar REDA</a></li>
				    <li><a href="javascript:;" onclick="loadPage('LogsValidationsReda.jsp')">Ver Logs</a></li>
					<li class="nav-header">Indexador</li>
					<li><a href="javascript:;" onclick="loadPage('Indexador.jsp')">Indexar</a></li>
					
					<li class="divider"></li>
					<li><a onclick="logout()" href="logout.jsp">Salir</a></li>
			    </ul>
			</div>
			<div id="main" class="span10">
				Hola
			</div>
		</div>
		
		<div class="row">
			<div id="footer-bottom"></div>
		</div>
	</div>
	<script type="text/javascript" src="resources/js/lib/jquery.min.js"></script>
	<script type="text/javascript" src="resources/bootstrap/js/bootstrap.js"></script>
	
	<script type="text/javascript" src="resources/js/app/appJSP.js"></script>
</body>
</html>