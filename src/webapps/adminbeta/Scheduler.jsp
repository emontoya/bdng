
<%@page import="java.util.Iterator"%>
<%@page import="org.bdng.harvmgr.dlConfig"%>
<%@page import="org.jdom2.Document"%>
<%@page import="java.util.List" %>
<%@page import="org.jdom2.Element"%>
<%@page import="org.jdom2.input.SAXBuilder" %>
<%@page import="org.bdng.harvmgr.harvester.DataProvider"%>
<%@page import="org.bdng.web.ProviderWeb"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
            
    <%    //verificamos session como "admin" sino, lo redirijimos al index.jsp
	HttpSession sesion = request.getSession();
    String user ="";
    String type = "";
    if(sesion.getAttribute("username")== null && sesion.getAttribute("type")== null) {
    	response.sendRedirect("index.jsp");
    	
	}else{
		user = (String)sesion.getAttribute("username");
    	type = (String)sesion.getAttribute("type");
    		if(!user.equals("admin") || !type.equals("admin"))
    			response.sendRedirect("index.jsp");
	} %>
    
    <%
    
    	
    	dlConfig doa = new dlConfig(this.getServletContext().getRealPath("/"));
    	List l = doa.GetVisualProvidersStatus(); 
    	//out.print("registros "+l.size());
    	
		
		
		
	Element oai_provider = null;
		
	Element shortName = null;
	Element schedule_status = null;
	 
  	
  	
  	
  	    %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Scheduler</title>

		<title>DataTables example</title>
		<link type="text/css" href="css/styles.css" rel="stylesheet"/>
		<style type="text/css" title="currentStyle">
			
			@import "DataTables/media/css/demo_table.css";
		</style>
		<script type="text/javascript">
		
		function Validate(form){ 
			
			
			if(form.listProviders.checked){//este if es para cuando el usuario hace un filtrado de tal forma que solo queda un elemento  y no podemos tratarlo con un array
				Reset(form);
				return true;
			}
			
			
			for(var i = 0; i < form.listProviders.length; i++){ 
				if(form.listProviders[i].checked){
				Reset(form);
				return true;
				}
			}
			alert('Debes seleccionar al menos un Repositorio'); 
			return false; 
			}
		
		function Reset(form){
				form.action = 'servlet/SchedulerWeb?sched_option=reset_scheduler_selected';
				form.submit();				
		}
		
		 
		
		
		</script>
		<script type="text/javascript" language="javascript" src="DataTables/media/js/jquery.js"></script>
		<script type="text/javascript" language="javascript" src="DataTables/media/js/jquery.dataTables.js"></script>
		<script type="text/javascript" charset="utf-8">
			$(document).ready(function() {
				$('#example').dataTable();
			} );
		</script>

</head>
	<body  id="dt_exampleX">
		<div  id="container">
			<div class="full_width big"></div>
			<div id=title_section>Programador de recoleccion</div>
			</br>
				<div  id="demo">
<form name="control" method="post" action="">
  <table  cellpadding="0" cellspacing="0" border="0" class="display" id="example">
    <thead>
    <tr>
    
    		<th>Repositorio</th>
			<th>Estado</th>
			<th>Acci�n</th>
			
			

    </tr>
    </thead>
    	<tbody>
    <%
    
for(int j = 0; j<l.size();j++){
		  		
  		oai_provider=(Element)l.get(j);

  			shortName=oai_provider.getChild("shortName");
  			schedule_status=oai_provider.getChild("schedule_status");
			
  				    			
          out.print("<tr class=\"gradeU\">");
          out.print("<td align=\"center\">"+shortName.getText()+"</td>");
          out.print("<td align=\"center\">"+schedule_status.getText()+"</td>");
          out.print("<td align=\"center\">"+"<input type=\"checkbox\"  name=\"listProviders\" value=\""+shortName.getText()+"\">"+"</td>");
          out.print("</tr>");
		  
          
      }    
    
    %>
    </tbody>
	<tfoot>
		<tr>
      		
      		<th>Repositorio</th>
			<th>Estado</th>
			<th>Acci�n</th>

		
		</tr>
	</tfoot>
	
</table>
  <p>&nbsp;</p>
  <table align="center" width="300" border="0">
      <tr>
        <td><input type="button" name="Actualizar" id="Actualizar" value="Actualizar" onClick="window.location.reload(true);"></td>
        <td><input type="button" name="Resetear" id="Resetear" value="Resetear" onClick="window.location='servlet/SchedulerWeb?sched_option=reset_sc_status'"></td>
        <td><input type="button" name="Resetear_sel" id="Resetear_sel" value="Resetear Seleccionados" onClick="Validate(this.form)"></td>
        <td><input type="button" name="Ejecutar" id="Ejecutar" value="Ejecutar" onClick="window.location='servlet/SchedulerWeb?sched_option=ejecutar_scheduler'"></td>
        
      </tr>
    </table>
  </form>
</body>
</html>