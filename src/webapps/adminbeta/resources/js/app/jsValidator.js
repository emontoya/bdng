
$(function(){
	
	$(document).on("click",".moreInfo", function(){
		
		//$(this).siblings('.ListLinks').toggle();
		var content = $(this).siblings('.ListLinks').html();
		$("#contentPopup").html(content);
		$(".modal").show();
		$(".modal").draggable();
		//$( ".modal" ).resizable();
		
	});
	
	$(document).on("click","#cboxClose", function(){

		$(".modal").hide();
	});
	
});

function validarTodos(){
	init_check_validator();
	$.get("ValidatorRedaWeb", { option: "validateAll" } );
}

var idInterval;

function init_check_validator(){
	console.log('entro al initcheck');
	var interval = 1000 ;
	idInterval = setInterval(ajax_call_validator, interval);
}

var ajax_call_validator = function check_progress(){
	
	
	console.log('entro al check progress');
	

	var dato = "dato";
	var url = "ValidatorRedaWeb?option=check_progress";



	loggeando  = $.ajax({
		type: "POST",
		data: {dato: dato},
		url: url,
		timeout: 25000,
		dataType: "html",
		beforeSend: function(){        	
		},
		complete: function(){
		},
		success: function( data ) {
			//alert("data "+data)

			var dataProvidersCompletos  = $(data).find("#dataProvidersCompletos").html();
			var dataProvidersSize  = $(data).find("#dataProvidersSize").html();
//			percentage = Math.floor((dataProvidersCompletos *100)/dataProvidersSize);
			percentage = Math.floor(dataProvidersCompletos);
			


//			$("#result").text("Completados: "+dataProvidersCompletos+" de  "+dataProvidersSize+" Repositorios "+percentage+"%");
			$( "#progressbar" ).progressbar({
				value: percentage
			});
			
			console.log(percentage);

			if(percentage==100){
				clearInterval(idInterval);
				setTimeout(Reload(),50000); // actualiza la pagina cuando termina la recoleccion
			}


		},
		error: function(result){
			if(result.statusText != "abort"){
				alert("Problemas en el servidor, Intentelo otra vez en unos minutos.");
			}
		}
	});
	return false;
};
