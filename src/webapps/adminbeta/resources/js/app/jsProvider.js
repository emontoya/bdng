	var req;

	
	$(document).ready(function(){	
		
//		$("#Municipio").chosen(); //no usado por ahora
		
//		console.log("Carga al cargar Provider.jsp");
		
		$("#Departamento").onchange = function() {
			console.log("cambio");
		};
		
		$("#Departamento").change( function(){
			change_municipio(); 
			$("#Municipio").trigger("liszt:updated");
			
			
		});
	
	});
	
	function cargarMunicipios() {
		change_municipio(); 
		$("#Municipio").trigger("liszt:updated");
	}
	
	function cargarContenido() {
	   
	   var urlbase = document.getElementById("urlbase");
	   var url = "servlet/ProviderWeb?prov_opcion=consultar_oaiprotocol&urlbase=" + encodeURIComponent(urlbase.value);
	   if (typeof XMLHttpRequest != "undefined") {
	       req = new XMLHttpRequest();
	   } else if (window.ActiveXObject) {
	       req = new ActiveXObject("Microsoft.XMLHTTP");
	   }
	   req.open("GET", url, true);
	   req.onreadystatechange = callback;
	   req.send(null);
	}



	function callback() {
	    if (req.readyState == 4) {
	        if (req.status == 200) {
	        	
	        	
	        	parseMessage();
	        }
	    }
	}
	
	function parseMessage() {
		 var adminEmail = req.responseXML.getElementsByTagName("adminEmail")[0];
		 var repositoryName = req.responseXML.getElementsByTagName("repositoryName")[0];
		// var description = req.responseXML.getElementsByTagName("description")[0];
		 
		 
		 document.getElementById('emailadmin').value =adminEmail.childNodes[0].nodeValue;
		 document.getElementById('repname').value =repositoryName.childNodes[0].nodeValue;
		// document.getElementById('descripcion').value =description.childNodes[0].nodeValue;
		 
		}
	
	//funcion que se encarga de checkear si el shortname existe por medio de Ajax 
	function check_provider() {
		       					
	   var shortname = document.getElementById("shortname");
	   var url = "servlet/ProviderWeb?prov_opcion=check_provider&shortname=" + encodeURIComponent(shortname.value);
	   if (typeof XMLHttpRequest != "undefined") {
	       req = new XMLHttpRequest();
	   } else if (window.ActiveXObject) {
	       req = new ActiveXObject("Microsoft.XMLHTTP");
	   }
	   req.open("GET", url, true);
	   req.onreadystatechange = callback2;
	   req.send(null);
	}
	
	
	
	function callback2() {
	    if (req.readyState == 4) {
	        if (req.status == 200) {
	        		
	        	var Result = "<span style=\"color:#f00\">"+req.responseText+"</span>"
	        	document.getElementById('Result').innerHTML = Result;
	           	
	        }
	    }
	}
	function List() {
			
		   
		   var urlbase = document.getElementById("urlbase");
		   var url = "servlet/ProviderWeb?prov_opcion=ListMetadataFormats&urlbase="+encodeURIComponent(urlbase.value);
		   if (typeof XMLHttpRequest != "undefined") {
		       req = new XMLHttpRequest();
		   } else if (window.ActiveXObject) {
		       req = new ActiveXObject("Microsoft.XMLHTTP");
		   }
		   req.open("GET", url, true);
		   req.onreadystatechange = callback3;
		   req.send(null);
		}
		
		//
		
		function callback3() {
		    if (req.readyState == 4) {
		        if (req.status == 200) {

		        	iterator = req.responseXML.getElementsByTagName("metadataFormat");

					var inner ="";
					for (i=0;i<iterator.length;i++)
			    	  {
						var metadatos=iterator[i].getElementsByTagName("metadataPrefix");
						var valor =metadatos[0].firstChild.nodeValue;
						if((jQuery.inArray(valor, arrayJs)!=-1)){
					 	inner = inner+"<option value='"+valor+"'>"+valor+"</option> ";
						}
					
			      		}
					if(iterator.length==0) //si el Identify falla solo ponemos los del archivo ListMetadataPrefix
						
						for (var i in arrayJs)
						{
							inner = inner+"<option value='"+arrayJs[i]+"'>"+arrayJs[i]+"</option> ";
							
						}
			
					

					//document.getElementById('metadataprefix').innerHTML = inner; // esta linea no funciona par IE por tal motivo se hizo uso de la funcion select_innerHTML
					select_innerHTML(document.getElementById("metadataprefix_oai"),inner);
		        }
		    }
		}

	
	function select_innerHTML(objeto,innerHTML){
		/******
		* select_innerHTML - corrige o bug do InnerHTML em selects no IE
		* Veja o problema em: http://support.microsoft.com/default.aspx?scid=kb;en-us;276228
		* Vers�o: 2.1 - 04/09/2007
		* Autor: Micox - N�iron Jos� C. Guimar�es - micoxjcg@yahoo.com.br
		* @objeto(tipo HTMLobject): o select a ser alterado
		* @innerHTML(tipo string): o novo valor do innerHTML
		*******/
		
		    objeto.innerHTML = ""
		    var selTemp = document.createElement("micoxselect")
		    var opt;
		    selTemp.id="micoxselect1"
		    document.body.appendChild(selTemp)
		    selTemp = document.getElementById("micoxselect1")
		    selTemp.style.display="none"
		    if(innerHTML.toLowerCase().indexOf("<option")<0){//se n�o � option eu converto
		        innerHTML = "<option>" + innerHTML + "</option>"
		    }
		    innerHTML = innerHTML.toLowerCase().replace(/<option/g,"<span").replace(/<\/option/g,"</span")
		    selTemp.innerHTML = innerHTML
		      
		    
		    for(var i=0;i<selTemp.childNodes.length;i++){
		  var spantemp = selTemp.childNodes[i];
		  
		        if(spantemp.tagName){     
		            opt = document.createElement("OPTION")
		    
		   if(document.all){ //IE
		    objeto.add(opt)
		   }else{
		    objeto.appendChild(opt)
		   }       
		    
		   //getting attributes
		   for(var j=0; j<spantemp.attributes.length ; j++){
		    var attrName = spantemp.attributes[j].nodeName;
		    var attrVal = spantemp.attributes[j].nodeValue;
		    if(attrVal){
		     try{
		      opt.setAttribute(attrName,attrVal);
		      opt.setAttributeNode(spantemp.attributes[j].cloneNode(true));
		     }catch(e){}
		    }
		   }
		   //getting styles
		   if(spantemp.style){
		    for(var y in spantemp.style){
		     try{opt.style[y] = spantemp.style[y];}catch(e){}
		    }
		   }
		   //value and text
		   opt.value = spantemp.getAttribute("value")
		   opt.text = spantemp.innerHTML
		   //IE
		   opt.selected = spantemp.getAttribute('selected');
		   opt.className = spantemp.className;
		  } 
		 }    
		 document.body.removeChild(selTemp)
		 selTemp = null
		}
	
	// esta funcion inicializa el estado del select collection, para que sea visible o no
	// ademas de inicializar tambien se usa en ejecucion para cambiar de estado
	function init(form) {
		console.log("entro a init");
		console.log(form);
		var style = document.getElementById('collection').style;
		if (form.staticCollection.checked) {
			style.visibility = "visible"; 
		} else {
			style.visibility = "hidden";
		}
		
		
	}
	//con esta funcion lo que vamos es a generar dinamicamte la lista de los municipios, una ves se selecciona el departamento
	// pendiente: podemos hacer una verificacion de que al guardar verifiquemos que el departamento no sea 0, osea no lo han seleccionado
	
	function change_municipio() {
		
		console.log("cambio de municipio");
		var mun 
	   	mun = document.form1.Departamento[document.form1.Departamento.selectedIndex].value
	   	
	   	//el siguiente while lo que hace es que si el departamento tiene espacios, los cambia por _ para poder asignarlo al vector correspondiente
	    while (mun.toString().indexOf(" ") != -1){
	    	   mun = mun.toString().replace(" ","_");
	    }
	//	alert(mun);

	   	
	   	var amazonas = new Array("el encanto","la chorrera","la pedrera","la victoria","leticia","miriti-parana","puerto alegria","puerto arica","puerto narino","puerto santander","tarapaca")
	   	var antioquia=new Array("abejorral","abriaqui","alejandria","amaga","amalfi","andes","angelopolis","angostura","anori","antioquia","anza","apartado","arboletes","argelia","armenia","barbosa","bello","belmira","betania","betulia","bolivar","brice�o","buritica","caceres","caicedo","caldas","campamento","ca�asgordas","caracoli","caramanta","carepa","carmen de viboral","carolina del principe","caucasia","chigorodo","cisneros","cocorna","concepcion","concordia","copacabana","dabeiba","donmatias","ebejico","el bagre","el pe�ol","el retiro","entrerrios","envigado","fredonia","frontino","giraldo","girardota","gomez plata","granada","guadalupe","guarne","guatape","heliconia","hispania","itag�i","ituango","jardin","jerico","la ceja","la estrella","la pintada","la union","liborina","maceo","marinilla","medellin","montebello","murindo","mutata","narino","nechi","necocli","olaya","peque","pueblorrico","puerto berrio","puerto nare","puerto triunfo","remedios","rionegro","sabanalarga","sabaneta","salgar","san andres de cuerquia","san carlos","san francisco","san jeronimo","san jose de la monta�a","san juan de uraba","san luis","san pedro de los milagros","san pedro de uraba","san rafael","san roque","san vicente","santa barbara","santa rosa de osos","santo domingo","santuario","segovia","sonson","sopetran","tamesis","taraza","tarso","titiribi","toledo","turbo","uramita","urrao","valdivia","valparaiso","vegachi","venecia","vigia del fuerte","yali","yarumal","yolombo","yondo","zaragoza")
		var arauca=new Array("arauca","arauquita","cravo norte","fortul","puerto rondon","saravena","tame")
		var atlantico=new Array("barranquilla","baranoa","campo de la cruz","candelaria","galapa","juan de acosta","luruaco","malambo","manati","palmar de varela","piojo","polonuevo","ponedera","puerto colombia","repelon","sabanagrande","sabanalarga","santa lucia","santo tomas","soledad","suan","tubara","usiacuri")
		var bolivar=new Array("achi","altos del rosario","arenal","arjona","arroyohondo","barranco de loba","brazuelo de papayal","calamar","cantagallo","el carmen de bolivar","cartagena de indias","cicuco","clemencia","cordoba","el guamo","el pe�on","hatillo de loba","magangue","mahates","margarita","maria la baja","montecristo","morales","norosi","pinillos","regidor","rio viejo","san cristobal","san estanislao","san fernando","san jacinto","san jacinto del cauca","san juan nepomuceno","san martin de loba","san pablo","san pablo norte","santa catalina","santa cruz de mompox","santa rosa","santa rosa del sur","simiti","soplaviento","talaigua nuevo","tiquisio","turbaco","turbana","villanueva","zambrano")
		var boyaca=new Array("almeida","aquitania","arcabuco","belen","berbeo","beteitiva","boavita","boyaca","brice�o","buenavista","busbanza","caldas","campohermoso","cerinza","chinavita","chiquinquira","chiquiza","chiscas","chita","chitaraque","chivata","chivor","cienega","combita","coper","corrales","covarachia","cubara","cucaita","cuitiva","duitama","el cocuy","el espino","firavitoba","floresta","gachantiva","gameza","garagoa","guacamayas","guateque","guayata","g�ican","iza","jenesano","jerico","la capilla","la uvita","la victoria","labranzagrande","macanal","maripi","miraflores","mongua","mongui","moniquira","motavita","muzo","nobsa","nuevo colon","oicata","otanche","pachavita","paez","paipa","pajarito","panqueba","pauna","paya","paz de rio","pesca","pisba","puerto boyaca","quipama","ramiriqui","raquira","rondon","saboya","sachica","samaca","san eduardo","san jose de pare","san luis de gaceno","san mateo","san miguel de sema","san pablo de borbur","santa maria","santa rosa de viterbo","santa sofia","santana","sativanorte","sativasur","siachoque","soata","socha","socota","sogamoso","somondoco","sora","soraca","sotaquira","susacon","sutamarchan","sutatenza","tasco","tenza","tibana","tibasosa","tinjaca","tipacoque","toca","tog�i","topaga","tota","tunja","tunungua","turmeque","tuta","tutaza","umbita","ventaquemada","villa de leyva","viracacha","zetaquira")
		var caldas=new Array("aguadas","anserma","aranzazu","belalcazar","chinchina","filadelfia","la dorada","la merced","manizales","manzanares","marmato","marquetalia","marulanda","neira","norcasia","pacora","palestina","pensilvania","riosucio","risaralda","salamina","samana","san jose","supia","victoria","villamaria","viterbo")
		var caqueta=new Array("albania","belen de los andaquies","cartagena del chaira","curillo","el doncello","el paujil","florencia","la monta�ita","morelia","puerto milan","puerto rico","san jose del fragua","san vicente del caguan","solano","solita","valparaiso")
		var casanare=new Array("aguazul","hato corozal","chameza","pore","la salina","sacama","paz de ariporo","monterrey","orocue","mani","san luis de palenque","trinidad","villanueva","yopal","recetor","sabanalarga","tamara","nunchia","tauramena")
		var cauca=new Array("buenos aires","caloto","corinto","guachene","miranda","padilla","puerto tejada","santander de quilichao","suarez","villa rica","cajibio","el tambo","la sierra","morales","piendamo","popayan","rosas","sotara","timbio","almaguer","argelia","balboa","bolivar","florencia","la vega","mercaderes","patia","piamonte","san sebastian","santa rosa","sucre","guapi","lopez","timbiqui","caldono","inza","jambalo","paez","purace","silvia","toribio","totoro")
		var cesar=new Array("aguachica","astrea","becerril","bosconia","chimichagua","chiriguana","codazzi","curumani","el copey","san antonio de el paso del adelantado","gamarra","gonzalez","la gloria","la jagua de ibirico","la paz","manaure balcon del cesar","pailitas","pelaya","pueblo bello","rio de oro","san alberto","san diego","san martin","tamalameque","valledupar")
		var choco=new Array("acandi","alto baudo","atrato","bagado","bahia solano","bajo baudo","bojaya","canton de san pablo","el carmen de atrato","certegui","condoto","el carmen del darien","istmina","jurado","litoral de san juan","lloro","medio atrato","medio baudo","medio san juan","novita","nuqui","quibdo","riosucio","rio iro","rio quito","san jose del palmar","sipi","tado","unguia","union panamericana")
		var cordoba=new Array("ayapel","buenavista","canalete","cerete","chima","chinu","cienaga de oro","cotorra","la apartada","los cordobas","momil","mo�itos","montelibano","monteria","planeta rica","pueblo nuevo","puerto escondido","puerto libertador","purisima","sahagun","san andres de sotavento","san antero","san bernardo del viento","san carlos","san jose de ure","san pelayo","santa cruz de lorica","tierralta","tuchin","valencia")
		var cundinamarca=new Array("agua de dios","alban","anapoima","anolaima","apulo","arbelaez","beltran","bituima","bogota","bojaca","cabrera","cachipay","cajica","caparrapi","caqueza","carmen de carupa","chaguani","chia","chipaque","choachi","choconta","cogua","cota","cucunuba","el colegio","el pe�on","el rosal","facatativa","fomeque","fosca","funza","fuquene","fusagasuga","gachala","gachancipa","gacheta","gama","girardot","granada","guacheta","guaduas","guasca","guataqui","guatavita","guayabal de siquima","guayabetal","gutierrez","jerusalen","junin","la calera","la mesa","la palma","la pe�a","la vega","lenguazaque","macheta","madrid","manta","medina","mosquera","narino","nemocon","nilo","nimaima","nocaima","pacho","paime","pandi","paratebueno","pasca","puerto salgar","puli","quebradanegra","quetame","quipile","ricaurte","san antonio del tequendama","san bernardo","san cayetano","san francisco","san juan de rioseco","sasaima","sesquile","sibate","silvania","simijaca","soacha","sopo","subachoque","suesca","supata","susa","sutatausa","tabio","tausa","tena","tenjo","tibacuy","tibirita","tocaima","tocancipa","topaipi","ubala","ubaque","ubate","une","�tica","venecia (ospina perez)","vergara","viani","villagomez","villapinzon","villeta","viota","yacopi","zipacon","zipaquira")
		var guainia=new Array("barrancominas","cacahual","inirida","la guadalupe","mapiripana","morichal nuevo","pana pana","puerto colombia","san felipe")
		var guaviare=new Array("calamar","el retorno","miraflores","san jose del guaviare")
		var huila=new Array("aipe","algeciras","baraya","campoalegre","colombia","hobo","�quira","neiva","palermo","rivera","santa maria","tello","teruel","villavieja","yaguara","altamira","agrado","garzon","gigante","pital","guadalupe","tarqui","suaza","la argentina","la plata","nataga","paicol","tesalia","acevedo","elias","isnos","oporapa","palestina","pitalito","saladoblanco","san agustin","timana")
		var la_guajira=new Array("albania","dibulla","maicao","manaure","riohacha","uribia","barrancas","distraccion","el molino","fonseca","hatonuevo","la jagua del pilar","san juan del cesar","urumita","villanueva")
		var magdalena=new Array("algarrobo","aracataca","ariguani","cerro de san antonio","chibolo","cienaga","el banco","el pi�on","el reten","fundacion","guamal","nueva granada","pedraza","piji�o del carmen","pivijay","plato","pueblo viejo","remolino","sabanas de san angel","salamina","san sebastian de buenavista","santa ana","santa barbara de pinto","santa marta","san zenon","sitionuevo","tenerife","zapayan","zona bananera")
		var meta=new Array("acacias","barranca de upia","cabuyaro","castilla la nueva","cubarral","cumaral","el calvario","el castillo","el dorado","fuente de oro","granada","guamal","la macarena","la uribe","lejanias","mapiripan","mesetas","puerto concordia","puerto gaitan","puerto lleras","puerto lopez","puerto rico","restrepo","san carlos de guaroa","san juan de arama","san juanito","san martin","villavicencio","vista hermosa")
		var narino=new Array("barbacoas","el charco","francisco pizarro","la tola","mag�i","mosquera","olaya herrera","roberto payan","santa barbara","tumaco","aldana","contadero","cordoba","cuaspud","cumbal","funes","guachucal","gualmatan","iles","ipiales","potosi","puerres","pupiales","alban","arboleda","belen","colon","el rosario","el tablon de gomez","la cruz","la union","leiva","policarpa","san bernardo","san lorenzo","san pablo","san pedro de cartago","taminango","buesaco","chachag�i","consaca","el tambo","el pe�ol","la florida","narino","san juan de pasto","sandona","tangua","yacuanquer","ancuya","cumbitara","guaitarilla","imues","la llanada","linares","los andes","mallama","ospina","providencia","ricaurte","tuquerres","samaniego","santacruz","sapuyes")
		var norte_de_santander=new Array("abrego","cachira","convencion","el carmen","la esperanza","hacari","la playa de belen","oca�a","san calixto","teorama","bucarasica","el tarra","sardinata","tibu","cucuta","el zulia","los patios","puerto santander","san cayetano","villa del rosario","cacota","chitaga","mutiscua","pamplona","pamplonita","santo domingo de silos","arboledas","cucutilla","gramalote","lourdes","salazar de las palmas","santiago","villa caro","bochalema","chinacota","durania","herran","labateca","ragonvalia","toledo")
		var putumayo=new Array("colon","mocoa","orito","puerto asis","puerto caicedo","puerto guzman","puerto leguizamo","san francisco","san miguel","santiago","sibundoy","valle del guamez","villa garzon")
		var quindio=new Array("armenia","buenavista","calarca","circasia","cordoba","filandia","genova","la tebaida","montenegro","pijao","quimbaya","salento")
		var risaralda=new Array("apia","balboa","belen de umbria","dosquebradas","guatica","la celia","la virginia","marsella","mistrato","pereira","pueblo rico","quinchia","santa rosa de cabal","santuario")
		var santander=new Array("aguada","albania","aratoca","barbosa","barichara","barrancabermeja","betulia","bolivar","bucaramanga","cabrera","california","capitanejo","carcasi","cepita","cerrito","charala","charta","chima","chipata","cimitarra","concepcion","confines","contratacion","coromoro","curiti","el carmen de chucuri","el guacamayo","el pe�on","el playon","encino","enciso","florian","floridablanca","galan","gambita","giron","guaca","guadalupe","guapota","guavata","g�epsa","hato","jesus maria","jordan","la belleza","la paz","landazuri","lebrija","los santos","macaravita","malaga","matanza","mogotes","molagavita","ocamonte","oiba","onzaga","palmar","palmas del socorro","paramo","piedecuesta","pinchote","puente nacional","puerto parra","puerto wilches","rionegro","sabana de torres","san andres","san benito","san gil","san joaquin","san jose de miranda","san miguel","san vicente","santa barbara","santa helena del opon","simacota","socorro","suaita","sucre","surata","tona","valle de san jose","velez","vetas","villanueva","zapatoca")
		var tolima=new Array("alpujarra","alvarado","ambalema","anzoategui","armero","ataco","cajamarca","carmen de apicala","casabianca","chaparral","coello","coyaima","cunday","dolores","espinal","falan","flandes","fresno","guamo","herveo","honda","ibague","icononzo","lerida","libano","mariquita","melgar","murillo","natagaima","ortega","palocabildo","piedras","planadas","prado","purificacion","rioblanco","roncesvalles","rovira","salda�a","san antonio","san luis","santa isabel","suarez","valle de san juan","venadillo","villahermosa","villarrica")
		var valle_del_cauca=new Array("alcala","alpujarra","alvarado","ambalema","andalucia","ansermanuevo","anzoategui","argelia","armero","ataco","bolivar","buenaventura","buga","bugalagrande","caicedonia","cajamarca","cali","candelaria","carmen de apicala","cartago","casabianca","chaparral","coello","coyaima","cunday","dagua","darien","dolores","el �guila","el cairo","el cerrito","el dovio","espinal","falan","flandes","florida","fresno","ginebra","guacari","guamo","herveo","honda","ibague","icononzo","jamundi","la cumbre","la union","la victoria","lerida","libano","mariquita","melgar","murillo","natagaima","obando","ortega","palmira","palocabildo","piedras","planadas","pradera","prado","purificacion","restrepo","rioblanco","riofrio","roldanillo","roncesvalles","rovira","salda�a","san antonio","san luis","san pedro","santa isabel","sevilla","suarez","toro","trujillo","tulua","ulloa","valle de san juan","venadillo","versalles","vijes","villahermosa","villarrica","yotoco","yumbo","zarzal")
		var vaupes=new Array("caruru","mitu","pacoa","papunaua","taraira","yavarate")
		var vichada=new Array("cumaribo","la primavera","puerto carre�o","santa rosalia")
		var sucre=new Array("buenavista","caimito","chalan","coloso","corozal","el roble","galeras","guaranda","la union","los palmitos","majagual","morroa","ovejas","palmito","sampues","san benito abad","san juan de betulia","san marcos","san onofre","san pedro","since","sincelejo","sucre","tolu","toluviejo")
		var san_andres_y_providencia=new Array("providencia","santa catalina")
		
		//alert(mun)
		if (mun != 0) {
			
			
	      	 
	      	 mis_mun=eval(mun) //aquie estan todos los municipios del determinado departamento
	      	 
	      	 //alert(mis_mun) 
	      	 //calculo el numero de municipios 
	      	 num_mun = mis_mun.length 
	      	 //marco el n�mero de provincias en el select 
	      	 document.form1.Municipio.length = num_mun 
	      	 //para cada Municipio del array, la introduzco en el select 
	      	 for(i=0;i<num_mun;i++){ 
	         	 document.form1.Municipio.options[i].value=mis_mun[i] 
	         	 document.form1.Municipio.options[i].text=mis_mun[i] 
	      	 }	
	   	}else{ 
	      	 //si no hab�a Muninicipio seleccionado, elimino los Muninipios del select 
	      	 document.form1.Municipio.length = 1 
	      	 //coloco un gui�n en la �nica opci�n que he dejado 
	      	 document.form1.Municipio.options[0].value = "-" 
	      	 document.form1.Municipio.options[0].text = "-" 
	   	} 
	   	//marco como seleccionada la opci�n primera de Municipio
	   	document.form1.Municipio.options[0].selected = true 
	
	    
	}
	
	function loadXMLDoc(dname)
	{
		if (window.XMLHttpRequest)
		  {
		  xhttp=new XMLHttpRequest();
		  }
		else
		  {
		  xhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xhttp.open("GET",dname,false);
		xhttp.send();
		return xhttp.responseXML;
	} 
	
	function cargarRutaXMLProvider(){
		console.log("funciona cargar");
	}
	
	function cargarXMLProvider(){
		console.log("funciona importar");
		console.log($("#providerXMLInput").val());
		xmlDoc=loadXMLDoc("file:///local/home/ctorres/bdng/bdng-2.0-20131024/conf/oai_providers.xml");
	}