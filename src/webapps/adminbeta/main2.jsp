<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="org.bdng.web.Utils"%>
<%
	HttpSession sesion = request.getSession();
	boolean login = false;
	if (sesion.getAttribute("type") != null) {
		login = true;

	}
%>
<%
	Utils u = new Utils(this.getServletContext().getRealPath("/"));
	u.uploadConf("serverconfig"); //se sube el archivo ServerConfig.xml a eXist
%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Modulo de Administración</title>

<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link type="text/css" href="resources/css/app/basic.css"
	rel="stylesheet" />

<link type="text/css" href="resources/bootstrap/css/bootstrap.css"
	rel="stylesheet">
<link type="text/css"
	href="resources/bootstrap/css/bootstrap-responsive.css"
	rel="stylesheet">

</head>
<body>
	<div class="container">
		<div class="row">
			<div id="header-top"></div>
		</div>
		<table width="100%" border="0">
			<tr>
				<td class="container-windows" align="center"><div
						id=title_section>Modulo de Administración</div></td>
			</tr>
		</table>

		<%
			if (!login) {
		%>
		<div id="containerLogin">
			<form id="loginForm" action="index.jsp" class="box login" method="POST">
				<fieldset class="boxBody">
					<label>Nombre de Usuario</label> <input id="username"
						name="username" type="text" tabindex="1" placeholder="username"
						required value="" onkeypress="Clear();" onfocus="Clear();">

					<label><a href="#frmRecuperar" class="rLink" rel="facebox">Olvido
							su contraseña?</a>Contraseña</label> <input id="password" name="password"
						type="password" tabindex="2" placeholder="password" required
						value="" onkeypress="Clear();" onfocus="Clear();"> <a
						id="redirect" style="display: none;" href="bdcol_admin_menu.jsp"
						target="menu"></a>
				</fieldset>
				<footer> <!-- 
		 	<div id="keeplogin">
			<label><input type="checkbox" tabindex="3">Mantener logueado</label>
			</div>
			 -->
				<div id="wrongLogin" style='display: none'>Usuario o
					contraseña invalida</div>
				<div id="inactive" style='display: none'>Usuario Inactivo</div>
				<input type="button" class="btnLogin" value="Entrar" tabindex="4"
					onclick="login_admin()"> </footer>
			</form>


			<div id="Recuperar" style='display: none'>
				<form name="frmRecuperar" id="frmRecuperar" method="post"
					enctype="multipart/form-data"
					style='width: 470px; margin: 0 auto; padding: 10px; background: #fff; text-align: center;'>

					<p>Ingrese el email con el que se registro.</p>
					<br />
					<table id="tableData" class="block" cellpadding="5" width="70%"
						align="center">


						<tr>
							<td align="left"><label>Email de usuario:</label> <input
								name="txtMail" id="txtMail" type="text" size="20" /></td>
						</tr>
						<tr>
							<td coslpan="2"><input name="action" id="recuperar"
								type="submit" text="Enviar" size="20" value="Enviar"
								onclick="recuperar()" /> <input name="action" id="btnRegresar"
								type="button" text="Regresar" size="20" value="Regresar"
								onClick="window.location.href='/adminbootstrap/main.jsp';" /></td>
							<td coslpan="2"></td>
						</tr>

						<tr>
							<td colspan="2"><br /></td>
						</tr>

					</table>


				</form>
			</div>
		</div>


		<%
			}
		%>

		<div class="row">
			<div id="footer-bottom"></div>
		</div>
	</div>

	<script type="text/javascript" src="resources/js/lib/jquery.min.js"></script>
	<script type="text/javascript" src="resources/js/app/jsLogin.js"
		charset="utf-8"></script>
	<script type="text/javascript"
		src="resources/bootstrap/js/bootstrap.js"></script>

</body>


</html>