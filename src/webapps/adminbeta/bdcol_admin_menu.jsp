<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
    <%
    HttpSession sesion = request.getSession();
    boolean login = false;
    
    String type = "";
    String name = "";
    

    type = (String)sesion.getAttribute("type");
    name = (String)sesion.getAttribute("name");
	//out.print("type"+type);
    //ok = (String)sesion.getAttribute("login");
	if (sesion.getAttribute("type") != null){
   		login = true;
   		
   	}
        
    %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Bdcol Admin Menu</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<link type="text/css" href="resources/css/app/basic.css" rel="stylesheet"/>
	
	<link type="text/css" href="resources/bootstrap/css/bootstrap.css" rel="stylesheet">
	<link type="text/css" href="resources/bootstrap/css/bootstrap-responsive.css" rel="stylesheet">

</head>
<body>

<div id="main" style="display: none">
<!-- 		<ul id="browser" class="filetree treeview"> -->
		
<!-- 		<ul> -->

  <%
	if (login && type.equals("admin")){
		//menu si la sesion existe
		
		%>
		
			<div class="span2 breadcrumb" id="menuAdmin">
			    <ul class="nav nav-list sidenav">
			    	<li class="nav-header">General</li>
			    	<li><a href="ServerConf.jsp" target="main_frame">Configuracion</a></li>
				    <li class="nav-header">Proveedores</li>
				    <li><a href="Provider.jsp" target="main_frame">Agregar</a></li>
					<li><a href="ListProviders.jsp" target="main_frame">Edicion</a></li>
					<li><a href="ListRepositories.jsp" target="main_frame">Lista </a></li>
					<li><a href="TreeRepositories.jsp" target="main_frame">Arbol </a></li>
					<li><a href="ListRepositoriesDetails.jsp" target="main_frame">Lista Detallada</a></li>
					<li class="nav-header">Datos</li>
				    <li><a href="ListValidations.jsp" target="main_frame">Validador</a></li>
					<li><a href="ListToPublish.jsp" target="main_frame">Aprobador</a></li>
					<li><a href="recolector.jsp" target="main_frame">Recolector</a></li>
					 <li class="closed"><span class="folder"><a href="Scheduler.jsp" target="main_frame">Programador</a> </span></li>
					<li><a href="Indexador.jsp" target="main_frame">Indexador</a></li>
					<li><a href="StatsForHarvester.jsp" target="main_frame">Estadisticas</a></li>
					<li class="divider"></li>
					<li><a onclick="logout()" href="logout.jsp" target="main_frame">Salir</a></li>
			    </ul>
			</div>
		
<!-- 		<li class="closed"><span class="folder"><a href="ServerConf.jsp" target="main_frame">Configuracion</a> </span></li> -->
<!-- 		<li class="closed"><span class="folder">Proveedores</a> </span> -->
		
<!-- 			<ul> -->
<!-- 				<li><span class="file"><a  href="Provider.jsp" target="main_frame">Agregar</a> </span></li> -->
<!-- 				<li><span class="file"><a  href="ListProviders.jsp" target="main_frame">Edicion</a> </span></li> -->
<!-- 				<li><span class="file"><a  href="ListRepositories.jsp" target="main_frame">Lista </a> </span></li> -->
<!-- 				<li><span class="file"><a  href="TreeRepositories.jsp" target="main_frame">Arbol </a> </span></li> -->
<!-- 				<li><span class="file"><a  href="ListRepositoriesDetails.jsp" target="main_frame">Lista Detallada </a> </span></li> -->
<!-- 			</ul> -->
<!-- 		</li> -->
<!-- 		<li class="closed"><span class="folder"><a href="ListValidations.jsp" target="main_frame">Validador</a> </span></li> -->
<!-- 		<li class="closed"><span class="folder"><a href="ListToPublish.jsp" target="main_frame">Aprobador</a> </span></li> -->
<!-- 		<li class="closed"><span class="folder"><a href="recolector.jsp" target="main_frame">Recolector</a> </span></li> -->
<!-- 		<!--  <li class="closed"><span class="folder"><a href="Scheduler.jsp" target="main_frame">Programador</a> </span></li>--> -->
<!-- 		<li class="closed"><span class="folder"><a href="Indexador.jsp" target="main_frame">Indexador</a> </span></li> -->
<!-- 		<li class="closed"><span class="folder"><a href="StatsForHarvester.jsp" target="main_frame">Estadisticas</a> </span></li> -->
<!-- 		<li class="closed"><span class="folder"><a onclick="logout()" href="logout.jsp" target="main_frame">Salir</a> </span></li> -->
		
		<%
		}
		
	else
		
		if (login && type.equals("adminRepo")){
			
			%>
		
			<li class="closed"><span class="folder"><a href="../bdcol/search/user/profile.xql" target="_blank">Mi Perfil</a> </span></li>
			<li class="closed"><span class="folder">Proveedores</a> </span>
			
				<ul>
					<li><span class="file"><a  href="Provider.jsp" target="main_frame">Agregar</a> </span></li>
					<li><span class="file"><a  href="ListProviders.jsp" target="main_frame">Edicion</a> </span></li>
				</ul>
			</li>
			<li class="closed"><span class="folder"><a href="ListValidations.jsp" target="main_frame">Validador</a> </span></li>
			<li class="closed"><span class="folder"><a href="ListToPublishUser.jsp" target="main_frame">Publicador</a> </span></li>
			<li class="closed"><span class="folder"><a onclick="logout()" href="logout.jsp" target="main_frame">Salir</a> </span></li>
			
			<%
		
			
			
		
			}else{
				// form de login si no existe la sesion.. 
				//out.print("form de login");
			}
	
%>
     
<!-- </ul> -->
<!-- </ul> -->
</div>

	<script type="text/javascript" src="resources/js/lib/jquery.min.js"></script>
	<script type="text/javascript" src="resources/bootstrap/js/bootstrap.js"></script>
	<script type="text/javascript" >
$(document).ready(function(){		
	
	$("#main").hide();
	$("#main").slideDown('slow');
	$("#menuAdmin").show();
// 	$("#browser").treeview();
	
	
	//verificamos session
	$('li a').click(function(e){
		//e.preventDefault();
		$.ajax({
			  url: 'check_session.jsp',
			  success: function(data) {
				var resp  = $(data).find("session").text();
				if(resp == 'true'){
					
				}
				else{
					window.top.location.href = "index.jsp"; //recarga todo el iframe contenedor 
				}
					
			    
			  }
			});
		
	});
	
	
});


function logout(){
	
	
	setTimeout(function() {
		//window.location.reload(true);
		$("#main").show();
		$("#main").slideUp('slow');
	},200);

}

</script>
</body>
</html>