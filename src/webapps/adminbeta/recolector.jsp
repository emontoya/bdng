<%@page import="java.util.Iterator"%>
<%@page import="org.bdng.harvmgr.dlConfig"%>
<%@page import="org.jdom2.Document"%>
<%@page import="java.util.List" %>
<%@page import="org.jdom2.Element"%>
<%@page import="org.jdom2.input.SAXBuilder" %>
<%@page import="org.bdng.harvmgr.harvester.DataProvider"%>
<%@page import="org.bdng.web.ProviderWeb"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
        
    <%    //verificamos session como "admin" sino, lo redirijimos al index.jsp
	HttpSession sesion = request.getSession();
    String user ="";
    String type = "";
    if(sesion.getAttribute("username")== null && sesion.getAttribute("type")== null) {
    	response.sendRedirect("errorSession.jsp");
    	
	}else{
		user = (String)sesion.getAttribute("username");
    	type = (String)sesion.getAttribute("type");
    		if(!user.equals("admin") || !type.equals("admin"))
    			response.sendRedirect("errorSession.jsp");
	} %>
    
    <%
    
    	
    	dlConfig doa = new dlConfig(this.getServletContext().getRealPath("/"));
    	List l = doa.GetVisualProvidersStatus(); 
    	//out.print("registros "+l.size());
    	
		
		
		
	Element oai_provider = null;
		
	Element shortName = null;
	Element harvest_count = null;
	Element last_harvest_date = null;
	Element harvest_status = null;
	Element last_id_file = null;
	Element active = null;
	 
  	
  	
  	
  	    %>

		<div  id="container">
			<div class="full_width big"></div>
			
			<div id="title_section" class="text-center">
				<h2>Recoleccion de metadatos de proveedores</h2>
			</div> 
				<div  id="demo">
<form name="control" id="control" method="post" action="">
<div align="right" > <input type="checkbox" id="checkAll" onchange="callAll()" /> Seleccionar todos</div>
  <table  cellpadding="0" cellspacing="0" border="0" class="display" id="example">
    <thead>
    <tr>
      		<th>Repositorio</th>
			<th>Cant Recolección</th>
			<th>Ult Recolección</th>
			<th>Est Recolección</th>
			<th>Ult Recolección</th>
			<th>Acción</th>
			
			

    </tr>
    </thead>
    	<tbody>
    <%
    
for(int j = 0; j<l.size();j++){
		  		
  		oai_provider=(Element)l.get(j);
  			
  			active=oai_provider.getChild("active");
  			
  			if(!active.getText().equals("0")) continue;

  			shortName=oai_provider.getChild("shortName");
  			harvest_count=oai_provider.getChild("harvest_count");
  			last_harvest_date=oai_provider.getChild("last_harvest_date");
  			harvest_status=oai_provider.getChild("harvest_status");
  			last_id_file=oai_provider.getChild("last_id_file");
  			active=oai_provider.getChild("active");
			
  				    			
          out.print("<tr class=\"gradeU\">");
          out.print("<td align=\"center\">"+shortName.getText()+"</td>");
          out.print("<td align=\"center\">"+harvest_count.getText()+"</td>");
          out.print("<td align=\"center\">"+last_harvest_date.getText()+"</td>");
          out.print("<td align=\"center\">"+harvest_status.getText()+"</td>");
          out.print("<td align=\"center\">"+last_id_file.getText()+"</td>");
          out.print("<td align=\"center\">"+"<input class=\"checkbox\" type=\"checkbox\"name=\"listProviders\"  value=\""+shortName.getText()+"\">"+"</td>");
          out.print("</tr>");
		  
          
      }    
    
    %>
    </tbody>
	<tfoot>
		<tr>
      		<th>Repositorio</th>
			<th>Cant Recolección</th>
			<th>Ult Recolección</th>
			<th>Est Recolección</th>
			<th>Ult Recolección</th>
			<th>Acción</th>

		
		</tr>
	</tfoot>
	<br>
</table>
<br>
<br>
		<div id="progress" style="width: 100%; margin: auto 0;">
	  		<div id="result"></div>
			<div id="progressbar"></div>
		</div>
</br>
  
    <table align="center" width="300" border="0">
      <tr>
        <td><input type="button" name="Actualizar" id="Actualizar" value="Actualizar" onclick="window.location='servlet/HarversterWeb?harv_opcion=refresh_providers'"></td>
        <td><input type="button" name="Resetear" id="Resetear" value="Resetear" onClick="window.location='servlet/HarversterWeb?harv_opcion=resetall_providers'"></td>
        <td><input type="button" name="Resetear_sel" id="Resetear_sel" value="Resetear Seleccionados" onclick="Validate(this.form)"></td>
        <td><input type="button" name="Iniciar" id="Iniciar" value="Iniciar" onclick="inciarTodos()"></td>
        <td><input type="button" name="Iniciar_sel" id="Iniciar_sel"value="Iniciar Seleccionados" onclick="inciarSeleccionados(this.form)"></td>
      <!--    <td><input type="button" name="Parar" id="Parar" value="Parar" onClick="window.location='servlet/HarversterWeb?harv_opcion=parar_todos'"></td> -->
      </tr>
    </table>
  </form>
 
 </div>
 </div>