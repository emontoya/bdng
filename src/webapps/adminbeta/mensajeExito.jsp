<%@ page language="java" contentType="text/html; charset=US-ASCII"
    pageEncoding="US-ASCII"%>
    <%    //verificamos session como "admin" o como "adminRepo" sino, lo redirijimos al index.jsp
	HttpSession sesion = request.getSession();
    String user ="";
    String type = "";
    String mensaje = "";
    String barraCargando = "";
    if(sesion.getAttribute("username")== null && sesion.getAttribute("type")== null) {
    	response.sendRedirect("main.jsp");
    	
	}else{
		user = (String)sesion.getAttribute("username");
    	type = (String)sesion.getAttribute("type");
    	mensaje = (String)sesion.getAttribute("message");
    	barraCargando = (String)sesion.getAttribute("barraCargando");
    		if(!type.equals("adminRepo") && !type.equals("admin"))
    		
    			response.sendRedirect("main.jsp");
	} %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
<title>Operacion exitosa</title>
</head>
<body>
	<div class="alert alert-success text-center"><i class="icon-ok"></i><strong> Operaci&oacute;n exitosa!</strong>
		<%if(mensaje == null || mensaje.equals("null") || mensaje.equals("")){ %>
			Los cambios se han guardado correctamente
		<%}else{ %>
			<%= mensaje %>
		<%} %>
	</div>
</body>
</html>
<% sesion.setAttribute("message", "null"); %>