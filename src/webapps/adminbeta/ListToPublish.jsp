<%@page import="java.util.Iterator"%>
<%@page import="org.bdng.harvmgr.dlConfig"%>
<%@page import="org.jdom2.Document"%>
<%@page import="java.util.List"%>
<%@page import="org.jdom2.Element"%>
<%@page import="org.jdom2.input.SAXBuilder"%>
<%@page import="org.bdng.harvmgr.harvester.DataProvider"%>
<%@page import="org.bdng.web.ProviderWeb"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	
	        
    <%    //verificamos session como "admin" sino, lo redirijimos al index.jsp
	HttpSession sesion = request.getSession();
    String user ="";
    String type = "";
    if(sesion.getAttribute("username")== null && sesion.getAttribute("type")== null) {
    	response.sendRedirect("errorSession.jsp");
    	
	}else{
		user = (String)sesion.getAttribute("username");
    	type = (String)sesion.getAttribute("type");
    		if(!user.equals("admin") || !type.equals("admin"))
    			response.sendRedirect("errorSession.jsp");
	} %>
   <% 

	
    dlConfig doa = new dlConfig(this.getServletContext().getRealPath("/"));
   	List l = doa.GetVisualProviders();

	DataProvider dp = new DataProvider(this.getServletContext().getRealPath("/"));
	

	
	Element oai_provider = null;
	
	Element shortName = null;
	Element repositoryName = null;
	Element baseURL = null;
	Element collection = null;
	Element metadataPrefix = null;
	Element protocol = null;
	Element owner = null;
  	
  	
  	
  	    %>

<div id="cargando" class="loadingDiv"></div>
	<div id="container">
		<div class="full_width big"></div>
		<div id="demo">
		<div id="title_section" class="text-center">
			<h2>Aprobador de proveedores</h2>
		</div> 
		<br />

			<table cellpadding="0" cellspacing="0" border="0" class="display"
				id="example">
				<thead>
					<tr>
						<th>Publicar</th>
						<th>Rechazar</th>
						<th>ID-rep</th>
						<th>Repositorio</th>
						<th>Resultado Validacion</th>
					</tr>
				</thead>
				<tbody>
					<%
					
					String active = "5";
    
for(int j = 0; j<l.size();j++){
		  		
  		oai_provider=(Element)l.get(j);
  			
  			shortName=oai_provider.getChild("shortName");
  			owner=oai_provider.getChild("owner");

  			active = dp.getActiveStatus(shortName.getText());
  		
			if(!active.equals("1")) continue;

  			
  			repositoryName=oai_provider.getChild("repositoryName");
  		//	baseURL=oai_provider.getChild("baseURL");
  			collection=oai_provider.getChild("collection");
  			metadataPrefix=oai_provider.getChild("metadataPrefix");
  			protocol=oai_provider.getChild("protocol");
			
  		    			
          out.print("<tr class=\"gradeU\">");
          out.print("<td class='elements'><A class='toltip' title='Click aqui para aprobar este repositorio' HREF=\"PublishWeb?action=allow&shortname="+shortName.getText()+"\"><img style='cursor:pointer;' src=\"images/btn_check.png\" width=\"20\" height=\"20\" border=\"0\"></A></td>");
          out.print("<td class='elements'><A class='toltip' title='Click aqui para rechazar este repositorio' HREF=\"PublishWeb?action=refuse&shortname="+shortName.getText()+"\"><img style='cursor:pointer;' src=\"images/btn_refused.png\" width=\"20\" height=\"20\" border=\"0\"></A></td>");
        		 
          
          
          out.print("<td>"+shortName.getText()+"</td>");
          out.print("<td>"+repositoryName.getText()+"</td>");
         //out.print("<td>"+baseURL.getText()+"</td>");
          out.print("<td> "+doa.GetScoreValidation(shortName.getText())+"</td>");
		 out.print("</tr>");
          
      }    
    
    %>
				</tbody>
				<tfoot>
					<tr>
						<th>Publicar</th>
						<th>Rechazar</th>
						<th>ID-rep</th>
						<th>Repositorio</th>
						<th>Resultado Validacion</th>
				</tr>
				</tfoot>
			</table>
</div>
</div>
