<%@page import="java.util.Iterator"%>
<%@page import="org.bdng.harvmgr.dlConfig"%>
<%@page import="org.jdom2.Document"%>
<%@page import="java.util.List" %>
<%@page import="org.jdom2.Element"%>
<%@page import="org.jdom2.input.SAXBuilder" %>
<%@page import="org.bdng.harvmgr.harvester.DataProvider"%>
<%@page import="org.bdng.web.ProviderWeb"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%
    
   	// this file is public, dont have session -luis
    
    dlConfig doa = new dlConfig(this.getServletContext().getRealPath("/"));
   	List l = doa.GetVisualProviders();
		
   	DataProvider dp = new DataProvider(this.getServletContext().getRealPath("/"));
	
	Element oai_provider = null;
	
	Element shortName = null;
	Element departamento = null;
	Element municipio = null;
	Element institution = null;
	Element repositoryName = null;
	Element urlhome = null;
	
	Element owner = null;
  	    %>

		<div  id="container">
			<div class="full_width big"></div>
			<div id="title_section" class="text-center">
				<h2>Listado de Repositorios</h2>
			</div> 
			<br />
				<div  id="demo">

  <table  cellpadding="0" cellspacing="0" border="0" class="display" id="example">
    <thead>
    <tr>
      		<th>Departamento</th>
			<th>Municipio</th>
			<th>Institucion</th>
			<th>Repositorio</th>

			

    </tr>
    </thead>
    	<tbody>
    <%
    		String active = "5";
    
    
for(int j = 0; j<l.size();j++){
		  		
  		oai_provider=(Element)l.get(j);
  		
			owner=oai_provider.getChild("owner");
			if(	!owner.getText().equals("admin")) continue;
			
			shortName=oai_provider.getChild("shortName");
  			active = dp.getActiveStatus(shortName.getText());
  	  		
			if(!active.equals("0")) continue;
  		
  			departamento=oai_provider.getChild("departamento");
  			municipio=oai_provider.getChild("municipio");
  			institution=oai_provider.getChild("institution");
  			repositoryName=oai_provider.getChild("repositoryName");
  			urlhome=oai_provider.getChild("urlhome");
  			
  			
  			
			
  		    			
          out.print("<tr class=\"gradeU\">");
          out.print("<td>"+departamento.getText()+"</td>");
          out.print("<td>"+municipio.getText()+"</td>");
          out.print("<td>"+institution.getText()+"</td>");
          out.print("<td><A target=\"_blank\" HREF=\""+urlhome.getText()+"\">"+repositoryName.getText()+"  </A></td>");
          out.print("</tr>");
          
      }    
    
    %>
    </tbody>
	
</table>

</div>
</div>
