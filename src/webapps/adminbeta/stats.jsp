<%@page import="java.util.TreeMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.bdng.harvmgr.dlConfig"%>
<%@page import="org.jdom2.Document"%>
<%@page import="java.util.List"%>
<%@page import="org.jdom2.Element"%>
<%@page import="org.jdom2.input.SAXBuilder"%>
<%@page import="org.bdng.harvmgr.harvester.DataProvider"%>
<%@page import="org.bdng.web.ProviderWeb"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%
	dlConfig doa = new dlConfig(this.getServletContext().getRealPath("/"));
	List l = doa.GetVisualProviders();

	Element oai_provider = null;
	
	Element shortName = null;
	Element departamento = null;
	Element municipio = null;
	Element institution = null;
	Element repositoryName = null;

	Element urlhome = null;
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
     "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
<title>Bdcol Chart Test</title>
<style type="text/css">
h1 { font-size: 2.0em; line-height: 1em; margin: 0 0 15px 0; color: #483D8B; text-shadow: 1px 1px 1px #FFFFFF; } 
</style>
<script type="text/javascript" src="js/jquery-1.6.1.min.js"></script>
<script type="text/javascript" src="js/jquery.gchart.js"></script>
<link rel="stylesheet" href="css/jquery-ui.css" type="text/css" media="all" /> 
<link rel="stylesheet" href="css/ui.theme.css" type="text/css" media="all" /> 
<script src="js/jquery-ui.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="css/jquery.treeview.css" />
<script type="text/javascript" src="js/jquery.treeview.js"></script>
<script type="text/javascript" src="js/jquery.cookie.js"></script>
<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=ABQIAAAAtHmBZRr54n7gpeTApsZM6hT3Uai14PfyTLBFec3rJQWPbehFqxSGZpnFarcJR2yJc1Ca-Q6P1aUsYQ" type="text/javascript"></script>
<script type="text/javascript">
$(function() {
	$("#tabs").tabs({
		ajaxOptions: {
			error: function(xhr, status, index, anchor) {
				$(anchor.hash).html("No se ha encontrado la p�gina. Vuelva pronto");
			}
		}
	});
});
	$(document).ready(function(){		
		$("#browser").treeview();
	});
	
	
	 function jsAjax(servletName,shortName,repository){
		//Gr�fica 
		$.get(servletName,{dataprovider:shortName},  function(data){  
				 //alert("in callback mmmm");  
				 $("#imageReport").html(data); 
				 $("#reportTab").css('visibility','visible')	
				 $("#reportTabText").text(repository);
		});
		
		//Tabla
		$.get("ProviderTableServlet",{dataprovider:shortName},  function(data2){  
				 //alert("in callback mmmm");  
				  $("#tableReport").html(data2); 
				// $("#reportTable").css('visibility','visible')	
				// $("#reportTableText").text(repository);
		});
			
				
	  } 
	  
	  function initialize() {
	      if (GBrowserIsCompatible()) {
	        var map = new GMap2(document.getElementById("map_canvas"));
	        map.setCenter(new GLatLng(4,-74),5);
	        map.setUIToDefault();
	        /////// Points  
	        // Create a base icon for all of our markers that specifies the
	        // shadow, icon dimensions, etc.
	        var baseIcon = new GIcon(G_DEFAULT_ICON);
	        baseIcon.shadow = "http://www.google.com/mapfiles/shadow50.png";
	        baseIcon.iconSize = new GSize(20, 34);
	        baseIcon.shadowSize = new GSize(37, 34);
	        baseIcon.iconAnchor = new GPoint(9, 34);
	        baseIcon.infoWindowAnchor = new GPoint(9, 2);

	        // Creates a marker whose info window displays the letter corresponding
	        // to the given index.
	        function createMarker(point, index) {
	          // Create a lettered icon for this point using our icon class
	          var letter = String.fromCharCode("A".charCodeAt(0) + index);
	          var letteredIcon = new GIcon(baseIcon);
	          letteredIcon.image = "http://www.google.com/mapfiles/marker" + letter + ".png";

	          // Set up our GMarkerOptions object
	          markerOptions = { icon:letteredIcon };
	          var marker = new GMarker(point, markerOptions);

	          GEvent.addListener(marker, "click", function() {
	            marker.openInfoWindowHtml("Marker <b>" + letter + "</b>");
	          });
	          return marker;
	        }

	        // Add 10 markers to the map at random locations
	        var bounds = map.getBounds();
	        var southWest = bounds.getSouthWest();
	        var northEast = bounds.getNorthEast();
	        var lngSpan = northEast.lng() - southWest.lng();
	        var latSpan = northEast.lat() - southWest.lat();
	        for (var i = 0; i < 10; i++) {
	          var latlng = new GLatLng(southWest.lat() + latSpan * Math.random(),
	            southWest.lng() + lngSpan * Math.random());
	          map.addOverlay(createMarker(latlng, i));
	        }
	        ////////////
	        document.getElementById("map_canvas").style.visibility='hidden';
	      }
	    }

	  function currentTab(varMax){
		  document.getElementById(varMax).style.visibility='hidden';
	  }
	  
	  function showMap(varMax){
			document.getElementById("map_canvas").style.visibility='visible';
	  }	  
</script>
</head>
<body onload="initialize()" onunload="GUnload()">
<h1>Bdcol - Charts & Stats</h1>
<p>Bienvenido al motor de estad&iacute;sticas de Bdcol. Por favor elija el repositorio sobre el que quiere ver estad�stica.</p>
<div id="tabs">
	<ul>
		<li><a href="#main"onclick="javascript:currentTab('map_canvas')">Inicio</a></li>
		<li><a href="#map_canvas" onclick="javascript:showMap()">Repositorios en Colombia</a></li>
		
		<li style="visibility:hidden" id="reportTab"><a href="#imageReport" onclick="javascript:currentTab('map_canvas')" id="reportTabText">Universidad Nacional M</a></li>
		<li style="visibility:hidden"><a href="ProviderStatsServlet?dataprovider=urosario">Universidad del Rosario</a></li>
		<li style="visibility:hidden"><a href="CommonStatsServlet">Informaci�n Com�n</a></li>		
		<li style="visibility:hidden" id="reportTable"><a href="#tableReport" onclick="javascript:currentTab('map_canvas')" id="reportTableText"></a></li>
		<li id="reportLogs"><a href="CommonStatsServlet">Reporte Logs</a></li>
		
	</ul>
<div id="main">
	<ul id="browser" class="filetree treeview">



		<%
			TreeMap<String, List<Element>> SortByDepto = new TreeMap<String, List<Element>>();
			TreeMap<String, List<Element>> SortByInst = new TreeMap<String, List<Element>>();

			
			for (int j = 0; j < l.size(); j++) {

				oai_provider = (Element) l.get(j);
				
				
				shortName = oai_provider.getChild("shortName");
				departamento = oai_provider.getChild("departamento");
				municipio = oai_provider.getChild("municipio");
				institution = oai_provider.getChild("institution");
				repositoryName = oai_provider.getChild("repositoryName");
				urlhome = oai_provider.getChild("urlhome");

				if (SortByDepto.containsKey(departamento.getText())) {
					List actual = SortByDepto.get(departamento.getText());
					actual.add(oai_provider);
					SortByDepto.put(departamento.getText(), actual);

				} else {

					List actual = new ArrayList();
					actual.add(oai_provider);
					SortByDepto.put(departamento.getText(), actual);

				}
				
				
				if (SortByInst.containsKey(institution.getText())) {
					List actual = SortByInst.get(institution.getText());
					actual.add(oai_provider);
					SortByInst.put(institution.getText(), actual);

				} else {

					List actual = new ArrayList();
					actual.add(oai_provider);
					SortByInst.put(institution.getText(), actual);

				}

			}
			
			
			
		%>
			 <!-- Ordenamiento por Departamento -->
		
		<li class="closed"><span class="folder"> Departamentos </span> <ul><%
				
				
			for (String depto : SortByDepto.keySet()) {
		%>
		
		
		<li class="closed"><span class="folder"> <%out.print(depto); %></span> <%
		
 		List actual = SortByDepto.get(depto);
 		for (int j = 0; j < actual.size(); j++) {

 			oai_provider = (Element) actual.get(j);

 			shortName = oai_provider.getChild("shortName");
 			departamento = oai_provider.getChild("departamento");
 			municipio = oai_provider.getChild("municipio");
 			institution = oai_provider.getChild("institution");
 			repositoryName = oai_provider.getChild("repositoryName");
 			urlhome = oai_provider.getChild("urlhome");
 			String StrshortName = shortName.getText();
 			String repository = repositoryName.getText();
 			
 %>
			<ul>
				
				<li><span class="file"><a href="#" onclick="jsAjax('ProviderStatsServlet','<%=StrshortName%>','<%=repository%>');"><%=repository%></a></span></li>
			</ul> <%
 	}
 %></li>

		<%
			}
		%>
	</ul>
	</li>
	
	 		
	</ul>
		<li><a href="#" onclick="jsAjax('CommonStatsServlet','','Informaci�n Com�n');">Informaci�n Com�n</a></li>
</div>


</div>
<div id="imageReport"></div>
<div id="map_canvas" style="width: 800px; height: 440px"></div>
<div id="tableReport"></div>
</body>
</html>
