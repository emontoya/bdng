<%@page import="java.text.SimpleDateFormat"%>
<%@ page language="java" import="java.io.*,java.util.*" errorPage="" %>
<%@page import="org.bdng.stats.record.ProviderStatRecord"%>
<%
String dataProviderName = (String)request.getAttribute("dataProviderName");
List<ProviderStatRecord> list = (List<ProviderStatRecord>)request.getAttribute("table");
if(list == null){
     list = new ArrayList<ProviderStatRecord>();
     
     
}


%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html dir="ltr" lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Data Tables and Cascading Style Sheets Gallery</title>
<link rel="stylesheet" href="css/csstg.css" type="text/css">
<style>
table {
  border-collapse: collapse;
  border: 1px solid #666666;
  font: normal 11px verdana, arial, helvetica, sans-serif;
  color: #363636;
  background: #f6f6f6;
  text-align:left;
  }
caption {
  text-align: center;
  font: bold 16px arial, helvetica, sans-serif;
  background: transparent;
  padding:6px 4px 8px 0px;
  color: #CC00FF;
  text-transform: uppercase;
}
thead, tfoot {
background:url('images/bg.png') repeat-x;
text-align:left;
height:30px;
}
thead th, tfoot th {
padding:5px;
}
table a {
color: #333333;
text-decoration:none;
}
table a:hover {
text-decoration:underline;
}
tr.odd {
background: #f1f1f1;
}
tbody th, tbody td {
padding:5px;
}</style></head><body><div id="boundary"><div id="content">
  <div id="itsthetable"><table summary="Submitted table designs"><caption>
REPOSITORIO DIGITAL <%=dataProviderName%>
</caption><thead><tr>
    <th width="115" scope="col">Fecha</th>
    <th width="150" scope="col">N&uacute;mero de Registos</th>
    <th width="137" scope="col">Recolecci&oacute;n Exitosa</th>
    <th width="200" scope="col">N&uacute;mero de Archivos</th>
    <th width="157" scope="col">Tiempo de Recolecci&oacute;n</th></tr>
    </thead>
    <tbody>
    
    
    <%
    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

	for(ProviderStatRecord actual : list)
	{
		
		String date = format.format(actual.getDate());
		int NumberOfRegisters= actual.getNumberOfRegisters();
		String Successful = actual.getSuccessful()?"Si":"No";
		int NumberOfFiles= actual.getNumberOfFiles();
		long HarvesterTime= actual.getHarvesterTime();
		
		
		
		
		%>
		<tr>
	        <th scope="row" id="r100"><%=date%></th>
	        <td><%=NumberOfRegisters%></td>
	        <td><%=Successful%></td>
	        <td><%=NumberOfFiles%></td>
	        <td><%=HarvesterTime%></td>
	    </tr>
		<%
		 
	}
    
    
    %>
    
     
        </tbody></table>
        </div>
    <div id="ads"></div>
</div>


</div></body></html>