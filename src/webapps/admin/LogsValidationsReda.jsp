<%@page import="org.bdng.harvmgr.validator.ValidadorLog"%>
<%@page import="java.io.File"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.bdng.harvmgr.dlConfig"%>
<%@page import="org.jdom2.Document"%>
<%@page import="java.util.List"%>
<%@page import="org.jdom2.Element"%>
<%@page import="org.jdom2.input.SAXBuilder"%>
<%@page import="org.bdng.harvmgr.harvester.DataProvider"%>
<%@page import="org.bdng.webold.ProviderWeb"%>
<%@ page language="java" contentType="text/html; charset=US-ASCII"
    pageEncoding="US-ASCII"%>
    
    <%    //verificamos session como "admin" o como "adminRepo" sino, lo redirijimos al index.jsp
	HttpSession sesion = request.getSession();
    String user ="";
    String type = "";
    String mostrarMensaje = "";
    if(sesion.getAttribute("username")== null && sesion.getAttribute("type")== null) {
    	response.sendRedirect("errorSession.jsp");
    	
	}else{
		user = (String)sesion.getAttribute("username");
    	type = (String)sesion.getAttribute("type");
    	mostrarMensaje = (String)sesion.getAttribute("mostrarmensaje");
    	if(mostrarMensaje == null){
    		mostrarMensaje = "";
    	}
    		if(!type.equals("adminRepo") && !type.equals("admin"))
    		
    			response.sendRedirect("errorSession.jsp");
	} %>
    
<%

    if(sesion.getAttribute("username")!= null)
    	user = (String)sesion.getAttribute("username");
	
    dlConfig doa = new dlConfig(this.getServletContext().getRealPath("/"));
   	List l = doa.GetVisualProviders();
	//	out.print("registros "+l.size());	
	
	ValidadorLog validadorLog = new ValidadorLog();
	List<File> listaLogs = validadorLog.retornarLogs();
    
	Element oai_provider = null;
	
	Element shortName = null;
	Element repositoryName = null;
	Element baseURL = null;
	Element collection = null;
	Element metadataPrefix = null;
	Element protocol = null;
	Element owner = null;
  	
  	
  	
  	    %>
    

	<div id="cargando" class="loadingDiv"></div>
	<div id="container">
		<div class="full_width big"></div>
		<div id="demo">
			<div class="alert alert-success text-center" style="display: <% if(mostrarMensaje.equals("true")){%>block; <% }else{%>none;<%}%> ">Ha terminado la validaci&oacute;n</div>
			<div id="title_section" class="text-center">
				<h2>Logs de Validaci&oacute;n - REDA</h2>
			</div> 
			<table cellpadding="0" cellspacing="0" border="0" class="display"
				id="example">
				<thead>
					<tr>
<!-- 						<th>Repositorio</th> -->
						<th>Log</th>
					</tr>
				</thead>
				<tbody>
					<%
						for(int i = 0; i<listaLogs.size();i++){
							out.print("<tr class=\"gradeU\">");
							out.print("<td><a href=\"/logs/"+listaLogs.get(i).getName()+"\" target=\"_blank\">" + listaLogs.get(i).getName() + "</a></td>");
							out.print("</tr>");
						}
					%>
				</tbody>
			</table>
		</div>
	</div>
	
	<% sesion.setAttribute("mostrarmensaje", null); %>
