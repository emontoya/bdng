<%@ page language="java" contentType="text/html; charset=US-ASCII"
	pageEncoding="US-ASCII"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
<title>BDNG</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link type="text/css" href="resources/css/app/basic.css"
	rel="stylesheet" />

<link type="text/css" href="resources/bootstrap/css/bootstrap.css"
	rel="stylesheet">
<link type="text/css"
	href="resources/bootstrap/css/bootstrap-responsive.css"
	rel="stylesheet">
</head>
<body>
	<div class="navbar navbar-inverse navbar-fixed-top">
		<div class="navbar-inner">
			<div class="container">
				<button type="button" class="btn btn-navbar" data-toggle="collapse"
					data-target=".nav-collapse">
					<span class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="brand" href="#">Administraci&oacute;n BDNG</a>
				<div class="nav-collapse collapse">
					<form id="loginForm" action="index.jsp" class="navbar-form pull-right" method="POST">
						<input id="username" name="username" class="span2" type="text" placeholder="Nombre de usuario" onkeypress="Clear();" onfocus="Clear();"> 
						<input id="password" name="password" class="span2" type="password" placeholder="Contrase&ntilde;a" onkeypress="Clear();" onfocus="Clear();"> 
						<a href="javascript:;" class="btn" onclick="login_admin()">Entrar</a>
					</form>
				</div>
				<!--/.nav-collapse -->
			</div>
		</div>
	</div>

	<div class="container">
		<div class="hero-unit">
			<h1>BDNG</h1>
			<p>Aplicaci&oacute;n para la administraci&oacute;n de la Biblioteca Digital de Nueva Generaci&oacute;n BDNG</p>
			<!--p><a href="#" class="btn btn-primary btn-large">Learn more &raquo;</a></p-->
		</div>

		<div class="alert alert-error text-center" id="wrongLogin"
			hidden="true">
			<strong>Error!</strong> Usuario o contrase&ntilde;a invalida
		</div>

		<hr>

		<footer>
		<p>&copy; BDNG 2007 - 2013 - Aplicaci&oacute;n creada por la Universidad EAFIT bajo la licencia <a href="http://www.gnu.org/copyleft/lesser.html" target="_blank">GNU LPGL</a></p>
		</footer>
	</div>


	<script type="text/javascript" src="resources/js/lib/jquery.min.js"></script>

	<script type="text/javascript" src="resources/js/app/jsLogin.js"
		charset="utf-8"></script>

	<script type="text/javascript"
		src="resources/bootstrap/js/bootstrap.js"></script>

	<script type="text/javascript" src="resources/js/app/appJSP.js"></script>
</body>
</html>