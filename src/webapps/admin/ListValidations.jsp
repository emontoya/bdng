<%@page import="java.util.Iterator"%>
<%@page import="org.bdng.harvmgr.dlConfig"%>
<%@page import="org.jdom2.Document"%>
<%@page import="java.util.List"%>
<%@page import="org.jdom2.Element"%>
<%@page import="org.jdom2.input.SAXBuilder"%>
<%@page import="org.bdng.harvmgr.harvester.DataProvider"%>
<%@page import="org.bdng.webold.ProviderWeb"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	
	        
    <%    //verificamos session como "admin" o como "adminRepo" sino, lo redirijimos al index.jsp
	HttpSession sesion = request.getSession();
    String user ="";
    String type = "";
    if(sesion.getAttribute("username")== null && sesion.getAttribute("type")== null) {
    	response.sendRedirect("errorSession.jsp");
    	
	}else{
		user = (String)sesion.getAttribute("username");
    	type = (String)sesion.getAttribute("type");
    		if(!type.equals("adminRepo") && !type.equals("admin"))
    		
    			response.sendRedirect("errorSession.jsp");
	} %>
    
<%

    if(sesion.getAttribute("username")!= null)
    	user = (String)sesion.getAttribute("username");
	
    dlConfig doa = new dlConfig(this.getServletContext().getRealPath("/"));
   	List l = doa.GetVisualProviders();
	//	out.print("registros "+l.size());	
	
	
	Element oai_provider = null;
	
	Element shortName = null;
	Element repositoryName = null;
	Element baseURL = null;
	Element collection = null;
	Element metadataPrefix = null;
	Element protocol = null;
	Element owner = null;
  	
  	
  	
  	    %>

<div id="cargando" class="loadingDiv"></div>
	<div id="container">
		<div class="full_width big"></div>
		<div id="demo">
		
		<div id="title_section" class="text-center">
			<h2>Validacion de proveedores</h2>
		</div> 
		<br />

			<table cellpadding="0" cellspacing="0" border="0" class="display"
				id="example">
				<thead>
					<tr>
						<th>Validar</th>
						<th>ID-rep</th>
						<th>Repositorio</th>
						<th>Resultado Validacion</th>
					</tr>
				</thead>
				<tbody>
					<%
    
for(int j = 0; j<l.size();j++){
		  		
  		oai_provider=(Element)l.get(j);
  			
  			owner=oai_provider.getChild("owner");
  			if( !user.equals("admin")) //si es el admin se salta la siguiente linea para mostrar TOODOS los providers
				if(	!owner.getText().equals(user)) continue;

  			shortName=oai_provider.getChild("shortName");
  			repositoryName=oai_provider.getChild("repositoryName");
  			baseURL=oai_provider.getChild("baseURL");
  			collection=oai_provider.getChild("collection");
  			metadataPrefix=oai_provider.getChild("metadataPrefix");
  			protocol=oai_provider.getChild("protocol");
			
  		    			
          out.print("<tr class=\"gradeU\">");
          out.print("<td align=\"center\" class='elements'><A class='toltip' title='Click aqui para validar este repositorio'  HREF=\"servlet/ValidatorWeb?option=validate&shortname="+shortName.getText()+"&urlbase="+baseURL.getText()+"&protocol="+protocol.getText()+"&metadataPrefix="+metadataPrefix.getText()+"\"><img style='cursor:pointer;' src=\"images/btn_validate.png\" width=\"25\" height=\"25\" border=\"0\"></A></td>");
          
          out.print("<td align=\"center\">"+shortName.getText()+"</td>");
          out.print("<td align=\"center\">"+repositoryName.getText()+"</td>");
          out.print("<td align=\"center\"> "+doa.GetScoreValidation(shortName.getText())+"</td>");
          out.print("</tr>");
          
      }    
    
    %>
				</tbody>
				<tfoot>
					<tr>
						<th>Validar</th>
						<th>ID-rep</th>
						<th>Repositorio</th>
						<th>Resultado Validacion</th>
					</tr>
				</tfoot>
			</table>
</div>
</div>
