
<%@page import="java.util.Iterator"%>
<%@page import="org.bdng.harvmgr.dlConfig"%>
<%@page import="org.jdom2.Document"%>
<%@page import="java.util.List" %>
<%@page import="org.jdom2.Element"%>
<%@page import="org.jdom2.input.SAXBuilder" %>
<%@page import="org.bdng.harvmgr.harvester.DataProvider"%>
<%@page import="org.bdng.webold.ProviderWeb"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
            
    <%    //verificamos session como "admin" sino, lo redirijimos al index.jsp
	HttpSession sesion = request.getSession();
    String user ="";
    String type = "";
    if(sesion.getAttribute("username")== null && sesion.getAttribute("type")== null) {
    	response.sendRedirect("errorSession.jsp");
    	
	}else{
		user = (String)sesion.getAttribute("username");
    	type = (String)sesion.getAttribute("type");
    		if(!user.equals("admin") || !type.equals("admin"))
    			response.sendRedirect("errorSession.jsp");
	} %>
    
    <%
    
    	
    	dlConfig doa = new dlConfig(this.getServletContext().getRealPath("/"));
    	List l = doa.GetVisualProviders();
    	//out.print("registros "+l.size());
    	
		
		
		
	Element oai_provider = null;
		
	Element shortName = null;
	Element repositoryName = null;
	Element nameAdmin = null;
	Element emailadmin = null;
	Element phoneAdmin = null;
	Element owner = null;
	 
  	
  	
  	
  	    %>

		<div  id="container">
			<div class="full_width big"></div>
			<div id="title_section" class="text-center">
				<h2>Lista Detalla de Repositorio</h2>
			</div> 
			<br />
				<div  id="demo">
<form name="control" method="post" action="">
  <table  cellpadding="0" cellspacing="0" border="0" class="display" id="example">
    <thead>
    <tr>
    
    		<th>Repositorio</th>
			<th>Nombre Corto</th>
			<th>Administrador</th>
			<th>Email</th>
			<th>Telefono</th>
			
			

    </tr>
    </thead>
    	<tbody>
    <%
    
for(int j = 0; j<l.size();j++){
		  		
		
		oai_provider=(Element)l.get(j);
	
		owner=oai_provider.getChild("owner");
		if(	!owner.getText().equals("admin")) continue;

  			shortName=oai_provider.getChild("shortName");
  			repositoryName=oai_provider.getChild("repositoryName");
  			nameAdmin=oai_provider.getChild("nameAdmin");
  			emailadmin=oai_provider.getChild("emailadmin");
  			phoneAdmin=oai_provider.getChild("phoneAdmin");
			
  				    			
          out.print("<tr class=\"gradeU\">");
          out.print("<td align=\"center\">"+repositoryName.getText()+"</td>");
          out.print("<td align=\"center\">"+shortName.getText()+"</td>");
          out.print("<td align=\"center\">"+nameAdmin.getText()+"</td>");
          out.print("<td align=\"center\">"+emailadmin.getText()+"</td>");
          out.print("<td align=\"center\">"+phoneAdmin.getText()+"</td>");
          
          out.print("</tr>");
		  
          
      }    
    
    %>
    </tbody>
	<tfoot>
		<tr>
    
    		<th>Repositorio</th>
			<th>Nombre Corto</th>
			<th>Administrador</th>
			<th>Email</th>
			<th>Telefono</th>

		
		</tr>
	</tfoot>
</table>
  

  </form>
</div>
</div>