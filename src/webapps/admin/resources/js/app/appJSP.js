$(document).ready(
		function() {

			$('#metadata_http').show();

			$('#protocol').bind(
					{

						keyup : function() {
							($(this).val() == "HTTP") ? $('#metadata_http')
									.show() : $('#metadata_http').hide();
							($(this).val() == "OAI") ? $('#metadata_oai')
									.show() : $('#metadata_oai').hide();
						},

						change : function() {
							($(this).val() == "HTTP") ? $('#metadata_http')
									.show() : $('#metadata_http').hide();
							($(this).val() == "OAI") ? $('#metadata_oai')
									.show() : $('#metadata_oai').hide();
						}

					});

		});

function loadPage(page) {
	if((page != "null") && (page != null)){
		var xmlhttp;
		if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		} else {// code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
				document.getElementById("main").innerHTML = xmlhttp.responseText;
			}
		};
		xmlhttp.open("GET", page, true);
		xmlhttp.send();
//		$.when(xmlhttp.send()).then(
//			function(){
//				console.log($(document.getElementsByTagName("form")));
//				$("#bodyProvider").onload = function() {
//					console.log("Funciona Body onload");
//				};
//			}
//		);
		
//		if(page == "Provider.jsp"){
//			console.log($("#form1").html());
//			init($("#form1"));
//		}
	}
};

function loadPageProviders(page){
	if((page != "null") && (page != null)){
		$.when(this.loadPage(page)).then(
			function() {
				console.log(document.getElementById("main"));
			}
		);
	}
}

