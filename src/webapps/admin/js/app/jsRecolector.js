
var percentage = 0;

$(document).ready(function() {
	
	

	$("#Iniciar_sel").bind('click', function() {
		
	//	console.log('entro al bind');
		init_check();
		ValidateInit(this.form);
		
	});


	$("#Iniciar").bind('click', function() {
		

		init_check();
		//window.location='servlet/HarversterWeb?harv_opcion=iniciar_todos';
		
		$.get("servlet/HarversterWeb", { harv_opcion: "iniciar_todos" } );
		
		
		
	
	});

	$('#example').dataTable();
} );

var idInterval;

function init_check(){
	//console.log('entro al initcheck');
	var interval = 1000 ;
	idInterval = setInterval(ajax_call, interval);

}


var  ajax_call = function check_progress(){
	
	
//	console.log('entro al check progress');
	

	var dato = "dato";
	var url = "servlet/HarversterWeb?harv_opcion=check_progress";



	loggeando  = $.ajax({
		type: "POST",
		data: {dato: dato},
		url: url,
		timeout: 25000,
		dataType: "html",
		beforeSend: function(){        	
		},
		complete: function(){
		},
		success: function( data ) {
			//alert("data "+data)

			var dataProvidersCompletos  = $(data).find("#dataProvidersCompletos").html();
			var dataProvidersSize  = $(data).find("#dataProvidersSize").html();
			percentage = Math.floor((dataProvidersCompletos *100)/dataProvidersSize);
			


			$("#result").text("Completados: "+dataProvidersCompletos+" de  "+dataProvidersSize+" Repositorios "+percentage+"%");
			$( "#progressbar" ).progressbar({
				value: percentage
			});

			if(percentage==100){
				clearInterval(idInterval);
				setTimeout(Reload(),50000); // actualiza la pagina cuando termina la recoleccion
			}


		},
		error: function(result){
			if(result.statusText != "abort"){
				alert("Problemas en el servidor, Intentelo otra vez en unos minutos.");
			}
		}
	});
	return false;
}  


function Reload(){
	console.log('Recargando');
	window.location.reload(true);
}



function Validate(form){
	if(form.listProviders.checked){//este if es para cuando el usuario hace un filtrado de tal forma que solo queda un elemento  y no podemos tratarlo con un array
		Reset(form);
		return true;
	}
	
	for(var i = 0; i < form.listProviders.length; i++){ 
		if(form.listProviders[i].checked){
		Reset(form);
		return true;
		}
	}
	alert('Debes seleccionar al menos un Repositorio'); 
	return false; 
	}

function Reset(form){
		
		form.action = 'servlet/HarversterWeb?harv_opcion=reset_seleccionados';
		form.submit();				
}

function ValidateInit(form){
//	alert(form.listProviders.length);
	
	//console.log('validando');
	if(form.listProviders.checked){
		Init(form);
		return true;
	}

	for(var i = 0; i < form.listProviders.length; i++){ 
		if(form.listProviders[i].checked){
		Init(form);
		return true;
		}
	}
	alert('Debes seleccionar al menos un Repositorio'); 
	return false; 
	}

function Init(form){
		/*
		form.action = 'servlet/HarversterWeb?harv_opcion=init_seleccionados';
		form.submit();
		*/
		$.post("servlet/HarversterWeb?harv_opcion=init_seleccionados", $("#control").serialize());	
		
}

function checkAll(){
	
	$(".checkbox").attr('checked', !$(".checkbox").attr('checked'));
	
}
function callAll (){

	checkAll();

}
