
<%@page import="java.util.Iterator"%>
<%@page import="org.bdng.harvmgr.dlConfig"%>
<%@page import="org.jdom2.Document"%>
<%@page import="java.util.List" %>
<%@page import="org.jdom2.Element"%>
<%@page import="org.jdom2.input.SAXBuilder" %>
<%@page import="org.bdng.harvmgr.harvester.DataProvider"%>
<%@page import="org.bdng.webold.ProviderWeb"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
            
    <%    //verificamos session como "admin" sino, lo redirijimos al index.jsp
	HttpSession sesion = request.getSession();
    String user ="";
    String type = "";
    if(sesion.getAttribute("username")== null && sesion.getAttribute("type")== null) {
    	response.sendRedirect("errorSession.jsp");
    	
	}else{
		user = (String)sesion.getAttribute("username");
    	type = (String)sesion.getAttribute("type");
    		if(!user.equals("admin") || !type.equals("admin"))
    			response.sendRedirect("errorSession.jsp");
	} %>
    
    <%
    
    	
    	dlConfig doa = new dlConfig(this.getServletContext().getRealPath("/"));
    	List l = doa.GetVisualProviders();
    	//out.print("registros "+l.size());
    	
		
		
		
	Element oai_provider = null;
		
	Element shortName = null;
	Element repositoryName = null;
	Element owner = null;
	 
  	
  	
  	
  	    %>

		<div  id="container">
			<div class="full_width big"></div>
			
			<div id="title_section" class="text-center">
				<h2>Indexacion de metadatos recolectados</h2>
			</div> 
			<br />
				<div  id="demo">
<form name="control" id="controlIndexer" method="post" action="">
<div align="right" > <input type="checkbox" id="checkAll" onchange="callAll()" /> Seleccionar todos</div>
  <table  cellpadding="0" cellspacing="0" border="0" class="display" id="example">
    <thead>
    <tr>
    
    		<th>Repositorio</th>
			<th>Nombre Corto</th>
			<th>Acci�n</th>
			
			

    </tr>
    </thead>
    	<tbody>
    <%
    
for(int j = 0; j<l.size();j++){
		  		
		
		oai_provider=(Element)l.get(j);
	
		owner=oai_provider.getChild("owner");
		if(	!owner.getText().equals("admin")) continue;

  			shortName=oai_provider.getChild("shortName");
  			repositoryName=oai_provider.getChild("repositoryName");
			
  				    			
          out.print("<tr class=\"gradeU\">");
          out.print("<td align=\"center\">"+repositoryName.getText()+"</td>");
          out.print("<td align=\"center\">"+shortName.getText()+"</td>");
          out.print("<td align=\"center\">"+"<input class=\"checkbox\" type=\"checkbox\"name=\"listProviders\" value=\""+shortName.getText()+"\">"+"</td>");
          out.print("</tr>");
		  
          
      }    
    
    %>
    </tbody>
	<tfoot>
		<tr>
      		
      		<th>Repositorio</th>
			<th>Nombre Corto</th>
			<th>Acci�n</th>
					
		</tr>
	</tfoot>
</table>

<br>
<br>
		<div id="progress" style="width: 100%; margin: auto 0;">
	  		<div id="result"></div>
			<div id="progressbar"></div>
		</div>
<br />
  
    <p>&nbsp;</p>
    <table align="center" width="300" border="0">
      <tr>
        <td><input type="button" name="Actualizar" id="Actualizar" value="Actualizar" onClick="window.location='servlet/IndexarWeb?index_opcion=refresh_providers'"></td>
        <td><input type="button" name="Indexar" id="Indexar" value="Indexar Todos" onClick="indexarTodos()"></td>
        <td><input type="button" name="Indexar_sel" id="Indexar_sel" value="Indexar Seleccionados" onClick="indexarSeleccionados(this.form)"></td>
        
        
      </tr>
    </table>
   
  </form>
</div>
</div>
