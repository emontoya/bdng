<%@ page language="java" contentType="text/html; charset=US-ASCII"
    pageEncoding="US-ASCII"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
<title>Error de sesi&oacute;n</title>
</head>
<body>
	<div class="alert alert-error text-center"><strong>Error!</strong> Su sesi&oacute;n a expirado, vuelva a iniciar sesi&oacute;n</div>
	<div class="text-center">
		<a href="main.jsp" class="btn btn-info">Iniciar Sesi&oacute;n</a>
	</div>
</body>
</html>