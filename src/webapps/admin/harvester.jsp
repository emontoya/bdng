<%@ page import  = "org.bdng.oai.db.DBExist,java.util.*"%>
<%
	try {
		DBExist db = DBExist.getInstance();
		Vector sets = new Vector(db.loadSets());
%>
<html>
	<head>
<title>BDNG OAI</title>
<META HTTP-EQUIV="Content-Type"
			content="text/html; charset=iso-8859-1">
		<META HTTP-EQUIV="Cache-Control" CONTENT="no-cache">
		<META HTTP-EQUIV="Cache-Control" CONTENT="no-store">
		<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
		<META HTTP-EQUIV="Expires" CONTENT="0">
		<META HTTP-EQUIV="Content-Encoding" CONTENT="gzip">
		<link rel="stylesheet" href="styles/styletxt2xml.css"
			type="text/css">
<script language="javascript">
<!--

function trim(str){
	return str.replace(/^\s*|\s*$/g,"");
}


function disableSet(){
	var form = document.formBusqueda;
	form.set.disabled = true;
}
function enableSet(){
	var form = document.formBusqueda;
	form.set.disabled = false;
}

function disableGetRecord(){
	var form = document.formBusqueda;
	form.identifier.disabled = true;
}
function enableGetRecord(){
	var form = document.formBusqueda;
	form.identifier.disabled = false;
}

function disableFromUntil(){
	var form = document.formBusqueda;
	form.from_year.disabled = true;
	form.from_month.disabled = true;
	form.from_day.disabled = true;
	form.until_year.disabled = true;
	form.until_month.disabled = true;
	form.until_day.disabled = true;
	form.from.disabled = true;
	form.until.disabled = true;
}
function enableFromUntil(){
	var form = document.formBusqueda;
	form.from_year.disabled = false;
	form.from_month.disabled = false;
	form.from_day.disabled = false;
	form.until_year.disabled = false;
	form.until_month.disabled = false;
	form.until_day.disabled = false;
	form.from.disabled = false;
	form.until.disabled = false;
}
function enableFrom() {
	var form = document.formBusqueda;
	form.from_year.disabled = true;
	form.from_month.disabled = true;
	form.from_day.disabled = true;
	form.from.disabled = false;
}
function enableUntil() {
	var form = document.formBusqueda;
	form.until_year.disabled = true;
	form.until_month.disabled = true;
	form.until_day.disabled = true;
	form.until.disabled = false;
}


function disableRT(){ //ResumptionToken
	var form = document.formBusqueda;
	form.resumptionToken.disabled = true;
}
function enableRT(){
	var form = document.formBusqueda;
	form.resumptionToken.disabled = false;
}



function enableDisable() {

	var forma = document.formBusqueda;
	var value = forma.verb.value;
	disableGetRecord();
	disableFromUntil();
	disableSet();
	disableRT();//Nuevo

	if(value == 'ListRecords' || value == 'ListIdentifiers') {
		//var 
		
		enableFromUntil();
		enableSet();
		enableRT();
		
		

	}else if(value == 'GetRecord') {
		enableGetRecord();
	}

}

function validateDateYear(date) {
	var temp = new Number(date);

	if(temp=='NaN' || temp==null) {
		return false;
	}

	if (temp > 1900 && temp < 2020) {
		return true;
	}

	return false;

}

function validateDateMonth(date) {
	var temp = new Number(date);

	if(temp=='NaN' || temp==null) {
		return false;
	}

	if (temp > 0 && temp < 13) {
		return true;
	}

	return false;

}

function validateDateDay(date) {
	var temp = new Number(date);

	if(temp=='NaN' || temp==null) {
		return false;
	}

	if (temp > 0 && temp < 32) {
		return true;
	}

	return false;

}

function validateFrom() {
	var forma = document.formBusqueda;

	var day = forma.from_day.value;
	var month = forma.from_month.value;
	var year = forma.from_year.value;

	if(validateDateDay(day) && validateDateMonth(month) && validateDateYear(year)) {
		return true;
	}

	return false;

}

function validateUntil() {

	var forma = document.formBusqueda;

	var day = forma.until_day.value;
	var month = forma.until_month.value;
	var year = forma.until_year.value;

	if(validateDateDay(day) && validateDateMonth(month) && validateDateYear(year)) {
		return true;
	}

	return false;

}

function isNullFrom() {
	var forma = document.formBusqueda;
	var day = forma.from_day.value;
	var month = forma.from_month.value;
	var year = forma.from_year.value;

	if(trim(day) == "" && trim(month) == "" && trim(year)=="") {
		return true;
	}

	return false;

}

function isNullUntil(){
	var forma = document.formBusqueda;
	var day = forma.until_day.value;
	var month = forma.until_month.value;
	var year = forma.until_year.value;

	if(trim(day) == "" && trim(month) == "" && trim(year)=="") {
		return true;
	}

	return false;

}

function getFrom() {
	var forma = document.formBusqueda;
	//var from = forma.from_year.value + "-" + forma.from_month.value + "-" + forma.from_day.value;
	var from = forma.from_year.value + "" + forma.from_month.value + "" + forma.from_day.value;
	//alert(from);
	return from;
}

function getUntil() {
	var forma = document.formBusqueda;
	//var until = forma.until_year.value + "-" + forma.until_month.value + "-" + forma.until_day.value;
	var until = forma.until_year.value + "" + forma.until_month.value + "" + forma.until_day.value;
	//alert(until);
	return until;
}

function sendSearch() {
	var enviar1 = false;
	var enviar2 = false;
	var form = document.formBusqueda;

	var value = form.verb.value;
	
				disableFromUntil();	
	
	if (value == 'ListRecords' || value == 'ListIdentifiers') {
		if(form.resumptionToken.value != '') {
				disableFromUntil();
				disableSet();
				enviar1 = true;
				enviar2= true;
				
		
		} else {
			form.resumptionToken.disabled = true;
		
			if(!isNullFrom()) {
				if(validateFrom()) {
					enviar1=true;
					enableFrom();
					form.from.value = getFrom();
				}
			}else {
				enviar1 = true;
			}

			if(!isNullUntil()) {
				if(validateUntil()) {
					enableUntil();
					enviar2=true;
					form.until.value = getUntil();
				}
			}else {
				enviar2 = true;
			}
		
		}

		if(enviar1 && enviar2) {
			form.submit();
			form.reset();
		}else {
			if (!enviar1 && !enviar2){
				alert('from datestamp and until datestamp invalid');
			}else if(enviar2){
				alert('from datestamp invalid');
			}else{
				alert('until datestamp invalid');
			}
		}

	}else {
		form.submit();
		form.reset();
	}

}



-->
</script>
	</head>
	<body class="twoColElsLtHdr">
		<div id="container">
		  <div id="header">
    <h1><tbody><tr>
					<td>
						<table border="0" cellpadding="0" cellspacing="0" width="100%">  <tbody><tr>    <td bgcolor="#b6c589" width="100%"><table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">      <tbody><tr>        <td><a href="http://www.eafit.edu.co/biblioteca.shtm" target="_blank"><img src="img/eafit/titulo.gif" alt="titulo" border="0" height="15" width="505"></a></td>      </tr>    </tbody></table></td>  </tr></tbody></table><table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">  
						  <tbody><tr>        <td width="795"><table border="0" cellpadding="0" cellspacing="0" width="100%">      <tbody><tr>        <td align="left"><img src="img/eafit/libro.jpg" height="90" width="55"></td>        <td align="center"><img src="img/eafit/titleDigital.gif" height="39" width="274"></td>        
						<td align="center"><img src="img/eafit/img.jpg" height="90" width="348"></td>        <td align="right"><a href="http://www.eafit.edu.co/" target="_blank"><img src="img/eafit/logoVerde.gif" alt="Regreso a página principal" border="0" height="89" width="118"></a></td>      </tr>    </tbody></table></td>      
						  </tr></tbody></table><a name="subir"></a></td>
				</tr>
				<tr>
					<td> <table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">
  </table>
<map name="Map" id="Map"><area shape="rect" coords="740,2,762,19" href="http://www.eafit.edu.co/biblioteca.shtm" alt="Volver al Home"><area shape="rect" coords="6,3,87,19" href="http://www.eafit.edu.co/EafitCn/Biblioteca/servicios/"><area shape="rect" coords="105,2,241,19" href="http://www.eafit.edu.co/EafitCn/Biblioteca/digital/"><area shape="rect" coords="264,3,379,19" href="http://www.eafit.edu.co/EafitCn/Biblioteca/recomendados/"><area shape="rect" coords="402,2,468,20" href="http://www.eafit.edu.co/EafitCn/Biblioteca/enlaces/"><area shape="rect" coords="486,3,602,18" href="http://www.eafit.edu.co/EafitCn/Biblioteca/generalidades/"><area shape="rect" coords="622,2,727,18" href="http://www.eafit.edu.co/Eafit/Includes/biblioteca/contactenos/contactenos.shtm">
  






</map>

<map name="Map2" id="Map2"><area shape="rect" coords="371,1,455,22" href="http://www.eafit.edu.co/buscador/"><area shape="rect" coords="196,1,340,24" href="http://www.eafit.edu.co/biblioteca/metabiblioteca/index.htm" target="_blank"><area shape="rect" coords="-1,1,157,22" href="http://zeta.eafit.edu.co:8080/sinbad/">


</map>
<map name="Map3" id="Map3"><area shape="rect" coords="2,1,96,23" href="http://www.eafit.edu.co/EafitCn/Biblioteca/english/services">
</map></td>
				</tr>
	</tbody></h1>
  <!-- end #header --></div>
			<div id="mainContent3">
<form name="formBusqueda"   action="servlet/co.edu.eafit.bdng.oai.servlet.OAI" method="get" target="_blank">
<table width="97%" id="table4" align="center" border="1" cellspacing="1" cellpadding="1">
<input  type="hidden"  name="from" disabled>
<input  type="hidden"  name="until" disabled>
   <tr>
      <td colspan="4" align="center"><p><strong><b>RECOLECCI&Oacute;N EN LINEA OAI - BDEAFIT</b></strong></p></td>
  </tr>
  <tr>
        <td colspan="4">&nbsp;</td>
  </tr>

  <tr>
    <td><p><b>Action (verb):</b></p></td>
    <td>
    	<select name="verb" onChange="enableDisable()">
			<option value="Identify">Identify</option>
			<option value="GetRecord">GetRecord</option>
			<option value="ListIdentifiers">ListIdentifiers</option>
			<option value="ListRecords">ListRecords</option>
			<option value="ListSets">ListSets</option>
			<option value="ListMetadataformats">ListMetadataformats</option>
		</select>
	</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
        <td colspan="4">&nbsp;</td>
  </tr>

  <tr>

      <td colspan="4"><p><b>Options for GetRecord</b></p></td>
  </tr>
  <tr>
      <td><p>Set:</p></td>
      <td>
      	<select  name="set" disabled>
	  		<%for (Iterator iter = sets.iterator(); iter.hasNext();) {
	  			String element = (String) iter.next();
	  		%>
	     		<option value="<%=element%>"><%=element%></option>
	     	<%}%>
	  	</select>
	  </td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
  </tr>
  <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
  </tr>
  <tr>
      <td><p>Record Id:</p></td>
      <td><input type="text" name="identifier" size="10" maxlength="25" disabled></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
  </tr>
  <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
  </tr>
  <tr>
      <td colspan="4"><p><b>Options for Listrecords and ListIdentifiers</b></p></td>
  </tr>
   <tr>
      <td><p>from (YYYY-MM-DD):</p></td>
      <td><input type="text" name="from_year" size="4" maxlength="4"  disabled>&nbsp;/&nbsp;<input type="text" name="from_month" size="2" maxlength="2" disabled>&nbsp;/&nbsp;<input type="text" name="from_day" size="2" maxlength="2" disabled>
	  </td>
      <td><p>until (YYYY-MM-DD):</p></td>
      <td><input type="text" name="until_year" size="4" maxlength="4" disabled>&nbsp;/&nbsp;<input type="text" name="until_month" size="2" maxlength="2" disabled>&nbsp;/&nbsp;<input type="text" name="until_day" size="2" maxlength="2" disabled></td>
  </tr>
  <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
  </tr>
  <tr>
      <td><p>ResumptionToken:</p></td>
      <td><input type="text" name="resumptionToken" size="20" disabled></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
  </tr>


  <tr>
        <td><input type="button" name="submitname"  value="Search" onClick="sendSearch()">
		</td>
		<td><input type="reset" name="submitname"  value="Reset" ></td>
  </tr>

</table>
</form>
			</div>
			<div id="footer">
				<p align="center">
					� Biblioteca Luis Echavarr�a Villegas - Universidad EAFIT -
					Medell�n Colombia � Carrera 49 No. 7 Sur-50 - Avenida Las Vegas -
					Tel�fono (57) (4) 261 95 00 - Ext. 261 AA 3300 - Fax (57) (4) 266
					42 84
				</p>
				<!-- end #footer -->
			</div>
			<!-- end #container -->
		</div>
</body>
</html>
<%}catch(Exception e) {
e.printStackTrace();

}
%>
