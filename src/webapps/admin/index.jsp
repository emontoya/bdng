<%@page import="org.bdng.harvmgr.dlConfig"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List" %>
<%@page import="org.jdom2.Element"%>
<%@ page language="java" contentType="text/html; charset=US-ASCII"
	pageEncoding="US-ASCII"%>
	<%    //verificamos session como "admin" o como "adminRepo" sino, lo redirijimos al index.jsp
	HttpSession sesion = request.getSession();
    String user ="";
    String type = "";
    String pagina = "";
    String barraCargando = "";
    if(sesion.getAttribute("username")== null && sesion.getAttribute("type")== null) {
    	response.sendRedirect("main.jsp");
    	
	}else{
		user = (String)sesion.getAttribute("username");
    	type = (String)sesion.getAttribute("type");
    	pagina = (String)sesion.getAttribute("pagina");
    	barraCargando = (String)sesion.getAttribute("barraCargando");
    		if(!type.equals("adminRepo") && !type.equals("admin"))
    		
    			response.sendRedirect("main.jsp");
	} %>
    
    <%
    if(sesion.getAttribute("username")!= null)
    	user = (String)sesion.getAttribute("username");
    
    	dlConfig doa = new dlConfig(this.getServletContext().getRealPath("/"));
    
    	String sysName =(String) doa.getSysName();
    	String sysNameFull=(String) doa.getSysNameFull();
    	
    	List ListMetadataPrefix = doa.GetVisualListMetadataPrefix();
		
    	Element eListMetadataPrefix = null;
		Element eMetadataPrefix= null;
		ArrayList<String> ArrayMetadataPrefix = new ArrayList<String>();
		
		
		
		for(int j = 0; j<ListMetadataPrefix.size();j++){
			
			eListMetadataPrefix=(Element)ListMetadataPrefix.get(j);
			eMetadataPrefix=eListMetadataPrefix.getChild("name");
			ArrayMetadataPrefix.add(eMetadataPrefix.getText());
		
      	}    
    
    %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
	<title><%=sysName%> - <%=sysNameFull%></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<link type="text/css" href="resources/css/app/basic.css" rel="stylesheet"/>
	
	<link type="text/css" href="resources/bootstrap/css/bootstrap.css" rel="stylesheet">
	<link type="text/css" href="resources/bootstrap/css/bootstrap-responsive.css" rel="stylesheet">
	
	<style type="text/css" title="currentStyle">
		@import "DataTables/media/css/demo_table.css";
	</style>
	
	<link rel="stylesheet" href="resources/css/redmond/jquery-ui-1.8.18.custom.css" type="text/css" />
	
	<script type="text/javascript" src="resources/js/lib/jquery.min.js"></script>
	<script type="text/javascript" src="resources/bootstrap/js/bootstrap.js"></script>

	<script type="text/javascript" src="resources/js/app/appJSP.js"></script>
	
	<script src="resources/js/app/jsProvider.js"></script>
	
	<script type="text/javascript" src="resources/js/jquery-ui-1.8.18.custom.min.js"></script>
	
	<script type="text/javascript" src="resources/js/app/jsRecolector.js" charset="utf-8"></script>
	
	<script type="text/javascript" src="resources/js/app/jsIndexer.js" charset="utf-8"></script>
	
	<script type="text/javascript" src="resources/js/app/jsValidator.js" charset="utf-8"></script>
	
	<script type="text/javascript" src="resources/js/spin.min.js"></script>
	
	<script type="text/javascript" charset="utf-8">

		//Opciones del spinner (loading)
		var opts = {
				lines: 16, // The number of lines to draw
				length: 1, // The length of each line
				width: 5, // The line thickness
				radius: 16, // The radius of the inner circle
				color: '#fff', // #rgb or #rrggbb
				speed: 1, // Rounds per second
				trail: 73, // Afterglow percentage
				shadow: false // Whether to render a shadow
		};
	
	</script>
	
	<script type="text/javascript">
	  	arrayJs= new Array(<% ArrayMetadataPrefix.size(); %>);
	  	<% for(String i : ArrayMetadataPrefix){  	%>
	  		arrayJs.push('<%=i.trim()%>'); 
	  	<%} %>
  	</script>
	
</head>
<body onload="loadPage('<%= pagina %>')">
	<% sesion.setAttribute("pagina", null); %>
	<div class="navbar navbar-inverse navbar-fixed-top">
		<div class="navbar-inner">
			<div class="container">
				<button type="button" class="btn btn-navbar" data-toggle="collapse"
					data-target=".nav-collapse">
					<span class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="brand" href="#">Administraci&oacute;n BDNG</a>
				<div class="nav-collapse collapse">
					<div class="pull-right">
						<a href="javascript:;" class="brand"><%= user %></a>
						<a href="logout.jsp" class="btn" onclick="logout()"> Salir</a>
					</div>
				</div>
				<!--/.nav-collapse -->
			</div>
		</div>
	</div>

	<div class="container">
		<div class="hero-unit">
			<h1>BDNG</h1>
			<p>Aplicaci&oacute;n para la administraci&oacute;n de la Biblioteca Digital de Nueva Generaci&oacute;n BDNG</p>
			<!--p><a href="#" class="btn btn-primary btn-large">Learn more &raquo;</a></p-->
		</div>

		<div class="row">
			<div class="span2 breadcrumb" id="menuAdmin">
			    <ul class="nav nav-list sidenav">
			    	<li class="nav-header">General</li>
			    	<li><a href="javascript:;" onclick="loadPage('ServerConf.jsp')">Configuraci&oacute;n</a></li>
				    <li class="nav-header">Repositorios</li>
				    <li><a href="javascript:;" onclick="loadPage('Provider.jsp')">Agregar</a></li>
				    <li><a href="javascript:;" onclick="loadPage('ImportarProvider.jsp')">Importar</a></li>
					<li><a href="javascript:;" onclick="loadPage('ListProviders.jsp')">Editar</a></li>
					<!--li><a href="javascript:;" onclick="loadPage('ListRepositories.jsp')">Listar Sencillo</a></li-->
					<li><a href="javascript:;" onclick="loadPage('ListRepositoriesDetails.jsp')">Listado</a></li>
					<!--li><a href="javascript:;" onclick="loadPage('TreeRepositories.jsp')">Navegar</a></li-->
					<!--li><a href="javascript:;" onclick="loadPage('ListToPublish.jsp')">Aprobador</a></li-->
					<li class="nav-header">Recolector</li>
					<li><a href="javascript:;" onclick="loadPage('recolector.jsp')">Recolectar</a></li>
					<li><a href="javascript:;" onclick="loadPage('StatsForHarvester.jsp')">Estad&iacute;sticas</a></li>
					<li class="nav-header">Validador</li>
				    <li><a href="javascript:;" onclick="loadPage('ListValidations.jsp')">Validar OAI_PMH</a></li>
				    <li><a href="javascript:;" onclick="loadPage('ListValidationsReda.jsp')">Validar XSD</a></li>
				    <li><a href="javascript:;" onclick="loadPage('LogsValidationsReda.jsp')">Ver Logs</a></li>
					<li class="nav-header">Indexador</li>
					<li><a href="javascript:;" onclick="loadPage('Indexador.jsp')">Indexar</a></li>
					
					<li class="divider"></li>
					<li><a onclick="logout()" href="logout.jsp">Salir</a></li>
			    </ul>
			</div>
			<div id="main" class="span10">
				
			</div>
		</div>

		<hr>

		<footer>
		<p>&copy; BDNG 2007 - 2013 - Aplicaci&oacute;n creada por la Universidad EAFIT bajo la licencia <a href="http://www.gnu.org/copyleft/lesser.html" target="_blank">GNU LPGL</a></p>
		</footer>
	</div>
	
	<script type="text/javascript">
	<%
		if(barraCargando != null){
			if(barraCargando.equals("true")){
	%>
				init_check_indexer();
	<%
			}
		}
	%>
	</script>
	<% sesion.setAttribute("barraCargando", "false"); %>

</body>
</html>