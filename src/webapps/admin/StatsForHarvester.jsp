

<%@page import="java.util.Arrays"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Vector"%>
<%@page import="java.util.List"%>
<%@page import="org.bdng.harvmgr.dlConfig"%>
<%@page import="org.jdom2.Document"%>
<%@page import="org.jdom2.Element"%>
<%@page import="org.jdom2.Element.*"%>
<%@page import="org.jdom2.input.SAXBuilder" %>
<%@page import="org.bdng.harvmgr.harvester.DataProvider"%>
<%@page import="org.bdng.webold.ProviderWeb"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
    <%    //verificamos session como "admin" sino, lo redirijimos al index.jsp
	HttpSession sesion = request.getSession();
    String user ="";
    String type = "";
    if(sesion.getAttribute("username")== null && sesion.getAttribute("type")== null) {
    	response.sendRedirect("errorSession.jsp");
    	
	}else{
		user = (String)sesion.getAttribute("username");
    	type = (String)sesion.getAttribute("type");
    		if(!user.equals("admin") || !type.equals("admin"))
    			response.sendRedirect("errorSession.jsp");
	} %>
    
    
    <%!
    
    

    

	  int posicion (String repositoryName, Vector<String> repositoryNames ){
		int pos = repositoryNames.size()-1;
		
		if (repositoryNames.contains(repositoryName)) {
			
			 pos =repositoryNames.indexOf(repositoryName);
	
		}else{
			repositoryNames.add(repositoryName);
			pos = repositoryNames.size()-1;
			
		}
		return pos;
		
	};
	
	  int fechas (String fecha, Vector<String> fechas ){
		int pos = fechas.size()-1;
		
		if (fechas.contains(fecha)) {
			
			 pos =fechas.indexOf(fecha);
	
		}else{	
			fechas.add(fecha);
			pos = fechas.size()-1;
			
		}
		return pos;
		
	};

%>
    
    <%
    
    	
    	dlConfig doa = new dlConfig(this.getServletContext().getRealPath("/"));
    	List l = doa.GetVisualStatsForHarvester(); 
   		//out.print("Numero de dataproviders "+l.size());
    	
		
		
		
	//Element harvester = null;
	Element dataProvider = null;
	
	Element id = null;
	  

  	
  	    %>

		<div  id="container">
			<div class="full_width big"></div>
			<div id="title_section" class="text-center">
				<h2>Estadisticas de recoleccion y estado</h2>
			</div> 
				<div  id="demo">
<form name="control" method="post" action="">
  <table  cellpadding="0" cellspacing="0" border="0" class="display" id="example">
    <thead>
    <tr>
        <%
        out.print("<th>Institucion</th>");
        out.print("<th>Repositorio</th>");
        String anterior = "-";
        int contF =0;
		for(int j = 0; j<l.size();j++){
			dataProvider=(Element)l.get(j);
			Element date=dataProvider.getChild("date");
			if(anterior.equals("-")||!anterior.equals(date.getText())){
				out.print("<th>"+date.getText()+"</th>");
				out.print("<th>Estado</th>");
				
				contF++;
			}
			anterior=date.getText();
		}
		
			%>
			

    </tr>
    </thead>
    	<tbody>
    <%
    
   
   

    List ldp = null;
	Vector<String> repositoryNames = new Vector<String>();
	Vector<String> fechas = new Vector<String>();
	
	HashMap<String, String> institutions = new HashMap <String, String>();
	HashMap<Integer, String[][]> linea = new HashMap<Integer,  String[][]>();
	
	String mat[][] = new String[1000][contF];
 	for (int i = 0; i < mat.length; i++) {
		Arrays.fill(mat[i], "-");
		
	} 

   
    	
    	dataProvider = null;
    	
    	Element repositoryName = null;
    	Element institution = null;
    	Element successfull = null;
    	Element date = null;
    	Element number_registers = null;
    	
    	

	    	for(int k = 0; k<l.size();k++){
	    		
	    		dataProvider=(Element)l.get(k);
	    		
	    		
	    		repositoryName=dataProvider.getChild("repositoryName");
	    		institution=dataProvider.getChild("institution");
	    		successfull=dataProvider.getChild("successfull");
	  			number_registers=dataProvider.getChild("number_registers");
	  			date=dataProvider.getChild("date");
	  			
	  			
			
	  			String sTrepositoryName= repositoryName.getText();
	  			String sTinstitution= institution.getText();
	  			String sTnumber_registers= number_registers.getText();
	  			String sTsuccessfull= successfull.getText().equals("true")?"OK":"Error";
	  			String sTdate= date.getText();
	  			
	  			String cont = sTnumber_registers+ " " +sTsuccessfull;
	  			int pos = posicion(sTrepositoryName,repositoryNames);
				int posfecha = fechas(sTdate,fechas );
				mat[pos][posfecha]= cont;
				
				institutions.put(sTrepositoryName,sTinstitution);

				linea.put(pos, mat);
	  	
  	
	    	}
	    	
	    
      
			
			Iterator it = linea.entrySet().iterator();
			while (it.hasNext()) {
				out.print("<tr class=\"gradeU\">");
				Map.Entry e = (Map.Entry)it.next();
				//System.out.print(e.getKey()+" "+repositoryNames.get((Integer)e.getKey()) );
				int indice = (Integer) e.getKey();
				String[][] m = (String[][]) e.getValue();
				String repositoryNameDisplay = repositoryNames.get(indice);
				String institutionDisplay = institutions.get(repositoryNameDisplay);
				
				out.print("<td align=\"left\">"+institutionDisplay+"</td>");
				out.print("<td align=\"left\">"+repositoryNameDisplay+"</td>");
					for (int i = 0; i < contF; i++) {
						if(m[indice][i].equals("-")){
							out.print("<td align=\"center\">-</td>");
							out.print("<td align=\"center\">-</td>");
						}
						else{
						String numreg = m[indice][i].substring(0,m[indice][i].indexOf(" "));
						String status = m[indice][i].substring(m[indice][i].indexOf(" "));
						
						
						out.print("<td align=\"center\">"+numreg+"</td>");
						out.print("<td align=\"center\">"+status+"</td>");
						}	
					}
					
					out.print("</tr>");
		
			}
			
			out.print("<tr class=\"gradeU\">");
			out.print("<td colspan=\"2\" align=\"center\"><strong>Total</strong></td>");
// 			out.print("<td align=\"left\"><strong>Total</strong></td>");
// 			Iterator it2 = null;
			for (int i = 0; i < contF; i++) {
				Iterator it2 = linea.entrySet().iterator();
				String totalRegistrosString = "";
				int totalRegistrosInt = 0;
				String status = "";
				while (it2.hasNext()) {
					Map.Entry e = (Map.Entry)it2.next();
					int indice = (Integer) e.getKey();
					String[][] m = (String[][]) e.getValue();
					if(m[indice][i].equals("-")){
						status = "OK";
					}
					else{
						String numreg = m[indice][i].substring(0,m[indice][i].indexOf(" "));
						status = m[indice][i].substring(m[indice][i].indexOf(" "));
						
						totalRegistrosInt = totalRegistrosInt + Integer.parseInt(numreg);
					}
				}
				out.print("<td align=\"center\">"+totalRegistrosInt+"</td>");
				out.print("<td align=\"center\">"+status+"</td>");
				
			}
			out.print("</tr>");
  
    %>
    </tbody>
	
</table>

  </form>

</div>
</div>