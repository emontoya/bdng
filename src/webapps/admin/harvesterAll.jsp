<%@ page import  = "org.bdng.oai.db.DBExist,java.util.*"%>
<%
	try {
		DBExist db = DBExist.getInstance();
		Vector sets = new Vector(db.loadSets());
%>
<html>
	<head>
<title>OAI Harvesting Interface</title>
<META HTTP-EQUIV="Content-Type"
			content="text/html; charset=iso-8859-1">
		<META HTTP-EQUIV="Cache-Control" CONTENT="no-cache">
		<META HTTP-EQUIV="Cache-Control" CONTENT="no-store">
		<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
		<META HTTP-EQUIV="Expires" CONTENT="0">
		<META HTTP-EQUIV="Content-Encoding" CONTENT="gzip">
		<link rel="stylesheet" href="styles/styletxt2xml.css"
			type="text/css">
            		<link rel="stylesheet" href="styles/layout.css"
			type="text/css">
<script language="JavaScript">

	function MM_findObj(n, d) 
	{
				 var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
				  d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);
				 }
				 if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
				 for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
				 if(!x && document.getElementById) x=document.getElementById(n); return x;
	}
	
	function MM_showhideLayers() 
	{
				 var i,p,v,obj,args=MM_showhideLayers.arguments; 
				 for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) {
				  v=args[i+2]; z=args[i+3]; 
				  if (!z) {
				   if (obj.style) { obj=obj.style; v=(v=='show')?'block':(v=='hide')?'none':v; } 
				   obj.display=v; 
				  } else {
				   if (obj.style) { obj=obj.style; v=(v=='show')?z:(v=='hide')?'none':v; } 
				   obj.display=v; 
				  }
				 }
	} 
	
	function showVerb(verb)
	{
		MM_showhideLayers('Identify','','hide');
		MM_showhideLayers('ListMetadataFormats','','hide');
		MM_showhideLayers('ListIdentifiers','','hide');
		MM_showhideLayers('ListSets','','hide');
		MM_showhideLayers('ListRecords','','hide');
		MM_showhideLayers('GetRecord','','hide');

		MM_showhideLayers(verb,'','show');		
	}

	function submitForm()
	{
		URL = document.sendQuery.oaiURL.value;
		verb = document.sendQuery.verb.value;
		MetadataPrefix = document.sendQuery.MetadataPrefix.value;
		from = document.sendQuery.from.value;
		until = document.sendQuery.until.value;
		set = document.sendQuery.set.value;
		identifier = document.sendQuery.identifier.value;
		resumptionToken = document.sendQuery.resumptionToken.value;
		formStatus = true;
		if (!URL)
		{
			alert("Es necesario llenar la lacula direcci�n electr�nica");
			formStatus = false;
		}
		if (verb == "Identify")
		{
			URL = URL+"?verb="+verb;
		}
		else if (verb == "ListMetadataFormats")
		{
			URL = URL+"?verb="+verb;
		}
		else if (verb == "ListIdentifiers")
		{
			URL = URL+"?verb="+verb;
			if (MetadataPrefix) 
			{URL = URL+"&metadataPrefix="+MetadataPrefix;}
			else 
			{ 
			alert("el par�metro metadataPrefix es obligat�rio");
			formStatus = false;
			}
			if (from) URL = URL+"&from="+from;
			if (until) URL = URL+"&until="+until;
			if (set) URL = URL+"&set="+set;
			if (resumptionToken) URL = URL+"&resumptionToken="+resumptionToken;
		}
		else if (verb == "ListSets")
		{
			URL = URL+"?verb="+verb;
			if (resumptionToken) URL = URL+"&resumptionToken="+resumptionToken;			
		}
		else if (verb == "ListRecords")
		{
			URL = URL+"?verb="+verb;
			if (MetadataPrefix) 
			{
				if(MetadataPrefix == 'oai_dc_agris'){
					if(!set){
						alert("el par�metro set es obligat�rio");
						formStatus = false;
					} 
				}
				URL = URL+"&metadataPrefix="+MetadataPrefix;}
			else 
			{ 
			alert("el par�metro metadataPrefix es obligat�rio");
			formStatus = false;
			}
			if (from) URL = URL+"&from="+from;
			if (until) URL = URL+"&until="+until;
			if (set) URL = URL+"&set="+set;			
			if (resumptionToken) URL = URL+"&resumptionToken="+resumptionToken;	
		}
		else if (verb == "GetRecord")
		{
			URL = URL+"?verb="+verb;
			if (MetadataPrefix) 
			{URL = URL+"&metadataPrefix="+MetadataPrefix;}
			else 
			{ 
			alert("el par�metro metadataPrefix es obligat�rio");
			formStatus = false;
			}
			if (identifier) 
			{URL = URL+"&identifier="+identifier;}
			else 
			{ 
			alert("el par�metro identifier es obligat�rio");
			formStatus = false;
			}						
		}
		else
		{
			alert("el par�metro verb es obligat�rio");
			formStatus = false;
		}
		if (formStatus)
		{
			alert("URL ES "+ URL);	
			document.sendQuery.outputURL.value = URL;
			window.open(URL,'oai_output');
		}
	}
	function setForm(verb)
	{
		for (i=0 ; i<document.sendQuery.elements.length ; i++)
		{	
			if (document.sendQuery.elements[i].name != "oaiURL")
				document.sendQuery.elements[i].disabled = true;
		}
		
		if (verb == "Identify")

		{
			document.sendQuery.verb.value = "Identify";
		}
		else if (verb == "ListMetadataFormats")
		{
			document.sendQuery.verb.value = "ListMetadataFormats";
		}
		else if (verb == "ListIdentifiers")
		{
			document.sendQuery.verb.value = "ListIdentifiers";
			document.sendQuery.MetadataPrefix.disabled = false;
			document.sendQuery.from.disabled = false;
			document.sendQuery.until.disabled = false;
			document.sendQuery.set.disabled = false;
			document.sendQuery.resumptionToken.disabled = false;										
		}
		else if (verb == "ListSets")
		{
			document.sendQuery.verb.value = "ListSets";
			document.sendQuery.resumptionToken.disabled = false;		
		}
		else if (verb == "ListRecords")
		{
			document.sendQuery.verb.value = "ListRecords";
			document.sendQuery.MetadataPrefix.disabled = false;
			document.sendQuery.from.disabled = false;
			document.sendQuery.until.disabled = false;
			document.sendQuery.set.disabled = false;
			document.sendQuery.resumptionToken.disabled = false;
		}
		else if (verb == "GetRecord")
		{
			document.sendQuery.verb.value = "GetRecord";
			document.sendQuery.MetadataPrefix.disabled = false;			
			document.sendQuery.identifier.disabled = false;			
		}		
	}
</script>
	</head>
	<body class="twoColElsLtHdr">
		<div id="container">
		  <div id="header">
    <h1><tbody><tr>
					<td>
						<table border="0" cellpadding="0" cellspacing="0" width="100%">  <tbody><tr>    <td bgcolor="#b6c589" width="100%"><table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">      <tbody><tr>        <td><a href="http://www.eafit.edu.co/biblioteca.shtm" target="_blank"><img src="img/eafit/titulo.gif" alt="titulo" border="0" height="15" width="505"></a></td>      </tr>    </tbody></table></td>  </tr></tbody></table><table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">  
						  <tbody><tr>        <td width="795"><table border="0" cellpadding="0" cellspacing="0" width="100%">      <tbody><tr>        <td align="left"><img src="img/eafit/libro.jpg" height="90" width="55"></td>        <td align="center"><img src="img/eafit/titleDigital.gif" height="39" width="274"></td>        
						<td align="center"><img src="img/eafit/img.jpg" height="90" width="348"></td>        <td align="right"><a href="http://www.eafit.edu.co/" target="_blank"><img src="img/eafit/logoVerde.gif" alt="Regreso a página principal" border="0" height="89" width="118"></a></td>      </tr>    </tbody></table></td>      
						  </tr></tbody></table><a name="subir"></a></td>
				</tr>
				<tr>
					<td> <table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">
  </table>
<map name="Map" id="Map"><area shape="rect" coords="740,2,762,19" href="http://www.eafit.edu.co/biblioteca.shtm" alt="Volver al Home"><area shape="rect" coords="6,3,87,19" href="http://www.eafit.edu.co/EafitCn/Biblioteca/servicios/"><area shape="rect" coords="105,2,241,19" href="http://www.eafit.edu.co/EafitCn/Biblioteca/digital/"><area shape="rect" coords="264,3,379,19" href="http://www.eafit.edu.co/EafitCn/Biblioteca/recomendados/"><area shape="rect" coords="402,2,468,20" href="http://www.eafit.edu.co/EafitCn/Biblioteca/enlaces/"><area shape="rect" coords="486,3,602,18" href="http://www.eafit.edu.co/EafitCn/Biblioteca/generalidades/"><area shape="rect" coords="622,2,727,18" href="http://www.eafit.edu.co/Eafit/Includes/biblioteca/contactenos/contactenos.shtm">
  






</map>

<map name="Map2" id="Map2"><area shape="rect" coords="371,1,455,22" href="http://www.eafit.edu.co/buscador/"><area shape="rect" coords="196,1,340,24" href="http://www.eafit.edu.co/biblioteca/metabiblioteca/index.htm" target="_blank"><area shape="rect" coords="-1,1,157,22" href="http://zeta.eafit.edu.co:8080/sinbad/">


</map>
<map name="Map3" id="Map3"><area shape="rect" coords="2,1,96,23" href="http://www.eafit.edu.co/EafitCn/Biblioteca/english/services">
</map></td>
				</tr>
	</tbody></h1>
  <!-- end #header --></div>
			<div id="mainContent3">
<form name="sendQuery" method="post" action="#">
  <p align="center"><strong><b>INTERFAZ DE RECOLECCI&Oacute;N EN LINEA OAI</b></strong></p>
<div id="main_group">
	<div id="group_title">OAI URL</div>
	<div id="group_content">URL: 
	  <input name="oaiURL" size="70" maxlength="70" type="text"> &nbsp; &nbsp; http://www.scielo.br/oai/scielo-oai.php</div>			
</div>	
<div>
	<table width="97%" id="table4" align="center" border="1" cellspacing="1" cellpadding="1">
		<tbody><tr>
			<td valign="top" width="30%">
			<div id="left">			
				<div id="group">
					<div id="group_title">verbs</div>
					<div id="group_content">
						<div id="menu_item"><a href="javascript:%20setForm('Identify');%20showVerb('Identify');">Identify</a></div>
						<div id="menu_item"><a href="javascript:%20setForm('ListMetadataFormats');%20showVerb('ListMetadataFormats');">ListMetadataFormats</a></div>
						<div id="menu_item"><a href="javascript:%20setForm('ListIdentifiers');%20showVerb('ListIdentifiers');">ListIdentifiers</a></div>
						<div id="menu_item"><a href="javascript:%20setForm('ListSets');%20showVerb('ListSets');">ListSets</a></div>
						<div id="menu_item"><a href="javascript:%20setForm('ListRecords');%20showVerb('ListRecords');">ListRecords</a></div>
						<div id="menu_item"><a href="javascript:%20setForm('GetRecord');%20showVerb('GetRecord');">GetRecord</a></div>																								
					</div>			
				</div>
				<div id="group">
					<div id="group_title">descripci�n del verb</div>
					<div id="group_content">
						<div id="Identify" name="Identify" style="display: none;">
							<p>Este verb retorna las informaciones principales del protocolo.</p>
							<p>
							<b>Par�metros Obligat�rios</b>
							</p><ul><li>verb</li></ul>
							
						</div>
						<div id="ListMetadataFormats" name="ListMetadataFormats" style="display: none;">
							<p>Lista cuales son los formatos de metadados utilizados en el protocolo, la SciELO utiliza el oai_dc.</p>
							<p>
							<b>Par�metros Obligat�rios</b>
							</p><ul>
								<li>verb</li>
								</ul>
														
						</div>
						<div id="ListIdentifiers" name="ListIdentifiers" style="display: none;">
							<p>Recupera una lista con el identificador �nico de los art�culo publicado.</p>
							<p>
							<b>Par�metros Obligat�rios</b>
							</p><ul>
								<li>verb</li>
								<li>metadataPrefix</li>								
							</ul>
							
							<p>
							<b>Par�metros Opcionales</b>
							</p><ul>
								<li>from</li>
								<li>until</li>
								<li>set</li>								
							</ul>
							<p>
							<b>Par�metros Exclusivos</b>
							</p><ul>
								<li>resumptionToken</li>
							</ul>
																
						</div>
						<div id="ListSets" name="ListSets" style="display: none;">
							<p>Recupera una lista con todas las revistas publicadas en la SciELO, informando el t�tulo y el setSpec (issn).</p>
							<p>
							<b>Par�metros Obligat�rios</b>
							</p><ul>
								<li>verb</li>
							</ul>
								
							<p>
							<b>Par�metros Exclusivos</b>
							</p><ul>
								<li>resumptionToken</li>
							</ul>
																
													
						</div>
						<div id="ListRecords" name="ListRecords" style="display: none;">
							<p>Recupera el resumen de los art�culos.</p>
							<p>
							<b>Par�metros Obligat�rios</b>
							</p><ul>
								<li>verb</li>
								<li>metadataPrefix</li>								
							</ul>
							
							<p>
							<b>Par�metros Opcionales</b>
							</p><ul>
								<li>from</li>
								<li>until</li>
								<li>set</li>								
							</ul>
							<p>
							<b>Par�metros Exclusivos</b>
							</p><ul>
								<li>resumptionToken</li>
							</ul>
																						
						</div>
						<div id="GetRecord" name="GetRecord" style="display: block;">
							<p>Recupera el metadado de un art�culo espec�fico.</p>
							<p>
							<b>Par�metros Obligat�rios</b>
							</p><ul>
								<li>verb</li>
								<li>metadataPrefix</li>						
								<li>identifier</li>								
							</ul>
														
						</div>																														
					</div>			
				</div>		
			</div>
			</td>
			<td valign="top" width="70%">
			<div id="right">
				<div id="group">
					<div id="group_title">configuraci�n de los par�metros</div>
					<div id="group_content">
							<table>
								<tbody><tr>
									<td>verb</td>
									<td><input name="verb" disabled="disabled" type="text"></td>
									<td></td>
								</tr>							
								<tr>
									<td>MetadataPrefix</td>
									<td><input name="MetadataPrefix" maxlength="15" type="text"></td>
									<td>oai_dc | oai_dc_agris</td>
								</tr>
								<tr>
									<td>set</td>
									<td><input disabled="disabled" name="set" size="9" maxlength="9" type="text"></td>
									<td>ISSN</td>
								</tr>	
								<tr>
									<td>identifier</td>
									<td><input name="identifier" size="34" maxlength="34" type="text"></td>
									<td></td>
								</tr>	
								<tr>
									<td>from</td>
									<td><input disabled="disabled" name="from" size="10" maxlength="10" type="text"></td>
									<td>YYYY-MM-DD</td>
								</tr>		
								<tr>
									<td>until</td>
									<td><input disabled="disabled" name="until" size="10" maxlength="10" type="text"></td>
									<td>YYYY-MM-DD</td>									
								</tr>	
								<tr>
									<td>resumptionToken</td>
									<td><input disabled="disabled" name="resumptionToken" size="34" type="text"></td>
									<td></td>									
								</tr>	
							</tbody></table>
							<p align="left">
							<a href="javascript:%20submitForm();">Recolectar</a>
							</p>		
					</div>			
				</div>
				<div id="group">
					<div id="group_title">salida</div>
					<div id="group_content">
						<div id="output">
							Direcci�n electr�nica recolectada:
							<input disabled="disabled" name="outputURL" size="100" type="text">
							Resultado:
							<iframe name="oai_output" width="99%">
						&lt;/div&gt;
					&lt;/div&gt;			
				&lt;/div&gt;	
			&lt;/div&gt;
			&lt;/td&gt;
		&lt;/tr&gt;
	&lt;/table&gt;	
&lt;/div&gt;
&lt;/FORM&gt;
&lt;/body&gt;
&lt;/html&gt;
</iframe></div></div></div></div></td></tr></tbody></table></div></form>
			</div>
			<div id="footer">
				<p align="center">
					� Biblioteca Luis Echavarr�a Villegas - Universidad EAFIT -
					Medell�n Colombia � Carrera 49 No. 7 Sur-50 - Avenida Las Vegas -
					Tel�fono (57) (4) 261 95 00 - Ext. 261 AA 3300 - Fax (57) (4) 266
					42 84
				</p>
				<!-- end #footer -->
			</div>
			<!-- end #container -->
		</div>
</body>
</html>
<%}catch(Exception e) {%>
Error
<%
}
%>
