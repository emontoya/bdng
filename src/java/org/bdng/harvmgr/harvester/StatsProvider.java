package org.bdng.harvmgr.harvester;

import java.util.Date;

/**
 * Gestiona los stats de un data provider en una recoleccion
 * 
 * @author sebastian
 * @since 28/04/2010
 * 
 */
public class StatsProvider {

	private String shortName;
	private String institution;
	private String RepositoryName;
	private int number_files;
	private boolean successfull = true;
	private long harvesterTime;
	private long number_registers;
	private Date date;

	public String getRepositoryName() {
		return RepositoryName;
	}

	public void setRepositoryName(String repositoryName) {
		RepositoryName = repositoryName;
	}

	public void setInstitution(String institution) {
		this.institution = institution;
	}

	public String getInstitution() {
		return institution;
	}

	/**
	 * @return the shortName
	 */
	public String getShortName() {
		return shortName;
	}

	/**
	 * @param shortName
	 *            the shortName to set
	 */
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	/**
	 * @return the number_files
	 */
	public int getNumber_files() {
		return number_files;
	}

	/**
	 * @param numberFiles
	 *            the number_files to set
	 */
	public void setNumber_files(int numberFiles) {
		number_files = numberFiles;
	}

	/**
	 * @return the successfull
	 */
	public boolean isSuccessfull() {
		return successfull;
	}

	/**
	 * @param successfull
	 *            the successfull to set
	 */
	public void setSuccessfull(boolean successfull) {
		this.successfull = successfull;
	}

	/**
	 * @return the harvesterTime
	 */
	public long getHarvesterTime() {
		return harvesterTime;
	}

	/**
	 * @param harvesterTime
	 *            the harvesterTime to set
	 */
	public void setHarvesterTime(long harvesterTime) {
		this.harvesterTime = harvesterTime;
	}

	/**
	 * @return the number_registers
	 */
	public long getNumber_registers() {
		return number_registers;
	}

	/**
	 * @param numberRegisters
	 *            the number_registers to set
	 */
	public void setNumber_registers(long numberRegisters) {
		number_registers = numberRegisters;
	}

	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * @param date
	 *            the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}

}
