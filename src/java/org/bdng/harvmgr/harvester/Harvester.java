package org.bdng.harvmgr.harvester;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Semaphore;

import org.apache.log4j.Logger;
import org.bdng.harvmgr.dlConfig;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;

/**
 * La clase <code>Harvester.java</code>

 * <p>
 * Se encarga de gestionar todos los proveedores de datos, para luego delegar la
 * recolecci_n de estos procedimientos.
 * </p>
 * 
 * @author C_stulo Ram_rez Londo_o. UNIVERSIDAD EAFIT.
 * @version 1.0, 01-jun-2008
 * @since JDK1.6.0_05
 */
/**
 * @author eafit
 * 
 */

public class Harvester extends Thread {

	Logger log = Logger.getLogger(Harvester.class);

	OAIHarvester oAIHarvester = null;
	HTTPHarvester hTTPHarvester = null;
	// HTTPHarvester recolectorrepositoriosURL = new HTTPHarvester();

	// Scheduler sh = new Scheduler();

	// semaforo que indica que ha acabado un modo de recoleccion
	static Semaphore harvester = new Semaphore(1);

	boolean working = false;
	String FILE_OAI_PROVIDERS = null;
	String FILE_OAI_PROVIDERS_STATUS = null;
	String strRuta;
	String strOption[];
	dlConfig dlconfig = null;
	private Thread[] hilosActuales = null;

	synchronized public boolean isWorking() {
		return working;
	}

	synchronized public void setWorking(boolean val) {
		working = val;
	}

	public String path = null;

	public Harvester() {
		dlconfig = new dlConfig();
		path = dlconfig.getString("dl_home");
		FILE_OAI_PROVIDERS = dlconfig.getOaiProviders();
		FILE_OAI_PROVIDERS_STATUS = dlconfig.getOaiProvidersStatus();
		oAIHarvester = new OAIHarvester(path);
	}

	public Harvester(String strRuta, String strOption[]) {
		this.strRuta = strRuta;
		this.strOption = strOption;
		dlconfig = new dlConfig(strRuta);
		path = dlconfig.getString("dl_home");
		FILE_OAI_PROVIDERS = dlconfig.getOaiProviders();
		FILE_OAI_PROVIDERS_STATUS = dlconfig.getOaiProvidersStatus();
		System.out
				.println("******** Iniciando Recoleccion de Metadatos ********");
		oAIHarvester = new OAIHarvester(path);
		hTTPHarvester = new HTTPHarvester(path);
	}

	/**
	 * METODO PRINCIPAL DE RECOLECCION. GESTIONA ESTADISTICA E INICIA HILOS
	 */
	private void configurarAplicacion(String Repository[]) {
		int numeroHilosACrear = Integer.parseInt(dlconfig
				.getString("number_threads"));
		Collection<DataProvider> dataProviderCol = null;
		if (Repository[0].equals("all")) {
			dataProviderCol = this.getDataProviders();
		} else {
			dataProviderCol = this.getDataProvider(Repository);
		}
		
		if(dataProviderCol.size() != 0){
			/* Inicializa gestor de estatistica */
			StatsManager.init(dataProviderCol, dlconfig);
			long totalInitTime = System.currentTimeMillis();
			try {
				harvester.acquire();
			} catch (InterruptedException e) {
			}
			/* Inicia recoleccion */
			recolectarProveedores(dataProviderCol, numeroHilosACrear);
			try {
				harvester.acquire();
				/*
				 * Lo que viene despues de este semaforo, se hace al finalizar todo
				 * el proceso de recoleccion y procesamiento
				 */
			} catch (InterruptedException e) {
			}
			long totalEndTime = System.currentTimeMillis();
			/* guardo la estadistica en disco */
			/*
			 * try { graba estadisticas en disco
			 * StatsManager.getInstance().writeStatsInDisk(); } catch (JDOMException
			 * e) { log.error(e); } catch (IOException e) { log.error(e); }
			 */
	
			HarvesterThread.dataProviderActual = 0;// add by edy
		}else{
			System.out.println("No se encontraron proveedores para recolectar");
		}
		System.out
				.println("******** La recoleccion de metadatos ha finalizado ********");

	}

	private void recolectarProveedores(
			Collection<DataProvider> dataProviderCol, int numeroHilos) {
		hilosActuales = new Thread[numeroHilos];
		HarvesterThread
				.setVariablesGlobales(strRuta, dataProviderCol.toArray());
		for (int i = 0; i < numeroHilos; i++) {
			HarvesterThread ht = new HarvesterThread();
			Thread temp = new Thread(ht);
			hilosActuales[i] = temp;
			temp.start();
		}
	}

	/**
	 * Obtiene los proveedores de repositorios OAI a partir de
	 * bdng_oai_providers. Se mapean por la clase DataProvider.
	 * 
	 * @return
	 */
	public Collection<DataProvider> getDataProviders() {
		Collection<DataProvider> dataprovidersCol = new ArrayList<DataProvider>();
		DataProvider dataProvider = null;
		String idrep = null;
		try {
			// if ((stRuta != null) && (stRuta.trim().length() != 0)) {
			// stRuta = stRuta.trim();
			// }
			SAXBuilder builder = new SAXBuilder();
			Document doc = builder.build(FILE_OAI_PROVIDERS);
			// System.out.println("nueva ruta="+stRuta +FILE_OAI_PROVIDERS);
			Element raiz = doc.getRootElement();
			Element providers = raiz.getChild(DataProvider.OAI_PROVIDERS);
			List provider = providers.getChildren(DataProvider.OAI_PROVIDER);
			Iterator i = provider.iterator();
			while (i.hasNext()) {
				dataProvider = new DataProvider(path);
				// dataProvider.setRuta(path);
				Element l = (Element) i.next();

				Element e = l.getChild(DataProvider.SHORTNAME);
				dataProvider.setShortName(e.getText());
				idrep = e.getText();

				e = l.getChild(DataProvider.REPOSITORY_NAME);
				dataProvider.setRepositoryName(e.getText());

				e = l.getChild(DataProvider.BASE_URL);
				dataProvider.setBaseURL(e.getText());

				e = l.getChild(DataProvider.DESCRIPTION);
				dataProvider.setDescription(e.getText());

				dataProvider.setCdata(Integer.parseInt(dataProvider.getText(
						idrep, DataProvider.CDATA)));
				dataProvider.setResumptionWithMetadataPrefix(Integer
						.parseInt(dataProvider.getText(idrep,
								DataProvider.RESUMPTION)));
				// e = (Element) l.getChild(DataProvider.RESUMPTION);
				// dataProvider.setResumptionWithMetadataPrefix(Integer.parseInt(e.getText()));

				e = l.getChild(DataProvider.METADATA_PREFIX);
				dataProvider.setMetadataPrefix(e.getText());

				e = l.getChild(DataProvider.COLLECTION);
				dataProvider.setCollection(e.getText());

				e = l.getChild(DataProvider.PROTOCOL);
				dataProvider.setProtocol(e.getText());
				// e = (Element) l.getChild(DataProvider.SCHEDULE_STATUS);

				// TODO: Active status
				// e = (Element) l.getChild(DataProvider.STATUS);
				// dataProvider.setStatus(e.getText());

				if (isDataProviderReady(idrep) /*
												 * && dataProvider.getStatus().
												 * equalsIngoreCase("activo")
												 */) {
					dataprovidersCol.add(dataProvider);
				}
			}
		} catch (Exception e) {
			log.error(e);
		}
		return dataprovidersCol;
	}

	/**
	 * Obtiene los proveedores de repositorios OAI a partir de
	 * FILE_OAI_PROVIDERS. Se mapean por la clase DataProvider. esta funcion es
	 * exactamente la misma anterior, solo que aquie no verificamos
	 * (isDataProviderReady). porque necesitamos los providers para procesar, no
	 * para harvester
	 * 
	 * @return
	 */

	public Collection<DataProvider> getDataProvidersToProcces() {
		Collection<DataProvider> dataprovidersCol = new ArrayList<DataProvider>();
		DataProvider dataProvider = null;
		String idrep = null;
		try {
			// if ((stRuta != null) && (stRuta.trim().length() != 0)) {
			// stRuta = stRuta.trim();
			// }
			SAXBuilder builder = new SAXBuilder();
			Document doc = builder.build(FILE_OAI_PROVIDERS);
			// System.out.println("nueva ruta="+stRuta +FILE_OAI_PROVIDERS);
			Element raiz = doc.getRootElement();
			Element providers = raiz.getChild(DataProvider.OAI_PROVIDERS);
			List provider = providers.getChildren(DataProvider.OAI_PROVIDER);
			Iterator i = provider.iterator();
			while (i.hasNext()) {
				dataProvider = new DataProvider(path);
				// dataProvider.setRuta(path);
				Element l = (Element) i.next();

				Element e = l.getChild(DataProvider.SHORTNAME);
				dataProvider.setShortName(e.getText());
				idrep = e.getText();

				e = l.getChild(DataProvider.BASE_URL);
				dataProvider.setBaseURL(e.getText());

				e = l.getChild(DataProvider.DESCRIPTION);
				dataProvider.setDescription(e.getText());

				dataProvider.setCdata(Integer.parseInt(dataProvider.getText(
						idrep, DataProvider.CDATA)));
				dataProvider.setResumptionWithMetadataPrefix(Integer
						.parseInt(dataProvider.getText(idrep,
								DataProvider.RESUMPTION)));
				// e = (Element) l.getChild(DataProvider.RESUMPTION);
				// dataProvider.setResumptionWithMetadataPrefix(Integer.parseInt(e.getText()));

				e = l.getChild(DataProvider.METADATA_PREFIX);
				dataProvider.setMetadataPrefix(e.getText());

				e = l.getChild(DataProvider.COLLECTION);
				dataProvider.setCollection(e.getText());

				e = l.getChild(DataProvider.PROTOCOL);
				dataProvider.setProtocol(e.getText());

				dataprovidersCol.add(dataProvider);

			}
		} catch (Exception e) {
			log.error(e);
		}
		return dataprovidersCol;
	}

	/**
	 * 
	 * Se genera una Collection para luego hacer recoleccion de n proveedores,
	 * incluyendo n=1 add by edy
	 * 
	 * @return
	 */
	public Collection<DataProvider> getDataProvider(String Repository[]) {
		Collection<DataProvider> dataprovidersCol = new ArrayList<DataProvider>();
		DataProvider dataProvider = null;
		String idrep = null;
		try {
			SAXBuilder builder = new SAXBuilder();
			Document doc = builder.build(FILE_OAI_PROVIDERS);
			Element raiz = doc.getRootElement();
			Element providers = raiz.getChild(DataProvider.OAI_PROVIDERS);
			List provider = providers.getChildren(DataProvider.OAI_PROVIDER);
			Iterator i = provider.iterator();
			while (i.hasNext()) {
				dataProvider = new DataProvider(path);
				// dataProvider.setRuta(path);
				Element l = (Element) i.next();
				Element e = l.getChild(DataProvider.SHORTNAME);
				// recorremos todo el vector de los providers y vamos comparando
				// con los shortnames del archivo
				for (int j = 0; j < Repository.length; j++) {
					if (e.getText().equals(Repository[j])) {

						dataProvider.setShortName(e.getText());
						idrep = e.getText();

						e = l.getChild(DataProvider.REPOSITORY_NAME);
						dataProvider.setRepositoryName(e.getText());

						e = l.getChild(DataProvider.BASE_URL);
						dataProvider.setBaseURL(e.getText());

						e = l.getChild(DataProvider.DESCRIPTION);
						dataProvider.setDescription(e.getText());

						dataProvider.setCdata(Integer.parseInt(dataProvider
								.getText(idrep, DataProvider.CDATA)));
						dataProvider.setResumptionWithMetadataPrefix(Integer
								.parseInt(dataProvider.getText(idrep,
										DataProvider.RESUMPTION)));
						// e = (Element) l.getChild(DataProvider.RESUMPTION);
						// dataProvider.setResumptionWithMetadataPrefix(Integer.parseInt(e.getText()));

						e = l.getChild(DataProvider.METADATA_PREFIX);
						dataProvider.setMetadataPrefix(e.getText());

						e = l.getChild(DataProvider.COLLECTION);
						dataProvider.setCollection(e.getText());

						e = l.getChild(DataProvider.PROTOCOL);
						dataProvider.setProtocol(e.getText());
						// e = (Element)
						// l.getChild(DataProvider.SCHEDULE_STATUS);

						if (isDataProviderReady(idrep)) {
							dataprovidersCol.add(dataProvider);
						}

					}

				}

			}
		} catch (Exception e) {
			log.error(e);
		}
		return dataprovidersCol;
	}

	// fin add edy

	/**
	 * 
	 * Se genera una Collection para luego hacer processor de n proveedores,
	 * incluyendo n=1 esta funcion es exactamente la misma anterior, solo que
	 * aquie no verificamos (isDataProviderReady). porque necesitamos los
	 * providers para procesar, no para harvester add by edy
	 * 
	 * @return
	 */
	public Collection<DataProvider> getDataProviderToProcess(
			String Repository[]) {
		Collection<DataProvider> dataprovidersCol = new ArrayList<DataProvider>();
		DataProvider dataProvider = null;
		String idrep = null;
		try {
			SAXBuilder builder = new SAXBuilder();
			Document doc = builder.build(FILE_OAI_PROVIDERS);
			Element raiz = doc.getRootElement();
			Element providers = raiz.getChild(DataProvider.OAI_PROVIDERS);
			List provider = providers.getChildren(DataProvider.OAI_PROVIDER);
			Iterator i = provider.iterator();
			while (i.hasNext()) {
				dataProvider = new DataProvider(path);
				// dataProvider.setRuta(path);
				Element l = (Element) i.next();
				Element e = l.getChild(DataProvider.SHORTNAME);
				// recorremos todo el vector de los providers y vamos comparando
				// con los shortnames del archivo
				for (int j = 0; j < Repository.length; j++) {
					if (e.getText().equals(Repository[j])) {

						dataProvider.setShortName(e.getText());
						idrep = e.getText();

						e = l.getChild(DataProvider.BASE_URL);
						dataProvider.setBaseURL(e.getText());

						e = l.getChild(DataProvider.DESCRIPTION);
						dataProvider.setDescription(e.getText());

						dataProvider.setCdata(Integer.parseInt(dataProvider
								.getText(idrep, DataProvider.CDATA)));
						dataProvider.setResumptionWithMetadataPrefix(Integer
								.parseInt(dataProvider.getText(idrep,
										DataProvider.RESUMPTION)));
						// e = (Element) l.getChild(DataProvider.RESUMPTION);
						// dataProvider.setResumptionWithMetadataPrefix(Integer.parseInt(e.getText()));

						e = l.getChild(DataProvider.METADATA_PREFIX);
						dataProvider.setMetadataPrefix(e.getText());

						e = l.getChild(DataProvider.COLLECTION);
						dataProvider.setCollection(e.getText());

						e = l.getChild(DataProvider.PROTOCOL);
						dataProvider.setProtocol(e.getText());
						// e = (Element)
						// l.getChild(DataProvider.SCHEDULE_STATUS);
						dataprovidersCol.add(dataProvider);

					}

				}

			}
		} catch (Exception e) {
			log.error(e);
		}
		return dataprovidersCol;
	}

	// fin add edy

	public boolean isDataProviderReady(String idrep) {
		boolean result = false;
		try {
			SAXBuilder builder = new SAXBuilder();
			Document doc = builder.build(FILE_OAI_PROVIDERS_STATUS);
			Element raiz = doc.getRootElement();
			Element providers = raiz.getChild(DataProvider.OAI_PROVIDERS);
			List provider = providers.getChildren(DataProvider.OAI_PROVIDER);
			Iterator i = provider.iterator();
			while (i.hasNext()) {
				Element l = (Element) i.next();
				Element e = l.getChild(DataProvider.SHORTNAME);
				if (e != null && e.getText().equals(idrep)) {
					Element e2 = l.getChild(DataProvider.SCHEDULE_STATUS);
					Element e3 = l.getChild(DataProvider.ACTIVE);
					if (e2 != null && e3 != null
							&& DataProvider.READY.equals(e2.getText())
							&& e3.getText().equals("0")) {
						result = true;
						break;
					}
				}
			}
		} catch (Exception e) {
			log.error(e);
		}
		return result;
	}

	/**
	 * Obtiene los proveedores de urls para ir a recolectarlos. Se mapean por la
	 * clase DataProvider.
	 * 
	 * @return
	 */
	private Collection<DataProvider> getDataProvidersURL() {
		Collection<DataProvider> dataprovidersColUrls = new ArrayList<DataProvider>();
		DataProvider dataProvider = new DataProvider();
		dataProvider.setBaseURL("http://dis.eafit.edu.co/metabd");
		// dataProvider.setUrl("http://dis.eafit.edu.co/metabd");
		dataProvider.setShortName("metabd");
		dataProvider.setDescription("Esta es la biblioteca digital de Eafit ");
		dataprovidersColUrls.add(dataProvider);
		return dataprovidersColUrls;
	}

	public void noContinuar() {
		oAIHarvester.noContinuar();
	}

	@Override
	public void run() {
		setWorking(true);
		DataProvider dp = new DataProvider(strRuta);
		// sh.validarDataProvidersOAI(strRuta);
		String all[] = { "all" };
		if (strOption != null && strOption[0].equals("all")) {
			dp.resetAllProviders();
			configurarAplicacion(all);
		} else {
			for (int i=0;i<strOption.length;i++)
				dp.resetProviderStatus(strOption[i]);
			configurarAplicacion(strOption);
		}
		System.out.println("FIN DE LA EJECUCION");
		setWorking(false);
		// notify();		
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String cmd = "start";
		String dl_home = null;

		if (args.length > 0) {
			dl_home = args[0];
		} else {
			System.out.println("DL_HOME no especificado");
			System.exit(0);
		}

		if (args.length > 1) {
			cmd = args[1];
		} else {
			System.out
					.println("Comando requiere parametros [Start,Reset] [all,nombre repositorio]");
			System.exit(0);
		}
		if (cmd.equals("start")) {
			String option = null;
			if (args.length > 2) {
				option = args[2];
			} else {
				System.out
						.println("Opcion no especificado para el comand 'start'");
				System.exit(0);
			}
			option = args[2];
			String Repository[] = { option };
			Harvester harvester = new Harvester(dl_home, Repository);
			harvester.run();
		} else if (cmd.equals("reset")) {
			String option = null;
			if (args.length > 2) {
				option = args[2];
				DataProvider dp = new DataProvider(dl_home);
				if (option.equals("all"))
					dp.resetAllProviders();
				else {
					dp.resetProviderStatus(option);
				}
			} else {
				System.out
						.println("Opcion no especificado para el comand 'reset'");
				System.exit(0);
			} 

		}
	}
}
