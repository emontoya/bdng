package org.bdng.harvmgr.harvester;

/**
 * 
 * @author Eafit
 */
public class HarvesterException extends Exception {

	public HarvesterException() {
		super();
	}

	public HarvesterException(String message, Throwable cause) {
		super(message, cause);
	}

	public HarvesterException(String message) {
		super(message);
	}

	public HarvesterException(Throwable cause) {
		super(cause);
	}
}