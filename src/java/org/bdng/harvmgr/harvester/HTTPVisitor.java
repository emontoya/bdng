package org.bdng.harvmgr.harvester;

import java.util.HashMap;

import org.htmlparser.Parser;
import org.htmlparser.Tag;
import org.htmlparser.util.ParserException;
import org.htmlparser.visitors.NodeVisitor;

public class HTTPVisitor extends NodeVisitor {

	HashMap<String, String> myList = null;
	String url = null;

	public HashMap<String, String> getXMLPaths(String url) throws Exception {
		myList = new HashMap<String, String>(1);
		this.url = url;
		try {
			Parser p = new Parser(url);
			p.visitAllNodesWith(this);
		} catch (ParserException e) {
			System.out.println("Error de interoperabilidad");

			/*
			 * Lanzamos de nuevo la excepci_n para realizar el proceso de
			 * recuperaci_n de la _ltima recolecci_n exitosa.
			 */
			throw new Exception();
		}
		return myList;

	}

	@Override
	public void visitTag(Tag tag) {
		String name = tag.getTagName();
		if (name.equalsIgnoreCase("a")) {
			String hrefValue = tag.getAttribute("href");
			if (hrefValue.toLowerCase().endsWith("xml")) {
				String miruta = "";
				if (hrefValue.contains("/")) {
					String[] tmp = hrefValue.split("/");
					miruta = url + "/" + tmp[tmp.length - 1];
				} else
					miruta = url + "/" + hrefValue;
				myList.put(hrefValue, miruta);
			}
		}
	}
}
