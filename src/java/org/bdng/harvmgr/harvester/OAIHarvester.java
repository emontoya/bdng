package org.bdng.harvmgr.harvester;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.util.Date;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.bdng.harvmgr.dlConfig;
import org.bdng.harvmgr.process.Processor;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * La clase <code>OAIHarvester.java</code>
 * <p>
 * Recolecta un DataProvider OAI y lo almacena localmente.
 * </p>
 * 
 * @author Castulo Ramirez Londono. UNIVERSIDAD EAFIT.
 * @version 1.0, 01-jun-2008
 * @since JDK1.6.0_05
 */
public class OAIHarvester extends HarvesterAbs {

	/**
	 * Variable global util para suspender el proceso en un momento dado
	 */
	static boolean continuar = true;

	static final String VERB_LIST_RECORDS = "ListRecords";

	static PrintWriter fileOutputLog;

	/* SE DEFINEN LAS CONSTANTES DEL METADATA PREFIX */
	private static final int METADATAPREFIXNOTDEFINED = 0;
	private static final int METADATAPREFIXNOTNEEDED = 1;
	private static final int METADATAPREFIXNEEDED = 2;
	private static final int METADATAPREFIXERROR = 3;
	private static ManejoFechas manejoFechas = new ManejoFechas();

	/*
	 * Variable que permite acceder a la siguiente URL con los archivos
	 * restantes
	 */
	boolean blnHayResumptionToken;

	/**
	 * Constructor. Inicializa las variables necesarias para recolectar
	 * 
	 * @param strRuta
	 *            String con el path absoluto de la aplicacion
	 */
	public OAIHarvester(String strRuta) {
		HarvesterAbs.strPath = strRuta;
		properties = new dlConfig(strPath);
		PROXY_ON = Boolean.parseBoolean(properties.getString("proxy"));
		log = Logger.getLogger(OAIHarvester.class);
		PropertyConfigurator.configure(strPath + "/conf/log4j.properties");
	}

	/**
	 * Detiene la recoleccion de todos los proveedores OAI
	 */
	public void noContinuar() {
		continuar = false;
	}

	/**
	 * Metodo principal de la clase. Ejecuta la recoleccion de un DataProvider y
	 * envia a recoleccion en un processor dado
	 * 
	 * @param dp
	 *            DataProvider a recolectar
	 * @param p
	 *            Processor para enviar una vez se finalice la recoleccion de un
	 *            archivo XML
	 * @throws Exception
	 * @author sebastian
	 * @since 24/03/2010
	 */
	public void ejecutarRecoleccionOAI(DataProvider dp, Processor p)
			throws Exception {
		String baseURL = dp.getBaseURL();
		String metadataPrefix = dp.getMetadataPrefix();
		OAIURLManager urlManager = new OAIURLManager();

		// if(metadataPrefix.equalsIgnoreCase("lom_co")){
		// metadataPrefix="oai_lom";
		// }
		/* Crea la primera URL de recoleccion */
		log.info("Recolectando OAI..." + dp.getShortName());

		String strUrl = crearURL(metadataPrefix, baseURL, dp, urlManager);
		tiempoTotal = 0;
		numeroRegistros = 0;
		resultadoTest = -1;
		blnRecolecto = false;
		blnFueExitoso = true;
		dp.setHarvestStatus(DataProvider.PROCESSING);
		if (PROXY_ON) {
			configurarProxy();
		}
		/* Crea el path para guardar */

		String path = properties.getString("dl_metadata") + "/"
				+ ServerConfXml.DBXML + "temp/" + dp.getShortInst() + "/"
				+ dp.getShortName(); // se modifica para 2 niveles
		// log.info("Repository=" + dp.getShortName());
		// System.out.println("path"+path);
		blnHayResumptionToken = true;

		/* Proceso de borrado de directorios recursivamente */
		HarvesterAbs.deleteDirectory(new File(path));
		HarvesterAbs.deleteDirectory(new File(path.replaceAll("dbxmltemp",
				"dbxmltemp-" + obtenerFecha())));
		HarvesterAbs.deleteDirectory(new File(path.replaceAll("dbxmltemp",
				"dbxml")));

		long tiempoInicialRecoleccion = System.currentTimeMillis();

		/* Ciclo que se ejecuta por cada archivo XML que exista */
		int contadorArchivos = 0;
		while (blnHayResumptionToken && continuar) {
			blnHayResumptionToken = procesarUrl(dp, p, strUrl, path, urlManager);
			strUrl = crearSiguienteURL(urlManager, dp);

			if (StatsManager.getInstance(dp).isSuccessfull()) {
				contadorArchivos++;

			}
			System.out.println("Recolectando repositorio '" + dp.getShortName()
					+ "'..." + dp.getNrorecs());
		}
		long tiempoFinalRecoleccion = System.currentTimeMillis();
		StatsManager.getInstance(dp).setHarvesterTime(
				tiempoFinalRecoleccion - tiempoInicialRecoleccion);
		StatsManager.getInstance(dp).setNumber_files(contadorArchivos);
		// System.out.println("dp.getRepositoryName()"+dp.getRepositoryName());
		// StatsManager.getInstance(dp).setRepositoryName(dp.getRepositoryName());
		finalizar(dp);
		StatsManager st = new StatsManager();
		st.writeStatsInDisk(StatsManager.getInstance(dp), properties);

	}

	/**
	 * Crea la siguiente URL para recolectar. Este metodo utiliza valores
	 * iniciales seteados en el metodo crearURL
	 * 
	 * @param urlManager
	 *            El Manager del URL
	 * @param dp
	 *            DataProvider sobre el que se recolecta
	 * @return String con la siguiente URL a recolectar
	 * @throws Exception
	 *             en caso de error de escritura del test de metadataPrefix
	 * @author sebastian
	 * @since 24/03/2010
	 */
	private String crearSiguienteURL(OAIURLManager urlManager, DataProvider dp)
			throws Exception {
		String stUrl = urlManager.getPrimeraParte()
				+ urlManager.getSegundaParte() + "&resumptionToken="
				+ urlManager.getValorResumptionToken();
		// AQUI SE VA A TESTEAR EL METADATA PREFIX
		if (resultadoTest == -1)
			resultadoTest = dp.isResumptionWithMetadataPrefix();
		if (resultadoTest == METADATAPREFIXNOTDEFINED) {
			resultadoTest = testMetadataPrefix(urlManager.getPrimeraParte(),
					urlManager.getSegundaParte(), urlManager.getTerceraParte());
			dp.writeResumptionWithMetadataPrefix(dp.getShortName(),
					resultadoTest);
		}
		if (resultadoTest == METADATAPREFIXNEEDED) {
			stUrl = stUrl + urlManager.getTerceraParte();
		}
		return stUrl;
	}

	/**
	 * Crea una URL por primera vez y configura el url manager
	 * 
	 * @param metadataPrefix
	 *            MetaDataPrefix del
	 * @param baseURL
	 *            URL base para la recoleccion
	 * @param dp
	 *            DataProvider que se recolectara
	 * @param urlManager
	 *            url manager
	 * @return string con la url a recolectar
	 * @author sebastian
	 * @since 24/03/2010
	 */
	public String crearURL(String metadataPrefix, String baseURL,
			DataProvider dp, OAIURLManager urlManager) {
		String stPrimeraParteUrl = baseURL;
		String stSegundaParte = "?" + "verb=" + VERB_LIST_RECORDS;
		String stTerceraParte = "";
		if (dp.getHarvestStatus().equals(DataProvider.PAUSE)) {
			String lrt = dp.getLastResumptionToken();
			if (lrt != null)
				stTerceraParte = "&resumptionToken=" + lrt;

			if (dp.isResumptionWithMetadataPrefix() == 2)
				stTerceraParte = stTerceraParte + "&metadataPrefix="
						+ metadataPrefix;

		} else {
			stTerceraParte = "&metadataPrefix=" + metadataPrefix;
		}

		String stUrl = stPrimeraParteUrl + stSegundaParte + stTerceraParte;
		String stCuartaParte = "";
		if (Integer.parseInt(dp.getHarvestCount()) > 1) {
			/* Recoleccion Incremental */
			String stfecha = dp.getLastHarvestDate();
			Date lastHarvestDate = null;
			try {
				lastHarvestDate = javFormatHarvest_.parse(stfecha);
				lastHarvestDate = manejoFechas.sumarDia(lastHarvestDate, 1);
			} catch (Exception e) {

				lastHarvestDate = new Date();
			}
			stfecha = javFormatHarvest_.format(lastHarvestDate);
			stCuartaParte = "&from=" + stfecha;
			stUrl = stUrl + stCuartaParte;
		}
		urlManager.setUrl(stUrl);
		urlManager.setPrimeraParte(stPrimeraParteUrl);
		urlManager.setSegundaParte(stSegundaParte);
		urlManager.setTerceraParte(stTerceraParte);
		urlManager.setCuartaParte(stCuartaParte);
		return stUrl;
	}

	/**
	 * Gestiona la descarga, validacion y envio a procesamiento de un archivo
	 * XML a recolectar
	 * 
	 * @param dp
	 *            DataProvider a recolectar
	 * @param p
	 *            Processor
	 * @param stUrl
	 *            URL a recolectar
	 * @param path
	 *            Path del archivo a guardar
	 * @param urlManager
	 *            url manager
	 * @return si hay resumption token o no, es decir, si se debe continuar
	 *         iterando
	 */
	private boolean procesarUrl(DataProvider dp, Processor p, String stUrl,
			String path, OAIURLManager urlManager) {
		boolean blnHayResumptionToken = false;
		long timeInicial = 0, timeFinal = 0, duracionSegundos = 0;
		try {
			timeInicial = System.currentTimeMillis();
			// System.out.println("PROCESARURL");
			// String con el total de los datos recolectados
			String stSalidaVerbo = downloadDataFromURL(stUrl);
			timeFinal = System.currentTimeMillis();
			duracionSegundos = timeFinal - timeInicial;
			numeroRegistros++;
			tiempoTotal += duracionSegundos;
			blnHayResumptionToken = false;
			/* Si existen resultados */
			if ((stSalidaVerbo != null) && (stSalidaVerbo.trim().length() != 0)) {
				String valorResumptionToken = escribirXMLEnDisco(stSalidaVerbo,
						path, dp);
				/* En esta etapa se empieza el processor */
				iniciarProcesamientoDeArchivo(dp, path, p);
				blnRecolecto = true;
				dp.incLastIdFile();
				if ((valorResumptionToken != null)
						&& (valorResumptionToken.trim().length() != 0)) {
					dp.setLastResumptionToken(valorResumptionToken);
					urlManager.setValorResumptionToken(valorResumptionToken);
					blnHayResumptionToken = true;
				}
			}
		} catch (Exception e) {
			// log.error(e);
			System.out.println(" SE HA PRESENTADO UN ERROR!");
			System.out.println(" Recolectando ultima recoleccion exitosa ...");

			inIntentosError++;
			blnFueExitoso = false;
			String stError = e.getMessage();
			if (e instanceof UnknownHostException)
				stError += ":HostDesconocido";
			else if (e instanceof IOException)
				stError += ":Error de entrada y salida";
			dp.setHarvestStatus(DataProvider.ERROR);
			log.error(dp.getShortName() + ">" + stError);
			dp.setMessage(stError);
			StatsManager.getInstance(dp).setSuccessfull(false);

			String lastdate = dp.getLastDate();
			// System.out.println("lastdate OK "+lastdate);
			if (!lastdate.equals("0000-00-00")) {

				/* Borramos archivos actuales */
				HarvesterAbs.deleteDirectory(new File(path.replaceAll(
						"dbxmltemp", "dbxml")));
				int doc = 0;
				path = path.replaceAll("dbxmltemp", "dbxmltemp" + "-"
						+ lastdate);
				/* Este ciclo es ejecutado por cada archivo xml */
				while (true) {

					/* Variable utilizada para recorrer los documentos */
					doc++;
					String xmlFormat = dp.getMetadataPrefix();
					String filename = dp.getShortName() + "_" + doc + ".xml";

					File f = new File(path + "/" + filename);
					log.error(f.toString());
					log.error(f.exists());
					// log.error(e);
					/*
					 * Si no existe fue porque finalizo el ultimo documento o el
					 * directorio esta vacio
					 */
					if (!f.exists()) {
						break;
					}
					System.out.println("Proceso de recuperacion finalizado "
							+ path + "/" + filename);
					//
					p.procesarArchivo(xmlFormat, path, filename, dp, "-"
							+ lastdate);

				}

			} else {
				System.out
						.println(" No se encontro recoleccion exitosa previa! ");
			}
			if (inIntentosError >= 2) {
				blnHayResumptionToken = false;
			}
		}

		dp.incContFiles();
		return blnHayResumptionToken;
	}

	/**
	 * Anuncia al processor un nuevo archivo para procesar
	 * 
	 * @param dp
	 *            DataProvider
	 * @param path
	 *            Path del archivo recolectado
	 * @param p
	 *            Processor
	 */
	/*
	 * public void iniciarProcesamientoDeArchivo(DataProvider dp, String path,
	 * Processor p) { String xmlFormat = dp.getMetadataPrefix(); String filename
	 * = dp.getShortName() + "_" + dp.getLastIdFile() + ".xml"; //
	 * System.out.println("anunciando para procesar "+ filename+" en "+path //
	 * ); // p.anunciarArchivoListo(xmlFormat,path, filename,dp); // se utiliza
	 * // cuando se ejecuta el procesador en su propio hilo
	 * p.procesarArchivo(xmlFormat, path, filename, dp, ""); }
	 */

	/**
	 * Escribe en disco un documento XML ya recolectado y retorna el valor del
	 * resumption token
	 * 
	 * @param text
	 *            XML recolectado
	 * @param path
	 *            Ruta en la que se debe guardar
	 * @param dp
	 *            DataProvider al que pertenece el archivo
	 * @throws Exception
	 *             Si hay error en escritura
	 * @author sebastian
	 */
	private String escribirXMLEnDisco(String text, String path, DataProvider dp)
			throws Exception {
		/* Escribo el documento XML en disco */
		Document doc = writeXML.getDocument(text);
		String valorResumptionToken = writeXML.obtenerValorResumptionToken(doc,
				"resumptionToken");
		String filename = dp.getShortName() + "_" + dp.getLastIdFile() + ".xml";
		writeXML.writeXmlFile(doc, path, filename);
		writeXML.writeXmlFile(doc,
				path.replaceAll("dbxmltemp", "dbxmltemp-" + obtenerFecha()),
				filename); // escribe en disco en una carpeta con fecha actual
		return valorResumptionToken;

	}

	/**
	 * Realiza un test para saber si la segunda conexion OAI debe tener el
	 * parametro metadataPrefix
	 * 
	 * @param stUrlBase
	 *            base de la url
	 * @param stUrlVerb
	 *            verbo de la url
	 * @param stUrlMetaData
	 *            metadata prefix del proveedor
	 * @return un entero indicando el resultado del test
	 * 
	 * @throws Exception
	 *             en caso de error
	 */
	private int testMetadataPrefix(String stUrlBase, String stUrlVerb,
			String stUrlMetaData) throws Exception {
		String stUrl = stUrlBase + stUrlVerb + stUrlMetaData;
		URL u = new URL(stUrl);
		URLConnection uc = u.openConnection();
		String stSalidaVerbo = "";
		StringBuilder sb = new StringBuilder();
		BufferedReader in = new BufferedReader(new InputStreamReader(
				uc.getInputStream()));
		String tmp = null;
		while ((tmp = in.readLine()) != null) {
			sb.append(tmp);
		}
		stSalidaVerbo = sb.toString();
		Document doc = writeXML.getDocument(stSalidaVerbo);
		String resumption = writeXML.obtenerValorResumptionToken(doc,
				"resumptionToken");
		if ((resumption != null) && (resumption.trim().length() != 0)) {
			// EXISTE RESUMPTION TOKEN. POR LO TANTO DEBO VERIFICAR SI FUNCIONA
			// SIN EL METADATAPREFIX (LO MAS COMUN)
			URL u2 = new URL(stUrlBase + stUrlVerb + "&resumptionToken="
					+ resumption);
			URLConnection uc2 = u2.openConnection();
			StringBuilder sb2 = new StringBuilder();
			BufferedReader in2 = new BufferedReader(new InputStreamReader(
					uc2.getInputStream()));
			String tmp2 = null;
			while ((tmp2 = in2.readLine()) != null) {
				sb2.append(tmp2);
			}
			String stSalidaVerbo2 = sb2.toString();
			Document segundoDocumento = writeXML.getDocument(stSalidaVerbo2);
			NodeList list = segundoDocumento.getElementsByTagName("*");
			for (int i = 0; i < list.getLength(); i++) {
				Element element = (Element) list.item(i);
				if (element.getTagName().equals("ListRecords")) {
					// NO HUBO ERROR
					return METADATAPREFIXNOTNEEDED;
				}
			}
			// AHORA DEBO PROBAR CON EL METADATAPREFIX HABER SI ASI SI DA
			URL u3 = new URL(stUrlBase + stUrlVerb + "&resumptionToken="
					+ resumption);
			URLConnection uc3 = u3.openConnection();
			StringBuilder sb3 = new StringBuilder();
			BufferedReader in3 = new BufferedReader(new InputStreamReader(
					uc3.getInputStream()));
			String tmp3 = null;
			while ((tmp3 = in3.readLine()) != null) {
				sb3.append(tmp3);
			}
			String stSalidaVerbo3 = sb3.toString();
			Document tercerDocumento = writeXML.getDocument(stSalidaVerbo3);
			list = tercerDocumento.getElementsByTagName("*");
			for (int i = 0; i < list.getLength(); i++) {
				Element element = (Element) list.item(i);
				if (element.getTagName().equals("ListRecords")) {
					/* NO HUBO ERROR */
					return METADATAPREFIXNEEDED;
				}
			}
			return METADATAPREFIXERROR;

		} else
			return METADATAPREFIXNOTDEFINED;
	}

	/**
	 * elimina tanto los processors como los hilos una vez se culmina la
	 * recoleccion
	 */
	public void eliminarHilosRestantes() {
		for (int i = 0; i < processorsActuales.size(); i++) {
			processorsActuales.get(i).interrupt();
		}
		for (int i = 0; i < hilosActuales.length; i++) {
			hilosActuales[i].interrupt();
		}

	}
}
