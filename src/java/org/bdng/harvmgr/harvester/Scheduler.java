package org.bdng.harvmgr.harvester;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.bdng.harvmgr.dlConfig;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;

public class Scheduler {
	Logger log = Logger.getLogger(Scheduler.class);

	protected static ManejoFechas manejofechas = new ManejoFechas();

	String FILE_OAI_PROVIDERS_STATUS = null;

	dlConfig dlconfig = null;

	String path_home = null;

	protected static SimpleDateFormat spSimple = new SimpleDateFormat(
			"yyyy-MM-dd");

	public Scheduler() {
		dlconfig = new dlConfig();
		FILE_OAI_PROVIDERS_STATUS = dlconfig.getOaiProvidersStatus();
	}

	public Scheduler(String path_home) {
		this.path_home = path_home;
		dlconfig = new dlConfig(path_home);
		FILE_OAI_PROVIDERS_STATUS = dlconfig.getOaiProvidersStatus();
	}

	/**
	 * Procesa el archivo de entrada conf/bdcol_oai_providers.xml cambia el
	 * estado del tag <schedule_status> en READY/NOT_READY
	 */
	synchronized public void validarDataProvidersOAI() {
		DataProvider dataProvider = null;
		try {
			SAXBuilder builder = new SAXBuilder();
			Document doc = builder.build(FILE_OAI_PROVIDERS_STATUS);
			Element raiz = doc.getRootElement();
			Element providers = raiz.getChild(DataProvider.OAI_PROVIDERS);
			List provider = providers.getChildren(DataProvider.OAI_PROVIDER);
			Iterator i = provider.iterator();
			while (i.hasNext()) {
				dataProvider = new DataProvider(path_home);
				Element l = (Element) i.next();
				Element e = l.getChild(DataProvider.SHORTNAME);
				dataProvider.setShortName(e.getText());
				// dataProvider.setRuta(stRuta);
				this.procesarLogica(dataProvider);
			}
		} catch (Exception e) {
			log.error(e);
		}
	}

	/**
	 * @param dp
	 */
	public void procesarLogica(DataProvider dp) {

		int iHarvestCountIncremental = Integer.parseInt(dp
				.getHarvestCountIncremental());
		int iHarvestCount = Integer.parseInt(dp.getHarvestCount());
		// Si el n_mero total de veces es menor que el n_mero de veces que he
		// recolectado el sitio
		// se debe hacer una recolecci_n FULL
		if ((iHarvestCountIncremental < iHarvestCount) || (iHarvestCount == 1)) {
			dp.resetProviderStatus(dp.getShortName());
			return;
		}

		// Da prioridad a los que est_n en estado de ERROR
		if (dp.getHarvestStatus().equals(DataProvider.ERROR)) {
			dp.setScheduleStatus(DataProvider.READY);
			return;
		}

		String stfecha = "";
		stfecha = dp.getLastHarvestDate();
		try {
			Date date = Scheduler.spSimple.parse(stfecha);
			FechaEnt fecha = ManejoFechas.obtenerComponentes(date);
			stfecha = Scheduler.spSimple.format(date);
			// System.out.println("Scheduler.procesarLogica(stfecha) " + stfecha
			// + " Shorname " + dp.getShortName() + " " + fecha);
			if (fecha.getAno() < 1800) {
				dp.resetProviderStatus(dp.getShortName());
				return;
			}

			int intHarvest_frecuency = Integer.parseInt(dp
					.getInitHarvestFrecuency());

			// System.out.println("Scheduler.procesarLogica() "
			// + intHarvest_frecuency);
			Date fechaRecoleccionMasLaFrecuencia = manejofechas.sumarDia(date,
					intHarvest_frecuency);

			// Comparando contra la fecha del sistema
			int itotal = manejofechas.compararfechas(
					fechaRecoleccionMasLaFrecuencia, new Date());
			// System.out.println("el resultado de la compararci_n es " +
			// itotal);
			// System.out.println("Scheduler.procesarLogica() "+ ManejoFechas
			// .obtenerComponentes(fechaRecoleccionMasLaFrecuencia));
			// Si la fechaRecoleccionMasLaFrecuencia es menor o igual a la del
			// sistema, no debe recolectar
			if ((itotal == 0) || (itotal > 0)) {
				dp.setScheduleStatus(DataProvider.NOT_READY);
				return;
			}
			// Verificar cuantos dias la fecha del sistema es mayor que la fecha
			// de recoleccion

			dp.setScheduleStatus(DataProvider.READY);

		} catch (Exception e) {
			log.error(e);
			stfecha = "1970-01-01";
			return;
		}

		// System.out.println("DataProvider.main(3) " + sp.format(javaDate));
		// int i = manejoFechas.compararfechas(javaDate, date);
		// System.out.println("el resultado de la compararci_n es " + i);

		// <last_harvest_date>2008-11-12T16:51:51</last_harvest_date>
		// <last_harvest_date>0000-00-00T00:00:00</last_harvest_date>

		// String stCuartaParte = "&from=" + stfecha;
		// stUrl = stUrl + stCuartaParte;
	}

	public static void main(String[] args) {
		String dl_home = null;

		if (args.length > 0) {
			dl_home = args[0];
		} else {
			System.out.println("DL_HOME no especificado");
			System.exit(0);
		}
		Scheduler sh = new Scheduler(dl_home);
		sh.validarDataProvidersOAI();
	}

}
