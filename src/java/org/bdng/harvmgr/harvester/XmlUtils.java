package org.bdng.harvmgr.harvester;

import java.util.StringTokenizer;

import org.apache.log4j.Logger;

/**
 * La clase <code>XmlUtils.java</code>
 * <p>
 * 
 * @author C_stulo Ram_rez Londo_o. UNIVERSIDAD EAFIT.
 * @version 1.0, 3/05/2004
 * @since JDK1.4.2_03
 */
public final class XmlUtils {

	Logger log = Logger.getLogger(XmlUtils.class);

	public static String quitarEspacios(String stIn) {
		String stOut = "";

		StringTokenizer tokens = new StringTokenizer(stIn, " ");
		int numberOfTokens = tokens.countTokens();
		if (numberOfTokens == 0)
			return stIn.trim();

		for (int i = 0; i < numberOfTokens; i++) {
			stOut += tokens.nextToken();
		}
		// log.error(e);

		return stOut.trim();
	}

	public static void main(String[] args) {
		// String stResult= " L%20004.64%20%20H292 ";
		String stResult = "Parent Directory     ";
		// stResult = stResult.replaceAll("%20", "");
		// System.out.println("XmlUtils.main:'"+stResult.trim()+"'");
		System.out.println("XmlUtils.main pp:'"
				+ XmlUtils.quitarEspacios(stResult) + "'");

		String stPatron = "<?xml-stylesheet type='text/xsl' href='/oai2.xsl'?>";

		String stSalida = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>+"
				+ "<?xml-stylesheet type='text/xsl' href='/oai2.xsl'?>+<OAI-PMH xmlns=\"http://www.openarchives.org/OAI/2.0/\">";

		stSalida = stSalida.replace(stPatron, "");

		System.out.println("OAIHarvester.main() \n" + stSalida);
	}

}
