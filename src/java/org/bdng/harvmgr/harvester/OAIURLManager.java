package org.bdng.harvmgr.harvester;

/**
 * Maneja la url y sus partes para generar las urls con que se recolectara
 * 
 * @author sebastian
 * @since 22/03/2010
 * 
 */

public class OAIURLManager {

	private String url;
	private String primeraParte;
	private String segundaParte;
	private String terceraParte;
	private String cuartaParte;
	private String valorResumptionToken;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getPrimeraParte() {
		return primeraParte;
	}

	public void setPrimeraParte(String primeraParte) {
		this.primeraParte = primeraParte;
	}

	public String getSegundaParte() {
		return segundaParte;
	}

	public void setSegundaParte(String segundaParte) {
		this.segundaParte = segundaParte;
	}

	public String getTerceraParte() {
		return terceraParte;
	}

	public void setTerceraParte(String terceraParte) {
		this.terceraParte = terceraParte;
	}

	public String getCuartaParte() {
		return cuartaParte;
	}

	public void setCuartaParte(String cuartaParte) {
		this.cuartaParte = cuartaParte;
	}

	public String getValorResumptionToken() {
		return valorResumptionToken;
	}

	public void setValorResumptionToken(String valorResumptionToken) {
		this.valorResumptionToken = valorResumptionToken;
	}

}
