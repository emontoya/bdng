package org.bdng.harvmgr.harvester;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Scanner;

public class HtmlParser {

	Stopwords stopwords = new Stopwords();
	XmlUtils xmlUtils = new XmlUtils();

	private Collection<String> valoresARecolectar = new ArrayList<String>();

	/**
	 * @param stSalida
	 * @return
	 */
	public boolean validarSalidaDeDatos(String stSalida) {
		int inDatosPrueba1 = stSalida.indexOf("<html>");
		int inDatosPrueba2 = stSalida.indexOf("<HTML>");
		if ((inDatosPrueba1 > -1) || ((inDatosPrueba2 > -1))) {
			return true;
		}

		System.out.println("inDatosPrueba1 " + inDatosPrueba1);
		System.out.println("inDatosPrueba2 " + inDatosPrueba2);
		// System.out.println("bln " + bln);
		return false;
	}

	/**
	 * Procesa la p_gina y las divide en tokens de href en href
	 * 
	 */
	public void pocesarPagina(String stPagina) {
		Scanner scanner = new Scanner(stPagina).useDelimiter("href=\"");
		for (Iterator it = scanner; it.hasNext();) {
			String token = (String) it.next();
			// System.out.println("PARTIDO 1 : " + token);
			this.procesarToken(token);
		}
		scanner = null;
	}

	/**
	 * Obtiene el nombre de los valores antes de los Hrfs
	 * 
	 * @param token
	 */
	private void procesarToken(String token) {
		Scanner scanner = new Scanner(token).useDelimiter("</a>");
		for (Iterator it = scanner; it.hasNext();) {
			String valor = (String) it.next();
			// System.out.println(" PARTIDO 2 : " + valor);
			this.procesarTokenDefinitivo(valor);
			break;
		}
		scanner = null;
	}

	/**
	 * 
	 * @param token
	 */
	private void procesarTokenDefinitivo(String token) {
		Scanner scanner = new Scanner(token).useDelimiter("/\">");
		for (Iterator it = scanner; it.hasNext();) {
			String valor = (String) it.next();
			// System.out.println(" PARTIDO 3 : " + valor);
			validarDefinitivo(valor);
			break;
		}
		scanner = null;
	}

	/**
	 * Este es el nuevo para para ser otra vez consultado
	 * 
	 * @param valor
	 */
	private void validarDefinitivo(String valor) {
		Collection<String> datosCol = new ArrayList<String>();
		String nombre = XmlUtils.quitarEspacios(valor);
		String stPrimeraLetra = nombre.substring(0, 1);
		boolean blnES = stopwords.is(stPrimeraLetra);
		if (!blnES) {
			datosCol.add(valor);
			this.valoresARecolectar = datosCol;
		}
		System.out.println(" PARTIDO 3 SEMI PROCESADO : " + nombre);
	}

	public Collection<String> getValoresARecolectar() {
		return valoresARecolectar;
	}

	public void setValoresARecolectar(Collection<String> valoresARecolectar) {
		this.valoresARecolectar = valoresARecolectar;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// HtmlParser manejoDatos = new HtmlParser();
		// Stopwords stopwords = new Stopwords();
		// String stSalida = "<html><head>sajkdklsdjklasjd<HTMLk><html>";
		// boolean blnValidarDatos = manejoDatos.validarSalidaDeDatos(stSalida);
		// if (blnValidarDatos) {
		// System.out
		// .println("HtmlParser.main(1) Existe los datos .........");
		// }
		//
		// stSalida = stSalida.substring(0, 1);
		// System.out.println("HtmlParser.main() " + stSalida);

		String stToken = "www.eluniversal.com.mx/graficos/gabiblog/Dasso.doc";
		Scanner scanner = new Scanner(stToken).useDelimiter("http://");
		for (Iterator it = scanner; it.hasNext();) {
			stToken = (String) it.next();
		}
		System.out.println(" PARTIDO 3 : " + stToken);

	}
}