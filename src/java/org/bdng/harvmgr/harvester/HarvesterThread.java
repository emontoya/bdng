package org.bdng.harvmgr.harvester;

import java.util.Vector;

import org.bdng.harvmgr.dlConfig;
import org.bdng.harvmgr.process.Processor;

/**
 * Maneja un hilo de recoleccion, independiente de su protocolo
 * 
 * @author sebastian
 * @since 17/03/2010
 */

public class HarvesterThread extends HarvesterAbs implements Runnable {

	public static int dataProvidersSize = 0;
	static Integer dataProviderActual = 0;
	public static Integer dataProvidersCompletos = 0;
	static Object[] arrayDataProviders = null;
	static String strRuta;
	static boolean continuar = true;
	public static Integer idProcessor = 1;
	public static Vector<Thread> processorsActuales = new Vector<Thread>();
	public static Vector<Processor> processors = new Vector<Processor>();
	static dlConfig properties = null;

	// OBJETOS QUE CONTROLAN CADA TIPO DE HARVESTING
	OAIHarvester oAIHarvester;
	HTTPHarvester hTTPHarvester;

	/**
	 * Constructor. Inicializa los harvester especificos
	 */
	public HarvesterThread() {
		oAIHarvester = new OAIHarvester(strRuta);
		hTTPHarvester = new HTTPHarvester(strRuta);
	}

	/**
	 * Metodo run. Es el que se ejecuta cuando se inicia el hilo. Maneja el
	 * flujo de la aplicacion
	 */
	public void run() {
		// System.out.println("entro a ejecuta el hilo del HarvesterThread id ="+Thread.currentThread().getId());
		// Inicializo processor
		// Processor p = inicializarProcessor(); //para manejar pool de
		// Procesadores
		Processor p = new Processor(strRuta); // para realizar el procesamiento
												// posterior a cada recoleccion
		// Ciclo mientras tenga DP por recolectar
		while (continuar) {
			DataProvider datosProvider = null;
			// Obtengo el DP que voy a recolectar. Metodo sincronizado
			// datosProvider = obtenerDataProvider(p); // para Procesador en
			// hilo propio
			datosProvider = obtenerDataProvider(); // para procesador
													// dependiente del harvester
			// si datos provider es null, significa que ya termine
			if (datosProvider == null) {
				break;
			}
			// Crea el repositorio fisico
			armarRepositorioProvider(datosProvider);
			// si es OAI ejecuto el metodo de oai
			if (datosProvider.getProtocol().toUpperCase().contains("OAI")) {
				try {
					oAIHarvester.ejecutarRecoleccionOAI(datosProvider, p);
				} catch (Exception e) {
					log.error(e);
				}
			}
			// Si es HTTP ejecuto la recoleccion http
			else {
				try {
					hTTPHarvester.ejecutarRecoleccionHTTP(datosProvider, p);
				} catch (Throwable e) {
					log.error(e);
				}
			}
			// Verifica si este es el ultimo data provider que se debia hacer.
			// En caso de que si, se sale
			synchronized (dataProvidersCompletos) {
				dataProvidersCompletos++;

				if (dataProvidersCompletos >= dataProvidersSize) {
					// p.setContinuar(false); // se descomentarea cuando
					// Processor en su propio hilo
					// p.signal.release(); // se descomentarea cuando el
					// processor en su propio hilo
					Harvester.harvester.release();
					break;
				}
			}
		}
		try {
			if (dataProvidersCompletos >= dataProvidersSize) {
				Harvester.harvester.release();
			}
			// Para el processor y elimina el hilo
			p.setContinuar(false);
			p.signal.release(10);
			finalize();
		} catch (Throwable e) {
			log.error(e);
		}
	}

	/**
	 * Crea el repositorio fisico de recoleccion
	 */
	private void armarRepositorioProvider(DataProvider datosProvider) {
		String rutaRepositorio = properties.getString("dl_metadata");
		rutaRepositorio = rutaRepositorio + "/dbxml/"
				+ datosProvider.getShortInst() + "/"
				+ datosProvider.getShortName();
		this.armarRepositorio(rutaRepositorio);
	}

	/**
	 * Retorna el nuevo proveedor sobre el que se debe recolectar
	 * 
	 * @param p
	 *            Processor a asignar a este DataProvider
	 * @return DataProvider, o null si ya se han completado todos
	 */
	private synchronized DataProvider obtenerDataProvider(Processor p) {
		int miDataProvider = 0;
		// ENTRO A LA SECCION CRITICA PARA ADQUIRIR UN HILO QUE ME TOCA
		synchronized (dataProviderActual) {
			if (dataProviderActual >= dataProvidersSize) {
				p.setContinuar(false);
				p.signal.release();
				return null;
			} else {
				// System.out.println("Adquiriendo el dataProvider #"+dataProviderActual);
				miDataProvider = dataProviderActual;
				dataProviderActual++;
			}
			DataProvider datosProvider = (DataProvider) arrayDataProviders[miDataProvider];
			return datosProvider;
		}
	}

	/**
	 * Retorna un DataProvider sobre el que se debe recolectar
	 * 
	 * @return DataProvider, o null si ya se han completado todos
	 */
	private synchronized DataProvider obtenerDataProvider() {
		int miDataProvider = 0;
		// ENTRO A LA SECCION CRITICA PARA ADQUIRIR UN HILO QUE ME TOCA
		synchronized (dataProviderActual) {
			if (dataProviderActual >= dataProvidersSize) {
				return null;
			} else {
				miDataProvider = dataProviderActual;
				dataProviderActual++;
			}
			DataProvider datosProvider = (DataProvider) arrayDataProviders[miDataProvider];
			return datosProvider;
		}
	}

	/**
	 * Inicia un processor en la ruta dada
	 * 
	 * @return Processor de este hilo de recoleccion
	 */
	private Processor inicializarProcessor() {
		Processor p = null;
		synchronized (idProcessor) {
			p = new Processor(strRuta, idProcessor);
			System.out.println("Processor.strRuta =" + strRuta);
			idProcessor++;
			Thread threadProcessor = new Thread(p);
			processorsActuales.add(threadProcessor);
			processors.add(p);
			threadProcessor.start();
		}
		return p;
	}

	/**
	 * Inicializa variables comunes a todos los hilos.
	 * 
	 * @param strRuta2
	 *            La ruta absoluta para tomar la configuracion
	 * @param dataProviders
	 *            El array con todos los DP a recolectar
	 */
	public static void setVariablesGlobales(String strRuta2,
			Object[] dataProviders) {
		initDataProvidersCompletos();
		setStrRuta(strRuta2);
		setArrayDataProviders(dataProviders);
		setProperties(new dlConfig(strRuta));
		setDataProvidersSize(dataProviders.length);
	}

	public static int getDataProvidersSize() {
		return dataProvidersSize;
	}

	public static void setDataProvidersSize(int dataProvidersSize) {
		HarvesterThread.dataProvidersSize = dataProvidersSize;
	}

	public static void initDataProvidersCompletos() {
		dataProvidersCompletos = 0;// add by edy - se agrega para cada nueva
									// recoleccion se reinicie a 0 este contador

	}

	public static Object[] getArrayDataProviders() {
		return arrayDataProviders;
	}

	public static void setArrayDataProviders(Object[] arrayDataProviders) {
		HarvesterThread.arrayDataProviders = arrayDataProviders;
	}

	public static void setStrRuta(String strRuta) {
		HarvesterThread.strRuta = strRuta;
	}

	public static void setProperties(dlConfig properties) {
		HarvesterThread.properties = properties;
	}
}
