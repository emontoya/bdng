package org.bdng.harvmgr.harvester;

import java.util.Calendar;
import java.util.Date;

/**
 * @author eafit
 */
public class ManejoFechas {
	/**
	 * obtiene la fecha dado un dia, mes y año
	 * 
	 * @param dia
	 * @param mes
	 * @param year
	 * @return retorna un objeto Date, con la fecha, especificada
	 */

	public Date getFecha(int dia, int mes, int year) {
		Calendar c = Calendar.getInstance();
		c.clear();
		c.set(Calendar.MONTH, mes - 1); // 1 enero, -1 empieza en 0
		c.set(Calendar.DAY_OF_MONTH, dia);
		c.set(Calendar.YEAR, year);
		return c.getTime();
	}

	/**
	 * compara dos fechas y devuelve el resultado como entero
	 * 
	 * este metodo compara dos fechas, objetos de tipo Date
	 * 
	 * @param pdOrigen
	 *            fecha 1
	 * @param pdDestino
	 *            fecha 2
	 * @return si fecha 1 es igual a fecha 2, retorna 0 si fecha 1 es mayor que
	 *         fecha 2, retorna mayor a 0 si fecha 1 es menor que fecha 2,
	 *         retorna menor a 0
	 */
	public int compararfechas(Date pdOrigen, Date pdDestino) {
		return pdOrigen.compareTo(pdDestino); // alto esfuerzo mental
	}

	/**
	 * le suma un numero de años a la fecha
	 * 
	 * esta suma no verifica que el numero de años a sumar sea negativo
	 * 
	 * @param pdFecha
	 *            fecha
	 * @param numeroYears
	 *            años a sumar
	 * @return retorna una fecha pdA+numeroYears
	 */
	public Date sumarYear(Date pdFecha, int numeroYears) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(pdFecha);
		calendar.add(Calendar.YEAR, numeroYears);
		return calendar.getTime();
	}

	/**
	 * le resta un numero de años a la fecha
	 * 
	 * esta resta no verifica que el numero de años a restar sea negativo
	 * 
	 * @param pdFecha
	 *            fecha
	 * @param numeroYears
	 *            años a restar
	 * @return retorna una fecha pdA-numeroYears
	 */
	public Date restarYear(Date pdFecha, int numeroYears) {
		return sumarYear(pdFecha, -numeroYears);
	}

	/**
	 * le suma un numero de meses a la fecha
	 * 
	 * esta suma no verifica que el numero de meses a sumar sea negativo
	 * 
	 * @param pdFecha
	 *            fecha
	 * @param numeroMeses
	 *            meses a sumar
	 * @return retorna una fecha pdA+numeroMeses
	 */
	public Date sumarMes(Date pdFecha, int numeroMeses) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(pdFecha);
		calendar.add(Calendar.MONTH, numeroMeses);
		return calendar.getTime();
	}

	/**
	 * le resta un numero de meses a la fecha
	 * 
	 * esta resta no verifica que el numero de meses a restar sea negativo
	 * 
	 * @param pdFecha
	 *            fecha
	 * @param numeroMeses
	 *            meses a restar
	 * @return retorna una fecha numeroMeses
	 */
	public Date restarMes(Date pdFecha, int numeroMeses) {
		return sumarMes(pdFecha, -numeroMeses);
	}

	/**
	 * le suma un numero de dias a la fecha
	 * 
	 * esta suma no verifica que el numero de dias a sumar sea negativo
	 * 
	 * @param pdFecha
	 *            fecha
	 * @param numeroDias
	 *            dias a sumar
	 * @return retorna una fecha pdA+numeroDias
	 */
	public Date sumarDia(Date pdFecha, int numeroDias) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(pdFecha);
		calendar.add(Calendar.DAY_OF_MONTH, numeroDias);
		return calendar.getTime();
	}

	/**
	 * le resta un numero de dias a la fecha
	 * 
	 * esta resta no verifica que el numero de dias a restar sea negativo
	 * 
	 * @param pdFecha
	 *            fecha
	 * @param numeroDias
	 *            dias a restar
	 * @return retorna una fecha numeroDias
	 */
	public Date restarDia(Date pdFecha, int numeroDias) {
		return sumarDia(pdFecha, -numeroDias);
	}

	/**
	 * le resta una fecha a otra y retorna un Date con el resultado
	 * 
	 * @param pdA
	 *            fecha A
	 * @param pdB
	 *            fecha B
	 * @return retorna A-B
	 */
	private Date restar(Date pdA, Date pdB) {
		Calendar a = Calendar.getInstance();
		a.setTime(pdA);
		int adia = a.get(Calendar.DAY_OF_MONTH);
		int ames = a.get(Calendar.MONTH) + 1;
		int ayear = a.get(Calendar.YEAR);
		Calendar b = Calendar.getInstance();
		b.setTime(pdB);
		int bdia = b.get(Calendar.DAY_OF_MONTH);
		int bmes = b.get(Calendar.MONTH) + 1;
		int byear = b.get(Calendar.YEAR);
		Calendar result = Calendar.getInstance();
		result.setTime(getFecha(0, 1, 0));
		result.add(Calendar.YEAR, ayear - byear);
		result.add(Calendar.MONTH, ames - bmes);
		result.add(Calendar.DAY_OF_MONTH, adia - bdia);
		return result.getTime();
	}

	/**
	 * Obtiene la edad de una persona, dada la fecha de nacimiento
	 * 
	 * @param pdFecha
	 *            fecha de nacimiento
	 * @return edad en años de la persona
	 */
	public int getEdad(Date pdFecha) {
		Date hoy = hoy();
		Calendar calendar = Calendar.getInstance();
		Date resta = restar(hoy, pdFecha);
		calendar.setTime(resta);
		return calendar.get(Calendar.YEAR);
	}

	/**
	 * @return
	 */
	public static Date hoy() {
		return Calendar.getInstance().getTime();

	}

	/**
	 * @param pudoDia
	 * @return
	 */
	public static Date ultimoDiaMes(Date pudoDia) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(pudoDia);
		int inYear = cal.get(Calendar.YEAR);
		int inMonth = cal.get(Calendar.MONTH);
		cal.clear();
		cal.set(Calendar.YEAR, inYear);
		cal.set(Calendar.MONTH, inMonth); // Pilas piMonth en si mismo es el
		// siguiente mes.
		cal.set(Calendar.DATE, 1);
		cal.add(Calendar.MONTH, 1);
		cal.add(Calendar.DATE, -1);
		return cal.getTime();
	}

	/**
	 * @param pudoDia
	 * @return
	 */
	public static final FechaEnt obtenerComponentes(Date pudoDia) {
		Calendar cal = Calendar.getInstance();
		FechaEnt result = new FechaEnt();
		cal.setTime(pudoDia);
		result.setAno(cal.get(Calendar.YEAR));
		result.setMes(cal.get(Calendar.MONTH));
		result.setDia(cal.get(Calendar.DATE));
		result.setHoras(cal.get(Calendar.HOUR_OF_DAY));
		result.setMilisegundos(cal.get(Calendar.MILLISECOND));
		result.setMinutos(cal.get(Calendar.MINUTE));
		result.setSegundos(cal.get(Calendar.SECOND));
		return result;
	}
}