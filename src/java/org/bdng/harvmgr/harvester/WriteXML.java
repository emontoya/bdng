package org.bdng.harvmgr.harvester;

import java.io.File;
import java.io.FileOutputStream;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.jdom2.JDOMException;
import org.jdom2.output.DOMOutputter;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

public class WriteXML {

	/**
	 * Obtiene el documento a partir de un String
	 * 
	 * @param stDocumento
	 * @return
	 * @throws Exception
	 */
	public Document getDocument(String stDocumento/* , boolean encodingISO */)
			throws Exception {
		Document doc = null;
		DocumentBuilder builder = DocumentBuilderFactory.newInstance()
				.newDocumentBuilder();
		doc = builder.parse(new InputSource(new StringReader(stDocumento)));

		return doc;
	}

	/**
	 * This method writes a DOM document to a file
	 * 
	 * @param doc
	 * @param filename
	 * @throws Exception
	 */
	public void writeXmlFile(Document doc, String dirs, String fileName)
			throws Exception {
		try {
			// Prepare the DOM document for writing
			Source source = new DOMSource(doc);
			// Prepare the output file
			File file = new File(dirs);
			// System.out.println("Ruta: "+dirs+"/"+fileName);
			file.mkdirs();
			file = new File(dirs + "/" + fileName);
			file.createNewFile();
			Result result = new StreamResult(file);
			// Write the DOM document to the file
			Transformer xformer = TransformerFactory.newInstance()
					.newTransformer();
			xformer.setOutputProperty(OutputKeys.INDENT, "yes");
			xformer.transform(source, result);
		} catch (Exception e1) {
			throw e1;
		}
	}

	public void writeXmlJdomFile(String pathSalida, String filename,
			org.jdom2.Document doc_out) throws Exception {
		File f_out = new File(pathSalida);
		f_out.mkdirs();
		f_out = new File(pathSalida + "/" + filename);
		// System.out.println("Path SALIDA"+
		// pathSalida+"/"+filename+" Exito? "+ f_out.createNewFile());
		f_out.createNewFile();
		XMLOutputter xml = new XMLOutputter(Format.getPrettyFormat());
		FileOutputStream out = new FileOutputStream(pathSalida + "/" + filename);
		xml.output(doc_out, out);
		out.close();
	}

	/**
	 * This method reads XML file and return a DOM document
	 * 
	 * @param filename
	 * @return
	 * @throws Exception
	 */
	public Document readXmlFile(String filename) throws Exception {
		try {
			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
			Document doc = docBuilder.parse(new File(filename));
			return doc;

		} catch (Exception e1) {
			throw e1;
		}
	}

	/* Obtiene un org.w3c.dom.Document a partir de un org.jdom2.Document */
	public org.w3c.dom.Document convertToDOM(org.jdom2.Document jdomDoc)
			throws JDOMException {

		DOMOutputter outputter = new DOMOutputter();
		return outputter.output(jdomDoc);
	}

	// resumptionToken
	public String obtenerValorResumptionToken(Document doc, String valorTag) {
		NodeList list = doc.getElementsByTagName("*");
		String stValue = "";
		for (int i = 0; i < list.getLength(); i++) {
			Element element = (Element) list.item(i);
			if (element.getTagName().equals(valorTag)) {
				stValue = element.getTextContent();
				// System.out
				// .println("El VALOR DE " + valorTag + " ES " + stValue);
			}
		}
		return stValue;
	}

}
