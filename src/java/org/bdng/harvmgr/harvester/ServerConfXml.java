package org.bdng.harvmgr.harvester;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.bdng.harvmgr.dlConfig;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.XMLOutputter;

public class ServerConfXml {

	Logger log = Logger.getLogger(ServerConfXml.class);

	public static String FILE_SERVER_CONFIG = null;

	public static String SERVER_CONFIG_TAG = "ServerConfig";

	public static String DBXML = "dbxml";

	private String ruta = "";

	public ServerConfXml(String path) {
		ruta = path;
		dlConfig p = new dlConfig(path);
		FILE_SERVER_CONFIG = p.getString("dl_home") + "/conf/ServerConfig.xml";
	}

	public String getRuta() {
		return ruta;
	}

	public void setRuta(String ruta) {
		this.ruta = ruta;
	}

	public boolean setOption(String option, String val) {
		boolean result = false;
		if (option != null) {
			result = setText(option, val);
		}
		return result;
	}

	public String getOption(String option) {
		String result = "";
		if (option != null) {
			result = getText(option);
		}
		return result;
	}

	public boolean setText(String option, String val) {
		boolean result = false;
		try {
			SAXBuilder builder = new SAXBuilder();
			Document doc = builder.build(FILE_SERVER_CONFIG);
			Element dl = doc.getRootElement();
			Element sc = dl.getChild(SERVER_CONFIG_TAG);
			List options = sc.getChildren();
			Iterator i = options.iterator();
			while (i.hasNext()) {
				Element o = (Element) i.next();
				if (o.getName().equals(option)) {
					o.setText(val);
					XMLOutputter xml = new XMLOutputter();
					FileOutputStream out = new FileOutputStream(
							FILE_SERVER_CONFIG);
					xml.output(doc, out);
					out.close();
					result = true;
					break;
				}
			}
		} catch (Exception e) {
			log.error(e);
		}

		return result;
	}

	public String getText(String option) {
		String result = "";
		try {
			SAXBuilder builder = new SAXBuilder();
			Document doc = builder.build(FILE_SERVER_CONFIG);
			Element dl = doc.getRootElement();
			Element sc = dl.getChild(SERVER_CONFIG_TAG);
			List options = sc.getChildren();
			Iterator i = options.iterator();
			while (i.hasNext()) {
				Element o = (Element) i.next();
				if (o.getName().equals(option)) {
					result = o.getText();
					break;
				}
			}
		} catch (Exception e) {
			log.error(e);
		}

		return result;
	}

	public String getXmlFile() {
		StringBuffer result = new StringBuffer();
		String tmp = null;
		try {
			BufferedReader br = new BufferedReader(new FileReader(
					FILE_SERVER_CONFIG));
			tmp = br.readLine();
			while (tmp != null) {
				result.append(tmp + "\n");
				tmp = br.readLine();
			}
		} catch (Exception e) {
			log.error(e);
		}
		return result.toString();
	}

	public static void main(String[] args) {
		ServerConfXml scxml = new ServerConfXml("");
		String bdcol_home = scxml.getOption("bdcol_metadata");
		System.out.println("Valor de bdcol_metadata=" + bdcol_home);
		scxml.setOption("bdcol_home", "c:/temp/bdcol/meta");
		System.out.println("Valor nuevo de bdcol_metadata="
				+ scxml.getOption("bdcol_metadata"));
		System.out.println("Archivo de configuraci_n:");
		System.out.print(scxml.getXmlFile());

	}
}