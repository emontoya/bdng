package org.bdng.harvmgr.harvester;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.Vector;

import org.apache.log4j.Logger;
import org.bdng.harvmgr.dlConfig;
import org.bdng.harvmgr.process.Processor;

/**
 * La clase <code>HarvesterAbs.java</code>
 * <p>
 * Recolecta los DataProviders OAI y los almacena localmente.
 * </p>
 * 
 * @author Castulo Ramirez Londono. UNIVERSIDAD EAFIT.
 * @version 1.0, 01-jun-2008
 * @since JDK1.6.0_05
 */
public abstract class HarvesterAbs {

	protected WriteXML writeXML = new WriteXML();

	protected static MyResources myResources = new MyResources();

	protected static dlConfig properties = null;

	protected boolean PROXY_ON = false;

	protected Logger log;

	protected static String strPath;

	protected boolean blnRecolecto;

	protected boolean blnFueExitoso;

	protected static boolean continuar;

	protected long tiempoTotal = 0;

	protected int numeroRegistros = 0;

	protected int resultadoTest = -1;

	protected int inIntentosError = 0;

	protected Thread[] hilosActuales = null;

	protected Vector<Thread> processorsActuales = null;

	protected Vector<Processor> processors = null;

	protected static Integer idProcessor = 1;

	boolean firstCheck = true;

	private HashMap<String, Character> mapeadoChars = new HashMap<String, Character>();

	// protected static ResourceBundle properties = myResources;

	protected static SimpleDateFormat javFormat_ = new SimpleDateFormat(
			"yyyyMMddHHmmssS");
	protected static SimpleDateFormat javFormatHarvest_ = new SimpleDateFormat(
			"yyyy-MM-dd");

	// public static boolean PROXY_ON =
	// Boolean.parseBoolean(properties.getString("bln_proxy"));
	// public static boolean PROXY_ON =
	// Boolean.parseBoolean(properties.getString("proxy"));

	public HarvesterAbs() {
		// construirMapCaracteres();

	}

	private void construirMapCaracteres() {
		// mapeadoChars.put("ó", Character.valueOf('_'));
		// mapeadoChars.put("í",Character.valueOf('_'));
		// mapeadoChars.put("á",Character.valueOf('_'));
		// mapeadoChars.put("ñ",Character.valueOf('_'));
		// mapeadoChars.put("é",Character.valueOf('_'));
		// mapeadoChars.put("ú",Character.valueOf('_'));

	}

	public String parsearCaracteresTexto(String textoSalida)
			throws UnsupportedEncodingException {
		char[] textoEntradaCaracteres = textoSalida.toCharArray();

		char[] textoSalidaCaracteres = new char[textoEntradaCaracteres.length + 5];
		// ArrayList<Character> textoSalidaArray = new ArrayList<Character>();
		// System.out.println(textoSalidaCaracteres.length);
		Set set = mapeadoChars.entrySet();
		int caracteres = 0;
		// StringBuilder texto=new StringBuilder();
		for (int i = 0; i < textoEntradaCaracteres.length - 1; i++) {
			// Get a set of the entries

			// Get an iterator
			Iterator iterator = set.iterator();
			// Display elements
			while (iterator.hasNext()) {
				Map.Entry me = (Map.Entry) iterator.next();
				if (me.getKey()
						.equals(String.valueOf(textoEntradaCaracteres[i])
								+ String.valueOf(textoEntradaCaracteres[i + 1]))) {
					// textoSalidaArray.add((Character) me.getValue());
					textoSalidaCaracteres[i] = (Character) me.getValue();
					System.out.println("OEEE");
					// texto.append(me.getValue());
					i++;

					break;
				} else {
					// textoSalidaArray.add((Character)
					// textoEntradaCaracteres[i]);
					textoSalidaCaracteres[i] = textoEntradaCaracteres[i];

					// texto.append(textoEntradaCaracteres[i]);
				}

				// System.out.print(me.getKey() + ": ");
				// System.out.println(me.getValue());
			}

		}

		// String strTexto = texto.toString();

		String texto = new String(textoSalidaCaracteres);
		int index = texto.indexOf("</lor");
		String textoTemp = texto.substring(0, index + 5);
		texto = textoTemp + ">";

		System.out.println(texto.substring(texto.length() - 5, texto.length()));
		System.out.println(texto.substring(0, 200));
		return texto;
	}

	/**
	 * Crea los nombres de los archivos con la fecha actual del sistema
	 * 
	 * @return String
	 */
	protected String armarNombreFile() {
		Date fechaActual = new Date();
		String stFecha = javFormat_.format(fechaActual);
		return stFecha;
	}

	/**
	 * Utiliza la sigla para crear un directorio d_nde se almacenara el material
	 * recolectado.
	 * 
	 * @param sigla
	 */
	protected void armarRepositorio(String rutaRepositorio) {
		try {
			File file = new File(rutaRepositorio);
			// deleteDirectory(file);
			file.mkdirs();
			file = null;
		} catch (Exception e) {

		}
	}

	/**
	 * Descarga un archivo XML a traves de una conexion URL
	 * 
	 * @param stUrl
	 *            URL a descargar
	 * @return String con el archivo descargado
	 * @throws IOException
	 *             En caso de error de conexion URL
	 */
	protected String downloadDataFromURL(String stUrl) throws IOException {
		// System.out.println("PRECREATINGURL");

		URL u = new URL(stUrl);
		// System.out.println("DOWNLOADDATA");
		/*
		 * SAXBuilder builder = new SAXBuilder();
		 * 
		 * // command line should offer URIs or file names try {
		 * builder.build(stUrl); // If there are no well-formedness errors, //
		 * then no exception is thrown System.out.println(stUrl +
		 * " is well-formed."); } // indicates a well-formedness error catch
		 * (JDOMException e) { System.out.println(builder.toString());
		 * System.out.println(stUrl + " is not well-formed.");
		 * System.out.println(e.getMessage()); }
		 */
		URLConnection uc = u.openConnection();
		uc.setReadTimeout(60000);
		String stSalidaVerbo = "";
		BufferedReader in = null;

		in = new BufferedReader(new InputStreamReader(uc.getInputStream(),
				"UTF-8"));

		StringBuilder sb = new StringBuilder();

		String tmp = null;
		char[] arrayChars;
		// System.out.println("ANTES DE DESCARGAR");
		while ((tmp = in.readLine()) != null) {
			arrayChars = tmp.toCharArray();
			for (int i = 0; i < arrayChars.length; i++) {
				if ((arrayChars[i] == 0x9)
						|| (arrayChars[i] == 0xA)
						|| (arrayChars[i] == 0xD)
						|| ((arrayChars[i] >= 0x20) && (arrayChars[i] <= 0xD7FF))
						|| ((arrayChars[i] >= 0xE000) && (arrayChars[i] <= 0xFFFD))
						|| ((arrayChars[i] >= 0x10000) && (arrayChars[i] <= 0x10FFFF)))
					sb.append(arrayChars[i]);
			}

		}
		stSalidaVerbo = sb.toString();

		int indexOfEncoding = stSalidaVerbo.substring(0, 100).toLowerCase()
				.indexOf(("iso-8859-1"));

		// if(indexOfEncoding!=-1){
		if (stSalidaVerbo.substring(0, 100).toLowerCase()
				.contains("iso-8859-1")) {
			// System.out.println("Entre al encoding");
			uc = u.openConnection();
			uc.setReadTimeout(60000);
			stSalidaVerbo = "";
			in = null;

			in = new BufferedReader(new InputStreamReader(uc.getInputStream(),
					"ISO-8859-1"));

			sb = new StringBuilder();

			tmp = null;
			arrayChars = null;
			// System.out.println("ANTES DE DESCARGAR");
			while ((tmp = in.readLine()) != null) {
				arrayChars = tmp.toCharArray();
				for (int i = 0; i < arrayChars.length; i++) {
					if ((arrayChars[i] == 0x9)
							|| (arrayChars[i] == 0xA)
							|| (arrayChars[i] == 0xD)
							|| ((arrayChars[i] >= 0x20) && (arrayChars[i] <= 0xD7FF))
							|| ((arrayChars[i] >= 0xE000) && (arrayChars[i] <= 0xFFFD))
							|| ((arrayChars[i] >= 0x10000) && (arrayChars[i] <= 0x10FFFF)))
						sb.append(arrayChars[i]);
				}

			}

			stSalidaVerbo = sb.toString();
			byte[] utf8 = stSalidaVerbo.getBytes("UTF-8");
			stSalidaVerbo = new String(utf8);
			// stSalidaVerbo=parsearCaracteresTexto(stSalidaVerbo);
		}
		// }
		return stSalidaVerbo;
	}

	/**
	 * Anuncia al processor un nuevo archivo para procesar
	 * 
	 * @param dp
	 *            DataProvider
	 * @param path
	 *            Path del archivo recolectado
	 * @param p
	 *            Processor
	 */

	protected void iniciarProcesamientoDeArchivo(DataProvider dp, String path,
			Processor p) {
		String xmlFormat = dp.getMetadataPrefix();
		String filename = dp.getShortName() + "_" + dp.getLastIdFile() + ".xml";
		// System.out.println("anunciando para procesar "+ filename+" en "+path
		// );
		// p.anunciarArchivoListo(xmlFormat,path, filename,dp); // se utiliza
		// cuando se ejecuta el procesador en su propio hilo
		p.procesarArchivo(xmlFormat, path, filename, dp, "");
	}

	/**
	 * Realiza las actualizaciones necesarias tanto en el archivo xml de data
	 * providers como en el log
	 * 
	 * @param dp
	 *            DataProvider finalizado exitosamente
	 */

	protected void finalizar(DataProvider dp) {
		if ((blnRecolecto) && (blnFueExitoso)) {
			if (continuar) {
				dp.setHarvestStatus(DataProvider.ENDED);
				dp.updateLastHarvestDate();
				dp.incHarvestCount(); // dp.resetScheduleStatus();
				dp.setScheduleStatus(DataProvider.NOT_READY);
			} else {
				dp.setHarvestStatus(DataProvider.ENDED); // temporal, pues
															// deberia de ser
															// PAUSE, pero por
															// alguna razon para
															// HTTP finaliza OK
															// y llega aqui
			}
			StatsManager.getInstance(dp).setSuccessfull(true);
			dp.setLastDate(obtenerFecha()); // hace el set de la fecha actual
											// como ultima recoleccion exitosa

			// la siguiente linea deberia de acomodarse en la clase Dataprovider
			dp.setText2(dp.getShortName(), "lastrecs_harvester_ok", ""
					+ StatsManager.getInstance(dp).getNumber_registers());
		}
	}

	/**
	 * Retorna la fecha actual en formato yyyy-MM-dd, es utilizada para guardar
	 * los documentos xml por fechas.
	 * 
	 * @return String fecha
	 */

	protected String obtenerFecha() {
		String fecha = "";
		Date date = new Date();
		SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
		fecha = formato.format(date);
		return fecha;
	}

	/**
	 * En caso de proxy habilitado, lo configura
	 */
	protected void configurarProxy() {
		Properties systemSettings = System.getProperties();
		systemSettings
				.put("http.proxyHost", properties.getString("proxy_host"));
		systemSettings
				.put("http.proxyPort", properties.getString("proxy_port"));
	}

	/**
	 * Elimina un directorio recursivamente, tenga o no datos.
	 * 
	 * @param path
	 * @return boolean
	 */
	public static boolean deleteDirectory(File path) {
		if (path.exists()) {
			File[] files = path.listFiles();
			for (int i = 0; i < files.length; i++) {
				if (files[i].isDirectory()) {
					deleteDirectory(files[i]);
				} else {
					files[i].delete();
				}
			}
		}
		return (path.delete());
	}
}
