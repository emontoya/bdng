package org.bdng.harvmgr.harvester;

import java.util.ListResourceBundle;

public class MyResources extends ListResourceBundle {
	@Override
	protected Object[][] getContents() {
		return new Object[][] {
				// #BDCOL General
				{ "dl_home", "c:/bdcol/" },
				{ "dl_metadata", "c:/bdcol/metadata/" },
				{ "dl_data", "c:/bdcol/data/" },
				{ "dl_name", "bdcol" },
				{ "dl_col", "general" },
				// #Configuraci_n de eXist
				{ "exist_uri", "xmldb:exist://localhost:8080/exist/xmlrpc" },
				{ "exist_driver", "org.exist.xmldb.DatabaseImpl" },
				{ "existUser", "admin" }, { "existPass", "admin" },
				// #XML Coding
				{ "recordXML", "oai_dc:dc" },
				// #Configuracion del proxy
				{ "proxy", "proxy.eafit.edu.co" }, { "port", "8080" },
				{ "bln_proxy", "true" } };
	}
}