package org.bdng.harvmgr.harvester;

import java.io.File;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.bdng.harvmgr.dlConfig;
import org.bdng.harvmgr.process.Processor;
import org.w3c.dom.Document;

/**
 * Gestiona la recoleccion por el protocolo HTTP
 * 
 * @author sebastian
 * 
 */

public class HTTPHarvester extends HarvesterAbs {

	static boolean continuar = true;
	Harvester harvester = null;
	int dataProvidersSize = 0;
	boolean encodingISO = false;

	/**
	 * Constructor de la aplicacion, (inicializa el Logger que recibe el path
	 * original adem_s de la ubicaci_n exacta del archivo log4j.properties)
	 * 
	 * @param strPath
	 *            Path absoluto de la aplicacion para cargar las propiedades
	 * @author sebastian & daniel
	 */
	public HTTPHarvester(String strPath) {
		HarvesterAbs.strPath = strPath;
		properties = new dlConfig(strPath);
		// System.out.println(properties.getString("dl_metadata"));
		PROXY_ON = Boolean.parseBoolean(properties.getString("proxy"));
		log = Logger.getLogger(HTTPHarvester.class);
		PropertyConfigurator.configure(strPath + "/conf/log4j.properties");
	}

	/**
	 * Detiene la recoleccion de todos los proveedores HTTP
	 */
	public void noContinuar() {
		continuar = false;
	}

	/**
	 * Recolecta un proveedor HTTP recibiendo un string con su ruta
	 * 
	 * @param url
	 *            URL a descargar
	 */
	public void recolectarProvider(String url) {

		try {
			HTTPVisitor hv = new HTTPVisitor();
			HashMap<String, String> paths = hv.getXMLPaths(url);
			Iterator<Map.Entry<String, String>> iterator = paths.entrySet()
					.iterator();
			while (iterator.hasNext()) {
				Map.Entry<String, String> entrada = iterator.next();
				String filename = entrada.getKey();
				String path = entrada.getValue();
				String textoSalida = downloadDataFromURL(path);
				String filePath = properties.getString("dl_metadata") + "/"
						+ ServerConfXml.DBXML + "temp/";
				Document doc = writeXML
						.getDocument(textoSalida/* , encodingISO */);
				writeXML.writeXmlFile(doc, filePath, filename);
			}
		} catch (Exception e) {

		}
	}

	/**
	 * Recolecta un proveedor HTTP recibiendo un data provider. Envia a
	 * procesamiento los resultados
	 * 
	 * @param dp
	 *            DataProvider a recolectar
	 * @param p
	 *            Processor para enviar una vez recolectado
	 * @throws Exception
	 * 
	 */
	public void recolectarProvider(DataProvider dp, Processor p)
			throws Exception {

		/* Obtener informaci_n del data provider */
		String baseURL = dp.getBaseURL();
		String medataPrefix = dp.getMetadataPrefix();

		/* Crea el path para guardar */

		String pathSalida = properties.getString("dl_metadata") + "/"
				+ ServerConfXml.DBXML + "temp/" + dp.getShortInst() + "/"
				+ dp.getShortName();
		try {

			HTTPVisitor hv = new HTTPVisitor();
			HashMap<String, String> paths = hv.getXMLPaths(baseURL);

			log.info("Recolectando HTTP..." + dp.getShortName());
			tiempoTotal = 0;
			numeroRegistros = 0;
			resultadoTest = -1;
			blnRecolecto = false;
			blnFueExitoso = true;

			Iterator<Map.Entry<String, String>> iterator = paths.entrySet()
					.iterator();
			StatsManager.getInstance(dp).setSuccessfull(true);
			dp.setHarvestStatus(DataProvider.PROCESSING);

			if (PROXY_ON) {
				configurarProxy();
			}

			int numeroRegistros = 0;

			/* Proceso de borrado de directorios recursivamente */

			HarvesterAbs.deleteDirectory(new File(pathSalida));
			HarvesterAbs.deleteDirectory(new File(pathSalida.replaceAll(
					"dbxmltemp", "dbxmltemp-" + obtenerFecha())));
			HarvesterAbs.deleteDirectory(new File(pathSalida.replaceAll(
					"dbxmltemp", "dbxml")));

			/* Iniciar contador del tiempo de recolecci_n */
			long tiempoInicialRecoleccion = System.currentTimeMillis();

			if (paths.size() == 0) {
				/* No existe directorio, path apuntando directamente al archivo */
				procesarEntrada(dp, p, pathSalida, baseURL);
				if (StatsManager.getInstance(dp).isSuccessfull()) {
					numeroRegistros++;

				}
				System.out.println("Recolectando repositorio '"
						+ dp.getShortName() + "'..." + dp.getNrorecs());
			} else {
				while (iterator.hasNext()) {

					/* Obtener entrada a partir del iterador del Map */
					Map.Entry<String, String> entrada = iterator.next();

					/* Obtengo el path */
					String path = entrada.getValue();

					/* Procesar Map Entry para recolecci_n y Procesado */
					procesarEntrada(dp, p, /* entrada, */pathSalida, path);

					if (StatsManager.getInstance(dp).isSuccessfull()) {
						numeroRegistros++;

					}
					System.out.println("Recolectando repositorio '"
							+ dp.getShortName() + "'..." + dp.getNrorecs());

				}
			}

			dp.setHarvestStatus(DataProvider.ENDED);
			dp.updateLastHarvestDate();
			dp.setScheduleStatus(DataProvider.NOT_READY);

			/* Obtener tiempo al finalizar recolecci_n */
			long tiempoFinalRecoleccion = System.currentTimeMillis();
			StatsManager.getInstance(dp).setHarvesterTime(
					tiempoFinalRecoleccion - tiempoInicialRecoleccion);
			StatsManager.getInstance(dp).setNumber_files(numeroRegistros);

			/*
			 * Fijar variables de finalizaci_n de la recolecci_n en el
			 * dataProvider
			 */
			finalizar(dp);
			StatsManager st = new StatsManager();
			st.writeStatsInDisk(StatsManager.getInstance(dp), properties);

		} catch (Exception e) {

			/*
			 * Si existe un error en la recolecci_n, se debe deshacer lo
			 * previamente recolectado y obtener la _ltima recolecci_n exitosa y
			 * procesar los archivos de la misma
			 */
			// log.error(e);
			System.out.println(" SE HA PRESENTADO UN ERROR!");
			System.out.println(" Recolectando ultima recoleccion exitosa ...");

			numeroRegistros--;
			inIntentosError++;
			blnFueExitoso = false;
			String stError = e.getMessage();
			if (e instanceof UnknownHostException)
				stError += ":HostDesconocido";
			else if (e instanceof IOException)
				stError += ":Error de entrada y salida";
			dp.setHarvestStatus(DataProvider.ERROR);
			log.error(dp.getShortName() + ">" + stError);
			dp.setMessage(stError);
			StatsManager.getInstance(dp).setSuccessfull(false);

			String lastdate = dp.getLastDate();
			// System.out.println("lastdate OK "+lastdate);
			System.out.println(" Ultima recoleccion exitosa: " + lastdate);

			if (!lastdate.equals("0000-00-00")) {

				HarvesterAbs.deleteDirectory(new File(pathSalida.replaceAll(
						"dbxmltemp", "dbxml"))); // borramos los archivos
													// actuales
				int doc = 0;
				strPath = pathSalida.replaceAll("dbxmltemp", "dbxmltemp" + "-"
						+ lastdate);
				/* Ciclo Ejecutado por cada Archivo */
				while (true) {

					/* Variable utilizada para recorrer documentos */
					doc++;
					String xmlFormat = dp.getMetadataPrefix();
					String filenameInException = dp.getShortName() + "_" + doc
							+ ".xml";

					File f = new File(strPath + "/" + filenameInException);
					log.error(f.toString());
					log.error(f.exists());

					/*
					 * Si no existe fue porque se ha terminado el ultimo
					 * documento, o el directorio esta vacio
					 */
					if (!f.exists()) {
						break;
					}
					System.out
							.println("Proceso de recuperacion finalizado ... "
									+ strPath + "/" + filenameInException);

					p.procesarArchivo(xmlFormat, strPath, filenameInException,
							dp, "-" + lastdate);

				}

			} else {
				System.out
						.println(" No se encontro recoleccion exitosa previa! ");
			}
			dp.incContFiles();

		}

	}

	/**
	 * Realiza la recolecci_n de los archivos, crea los directorios para guardar
	 * y llama al m_todo que iniciar_ el procesamiento para cada uno de ellos.
	 * 
	 * @param dp
	 *            DataProvider finalizado exitosamente
	 * @param p
	 *            Processor para enviar una vez recolectado
	 * @param entrada
	 *            Map.Entry<String, String> que contiene un path de descarga
	 *            (Key and Value)
	 * @param pathSalida
	 *            String con la ubicaci_n del directorio para guardar
	 * @throws Exception
	 *             Principalmente lanzada de los metodos provenientes de la
	 *             clase WriteXML.java, es controlada en el metodo
	 *             recolectarProvider(DataProvider dp, Processor p)
	 * @return none
	 * @author Daniel Duque
	 * @since 20/12/2011
	 */

	public void procesarEntrada(DataProvider dp, Processor p,
	/* Entry<String, String> entrada, */String pathSalida, String path)
			throws Exception {
		boolean encodingISO = false;
		// String filename = entrada.getKey();
		// String path = entrada.getValue();
		String textoSalida = downloadDataFromURL(path);

		/* Testing */
		// String sFichero = "C:/Users/bdcol2/fichero.xml";
		// File fichero = new File(sFichero);
		// BufferedWriter BW = new BufferedWriter(new FileWriter(sFichero));
		// BW.write(textoSalida);
		// BW.flush();
		// BW.close();
		//
		// TODO: Solucionar dinamicamente el dtd
		/* SOLUCION TEMPORAL PROBLEMA DTD y ENCODING */
		if (textoSalida.contains(".dtd")) {
			textoSalida = textoSalida.substring(0,
					textoSalida.indexOf("DOCTYPE") - 2)
					+ textoSalida
							.substring(
									textoSalida.indexOf("DOCTYPE")
											+ ((textoSalida.indexOf(".dtd") + 6) - textoSalida
													.indexOf("DOCTYPE")),
									textoSalida.length());
		}
		if (textoSalida.toLowerCase().contains("iso-8859-1")) {
			int indexEncoding = textoSalida.indexOf("encoding=");
			int indexFinalEncoding = textoSalida.indexOf("?>");
			String textoSalidaPreEncoding = textoSalida.substring(0,
					(indexEncoding + 9));
			// System.out.println(textoSalidaPreEncoding+" primera parte!!");
			String textoSalidaAfterEncoding = textoSalida.substring(
					(indexFinalEncoding), textoSalida.length());
			// System.out.println(textoSalidaAfterEncoding.substring(0,250)+" segunda parte!!");
			textoSalida = textoSalidaPreEncoding + "\"UTF-8\""
					+ textoSalidaAfterEncoding;
			// System.out.println("FULLLLLLLLL!!!"+textoSalida.substring(0,100));
		}

		Document doc = writeXML.getDocument(textoSalida/* , encodingISO */);
		String fileSalida = dp.getShortName() + "_" + dp.getLastIdFile()
				+ ".xml";

		writeXML.writeXmlFile(doc, pathSalida, fileSalida);
		/* Escribe en disco en una carpeta con la fecha actual */
		writeXML.writeXmlFile(
				doc,
				pathSalida.replaceAll("dbxmltemp", "dbxmltemp-"
						+ obtenerFecha()), fileSalida);

		String xmlFormat = dp.getMetadataPrefix();

		/* Escribe en la carpeta temporal */
		writeXML.writeXmlFile(doc, pathSalida.replaceAll("dbxmltemp", "dbxml"),
				fileSalida);

		/*
		 * Iniciar Procesamiento de ARCHIVO, se anuncia el archivo listo para
		 * procesar
		 */

		// p.anunciarArchivoListo(xmlFormat, pathSalida, fileSalida,
		// dp);

		iniciarProcesamientoDeArchivo(dp, pathSalida, p);

		/* Procesamiento exitoso, booleano blnRecolecto fijado a true */
		blnRecolecto = true;
		dp.incLastIdFile();
	}

	public static void main(String[] args) {

		HTTPHarvester hh = new HTTPHarvester("/temp/bdcol-luis");
		hh.recolectarProvider("http://dis.eafit.edu.co/bdeafit");
	}

	/**
	 * Metodo principal de la clase. Coordina la recoleccion y envio a
	 * procesamiento de un Data Provider
	 * 
	 * @param dp
	 *            Data provider a recolectar
	 * @param p
	 *            Processor a utilizar
	 * @throws Throwable
	 *             En caso de error
	 * @author sebastian
	 * @since 24/03/2010
	 */
	public void ejecutarRecoleccionHTTP(DataProvider dp, Processor p)
			throws Throwable {
		String rutaRepositorio = properties.getString("dl_metadata");
		rutaRepositorio = rutaRepositorio + "/dbxml/" + dp.getShortInst() + "/"
				+ dp.getShortName();
		this.armarRepositorio(rutaRepositorio);
		this.recolectarProvider(dp, p);
		p.setContinuar(false);
		p.signal.release(10);
		finalize();
	}
}
