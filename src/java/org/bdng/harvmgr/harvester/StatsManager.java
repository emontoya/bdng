package org.bdng.harvmgr.harvester;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Vector;

import org.apache.log4j.Logger;
import org.bdng.harvmgr.dlConfig;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

/**
 * Clase principal que maneja la estadistica del sistema. Es un objeto unico que
 * controla toda la estadistica de una recoleccion
 * 
 * @author sebastian
 * @since 28/04/2010
 * 
 */
public class StatsManager {

	Logger log = Logger.getLogger(StatsManager.class);

	/* Objeto que permite unicidad de gestion de estadistica (Singleton) */
	private static StatsManager statsManager;

	/* vector con los stats de cada proveedor */
	private static Vector<StatsProvider> vectorStats;

	private dlConfig dlconfig;

	private SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy");

	Document statsDocument = null;

	public static void init(Collection<DataProvider> dataProviders,
			dlConfig dlconfig) {
		statsManager = new StatsManager();
		statsManager.dlconfig = dlconfig;
		vectorStats = new Vector<StatsProvider>();
		Date date = new Date();
		/* inicializo los elementos de stats de los providers dados */
		for (DataProvider dp : dataProviders) {
			StatsProvider statsProvider = new StatsProvider();
			statsProvider.setShortName(dp.getShortName());
			statsProvider.setRepositoryName((dp.getRepositoryName()));
			statsProvider.setInstitution((dp.getInstitution()));
			statsProvider.setDate(date);
			vectorStats.add(statsProvider);
		}
	}

	/**
	 * Obtiene el objeto de stats de un data provider
	 * 
	 * @return
	 */
	public static StatsProvider getInstance(DataProvider dp) {
		if (statsManager == null)
			return null;
		return statsManager.getProviderStatObject(dp);
	}

	/**
	 * Obtiene el manager de stats para efectos de inicializacion y escritura
	 * 
	 * @return
	 */
	public static StatsManager getInstance() {
		return statsManager;
	}

	/**
	 * Busca en el vector el objeto de stats del data provider y lo retorna
	 * 
	 * @param dataProvider
	 *            shortname a buscar
	 * @return objeto statsprovider correspondiente al proveedor
	 */
	private StatsProvider getProviderStatObject(DataProvider dataProvider) {
		for (StatsProvider statsProvider : vectorStats) {
			if (statsProvider.getShortName()
					.equals(dataProvider.getShortName())) {
				return statsProvider;
			}
		}
		return null;
	}

	/**
	 * Guarda en Disco la estadistica que se encuentra en memoria. Solo se debe
	 * ejecutar al final de recoleccion. Genera 2 archivos: 1. Estad_stica por
	 * harvester: El tag padre es el harvester y cada hijo representa un
	 * proveedor recolectado en ese harvester. 2. Estadistica por proveedor: El
	 * tag padre es el proveedor, y posee cada harvester como hijo.
	 * 
	 * @param dlconfig
	 * @throws JDOMException
	 *             Si el documento original de stats esta mal formado
	 * @throws IOException
	 *             io error
	 */
	public synchronized void writeStatsInDisk(StatsProvider statsProvider,
			dlConfig dlconfig) throws JDOMException, IOException {
		synchronized (statsProvider) {
			/* Graba la estad_stica por harvester */
			// dlConfig dlconfig = new dlConfig();
			SAXBuilder builder = new SAXBuilder();
			statsDocument = null;
			try {
				statsDocument = builder.build(dlconfig
						.getStatsForHarvesterFile());

			} catch (Exception e) {
				System.out.println("repositorio = "
						+ statsProvider.getShortName() + "<>" + e.getMessage());
				File f = new File(dlconfig.getStatsForHarvesterFile());
				f.createNewFile();
				/* the file does not exist. */
			}
			if (statsDocument == null) {
				statsDocument = new Document();
				Element stats = new Element("stats");
				statsDocument.setRootElement(stats);
			}
			Element elementoParaHarvester = generateStatsPerHarvesterJDomElement(statsProvider);
			// Element consecutive = new Element("id");
			// consecutive.setText(String.valueOf(statsDocument.getRootElement().getChildren().size()
			// + 1));
			// elementoParaHarvester.addContent(0, consecutive);
			statsDocument.getRootElement().addContent(elementoParaHarvester);
			XMLOutputter xmlOutputter = new XMLOutputter(
					Format.getPrettyFormat());
			// File f = new File(dlconfig.getStatsForHarvesterFile());
			// f.createNewFile();
			FileOutputStream out = new FileOutputStream(
					dlconfig.getStatsForHarvesterFile());
			xmlOutputter.output(statsDocument, out);
			out.close();
			/* Graba la estad_stica por data provider */
			builder = new SAXBuilder();
			createproviderStats(statsProvider, dlconfig);
		}

	}

	/**
	 * Este metodo crea la estadistica para un proveedor en el modo de
	 * estadistica por proveedor.
	 * 
	 * @param statsDocument
	 *            documento xml de estadistica por proveedor.
	 * @param statsProvider
	 * @param dlconfig
	 */
	private synchronized void createproviderStats(StatsProvider statsProvider,
			dlConfig dlconfig) {

		// dlConfig dlconfig = new dlConfig();
		Element providerElement = null;
		SAXBuilder builder = new SAXBuilder();
		Document statsDocument = null;
		try {
			statsDocument = builder.build(dlconfig.getStatsForProviderPath()
					+ "/" + statsProvider.getShortName() + ".xml");
		} catch (JDOMException e) {
		} catch (IOException e) {
		}
		/*
		 * Si entra a este if es por que es la primera vez que se recolecta este
		 * proveedor, entonces lo creamos
		 */
		if (statsDocument == null) {
			statsDocument = new Document();
			providerElement = new Element("providerstats");
			statsDocument.setRootElement(providerElement);
			// Element shortName = new Element("shortName");
			// shortName.setText(statsProvider.getShortName());
			Element repositoryName = new Element("repositoryName");
			repositoryName.setText(statsProvider.getRepositoryName());
			Element stats = new Element("stats");
			providerElement.addContent(repositoryName);
			providerElement.addContent(stats);
		}
		Element stats = statsDocument.getRootElement().getChild("stats");
		Element harvester = new Element("harvester");
		stats.addContent(harvester);
		// Element shortNameElement = new Element("shortName");
		// shortNameElement.setText(statsProvider.getShortName());
		// harvester.addContent(shortNameElement);
		Element repositoryNameElement = new Element("repositoryName");
		repositoryNameElement.setText(statsProvider.getRepositoryName());
		harvester.addContent(repositoryNameElement);
		Element institutionElement = new Element("institution");
		institutionElement.setText(statsProvider.getInstitution());
		harvester.addContent(institutionElement);
		Element successfullElement = new Element("successfull");
		successfullElement.setText((statsProvider.isSuccessfull() ? "true"
				: "false"));
		harvester.addContent(successfullElement);
		Element dateElement = new Element("date");
		dateElement.setText(dateFormatter.format(statsProvider.getDate()));
		harvester.addContent(dateElement);
		Element number_filesElement = new Element("number_files");
		number_filesElement.setText(String.valueOf(statsProvider
				.getNumber_files()));
		harvester.addContent(number_filesElement);
		Element number_registersElement = new Element("number_registers");
		number_registersElement.setText(String.valueOf(statsProvider
				.getNumber_registers()));
		harvester.addContent(number_registersElement);
		Element harvesterTimeElement = new Element("harvesterTime");
		harvesterTimeElement.setText(String.valueOf(statsProvider
				.getHarvesterTime()));
		harvester.addContent(harvesterTimeElement);
		/* saves! */
		XMLOutputter xmlOutputter = new XMLOutputter(Format.getPrettyFormat());
		File f = new File(dlconfig.getStatsForProviderPath() + "/"
				+ statsProvider.getShortName() + ".xml");
		try {
			f.createNewFile();
			FileOutputStream out = new FileOutputStream(
					dlconfig.getStatsForProviderPath() + "/"
							+ statsProvider.getShortName() + ".xml");
			xmlOutputter.output(statsDocument, out);
			out.close();
		} catch (IOException e) {
			log.error(e);
		}
	}

	/**
	 * Genera un documento xml con la estadistica de una recoleccion
	 * 
	 * @return elemento root de la estadistica NOTA: metodo no usado por el
	 *         momento..... edy
	 */
	private Element generateStatsPerHarvesterJDomElement() {
		Element rootElement = new Element("harvester");
		for (StatsProvider statsProvider : vectorStats) {
			Element dpElement = new Element("dataProvider");
			Element shortNameElement = new Element("shortName");
			shortNameElement.setText(statsProvider.getShortName());
			dpElement.addContent(shortNameElement);
			Element successfullElement = new Element("successfull");
			successfullElement.setText((statsProvider.isSuccessfull() ? "true"
					: "false"));
			dpElement.addContent(successfullElement);
			Element dateElement = new Element("date");
			dateElement.setText(dateFormatter.format(statsProvider.getDate()));
			dpElement.addContent(dateElement);
			Element number_filesElement = new Element("number_files");
			number_filesElement.setText(String.valueOf(statsProvider
					.getNumber_files()));
			dpElement.addContent(number_filesElement);
			Element number_registersElement = new Element("number_registers");
			number_registersElement.setText(String.valueOf(statsProvider
					.getNumber_registers()));
			dpElement.addContent(number_registersElement);
			Element harvesterTimeElement = new Element("harvesterTime");
			harvesterTimeElement.setText(String.valueOf(statsProvider
					.getHarvesterTime()));
			dpElement.addContent(harvesterTimeElement);
			rootElement.addContent(dpElement);
		}
		return rootElement;
	}

	/**
	 * Genera un documento xml con la estadistica de una recoleccion
	 * 
	 * @return elemento root de la estadistica
	 */

	private synchronized Element generateStatsPerHarvesterJDomElement(
			StatsProvider statsProvider) {
		// System.out.println("wrinting stats for "+statsProvider.getShortName()+" in StatsForHarvester.xml");
		// Element rootElement = new Element("harvester");
		Element dpElement = new Element("dataProvider");
		// Element shortNameElement = new Element("shortName");
		// shortNameElement.setText(statsProvider.getShortName());
		// dpElement.addContent(shortNameElement);
		Element repositoryNameElement = new Element("repositoryName");
		repositoryNameElement.setText(statsProvider.getRepositoryName());
		dpElement.addContent(repositoryNameElement);
		Element institutionElement = new Element("institution");
		institutionElement.setText(statsProvider.getInstitution());
		dpElement.addContent(institutionElement);
		Element successfullElement = new Element("successfull");
		successfullElement.setText((statsProvider.isSuccessfull() ? "true"
				: "false"));
		dpElement.addContent(successfullElement);
		Element dateElement = new Element("date");
		dateElement.setText(dateFormatter.format(statsProvider.getDate()));
		dpElement.addContent(dateElement);
		Element number_filesElement = new Element("number_files");
		number_filesElement.setText(String.valueOf(statsProvider
				.getNumber_files()));
		dpElement.addContent(number_filesElement);
		Element number_registersElement = new Element("number_registers");
		number_registersElement.setText(String.valueOf(statsProvider
				.getNumber_registers()));
		dpElement.addContent(number_registersElement);
		Element harvesterTimeElement = new Element("harvesterTime");
		harvesterTimeElement.setText(String.valueOf(statsProvider
				.getHarvesterTime()));
		dpElement.addContent(harvesterTimeElement);
		// rootElement.addContent(dpElement);
		return dpElement;
	}
}
