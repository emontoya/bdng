package org.bdng.harvmgr.harvester;

/**
 * 
 * @author eafit
 * 
 */
public final class FechaEnt {

	private int minDia;
	private int minMes;
	private int minAno;
	private int minHoras;
	private int minMinutos;
	private int minSegundos;
	private int minMilisegundos;

	/**
	 * @return
	 */
	public int getAno() {
		return minAno;
	}

	/**
	 * @return
	 */
	public int getDia() {
		return minDia;
	}

	/**
	 * @return
	 */
	public int getHoras() {
		return minHoras;
	}

	/**
	 * @return
	 */
	public int getMes() {
		return minMes;
	}

	/**
	 * @return
	 */
	public int getMilisegundos() {
		return minMilisegundos;
	}

	/**
	 * @return
	 */
	public int getMinutos() {
		return minMinutos;
	}

	/**
	 * @return
	 */
	public int getSegundos() {
		return minSegundos;
	}

	/**
	 * @param i
	 */
	public void setAno(int i) {
		minAno = i;
	}

	/**
	 * @param i
	 */
	public void setDia(int i) {
		minDia = i;
	}

	/**
	 * @param i
	 */
	public void setHoras(int i) {
		minHoras = i;
	}

	/**
	 * @param i
	 */
	public void setMes(int i) {
		minMes = i;
	}

	/**
	 * @param i
	 */
	public void setMilisegundos(int i) {
		minMilisegundos = i;
	}

	/**
	 * @param i
	 */
	public void setMinutos(int i) {
		minMinutos = i;
	}

	/**
	 * @param i
	 */
	public void setSegundos(int i) {
		minSegundos = i;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "A_o: " + getAno() + ", Mes:" + getMes() + ", Dia: " + getDia()
				+ ", Horas: " + getHoras() + ", Minutos: " + getMinutos()
				+ ", Segundos: " + getSegundos() + ", Milisegundos: "
				+ getMilisegundos();
	}

}
