package org.bdng.harvmgr.harvester;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.bdng.harvmgr.dlConfig;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

public class DataProvider {

	Logger log = Logger.getLogger(DataProvider.class);
	boolean PROXY_ON = false;

	/*
	 * OAI fields
	 */
	private String baseURL = "";
	private String metadataPrefix = "";
	private String repositoryName = "";
	private String schema = "";
	private String metadataNamespace = "";
	/*
	 * DL fields
	 */
	private String shortName = "";
	private String description = "";
	private int resumptionWithMetadataPrefix = 0;
	private String collection = "";
	private String urlhome;
	private String protocol;
	private String nameAdmin;
	private String emailadmin;
	private String phoneAdmin;
	private String reptype;
	private String institution;
	private String shortInst;
	private String staticCollection;
	private String departamento;
	private String municipio;
	private String bdcol;
	private int cdata;
	//private int contRegisters;
	private int contFiles;
	private int harvesterTime;

	private long nrorecs = 0;

	public void incNrorecs() {
		
		nrorecs++;

	}

	public long getNrorecs() {
		return nrorecs;
	}

	public void resetNrorecs() {
		nrorecs = 0;
	}

	// public static String FILE_OAI_PROVIDERS = "conf/FILE_OAI_PROVIDERS.xml";
	// // corregir
	String FILE_OAI_PROVIDERS = null;
	String FILE_OAI_PROVIDERS_STATUS = null;

	public static String OAI_PROVIDERS = "oai_providers";
	public static String OAI_PROVIDER = "oai_provider";

	public static String SHORTNAME = "shortName";
	public static String BASE_URL = "baseURL";
	public static String METADATA_PREFIX = "metadataPrefix";
	public static String REPOSITORY_NAME = "repositoryName";
	public static String COLLECTION = "collection";
	public static String DESCRIPTION = "description";
	public static String RESUMPTION = "resumption";

	public static String INSTITUTION = "institution";
	public static String STATICCOLLECTION = "staticCollection"; // add by Edwin,
																// jan 30 011
	public static String SHORTINST = "shortInst";
	public static String URLHOME = "urlhome";
	public static String PROTOCOL = "protocol";
	public static String NAMEADMIN = "nameAdmin";
	public static String EMAILADMIN = "emailadmin";
	public static String PHONEADMIN = "phoneAdmin";
	public static String REPTYPE = "reptype";
	public static String CDATA = "cdata";
	public static String INIT_LAST_ID_FILE = "1";
	public static String LAST_ID_FILE = "last_id_file";
	public static String DEPARTAMENTO = "departamento";
	public static String MUNICIPIO = "municipio";
	public static String BDCOL = "bdcol";
	public static String OWNER = "owner"; // add luis, feb 02

	public static String LASTDATE_HARVESTER_OK = "lastdate_harvester_ok";
	public static String LASTRECS_HARVESTER_OK = "lastrecs_harvester_ok";
	public static String NOT_STARTED = "NOT_STARTED";
	public static String PROCESSING = "PROCESSING";
	public static String ENDED = "ENDED";
	public static String ERROR = "ERROR";
	public static String PAUSE = "PAUSE";
	public static String INIT_HARVEST_STATUS = NOT_STARTED;
	public static String HARVEST_STATUS = "harvest_status";

	public static String INIT_LAST_RESUMPTION_TOKEN = "";
	public static String LAST_RESUMPTION_TOKEN = "last_resumption_token";

	public static String INIT_LAST_HARVEST_DATE = "0000-00-00T00:00:00";
	public static String LAST_HARVEST_DATE = "last_harvest_date";

	public static String INIT_LASTDATE_HARVESTER_OK = "0000-00-00";
	public static String INIT_LASTRECS_HARVESTER_OK = "0";

	public static String INIT_HARVEST_COUNT = "1";
	public static String HARVEST_COUNT = "harvest_count";

	public static String INIT_HARVEST_COUNT_INCREMENTAL = "4";
	public static String HARVEST_COUNT_INCREMENTAL = "harvest_count_incremental";

	public static String INIT_HARVEST_FRECUENCY = "8";
	public static String HARVEST_FRECUENCY = "harvest_frecuency";

	public static String SCHEDULE_STATUS = "schedule_status";
	public static String CONT_REGISTERS = "number_registers";
	public static String READY = "READY";
	public static String NOT_READY = "NOT_READY";

	public static String MESSAGE = "message";
	public static String BLANK = "";

	public static String INIT_ACTIVE = "5";// add luis, mar 15
	public static String ACTIVE = "active"; // add luis, mar 15

	static Document docProvidersStatus = null;

	private String ruta = "";

	// private String oai_providers = FILE_OAI_PROVIDERS;

	public String getRuta() {
		return ruta;
	}

	public void setRuta(String ruta) {
		// oai_providers = ruta + "/" + FILE_OAI_PROVIDERS;
		this.ruta = ruta + "/";
	}

	dlConfig dlconfig = null;

	public DataProvider() {
		dlconfig = new dlConfig();
		init();
	}

	public DataProvider(String path_home) {
		this.ruta = path_home;
		dlconfig = new dlConfig(path_home);
		init();

	}

	//public int getContRegisters() {
		//return contRegisters;
	//}

	// INCREMENT IN ONE THE CONT REGISTERS VARIABLE IN MEMORY
	//public void incContRegisters() {
		//this.contRegisters++;
	//}

	// WRITE THE CONT REGISTERS IN THE XML FILE
	public boolean writeContRegisters() {
		boolean result = false;
		if (shortName != null) {
			result = setText(shortName, CONT_REGISTERS,
					String.valueOf(nrorecs));
		}
		return result;
	}

	public void init() {
		FILE_OAI_PROVIDERS = dlconfig.getOaiProviders();
		FILE_OAI_PROVIDERS_STATUS = dlconfig.getOaiProvidersStatus();
		SAXBuilder builder = new SAXBuilder();
		try {
			docProvidersStatus = builder.build(FILE_OAI_PROVIDERS_STATUS);
		} catch (JDOMException e) {
			log.error(e);
		} catch (IOException e) {
			log.error(e);
		}
		PROXY_ON = Boolean.parseBoolean(dlconfig.getString("proxy"));
		if (PROXY_ON) {
			// System.out.println("PROXY_ACTIVADO");
			Properties systemSettings = System.getProperties();
			systemSettings.put("http.proxyHost",
					dlconfig.getString("proxy_host")); // http
			systemSettings.put("http.proxyPort",
					dlconfig.getString("proxy_port"));
		}
	}

	// IManagement mgt = null;
	/*
	 * public DataProvider(IManagement mgt) { this.mgt = mgt; }
	 */

	public String getLastDate() {
		String result = null;
		if (shortName != null) {
			result = getText2(shortName, LASTDATE_HARVESTER_OK);
		}
		return result;
	}

	public boolean setLastDate(String val) {
		boolean result = false;
		if (shortName != null) {
			result = setText2(shortName, LASTDATE_HARVESTER_OK, val);
		}
		return result;
	}

	public String getMessage() {
		String result = null;
		if (shortName != null) {
			result = getText2(shortName, MESSAGE);
		}
		return result;
	}

	public boolean setMessage(String val) {
		boolean result = false;
		if (shortName != null) {
			result = setText2(shortName, MESSAGE, val);
		}
		return result;
	}

	public boolean resetMessage() {
		boolean result = false;
		if (shortName != null) {
			result = setText2(shortName, MESSAGE, BLANK);
		}
		return result;
	}

	public String getScheduleStatus() {
		String result = null;
		if (shortName != null) {
			result = getText2(shortName, SCHEDULE_STATUS);
		}
		return result;
	}

	public String getInitHarvestFrecuency() {
		String result = null;
		if (shortName != null) {
			result = getText2(shortName, HARVEST_FRECUENCY);
		}
		return result;
	}

	public boolean setScheduleStatus(String val) {
		boolean result = false;
		if (shortName != null) {
			result = setText2(shortName, SCHEDULE_STATUS, val);
		}
		return result;
	}

	public boolean resetScheduleStatus(String repname) {
		boolean result = false;
		if (repname != null) {
			result = setText2(repname, SCHEDULE_STATUS, READY);
		}
		return result;
	}

	public String getBaseURL() {
		return baseURL;
	}

	public void setBaseURL(String baseURL) {
		this.baseURL = baseURL;
	}

	public String getMetadataPrefix() {
		return metadataPrefix;
	}

	public int getCdata() {
		return cdata;
	}

	public void setMetadataPrefix(String metadataPrefix) {
		this.metadataPrefix = metadataPrefix;
	}

	public String getRepositoryName() {
		return repositoryName;
	}

	public void setRepositoryName(String repositoryName) {
		this.repositoryName = repositoryName;
	}

	public String getSchema() {
		return schema;
	}

	public void setSchema(String schema) {
		this.schema = schema;
	}

	public String getMetadataNamespace() {
		return metadataNamespace;
	}

	public void setMetadataNamespace(String metadataNamespace) {
		this.metadataNamespace = metadataNamespace;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int isResumptionWithMetadataPrefix() {
		return resumptionWithMetadataPrefix;
	}

	public void setResumptionWithMetadataPrefix(int resumptionWithMetadataPrefix) {
		this.resumptionWithMetadataPrefix = resumptionWithMetadataPrefix;
	}

	public void writeResumptionWithMetadataPrefix(String repname, int val)
			throws Exception {
		if (repname != null) {
			setText(repname, RESUMPTION, String.valueOf(val));
		}
	}

	public String getCollection() {
		return collection;
	}

	public void setCollection(String collection) {
		this.collection = collection;
	}

	public String getUrlhome() {
		return urlhome;
	}

	public void setUrlhome(String urlhome) {
		this.urlhome = urlhome;
	}

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	public void setCdata(int cdata) {
		this.cdata = cdata;
	}

	public String getEmailadmin() {
		return emailadmin;
	}

	public void setEmailadmin(String emailadmin) {
		this.emailadmin = emailadmin;
	}

	public String getNameAdmin() {
		return nameAdmin;
	}

	public void setNameAdmin(String nameAdmin) {
		this.nameAdmin = nameAdmin;
	}

	public String getPhoneAdmin() {
		return phoneAdmin;
	}

	public void setPhoneAdmin(String phoneAdmin) {
		this.phoneAdmin = phoneAdmin;
	}

	public String getReptype() {
		return reptype;
	}

	public void setReptype(String reptype) {
		this.reptype = reptype;
	}

	public String getInstitution2() {
		return institution;
	}

	public String getInstitution() {
		String result = null;
		if (shortName != null) {
			result = getText(shortName, INSTITUTION);
		}
		return result;
	}

	public void setInstitution(String institution) {
		this.institution = institution;
	}

	public String getShortInst2() {
		return shortInst;
	}

	public String getShortInst() {
		String result = null;
		if (shortName != null) {
			result = getText(shortName, SHORTINST);
		}
		return result;
	}

	public void setShortInst(String shortInst) {
		this.shortInst = shortInst;
	}

	public void setStaticCollection(String staticCollection) {
		this.staticCollection = staticCollection;
	}

	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	public String getDepartamento() {
		return departamento;
	}
	
	public String getTagDepartamento() {
		return getText(shortName, DEPARTAMENTO);
	}
	
	/**
	 * 
	 * @return true filtra registros, false NO filtra registros (valor por defecto)
	 */
	public boolean filterBadRecords() {
		boolean result=false;
		String filter=getText(shortName, REPTYPE);
		
		if (filter!=null && filter.equals("1"))
			result = true;
		
		return result;
	}

	public String getMunicipio() {
		return municipio;
	}

	public String getStaticCollection2() {
		return staticCollection;
	}

	public String getBdcol() {
		return bdcol;
	}

	public void setBdcol(String bdcol) {
		this.bdcol = bdcol;
	}

	// add by Edwin, jan 30, 2011
	public String getStaticCollection() {
		String result = null;
		if (shortName != null) {
			result = getText(shortName, STATICCOLLECTION); // return "true" or
															// "false"
		}
		return result;
	}

	/**
	 * 
	 * @param shortName
	 * @param tag
	 * @return
	 */
	public synchronized String getText(String shortName, String tag) {
		String result = null;
		try {
			SAXBuilder builder = new SAXBuilder();
			Document doc = builder.build(FILE_OAI_PROVIDERS);
			Element raiz = doc.getRootElement();
			Element providers = raiz.getChild(OAI_PROVIDERS);
			List provider = providers.getChildren(OAI_PROVIDER);
			Iterator i = provider.iterator();
			while (i.hasNext()) {
				Element l = (Element) i.next();
				Element e = l.getChild(SHORTNAME);
				if (e.getText().equals(shortName)) {
					e = l.getChild(tag);
					if (e != null) {
						result = e.getText();
						break;
					}
				}
			}
		} catch (Exception e) {
			log.error(e);
		}
		return result;
	}

	public synchronized String getText2(String shortName, String tag) {
		synchronized (docProvidersStatus) {
			String result = null;
			try {
				Element raiz = docProvidersStatus.getRootElement();
				Element providers = raiz.getChild(OAI_PROVIDERS);
				List provider = providers.getChildren(OAI_PROVIDER);
				Iterator i = provider.iterator();
				while (i.hasNext()) {
					Element l = (Element) i.next();
					Element e = l.getChild(SHORTNAME);
					if (e.getText().equals(shortName)) {
						e = l.getChild(tag);
						if (e != null) {
							result = e.getText();
							break;
						}
					}
				}
			} catch (Exception e) {
				log.error(e);
			}
			return result;
		}
	}

	/**
	 * @param shortName
	 * @param tag
	 * @param val
	 * @return
	 */
	public synchronized boolean setText(String shortName, String tag, String val) {
		boolean result = false;
		String tagval = null;
		try {
			SAXBuilder builder = new SAXBuilder();
			Document doc = builder.build(FILE_OAI_PROVIDERS);
			Element raiz = doc.getRootElement();
			Element providers = raiz.getChild(OAI_PROVIDERS);
			List provider = providers.getChildren(OAI_PROVIDER);
			Iterator i = provider.iterator();
			while (i.hasNext()) {
				Element l = (Element) i.next();
				Element e = l.getChild(SHORTNAME);
				if (e.getText().equals(shortName)) {
					e = l.getChild(tag);
					e.setText(val);
					XMLOutputter xml = new XMLOutputter();
					FileOutputStream out = new FileOutputStream(
							FILE_OAI_PROVIDERS);
					xml.output(doc, out);
					out.close();
					result = true;
					break;
				}
			}
		} catch (Exception e) {
			log.error(e);
		}
		return result;
	}

	public synchronized boolean setText2(String shortName, String tag,
			String val) {
		synchronized (docProvidersStatus) {
			boolean result = false;
			String tagval = null;
			try {
				Element raiz = docProvidersStatus.getRootElement();
				Element providers = raiz.getChild(OAI_PROVIDERS);
				List provider = providers.getChildren(OAI_PROVIDER);
				Iterator i = provider.iterator();
				while (i.hasNext()) {
					Element l = (Element) i.next();
					Element e = l.getChild(SHORTNAME);
					if (e.getText().equals(shortName)) {
						e = l.getChild(tag);
						e.setText(val);
						XMLOutputter xml = new XMLOutputter();
						FileOutputStream out = new FileOutputStream(
								FILE_OAI_PROVIDERS_STATUS);
						xml.output(docProvidersStatus, out);
						out.close();
						result = true;
						break;
					}
				}
			} catch (Exception e) {
				log.error("Tag not found: " + shortName + "<" + tag + ">" + val);
				// log.error(e);
			}
			return result;
		}
	}

	/* rutinas que leen o escriben directamente del archivo de log en xml */

	public boolean resetLastIdFile() {
		boolean result = false;
		if (shortName != null) {
			result = setText2(shortName, LAST_ID_FILE, INIT_LAST_ID_FILE);
		}
		return result;
	}

	public boolean setLastIdFile(String val) {
		boolean result = false;
		if (shortName != null) {
			result = setText2(shortName, LAST_ID_FILE, val);
		}
		return result;
	}

	public String getLastIdFile() {
		String result = null;
		if (shortName != null) {
			result = getText2(shortName, LAST_ID_FILE);
		}
		return result;
	}

	public String getHarvestCountIncremental() {
		String result = null;
		if (shortName != null) {
			result = getText2(shortName, HARVEST_COUNT_INCREMENTAL);
		}
		return result;
	}

	public boolean resetHarvestCountIncremental() {
		boolean result = false;
		if (shortName != null) {
			result = setText2(shortName, HARVEST_COUNT_INCREMENTAL,
					INIT_HARVEST_COUNT_INCREMENTAL);
		}
		return result;
	}

	// public static String INIT_HARVEST_COUNT_INCREMENTAL = "4";
	// public static String HARVEST_COUNT_INCREMENTAL =
	// "harvest_count_incremental";

	public boolean incLastIdFile() {
		boolean result = false;
		if (shortName != null) {
			String count = getText2(shortName, LAST_ID_FILE);
			result = setText2(shortName, LAST_ID_FILE,
					Integer.toString(Integer.parseInt(count) + 1));
		}
		return result;
	}

	public boolean resetHarvestCount() {
		boolean result = false;
		if (shortName != null) {
			result = setText2(shortName, HARVEST_COUNT, INIT_HARVEST_COUNT);
		}
		return result;
	}

	public boolean setHarvestCount(String val) {
		boolean result = false;
		if (shortName != null) {
			result = setText2(shortName, HARVEST_COUNT, val);
		}
		return result;
	}

	public String getHarvestCount() {
		String result = null;
		if (shortName != null) {
			result = getText2(shortName, HARVEST_COUNT);
		}
		return result;
	}

	public boolean incHarvestCount() {
		
		boolean result = false;
		if (shortName != null) {
			String count = getText2(shortName, HARVEST_COUNT);
			System.out.println("incHarvestCount "+count+1);
			result = setText2(shortName, HARVEST_COUNT,
					Integer.toString(Integer.parseInt(count) + 1));
		}
		return result;
	}

	public boolean resetLastHarvestDate() {
		boolean result = false;
		if (shortName != null) {
			result = setText2(shortName, LAST_HARVEST_DATE,
					INIT_LAST_HARVEST_DATE);
		}
		return result;
	}

	public boolean setLastHarvestDate(String val) {
		boolean result = false;
		if (shortName != null) {
			result = setText2(shortName, LAST_HARVEST_DATE, val);
		}
		return result;
	}

	public String getLastHarvestDate() {
		String result = null;
		if (shortName != null) {
			result = getText2(shortName, LAST_HARVEST_DATE);
		}
		return result;
	}

	public boolean updateLastHarvestDate() {
		boolean result = false;
		if (shortName != null) {
			Date now = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
			result = setText2(shortName, LAST_HARVEST_DATE, sdf.format(now));
		}
		return result;
	}

	public boolean resetLastResumptionToken() {
		boolean result = false;
		if (shortName != null) {
			result = setText2(shortName, LAST_RESUMPTION_TOKEN,
					INIT_LAST_RESUMPTION_TOKEN);
		}
		return result;
	}

	public boolean setLastResumptionToken(String val) {
		boolean result = false;
		if (shortName != null) {
			result = setText2(shortName, LAST_RESUMPTION_TOKEN, val);
		}
		return result;
	}

	public String getLastResumptionToken() {
		String result = null;
		if (shortName != null) {
			result = getText2(shortName, LAST_RESUMPTION_TOKEN);
		}
		return result;
	}

	public boolean resetHarvestStatus() {
		boolean result = false;
		if (shortName != null) {
			result = setText2(shortName, HARVEST_STATUS, INIT_HARVEST_STATUS);
		}
		return result;
	}

	public boolean setHarvestStatus(String val) {
		boolean result = false;
		if (shortName != null) {
			result = setText2(shortName, HARVEST_STATUS, val);
		}
		return result;
	}

	public String getHarvestStatus() {
		String result = null;
		if (shortName != null) {
			result = getText2(shortName, HARVEST_STATUS);
		}
		return result;
	}

	/**
	 * Setea todos lo valores del XML de los dataproviders.
	 */
	public synchronized void resetAllProviders() {
		try {
			SAXBuilder builder = new SAXBuilder();
			Document doc = builder.build(FILE_OAI_PROVIDERS_STATUS);
			Element raiz = doc.getRootElement();
			Element providers = raiz.getChild(OAI_PROVIDERS);
			List provider = providers.getChildren(OAI_PROVIDER);
			Iterator i = provider.iterator();
			while (i.hasNext()) {
				Element l = (Element) i.next();

				Element e = l.getChild(HARVEST_COUNT);
				if (e == null) {
					e = new Element(HARVEST_COUNT);
					l.addContent(e);
				}
				e.setText(INIT_HARVEST_COUNT);

				e = l.getChild(LAST_HARVEST_DATE);
				if (e == null) {
					e = new Element(LAST_HARVEST_DATE);
					l.addContent(e);
				}
				e.setText(INIT_LAST_HARVEST_DATE);

				e = l.getChild(HARVEST_STATUS);
				if (e == null) {
					e = new Element(HARVEST_STATUS);
					l.addContent(e);
				}
				e.setText(INIT_HARVEST_STATUS);

				e = l.getChild(LAST_RESUMPTION_TOKEN);
				if (e == null) {
					e = new Element(LAST_RESUMPTION_TOKEN);
					l.addContent(e);
				}
				e.setText(INIT_LAST_RESUMPTION_TOKEN);

				e = l.getChild(LAST_ID_FILE);
				if (e == null) {
					e = new Element(LAST_ID_FILE);
					l.addContent(e);
				}
				e.setText(INIT_LAST_ID_FILE);

				e = l.getChild(HARVEST_COUNT_INCREMENTAL);
				if (e == null) {
					e = new Element(HARVEST_COUNT_INCREMENTAL);
					l.addContent(e);
				}
				e.setText(INIT_HARVEST_COUNT_INCREMENTAL);

				e = l.getChild(HARVEST_FRECUENCY);
				if (e == null) {
					e = new Element(HARVEST_FRECUENCY);
					l.addContent(e);
				}
				e.setText(INIT_HARVEST_FRECUENCY);

				e = l.getChild(SCHEDULE_STATUS);
				if (e == null) {
					e = new Element(SCHEDULE_STATUS);
					l.addContent(e);
				}
				e.setText(READY);

				e = l.getChild(MESSAGE);
				if (e == null) {
					e = new Element(MESSAGE);
					l.addContent(e);
				}
				e.setText(BLANK);

			}

			XMLOutputter xml = new XMLOutputter(Format.getPrettyFormat());
			FileOutputStream out = new FileOutputStream(
					FILE_OAI_PROVIDERS_STATUS);
			xml.output(doc, out);
			out.close();
		} catch (Exception e) {
			log.error(e);
		}
	}

	/*
	 * Funcion que nos devuelve el valor del tag <active> del
	 * oai_providers_status.xml - luis
	 */
	public String getActiveStatus(String idrep) {
		String result = "1";
		if (idrep != null) {
			result = getText2(idrep, ACTIVE);
			// System.out.println(result);
		}
		return result;
	}

	/*
	 * Funcion que cambia el valor del tag <active> del oai_providers_status.xml
	 * - luis
	 */
	public boolean setActiveStatus(String idrep, String val) {
		boolean result = false;
		if (idrep != null && val != null) {
			result = setText2(idrep, ACTIVE, val);
		}
		return result;
	}

	/*
	 * Funcion que nos devuelve el valor del tag <owner> del oai_providers.xml -
	 * luis
	 */
	public String getOwner(String idrep) {
		String result = "admin";
		if (idrep != null) {
			result = getText(idrep, OWNER);
		}
		return result;
	}

	/*
	 * Funcion que cambia el valor del tag <owner> del oai_providers.xml - luis
	 */

	public boolean setOwner(String idrep, String val) {
		boolean result = false;
		if (idrep != null && val != null) {
			result = setText(idrep, OWNER, val);
		}
		return result;
	}

	public synchronized boolean isProvider(String fileproviders, String idRepName) {
		boolean result = false;
		try {
			SAXBuilder builder = new SAXBuilder();
			Document doc = builder.build(fileproviders);
			Element raiz = doc.getRootElement();
			Element providers = raiz.getChild(OAI_PROVIDERS);
			List provider = providers.getChildren(OAI_PROVIDER);
			Iterator i = provider.iterator();
			while (i.hasNext()) {
				Element l = (Element) i.next();
				Element e = l.getChild(SHORTNAME);
				if (e != null && e.getText().equals(idRepName)) {
					result = true;
				}
			}
		} catch (Exception e) {
			log.error(e);
		}
		return result;
	}
	
	public synchronized boolean isUrlbase(String fileproviders, String idUrlBase) {
//		System.out.println("Entro a DataProvider.isUrlbase");

		boolean result = false;
		try {
			SAXBuilder builder = new SAXBuilder();
			Document doc = builder.build(fileproviders);
			Element raiz = doc.getRootElement();
			Element providers = raiz.getChild(OAI_PROVIDERS);
			List provider = providers.getChildren(OAI_PROVIDER);
			Iterator i = provider.iterator();
			while (i.hasNext()) {
				Element l = (Element) i.next();
				Element e = l.getChild(BASE_URL);
				if (e != null && e.getText().equals(idUrlBase)) {
					result = true;
				}
			}
		} catch (Exception e) {
			log.error(e);
			System.out.println(e);
		}
		return result;
	}

	private boolean createProviderStatus(String shortName, String owner) {
		boolean result = false;
		Document doc;
		try {
			doc = openXmlProvider(FILE_OAI_PROVIDERS_STATUS);
			Element raiz = doc.getRootElement();
			Element providers = raiz.getChild(OAI_PROVIDERS);
			Element p = new Element(OAI_PROVIDER);
			providers.addContent(p);

			Element e = new Element(SHORTNAME);
			e.setText(shortName);
			p.addContent(e);

			e = new Element(HARVEST_COUNT);
			e.setText(INIT_HARVEST_COUNT);
			p.addContent(e);

			e = new Element(LAST_HARVEST_DATE);
			e.setText(INIT_LAST_HARVEST_DATE);
			p.addContent(e);

			e = new Element(LASTDATE_HARVESTER_OK);
			e.setText(INIT_LASTDATE_HARVESTER_OK);
			p.addContent(e);

			e = new Element(LASTRECS_HARVESTER_OK);
			e.setText(INIT_LASTRECS_HARVESTER_OK);
			p.addContent(e);

			e = new Element(HARVEST_STATUS);
			e.setText(INIT_HARVEST_STATUS);
			p.addContent(e);

			e = new Element(LAST_RESUMPTION_TOKEN);
			e.setText(INIT_LAST_RESUMPTION_TOKEN);
			p.addContent(e);

			e = new Element(LAST_ID_FILE);
			e.setText(INIT_LAST_ID_FILE);
			p.addContent(e);

			e = new Element(HARVEST_COUNT_INCREMENTAL);
			e.setText(INIT_HARVEST_COUNT_INCREMENTAL);
			p.addContent(e);

			e = new Element(HARVEST_FRECUENCY);
			e.setText(INIT_HARVEST_FRECUENCY);
			p.addContent(e);

			e = new Element(SCHEDULE_STATUS);
			e.setText(READY);
			p.addContent(e);

			e = new Element(MESSAGE);
			e.setText(BLANK);
			p.addContent(e);

			e = new Element(ACTIVE);
			e.setText(owner.equals("admin") ? "0" : INIT_ACTIVE); // si es el
																	// admin el
																	// que crea
																	// el
																	// repositorio
																	// de una
																	// queda
																	// listo
																	// para
																	// recolectar
			p.addContent(e);

			writeXmlProvider(doc, FILE_OAI_PROVIDERS_STATUS);
			result = true;
		} catch (Exception e) {
			log.error(e);
		}
		return result;

	}

	public void resetAllSchedulerStatus() {
		try {

			Document doc = null;
			doc = openXmlProvider(FILE_OAI_PROVIDERS_STATUS);

			Element raiz = doc.getRootElement();
			Element providers = raiz.getChild(OAI_PROVIDERS);
			List provider = providers.getChildren(OAI_PROVIDER);
			Iterator i = provider.iterator();

			while (i.hasNext()) {
				Element l = (Element) i.next();
				Element e = l.getChild(SCHEDULE_STATUS);
				e.setText(READY);

			}
			writeXmlProvider(doc, FILE_OAI_PROVIDERS_STATUS);
		} catch (Exception e) {
			log.error(e);
		}
	}

	/**
	 * Set de todos los valores de un provider para hacer luego get Luis
	 * Edimerchk Laverde 13/12/10
	 */

	public void setValues(String shortName) {

		try {
			init();
			SAXBuilder builder = new SAXBuilder();
			Document doc = builder.build(FILE_OAI_PROVIDERS);
			Element raiz = doc.getRootElement();
			Element providers = raiz.getChild(OAI_PROVIDERS);
			List provider = providers.getChildren(OAI_PROVIDER);
			Iterator i = provider.iterator();
			while (i.hasNext()) {
				Element l = (Element) i.next();
				Element e = l.getChild(SHORTNAME);

				if (e != null && e.getText().equals(shortName)) {
					{

						// System.out.println("lo encontro...:");

						e = l.getChild("repositoryName");
						String repositoryName = e.getText();
						setRepositoryName(repositoryName);

						e = l.getChild("description");
						String description = e.getText();
						setDescription(description);

						e = l.getChild("urlhome");
						String urlhome = e.getText();
						setUrlhome(urlhome);

						e = l.getChild("protocol");
						String protocol = e.getText();
						setProtocol(protocol);

						e = l.getChild("baseURL");
						String baseURL = e.getText();
						setBaseURL(baseURL);

						e = l.getChild("nameAdmin");
						String nameAdmin = e.getText();
						setNameAdmin(nameAdmin);

						e = l.getChild("emailadmin");
						String emailadmin = e.getText();
						setEmailadmin(emailadmin);

						e = l.getChild("phoneAdmin");
						String phoneAdmin = e.getText();
						setPhoneAdmin(phoneAdmin);

						e = l.getChild("reptype");
						String reptype = e.getText();
						setReptype(reptype);

						e = l.getChild("collection");
						String collection = e.getText();
						setCollection(collection);

						e = l.getChild("metadataPrefix");
						String metadataPrefix = e.getText();
						setMetadataPrefix(metadataPrefix);

						e = l.getChild("institution");
						String institution = e.getText();
						setInstitution(institution);

						e = l.getChild("shortInst");
						String shortInst = e.getText();
						setShortInst(shortInst);

						e = l.getChild("staticCollection");
						String staticCollection = e.getText();
						setStaticCollection(staticCollection);

						e = l.getChild("departamento");
						String departamento = e.getText();
						setDepartamento(departamento);

						e = l.getChild("municipio");
						String municipio = e.getText();
						setMunicipio(municipio);

						e = l.getChild("bdcol");
						String bdcol = e.getText();
						setBdcol(bdcol);

						e = l.getChild("owner");
						String owner = e.getText();
						setBdcol(owner);

					}
				}
			}

		} catch (Exception e) {
			log.error(e);
		}
	}

	/**
	 * Setea todos los valores del XML de un dataprovider.
	 */
	public boolean resetProviderStatus(String shortName) {
		boolean result = false;
		try {
			SAXBuilder builder = new SAXBuilder();
			Document doc = builder.build(FILE_OAI_PROVIDERS_STATUS);
			Element raiz = doc.getRootElement();
			Element providers = raiz.getChild(OAI_PROVIDERS);
			List provider = providers.getChildren(OAI_PROVIDER);
			Iterator i = provider.iterator();
			while (i.hasNext()) {
				Element l = (Element) i.next();
				Element e = l.getChild(SHORTNAME);

				if (e != null && e.getText().equals(shortName)) {
					{

						e = l.getChild(HARVEST_COUNT);
						if (e == null) {
							e = new Element(HARVEST_COUNT);
							l.addContent(e);
						}
						e.setText(INIT_HARVEST_COUNT);

						e = l.getChild(LAST_HARVEST_DATE);
						if (e == null) {
							e = new Element(LAST_HARVEST_DATE);
							l.addContent(e);
						}
						e.setText(INIT_LAST_HARVEST_DATE);

						e = l.getChild(HARVEST_STATUS);
						if (e == null) {
							e = new Element(HARVEST_STATUS);
							l.addContent(e);
						}
						e.setText(INIT_HARVEST_STATUS);

						e = l.getChild(LAST_RESUMPTION_TOKEN);
						if (e == null) {
							e = new Element(LAST_RESUMPTION_TOKEN);
							l.addContent(e);
						}
						e.setText(INIT_LAST_RESUMPTION_TOKEN);

						e = l.getChild(LAST_ID_FILE);
						if (e == null) {
							e = new Element(LAST_ID_FILE);
							l.addContent(e);
						}
						e.setText(INIT_LAST_ID_FILE);

						e = l.getChild(HARVEST_COUNT_INCREMENTAL);
						if (e == null) {
							e = new Element(HARVEST_COUNT_INCREMENTAL);
							l.addContent(e);
						}
						e.setText(INIT_HARVEST_COUNT_INCREMENTAL);

						e = l.getChild(HARVEST_FRECUENCY);
						if (e == null) {
							e = new Element(HARVEST_FRECUENCY);
							l.addContent(e);
						}
						e.setText(INIT_HARVEST_FRECUENCY);

						e = l.getChild(SCHEDULE_STATUS);
						if (e == null) {
							e = new Element(SCHEDULE_STATUS);
							l.addContent(e);
						}
						e.setText(READY);

						e = l.getChild(MESSAGE);
						if (e == null) {
							e = new Element(MESSAGE);
							l.addContent(e);
						}
						e.setText(BLANK);
						// mgt.deleteRep(shortName); // debe borrar tambien el
						// repositorio de eXist.

						XMLOutputter xml = new XMLOutputter(
								Format.getPrettyFormat());
						FileOutputStream out = new FileOutputStream(
								FILE_OAI_PROVIDERS_STATUS);
						xml.output(doc, out);
						out.close();
						result = true;
						break;
					}
				}
			}
		} catch (Exception e) {
			log.error(e);
		}
		return result;
	}

	public String queryOaiProvider(String url) {
		StringBuffer sb = new StringBuffer();
		String salida = "";
		try {
			URL u = new URL(url + "?verb=Identify");
			URLConnection uc = u.openConnection();
			BufferedReader in = new BufferedReader(new InputStreamReader(
					uc.getInputStream()));
			String tmp = null;
			while ((tmp = in.readLine()) != null) {
				sb.append(tmp);
			}
			salida = sb.toString();
		} catch (Exception e) {
			log.error(e);
		}
		return sb.toString();
	}

	public void queryXMLProvider(String url) {
		try {
			SAXBuilder builder = new SAXBuilder();
			URL u = new URL(url + "?verb=ListMetadataFormats");
			Document doc = builder.build(u);
			Element raiz = doc.getRootElement();
			// Element iden = raiz.getChildren("Identify");
			List items = raiz.getChildren();
			Iterator i = items.iterator();
			while (i.hasNext()) {
				Element l = (Element) i.next();
				if (l.getName() == "ListMetadataFormats") {
					List l2 = l.getChildren();
					Iterator i2 = l2.iterator();
					while (i2.hasNext()) {
						Element l3 = (Element) i2.next();
					}
				}
			}
		} catch (Exception e) {
			log.error(e);
		}
	}

	public String Identify(String url) {
		Document doc = null;
		StringBuffer sb = new StringBuffer();
		try {
			SAXBuilder builder = new SAXBuilder();
			URL u = new URL(url + "?verb=Identify");
			// System.out.println("URL-Identify="+url+"?verb=Identify");
			doc = builder.build(u);
			// System.out.println("Salio...");
			Element raiz = doc.getRootElement();
			// System.out.println(raiz.getName()+"="+raiz.getText());
			// Element iden = raiz.getChildren("Identify");
			// System.out.println(iden.getName()+"="+iden.getText());
			List items = raiz.getChildren();
			Iterator i = items.iterator();
			while (i.hasNext()) {
				Element l = (Element) i.next();
				if (l.getName() == "Identify") {
					sb.append("<Identify>\n");
					List l2 = l.getChildren();
					Iterator i2 = l2.iterator();
					while (i2.hasNext()) {
						Element l3 = (Element) i2.next();
						// System.out.println("\t>"+l3.getName()+"="+l3.getText());
						sb.append("<" + l3.getName() + ">" + l3.getText()
								+ "</" + l3.getName() + ">\n");
					}
					sb.append("</Identify>\n");
				} // else
					// System.out.println(">"+l.getName()+"="+l.getText());
			}
		} catch (Exception e) {
			log.error(e);
		}
		String result = sb.toString();
		return result;
	}

	public String ListMetadataFormats(String url) {
		Document doc = null;
		StringBuffer sb = new StringBuffer();
		try {
			SAXBuilder builder = new SAXBuilder();
			URL u = new URL(url + "?verb=ListMetadataFormats");
			doc = builder.build(u);
			Element raiz = doc.getRootElement();
			// System.out.println(raiz.getName()+"="+raiz.getText());
			// Element iden = raiz.getChildren("Identify");
			// System.out.println(iden.getName()+"="+iden.getText());
			List items = raiz.getChildren();
			Iterator i = items.iterator();
			while (i.hasNext()) {
				Element l = (Element) i.next();
				if (l.getName() == "ListMetadataFormats") {
					sb.append("<ListMetadataFormats>\n");
					List l2 = l.getChildren();
					Iterator i2 = l2.iterator();
					while (i2.hasNext()) {
						Element l3 = (Element) i2.next();
						if (l3.getName().equals("metadataFormat"))
							sb.append("<" + l3.getName() + ">");
						List l4 = l3.getChildren();
						Iterator i3 = l4.iterator();
						while (i3.hasNext()) {
							Element e2 = (Element) i3.next();
							if (e2.getName().equals("metadataPrefix")) {
								sb.append("<" + e2.getName() + ">"
										+ e2.getText() + "</" + e2.getName()
										+ ">\n");
								sb.append("</" + l3.getName() + ">\n");
							}
						}
					}
					sb.append("</ListMetadataFormats>\n");
				} // else
					// System.out.println(">"+l.getName()+"="+l.getText());
			}
		} catch (Exception e) {
			log.error(e);
		}
		String result = sb.toString();
		System.out.println(result);
		return result;
	}

	public boolean testRTwithMP(String url) {
		boolean result = false;
		return result;
	}

	private Document openXmlProvider(String filename) throws Exception {
		SAXBuilder builder = new SAXBuilder();
		Document doc = builder.build(filename);
		return doc;
	}

	private void writeXmlProvider(Document doc, String filename)
			throws Exception {
		XMLOutputter xml = new XMLOutputter(Format.getPrettyFormat());
		FileOutputStream out = new FileOutputStream(filename);
		xml.output(doc, out);
		out.close();
	}

	public boolean create(String owner, String repName, String urlBase,
			String shortName, String descripcion, String metadataPrefix,
			String prefijo, String collection, String urlhome, String protocol,
			String emailadmin, String nameAdmin, String phoneAdmin,
			String reptype, String cdata, String institution, String shortInst,
			String staticCollection, String Departamento, String Municipio,
			String bdcol) {
		boolean result = false;

		boolean test = isProvider(FILE_OAI_PROVIDERS, shortName);
		if (test == false) {
			result = createProvider(owner, repName, urlBase, shortName,
					descripcion, metadataPrefix, prefijo, collection, urlhome,
					protocol, emailadmin, nameAdmin, phoneAdmin, reptype,
					cdata, institution, shortInst, staticCollection,
					Departamento, Municipio, bdcol);
			result = createProviderStatus(shortName, owner);
		} else
			result = updateProvider(owner, repName, urlBase, shortName,
					descripcion, metadataPrefix, prefijo, collection, urlhome,
					protocol, emailadmin, nameAdmin, phoneAdmin, reptype,
					institution, shortInst, staticCollection, Departamento,
					Municipio, bdcol);
		result = resetProviderStatus(shortName);

		return result;
	}

	private boolean updateProvider(String owner, String repName,
			String urlBase, String shortName, String descripcion,
			String metadataPrefix, String prefijo, String collection,
			String urlhome, String protocol, String emailadmin,
			String nameAdmin, String phoneAdmin, String reptype,
			String institution, String shortInst, String staticCollection,
			String Departamento, String Municipio, String bdcol) {
		boolean result = false;

		Document doc;
		try {
			doc = openXmlProvider(FILE_OAI_PROVIDERS);
			Element raiz = doc.getRootElement();
			Element providers = raiz.getChild(OAI_PROVIDERS);
			List list = providers.getChildren(OAI_PROVIDER);
			Iterator i = list.iterator();
			while (i.hasNext()) {
				Element l = (Element) i.next();
				Element e = l.getChild(SHORTNAME);

				if (e != null && e.getText().equals(shortName)) {

					e = l.getChild(REPOSITORY_NAME);
					if (e == null) {
						e = new Element(REPOSITORY_NAME);
						l.addContent(e);
					}
					e.setText(repName);

					e = l.getChild(BASE_URL);
					if (e == null) {
						e = new Element(BASE_URL);
						l.addContent(e);
					}
					e.setText(urlBase);

					e = l.getChild(DESCRIPTION);
					if (e == null) {
						e = new Element(DESCRIPTION);
						l.addContent(e);
					}
					e.setText(descripcion);

					e = l.getChild(METADATA_PREFIX);
					if (e == null) {
						e = new Element(METADATA_PREFIX);
						l.addContent(e);
					}
					e.setText(metadataPrefix);
					/*
					 * e = (Element) l.getChild(CDATA); if (e == null) { e = new
					 * Element(CDATA); l.addContent(e); } e.setText(cdata);
					 * //aqui es donde estaba el bug del cdata
					 */
					// e = (Element) l.getChild(RESUMPTION);
					// if (e == null) {
					// e = new Element(RESUMPTION);
					// l.addContent(e);
					// }
					// e.setText(prefijo);
					// e.setText("0");

					e = l.getChild(COLLECTION);
					if (e == null) {
						e = new Element(COLLECTION);
						l.addContent(e);
					}
					e.setText(collection);

					e = l.getChild(URLHOME);
					if (e == null) {
						e = new Element(URLHOME);
						l.addContent(e);
					}
					e.setText(urlhome);

					e = l.getChild(PROTOCOL);
					if (e == null) {
						e = new Element(PROTOCOL);

						l.addContent(e);
					}
					e.setText(protocol);

					e = l.getChild(NAMEADMIN);
					if (e == null) {
						e = new Element(NAMEADMIN);
						l.addContent(e);
					}
					e.setText(nameAdmin);

					e = l.getChild(EMAILADMIN);
					if (e == null) {
						e = new Element(EMAILADMIN);
						l.addContent(e);
					}
					e.setText(emailadmin);

					e = l.getChild(PHONEADMIN);
					if (e == null) {
						e = new Element(PHONEADMIN);
						l.addContent(e);
					}
					e.setText(phoneAdmin);

					e = l.getChild(REPTYPE);
					if (e == null) {
						e = new Element(REPTYPE);
						l.addContent(e);
					}
					e.setText(reptype);

					e = l.getChild(INSTITUTION);
					if (e == null) {
						e = new Element(INSTITUTION);
						l.addContent(e);
					}
					e.setText(institution);

					e = l.getChild(SHORTINST);
					if (e == null) {
						e = new Element(SHORTINST);
						l.addContent(e);
					}
					e.setText(shortInst);

					e = l.getChild(STATICCOLLECTION);
					if (e == null) {
						e = new Element(STATICCOLLECTION);
						l.addContent(e);
					}
					e.setText(staticCollection);

					e = l.getChild(DEPARTAMENTO);
					if (e == null) {
						e = new Element(DEPARTAMENTO);
						l.addContent(e);
					}
					e.setText(Departamento);

					e = l.getChild(MUNICIPIO);
					if (e == null) {
						e = new Element(MUNICIPIO);
						l.addContent(e);
					}
					e.setText(Municipio);

					e = l.getChild(BDCOL);
					if (e == null) {
						e = new Element(BDCOL);
						l.addContent(e);
					}
					e.setText(bdcol);

					e = l.getChild(OWNER);
					if (e == null) {
						e = new Element(OWNER);
						l.addContent(e);
					}
					e.setText(owner);

					writeXmlProvider(doc, FILE_OAI_PROVIDERS);
					result = true;
				}
			}
		} catch (Exception e) {
			log.error(e);
		}

		return result;
	}

	public boolean delete(String shortName) {

		boolean result = false;

		Document doc;
		try {
			doc = openXmlProvider(FILE_OAI_PROVIDERS);
			Element raiz = doc.getRootElement();
			Element providers = raiz.getChild(OAI_PROVIDERS);
			List list = providers.getChildren(OAI_PROVIDER);
			Iterator i = list.iterator();
			while (i.hasNext()) {
				Element l = (Element) i.next();
				Element e = l.getChild(SHORTNAME);
				if (e != null && e.getText().equals(shortName)) {
					result = providers.removeContent(l);
					writeXmlProvider(doc, FILE_OAI_PROVIDERS);
					break;
				}
			}
		} catch (Exception e) {
			log.error(e);
		}

		try {
			doc = openXmlProvider(FILE_OAI_PROVIDERS_STATUS);
			Element raiz = doc.getRootElement();
			Element providers = raiz.getChild(OAI_PROVIDERS);
			List list = providers.getChildren(OAI_PROVIDER);
			Iterator i = list.iterator();
			while (i.hasNext()) {
				Element l = (Element) i.next();
				Element e = l.getChild(SHORTNAME);
				if (e != null && e.getText().equals(shortName)) {
					result = providers.removeContent(l);
					writeXmlProvider(doc, FILE_OAI_PROVIDERS_STATUS);
					break;
				}
			}
		} catch (Exception e) {
			log.error(e);
		}

		return result;
	}

	private boolean createProvider(String owner, String repName,
			String urlBase, String shortName, String descripcion,
			String metadataPrefix, String prefijo, String collection,
			String urlhome, String protocol, String emailadmin,
			String nameAdmin, String phoneAdmin, String reptype, String cdata,
			String institution, String shortInst, String staticCollection,
			String Departamento, String Municipio, String bdcol) {
		boolean result = false;

		Document doc;
		try {
			doc = openXmlProvider(FILE_OAI_PROVIDERS);
			Element raiz = doc.getRootElement();
			Element providers = raiz.getChild(OAI_PROVIDERS);
			Element p = new Element(OAI_PROVIDER);
			providers.addContent(p);

			Element e = new Element(REPOSITORY_NAME);
			e.setText(repName);
			p.addContent(e);

			e = new Element(BASE_URL);
			e.setText(urlBase);
			p.addContent(e);

			e = new Element(SHORTNAME);
			e.setText(shortName);
			p.addContent(e);

			e = new Element(DESCRIPTION);
			e.setText(descripcion);
			p.addContent(e);

			e = new Element(METADATA_PREFIX);
			e.setText(metadataPrefix);
			p.addContent(e);

			e = new Element(RESUMPTION);
			// e.setText(prefijo);
			e.setText("0");
			p.addContent(e);

			e = new Element(COLLECTION);
			e.setText(collection);
			p.addContent(e);

			e = new Element(URLHOME);
			e.setText(urlhome);
			p.addContent(e);

			e = new Element(PROTOCOL);
			e.setText(protocol);
			p.addContent(e);

			e = new Element(NAMEADMIN);
			e.setText(nameAdmin);
			p.addContent(e);

			e = new Element(EMAILADMIN);
			e.setText(emailadmin);
			p.addContent(e);

			e = new Element(PHONEADMIN);
			e.setText(phoneAdmin);
			p.addContent(e);

			e = new Element(REPTYPE);
			e.setText(reptype);
			p.addContent(e);

			e = new Element(CDATA);
			e.setText(cdata);
			p.addContent(e);

			e = new Element(INSTITUTION);
			e.setText(institution);
			p.addContent(e);

			e = new Element(SHORTINST);
			e.setText(shortInst);
			p.addContent(e);

			e = new Element(STATICCOLLECTION);
			e.setText(staticCollection);
			p.addContent(e);

			e = new Element(DEPARTAMENTO);
			e.setText(Departamento);
			p.addContent(e);

			e = new Element(MUNICIPIO);
			e.setText(Municipio);
			p.addContent(e);

			e = new Element(BDCOL);
			e.setText(bdcol);
			p.addContent(e);

			e = new Element(OWNER);
			e.setText(owner);
			p.addContent(e);

			writeXmlProvider(doc, FILE_OAI_PROVIDERS);
			result = true;

		} catch (Exception e) {
			log.error(e);
		}

		return result;

	}

	public String getXmlProviderStatus() {
		String xml = null;
		try {
			File f = new File(FILE_OAI_PROVIDERS_STATUS);
			int ch;
			StringBuffer strContent = new StringBuffer("");
			FileInputStream fin = null;

			fin = new FileInputStream(f);
			while ((ch = fin.read()) != -1) {
				strContent.append((char) ch);
			}
			fin.close();
			xml = strContent.toString();
			xml = xml.replace("<" + OAI_PROVIDERS + ">", "");
			xml = xml.replace("</" + OAI_PROVIDERS + ">", "");
		} catch (Exception e) {
			log.error(e);
		}
		return xml;
	}

	public String getXmlProvider() {
		String xml = null;
		try {
			File f = new File(FILE_OAI_PROVIDERS);
			int ch;
			StringBuffer strContent = new StringBuffer("");
			FileInputStream fin = null;

			fin = new FileInputStream(f);

			while ((ch = fin.read()) != -1) {
				strContent.append((char) ch);

			}
			fin.close();
			xml = strContent.toString();
			xml = xml.replace("<" + OAI_PROVIDERS + ">", "");
			xml = xml.replace("</" + OAI_PROVIDERS + ">", "");

		} catch (Exception e) {
			log.error(e);
		}
		return xml;
	}

	public int getContFiles() {
		return contFiles;
	}

	public void incContFiles() {
		this.contFiles = contFiles + 1;
	}

	public int getHarvesterTime() {
		return harvesterTime;
	}

	public void setHarvesterTime(int harvesterTime) {
		this.harvesterTime = harvesterTime;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		DataProvider dp = new DataProvider("/Applications/bdcol");
		String xml = dp
				.Identify("http://revistas.unal.edu.co/index.php/index/oai");
		System.out.println("OAI-identify\n" + xml);
		// dp.setRuta("/Applications/bdcol");
		// dp.create("eafit", "urlBase", "PEPE", "descripcion",
		// "metadataPrefix", "cualquier cosa", "collection", "urlhome",
		// "protocol", "emailadmin", "reptype");
		// dp.delete("PEPE");
		// dp.setShortName("upc");
		// dp.reset();
		// dp.resetAllProviders();
		// String salida = dp.Identify(args[0]);
		// System.out.print("XML de Identify=\n"+salida);
		// salida = dp.ListMetadataFormats(args[0]);
		// System.out.print("XML de ListMF=\n"+salida);
		// System.out.println(xml);
		// dp.setShortName("upc");
		// String st = dp.getHarvestCountIncremental();
		// System.out.println("...DataProvider.main(1) " + st);
		// System.out.println("..DataProvider.main(2) " + dp.getLastIdFile());
		//
		// String stfecha = dp.getLastHarvestDate();
		// System.out.println("DataProvider.main(1) " + stfecha);
		// SimpleDateFormat sp = new SimpleDateFormat("yyyy-MM-dd");
		// try {
		// Date date = sp.parse(stfecha);
		// System.out.println("DataProvider.main(2) " + sp.format(date));
		//
		// ManejoFechas manejoFechas = new ManejoFechas();
		// Date javaDate = manejoFechas.sumarDia(date, 1);
		// System.out.println("DataProvider.main(3) " + sp.format(javaDate));
		// int i = manejoFechas.compararfechas(javaDate, date);
		// System.out.println("el resultado de la compararci_n es " + i);
		//
		// // si fecha 1 es igual a fecha 2, retorna 0 si fecha 1 es mayor que
		// // fecha 2, retorna mayor a 0 si fecha 1 es menor que fecha 2,
		// // retorna menor a 0
		//
		// } catch (ParseException e) {
		// log.error(e);
		// }

		// dp.reset();
		// dp.reset("upc");
		// st = dp.getHarvestCountIncremental();
		// System.out.println("DataProvider.main(1) " + st);
		// dp.resetHarvestCountIncremental();
		// st = dp.getHarvestCountIncremental();
		// System.out.println("DataProvider.main(2) " + st);
	}

	/*
	 * public void escribirNuevoResumption(int resultadoTest) throws Exception{
	 * SAXBuilder builder = new SAXBuilder(); Document doc =
	 * builder.build(FILE_OAI_PROVIDERS); Element raiz = doc.getRootElement();
	 * Element providers = (Element) raiz.getChild(OAI_PROVIDERS); List provider
	 * = providers.getChildren(OAI_PROVIDER); Iterator i = provider.iterator();
	 * while (i.hasNext()) { Element l = (Element) i.next(); Element e =
	 * (Element) l.getChild(RESUMPTION); if (e == null) { e = new
	 * Element(RESUMPTION); l.addContent(e); }
	 * e.setText(String.valueOf(resultadoTest)); } XMLOutputter xml = new
	 * XMLOutputter(Format.getPrettyFormat()); FileOutputStream out = new
	 * FileOutputStream(FILE_OAI_PROVIDERS); xml.output(doc, out); out.close();
	 * }
	 */
}
