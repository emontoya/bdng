package org.bdng.harvmgr.harvester;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Properties;

import org.apache.log4j.Logger;

public class PowerUsers {

	Logger log = Logger.getLogger(PowerUsers.class);

	public PowerUsers() {
		leerUrl();

	}

	private void leerUrl() {
		InputStream is = null;
		DataInputStream dis;
		String s = "a";
		byte[] b = new byte[1];

		try {
			Properties systemSettings = System.getProperties();
			systemSettings.put("http.proxyHost", "200.12.176.35"); // wwwproxy
			// alias to
			// http://proxypac/proxy.pac
			systemSettings.put("http.proxyPort", "8080");

			URL u = new URL("http://www.google.com");
			HttpURLConnection url = (HttpURLConnection) u.openConnection();

			// url.setDoInput(true);
			// url.setDoOutput(true);
			// url.setUseCaches(false);
			url.setRequestProperty("Content-Type",
					"application/x-www-form-urlencoded");
			String agent = "Mozilla/4.0";
			url.setRequestProperty("User-Agent", agent);

			// url.getContent();
			dis = new DataInputStream(new BufferedInputStream(
					url.getInputStream()));
			// Read File Line By Line
			while (dis.read(b, 0, 1) != -1) {
				// Print the content on the console
				System.out.println(new String(b));
			}

		} catch (Exception e) {
			log.error(e);
		}

	}

	public static void main(String[] args) {
		new PowerUsers();
	}

}