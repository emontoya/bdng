package org.bdng.harvmgr.indexer;

import java.io.File;

import org.apache.log4j.Logger;
import org.bdng.harvmgr.dlConfig;
import org.bdng.harvmgr.harvester.DataProvider;
import org.xmldb.api.DatabaseManager;
import org.xmldb.api.base.Collection;
import org.xmldb.api.base.Database;
import org.xmldb.api.modules.CollectionManagementService;
import org.xmldb.api.modules.XMLResource;

public class RepositoryExist {

	Logger log = Logger.getLogger(RepositoryExist.class);

	Collection existCollection = null;
	String dl_col = null;
	String dl_inst = null;
	String dl_repname = null;
	String dl_name = null;

	dlConfig dlconfig = null;

	private void init() throws Exception {
		Database database = null;
		Class cl = null;
		cl = Class.forName(dlconfig.getString("exist_driver"));
		database = (Database) cl.newInstance();
		DatabaseManager.registerDatabase(database);
	}

	private void startCollection() throws Exception {
		String col = dlconfig.getExist_uri() + "/db/"
				+ dlconfig.getString("dl_name") + "/" + dl_inst + "/"
				+ dl_repname;
		existCollection = DatabaseManager.getCollection(col,
				dlconfig.getString("exist_user"),
				dlconfig.getString("exist_pass"));
		if (existCollection == null) {
			Collection root = DatabaseManager.getCollection(
					dlconfig.getExist_uri() + "/db",
					dlconfig.getString("exist_user"),
					dlconfig.getString("exist_pass"));
			CollectionManagementService mgtService = (CollectionManagementService) root
					.getService("CollectionManagementService", "1.0");
			existCollection = mgtService.createCollection(dlconfig
					.getString("dl_name") + "/" + dl_inst + "/" + dl_repname);
		}
	}

	public void startCollectionConf() throws Exception {
		String col = dlconfig.getExist_uri() + "/db/conf";
		existCollection = DatabaseManager.getCollection(col,
				dlconfig.getString("exist_user"),
				dlconfig.getString("exist_pass"));
		if (existCollection == null) {
			Collection root = DatabaseManager.getCollection(
					dlconfig.getExist_uri() + "/db",
					dlconfig.getString("exist_user"),
					dlconfig.getString("exist_pass"));
			CollectionManagementService mgtService = (CollectionManagementService) root
					.getService("CollectionManagementService", "1.0");
			existCollection = mgtService.createCollection("conf");
		}
	}

	public RepositoryExist(String strRuta) {
		dl_name = "demo";
		dl_col = "general";
		dl_inst = "test";
		dl_repname = "test";
		dlconfig = new dlConfig(strRuta);
		try {
			init();
		} catch (Exception e) {
			log.error(e);
		}
	}

	public RepositoryExist() {
		dl_name = "demo";
		dl_col = "general";
		dl_inst = "test";
		dl_repname = "test";
		dlconfig = new dlConfig();
		try {
			init();
		} catch (Exception e) {
			log.error(e);
		}
	}

	public void storeConfToExist(String file) throws Exception {

		File f = new File(file);
		if (!f.canRead())
			System.err.println("File can't be readen " + file);
		else {

			XMLResource document = (XMLResource) existCollection
					.createResource(f.getName(), "XMLResource");
			document.setContent(f);
			System.out.println("Indexando " + document.getDocumentId());
			existCollection.storeResource(document);

		}
	}

	public void storeExist(String file) throws Exception {

		File f = new File(file);
		if (!f.canRead())
			System.err.println("File can't be readen " + file);
		else {
			XMLResource res = (XMLResource) existCollection.getResource(f
					.getName());
			XMLResource document = (XMLResource) existCollection
					.createResource(f.getName(), "XMLResource");
			document.setContent(f);
			if (res == null) {
				System.out.println("Indexando archivo "
						+ document.getDocumentId() + " ...");
			} else {
				System.out.println("RE-Indexando archivo "
						+ document.getDocumentId() + " ...");
				existCollection.removeResource(document);

			}
			existCollection.storeResource(document);
		}
	}

	public void uploadFile(String inst, String rep, String xmlfile) {
		if (!dl_inst.equals(inst) || !dl_repname.equals(rep)) {
			dl_inst = inst;
			dl_repname = rep;
			try {
				startCollection();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
		if (xmlfile.endsWith(".xml")) {
			try {
				storeExist(xmlfile);
			} catch (Exception e) {
				System.out.println("ERROR UPLOAD =" + xmlfile);
				// log.error(e);
			}
		} else
			System.out.println("NO ES UN ARCHIVO XML = " + xmlfile);
	}
}
