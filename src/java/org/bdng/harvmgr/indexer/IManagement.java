package org.bdng.harvmgr.indexer;

public interface IManagement {

	public boolean createBidi(String name);

	public boolean deleteBidi(String name);

	public String[] listBidi();

	public boolean createCol(String colname);

	public boolean deleteCol(String colname);

	public String[] listCol();

	public boolean createInst(String inst);

	public boolean deleteInst(String inst);

	public String[] listInst();

	public boolean createRep(String name);

	public boolean deleteRep(String name);

	public String[] listRep();

}
