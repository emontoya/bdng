package org.bdng.harvmgr.indexer;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import org.apache.log4j.Logger;
import org.bdng.harvmgr.dlConfig;

public class Indexer extends Thread {
	
	Logger log = Logger.getLogger(Indexer.class);

	boolean working = false;
	boolean continuar = true;
	String[] repos = null;

	String url_counter_cols = null;
	
	int numfiles=0;
	float percentageIndexed=0;
	String currentFile = null;
	int count = 0;

	@Override
	public void run() {
		working = true;
		if (type.equals("all")) {
			numfiles = countfiles();
			upload2exist();
		}
		else {
			numfiles = countfilesSelected();
			for (int i = 0; i < repos.length; i++)
				upload2exist(repos[i]);
		}
		working = false;
	}

	dlConfig dlconfig = null;

	private void updateConf() {
		URL u = null;
		URLConnection uc = null;
		try {
			u = new URL(dlconfig.getDlUrl()
					+ "/servicios/UpdateConf.xql?username="
					+ dlconfig.getExistUser() + "&password="
					+ dlconfig.getExistPass());
			uc = u.openConnection();
			uc.connect();
			StringBuilder sb = new StringBuilder();
			BufferedReader in = new BufferedReader(new InputStreamReader(
					uc.getInputStream()));
			String tmp = null;
			while ((tmp = in.readLine()) != null) {
				sb.append(tmp);
			}

		} catch (Exception e) {
			// log.error(e);
			System.out.println("Ocurrio un error en update conf:");
			System.out.println(e);
		}

	}

	private void updateCounterCols() {
		URL u = null;
		URLConnection uc = null;
		try {
			u = new URL(url_counter_cols);
			uc = u.openConnection();
			uc.connect();
			StringBuilder sb = new StringBuilder();
			BufferedReader in = new BufferedReader(new InputStreamReader(
					uc.getInputStream()));
			String tmp = null;
			while ((tmp = in.readLine()) != null) {
				sb.append(tmp);
			}

		} catch (Exception e) {
			System.out.println("Ocurrio un error en update counter cols:");
			System.out.println(e);
			log.error(e);
		}

	}

	public boolean isWorking() {
		return working;
	}

	synchronized public void Continuar() {
		continuar = true;
	}

	synchronized public void NoContinuar() {
		continuar = false;
	}

	RepositoryExist exist = null;

	String strRuta = null;

	String type = "all";

	public Indexer(String strRuta) {
		this.strRuta = strRuta;
		type = "all";
		dlconfig = new dlConfig(strRuta);
		url_counter_cols = dlconfig.getDlUrl()
				+ "/servicios/CounterCols.xql?dl_home=/db/" + dlconfig.getDlName()+ "&username=" + dlconfig.getExistUser() + "&password=" + dlconfig.getExistPass();		
	}

	public Indexer(String strRuta, String[] repos) {
		this.strRuta = strRuta;
		type = "selected";
		this.repos = repos;
		dlconfig = new dlConfig(strRuta);
		url_counter_cols = dlconfig.getDlUrl()
				+ "/servicios/CounterCols.xql?dl_home=/db/" + dlconfig.getDlName()+ "&username=" + dlconfig.getExistUser() + "&password=" + dlconfig.getExistPass();
		
	
		
	}

	public Indexer() {
		strRuta = System.getProperty("user.dir") + "/";
		type = "all";
		dlconfig = new dlConfig();
		url_counter_cols = dlconfig.getDlUrl()
				+ "/servicios/CounterCols.xql?dl_home=/db/" + dlconfig.getDlName()+ "&username=" + dlconfig.getExistUser() + "&password=" + dlconfig.getExistPass();
		// dlConfig.loadProps(strRuta+"/conf/OaiServerConfig.xml");
	}

	// metodo que se utiliza para subir archivos al directorio /db/conf en eXist
	// - luis
	public void uploadConf(String file) {

		try {
			/*
			 * dlconfig = new dlConfig(strRuta); File file_providers =new
			 * File(dlconfig.getOaiProviders()); File file_conf =new
			 * File(dlconfig.getfileServerConfig());
			 * 
			 * System.out.println(file_providers.toString());
			 * System.out.println(file_conf.toString());
			 */

			updateConf();

			exist = new RepositoryExist(strRuta);
			exist.startCollectionConf(); // configura el api de exist para que
											// suba al directorio /db/conf
			exist.storeConfToExist(file);

		} catch (Exception e) {

			log.error(e);
		}

	}
	/**
	 * retorna el porcentage de progreso de la indexación
	 * @return porcentage 
	 */
	
	public int getProgress() {
		return (int)(percentageIndexed*100);
	}
	
	public String getCurrentFile() {
		return currentFile;
	}
	
	public void upload2exist() {
		File c, r, f;
		String[] cols, reps, files;
		String col, rep, file;
		String dir = dlconfig.getString("dl_metadata") + "/dbxml";
		c = new File(dir);
		
		if (c != null) {
			cols = c.list();
			exist = null;
			for (int i = 0; cols != null && i < cols.length && continuar; i++) {
				col = dir + "/" + cols[i];
				c = new File(col);
				if (c != null && c.isDirectory()) {
					reps = c.list();
					for (int j = 0; reps != null && j < reps.length && continuar; j++) {
						rep = col + "/" + reps[j];
						r = new File(rep);
						if (r != null && r.isDirectory()) {
							files = r.list();
							for (int k = 0; files != null && k < files.length && continuar; k++) {
								file = rep + "/" + files[k];
								f = new File(file);
								if (f != null && f.isFile() && files[k].endsWith(".xml")) {
									if (exist == null) {
										exist = new RepositoryExist(strRuta);
									}
									exist.uploadFile(cols[i], reps[j], file);
									count++;
									percentageIndexed=(float)count/numfiles;
									currentFile=f.getName();
									System.out.println(getCurrentFile()+" %="+getProgress());
								} else
									System.out.println("Directorio: " + files[k]);
							}
						}
					}
	
				}
			}
	
			updateCounterCols();
		}
	}

	public void upload2exist(String repname) {
		File c, r, f;
		String[] cols, reps, files;
		String col, rep, file;
		String dir = dlconfig.getString("dl_metadata") + "/dbxml";
		c = new File(dir);
		if (c != null) {
			cols = c.list();
	
			exist = null;
			for (int i = 0; cols != null && i < cols.length && continuar; i++) {
				col = dir + "/" + cols[i];
				c = new File(col);
				if (c != null && c.isDirectory()) {
					reps = c.list();
					for (int j = 0; reps != null && j < reps.length && continuar; j++) {
						rep = col + "/" + reps[j];
						r = new File(rep);
						if (r != null && r.isDirectory() && r.getName().equals(repname)) {
							files = r.list();
							for (int k = 0; files != null && k < files.length && continuar; k++) {
								file = rep + "/" + files[k];
								f = new File(file);
								if (f != null && f.isFile() && files[k].endsWith(".xml")) {
									if (exist == null) {
										exist = new RepositoryExist(strRuta);
									}
									exist.uploadFile(cols[i], reps[j], file);
									count++;
									percentageIndexed=(float)count/numfiles;
									currentFile=f.getName();
									System.out.println(getCurrentFile()+" %="+getProgress());
								} else
									System.out.println("Directorio: " + files[k]);
							}
						}
					}
	
				}
			}
	
			updateCounterCols();
		}
	}
	
	private int countfiles() {
		int numfiles=0;
		File c, r, f;
		String[] cols, reps, files;
		String col, rep, file;
		String dir = dlconfig.getString("dl_metadata") + "/dbxml";
		c = new File(dir);
		if (c != null) {
			cols = c.list();
			exist = null;
			for (int i = 0; cols != null && i < cols.length && continuar; i++) {
				col = dir + "/" + cols[i];
				c = new File(col);
				if (c != null && c.isDirectory()) {
					reps = c.list();
					for (int j = 0; reps != null && j < reps.length && continuar; j++) {
						rep = col + "/" + reps[j];
						r = new File(rep);
						if (r != null && r.isDirectory()) {
							files = r.list();
							for (int k = 0; files != null && k < files.length && continuar; k++) {
								file = rep + "/" + files[k];
								f = new File(file);
								if (f != null && f.isFile() && files[k].endsWith(".xml")) {
									numfiles++;
									}
							}
						}
					}
	
				}
			}
		}
		return numfiles;
	}
	
	private boolean isRepoSelected(String rep) {
		boolean result = false;
		for (int i=0;i<repos.length;i++) {
			if (rep.equals(repos[i])) {
				result = true;
				break;
			}
		}
		return result;
	}
	
	private int countfilesSelected() {
		int numfiles=0;
		File c, r, f;
		String[] cols, reps, files;
		String col, rep, file;
		String dir = dlconfig.getString("dl_metadata") + "/dbxml";
		c = new File(dir);
		if (c != null) {
			cols = c.list();
			exist = null;
			for (int i = 0; cols != null && i < cols.length && continuar; i++) {
				col = dir + "/" + cols[i];
				c = new File(col);
				if (c != null && c.isDirectory()) {
					reps = c.list();
					for (int j = 0; reps != null && j < reps.length && continuar; j++) {
						rep = col + "/" + reps[j];
						r = new File(rep);
						if (r != null && r.isDirectory() && isRepoSelected(r.getName())) {
							files = r.list();
							for (int k = 0; files != null && k < files.length && continuar; k++) {
								file = rep + "/" + files[k];
								f = new File(file);
								if (f != null && f.isFile() && files[k].endsWith(".xml")) {
									numfiles++;
									}
							}
						}
					}
	
				}
			}
		}
		return numfiles;
	}

	public static void main(String[] args) {
		String dl_home = null;
		String repname = null;

		if (args.length > 1) {
			dl_home = args[0];
			repname = args[1];
		} else {
			System.out.println("DL_HOME no especificado");
			System.exit(0);
		}
		Indexer indexer = new Indexer(dl_home);
		System.out
				.println("******** Iniciando el proceso de indexacion ********");
		if (repname.equals("all"))
			indexer.upload2exist();
		else
			indexer.upload2exist(repname);

		System.out
				.println("******** El proceso de indexacion finalizo ********");
	}
}
