package org.bdng.harvmgr.indexer;

import org.apache.log4j.Logger;
import org.bdng.harvmgr.dlConfig;
import org.bdng.harvmgr.harvester.DataProvider;
import org.xmldb.api.DatabaseManager;
import org.xmldb.api.base.Collection;
import org.xmldb.api.base.Database;
import org.xmldb.api.base.XMLDBException;
import org.xmldb.api.modules.CollectionManagementService;

public class ManagementExist implements IManagement {

	Logger log = Logger.getLogger(ManagementExist.class);

	Collection existCollection = null;
	Database database = null;
	String dl_name = null;
	String dl_col = null;
	String dl_inst = null;
	String dl_repname = null;
	dlConfig dlconfig = null;

	public ManagementExist() {
		dlconfig = new dlConfig();
		init();
	}

	// metodos para manipular Bidi

	public boolean createBidi(String dlname) {
		return createExist(dlname);
	}

	public boolean deleteBidi(String dlname) {
		return deleteExist(dlname);
	}

	public String[] listBidi() {
		return listExist("");
	}

	// metodos para manipular Colecciones de una Bidi

	public boolean createCol(String colname) {
		if (dl_name != null) { // revisar si es != o ==
			dl_name = dlconfig.getString("dl_name");
		}
		return createExist(dl_name + "/" + colname);
	}

	public boolean deleteCol(String colname) {
		if (dl_name != null) {
			dl_name = dlconfig.getString("dl_name");
		}
		return deleteExist(dl_name + "/" + colname);
	}

	public String[] listCol() {
		if (dl_name != null) {
			dl_name = dlconfig.getString("dl_name");
		}
		return listExist(dl_name);
	}

	// metodos para manipular Instituciones dentro de una Coleccion

	public boolean createInst(String inst) {
		if (dl_name != null) // revisar si es != o ==
			dl_name = dlconfig.getString("dl_name");

		if (dl_col != null)
			dl_col = dlconfig.getString("dl_col");

		return createExist(dl_name + "/" + dl_col + "/" + inst);
	}

	public boolean deleteInst(String inst) {
		if (dl_name != null)
			dl_name = dlconfig.getString("dl_name");

		if (dl_col != null)
			dl_col = dlconfig.getString("dl_col");
		return deleteExist(dl_name + "/" + dl_col + "/" + inst);
	}

	public String[] listInst() {
		if (dl_name != null)
			dl_name = dlconfig.getString("dl_name");

		if (dl_col != null)
			dl_col = dlconfig.getString("dl_col");
		return listExist(dl_name + "/" + dl_col);
	}

	// metodos para manipular Repositorios dentro de una Coleccion/Institucion

	public boolean createRep(String repname) {

		if (dl_name != null) {
			dl_name = dlconfig.getString("dl_name");
		}
		if (dl_col != null)
			dl_col = dlconfig.getString("dl_col");

		if (dl_inst != null)
			dl_inst = dlconfig.getString("dl_inst");

		return createExist(dl_name + "/" + dl_col + "/" + dl_inst + "/"
				+ repname);
	}

	public boolean deleteRep(String repname) {
		if (dl_name == null) {
			dl_name = dlconfig.getString("dl_name");
		}
		if (dl_col == null)
			dl_col = dlconfig.getString("dl_col");

		if (dl_inst == null)
			dl_inst = dlconfig.getString("dl_inst");

		System.out.println("borrando el repo2:" + dl_name + "/" + dl_col + "/"
				+ dl_inst + "/" + repname);

		return deleteExist(dl_name + "/" + dl_col + "/" + dl_inst + "/"
				+ repname);
	}

	public String[] listRep() {
		if (dl_name != null)
			dl_name = dlconfig.getString("dl_name");

		if (dl_col != null)
			dl_col = dlconfig.getString("dl_col");

		if (dl_inst != null)
			dl_inst = dlconfig.getString("dl_inst");

		return listExist(dl_name + "/" + dl_col + "/" + dl_inst);
	}

	public String getBidi() {
		return dl_name;
	}

	public void setBidi(String dl) {
		this.dl_name = dl;
	}

	public String getCol() {
		return dl_col;
	}

	public void setCol(String dl_col) {
		this.dl_col = dl_col;
	}

	public String getInst() {
		return dl_inst;
	}

	public void setInst(String dl_inst) {
		this.dl_inst = dl_inst;
	}

	public String getRepname() {
		return dl_repname;
	}

	public void setRepname(String repname) {
		this.dl_repname = repname;
	}

	// metodos propios de eXist

	private void init() {
		Class cl = null;
		try {
			cl = Class.forName(dlconfig.getString("exist_driver"));
			database = (Database) cl.newInstance();
			DatabaseManager.registerDatabase(database);
		} catch (ClassNotFoundException e) {
			log.error(e);
		} catch (InstantiationException e) {
			log.error(e);
		} catch (IllegalAccessException e) {
			log.error(e);
		} catch (XMLDBException e) {
			log.error(e);
		}

	}

	private boolean createExist(String path) {
		boolean result = false;
		String dl_col = dlconfig.getExist_uri() + "/db/" + path;
		try {
			existCollection = DatabaseManager.getCollection(dl_col,
					dlconfig.getString("existUser"),
					dlconfig.getString("existPass"));
			if (existCollection == null) {
				Collection root = DatabaseManager.getCollection(
						dlconfig.getExist_uri() + "/db",
						dlconfig.getString("existUser"),
						dlconfig.getString("existPass"));
				CollectionManagementService mgtService = (CollectionManagementService) root
						.getService("CollectionManagementService", "1.0");
				existCollection = mgtService.createCollection(path);
				result = true;
			}
		} catch (XMLDBException e) {
			log.error(e);
		}

		return result;
	}

	private boolean deleteExist(String path) {
		boolean result = false;
		String dl_col = dlconfig.getExist_uri() + "/db/" + path;
		try {
			existCollection = DatabaseManager.getCollection(dl_col,
					dlconfig.getString("existUser"),
					dlconfig.getString("existPass"));
			if (existCollection != null) {
				Collection root = DatabaseManager.getCollection(
						dlconfig.getExist_uri() + "/db",
						dlconfig.getString("existUser"),
						dlconfig.getString("existPass"));
				CollectionManagementService mgtService = (CollectionManagementService) root
						.getService("CollectionManagementService", "1.0");
				mgtService.removeCollection(path);
				result = true;
			}
		} catch (XMLDBException e) {
			log.error(e);
		}

		return result;
	}

	private String[] listExist(String path) {
		String list[] = null;
		String dl_col = dlconfig.getExist_uri() + "/db/" + path;
		try {
			existCollection = DatabaseManager.getCollection(dl_col,
					dlconfig.getString("existUser"),
					dlconfig.getString("existPass"));
			if (existCollection != null)
				list = existCollection.listChildCollections();
		} catch (XMLDBException e) {
			log.error(e);
		}
		return list;
	}
}
