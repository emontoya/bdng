package org.bdng.harvmgr.validator;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CargarXML {
	// public static void main(String[] args) throws IOException
	// {
	// CargarXML verArchivo = new CargarXML();
	// final String datos =
	// verArchivo.leerArchivo("D:/Proyectos Netbeans/Proyectos Java/MyXsd/PruebaXSLT/bdng-sample.xml");
	// System.out.println("datos: " +datos.split("###XXX###")[1]);
	// final String datosIds =
	// verArchivo.crearArchivoTemp(datos.split("###XXX###")[0],"D:/Proyectos Netbeans/Proyectos Java/MyXsd/temp1.xml",Integer.parseInt(datos.split("###XXX###")[1]));
	// System.out.println("Datos Ids: "+datosIds);
	//
	// }

	final String dcrecord = "<dc:record xmlns:dc=\"http://purl.org/dc/elements/1.1/\">";

	public String leerArchivoSimple(final String PathArchivoXMLUNico)
			throws IOException {
		final StringBuilder archivo = new StringBuilder();
		try {
			final File deDonde = new File(PathArchivoXMLUNico);
			if (deDonde.exists()) {
				final FileReader file_reader = new FileReader(deDonde);
				final BufferedReader bufer_reader = new BufferedReader(
						file_reader);
				try {
					String LINEA = bufer_reader.readLine();
					while (LINEA != null) {
						LINEA = bufer_reader.readLine();
						// System.out.println("linea:"+LINEA);
						archivo.append(LINEA);
					}
				} catch (Exception ex) {
					ex.printStackTrace();
				} finally {
					bufer_reader.close();
					file_reader.close();
				}
			}
		} catch (FileNotFoundException ex) {
		}
		if (archivo.toString().isEmpty()) {
			final String idmastitulo = "Registro con identifier = No Encontrado###YYY### y titulo: No encontrado";
			archivo.delete(0, archivo.length());
			archivo.append(idmastitulo);
		} else {
			final String identifier = archivo.toString().split("<dmp:general>")[1]
					.split("<dmp:identifier>")[1].split("</dmp:identifier>")[0]
					.split("<dmp:catalog")[1].split("</dmp:catalog>")[0]
					.split(">")[1].trim();
			archivo.delete(0, archivo.length());

			final String add = "Registro con identifier = " + identifier
					+ "###YYY### y titulo: No Encontrado";

			archivo.append(add);

		}
		return archivo.toString();
	}

	public void borrarArchivo(final String PathDestTempDel) {
		try {
			final File deDonde = new File(PathDestTempDel);
			if (deDonde.exists()) {
				// deDonde.delete();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public String leerArchivo(final String PathArchivoXML) throws IOException {
		final StringBuilder archivo = new StringBuilder();
		try {
			final File deDonde = new File(PathArchivoXML);
			if (deDonde.exists()) {
				final FileReader file_reader = new FileReader(deDonde);
				final BufferedReader bufer_reader = new BufferedReader(
						file_reader);
				try {
					String LINEA = bufer_reader.readLine();
					while (LINEA != null) {
						LINEA = bufer_reader.readLine();
						// System.out.println("linea:"+LINEA);
						archivo.append(LINEA);
					}
				} catch (Exception ex) {
					ex.printStackTrace();
				} finally {
					bufer_reader.close();
					file_reader.close();
				}
			}
		} catch (FileNotFoundException ex) {

		}
		final int cantRegistros = archivo.toString().split(dcrecord).length - 1;
		return archivo.toString() + "###XXX###" + cantRegistros;
	}

	public String crearArchivoTemp(final String archivoOrigen,
			final String PathDestinoTemp, final int registro) {
		// Calculo Registros
		final int registros = archivoOrigen.split("</dc:record>").length - 1;
		// Crea el archivo Temp
		final String add = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
				+ archivoOrigen.split(dcrecord)[0]
				+ dcrecord
				+ archivoOrigen.split(dcrecord)[registro + 1]
						.split("</dc:record>")[0]
				+ "</dc:record>"
				+ archivoOrigen.split("</dc:record>")[registros].replaceAll(
						"</bdng>null", "</bdng>").replaceAll("null", "");

		imprimir(add, PathDestinoTemp);

		// Envia los identificadores que solicita

		final String identifier = archivoOrigen.split(dcrecord)[registro + 1]
				.split("<dmp:identifier>")[1].split("</dmp:identifier>")[0]
				.split(">")[1].split("<")[0];

		final String titulo = archivoOrigen.split(dcrecord)[registro + 1]
				.split("<dmp:titlePack")[1].split("</dmp:titlePack>")[0]
				.split("<dmp:title")[1].split("</dmp:title")[0].split(">")[1]
				.split("<")[0];
		final String datos_ids = "Registro con identifier = " + identifier
				+ "###YYY### y titulo: " + titulo;
		return datos_ids;
	}

	public void imprimir(final String aImprimir, final String pathNombrelog) {

		try {
			final FileWriter filewrit = new FileWriter(pathNombrelog/* ,true */);
			final BufferedWriter buffwrit = new BufferedWriter(filewrit);
			buffwrit.write(aImprimir);
			buffwrit.newLine();
			buffwrit.close();
			filewrit.close();
		} catch (Exception ex) {
			Logger.getLogger(ValidadorLog.class.getName()).log(Level.SEVERE,
					null, ex);
		}
	}
}
