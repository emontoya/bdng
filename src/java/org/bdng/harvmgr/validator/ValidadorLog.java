package org.bdng.harvmgr.validator;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.bdng.harvmgr.dlConfig;

import lombok.Cleanup;

public class ValidadorLog {
	// public String leerProperties(final String proper)
	// {
	// String propiedad;
	// final Properties prop = new Properties();
	// try
	// {
	// final @Cleanup InputStream input_stream = new
	// FileInputStream("AS400.properties");
	// prop.load(input_stream);
	// input_stream.close();
	// }
	// catch (IOException ex) {this.imprimir(ex.toString());}
	// propiedad = prop.getProperty(proper,"");
	// prop.clear();
	// return propiedad;
	// }

	private String obtenerFechaActual() {
		final Calendar capturar_fecha = Calendar.getInstance();
		return Integer.toString(capturar_fecha.get(Calendar.YEAR))
				+ Integer.toString((capturar_fecha.get(Calendar.MONTH)) + 1)
				+ Integer.toString(capturar_fecha.get(Calendar.DATE));
	}

	private String generarNombreDocumento() {
		return obtenerFechaActual() + ".txt";
	}

	public void imprimir(final String aImprimir, final String institucion) {
		try {
			final FileWriter filewrit = new FileWriter(institucion + "-"
					+ generarNombreDocumento(), true);
			final BufferedWriter buffwrit = new BufferedWriter(filewrit);
			buffwrit.write(aImprimir);
			buffwrit.newLine();
			buffwrit.close();
			filewrit.close();
		} catch (Exception ex) {
			Logger.getLogger(ValidadorLog.class.getName()).log(Level.SEVERE,
					null, ex);
		}
	}

	public String obtenerHoraActual() {
		return new Time(new Date().getTime()).toString();
	}

	public void imprimirTrasabilidad(final String aImprimir) {
		try {
			final FileWriter filewrit = new FileWriter(obtenerFechaActual()
					+ "-CONEXIONES.txt", true);
			final BufferedWriter buffwrit = new BufferedWriter(filewrit);
			buffwrit.write(obtenerHoraActual() + "  ---->  " + aImprimir);
			buffwrit.newLine();
			buffwrit.close();
			filewrit.close();
		} catch (Exception ex) {
			Logger.getLogger(ValidadorLog.class.getName()).log(Level.SEVERE,
					null, ex);
		}
	}

	public List<File> retornarLogs() {
		List<File> result = new ArrayList<>();
		String myDl_home = System.getProperty("user.dir").replace("eXist", "");
		File dirLogs = new File(myDl_home + "/logs/");
		if (dirLogs.exists()) {
			File[] ficheros = dirLogs.listFiles();
			for (int i = 0; i < ficheros.length; i++) {
				if ((getExtension(ficheros[i])).equals("txt")) {
					result.add(ficheros[i]);
				}
			}
		} else {
		}
		return result;
	}

	public String getExtension(File f) {
		String ext = "";
		int dot = f.getName().lastIndexOf(".");
		ext = f.getName().substring(dot + 1);
		return ext;
	}
}