package org.bdng.harvmgr.validator;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.bdng.harvmgr.indexer.Indexer;
import org.bdng.harvmgr.indexer.RepositoryExist;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Time;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.bdng.harvmgr.dlConfig;

public class ValidadorReda {
	String archivoXsd = null;
	String directorioLogs = null;
	String archivoSimpleXml = null;
	String archivoMultipleXml = null;
	String dlHome = null;
	dlConfig dlconfig = null;
	String type = "all";
	String[] repos = null;

	// Logger log = Logger.getLogger(Indexer.class);

	boolean working = false;
	boolean continuar = true;
	String[] reps = null;
	
	int numfiles=0;
	float percentageIndexed=0;
	String currentFile = null;
	int count=0;

	ValidadorReda valreda = null;

	public ValidadorReda(String bdng_home, String xsd, String dirlogs) {
		this.archivoXsd = xsd;
		this.directorioLogs = dirlogs;
		this.dlHome = bdng_home;
		dlconfig = new dlConfig(bdng_home);

	}
	
	public ValidadorReda(String bdng_home, String xsd, String dirlogs,String[] reps) {
		this.archivoXsd = xsd;
		this.directorioLogs = dirlogs;
		this.dlHome = bdng_home;
		this.repos = reps;
		dlconfig = new dlConfig(bdng_home);

	}

	public String getDirLogs() {
		return directorioLogs;
	}

	public boolean validarSimpleReda(final String PathNomArchValid,
			final String PathDirectorioLog) throws SAXException, IOException {
		boolean retorno = true;
		final Source schemaFile = new StreamSource(new File(archivoXsd));
		final SchemaFactory schemaFactory = SchemaFactory
				.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		final Schema schema = schemaFactory.newSchema(schemaFile);
		final Validator validator = schema.newValidator();
		final List<SAXParseException> exceptions = new LinkedList<SAXParseException>();

		validator.setErrorHandler(new ErrorHandler() {
			@Override
			public void warning(final SAXParseException exception)
					throws SAXException {
				exceptions.add(exception);
			}

			@Override
			public void fatalError(final SAXParseException exception)
					throws SAXException {
				exceptions.add(exception);
			}

			@Override
			public void error(final SAXParseException exception)
					throws SAXException {
				exceptions.add(exception);
			}
		});

		CargarXML cargarId = new CargarXML();
		final String datosIds = cargarId.leerArchivoSimple(PathNomArchValid);
		final Source xmlFile = new StreamSource(new File(PathNomArchValid));
		try {

			validator.validate(xmlFile);
			generarLog(exceptions, PathNomArchValid, PathDirectorioLog,
					datosIds);
		} catch (Exception e) {
			generarLog(exceptions, PathNomArchValid, PathDirectorioLog,
					datosIds);
		}
		if (!exceptions.isEmpty() && exceptions.size() > 0) {
			retorno = false;
		}
		return retorno;
	}

	// New Metodo
	public boolean validaSimpleDCReda(final String PathNombreArchivoValidar,
			final String PathDirectorioLog, final String datosIds)
			throws SAXException {
		boolean retorno = true;
		final Source schemaFile = new StreamSource(new File(archivoXsd));
		final SchemaFactory schemaFactory = SchemaFactory
				.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		final Schema schema = schemaFactory.newSchema(schemaFile);
		final Validator validator = schema.newValidator();
		final List<SAXParseException> exceptions = new LinkedList<SAXParseException>();

		validator.setErrorHandler(new ErrorHandler() {
			@Override
			public void warning(SAXParseException exception)
					throws SAXException {
				exceptions.add(exception);
			}

			@Override
			public void fatalError(SAXParseException exception)
					throws SAXException {
				exceptions.add(exception);
			}

			@Override
			public void error(SAXParseException exception) throws SAXException {
				exceptions.add(exception);
			}
		});

		final Source xmlFile = new StreamSource(new File(
				PathNombreArchivoValidar));
		try {
			validator.validate(xmlFile);
			generarLog(exceptions, PathNombreArchivoValidar, PathDirectorioLog,
					datosIds);
		} catch (Exception e) {
			generarLog(exceptions, PathNombreArchivoValidar, PathDirectorioLog,
					datosIds);
		}
		if (!exceptions.isEmpty() && exceptions.size() > 0) {
			retorno = false;
		}
		return retorno;
	}

	public boolean validarMultipleReda(final String PathNombreArchivoValidar,
			final String PathDirectorioLog, final String PathCreacionTemporales)
			throws SAXException {
		boolean retorno = true;
		final Source schemaFile = new StreamSource(new File(archivoXsd));
		final SchemaFactory schemaFactory = SchemaFactory
				.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		final Schema schema = schemaFactory.newSchema(schemaFile);
		final Validator validator = schema.newValidator();
		final List<SAXParseException> exceptions = new LinkedList<SAXParseException>();

		try {
			final CargarXML verArchivo = new CargarXML();
			// retorna archivo xml.completo y numero de registros separado por
			// ###XXX###
			final String datos = verArchivo
					.leerArchivo(PathNombreArchivoValidar);// .replaceAll("null",
															// "");
			// System.out.println("datos: " +datos.split("###XXX###")[0]);

			final int cantRegistros = Integer
					.parseInt(datos.split("###XXX###")[1]);

			for (int i = 0; i < cantRegistros; i++) {
				final CargarXML verArchivo1 = new CargarXML();

				final String datosIds = verArchivo1.crearArchivoTemp(
						datos.split("###XXX###")[0], PathCreacionTemporales, i);

				validaSimpleDCReda(PathCreacionTemporales, PathDirectorioLog,
						datosIds);

				final CargarXML verArchivo2 = new CargarXML();
				verArchivo2.borrarArchivo(PathCreacionTemporales);
				exceptions.clear();
			}
			final CargarXML verArchivo2 = new CargarXML();
			verArchivo2.borrarArchivo(PathCreacionTemporales);
		} catch (Exception e) {
			final CargarXML verArchivo2 = new CargarXML();
			verArchivo2.borrarArchivo(PathCreacionTemporales);
		}
		if (!exceptions.isEmpty() && exceptions.size() > 0) {
			retorno = false;
		}
		return retorno;
	}

	public void generarLog(final List<SAXParseException> exceptions,
			final String nombrearchivo, final String PathArchivoLog,
			final String datosIds) {
		// System.out.println("llegue");
		StringBuilder valido = new StringBuilder();
		valido.append("Valido");
		final StringBuilder acumulador = new StringBuilder();

		final String idtitulo = datosIds.split("###YYY###")[0] + " "
				+ datosIds.split("###YYY###")[1];

		acumulador.append(idtitulo);
		if (exceptions != null && !exceptions.isEmpty()) {
			// System.out.println("No es valido");
			valido.delete(0, valido.length());
			valido.append(" NO ES VALIDO");

			acumulador.append(valido.toString() + " Por lo siguiente ");

			for (int i = 0; i < exceptions.size(); i++) {
				// System.out.println((i+1)+" Exepciones---"+exceptions.get(i).toString());
				final String add = "\n " + (i + 1) + ". "
						+ analizarLog(exceptions.get(i).toString());// +" <-------> ";
				acumulador.append(add);
			}
			final String add = acumulador.substring(0,
					(acumulador.length() - 11));
			acumulador.delete(0, acumulador.length());
			acumulador.append(add);

			imprimir(obtenerHoraActual() + " - " + acumulador.toString(),
					PathArchivoLog);
		} else {
			// System.out.println("Es Valido");
			// System.out.println("PathArchivoLog:"+PathArchivoLog);
			valido.delete(0, valido.length());
			valido.append(" ES VALIDO");
			acumulador.append(valido.toString());

			if (datosIds.contains("prueba###YYY###1")) {
				imprimir(obtenerHoraActual() + " - EL ARCHIVO " + nombrearchivo
						+ " ES VALIDO", PathArchivoLog);
			} else {
				imprimir(obtenerHoraActual() + " - " + acumulador.toString(),
						PathArchivoLog);
			}

		}

	}

	private String analizarLog(final String exepcionProcesar) {
		final String Analizar1 = "org.xml.sax.SAXParseException: cvc-complex-type.3.2.2:";
		final String Analizar2 = "org.xml.sax.SAXParseException: cvc-complex-type.2.4.a:";
		final String Analizar3 = "org.xml.sax.SAXParseException: cvc-complex-type.2.4.b:";
		final String Analizar4 = "org.xml.sax.SAXParseException: cvc-complex-type.3.2.2:";
		final String Analizar5 = "org.xml.sax.SAXParseException: cvc-complex-type.4:";
		final String Analizar6 = "org.xml.sax.SAXParseException:";
		final String Analizar7 = "cvc-elt.1:";

		final String Analizar8 = " cvc-complex-type.3.2.2:";
		final String Analizar9 = " cvc-complex-type.2.4.a:";
		final String Analizar10 = " cvc-complex-type.2.4.b:";
		final String Analizar11 = " cvc-complex-type.3.2.2:";
		final String Analizar12 = " cvc-complex-type.4:";

		final String retorno1 = exepcionProcesar.replaceAll(Analizar1, "")
				.replaceAll(Analizar2, "").replaceAll(Analizar3, "")
				.replaceAll(Analizar4, "").replaceAll(Analizar5, "")
				.replaceAll(Analizar6, "").replaceAll(Analizar7, "")
				.replaceAll(Analizar8, "").replaceAll(Analizar9, "")
				.replaceAll(Analizar10, "").replaceAll(Analizar11, "")
				.replaceAll(Analizar12, "");

		final String retornar = retorno1.replaceAll("Attribute", "El Atributo");
		final String retornar1 = retornar.replaceAll(
				"is not allowed to appear in element",
				"no es permitido en el elemento");
		final String retornar2 = retornar1.replaceAll("must appear on element",
				"debe aparecer en el elemento");
		final String retornar3 = retornar2.replaceAll(
				"Invalid content was found starting with element",
				"Contenido invalido encontrado iniciando el elemento");
		final String retornar4 = retornar3.replaceAll("The content of element",
				"El contenido del elemento");
		final String retornar5 = retornar4.replaceAll("is not complete",
				"no esta completo");
		final String retornar6 = retornar5.replaceAll("One of", "Uno de");
		final String retornar7 = retornar6.replaceAll("is expected\\.",
				"es esperado.");

		String retorno = retornar7;
		if (retornar7.contains("cvc-enumeration-valid: ")) {
			final String retornar8 = retornar7.replaceAll(
					"cvc-enumeration-valid: ", "");
			retorno = retornar8;
		}

		if (retorno.contains("cvc-attribute.3: ")) {
			final String retornar8 = retorno
					.replaceAll("cvc-attribute.3: ", "");
			retorno = retornar8;
		}

		if (retorno.contains("cvc-complex-type.2.2: ")) {
			final String retornar8 = retorno.replaceAll(
					"cvc-complex-type.2.2: ", "");
			retorno = retornar8;
		}

		if (retorno.contains("The value")) {
			final String retornar8 = retorno
					.replaceAll("The value", "EL Valor");
			retorno = retornar8;
		}

		if (retorno.contains("Value")) {
			final String retornar8 = retorno.replaceAll("Value", "Valor");
			retorno = retornar8;
		}

		if (retorno.contains(" It must be a value from the enumeration.")) {
			final String retornar8 = retorno.replaceAll(
					" It must be a value from the enumeration.",
					" Debe ser un valor de la numeracion.");
			retorno = retornar8;
		}

		if (retorno
				.contains(" is not facet-valid with respect to enumeration ")) {
			final String retornar8 = retorno.replaceAll(
					" is not facet-valid with respect to enumeration ",
					" no es valido respecto a ");
			retorno = retornar8;
		}

		if (retorno.contains(" on element ")) {
			final String retornar8 = retorno.replaceAll(" on element ",
					" en el elemento ");
			retorno = retornar8;
		}

		if (retorno.contains(" of attribute ")) {
			final String retornar8 = retorno.replaceAll(" of attribute ",
					" del atributo ");
			retorno = retornar8;
		}

		if (retorno.contains(" is not valid with respect to its type, ")) {
			final String retornar8 = retorno.replaceAll(
					" is not valid with respect to its type, ",
					" no es valido con respecto a este tipo, ");
			retorno = retornar8;
		}

		if (retorno.contains(" Element ")) {
			final String retornar8 = retorno.replaceAll(" Element ",
					" EL Elemento");
			retorno = retornar8;
		}

		if (retorno.contains(" must have no element ")) {
			final String retornar8 = retorno.replaceAll(
					" must have no element [children],",
					" no debe tener elementos[hijos],");
			retorno = retornar8;
		}

		if (retorno.contains(" and the value must be valid.")) {
			final String retornar8 = retorno.replaceAll(
					" and the value must be valid.",
					" y el valor debe ser valido.");
			retorno = retornar8;
		}

		// add new 24-Sept-2013

		if (retorno.contains(" Cannot find the declaration of element ")) {
			final String retornar8 = retorno.replaceAll(
					" Cannot find the declaration of element ",
					" No se encontro la declaracion del elemento ");
			retorno = retornar8;
		}

		if (retorno.contains("lineNumber:")) {
			final String retornar8 = "lineNumber: "
					+ retorno.split("lineNumber:")[1];

			retorno = retornar8;
		}

		return retorno;
	}

	// PARA GENERAR ARCHIVO LOG
	public String obtenerHoraActual() {
		return new Time(new Date().getTime()).toString();
	}

	public void imprimir(final String aImprimir, final String pathNombrelog) {
		try {
			final FileWriter filewrit = new FileWriter(
					generarNombreDocumento(pathNombrelog), true);
			final BufferedWriter buffwrit = new BufferedWriter(filewrit);
			buffwrit.write(aImprimir);
			buffwrit.newLine();
			buffwrit.close();
			filewrit.close();
		} catch (Exception ex) {
			Logger.getLogger(ValidadorLog.class.getName()).log(Level.SEVERE,
					null, ex);
		}
	}

	private String generarNombreDocumento(final String pathNombrelog) {
		return pathNombrelog + ".txt";
	}

	public String getFechaHoraActual() {
		final Calendar capturar_fecha = Calendar.getInstance();
		final String fechaHora = Integer.toString(capturar_fecha
				.get(Calendar.YEAR))
				+ rellenarMonthOrDay(Integer.toString((capturar_fecha
						.get(Calendar.MONTH)) + 1))
				+ rellenarMonthOrDay(Integer.toString(capturar_fecha
						.get(Calendar.DATE)))
				+ "-"
				+ Integer.toString(capturar_fecha.get(Calendar.HOUR_OF_DAY))
				+ Integer.toString(capturar_fecha.get(Calendar.MINUTE));
		return fechaHora;
	}

	public String getFechaActual() {
		final Calendar capturar_fecha = Calendar.getInstance();
		final String fecha = Integer
				.toString(capturar_fecha.get(Calendar.YEAR))
				+ rellenarMonthOrDay(Integer.toString((capturar_fecha
						.get(Calendar.MONTH)) + 1))
				+ rellenarMonthOrDay(Integer.toString(capturar_fecha
						.get(Calendar.DATE)));
		return fecha;
	}

	public String rellenarMonthOrDay(final String fechacuadrar) {
		StringBuilder retorno = new StringBuilder();
		if (fechacuadrar.trim().length() == 1) {
			final String montOrDay = "0" + fechacuadrar;
			retorno.append(montOrDay);
		} else {
			retorno.append(fechacuadrar);
		}
		return retorno.toString();
	}
	
	/**
	 * retorna el porcentage de progreso de la indexación
	 * @return porcentage 
	 */
	
	public int getProgress() {
		return (int)(percentageIndexed*100);
	}
	
	public String getCurrentFile() {
		return currentFile;
	}

	public void validarTodos() {
		File c, r, f;
		String[] cols, reps, files;
		String col, rep, file;
		String dir = dlconfig.getString("dl_metadata") + "/dbxml";
		c = new File(dir);
		if (c != null) {
			cols = c.list();
			int count = 0;
			valreda = new ValidadorReda(dlHome, this.dlHome
					+ "/conf/xsd/doermp.xsd", this.dlHome + "/logs/");
			String fechahora = valreda.getFechaHoraActual();
			String fecha = valreda.getFechaActual();

			numfiles = countfiles();

			for (int i = 0; cols != null && i < cols.length && continuar; i++) {
				col = dir + "/" + cols[i];
				c = new File(col);
				if (c!=null && c.isDirectory()) {
					reps = c.list();
					for (int j = 0; reps != null && j < reps.length
							&& continuar; j++) {
						rep = col + "/" + reps[j];
						r = new File(rep);
						if (r!=null && r.isDirectory()) {
							files = r.list();
							for (int k = 0; files != null && k < files.length
									&& continuar; k++) {
								file = rep + "/" + files[k];
								f = new File(file);
								if (f.isFile() && files[k].endsWith(".xml")) {

									try {
										System.out
												.println("Validando repositorio:"
														+ reps[j]
														+ "_"
														+ (k + 1));
										// crea log por archivo:
										// valreda.validarMultipleReda(file,
										// valreda.getDirLogs()+reps[i]+"_"+(k+1)+"-"+fechahora,valreda.getDirLogs()+reps[i]+"-temp.xml");

										// crea un solo log por repositorio
										valreda.validarMultipleReda(file,
												valreda.getDirLogs() + reps[j]
														+ "-" + fechahora,
												valreda.getDirLogs() + reps[j]
														+ "-temp.xml");
										count++;
										percentageIndexed = (float) count
												/ numfiles;

										currentFile = f.getName();
										System.out.println(getCurrentFile()
												+ " %=" + getProgress());

									} catch (SAXException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}

								} else
									System.out.println("Directorio: "
											+ files[k]);
							}
						}
					}

				}
			}
		}
	}

	public void validarRepo(String repname) {
		File c, r, f;
		String[] cols, reps, files;
		String col, rep, file;
		String dir = dlconfig.getString("dl_metadata") + "/dbxml";
		c = new File(dir);
		if (c!=null) {
		cols = c.list();
		valreda = new ValidadorReda(dlHome, this.dlHome
				+ "/conf/xsd/doermp.xsd", this.dlHome + "/logs/");
		String fechahora = valreda.getFechaHoraActual();
		String fecha = valreda.getFechaActual();
		
		numfiles=countfilesSelected();
		
		for (int i = 0; cols!=null && i < cols.length && continuar; i++) {
			
			col = dir + "/" + cols[i];
			//System.out.println("Col: " + col);
			c = new File(col);
			if (c!=null && c.isDirectory()) {
				reps = c.list();
				for (int j = 0; reps!=null && j < reps.length && continuar; j++) {
					rep = col + "/" + reps[j];
					r = new File(rep);
					if (r!=null && r.isDirectory() && r.getName().equals(repname)) {
						
						files = r.list();
						for (int k = 0; files != null && k < files.length && continuar; k++) {
							
							file = rep + "/" + files[k];
							f = new File(file);
							if (f!=null && f.isFile() && files[k].endsWith(".xml")) {
								
								try {
									
									System.out.println("Validando repositorio:"
											+ reps[j] + "_" + (k + 1));
									
									// crea log por archivo:
									// valreda.validarMultipleReda(file,
									// valreda.getDirLogs()+reps[i]+"_"+(k+1)+"-"+fechahora,valreda.getDirLogs()+reps[i]+"-temp.xml");

									// crea un solo log por repositorio
									valreda.validarMultipleReda(file,
											valreda.getDirLogs() + reps[j]
													+ "-" + fechahora,
											valreda.getDirLogs() + reps[j]
													+ "-temp.xml");
									
									count++;
									percentageIndexed=(float)count/numfiles;
									currentFile=f.getName();
									System.out.println(getCurrentFile()+" %="+getProgress());
									
								} catch (SAXException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

							} else
								System.out.println("Directorio: " + files[k]);
						}
					}
				}

			}
		}
		}

	}
	
	
	private boolean isRepoSelected(String rep) {
		boolean result = false;
		for (int i=0;i<repos.length;i++) {
			if (rep.equals(repos[i])) {
				result = true;
				break;
			}
		}
		return result;
	}
	
	private int countfilesSelected() {
		int numfiles=0;
		File c, r, f;
		String[] cols, reps, files;
		String col, rep, file;
		String dir = dlconfig.getString("dl_metadata") + "/dbxml";
		c = new File(dir);
		if (c!=null) {
		cols = c.list();
		for (int i = 0; cols!=null && i < cols.length && continuar; i++) {
			col = dir + "/" + cols[i];
			c = new File(col);
			if (c!=null && c.isDirectory()) {
				reps = c.list();
				for (int j = 0; reps!=null && j < reps.length && continuar; j++) {
					rep = col + "/" + reps[j];
					r = new File(rep);
					if (r!=null && r.isDirectory() && isRepoSelected(r.getName())) {
						files = r.list();
						for (int k = 0; files!=null && k < files.length && continuar; k++) {
							file = rep + "/" + files[k];
							f = new File(file);
							if (f!=null && f.isFile() && files[k].endsWith(".xml")) {
								numfiles++;
								}
						}
					}
				}

			}
		}
		}
		return numfiles;
	}

	
	private int countfiles() {
		int numfiles = 0;
		File c, r, f;
		String[] cols, reps, files;
		String col, rep, file;
		String dir = dlconfig.getString("dl_metadata") + "/dbxml";
		c = new File(dir);
		if (c != null) {
			cols = c.list();
			for (int i = 0; cols != null && i < cols.length && continuar; i++) {
				col = dir + "/" + cols[i];
				c = new File(col);
				if (c.isDirectory()) {
					reps = c.list();
					for (int j = 0; reps != null && j < reps.length
							&& continuar; j++) {
						rep = col + "/" + reps[j];
						r = new File(rep);
						if (r.isDirectory()) {
							files = r.list();
							for (int k = 0; files != null && k < files.length
									&& continuar; k++) {
								file = rep + "/" + files[k];
								f = new File(file);
								if (f.isFile() && files[k].endsWith(".xml")) {
									numfiles++;
								}
							}
						}
					}

				}
			}
		}

		return numfiles;
	}

	public void run() {
		working = true;
		if (type.equals("all"))
			validarTodos();
		else {
			for (int i = 0; i < reps.length; i++)
				validarRepo(reps[i]);
		}
		working = false;
	}

	public static void main(String[] args) {
		String dl_home = null;
		String repname = null;

		if (args.length > 1) {
			dl_home = args[0];
			repname = args[1];
		} else {
			System.out.println("DL_HOME no especificado");
			System.exit(0);
		}

		String defaultxsd = dl_home + "/conf/xsd/bdng.xsd";
		String defaultlogs = dl_home + "/logs/";

		ValidadorReda validar = new ValidadorReda(dl_home, defaultxsd,
				defaultlogs);

		System.out
				.println("******** Iniciando el proceso de VALIDACION ********");
		if (repname.equals("all"))
			validar.validarTodos();
		else
			validar.validarRepo(repname);

		System.out
				.println("******** El proceso de VALIDACION finalizo ********");
	}

}
