package org.bdng.harvmgr.validator;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.jdom2.Document;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

public class ValidatorDoer {

	Source schemaFile = null;
	List<SAXParseException> exceptions = new LinkedList<SAXParseException>();

	public ValidatorDoer(String filexsd) {
		schemaFile = new StreamSource(new File(filexsd));
	}

	public boolean validar(String filexml) {
		boolean result = false;
		SchemaFactory schemaFactory = SchemaFactory
				.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		Schema schema;
		javax.xml.validation.Validator validator = null;
		try {
			schema = schemaFactory.newSchema(schemaFile);
			validator = schema.newValidator();
		} catch (SAXException e1) {
			e1.printStackTrace();
		}
		validator.setErrorHandler(new ErrorHandlerMan());
		Source xmlFile = new StreamSource(new File(filexml));
		try {
			validator.validate(xmlFile);

			if (exceptions.size() == 0)
				result = true;
			// System.out.println(xmlFile.getSystemId() + " es valido");
			for (int i = 0; i < exceptions.size(); i++) {
				// System.out.println(exceptions.get(i).getLocalizedMessage());
			}
		} catch (SAXException | IOException e) {
			// System.out.println(xmlFile.getSystemId() + " is not valid");
			// System.out.println("Reason: " + e.getLocalizedMessage());
		}
		return result;
	}

	private class ErrorHandlerMan implements ErrorHandler {
		public void warning(SAXParseException exception) throws SAXException {
			exceptions.add(exception);
		}

		public void fatalError(SAXParseException exception) throws SAXException {
			exceptions.add(exception);
		}

		public void error(SAXParseException exception) throws SAXException {
			exceptions.add(exception);
		}
	}

	public static void main(String[] args) {
		String filexsd = "xsd/bdng.xsd";
		String filexml = "xml/reda-ri_1.xml";
		ValidatorDoer valdoer = new ValidatorDoer(filexsd);
		valdoer.validar(filexml);
	}

}
