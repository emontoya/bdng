package org.bdng.harvmgr.validator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URL;
import java.net.URLConnection;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import java.util.concurrent.Semaphore;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.bdng.harvmgr.dlConfig;
import org.bdng.harvmgr.harvester.HTTPVisitor;
import org.bdng.harvmgr.harvester.WriteXML;
import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.Namespace;
import org.jdom2.input.SAXBuilder;
import org.xml.sax.SAXException;

public class Validator {

	static final String VERB_LIST_RECORDS = "ListRecords";
	static final String VERB_IDENTIFY = "Identify";
	static final String VERB_LIST_METADATA_FORMATS = "ListMetadataFormats";
	static final String VERB_LIST_IDENTIFIERS = "ListIdentifiers";
	static final String VERB_GET_RECORD = "GetRecord";
	static final String VERB_LISTSETS = "ListSets";
	private String FILE_OAI_PROVIDERS = null;
	private String FILE_OAI_PROVIDERS_STATUS = null;
	private String OAI_DC_SCHEMA = null;
	private String OAI_LOMCO_SCHEMA = null;
	private String OAI_LOM_SCHEMA = null;
	Logger log;

	/* Constantes de validacion semantica */
	private static final String MANDATORY = "1";
	private static final String RECOMMENDED = "2";
	private static final String OPTIONAL = "3";
	private static final String ONEINSTANCE = "1";
	private static final String MULTIPLEINSTANCE = "n";

	/* Constantes gestion TextPatterns */
	private static final String PATTERN = "pattern";
	private static final String APPLY_FOR = "apply_for";
	private static final String TAG = "tag";
	private static final String OLDTEXT = "oldText";
	private static final String NEWTEXT = "newText";
	private static final String GENERAL = "desconocido";
	private static final String UNKNOWN = "unknown";
	private static final String ALL = "all";

	static String identifier = null;
	static String metadataPrefix = null;
	static String baseUrl = null;
	static Semaphore harvester = new Semaphore(1);
	static String resumptionToken = null;
	private WriteXML writeXML = new WriteXML();

	boolean errorSemantico = false;
	String mensajeErrorSemantico = "";
	private dlConfig dlconfig = null;

	private static String archivoTextPatterns = null;
	private String archivoAcceptedTags = null;
	private String archivoMapeadoCodigos = null;
	private static String archivoValidDublinCoreTags = null;
	private static String archivoValidLomCoTags = null;

	public String path = null;

	private String outputMessage;
	private HashMap<Integer, String> mapeadoCodigos = new HashMap<Integer, String>();
	private Document resultadoFinalDoc;

	/*
	 * Se crean HashMap's para guardar cada etiqueta con su respectivo arrayList
	 * de posibilidades de contenido
	 */
	private HashMap<String, ArrayList<String>> tagsLomCoValidation = new HashMap<String, ArrayList<String>>();
	private HashMap<String, ArrayList<String>> tagsDcValidation = new HashMap<String, ArrayList<String>>();

	/*
	 * Se crean HashMap's para guardar cada etiqueta con su respectiva
	 * multiplicidad, un arreglo de dos posiciones en donde se tienen
	 * obligatoriedad(0:Opcional,1:Obligatorio,2:Recomendado) e instancias(1,n)
	 */
	private HashMap<String, ArrayList<String>> tagsLomCoMultiplicity = new HashMap<String, ArrayList<String>>();
	private HashMap<String, ArrayList<String>> tagsDcMultiplicity = new HashMap<String, ArrayList<String>>();

	/*
	 * Variables para estadisticas, son utilizadas para generar score en informe
	 * de validacion
	 */
	private int registrosRecorridos = 0;
	boolean registroExitosoSemantica = true;
	boolean tagExitosoSemantica = true;

	private int registrosExitososSemantica = 0;

	private int registrosTotales = 0;
	private static final double VALORPORCENTAJEPROTOCOLO = 0.20;
	private static final double VALORPORCENTAJESINTAXIS = 0.40;
	private static final double VALORPORCENTAJESEMANTICA = 0.40;
	
	Semantic semantic = null;

	/* Constructor WEB */
	public Validator(String dl_home) {
		dlconfig = new dlConfig(dl_home);
		path = dlconfig.getString("dl_home");
		FILE_OAI_PROVIDERS = dlconfig.getOaiProviders();
		FILE_OAI_PROVIDERS_STATUS = dlconfig.getOaiProvidersStatus();
		archivoAcceptedTags = path + "/" + dlconfig.getAcceptedTags();
		archivoMapeadoCodigos = path + "/"
				+ dlconfig.getValidationCodesMapping();
		archivoTextPatterns = path + "/" + dlconfig.getTextPatterns();
		OAI_LOMCO_SCHEMA = "file://" + path + "/" + dlconfig.getSchema_lomco();
		loadCodigosMensajes();
		log = Logger.getLogger(Validator.class);
	}

	/**
	 * Utilizado por el validador WEB. Carga todos los elementos validos para un
	 * esquema definido en un arrayList, este es un utilizado posteriormente en
	 * validacion sintactica. Se logra debido a que se recibe el parametro
	 * metadataPrefix
	 * 
	 * @param metadataPrefix
	 * @throws JDOMException
	 * @throws IOException
	 * 
	 * 
	 */
	public void loadValidTags(String metadataPrefix) throws JDOMException,
			IOException {

		SAXBuilder sb = new SAXBuilder();
		Document documento = null;

		/*
		 * Este ArrayList contiene en la posicion 0 la multiplicidad y en la 1
		 * la obligatoriedad del elemento Key
		 */

		HashMap<String, ArrayList<String>> mapValidation = new HashMap<String, ArrayList<String>>();
		HashMap<String, ArrayList<String>> mapMultiplicity = new HashMap<String, ArrayList<String>>();

		/*
		 * Carga documento con el archivo xml, asignar dinamicamente los
		 * HashMap's
		 */
		if (metadataPrefix.equalsIgnoreCase("oai_dc")) {
			archivoValidDublinCoreTags = path + "/"
					+ dlconfig.getValidDublinCoreTags();
			documento = sb.build(archivoValidDublinCoreTags);
			// mapValidation = tagsDcValidation;
			mapMultiplicity = tagsDcMultiplicity;
		} else if (metadataPrefix.equalsIgnoreCase("oai_lomco")) {
			archivoValidLomCoTags = path + "/" + dlconfig.getValidLomCoTags();
			documento = sb.build(archivoValidLomCoTags);
			// mapValidation = tagsLomCoValidation;
			mapMultiplicity = tagsLomCoMultiplicity;
		}

		/*
		 * Obtiene elemento raiz con etiqueta <elements> y sus hijos
		 */
		Element elementoRaiz = documento.getRootElement();

		List<Element> elementosHijos = elementoRaiz.getChildren();
		Iterator<Element> iterador = elementosHijos.iterator();

		/* Agrega informacion del elemento al HashMap tagsDcValidation */
		while (iterador.hasNext()) {

			Element elemento = iterador.next();
			String nombreElemento = (elemento.getChild("name").getText());

			Element vocabulario = elemento.getChild("vocabulary");
			/* Revisar que el elemento tiene vocabulario controlado */
			if (vocabulario != null) {

				/*
				 * Crear arrayList que sera agregado al value del HashMap
				 * tagsDcValidation, este contiene todos los vocabularios
				 * validos para un elemento particular(Key del hashMap)
				 */
				ArrayList<String> vocabularioValido = new ArrayList<String>();

				List contenidos = vocabulario.getChildren();
				Iterator i = contenidos.iterator();
				while (i.hasNext()) {
					/*
					 * Obtener cada elemento contenido, agregarlo al ArrayList
					 * vocabularioValido, que posteriormente sera agregado al
					 * HashMap como value
					 */
					Element contenido = (Element) i.next();
					/* Agrega una instancia en MAYUS */
					vocabularioValido.add(contenido.getText().toUpperCase());
				}
				mapValidation.put(nombreElemento, vocabularioValido);

			} else {
				/*
				 * No agregar el ArrayList de vocabulario debido a que el
				 * elemento no tiene vocabulario controlado
				 */
				mapValidation.put(nombreElemento, null);
			}
			String multiplicidad = (elemento.getChild("multiplicity").getText());
			String obligatoriedad = (elemento.getChild("mandatory").getText());
			/*
			 * Agregar al ArrayList la multiplicidad y la obligatoriedad,
			 * posicion 0 y 1 respectivamente
			 */

			ArrayList<String> multiplicidadyObligatoriedad = new ArrayList<String>();

			multiplicidadyObligatoriedad.add(0, multiplicidad);
			multiplicidadyObligatoriedad.add(1, obligatoriedad);
			mapMultiplicity.put(nombreElemento, multiplicidadyObligatoriedad);

		}
		elementosHijos = null;

		this.tagsDcValidation = mapValidation;
		this.tagsLomCoValidation = mapValidation;

		this.tagsDcMultiplicity = mapMultiplicity;
		this.tagsLomCoMultiplicity = mapMultiplicity;

	}

	/**
	 * Utilizado por el validador en modo consola. Carga todos los elementos
	 * validos para el esquema Dublin Core en sus correspondientes HashMaps a
	 * partir del archivo validTagsDublinCore.xml, estos HashMaps son utilizados
	 * posteriormente en validacion sintactica y semantica.
	 * 
	 * @throws JDOMException
	 * @throws IOException
	 */
	public void loadTagsDc() throws JDOMException, IOException {
		SAXBuilder sb = new SAXBuilder();
		Document documento = null;
		/*
		 * Crear arrayList que sera agregado al value del HashMap
		 * tagsDcValidation, este contiene todos los vocabularios validos para
		 * un elemento particular(Key del hashMap)
		 */
		ArrayList<String> vocabularioValido = new ArrayList<String>();

		/*
		 * Este ArrayList contiene en la posicion 0 la multiplicidad y en la 1
		 * la obligatoriedad del elemento Key
		 */
		ArrayList<String> multiplicidadyObligatoriedad = new ArrayList<String>();

		/* Carga documento con el archivo xml */
		documento = sb.build(archivoValidDublinCoreTags);

		/*
		 * Obtiene elemento raiz con etiqueta <elements> y sus hijos
		 */
		Element elementoRaiz = documento.getRootElement();

		List<Element> elementosHijos = elementoRaiz.getChildren();
		Iterator<Element> iterador = elementosHijos.iterator();

		/* Agrega informacion del elemento al HashMap tagsDcValidation */
		while (iterador.hasNext()) {
			Element elemento = iterador.next();
			String nombreElemento = (elemento.getChild("name").getText());
			Element vocabulario = elemento.getChild("vocabulary");
			/* Revisar que el elemento tiene vocabulario controlado */
			if (vocabulario != null) {
				List contenidos = vocabulario.getChildren();
				Iterator i = contenidos.iterator();
				while (i.hasNext()) {
					/*
					 * Obtener cada elemento contenido, agregarlo al ArrayList
					 * vocabularioValido, que posteriormente sera agregado al
					 * HashMap como value
					 */
					Element contenido = (Element) i.next();
					vocabularioValido.add(contenido.getText());
				}
				this.tagsDcValidation.put(nombreElemento, vocabularioValido);
			} else {
				/*
				 * No agregar el ArrayList de vocabulario debido a que el
				 * elemento no tiene vocabulario controlado
				 */
				this.tagsDcValidation.put(nombreElemento, null);
			}
			String multiplicidad = (elemento.getChild("multiplicity").getText());
			String obligatoriedad = (elemento.getChild("mandatory").getText());

			/*
			 * Agregar al ArrayList la multiplicidad y la obligatoriedad,
			 * posicion 0 y 1 respectivamente
			 */
			multiplicidadyObligatoriedad.add(0, multiplicidad);
			multiplicidadyObligatoriedad.add(1, obligatoriedad);
			this.tagsDcMultiplicity.put(nombreElemento,
					multiplicidadyObligatoriedad);

		}
		elementosHijos = null;

	}

	/**
	 * Utilizado por el validador en modo consola. Carga todos los elementos
	 * validos para el esquema LomCo en el arrayList tagsLomCo a partir del
	 * archivo validTagsLomCo.xml, este arrayList es utilizado posteriormente en
	 * validacion sintactica.
	 * 
	 * @throws JDOMException
	 * @throws IOException
	 */
	public void loadTagsLomCo() throws JDOMException, IOException {

		SAXBuilder sb = new SAXBuilder();
		Document documento = null;
		/*
		 * Crear arrayList que sera agregado al value del HashMap
		 * tagsDcValidation, este contiene todos los vocabularios validos para
		 * un elemento particular(Key del hashMap)
		 */
		ArrayList<String> vocabularioValido = new ArrayList<String>();

		/*
		 * Este ArrayList contiene en la posicion 0 la multiplicidad y en la 1
		 * la obligatoriedad del elemento Key
		 */
		ArrayList<String> multiplicidadyObligatoriedad = new ArrayList<String>();

		/* Carga documento con el archivo xml */
		documento = sb.build(archivoValidLomCoTags);

		/*
		 * Obtiene elemento raiz con etiqueta <elements> y sus hijos
		 */
		Element elementoRaiz = documento.getRootElement();

		List<Element> elementosHijos = elementoRaiz.getChildren();
		Iterator<Element> iterador = elementosHijos.iterator();

		/* Agrega informacion del elemento al HashMap tagsDcValidation */
		while (iterador.hasNext()) {
			Element elemento = iterador.next();
			String nombreElemento = (elemento.getChild("name").getText());
			Element vocabulario = elemento.getChild("vocabulary");
			/* Revisar que el elemento tiene vocabulario controlado */
			if (vocabulario != null) {
				List contenidos = vocabulario.getChildren();
				Iterator i = contenidos.iterator();
				while (i.hasNext()) {
					/*
					 * Obtener cada elemento contenido, agregarlo al ArrayList
					 * vocabularioValido, que posteriormente sera agregado al
					 * HashMap como value
					 */
					Element contenido = (Element) i.next();
					vocabularioValido.add(contenido.getText());
				}
				this.tagsLomCoValidation.put(nombreElemento, vocabularioValido);
			} else {
				/*
				 * No agregar el ArrayList de vocabulario debido a que el
				 * elemento no tiene vocabulario controlado
				 */
				this.tagsLomCoValidation.put(nombreElemento, null);
			}
			String multiplicidad = (elemento.getChild("multiplicity").getText());
			String obligatoriedad = (elemento.getChild("mandatory").getText());
			/*
			 * Agregar al ArrayList la multiplicidad y la obligatoriedad,
			 * posicion 0 y 1 respectivamente
			 */
			multiplicidadyObligatoriedad.add(0, multiplicidad);
			multiplicidadyObligatoriedad.add(1, obligatoriedad);
			this.tagsLomCoMultiplicity.put(nombreElemento,
					multiplicidadyObligatoriedad);

		}
		elementosHijos = null;

	}

	public boolean requiereMetadataPrefixParameter(String verb) {
		if (verb.equalsIgnoreCase(VERB_GET_RECORD)
				|| verb.equalsIgnoreCase(VERB_LIST_IDENTIFIERS)
				|| verb.equalsIgnoreCase(VERB_LIST_RECORDS))
			return true;

		return false;
	}

	/**
	 * Crea la siguiente URL con el resumptionToken
	 * 
	 * @param resumptionToken
	 * @param baseURL
	 * @author luis
	 * @return
	 */
	public String formarNextURL(String resumptionToken, String baseURL) {
		String stPrimeraParteUrl = baseURL;
		String stSegundaParte = "?" + "verb=ListRecords";
		String stTerceraParte = "&resumptionToken=" + resumptionToken;

		String nextUrl = stPrimeraParteUrl + stSegundaParte + stTerceraParte;

		return nextUrl;
	}

	/**
	 * Crea la URL necesaria para probar el verbo a partir de una base y un
	 * metadataPrefix
	 * 
	 * @param metadataPrefix
	 * @param baseURL
	 * @param verb
	 * @return
	 */
	public String formarURL(String metadataPrefix, String baseURL, String verb) {
		String stPrimeraParteUrl = baseURL;
		String stSegundaParte = "?" + "verb=" + verb;
		String stTerceraParte = "";
		String stCuartaParte = "";

		System.out.println("\nTesting verb: " + verb);
		System.out.println(metadataPrefix);

		if (requiereMetadataPrefixParameter(verb)) {
			stTerceraParte = "&metadataPrefix=" + metadataPrefix;
		}

		if (verb.equalsIgnoreCase(VERB_GET_RECORD)) {
			if (identifier == null) {
				System.out
						.println("		/Identifier de record no obtenido previamente.(ListRecords)");
			} else {
				stCuartaParte = "&identifier=" + identifier;
			}

		}

		String stUrl = stPrimeraParteUrl + stSegundaParte + stTerceraParte
				+ stCuartaParte;

		return stUrl;
	}

	/**
	 * Valida los verbos del protocolo OAI y brinda una estadistica acerca del
	 * cumplimiento de estandares del repositorio, metodo para constructor WEB
	 * 
	 * @param baseURL
	 * @param shortName
	 * @param protocol
	 * @param metadataPrefix
	 */

	public void validarRepositorio(String baseURL, String shortName,
			String protocol, String metadataPrefix) {

		/*
		 * Carga en el respectivo array todos los elementos validos aceptados
		 * para el formato de metadatos
		 */
		try {

			loadValidTags(metadataPrefix);

			// loadTagsDc();
			// loadTagsLomCo();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error("Error cargando los patrones de elementos validos.");
			System.out
					.println("Error cargando los patrones de elementos validos.");
			log.error(e);

		}

		long totalInitTime = System.currentTimeMillis();
		HashMap<String, Integer> hmCodigoResultado = new HashMap<String, Integer>();
		// int[] aCodigoResultado= new int[6];
		resultadoFinalDoc = new Document();
		Element raiz = new Element("RNRRE");
		resultadoFinalDoc.setRootElement(raiz);
		Validator.metadataPrefix = metadataPrefix;

		/* Creacion de jerarquia de documento XML que presenta el resultado */
		Element elementoResultado = new Element("results");
		Element elementoDetalles = new Element("details");
		Element elementoDetallesProtocolo = new Element("protocol")
				.setAttribute(new Attribute("score", "0")); // default score por
															// si no puede hacer
															// la validacion
															// sintactica
		Element elementoDetallesmetadataPrefix = new Element("metadataPrefix");
		elementoDetalles.addContent(elementoDetallesProtocolo);
		elementoDetalles.addContent(elementoDetallesmetadataPrefix);
		Element elementoDetallesSintaxis = new Element("syntax")
				.setAttribute(new Attribute("score", "0")); // default score por
															// si no puede hacer
															// la validacion
															// sintactica
		Element elementoDetallesSemantica = new Element("semantic")
				.setAttribute(new Attribute("score", "0")); // default score por
															// si no puede hacer
															// la validacion
															// semantica
		raiz.addContent(elementoResultado);

		try {

			/* Variables necesarias para ambos tipos de protocolo */
			long totalEndTime = 0;
			Long tiempoTotal = null;
			String finalScoreProtocol = "0";
			Double finalScoreProtocolDouble = 0.0;
			String finalScoreSyntaxis = "0";
			Double finalScoreSyntaxisDouble = 0.0;
			String finalScoreSemantic = "0";
			Double finalScoreSemanticDouble = 0.0;
			String finalScore = "0";
			double contador = 0.0;
			DecimalFormat contadorFormat = new DecimalFormat("####.##");
			String resultMessage = "";

			/* Revisa protocolo */
			if (protocol.equalsIgnoreCase("OAI")) {
				/* PROTOCOLO OAI */
				System.out.println("Validando OAI");
				/* Agrega elemento de protocolo al documento de salida */
				elementoResultado.addContent(new Element("protocol")
						.setText("OAI"));
				elementoResultado.addContent(new Element("metadataPrefix")
						.setText(metadataPrefix));

				/* Validacion de verbos para protocolo OAI */
				// if(metadataPrefix.contains("lom_co")){
				// metadataPrefix="oai_lom";
				// }
				baseUrl = baseURL;

				hmCodigoResultado.put("Identify",
						validarIdentify(baseUrl, elementoResultado));
				hmCodigoResultado.put("ListMetadataFormats",
						validarListMetadataFormats(baseUrl, elementoResultado));
				hmCodigoResultado.put(
						"ListIdentifiers",
						validarListIdentifiers(metadataPrefix, baseUrl,
								elementoResultado));
				hmCodigoResultado.put(
						"ListRecords",
						validarListRecords(metadataPrefix, baseUrl, shortName,
								elementoResultado, protocol,
								elementoDetallesSintaxis,
								elementoDetallesSemantica));
				hmCodigoResultado.put(
						"ListSets",
						validarListSets(metadataPrefix, baseUrl,
								elementoResultado));
				hmCodigoResultado.put(
						"GetRecord",
						validarGetRecord(metadataPrefix, baseUrl, identifier,
								elementoResultado));

				contador = 0.0;
				Set set = hmCodigoResultado.entrySet();
				/* Obtener iterador */
				Iterator i = set.iterator();
				/* Desplegar los elementos */
				while (i.hasNext()) {
					Map.Entry me = (Map.Entry) i.next();
					if (me.getValue() == (Integer) 1) {
						contador++;
					} else if (me.getValue() == (Integer) 2) {
						elementoDetalles
								.addContent(new Element("detail")
										.setText("Revisar pruebas negativas(parametros erroneos) para el verbo "
												+ me.getKey()));
						resultMessage = resultMessage
								+ "Revisar pruebas negativas(parametros erroneos) para el verbo "
								+ me.getKey() + "\n";
						contador = contador + 0.8;
					} else if (me.getValue() == (Integer) 3) {
						elementoDetalles
								.addContent(new Element("detail")
										.setText("Revisar estado de la respuesta xml, caracteristicas de tags requeridos para el verbo "
												+ me.getKey()));
						resultMessage = resultMessage
								+ "Revisar estado de la respuesta xml, caracteristicas de tags requeridos para el verbo "
								+ me.getKey() + "\n";
						contador = contador + 0.3;
					} else {
						elementoDetalles
								.addContent(new Element("detail")
										.setText("Revisar estado url y adquisicion de datos para el verbo "
												+ me.getKey()));
						resultMessage = resultMessage
								+ "Revisar estado url y adquisicion de datos para el verbo "
								+ me.getKey() + "\n";
					}
					// System.out.print(me.getKey() + ": ");
					// System.out.println(me.getValue());
				}

				/* Calcula tiempo total de ejecucion */
				totalEndTime = System.currentTimeMillis();
				tiempoTotal = (totalEndTime - totalInitTime) / 1000;
				// finalScore="0";

				elementoDetalles.addContent(elementoDetallesSintaxis);
				elementoDetalles.addContent(elementoDetallesSemantica);

				if (contador == 0.0) {
					elementoDetallesProtocolo
							.addContent(new Element("detail")
									.setText("1.Revisar estado url's y obtencion de datos para verbos en general"));
					elementoDetallesProtocolo
							.addContent(new Element("detail")
									.setText("2.Revisar estado de la respuesta xml, caracteristicas de tags requeridos para verbos en general"));
					elementoDetallesProtocolo
							.addContent(new Element("detail")
									.setText("3.Revisar pruebas negativas (parametros erroneos) para verbos en general"));
					resultMessage = "1. Revisar estado de url's\n2. Revisar estado de la respuesta xml, caracteristicas de tags requeridos\n"
							+ "3. Revisar pruebas negativas (parametros erroneos)";
					// System.out.println("Resultado final: \n" +
					// "Tiempo total:"+tiempoTotal+"\n"+
					// "El repositorio cumple el protocolo OAI en un 0%\n" +
					// "1. Revisar estado de url's\n2.Revisar estado de la respuesta xml, caracteristicas de tags requeridos\n"
					// +
					// "3. Revisar pruebas negativas (parametros erroneos)\n");

				} else {
					finalScoreProtocol = contadorFormat
							.format((contador / hmCodigoResultado.size()) * 100);
					finalScoreProtocolDouble = (contador / hmCodigoResultado
							.size()) * 100;

					// elementoDetallesProtocolo.addContent(new
					// Element("score").setText(finalScoreProtocol +
					// "% ("+VALORPORCENTAJEPROTOCOLO*100+"% del puntaje total de validacion.)"));
					elementoDetallesProtocolo.setAttribute(new Attribute(
							"score", finalScoreProtocol)); // add edy
					elementoDetallesProtocolo.setAttribute(new Attribute(
							"success", "success")); // add edy
					if (finalScoreProtocol.equalsIgnoreCase("100")) {
						elementoDetallesProtocolo
								.addContent(new Element("detail")
										.setText("Validacion de protocolo ha sido totalmente exitosa"));
					} else {
						elementoDetallesProtocolo
								.addContent(new Element("detail")
										.setText("Validacion de protocolo no ha sido totalmente exitosa"));
					}

				}

				/*
				 * 
				 * if(!errorSintactico){ finalScoreSyntaxis="100";
				 * finalScoreSyntaxisDouble = 100.0;
				 * elementoDetallesSintaxis.addContent(new
				 * Element("detail").setText("Validacion sintactica exitosa"));
				 * }else{ finalScoreSyntaxis=contadorFormat.format(((double)
				 * registrosExitososSintaxis/registrosTotales)*100);
				 * finalScoreSyntaxisDouble
				 * =(((double)registrosExitososSintaxis/registrosTotales)*100);
				 * elementoDetallesSintaxis.addContent(new
				 * Element("detail").setText
				 * ("Validacion sintactica no ha sido completamente exitosa"));
				 * } elementoDetallesSintaxis.addContent(new
				 * Element("score").setText
				 * (finalScoreSyntaxis+"% ("+VALORPORCENTAJESINTAXIS
				 * *100+"% del puntaje total de validacion.)"));
				 */

				if (!errorSemantico) {
					finalScoreSemantic = "100";
					finalScoreSemanticDouble = 100.0;

				} else {
					finalScoreSemantic = contadorFormat
							.format(((double) registrosExitososSemantica / registrosTotales) * 100);
					finalScoreSemanticDouble = (((double) registrosExitososSemantica / registrosTotales) * 100);
					elementoDetallesSemantica
							.addContent(new Element("detail")
									.setText("Validacion semantica no ha sido completamente exitosa"));
				}

				// elementoDetallesSemantica.addContent(new
				// Element("score").setText(finalScoreSemantic+"% ("+VALORPORCENTAJESEMANTICA*100+"% del puntaje total de validacion.)"));

				if (registrosTotales == 0) {
					finalScoreSemantic = "0";
					finalScoreSemanticDouble = 0.0;
					elementoDetallesSemantica
							.addContent(new Element("detail")
									.setText("Validacion semantica no se ha podido realizar"));
				}

				// elementoDetallesSemantica.setAttribute(new Attribute("score",
				// finalScoreSemantic)); //add edy
				// elementoDetallesSemantica.setAttribute(new
				// Attribute("success", "success")); //add edy

				finalScoreSyntaxisDouble = Double
						.parseDouble(elementoDetallesSintaxis
								.getAttributeValue("score"));
				finalScoreSemanticDouble = Double
						.parseDouble(elementoDetallesSemantica
								.getAttributeValue("score"));

				/*
				 * System.out.println("SCORES");
				 * System.out.println(finalScoreProtocolDouble);
				 * System.out.println(finalScoreSemanticDouble);
				 * System.out.println(finalScoreSyntaxisDouble);
				 */

				/* Calcular el score final */
				Double finalScoreDouble = ((finalScoreProtocolDouble * VALORPORCENTAJEPROTOCOLO)
						+ (finalScoreSemanticDouble * VALORPORCENTAJESEMANTICA) + (finalScoreSyntaxisDouble * VALORPORCENTAJESINTAXIS));

				finalScore = contadorFormat.format(finalScoreDouble);
				System.out
						.println("Resultado final: \n"
								+ "Tiempo total:"
								+ tiempoTotal
								+ "\n"
								+ "El repositorio cumple los estandares de protocolo, sintaxis y semantica en un "
								+ finalScore + "%\n" + resultMessage);

				if (finalScore.equalsIgnoreCase("100")) {
					// elementoDetalles.addContent(new
					// Element("general").addContent(new
					// Element("detail").setText("Validacion de protocolo, sintaxis y semantica totalmente exitosa")));
					resultMessage = "Validacion totalmente exitosa";
					System.out.println("Validacion totalmente exitosa.");
				} else {
					// elementoDetalles.addContent(new
					// Element("general").addContent(new
					// Element("detail").setText("La validacion de protocolo, sintaxis y semantica no ha sido totalmente exitosa")));
					System.out
							.println("La validacion no ha sido totalmente exitosa.");
				}
			} else if (protocol.equalsIgnoreCase("HTTP")) {

				/* PROTOCOLO HTTP */
				System.out.println("Validando HTTP");
				/* Agrega elemento protocol al documento xml de salida */
				elementoResultado.addContent(new Element("protocol")
						.setText("HTTP"));
				elementoResultado.addContent(new Element("metadataPrefix")
						.setText(metadataPrefix));

				baseUrl = baseURL;
				ArrayList<Integer> arrCodigoResultado = new ArrayList<Integer>();
				HTTPVisitor hv = new HTTPVisitor();
				HashMap<String, String> paths = hv.getXMLPaths(baseUrl);

				Iterator<Map.Entry<String, String>> iterator = paths.entrySet()
						.iterator();

				if (paths.size() == 0) {
					/*
					 * No existe directorio, path apuntando directamente al
					 * archivo
					 */
					arrCodigoResultado.add(validarEntradaHTTP(baseUrl,
							elementoDetallesProtocolo,
							elementoDetallesSintaxis,
							elementoDetallesSemantica, protocol, shortName));

				} else {
					while (iterator.hasNext()) {

						/* Obtener entrada a partir del iterador del Map */
						Map.Entry<String, String> entrada = iterator.next();

						/* Obtengo el path */
						String path = entrada.getValue();

						/* Procesar Map Entry para recolecci_n y Procesado */
						arrCodigoResultado
								.add(validarEntradaHTTP(path,
										elementoDetallesProtocolo,
										elementoDetallesSintaxis,
										elementoDetallesSemantica, protocol,
										shortName));
					}
				}

				totalEndTime = System.currentTimeMillis();
				tiempoTotal = (totalEndTime - totalInitTime) / 1000;

				Iterator iteradorarray = arrCodigoResultado.iterator();
				while (iteradorarray.hasNext()) {
					Integer codeResultado = (Integer) iteradorarray.next();
					if (codeResultado == 1) {
						contador++;
						resultMessage = resultMessage
								+ "		/Archivo obtenido con exito!\n";
					} else if (codeResultado == 4) {
						elementoDetalles.addContent(new Element("detail")
								.setText("Revisar adquisicion de datos"));
						resultMessage = resultMessage
								+ "		/Revisar adquisicion de datos.\n";
						contador = contador + 0.3;
					} else if (codeResultado == 5) {
						resultMessage = resultMessage
								+ "		/HTTP 500! Peticion fallida.\n";
					} else {
						resultMessage = resultMessage
								+ "		/Error ocurrio accediendo un archivo.\n";
					}
				}

				finalScoreProtocolDouble = (contador / arrCodigoResultado
						.size()) * 100;
				finalScoreProtocol = contadorFormat
						.format((contador / arrCodigoResultado.size()) * 100);
				System.out.println("Resultado final: \n" + "Tiempo total:"
						+ tiempoTotal + "\n"
						+ "El repositorio cumple el protocolo HTTP en un "
						+ finalScoreProtocol + "%\n" + resultMessage);
				// elementoDetallesProtocolo.addContent(new
				// Element("score").setText(finalScoreProtocol +
				// "% (20% del puntaje total de validacion.)"));
				elementoDetallesProtocolo.setAttribute(new Attribute("score",
						finalScoreProtocol)); // add edy
				elementoDetallesProtocolo.setAttribute(new Attribute("success",
						"success")); // add edy
				if (finalScoreProtocol.equalsIgnoreCase("100")) {
					elementoDetallesProtocolo
							.addContent(new Element("detail")
									.setText("Validacion de protocolo ha sido totalmente exitosa"));
					System.out
							.println("Validacion de protocolo totalmente exitosa.");
					resultMessage = "			/Validacion de protocolo totalmente exitosa.";
				} else {
					elementoDetallesProtocolo
							.addContent(new Element("detail")
									.setText("La validacion de protocolo no ha sido totalmente exitosa."));
					System.out
							.println("La validacion de protocolo no ha sido totalmente exitosa.");
				}

				elementoDetalles.addContent(elementoDetallesSintaxis);
				elementoDetalles.addContent(elementoDetallesSemantica);

				/*
				 * if(!errorSintactico){ finalScoreSyntaxis="100";
				 * finalScoreSyntaxisDouble = 100.0;
				 * elementoDetallesSintaxis.addContent(new
				 * Element("detail").setText("Validacion sintactica exitosa"));
				 * }else{ finalScoreSyntaxis=contadorFormat.format(((double)
				 * registrosExitososSintaxis/registrosTotales)*100);
				 * finalScoreSyntaxisDouble
				 * =(((double)registrosExitososSintaxis/registrosTotales)*100);
				 * elementoDetallesSintaxis.addContent(new
				 * Element("detail").setText
				 * ("Validacion sintactica no ha sido completamente exitosa"));
				 * } elementoDetallesSintaxis.addContent(new
				 * Element("score").setText
				 * (finalScoreSyntaxis+"% (40% del puntaje total de validacion.)"
				 * ));
				 */

				if (!errorSemantico) {
					finalScoreSemantic = "100";
					finalScoreSemanticDouble = 100.0;
					elementoDetallesSemantica.addContent(new Element("detail")
							.setText("Validacion semantica exitosa"));
				} else {
					finalScoreSemantic = contadorFormat
							.format(((double) registrosExitososSemantica / registrosTotales) * 100);
					finalScoreSemanticDouble = (((double) registrosExitososSemantica / registrosTotales) * 100);
					elementoDetallesSemantica
							.addContent(new Element("detail")
									.setText("Validacion semantica no ha sido completamente exitosa"));
				}

				// elementoDetallesSemantica.addContent(new
				// Element("score").setText(finalScoreSemantic+"% (40% del puntaje total de validacion.)"));
				// elementoDetallesSemantica.setAttribute(new Attribute("score",
				// finalScoreSemantic));
				// elementoDetallesSemantica.setAttribute(new
				// Attribute("success", "success"));

				finalScoreSemanticDouble = Double
						.parseDouble(elementoDetallesSemantica
								.getAttributeValue("score")); // add edy
				finalScoreSyntaxisDouble = Double
						.parseDouble(elementoDetallesSintaxis
								.getAttributeValue("score")); // add edy

				/* Calcular el score final */
				Double finalScoreDouble = ((finalScoreProtocolDouble * VALORPORCENTAJEPROTOCOLO)
						+ (finalScoreSemanticDouble * VALORPORCENTAJESEMANTICA) + (finalScoreSyntaxisDouble * VALORPORCENTAJESINTAXIS));

				finalScore = contadorFormat.format(finalScoreDouble);
				System.out
						.println("Resultado final: \n"
								+ "Tiempo total:"
								+ tiempoTotal
								+ "\n"
								+ "El repositorio cumple los estandares de protocolo, sintaxis y semantica en un "
								+ finalScore + "%\n" + resultMessage);

				if (finalScore.equalsIgnoreCase("100")) {
					elementoDetalles
							.addContent(new Element("general").addContent(new Element(
									"detail")
									.setText("Validacion de protocolo, sintaxis y semantica totalmente exitosa")));
					resultMessage = "Validacion totalmente exitosa";
					System.out.println("Validacion totalmente exitosa.");
				} else {
					// elementoDetalles.addContent(new
					// Element("general").addContent(new
					// Element("detail").setText("La validacion de protocolo, sintaxis y semantica no ha sido totalmente exitosa")));
					System.out
							.println("La validacion no ha sido totalmente exitosa.");
				}
			}
			// }
			Element total = new Element("total");
			elementoResultado.addContent(total);
			total.addContent(new Element("totalTime").setText(tiempoTotal
					.toString() + "(s)"));
			total.addContent(new Element("score").setText(finalScore + "%"));
			total.addContent(elementoDetalles);

			String filename = shortName + ".xml";
			String pathSalida = path + "/" + dlconfig.getFileValidation();
			// HarvesterAbs.deleteDirectory(new File(pathSalida));
			// System.out.println(pathSalida);
			writeXML.writeXmlJdomFile(pathSalida, filename, resultadoFinalDoc);

		} catch (IOException e) {
			log.error(e);

			// System.out.println("LOCALIZED MESSAGE: "+e.getLocalizedMessage());
			System.out
					.println("Error de entrada y salida mientras se validaba el repositorio.");

		} catch (Exception e) {
			log.error(e);

			// System.out.println("LOCALIZED MESSAGE: "+e.getLocalizedMessage());
			System.out.println("Error de validacion.");
		}

	}

	/**
	 * Valida el verbo Identifier para el protocolo OAI
	 * 
	 * @param metadataPrefix
	 * @param baseUrl
	 * @param dataProvider
	 * @return codigoResultado 1. Validaci_n Exitosa, requisitos respuesta
	 *         esperada y prueba negativa exitosa. 2. Validaci_n Exitosa,
	 *         requisitos respuesta esperada pero prueba negativa fallida. 3.
	 *         Verbo exitoso, prueba negativa exitosa pero requisitos de
	 *         respuesta no encontrados/faltantes/erroneos. 4. Verbo exitoso,
	 *         datos no obtenidos. 5. Peticion no exitosa, error de peticion con
	 *         verbo identificado. 6. Error inesperado.
	 * @throws Exception
	 */
	public int validarListIdentifiers(String metadataPrefix, String baseUrl,
			Element elementoResultado) throws Exception {
		String stOutput = null;
		String stUrl = null;
		Integer codigoResultado = 0;
		boolean exito = false;

		/* Agregamos la jerarquia del documento */
		Element elementoVerbo = new Element("ListIdentifiers");
		elementoResultado.addContent(elementoVerbo);
		Element elementoDetalles = new Element("details");

		/* FormaURL y hace descarga del archivo xml que genera la peticion */
		stUrl = formarURL(metadataPrefix, baseUrl, "ListIdentifiers");
		stOutput = downloadDataFromURL(stUrl);
		establecerMensaje(stOutput);

		if (outputMessage.equals("request successfull")) {
			codigoResultado = 1;
			elementoDetalles.addContent(new Element("detail")
					.setText("HTTP 200 OK!"));
			System.out.println("		HTTP 200             OK!");
			SAXBuilder sb = new SAXBuilder();
			Document doc = sb.build(new StringReader(stOutput));
			Element raiz = doc.getRootElement();
			Namespace namespace = raiz.getNamespace();
			Element identifiers = raiz.getChild("ListIdentifiers", namespace);
			if (identifiers != null) {
				List<Element> identifiersc = identifiers.getChildren();
				if (identifiersc.size() == 0) {
					elementoDetalles
							.addContent(new Element("detail")
									.setText("El verbo identifiers no contiene campos de informacion, no existen registros."));
					System.out
							.println("		/El verbo identifiers no contiene campos de informacion."
									+ "		/No existen registros");
				} else {
					// Iterator<Element> iterator = identifiersc.iterator();
					// int contadorReg=0;
					// while (iterator.hasNext()) {
					// contadorReg++;
					// }
					// System.out.println("		/	" + contadorReg
					// + " encabezados encontrados" );
					elementoDetalles.addContent(new Element("detail")
							.setText("Encabezados de registros encontrados."));
					System.out.println("		/Encabezados encontrados");
				}

			} else {
				elementoDetalles
						.addContent(new Element("detail")
								.setText("El verbo ListIdentifiers no devuelve la salida correcta."));
				System.out
						.println("		/El verbo ListIdentifiers no devuelve la salida correcta.");
				codigoResultado = 3;
			}
			exito = pruebaNegativa("ListIdentifiers", baseUrl);

			if (!exito) {
				codigoResultado = 2;
				elementoDetalles.addContent(new Element("detail")
						.setText("Prueba negativa al verbo fallada."));
				System.out.println("		/Prueba negativa al verbo fallada.");
			}
		} else
			codigoResultado = establecerCodigoResultado(/* codigoResultado, */elementoDetalles);

		elementoVerbo.addContent(new Element("outputCode")
				.setText(codigoResultado.toString()));
		elementoVerbo
				.addContent(new Element("codeMessage")
						.setText(obtenerMensajeCodigo(mapeadoCodigos,
								codigoResultado)));
		elementoVerbo.addContent(elementoDetalles);
		System.out.println("Codigo Resultado : " + codigoResultado);
		return codigoResultado;
	}

	/**
	 * Valida el verbo Identifier para el protocolo OAI
	 * 
	 * @param metadataPrefix
	 * @param baseUrl
	 * @param dataProvider
	 * @return codigoResultado 1. Validaci_n Exitosa, requisitos respuesta
	 *         esperada y prueba negativa exitosa. 2. Validaci_n Exitosa,
	 *         requisitos respuesta esperada pero prueba negativa fallida. 3.
	 *         Verbo exitoso, prueba negativa exitosa pero requisitos de
	 *         respuesta no encontrados/faltantes/erroneos. 4. Verbo exitoso,
	 *         datos no obtenidos. 5. Peticion no exitosa, error de peticion con
	 *         verbo identificado. 6. Error inesperado.
	 * @throws Exception
	 */
	public int validarIdentify(String baseUrl, Element elementoResultado) {
		String stOutput = null;
		String stUrl = null;
		StringBuilder mensajes = new StringBuilder();
		boolean exito = false;
		ArrayList<String> tagsAceptados = new ArrayList<String>();
		SAXBuilder sb = new SAXBuilder();
		/* Crea documento con el archivo xml Archivo archivoAcceptedVerbs */
		Document documentoTags;
		try {
			documentoTags = sb.build(archivoAcceptedTags);
		} catch (JDOMException e) {
			log.error(e);
			return 6;
		} catch (IOException e) {
			log.error(e);
			return 6;
		}
		ArrayList<String> mustTagsText = new ArrayList<String>();
		ArrayList<String> optionalTagsText = new ArrayList<String>();
		// Element mustElement = null;
		Element optionalElement = null;
		Iterator mustTagsIterator = null;
		Iterator optionalTagsIterator = null;
		List mustTags = null;
		List optionalTags = null;
		/* Agregamos la jerarquia del documento */
		Element elementoVerbo = new Element("Identify");
		elementoResultado.addContent(elementoVerbo);
		Element elementoDetalles = new Element("details");
		// elementoVerbo.addContent(elementoResumen);
		// outputMessage=null;
		Integer codigoResultado = 0;
		// baseUrl="https://mail.google.com/mail/?shva=1#inbox";
		stUrl = formarURL("", baseUrl, "Identify");
		stOutput = downloadDataFromURL(stUrl);
		establecerMensaje(stOutput);
		/* Formato xml para facilitar lectura en aplicacion WEB */
		// mensajes.append("<Identify><string>");

		if (outputMessage.equals("request successfull")) {
			elementoDetalles.addContent(new Element("detail")
					.setText("HTTP 200 OK!"));
			codigoResultado = 1;
			System.out.println("		HTTP 200             OK!");
			// SAXBuilder sb = new SAXBuilder();
			Document doc;
			try {
				doc = sb.build(new StringReader(stOutput));
			} catch (JDOMException e) {
				log.error(e);
				return 6;
			} catch (IOException e) {
				log.error(e);
				return 6;
			}
			Element raiz = doc.getRootElement();
			Namespace namespace = raiz.getNamespace();
			Element identify = raiz.getChild("Identify", namespace);
			if (identify != null) {
				List<Element> identifyChildren = identify.getChildren();
				if (identifyChildren.size() == 0) {
					elementoDetalles
							.addContent(new Element("detail")
									.setText("El verbo identify no contiene campos de informacion."));
					// mensajes.append("El verbo identify no contiene campos de informacion.\n");
					System.out
							.println("		/El verbo identify no contiene campos de informacion.");
					codigoResultado = 3;
				} else {
					Element raizVerbos = documentoTags.getRootElement();
					List elementos = raizVerbos.getChildren();
					Iterator<Element> iterador = elementos.iterator();
					while (iterador.hasNext()) {
						Element elemento = iterador.next();
						if (elemento.getAttributeValue("name")
								.equalsIgnoreCase("Identify")) {
							// mustElement=elemento.getChild("must");

							/*
							 * Obtener desde el archivo xml los tags
							 * obligatorios
							 */
							mustTags = elemento.getChild("must").getChildren();
							Iterator<Element> iteradorTags = mustTags
									.iterator();
							while (iteradorTags.hasNext()) {
								Element tag = iteradorTags.next();
								/*
								 * Agregar el texto que contiene cada uno de los
								 * tags
								 */
								mustTagsText.add(tag.getText());
							}
							/* Obtener desde el archivo xml los tags opcionales */
							optionalTags = elemento.getChild("optional")
									.getChildren();
							iteradorTags = null;
							iteradorTags = optionalTags.iterator();
							while (iteradorTags.hasNext()) {
								Element tag = iteradorTags.next();
								/*
								 * Agregar el texto que contiene cada uno de los
								 * tags
								 */
								optionalTagsText.add(tag.getText());
							}

							break;
						}

					}

					Iterator<Element> iterator = identifyChildren.iterator();
					/*
					 * Cada iteracion de la lista es un elemento hijo de la
					 * etiqueta Identify
					 */
					while (iterator.hasNext()) {
						Element child = iterator.next();
						String childName = child.getName();
						if (child.getChildren().size() == 0) {
							System.out.println("		/	" + childName + ": "
									+ child.getText());
							if (mustTagsText.contains(childName)) {
								if (tagsAceptados.contains(childName)) {
									mustTagsIterator = mustTags.iterator();
									while (mustTagsIterator.hasNext()) {
										Element mustTag = (Element) mustTagsIterator
												.next();
										if (childName.equalsIgnoreCase(mustTag
												.getText())) {
											if (mustTag.getAttribute(
													"multiplicity").equals("1")) {
												elementoDetalles
														.addContent(new Element(
																"detail")
																.setText("Multiples instancias para la etiqueta "
																		+ childName
																		+ ". El formato de la respuesta no es correcto."));
												System.out
														.println("		/Multiples instancias para la etiqueta "
																+ childName
																+ ". El formato de la respuesta no es correcto.");
												codigoResultado = 3;
											} else {
												tagsAceptados.add(childName);
											}
											break;
										}
									}
								} else {
									tagsAceptados.add(childName);
								}
							} else if (optionalTagsText.contains(childName)) {
								if (tagsAceptados.contains(childName)) {
									optionalTagsIterator = optionalTags
											.iterator();
									while (optionalTagsIterator.hasNext()) {
										Element optionalTag = (Element) optionalTagsIterator
												.next();
										if (childName
												.equalsIgnoreCase(optionalTag
														.getText())) {
											if (optionalTag.getAttribute(
													"multiplicity").equals("1")) {
												elementoDetalles
														.addContent(new Element(
																"detail")
																.setText("Multiples instancias para la etiqueta "
																		+ childName
																		+ ". El formato de la respuesta no es correcto."));
												System.out
														.println("		/Multiples instancias para la etiqueta "
																+ childName
																+ ". El formato de la respuesta no es correcto.");
												codigoResultado = 3;
											} else {
												tagsAceptados.add(childName);
											}
											break;
										}
									}
								} else {
									tagsAceptados.add(childName);
								}
							} else {
								elementoDetalles
										.addContent(new Element("detail")
												.setText("La etiqueta "
														+ childName
														+ " no es una etiqueta predeterminada. El formato de la respuesta no es correcto."));
								System.out
										.println("		/La etiqueta "
												+ childName
												+ " no es una etiqueta predeterminada. El formato de la respuesta no es correcto.");
								codigoResultado = 3;
							}

						} else {
							// Namespace namespace =
							// Namespace.getNamespace("oai_dc",
							// "http://www.openarchives.org/OAI/2.0/oai_dc/");

							Element oai_identifier = child.getChildren().get(0);
							List children = oai_identifier.getChildren();
							Iterator i = children.iterator();
							System.out.println("		/	" + childName + ":");
							while (i.hasNext()) {
								Element subChild = (Element) i.next();
								System.out.println("	 	   "
										+ subChild.getName() + ": "
										+ subChild.getText());

							}
						}
					}
				}

			} else {
				elementoDetalles
						.addContent(new Element("detail")
								.setText("El verbo Identify no devuelve la salida correcta."));
				System.out
						.println("		/El verbo Identify no devuelve la salida correcta.");
				codigoResultado = 4;
			}

			/* Prueba negativa al verbo, alli se agregaron parametros erroneos */
			exito = pruebaNegativa("Identify", baseUrl);

			if (!exito) {
				codigoResultado = 2;
				elementoDetalles.addContent(new Element("detail")
						.setText("Prueba negativa al verbo fallida."));
				System.out.println("		/Prueba negativa al verbo fallida.");
			}

		} else
			codigoResultado = establecerCodigoResultado(elementoDetalles);

		elementoVerbo.addContent(new Element("outputCode")
				.setText(codigoResultado.toString()));
		elementoVerbo
				.addContent(new Element("codeMessage")
						.setText(obtenerMensajeCodigo(mapeadoCodigos,
								codigoResultado)));
		elementoVerbo.addContent(elementoDetalles);
		System.out.println("Codigo Resultado : " + codigoResultado);

		return codigoResultado;
	}

	/**
	 * Valida etiquetas validas para el verbo Identify
	 * 
	 * @param mustTagsText
	 * @param optionalTagsText
	 * @param tagsAceptados
	 * @param children
	 * @param mustTags
	 * @param optionalTags
	 * @param codigoResultado
	 */
	public void validarTags(ArrayList<String> mustTagsText,
			ArrayList<String> optionalTagsText,
			ArrayList<String> tagsAceptados, List children, List mustTags,
			List optionalTags, int codigoResultado) {

		Iterator<Element> iterator = children.iterator();
		/*
		 * Cada iteracion de la lista es un elemento hijo de la etiqueta
		 * Identify
		 */
		while (iterator.hasNext()) {
			Element child = iterator.next();
			String childName = child.getName();
			// if (iterator.next().getChildren().size() == 0){
			System.out.println("		/	" + childName + ": " + child.getText());
			if (mustTagsText.contains(childName)) {
				// TODO : ALL
				if (tagsAceptados.contains(childName)) {
					Iterator mustTagsIterator = mustTags.iterator();
					while (mustTagsIterator.hasNext()) {
						Element mustTag = (Element) mustTagsIterator.next();
						if (childName.equalsIgnoreCase(mustTag.getText())) {
							if (mustTag.getAttribute("multiplicity")
									.equals("1")) {
								// mensajes.append("Multiples instancias para la etiqueta "+childName+". El formato de la respuesta no es correcto. ");
								System.out
										.println("		/Multiples instancias para la etiqueta "
												+ childName
												+ ". El formato de la respuesta no es correcto.");
								codigoResultado = 3;
							} else {
								tagsAceptados.add(childName);
							}
							break;
						}
					}
				} else {
					tagsAceptados.add(childName);
				}
			} else if (optionalTagsText.contains(childName)) {
				if (tagsAceptados.contains(childName)) {
					Iterator optionalTagsIterator = optionalTags.iterator();
					while (optionalTagsIterator.hasNext()) {
						Element optionalTag = (Element) optionalTagsIterator
								.next();
						if (childName.equalsIgnoreCase(optionalTag.getText())) {
							if (optionalTag.getAttribute("multiplicity")
									.equals("1")) {
								// mensajes.append("Multiples instancias para la etiqueta "+childName+". El formato de la respuesta no es correcto. ");
								System.out
										.println("		/Multiples instancias para la etiqueta "
												+ childName
												+ ". El formato de la respuesta no es correcto.");
								codigoResultado = 3;
							} else {
								tagsAceptados.add(childName);
							}
							break;
						}
					}
				} else {
					tagsAceptados.add(childName);
				}
			} else {
				// mensajes.append("La etiqueta "+childName+" no es una etiqueta predeterminada. El formato de la respuesta no es correcto. ");
				System.out
						.println("		/La etiqueta "
								+ childName
								+ " no es una etiqueta predeterminada. El formato de la respuesta no es correcto.");
				codigoResultado = 3;
			}

			// }
		}
	}

	/**
	 * Valida el verbo Identifier para el protocolo OAI
	 * 
	 * @param metadataPrefix
	 * @param baseUrl
	 * @param dataProvider
	 * @return codigoResultado 1. Validaci_n Exitosa, prueba positiva y negativa
	 *         exitosa. 2. Verbo exitoso, datos obtenidos, prueba positiva o
	 *         negativa fallida 3. Verbo exitoso, datos no obtenidos. 5. Error
	 *         de resquest con verbo identificado. 6. Error inesperado.
	 * @throws Exception
	 */
	public int validarListRecords(String metadataPrefix, String baseUrl,
			String strShortName, Element elementoResultado, String strProtocol,
			Element elementoDetallesSyntax, Element elementoDetallesSemantica)
			throws Exception {
		String stOutput = null;
		String stUrl = null;
		boolean exito = false;
		// outputMessage="";
		Integer codigoResultado = 0;
		Element elementoVerbo = new Element("ListRecords");
		elementoResultado.addContent(elementoVerbo);
		Element elementoDetalles = new Element("details");

		stUrl = formarURL(metadataPrefix, baseUrl, "ListRecords");
		stOutput = downloadDataFromURL(stUrl);
		establecerMensaje(stOutput);

		if (outputMessage.equals("request successfull")) {
			codigoResultado = 1;
			elementoDetalles.addContent(new Element("detail")
					.setText("HTTP 200 OK!"));
			System.out.println("		HTTP 200             OK!");
			Document doc = null;

			doc = validarEstandares(baseUrl, metadataPrefix, stOutput,
					strProtocol, strShortName, elementoDetallesSyntax,
					elementoDetallesSemantica);

			Element raiz = doc.getRootElement();
			Namespace namespace = raiz.getNamespace();
			Element listRecords = raiz.getChild("ListRecords", namespace);
			if (listRecords != null) {
				List<Element> records = listRecords.getChildren();
				if (records.size() == 0) {
					elementoDetalles
							.addContent(new Element("detail")
									.setText("El verbo ListRecords no obtuvo registros con ese particular formato de metadatos. "));
					System.out
							.println("		/El verbo ListRecords no obtuvo registros con ese particular formato de metadatos.");
					codigoResultado = 3;
				} else {
					Iterator<Element> iterator = records.iterator();
					int contadorReg = 0;
					while (iterator.hasNext()) {
						contadorReg++;
						Element record = iterator.next();
						Element header = record.getChild("header", namespace);
						if (record.getChildren().size() != 0
								&& (header.getAttributes().size() == 0))
							identifier = header.getChild("identifier",
									namespace).getText();
					}
					// elementoDetalles.addContent(new
					// Element("detail").setText(contadorReg+" registros encontrados."));
					elementoDetalles.addContent(new Element("detail")
							.setText("Identifier de un registro obtenido!"));
					// System.out.println("		/" + contadorReg+
					// " registros encontrados" );
					System.out.println("		/Identifier de un registro obtenido");

				}

			} else {
				elementoDetalles
						.addContent(new Element("detail")
								.setText("El verbo ListRecords no devuelve la salida correcta."));
				System.out
						.println("		/El verbo ListRecords no devuelve la salida correcta.");
				codigoResultado = 3;
			}
			exito = pruebaNegativa("ListRecords", baseUrl);

			if (!exito) {
				codigoResultado = 2;
				elementoDetalles.addContent(new Element("detail")
						.setText("Prueba negativa al verbo fallida."));
				System.out.println("		/Prueba negativa al verbo fallida.");
			}

		} else
			codigoResultado = establecerCodigoResultado(elementoDetalles);

		elementoVerbo.addContent(new Element("resultCode")
				.setText(codigoResultado.toString()));
		elementoVerbo
				.addContent(new Element("codeMessage")
						.setText(obtenerMensajeCodigo(mapeadoCodigos,
								codigoResultado)));
		elementoVerbo.addContent(elementoDetalles);
		System.out.println("Codigo Resultado : " + codigoResultado);
		return codigoResultado;
	}

	/**
	 * Valida el verbo Identifier para el protocolo OAI
	 * 
	 * @param metadataPrefix
	 * @param baseUrl
	 * @param dataProvider
	 * @return codigoResultado 1. Validaci_n Exitosa, prueba positiva y negativa
	 *         exitosa. 2. Verbo exitoso, datos obtenidos, prueba positiva o
	 *         negativa fallida 3. Verbo exitoso, datos no obtenidos. 5. Error
	 *         de resquest con verbo identificado. 6. Error inesperado.
	 * @throws Exception
	 */
	public int validarListMetadataFormats(String baseUrl,
			Element elementoResultado) throws Exception {
		String stOutput = null;
		String stUrl = null;
		boolean exito = false;

		/* Agregamos la jerarquia del documento */
		Element elementoVerbo = new Element("ListMetadataFormats");
		elementoResultado.addContent(elementoVerbo);
		Element elementoDetalles = new Element("details");

		// outputMessage="";
		Integer codigoResultado = 0;
		stUrl = formarURL("", baseUrl, "ListMetadataFormats");
		stOutput = downloadDataFromURL(stUrl);
		establecerMensaje(stOutput);

		if (outputMessage.equals("request successfull")) {
			codigoResultado = 1;
			elementoDetalles.addContent(new Element("detail")
					.setText("HTTP 200 OK!"));
			System.out.println("		HTTP 200             OK!");
			SAXBuilder sb = new SAXBuilder();
			Document doc = sb.build(new StringReader(stOutput));
			Element raiz = doc.getRootElement();
			Namespace namespace = raiz.getNamespace();
			Element ListMetadataFormats = raiz.getChild("ListMetadataFormats",
					namespace);
			if (ListMetadataFormats != null) {
				List<Element> formats = ListMetadataFormats.getChildren();
				if (formats.size() == 0
						|| (stOutput.equalsIgnoreCase("noMetadataFormats"))) {
					elementoDetalles
							.addContent(new Element("detail")
									.setText("El repositorio no soporta ningun formato."));
					System.out
							.println("		/El repositorio no soporta ningun formato.");
					codigoResultado = 3;
				} else {
					Iterator<Element> iterator = formats.iterator();
					while (iterator.hasNext()) {
						Element metadataFormat = iterator.next();
						if (metadataFormat
								.getChild("metadataPrefix", namespace) != null) {
							elementoDetalles
									.addContent(new Element("detail")
											.setText("Formato soportado(metadataPrefix): "
													+ metadataFormat.getChild(
															"metadataPrefix",
															namespace)
															.getText()));
							System.out.println("		/metadataPrefix: "
									+ metadataFormat.getChild("metadataPrefix",
											namespace).getText());
						} else {
							elementoDetalles
									.addContent(new Element("detail")
											.setText("El xml de salida no esta bien formado."));
							System.out
									.println("		/El xml de salida no esta bien formado.");
							codigoResultado = 3;
						}
					}
				}

			} else {
				elementoDetalles
						.addContent(new Element("detail")
								.setText("El verbo ListMetadataFormats no devuelve la salida correcta."));
				System.out
						.println("		/El verbo ListMetadataFormats no devuelve la salida correcta.");
				codigoResultado = 3;
			}
			exito = pruebaNegativa("ListMetadataFormats", baseUrl);

			if (!exito) {
				codigoResultado = 2;
				elementoDetalles.addContent(new Element("detail")
						.setText("Prueba negativa al verbo fallida."));
				System.out.println("		/Prueba negativa al verbo fallada.");
			}
		} else
			codigoResultado = establecerCodigoResultado(elementoDetalles);

		elementoVerbo.addContent(new Element("outputCode")
				.setText(codigoResultado.toString()));
		elementoVerbo
				.addContent(new Element("codeMessage")
						.setText(obtenerMensajeCodigo(mapeadoCodigos,
								codigoResultado)));
		elementoVerbo.addContent(elementoDetalles);

		System.out.println("Codigo Resultado : " + codigoResultado);
		return codigoResultado;
	}

	/**
	 * Valida el verbo Identifier para el protocolo OAI
	 * 
	 * @param metadataPrefix
	 * @param baseUrl
	 * @param dataProvider
	 * @return codigoResultado 1. Validaci_n Exitosa, prueba positiva y negativa
	 *         exitosa. 2. Verbo exitoso, datos obtenidos, prueba positiva o
	 *         negativa fallida 3. Verbo exitoso, datos no obtenidos. 5. Error
	 *         de resquest con verbo identificado. 6. Error inesperado.
	 * @throws Exception
	 */
	public int validarGetRecord(String metadataPrefix, String baseUrl,
			String identifier, Element elementoResultado) throws Exception {
		String stOutput = null;
		String stUrl = null;
		boolean exito = false;
		Integer codigoResultado = 0;

		/* Agregamos la jerarquia del documento */
		Element elementoVerbo = new Element("GetRecord");
		elementoResultado.addContent(elementoVerbo);
		Element elementoDetalles = new Element("details");

		stUrl = formarURL(metadataPrefix, baseUrl, "GetRecord");
		stOutput = downloadDataFromURL(stUrl);
		establecerMensaje(stOutput);

		if (outputMessage.equals("request successfull")) {
			codigoResultado = 1;
			elementoDetalles.addContent(new Element("detail")
					.setText("HTTP 200 OK!"));
			System.out.println("		HTTP 200             OK!");
			SAXBuilder sb = new SAXBuilder();
			Document doc = sb.build(new StringReader(stOutput));
			Element raiz = doc.getRootElement();
			Namespace namespace = raiz.getNamespace();
			Element getRecord = raiz.getChild("GetRecord", namespace);
			if (getRecord != null) {
				Element record = getRecord.getChild("record", namespace);
				if (record == null) {
					elementoDetalles
							.addContent(new Element("detail")
									.setText("No se obtiene record con identifier de prueba"));
					System.out
							.println("		/No se obtiene record con identifier de prueba");
					codigoResultado = 3;
				} else {
					Element header = record.getChild("header", namespace);
					Element metadata = record.getChild("header", namespace);
					if (header != null) {
						elementoDetalles.addContent(new Element("detail")
								.setText("Encabezado del registro obtenido."));
						System.out
								.println("		/Encabezado del registro obtenido.");
						if (metadata != null) {
							elementoDetalles
									.addContent(new Element("detail")
											.setText("Metadatos del registro obtenido."));
							System.out
									.println("		/Metadatos del registro obtenido.");
						} else {
							elementoDetalles
									.addContent(new Element("detail")
											.setText("Metadatos del registro no obtenidos."));
							System.out
									.println("		/Metadatos del registro no obtenidos.");
						}
					} else {
						elementoDetalles
								.addContent(new Element("detail")
										.setText("Encabezado del registro no obtenido."));
						System.out
								.println("		/Encabezado del registro no obtenido.");
					}

				}

			} else {
				elementoDetalles
						.addContent(new Element("detail")
								.setText("El verbo getRecord no obtiene la salida esperada."));
				System.out
						.println("		/El verbo getRecord no obtiene la salida esperada.");
				codigoResultado = 3;
			}
			exito = pruebaNegativa("GetRecord", baseUrl);

			if (!exito) {
				codigoResultado = 2;
				elementoDetalles.addContent(new Element("detail")
						.setText("Prueba negativa al verbo fallida."));
				System.out.println("		/Prueba negativa al verbo fallida.");
			}
		} else
			codigoResultado = establecerCodigoResultado(elementoDetalles);

		elementoVerbo.addContent(new Element("outputCode")
				.setText(codigoResultado.toString()));
		elementoVerbo
				.addContent(new Element("codeMessage")
						.setText(obtenerMensajeCodigo(mapeadoCodigos,
								codigoResultado)));
		elementoVerbo.addContent(elementoDetalles);
		System.out.println("Codigo Resultado : " + codigoResultado);
		return codigoResultado;
	}

	/**
	 * Valida el verbo Identifier para el protocolo OAI
	 * 
	 * @param metadataPrefix
	 * @param baseUrl
	 * @param dataProvider
	 * @return codigoResultado 1. Validaci_n Exitosa, prueba positiva y negativa
	 *         exitosa. 2. Verbo exitoso, datos obtenidos, prueba positiva o
	 *         negativa fallida 3. Verbo exitoso, datos no obtenidos. 5. Error
	 *         de resquest con verbo identificado. 6. Error inesperado.
	 * @throws Exception
	 */
	public int validarListSets(String metadataPrefix, String baseUrl,
			Element elementoResultado) throws Exception {
		String stOutput = null;
		String stUrl = null;
		boolean exito = false;
		Integer codigoResultado = 0;

		Element elementoVerbo = new Element("ListSets");
		elementoResultado.addContent(elementoVerbo);
		Element elementoDetalles = new Element("details");

		stUrl = formarURL("", baseUrl, "ListSets");
		stOutput = downloadDataFromURL(stUrl);

		establecerMensaje(stOutput);

		if (outputMessage.equals("request successfull")) {
			codigoResultado = 1;
			elementoDetalles.addContent(new Element("detail")
					.setText("HTTP 200 OK!"));
			System.out.println("		HTTP 200             OK!");
			SAXBuilder sb = new SAXBuilder();
			Document doc = sb.build(new StringReader(stOutput));
			Element raiz = doc.getRootElement();
			Namespace namespace = raiz.getNamespace();
			Element ListSets = raiz.getChild("ListSets", namespace);
			if (ListSets != null) {
				List<Element> sets = ListSets.getChildren();
				if (sets.size() == 0) {
					elementoDetalles
							.addContent(new Element("detail")
									.setText("El repositorio no tiene Sets establecidos."));
					System.out
							.println("		/El repositorio no tiene Sets establecidos.");

				} else {
					elementoDetalles.addContent(new Element("detail")
							.setText("Sets establecidos obtenidos."));
					System.out.println("		/Sets establecidos obtenidos.");
					Iterator<Element> iterator = sets.iterator();
					while (iterator.hasNext()) {
						Element set = iterator.next();
						if (set.getChild("setName", namespace) != null) {
							elementoDetalles
									.addContent(new Element("detail")
											.setText("Set: "
													+ set.getChild("setName",
															namespace)
															.getText()));
							System.out.println("		/	"
									+ set.getName()
									+ ": "
									+ set.getChild("setName", namespace)
											.getText());
						} else {
							elementoDetalles
									.addContent(new Element("detail")
											.setText("No es posible obtener la identificacion de cada set."));
							System.out
									.println("		/No es posible obtener la identificacion de cada set.");
						}
					}
				}

			} else {
				elementoDetalles
						.addContent(new Element("detail")
								.setText("El verbo ListSets no presenta un salida correcta"));
				System.out
						.println("		/El verbo ListSets no presenta un salida correcta.");
				codigoResultado = 3;
			}
			exito = pruebaNegativa("ListSets", baseUrl);

			if (!exito) {
				codigoResultado = 2;
				elementoDetalles.addContent(new Element("detail")
						.setText("Prueba negativa al verbo fallida."));
				System.out.println("		/Prueba negativa al verbo fallida.");
			}
		} else
			codigoResultado = establecerCodigoResultado(elementoDetalles);

		elementoVerbo.addContent(new Element("outputCode")
				.setText(codigoResultado.toString()));
		elementoVerbo
				.addContent(new Element("codeMessage")
						.setText(obtenerMensajeCodigo(mapeadoCodigos,
								codigoResultado)));
		elementoVerbo.addContent(elementoDetalles);
		System.out.println("Codigo Resultado : " + codigoResultado);
		return codigoResultado;
	}

	/**
	 * Retorna el mensaje adecuado dependiendo de la salida de la descarga
	 * 
	 * @param stOutput
	 */
	public void establecerMensaje(String stOutput) {

		if (outputMessage == null) {
			/*
			 * version 1.4.3: element.getText().isEmpty() version 2.0:
			 * element.getText() == null
			 */
			if (stOutput == null)
				outputMessage = "no data retrieved";
			else {
				outputMessage = "request successfull";
			}
		}

	}

	/**
	 * Establece el codigo de resultado para las posibles salidas de validaci_n
	 * de verbos
	 * 
	 * @param codigoResultado
	 * @return
	 */
	public int establecerCodigoResultado(Element elementoDetalles) {
		int codigoResultado = 0;
		if (outputMessage.equals("no data retrieved")) {
			elementoDetalles.addContent(new Element("detail")
					.setText("HTTP 200 OK!"));
			elementoDetalles.addContent(new Element("detail")
					.setText("Datos no obtenidos!"));
			System.out.println("		HTTP 200             OK!");
			System.out.println("		/Datos no obtenidos!");
			codigoResultado = 4;
		} else if (outputMessage.equalsIgnoreCase("request failed")) {
			elementoDetalles.addContent(new Element("detail")
					.setText("HTTP 500 ERROR!,Peticion fallida."));
			System.out.println("		HTTP 500             ERROR!");
			System.out.println("		/Peticion fallida.");
			codigoResultado = 5;
		} else if (outputMessage.equals("error ocurred")) {
			codigoResultado = 6;
			elementoDetalles
					.addContent(new Element("detail")
							.setText("Ocurrio un error inesperado en momento de prueba."));
			System.out
					.println("    	/Ocurrio un error inesperado en momento de prueba.");

		}
		return codigoResultado;
	}

	/**
	 * Realiza una prueba negativa con parametros erroneos para el verbo
	 * especificado
	 * 
	 * @param verb
	 * @param baseURL
	 * @return
	 */
	public boolean pruebaNegativa(String verb, String baseURL) {

		boolean exito = false;
		String stURL = null;
		String stOutput = null;

		if (verb.equals("Identify")) {
			stURL = baseURL + "?verb=" + verb + "&" + "metadataPrefix=test";
			stOutput = downloadDataFromURL(stURL);

		} else if (verb.equals("ListIdentifiers")) {
			stURL = baseURL + "?" + "verb" + "=" + verb + "&"
					+ "identifier=test";
			stOutput = downloadDataFromURL(stURL);
		} else if (verb.equals("ListMetadataFormats")) {
			stURL = baseURL + "?" + "verb" + "=" + verb + "&"
					+ "really_wrong_verb=test";
			stOutput = downloadDataFromURL(stURL);
		} else if (verb.equals("ListRecords")) {
			stURL = baseURL + "?" + "verb" + "=" + verb + "&"
					+ "identifier=test";
			stOutput = downloadDataFromURL(stURL);
		} else if (verb.equals("ListSets")) {
			stURL = baseURL + "?" + "verb" + "=" + verb + "&"
					+ "metadataPrefix=test";
			stOutput = downloadDataFromURL(stURL);
		} else if (verb.equals("GetRecord")) {
			stURL = baseURL + "?" + "verb" + "=" + verb + "&"
					+ "metadataPrefix=test";
			stOutput = downloadDataFromURL(stURL);
		}

		if (stOutput.contains("badArgument") || stOutput.contains("badVerb")
				|| stOutput.contains("idDoesNotExist")) {
			exito = true;
		}

		return exito;

	}

	/**
	 * Descarga un archivo XML a traves de una conexion URL
	 * 
	 * @param stUrl
	 *            URL a descargar
	 * @return String con el archivo descargado
	 * @throws Exception
	 */
	public String downloadDataFromURL(String stUrl) {
		// System.out.println("PRECREATINGURL");
		String stSalida = "";
		outputMessage = null;
		try {
			URL u = new URL(stUrl);
			// System.out.println("DOWNLOADDATA");
			/*
			 * SAXBuilder builder = new SAXBuilder();
			 * 
			 * // command line should offer URIs or file names try {
			 * builder.build(stUrl); // If there are no well-formedness errors,
			 * // then no exception is thrown System.out.println(stUrl +
			 * " is well-formed."); } // indicates a well-formedness error catch
			 * (JDOMException e) { System.out.println(builder.toString());
			 * System.out.println(stUrl + " is not well-formed.");
			 * System.out.println(e.getMessage()); }
			 */
			URLConnection uc = u.openConnection();
			uc.setReadTimeout(60000);
			String stSalidaVerbo = "";
			BufferedReader in = null;

			in = new BufferedReader(new InputStreamReader(uc.getInputStream(),
					"UTF-8"));

			StringBuilder sb = new StringBuilder();

			String tmp = null;
			char[] arrayChars;
			// System.out.println("ANTES DE DESCARGAR");
			while ((tmp = in.readLine()) != null) {
				arrayChars = tmp.toCharArray();
				for (int i = 0; i < arrayChars.length; i++) {
					if ((arrayChars[i] == 0x9)
							|| (arrayChars[i] == 0xA)
							|| (arrayChars[i] == 0xD)
							|| ((arrayChars[i] >= 0x20) && (arrayChars[i] <= 0xD7FF))
							|| ((arrayChars[i] >= 0xE000) && (arrayChars[i] <= 0xFFFD))
							|| ((arrayChars[i] >= 0x10000) && (arrayChars[i] <= 0x10FFFF)))
						sb.append(arrayChars[i]);
				}

			}
			stSalidaVerbo = sb.toString();

			// int indexOfEncoding = stSalidaVerbo.substring(0,
			// 100).toLowerCase().indexOf(("iso-8859-1"));

			// if(indexOfEncoding!=-1){
			if (stSalidaVerbo.substring(0, 100).toLowerCase()
					.contains("iso-8859-1")) {
				// System.out.println("Entre al encoding");
				uc = u.openConnection();
				uc.setReadTimeout(60000);
				stSalidaVerbo = "";
				in = null;

				in = new BufferedReader(new InputStreamReader(
						uc.getInputStream(), "ISO-8859-1"));

				sb = new StringBuilder();

				tmp = null;
				arrayChars = null;
				// System.out.println("ANTES DE DESCARGAR");
				while ((tmp = in.readLine()) != null) {
					arrayChars = tmp.toCharArray();
					for (int i = 0; i < arrayChars.length; i++) {
						if ((arrayChars[i] == 0x9)
								|| (arrayChars[i] == 0xA)
								|| (arrayChars[i] == 0xD)
								|| ((arrayChars[i] >= 0x20) && (arrayChars[i] <= 0xD7FF))
								|| ((arrayChars[i] >= 0xE000) && (arrayChars[i] <= 0xFFFD))
								|| ((arrayChars[i] >= 0x10000) && (arrayChars[i] <= 0x10FFFF)))
							sb.append(arrayChars[i]);
					}

				}

				stSalidaVerbo = sb.toString();
				byte[] utf8 = stSalidaVerbo.getBytes("UTF-8");
				stSalidaVerbo = new String(utf8);
				// stSalidaVerbo=parsearCaracteresTexto(stSalidaVerbo);
			}
			// }
			return stSalidaVerbo;

		} catch (IOException e) {
			outputMessage = "request failed";
		} catch (Exception e) {
			log.error(e);
			outputMessage = "error ocurred";
		}
		return stSalida; // Can be null or text

	}

	/**
	 * Realiza la recolecci_n de los archivos
	 * 
	 * @param entrada
	 *            Map.Entry<String, String> que contiene un path de descarga
	 *            (Key and Value)
	 * @param elementoDetalles
	 *            Elemento en jerarquia del documento de salida(.xml)
	 * @throws Exception
	 *             controlada en el metodo validarRepositorio(DataProvider dp,
	 *             Processor p)
	 * @return codigoResultado Integer con el valor que devuelve la validacion.
	 * @author Daniel Duque
	 * @since 1/03/2012
	 */

	@SuppressWarnings("unused")
	public Integer validarEntradaHTTP(String path,
			Element elementoDetallesProtocol, Element elementoDetallesSyntax,
			Element elementoDetallesSemantica, String strProtocol,
			String ShortName) throws Exception {
		// String filename = entrada.getKey();
		// String path = entrada.getValue();
		String stOutput = downloadDataFromURL(path);
		// stOutput =
		// stOutput.substring(0,stOutput.indexOf("DOCTYPE")-2)+stOutput.substring(stOutput.indexOf("DOCTYPE")+30,stOutput.length());

		// TODO: Solucionar dinamicamente el dtd
		/* SOLUCION TEMPORAL PROBLEMA DTD */
		// if(stOutput.contains(".dtd")){
		// stOutput =
		// stOutput.substring(0,stOutput.indexOf("DOCTYPE")-2)+stOutput.substring(stOutput.indexOf("DOCTYPE")+((stOutput.indexOf(".dtd")+6)-stOutput.indexOf("DOCTYPE")),stOutput.length());
		// }

		/* Remover .dtd */
		if (stOutput.contains(".dtd")) {
			stOutput = stOutput.substring(0, stOutput.indexOf("DOCTYPE") - 2)
					+ stOutput
							.substring(
									stOutput.indexOf("DOCTYPE")
											+ ((stOutput.indexOf(".dtd") + 6) - stOutput
													.indexOf("DOCTYPE")),
									stOutput.length());
		}

		/* Lograr transformacion de encoding a UTF-8 */
		if (stOutput.toLowerCase().contains("iso-8859-1")) {
			int indexEncoding = stOutput.indexOf("encoding=");
			int indexFinalEncoding = stOutput.indexOf("?>");
			String textoSalidaPreEncoding = stOutput.substring(0,
					(indexEncoding + 9));
			// System.out.println(textoSalidaPreEncoding+" primera parte!!");
			if (indexFinalEncoding != -1) {
				String textoSalidaAfterEncoding = stOutput.substring(
						(indexFinalEncoding), stOutput.length());
				// System.out.println(textoSalidaAfterEncoding.substring(0,250)+" segunda parte!!");
				stOutput = textoSalidaPreEncoding + "\"UTF-8\""
						+ textoSalidaAfterEncoding;
				// System.out.println("FULLLLLLLLL!!!"+textoSalida.substring(0,100));
			}
		}

		establecerMensaje(stOutput);
		Integer codigoResultado = null;
		if (outputMessage.equalsIgnoreCase("request successfull")) {
			codigoResultado = 1;
			elementoDetallesProtocol.addContent(new Element("detail")
					.setText("HTTP 200 OK!, archivo descargado con exito"));
			System.out.println("		/HTTP 200 OK!, archivo descargado con exito");
			Document doc = null;
			// try{
			doc = validarEstandares(baseUrl, metadataPrefix, stOutput,
					strProtocol, ShortName, elementoDetallesSyntax,
					elementoDetallesSemantica);
			// }catch(Exception e){
			//
			// SAXBuilder sb = new SAXBuilder();
			// /* Cegar al builder del DTD, no agregarlo */
			// sb.setFeature(
			// "http://apache.org/xml/features/nonvalidating/load-external-dtd",
			// false);
			// doc = sb.build(new StringReader(stOutput));
			// errorSintactico = true;
			// mensajeErrorSemantico += e.getMessage();
			// log.error(e);
			// System.out
			// .println("Error de semantica encontrado!");
			// }

			Element raiz = doc.getRootElement();
			Namespace namespace = raiz.getNamespace();
			if (raiz == null) {
				codigoResultado = 4;
				elementoDetallesProtocol
						.addContent(new Element("detail")
								.setText("La descarga es exitosa, pero no contiene datos."));
				System.out
						.println("La descarga es exitosa, pero no contiene datos.");
			} else {
				List rdfChildren = raiz.getChildren();
				if (rdfChildren.size() == 0) {
					codigoResultado = 4;
					elementoDetallesProtocol
							.addContent(new Element("detail")
									.setText("La descarga es exitosa, pero no contiene registros."));
					System.out
							.println("La descarga es exitosa, pero no contiene registros.");
				} else {
					elementoDetallesProtocol
							.addContent(new Element("detail")
									.setText("Numero de registros obtenidos en archivo: "
											+ rdfChildren.size()));
					System.out
							.println("		/Numero de registros obtenidos en archivo: "
									+ rdfChildren.size());
				}
			}

		} else {
			codigoResultado = establecerCodigoResultado(elementoDetallesProtocol);
		}
		return codigoResultado;
	}

	/**
	 * Retorn el valor del tag resumptionToken de un doc
	 * 
	 * @param doc
	 * @return resumptionToken or null if dont exist
	 * @author luis
	 */
	public String GetResumptionToken(Document doc) {
		String resumptionToken = null;
		Namespace ns = Namespace
				.getNamespace("http://www.openarchives.org/OAI/2.0/");

		Element raiz = doc.getRootElement();
		Element listRecords = raiz.getChild("ListRecords", ns);

		Element eResumptionToken = listRecords.getChild("resumptionToken", ns);

		if (eResumptionToken == null)
			return null;
		else
			resumptionToken = eResumptionToken.getText();

		if ((resumptionToken != null) && (resumptionToken.trim().length() != 0)) // aveces
																					// el
																					// resumptionToken
																					// viene
																					// vacio
																					// (el
																					// del
																					// ultimo
																					// doc)

			return resumptionToken;
		else
			return null;

	}

	/**
	 * Valida los tags con respecto a aquellos cargados desde los archivos XML.
	 * Si un registro es exitoso, sera escrito en otro documento que solo
	 * contendra aquellos que pasen la prueba.
	 * 
	 * @param metadataPrefix
	 * @param stOutput
	 * @param strProtocol
	 * @param elementoDetallesSemantica
	 * @return
	 * @throws JDOMException
	 * @throws IOException
	 * @throws SAXException
	 * @throws ParserConfigurationException
	 */
	public org.jdom2.Document validarEstandares(String baseUrl,
			String metadataPrefix, String stOutput, String strProtocol,
			String strShortName, Element elementoDetallesSyntax,
			Element elementoDetallesSemantica) throws JDOMException,
			IOException, SAXException, ParserConfigurationException {
		Document doc = null;
		Document docTemp = null;
		SAXBuilder builder = new SAXBuilder();
		String firstOutput = null;
		registrosRecorridos = 0;
		registroExitosoSemantica = true;

		/*
		 * Documento donde se guardan los cambios de los tags invalidos para
		 * posterior chequeo y mapeado
		 */
		Document docOutTextPatterns = builder.build(archivoTextPatterns);
		Element elementoRaiz = docOutTextPatterns.getRootElement();
		int succesSemantica = 0;
		firstOutput = stOutput;

		if (metadataPrefix.equalsIgnoreCase("oai_dc")) {

			if (strProtocol.equalsIgnoreCase("OAI")) {
				Namespace ns = Namespace
						.getNamespace("http://www.openarchives.org/OAI/2.0/");
				registrosExitososSemantica = 0;
				// AKI
				firstOutput = stOutput;
				doc = builder.build(new StringReader(stOutput));
				List<Element> SuperList = new ArrayList<Element>(); // aqqui se
																	// guardaran
																	// todos los
																	// records
																	// iterando
																	// por el
																	// resumptionToken,
																	// si este
																	// existe

				Element raiz = doc.getRootElement();
				Element listRecords = raiz.getChild("ListRecords", ns);
				List<Element> records = listRecords.getChildren(); // aqui esta
																	// la lista
																	// del
																	// primer
																	// xml

				SuperList.addAll(records);

				while (GetResumptionToken(doc) != null) { // en este while vamos
															// interando por
															// resumptiontoken,
															// para formar la
															// SuperList

					try {

						String resumptionToken = GetResumptionToken(doc);
						String NextURL = formarNextURL(resumptionToken, baseUrl);
						stOutput = downloadDataFromURL(NextURL);
						doc = builder.build(new StringReader(stOutput));
						raiz = doc.getRootElement();
						listRecords = raiz.getChild("ListRecords", ns);

						List<Element> recordsActual = listRecords.getChildren(
								"record", ns);
						// System.out.println("actual "+listRecords.getChildren("record",ns).size());

						SuperList.addAll(recordsActual);
						System.out.println("check in register number "
								+ SuperList.size());
					} catch (Exception e) {
						break; // / por si salta una exception durante la
								// iteracion sobre los listRecords no se pierda
								// las demas validaciones
					}
				}

				registrosTotales = SuperList.size();
				Iterator iterator = SuperList.iterator();

				HashMap<String, Semantic> Result = new HashMap<String, Semantic>(); // Aqui
																					// se
																					// guarda
																					// el
																					// Resultado
																					// de
																					// la
																					// validacion
																					// sintactica
				double TotalTags = 0;
				double TotalExito = 0;

				while (iterator.hasNext()) {

					String identifier = "";
					registrosRecorridos++;
					registroExitosoSemantica = true;

					Element record = (Element) iterator.next();

					Element header = record.getChild("header", ns);
					if (header == null) {
						System.out.println("Header no presente");

					} else {
						Element Eidentifier = header.getChild("identifier", ns);
						if (Eidentifier == null) {
							System.out.println("Identifier no presente");

						}
						identifier = Eidentifier.getText(); // para relacionar
															// los errores con
															// el identifier

					}

					/* Obtener metadata de cada elemento */
					Element metadata = record.getChild("metadata", ns);
					if (metadata != null && metadata.getChildren().size() != 0) {
						Element padreElementos = metadata.getChildren().get(0);

						// Aqui empieza el validador sintactico
						HashMap<String, Integer> MultiplicityActualRecord = new HashMap<String, Integer>();

						List tags = padreElementos.getChildren();

						for (Object i : tags) {
							Element tag = (Element) i;

							if (MultiplicityActualRecord.containsKey(tag
									.getName()))
								MultiplicityActualRecord.put(tag.getName(),
										MultiplicityActualRecord.get(tag
												.getName()) + 1);
							else
								MultiplicityActualRecord.put(tag.getName(), 1);
						}
						/*
						 * for(String i: MultiplicityActualRecord.keySet() ){
						 * System.out.println(i +" : "+
						 * MultiplicityActualRecord.get(i)); }
						 */

						for (String tag : tagsDcMultiplicity.keySet()) {
							boolean errorSintactico = false;
							semantic = new Semantic(tag);
							if (Result.containsKey(tag)) {
								semantic = Result.get(tag);
							}

							ArrayList<String> rules = tagsDcMultiplicity
									.get(tag);
							String multiplicity = rules.get(0);
							String mandatory = rules.get(1);
							// System.out.println(tag +" "+ rules.get(0)+
							// " "+rules.get(1));

							if (MultiplicityActualRecord.containsKey(tag)) {
								// tag presente en record actual
								if (multiplicity.equals("1")
										&& MultiplicityActualRecord.get(tag) > 1) {
									errorSintactico = true;
									semantic.errorMultiple.add(identifier);
								}

							} else {
								// tag NO presente en record actual
								if (mandatory.equals("1")) {
									errorSintactico = true;
									semantic.errorObligatorio.add(identifier);
								} else if (mandatory.equals("2")) {
									errorSintactico = true;
									semantic.warningRecomendado.add(identifier);
								}
							}
							if (errorSintactico) { // si hubo almenos un error
													// sintatico lo escribira
								Result.put(tag, semantic);
							}

						}
						Iterator iteratorRecord = padreElementos.getChildren()
								.iterator();

						// HERE

						int numOfTags = padreElementos.getChildren().size();
						TotalTags = TotalTags + numOfTags;
						int tagsExitosos = 0;

						while (iteratorRecord.hasNext()) {

							tagExitosoSemantica = true;

							Element element = (Element) iteratorRecord.next();

							validarSemanticaSintactica(element,
									elementoDetallesSyntax,
									elementoDetallesSemantica, elementoRaiz,
									strShortName, docOutTextPatterns,
									tagsDcValidation, tagsDcMultiplicity);

							/*
							 * Agregar estadisitca de registro exitoso si no
							 * ocurrio un error de validacion
							 */
							if (tagExitosoSemantica) {
								tagsExitosos++;
								TotalExito++;
							}

						}

						/*
						 * Agregar estadisitca de registro exitoso si no ocurrio
						 * un error de validacion
						 */
						if (registroExitosoSemantica) {
							// System.out.println("Fallo "+
							// registrosExitososSemantica);
							registrosExitososSemantica++;
						}

					} else {
						if (!record.getName().equalsIgnoreCase(
								"resumptionToken")) {
							elementoDetallesSyntax.addContent(new Element(
									"detail").setText("Registro "
									+ registrosRecorridos
									+ " no contiene metadatos"));

						}
					}

				} // fin while de Records

				//
				/*
				 * 
				 * System.out.println("TotalTags "+TotalTags);
				 * System.out.println("TotalExito "+TotalExito);
				 */

				int ScoreSemantica = (int) Math
						.round((TotalExito / TotalTags) * 100);

				elementoDetallesSemantica.setAttribute(new Attribute("score",
						ScoreSemantica + ""));
				elementoDetallesSyntax.setAttribute(new Attribute("success",
						"success")); // este atributo nos sirve para controlar
										// de que la validacion sintactica SI se
										// pudo hacer

				// AKI recorremos el Result de semantic y grabamos
				double Sumatoria = 0;
				for (String i : Result.keySet()) {
					Element Tag = new Element("Tag");
					Tag.setAttribute(new Attribute("name", i));

					Semantic actual = Result.get(i);
					String failType = "none"; // posibles valores: Mandatory,
												// Recommended, Multiple
					int errores = 0;

					// estos errores son mutuamente excluyentes

					if (actual.errorMultiple.size() != 0) {
						failType = "Multiple";
						errores = actual.errorMultiple.size();
					}
					if (actual.errorObligatorio.size() != 0) {
						failType = "Mandatory";
						errores = actual.errorObligatorio.size();
					}
					if (actual.warningRecomendado.size() != 0) {
						failType = "Recommended";
						errores = actual.warningRecomendado.size();
					}

					Sumatoria = Sumatoria
							+ (((Double.valueOf(registrosTotales - errores) / registrosTotales) * 100));

					Element ListIdentifier = new Element("ListIdentifier");
					ListIdentifier.setAttribute(new Attribute("error", ""
							+ errores));
					ListIdentifier.setAttribute(new Attribute("size", ""
							+ registrosTotales));
					ListIdentifier.setAttribute(new Attribute("failType",
							failType));

					int Max = 10; // Definimos un maximo de Identifiers para la
									// que no se agreguen todos
					int cont = 0; // variable contador para controlar que si
									// ingresen Max Identifiers

					// se recorrer los 3 pero realmente 2 tendran size 0, osea 1
					// solo tendra links para agregar a lista

					for (String identifier : actual.errorMultiple) {
						ListIdentifier.addContent(new Element("identifier")
								.setText(identifier));
						cont++;
						if (cont >= Max)
							break;
					}
					for (String identifier : actual.errorObligatorio) {
						ListIdentifier.addContent(new Element("identifier")
								.setText(identifier));
						cont++;
						if (cont >= Max)
							break;
					}

					for (String identifier : actual.warningRecomendado) {
						ListIdentifier.addContent(new Element("identifier")
								.setText(identifier));
						cont++;
						if (cont >= Max)
							break;
					}

					/*
					 * System.out.println(i);
					 * System.out.println(actual.errorMultiple.size());
					 * System.out.println(actual.errorObligatorio.size());
					 * System.out.println(actual.warningRecomendado.size());
					 */
					Tag.addContent(ListIdentifier);

					elementoDetallesSyntax.addContent(Tag);

				}
				int totalTags = tagsDcMultiplicity.size();
				int failTags = Result.size();
				int successTags = totalTags - failTags;

				Sumatoria = Sumatoria + successTags * 100; // los buenos siempre
															// seran 100%

				/*
				 * System.out.println("totalTags "+totalTags);
				 * System.out.println("failTags "+failTags);
				 * System.out.println("successTags "+successTags);
				 */

				int scoreSintaxis = (int) Math.round(Sumatoria / totalTags);
				elementoDetallesSyntax.setAttribute(new Attribute("success",
						"success")); // este atributo nos sirve para controlar
										// de que la validacion sintactica SI se
										// pudo hacer
				elementoDetallesSyntax.setAttribute(new Attribute("score",
						scoreSintaxis + ""));
				elementoDetallesSyntax.setAttribute(new Attribute("baseUrl",
						baseUrl)); // esta la necesito para luego formar los
									// links que apuntan a los records

			} else {

				registrosExitososSemantica = 0;

				doc = builder.build(new StringReader(stOutput));
				Element raiz = doc.getRootElement();
				List objects = raiz.getChildren();
				registrosTotales = objects.size();
				Iterator iterator = objects.iterator();
				while (iterator.hasNext()) {
					registrosRecorridos++;
					Element object = (Element) iterator.next();
					if (object.getChildren().size() != 0) {
						Iterator iteratorObject = object.getChildren()
								.iterator();
						Element element = (Element) iteratorObject.next();

						validarSemanticaSintactica(element,
								elementoDetallesSyntax,
								elementoDetallesSemantica, elementoRaiz,
								strShortName, docOutTextPatterns,
								tagsDcValidation, tagsDcMultiplicity);
					} else {
						elementoDetallesSyntax.addContent(new Element("detail")
								.setText("Registro " + registrosRecorridos
										+ " no contiene metadatos"));
					}

					/*
					 * Agregar estadisitca de registro exitoso si no ocurrio un
					 * error de validacion
					 */
					if (registroExitosoSemantica) {
						registrosExitososSemantica++;
					}

				}
			}
		} else if (metadataPrefix.equalsIgnoreCase("oai_lomco")) {

			/* Generar Sets para los HashMap's que contienen la informacion */
			// Set setValidacion = tagsLomCoValidation.entrySet();
			// Set setMultiplicidad = tagsLomCoMultiplicity.entrySet();

			registrosExitososSemantica = 0;

			doc = builder.build(new StringReader(stOutput));

			Element raiz = doc.getRootElement();
			List objects = raiz.getChildren();
			registrosTotales = objects.size();
			Iterator iterator = objects.iterator();

			HashMap<String, Semantic> Result = new HashMap<String, Semantic>(); // Aqui
																				// se
																				// guarda
																				// el
																				// Resultado
																				// de
																				// la
																				// validacion
																				// sintactica
			double TotalTags = 0;
			double TotalExito = 0;

			while (iterator.hasNext()) {
				registrosRecorridos++;

				Element object = (Element) iterator.next();

				// Aqui empieza el validador sintactico
				HashMap<String, Integer> MultiplicityActualRecord = new HashMap<String, Integer>();

				List tags = object.getChildren();
				String location = object.getChildText("location");

				for (Object i : tags) {
					Element tag = (Element) i;

					if (MultiplicityActualRecord.containsKey(tag.getName()))
						MultiplicityActualRecord
								.put(tag.getName(), MultiplicityActualRecord
										.get(tag.getName()) + 1);
					else
						MultiplicityActualRecord.put(tag.getName(), 1);
				}
				/*
				 * for(String i: MultiplicityActualRecord.keySet() ){
				 * System.out.println(i +" : "+
				 * MultiplicityActualRecord.get(i)); }
				 */

				for (String tag : tagsLomCoMultiplicity.keySet()) {
					boolean errorSintactico = false;
					semantic = new Semantic(tag);
					if (Result.containsKey(tag)) {
						semantic = Result.get(tag);
					}

					ArrayList<String> rules = tagsLomCoMultiplicity.get(tag);
					String multiplicity = rules.get(0);
					String mandatory = rules.get(1);

					if (MultiplicityActualRecord.containsKey(tag)) {
						// tag presente en record actual
						if (multiplicity.equals("1")
								&& MultiplicityActualRecord.get(tag) > 1) {
							// System.out.println("error multiple");
							errorSintactico = true;
							semantic.errorMultiple.add(location);
						}

					} else {
						// tag NO presente en record actual
						if (mandatory.equals("1")) {
							// System.out.println("error obligatorio");
							errorSintactico = true;
							semantic.errorObligatorio.add(location);
						} else if (mandatory.equals("2")) {
							// System.out.println("error recomendado");
							errorSintactico = true;
							semantic.warningRecomendado.add(location);
						}
					}
					if (errorSintactico) { // si hubo almenos un error sintatico
											// lo escribira
						// System.out.println("tag fallo "+tag);

						Result.put(tag, semantic);
					}

					if (object.getChildren().size() != 0) {
						/* Obtener los metadatos del elemento */

						// HERE
						Iterator iteratorObject = object.getChildren()
								.iterator();

						int numOfTags = object.getChildren().size();
						TotalTags = TotalTags + numOfTags;
						int tagsExitosos = 0;

						while (iteratorObject.hasNext()) {

							Element element = (Element) iteratorObject.next();

							tagExitosoSemantica = true;
							validarSemanticaSintactica(element,
									elementoDetallesSyntax,
									elementoDetallesSemantica, elementoRaiz,
									strShortName, docOutTextPatterns,
									tagsLomCoValidation, tagsLomCoMultiplicity);

							/*
							 * Agregar estadisitca de registro exitoso si no
							 * ocurrio un error de validacion
							 */
							if (tagExitosoSemantica) {
								tagsExitosos++;
								TotalExito++;
							}

						}
						// System.out.println("tagsExitosos  "+tagsExitosos+" de "+numOfTags);

					} else {
						/*
						 * Error de sintaxis, elemento no contiene metadatos,
						 * registrar en el xml
						 */
						elementoDetallesSyntax.addContent(new Element("detail")
								.setText("Registro " + registrosRecorridos
										+ " no contiene metadatos"));
						errorSintactico = true;
					}

				}

			}
			/*
			 * System.out.println("TotalTags "+TotalTags);
			 * System.out.println("TotalExito "+TotalExito);
			 */

			int ScoreSemantica = (int) Math
					.round((TotalExito / TotalTags) * 100);

			elementoDetallesSemantica.setAttribute(new Attribute("score",
					ScoreSemantica + ""));
			elementoDetallesSyntax.setAttribute(new Attribute("success",
					"success")); // este atributo nos sirve para controlar de
									// que la validacion sintactica SI se pudo
									// hacer

			// AKI recorremos el Result de semantic y grabamos
			double Sumatoria = 0;

			for (String i : Result.keySet()) {
				Element Tag = new Element("Tag");
				Tag.setAttribute(new Attribute("name", i));

				Semantic actual = Result.get(i);
				String failType = "none"; // posibles valores: Mandatory,
											// Recommended, Multiple
				int errores = 0;

				// estos errores son mutuamente excluyentes

				if (actual.errorMultiple.size() != 0) {
					failType = "Multiple";
					errores = actual.errorMultiple.size();
				}
				if (actual.errorObligatorio.size() != 0) {
					failType = "Mandatory";
					errores = actual.errorObligatorio.size();
				}
				if (actual.warningRecomendado.size() != 0) {
					failType = "Recommended";
					errores = actual.warningRecomendado.size();
				}

				// System.out.println("errores "+errores);
				Sumatoria = Sumatoria
						+ (((Double.valueOf(registrosTotales - errores) / registrosTotales) * 100));

				Element ListIdentifier = new Element("ListIdentifier");
				ListIdentifier
						.setAttribute(new Attribute("error", "" + errores));
				ListIdentifier.setAttribute(new Attribute("size", ""
						+ registrosTotales));
				ListIdentifier
						.setAttribute(new Attribute("failType", failType));

				int Max = 10; // Definimos un maximo de Identifiers para la que
								// no se agreguen todos
				int cont = 0; // variable contador para controlar que si
								// ingresen Max Identifiers

				// se recorrer los 3 pero realmente 2 tendran size 0, osea 1
				// solo tendra links para agregar a lista

				for (String identifier : actual.errorMultiple) {
					ListIdentifier.addContent(new Element("identifier")
							.setText(identifier));
					cont++;
					if (cont >= Max)
						break;
				}
				for (String identifier : actual.errorObligatorio) {
					ListIdentifier.addContent(new Element("identifier")
							.setText(identifier));
					cont++;
					if (cont >= Max)
						break;
				}

				for (String identifier : actual.warningRecomendado) {
					ListIdentifier.addContent(new Element("identifier")
							.setText(identifier));
					cont++;
					if (cont >= Max)
						break;
				}

				Tag.addContent(ListIdentifier);

				elementoDetallesSyntax.addContent(Tag);

			}
			int totalTags = tagsLomCoMultiplicity.size();
			int failTags = Result.size();
			int successTags = totalTags - failTags;

			Sumatoria = Sumatoria + successTags * 100; // los buenos siempre
														// seran 100%

			/*
			 * System.out.println("totalTags "+totalTags);
			 * System.out.println("failTags "+failTags);
			 * System.out.println("successTags "+successTags);
			 */

			int scoreSintaxis = (int) Math.round(Sumatoria / totalTags);
			// System.out.println("Sumatoria "+ Sumatoria);
			// System.out.println("scoreSintaxis "+scoreSintaxis);
			elementoDetallesSyntax.setAttribute(new Attribute("success",
					"success")); // este atributo nos sirve para controlar de
									// que la validacion sintactica SI se pudo
									// hacer
			elementoDetallesSyntax.setAttribute(new Attribute("score",
					scoreSintaxis + ""));
			elementoDetallesSyntax.setAttribute(new Attribute("baseUrl",
					baseUrl)); // esta la necesito para luego formar los links
								// que apuntan a los records

		} else if (metadataPrefix.equalsIgnoreCase("oai_lom")) {

			/*
			 * No completamente implementado 1. Necesario refinar el recolector
			 * para generar salida unificada con los otros dos metadatos, se
			 * realiza parcialmente. 2. Validar mediante JDOM.
			 */
			builder = new SAXBuilder("org.apache.xerces.parsers.SAXParser",
					true);
			/* Cegar al builder del DTD, no agregarlo */
			builder.setFeature(
					"http://apache.org/xml/features/nonvalidating/load-external-dtd",
					false);
			builder.setFeature(
					"http://apache.org/xml/features/validation/schema", true);
			builder.setValidation(true);
			builder.setProperty(
					"http://apache.org/xml/properties/schema/external-noNamespaceSchemaLocation",
					OAI_LOM_SCHEMA);

		}

		doc = builder.build(new StringReader(firstOutput));
		return doc;
	}

	/**
	 * 
	 * @param element
	 * @param elementoDetallesSyntax
	 * @param elementoDetallesSemantica
	 * @param elementoRaiz
	 * @param strShortName
	 * @param docOutTextPatterns
	 * @throws IOException
	 * @throws JDOMException
	 */

	public void validarSemanticaSintactica(Element element,
			Element elementoDetallesSyntax, Element elementoDetallesSemantica,
			Element elementoRaiz, String strShortName,
			org.jdom2.Document docOutTextPatterns,
			HashMap<String, ArrayList<String>> tagsValidation,
			HashMap<String, ArrayList<String>> hMultiplicity)
			throws JDOMException, IOException {

		/* Verificar que el elemento no tenga jerarquia */
		if (element.getChildren().size() == 0) {

			if (!tagsValidation.containsKey(element.getName().toLowerCase())) {
				// elementoDetallesSyntax.addContent(new
				// Element("detail").setText("Elemento no permitido encontrado: "+element.getName().toUpperCase()+" en registro "+registrosRecorridos));//comentariado
				// por luis
				errorSemantico = true;

			} else {

				/* Validacion de vocabulario controlado */
				// TODO: SET DE MULTIPLICIDAD Y VALIDATION
				/*
				 * Cargar el array con todos los vocabularios controlados para
				 * el verbo especifico(value)
				 */
				ArrayList<String> vocabulario = tagsValidation.get(element
						.getName());

				/* Verificar que este elemento contiene vocabulario controlado */
				if (vocabulario != null) {

					/*
					 * System.out.println("tag "+ element.getName()); for(String
					 * i : vocabulario){
					 * 
					 * System.out.println(i); }
					 */

					/*
					 * Verificar si el texto del elemento se encuentra en el
					 * vocabulario controlado
					 */
					if (!vocabulario.contains(element.getText().toUpperCase())) {

						/* No se encuentra, informar error en el xml de salida */
						// AKI
						// elementoDetallesSemantica.addContent(new
						// Element("detail").setText("Vocabulario no conocido encontrado en elemento "+
						// element.getName().toUpperCase() +" del registro "
						// +registrosRecorridos + " : "+element.getText()));
						// System.out.println("Vocabulario no conocido encontrado en elemento "+
						// element.getName().toUpperCase() +" del registro "
						// +registrosRecorridos + " : "+element.getText());
						errorSemantico = true;
						registroExitosoSemantica = false;
						tagExitosoSemantica = false;

						/* Gestion del archivo TextPatterns.xml */
						boolean escribirEnDisco = insertarTextPatterns(
								elementoRaiz, strShortName, element.getName(),
								element.getText());
						/*
						 * Sobreescribir el archivo TextPatterns.xml si es
						 * necesario, se debe realizar inmediatamente para no
						 * tener varias instancias de la misma entrada
						 * Tag/OldText
						 */
						if (escribirEnDisco) {
							try {

								writeXML.writeXmlJdomFile(path + "/conf/",
										"TextPatterns.xml", docOutTextPatterns);
							} catch (Exception e) {
								System.out
										.println("Error intentando sobreescribir el archivo textPatterns.xml");
							}
						}
					}

					/*
					 * El texto del elemento esta acorde con el vocabulario
					 * controlado
					 */

				}
				// TODO: Validacion de multiplicidad

			}
		} else {
			elementoDetallesSyntax.addContent(new Element("detail")
					.setText("Jerarquia no permitida encontrada en elemento: "
							+ element.getName().toUpperCase()));
		}
	}

	/**
	 * Ingresa una entrada en el textPattern para el repositorio especifico.
	 * Primero buscara una concidencia para aquellos tag reportados como
	 * applyFor "ALL", si lo hace, tomara este patron y lo escribira como un
	 * nuevo patron para el repositorio especifico (applyFor "shortName"). Si no
	 * encuentra coindicidencias escribira un nuevo patron para el repositorio
	 * especifico asignando en el elemento newText el valor "general", que
	 * posteriormente podra ser modificado por entrada del usuario desde modo
	 * WEB invocando el metodo modificarTextPatterns(@params)
	 * 
	 * @param elementoRaiz
	 * @param nombreElemento
	 * @param textoElemento
	 * @throws IOException
	 * @throws JDOMException
	 */
	public boolean insertarTextPatterns(Element elementoRaiz,
			String nombreCortoRepositorio, String nombreElemento,
			String textoElemento) throws JDOMException, IOException {
		if (textoElemento.length() == 0) {
			// System.out.println(nombreElemento);
			textoElemento = "EMPTY"; // esto se lo pongo para que le muestre al
										// usuario que esta vacio el contenido
										// -luis
		}

		/* Obtener todos los elementos pattern */
		List patterns = elementoRaiz.getChildren();
		Element newPattern = null;
		/* Crear nuevo patron */
		newPattern = new Element("pattern");
		boolean newTextExistente = false;
		boolean escribirEnDisco = false;
		if (patterns.size() != 0) {

			/* Crear elementos caracteristicos para el nuevo patron */
			newPattern.addContent(new Element(APPLY_FOR)
					.setText(nombreCortoRepositorio.toLowerCase()));
			newPattern.addContent(new Element(TAG).setText(nombreElemento
					.toLowerCase()));
			newPattern.addContent(new Element(OLDTEXT).setText(textoElemento));

			Iterator i = patterns.iterator();
			while (i.hasNext()) {
				Element pattern = (Element) i.next();

				/*
				 * Validar si existe un patron para el tag especifico que aplica
				 * para TODOS (ALL) los repositorios
				 */
				if (pattern.getChildText(APPLY_FOR).equalsIgnoreCase(ALL)
						&& pattern.getChildText(TAG).equalsIgnoreCase(
								nombreElemento)
						&& pattern.getChildText(OLDTEXT).equalsIgnoreCase(
								textoElemento)) {
					/*
					 * Exite coincidencia, se agrega el newText a el nuevo
					 * patron a partir del newText del patron applyFor"ALL" con
					 * las caracteristicas especificas.
					 */
					newPattern.addContent(new Element(NEWTEXT).setText(pattern
							.getChildText(NEWTEXT)));
					escribirEnDisco = true;
					break;
				} else if (pattern.getChildText(APPLY_FOR).equalsIgnoreCase(
						nombreCortoRepositorio.toLowerCase())
						&& pattern.getChildText(TAG).equalsIgnoreCase(
								nombreElemento.toLowerCase())
						&& pattern.getChildText(OLDTEXT).equalsIgnoreCase(
								textoElemento)) {
					/*
					 * Este patron ya existe para el repositorio especifico
					 */
					newTextExistente = true;
					break;
				}
			}
			/*
			 * Valido si se encontro un patron con coincidencia, si no, agregara
			 * el ultimo elemento newText con el valor "general"
			 */
			if (!escribirEnDisco && !newTextExistente) {
				newPattern.addContent(new Element(NEWTEXT).setText(GENERAL));
				escribirEnDisco = true;
			}
		} else {
			/*
			 * Si no existe ningun patron, crear directamente para el
			 * repositorio especifico
			 */
			newPattern.addContent(new Element(APPLY_FOR)
					.setText(nombreCortoRepositorio));
			newPattern.addContent(new Element(TAG).setText(nombreElemento));
			newPattern.addContent(new Element(OLDTEXT).setText(textoElemento));
			newPattern.addContent(new Element(NEWTEXT).setText(GENERAL));
			escribirEnDisco = true;
		}
		/* Agregar el elemento, revisar que no existe previamente */
		if (escribirEnDisco) {
			// System.out.println("Escribiendo...."+newPattern.getChildText("tag")
			// +" "+newPattern.getChildText("oldText")+" "+newPattern.getChildText("newText"));
			if (!checkPatternIfExist(newPattern)) // si no existe el patron lo
													// agrega
				elementoRaiz.addContent(newPattern);
		} else {
			/* Liberar memoria */
			newPattern = null;
		}
		return escribirEnDisco;
	}

	/**
	 * Verifica si un patron existe o no, para no volverlo a escribir
	 * 
	 * @param Pattern
	 * @return boolean value if exist or doesn't
	 * @author luis
	 * @throws IOException
	 * @throws JDOMException
	 */
	public boolean checkPatternIfExist(Element PatternToCheck)
			throws JDOMException, IOException {

		SAXBuilder builder = new SAXBuilder();
		Document doc = builder.build(archivoTextPatterns);
		Element raiz = doc.getRootElement();
		List patterns = raiz.getChildren();

		if (patterns.size() != 0) {

			Iterator i = patterns.iterator();
			while (i.hasNext()) {
				Element pattern = (Element) i.next();

				if (PatternToCheck.getChildText("apply_for").equals(
						pattern.getChildText("apply_for"))
						&& PatternToCheck.getChildText("tag").equals(
								pattern.getChildText("tag"))
						&& PatternToCheck.getChildText("oldText").equals(
								pattern.getChildText("oldText"))
						&& PatternToCheck.getChildText("newText").equals(
								pattern.getChildText("newText"))) {
					// System.out.println("Patron Igual");
					return true;

				}

			}
		}

		return false;
	}

	/**
	 * Realiza la modificacion de patrones para un repositorio especifico a
	 * partir del input del usuario por medio WEB.
	 * 
	 * @param nombreCortoRepositorio
	 * @param tag
	 * @param oldText
	 * @param newText
	 * @throws Exception
	 * @author Luis
	 */
	public void modificarTextPatterns(String nombreCortoRepositorio,
			String tag, String oldText, String newText) throws Exception {

		SAXBuilder builder = new SAXBuilder();

		boolean escribirEnDisco = false;
		/* Obtener todos los elementos pattern */
		/*
		 * Documento donde se guardan los cambios de los tags invalidos para
		 * posterior chequeo y mapeado
		 */
		Document docOutTextPatterns = builder.build(archivoTextPatterns);
		Element elementoRaiz = docOutTextPatterns.getRootElement();
		List patterns = elementoRaiz.getChildren();

		if (patterns.size() != 0) {

			Iterator i = patterns.iterator();
			while (i.hasNext()) {
				Element pattern = (Element) i.next();

				/*
				 * Validar si existe un patron para el tag especifico que aplica
				 * para el repositorio especifico.
				 */
				if (nombreCortoRepositorio.equalsIgnoreCase(pattern
						.getChildText(APPLY_FOR))) {

					if (tag.equalsIgnoreCase(pattern.getChildText(TAG))) {

						if (oldText.equalsIgnoreCase(pattern
								.getChildText(OLDTEXT))) {
							pattern.removeChild(NEWTEXT);
							pattern.addContent(new Element(NEWTEXT)
									.setText(newText));
							pattern.setAttribute(new Attribute("autoMap",
									"true")); // este atributo nos indica que se
												// hara un autoMapeado
							escribirEnDisco = true;

						}
					}

				}

			}
		}
		if (escribirEnDisco) {
			try {
				// System.out.println("ANTES DE ESCRIBIR EN DISCO");
				writeXML.writeXmlJdomFile(path + "/conf/", "TextPatterns.xml",
						docOutTextPatterns);
			} catch (Exception e) {
				System.out
						.println("Error intentando sobreescribir el archivo textPatterns.xml");
			}
		}

	}

	/**
	 * Realiza la modificacion de patrones para un repositorio especifico a
	 * partir del input del usuario por medio WEB.
	 * 
	 * @param informacionDeMapeado
	 *            <Key> es el shortName del repositorio <ArrayList<String>>
	 *            contiene nombreElemento,viejoTexto,nuevoTexto(para ese
	 *            elemento)
	 */
	public void modificarTextPatterns(String nombreCortoRepositorio,
			HashMap<String, ArrayList<String>> informacionDeMapeado)
			throws Exception {

		SAXBuilder builder = new SAXBuilder();
		ArrayList<String> valueInformation = null;
		boolean escribirEnDisco = false;
		/* Obtener todos los elementos pattern */
		/*
		 * Documento donde se guardan los cambios de los tags invalidos para
		 * posterior chequeo y mapeado
		 */
		Document docOutTextPatterns = builder.build(archivoTextPatterns);
		Element elementoRaiz = docOutTextPatterns.getRootElement();
		List patterns = elementoRaiz.getChildren();

		if (patterns.size() != 0) {

			Iterator i = patterns.iterator();
			while (i.hasNext()) {
				Element pattern = (Element) i.next();

				/*
				 * Validar si existe un patron para el tag especifico que aplica
				 * para el repositorio especifico. Se compara con la KEY del
				 * HashMap
				 */
				if (nombreCortoRepositorio.equalsIgnoreCase(pattern
						.getChildText(APPLY_FOR))) {

					if (informacionDeMapeado.containsKey(pattern
							.getChildText(TAG))) {
						/*
						 * Obtener el value del HashMap asociado a la Key
						 * anterior(ArrayList)
						 */
						valueInformation = informacionDeMapeado.get(pattern
								.getChildText(TAG));
						/*
						 * Comparar en las posiciones 0 viejoTexto
						 * respectivamente y reemplazar el newText del patron
						 * con el valor en la posicion 1.
						 */
						if (valueInformation.get(0).equalsIgnoreCase(
								pattern.getChildText(OLDTEXT))) {
							pattern.removeChild(NEWTEXT);
							pattern.addContent(new Element(NEWTEXT)
									.setText(valueInformation.get(1)));
							escribirEnDisco = true;
						}
					}

				}

			}
		}
		if (escribirEnDisco) {
			try {
				System.out.println("ANTES DE ESCRIBIR EN DISCO");
				writeXML.writeXmlJdomFile(path + "/conf/", "TextPatterns.xml",
						docOutTextPatterns);
			} catch (Exception e) {
				System.out
						.println("Error intentando sobreescribir el archivo textPatterns.xml");
			}
		}

	}

	/**
	 * Realiza la identificacion de patrones para un repositorio especifico a
	 * que se encuentran con el atrubuto autoMap = true en el tag pattern en el
	 * archivo TextPatterns.xml, estos seran desplegados en modo WEB.
	 * 
	 * @param nombreCortoRepositorio
	 * @return List de Patterns
	 * @throws Exception
	 */
	public static List<Element> identificarAutoCorrecciones(
			String nombreCortoRepositorio) throws Exception {

		SAXBuilder builder = new SAXBuilder();

		/* Obtener todos los elementos pattern */
		/*
		 * Documento donde se guardan los cambios de los tags invalidos para
		 * posterior chequeo y mapeado
		 */
		Document docOutTextPatterns = builder.build(archivoTextPatterns);
		Element elementoRaiz = docOutTextPatterns.getRootElement();
		List patterns = elementoRaiz.getChildren();

		List ListAutoMap = new ArrayList<Element>();

		if (patterns.size() != 0) {

			Iterator i = patterns.iterator();
			while (i.hasNext()) {
				Element pattern = (Element) i.next();

				/* Buscar si el patron es del repositorio especifico */
				if (nombreCortoRepositorio.equalsIgnoreCase(pattern
						.getChildText(APPLY_FOR))) {
					String atributo = pattern.getAttributeValue("autoMap");
					if (atributo != null && atributo.equals("true")) {
						ListAutoMap.add(pattern);

					}

				}

			}
		}
		return ListAutoMap;

	}

	/**
	 * Realiza la identificacion de patrones para un repositorio especifico a
	 * que se encuentran con su newText con valor de 'general' en el archivo
	 * TextPatterns.xml, estos seran desplegados en modo WEB.
	 * 
	 * @param nombreCortoRepositorio
	 * @return HashMap<String,Vector<String>> Tag - oldtext1, oldtext2....
	 * @throws Exception
	 */
	public HashMap<String, Vector<String>> identificarPatrones(
			String nombreCortoRepositorio) throws Exception {

		SAXBuilder builder = new SAXBuilder();

		/* Obtener todos los elementos pattern */
		/*
		 * Documento donde se guardan los cambios de los tags invalidos para
		 * posterior chequeo y mapeado
		 */
		Document docOutTextPatterns = builder.build(archivoTextPatterns);
		Element elementoRaiz = docOutTextPatterns.getRootElement();
		List patterns = elementoRaiz.getChildren();

		HashMap<String, Vector<String>> variablesParaModificar = new HashMap<String, Vector<String>>();

		if (patterns.size() != 0) {

			Iterator i = patterns.iterator();
			while (i.hasNext()) {
				Element pattern = (Element) i.next();

				/* Buscar si el patron es del repositorio especifico */
				if (nombreCortoRepositorio.equalsIgnoreCase(pattern
						.getChildText(APPLY_FOR))) {

					if (pattern.getChildText(NEWTEXT).equalsIgnoreCase(GENERAL)) {
						/*
						 * Existe un elemento en el respectivo repositorio que
						 * contiene en newText el valor 'general'. Obtenemos el
						 * valor de TAG y OLDTEXT y lo agregamos al HashMap que
						 * retorna.
						 */
						Vector<String> update = new Vector<String>();
						if (variablesParaModificar.containsKey(pattern
								.getChildText(TAG))) {

							update = variablesParaModificar.get(pattern
									.getChildText(TAG));
						}
						update.add(pattern.getChildText(OLDTEXT));
						variablesParaModificar.put(pattern.getChildText(TAG),
								update);

					}

				}

			}
		}
		return variablesParaModificar;

	}

	/**
	 * 
	 * @param tag
	 * @return
	 * @throws Exception
	 */
	public ArrayList<String> obtenerVocabulario(String tag,
			String metadataPrefix) throws Exception {

		ArrayList<String> valueInformation = new ArrayList<String>();
		try {
			SAXBuilder builder = new SAXBuilder();
			Document docValidTags = null;
			/* Cargar archivo con los vocabularios controlados */
			if (metadataPrefix.equalsIgnoreCase("oai_lomco")) {
				docValidTags = builder.build(archivoValidLomCoTags);
			} else if (metadataPrefix.equalsIgnoreCase("oai_dc")) {
				docValidTags = builder.build(archivoValidDublinCoreTags);
			}

			Element elementoRaiz = docValidTags.getRootElement();
			List elementos = elementoRaiz.getChildren();

			if (elementos.size() != 0) {
				/* Recorrer todos los elementos en el documento */
				Iterator i = elementos.iterator();

				while (i.hasNext()) {

					Element elemento = (Element) i.next();
					if (elemento.getChildText("name").equalsIgnoreCase(tag)) {
						Element vocabularies = elemento.getChild("vocabulary");
						if (vocabularies != null) {
							Iterator iterador = vocabularies.getChildren()
									.iterator();
							while (iterador.hasNext()) {
								Element contenido = (Element) iterador.next();
								// System.out.println(contenido.getText());
								valueInformation.add(contenido.getText());
							}
						}
					}

				}
			}

		} catch (Exception e) {
			log.error(e);
		}
		return valueInformation;
	}

	public String obtenerMensajeCodigo(HashMap<Integer, String> mapeadoCodigos,
			Integer codigoResultado) {
		String mensajeCodigo = null;
		Set set = mapeadoCodigos.entrySet();
		Iterator im = set.iterator();
		while (im.hasNext()) {
			Map.Entry me = (Map.Entry) im.next();
			if (codigoResultado.equals(me.getKey())) {
				mensajeCodigo = (String) me.getValue();
				break;
			}
		}
		return mensajeCodigo;
	}

	/**
	 * Carga los posibles tipos de mensajes y codigos que genera una validacion
	 * 
	 * @throws Exception
	 *             en caso de error de carga
	 * @author daniel
	 */
	@SuppressWarnings("unchecked")
	private void loadCodigosMensajes() {
		try {
			SAXBuilder sb = new SAXBuilder();
			/* Crea documento con el archivo xml Archivo validationCodesMapping */
			Document documento = sb.build(archivoMapeadoCodigos);

			/*
			 * Obtiene elemento raiz con etiqueta <codes> y cada uno de los
			 * codigos, insertando la relacion en el HashMap mapeadoCodigos
			 */
			Element elementoRaiz = documento.getRootElement();
			List<Element> codes = elementoRaiz.getChildren();
			Iterator<Element> iterador = codes.iterator();
			while (iterador.hasNext()) {
				/* Adicionar los elementos al HashMap */
				Element code = iterador.next();
				Integer value = Integer.parseInt(code.getChild("value")
						.getText());
				this.mapeadoCodigos.put(value, code.getChild("message")
						.getText());
			}
		} catch (Exception e) {
			System.out
					.println("No fue posible cargar los codigos y los mensajes a el hash map");
		}
	}

	/**
	 * Testea una Url haber si trae contenido
	 * 
	 * @param url
	 * @return
	 * @author luis
	 */
	public boolean testUrl(String url) {

		try {
			String data = downloadDataFromURL(url);
			if (data != null && data.length() > 0)
				return true;
			return false;

		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}

	}

	public static void main(String[] args) throws Exception {

		Validator validator = new Validator("/Applications/REDA");

		/*
		 * List L = identificarAutoCorrecciones("ces-ri");
		 * 
		 * for (Object e : L) { Element pattern = (Element)e;
		 * 
		 * 
		 * String tag = pattern.getChildText("tag"); String oldText =
		 * pattern.getChildText("oldText"); String newText =
		 * pattern.getChildText("newText");
		 * 
		 * System.out.println(tag + " "+ oldText+ " "+ newText); }
		 * System.exit(0);
		 */
		boolean OAI = true;

		String stShortname = "unal-boa";
		String stUrlBase = "http://aplicaciones.virtual.unal.edu.co/drupal/files/objs.xml";
		String stProtocol = "HTTP";
		String stMetadataPrefix = "oai_lomco";
		if (OAI) {
			stShortname = "test_oai";
			stUrlBase = "http://www.iatreia.udea.edu.co/index.php/index/oai";
			stProtocol = "OAI";
			stMetadataPrefix = "oai_dc";
		}

		validator.validarRepositorio(stUrlBase, stShortname, stProtocol,
				stMetadataPrefix);

		/*
		 * String dl_home = null; String repname = null;
		 * 
		 * 
		 * if (args.length > 1) { dl_home = args[0]; repname = args[1]; } else {
		 * System.out.println("DL_HOME no especificado"); System.exit(0); } if
		 * (args.length > 2){ System.out.println("Demasiados argumentos ...");
		 * System.out.println("Utilizar: dl-validate [repname]");
		 * System.exit(0); } Validator validator= new Validator(dl_home);
		 * System.out.println("Iniciando Validador..."); String Repository[] =
		 * {repname}; validator.validarRepositorio(Repository); // if
		 * (repname.equals("all")) // validator.validarVerbos(); // else{ //
		 * String Repository[] = {repname}; //
		 * validator.validarVerbos(Repository); // } System.out.println(
		 * "\n******************Validador ha finalizado******************");
		 */
	}

}
