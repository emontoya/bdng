package org.bdng.harvmgr.process;

import org.bdng.harvmgr.harvester.DataProvider;

public class FileReady {

	private String xmlFormat;
	private String path;
	private String filename;
	private DataProvider dp;

	public FileReady(String xmlFormat, String path, String filename,
			DataProvider dp) {
		super();
		this.xmlFormat = xmlFormat;
		this.path = path;
		this.filename = filename;
		this.dp = dp;
	}

	public String getXmlFormat() {
		return xmlFormat;
	}

	public void setXmlFormat(String xmlFormat) {
		this.xmlFormat = xmlFormat;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public DataProvider getDp() {
		return dp;
	}

	public void setDp(DataProvider dp) {
		this.dp = dp;
	}

}
