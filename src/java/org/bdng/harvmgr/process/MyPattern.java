package org.bdng.harvmgr.process;

public class MyPattern {
	String oldPattern;
	String newPattern;
	String tag;
	String shortname;

	public MyPattern(String oldPattern, String newPattern, String shortname,
			String tag) {
		super();
		this.oldPattern = oldPattern;
		this.newPattern = newPattern;
		this.shortname = shortname;
		this.tag = tag;
	}

	public String getOldPattern() {
		return oldPattern;
	}

	public void setOldPattern(String oldPattern) {
		this.oldPattern = oldPattern;
	}

	public String getNewPattern() {
		return newPattern;
	}

	public void setNewPattern(String newPattern) {
		this.newPattern = newPattern;
	}

	public String getShortname() {
		return shortname;
	}

	public void setShortname(String shortname) {
		this.shortname = shortname;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}
}
