package org.bdng.harvmgr.process;

import org.jdom2.Element;
import org.jdom2.Namespace;

public class DmpDoer {
	Element dmp = null;

	Namespace ns_dmp = Namespace.getNamespace("dmp",
			"http://reda.net.co/doer/ns/1.0/");

	public DmpDoer(Element e) {
		dmp = e;
	}

	public String getTitle() {
		String result = "SIN_TITULO";
		if (dmp != null) {
			Element g = dmp.getChild("general", ns_dmp);
			if (g != null) {
				Element tp = g.getChild("titlePack", ns_dmp);
				if (tp != null) {
					Element t = tp.getChild("title", ns_dmp);
					if (t != null) {
						result = t.getText();
					}
				}
			}
		}
		return result;
	}

	public String getFirstId() {
		String result = "SIN_ID";
		if (dmp != null) {
			Element g = dmp.getChild("general", ns_dmp);
			if (g != null) {
				Element i = g.getChild("identifier", ns_dmp);
				if (i != null) {
					Element c = i.getChild("catalog", ns_dmp);
					if (c != null) {
						result = c.getText();
					}
				}
			}
		}
		return result;
	}

}
