package org.bdng.harvmgr.process;

/*
 * Jan 30, 2011
 *    se adiciono en procesarXmlOai y procesarXmlRdf el tag <institution> al archivo de salida.
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Vector;
import java.util.concurrent.Semaphore;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.bdng.harvmgr.dlConfig;
import org.bdng.harvmgr.harvester.DataProvider;
import org.bdng.harvmgr.harvester.Harvester;
import org.bdng.harvmgr.harvester.ServerConfXml;
import org.bdng.harvmgr.harvester.StatsManager;
import org.bdng.harvmgr.harvester.WriteXML;
import org.bdng.harvmgr.validator.ValidadorReda;
import org.bdng.harvmgr.validator.ValidatorDoer;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.Namespace;
import org.jdom2.Text;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

//import org.apache.poi.util.SystemOutLogger;

/**
 * Procesa los archivos XML recolectados para dar una serie de formatos y
 * utilidades
 * 
 * @author sebastian
 * 
 */
public class Processor implements Runnable {

	Logger log = Logger.getLogger(Processor.class);

	boolean certificacion = true; // con esta variable podemos prender o apagar
									// el proceso de certificacion- luis
	public final static String ALL = "all";
	String dlHome = null;
	dlConfig dlconfig = null;
	String dir_in = null;
	String archivoPatrones = null;
	String archivoBdngCols = null;
	String archivoValidDublinCoreTags = null;
	String archivoValidLomCoTags = null;
	String archivoValidDmpTags = null;
	String fileBlacklist = null;
	String fileWhitelist = null;
	String archivoCleanCharacters = null;
	private String archivoCaracteresCDATA = null;
	HashMap<String, Vector<MyPattern>> patterns = null;
	HashMap<String, Vector<MyPattern>> tagPatterns = null;
	String[] caracteresTildados = null;
	String[] caracteresNoTildados = null;
	public Queue<FileReady> fileReadyQueue = null;
	boolean continuar;
	public Semaphore signal = null;
	private String[] caracteresInvalidosCData = null;
	private String[] caracteresValidosCData = null;
	private int id;
	private WriteXML writeXml = new WriteXML();

	private String elementStr;
	private String docType;
	private String sLearningObjectType;
	private String sLinkIdentifier;
	private String sInstitution;
	private String sPublisher;
	private String sCreator;
	private String sDate;

	private String objectPurpose;
	private String objectDifficulty;
	private String sYear;
	private String lastStrInserted = "";
	private ArrayList<String> bdngcols = null;
	boolean isLom = false;
	boolean isDmp = false;
	boolean first = true;
	Element recordsOutputDCQ;

	ValidatorDoer valdoer = null;
	ValidadorReda valreda = null;
	
	Namespace ns = 		Namespace.getNamespace("http://www.openarchives.org/OAI/2.0/");
	Namespace ns_oai = 	Namespace.getNamespace("oai_dc",	"http://www.openarchives.org/OAI/2.0/oai_dc/");
	Namespace ns_oailom=Namespace.getNamespace("oai_lom", "http://ltsc.ieee.org/xsd/LOM");
	Namespace ns_oaidmp=Namespace.getNamespace("oai_dmp", "http://");
	Namespace ns_dc = 	Namespace.getNamespace("dc","http://purl.org/dc/elements/1.1/");
	Namespace ns_lom = 	Namespace.getNamespace("lom", "http://ltsc.ieee.org/xsd/LOM");
	Namespace ns_dmp = 	Namespace.getNamespace("dmp", "http://");
	Namespace ns_xsi = 	Namespace.getNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");

	/*
	 * Se crean HashMap's para guardar cada etiqueta con su respectivo arrayList
	 * de posibilidades de contenido
	 */
	private HashMap<String, ArrayList<String>> tagsLomCoValidation = new HashMap<String, ArrayList<String>>();
	private HashMap<String, ArrayList<String>> tagsDmpValidation = new HashMap<String, ArrayList<String>>();
	private HashMap<String, ArrayList<String>> tagsDcValidation = new HashMap<String, ArrayList<String>>();

	public Processor(int id) {
		dlconfig = new dlConfig();
		this.dlHome = dlconfig.getString("dl_home");
		init();
		this.id = id;
	}

	public Processor(String path, int id) {
		dlconfig = new dlConfig(path);
		this.dlHome = dlconfig.getString("dl_home");
		init();
		this.id = id;
	}

	public Processor(String path) {
		dlconfig = new dlConfig(path);
		this.dlHome = dlconfig.getString("dl_home");
		init();
		this.id = -1;
	}

	/**
	 * Inicializa las variables del processor
	 */
	private void init() {
		valdoer = new ValidatorDoer(this.dlHome + "/conf/xsd/doermp.xsd");
		valreda = new ValidadorReda(dlHome, this.dlHome
				+ "/conf/xsd/doermp.xsd", this.dlHome + "/logs/");
		continuar = true;
		signal = new Semaphore(0);
		patterns = new HashMap<String, Vector<MyPattern>>();
		tagPatterns = new HashMap<String, Vector<MyPattern>>();
		fileReadyQueue = new LinkedList<FileReady>();
		bdngcols = new ArrayList<String>();
		dir_in = dlconfig.getString("dl_metadata") + "/dbxmltemp";
		archivoPatrones = dlHome + "/conf/TextPatterns.xml";
		archivoBdngCols = dlHome + "/conf/BdngCollections.xml";
		archivoCleanCharacters = dlHome + "/conf/cleanCharacters.txt";
		archivoCaracteresCDATA = dlHome + "/conf/caracteresEspecialesCDATA.txt";
		// fileBlacklist = dlHome + "/conf/blacklist.xml";
		// fileWhitelist = dlHome + "/conf/whiteList.xml";

		fileBlacklist = dlconfig.getDlUrl() + "/rest//db/conf/whitelist.xml";
		fileWhitelist = dlconfig.getDlUrl() + "/rest//db/conf/blacklist.xml";

		/*
		 * String prueba = dlconfig.getDlUrl()+"/rest//db/conf/whitelist.xml";
		 * System.out.println("PRUEBAAAAAAAA "+prueba); System.exit(0);
		 */

		archivoValidDublinCoreTags = dlHome + "/conf/validDublinCoreTags.xml";
		archivoValidLomCoTags = dlHome + "/conf/validLomCoTags.xml";
		archivoValidDmpTags = dlHome + "/conf/validDmpTags.xml";

		try {
			loadVocabularioDc();
			loadVocabularioLomco();
			loadVocabularioDmp();

			loadTextPatterns();
			loadBdngCollections();
			loadCleanCharacters();
			loadCDataCharacters();

		} catch (Exception e) {
			log.error("Error cargando los patrones de texto o Vocabularios controlados");
			log.error(e);
		}
	}

	/**
	 * Carga el Vocabulario valido para DC
	 * 
	 * @throws JDOMException
	 * @throws IOException
	 * @author luis
	 */
	private void loadVocabularioDc() throws JDOMException, IOException {

		SAXBuilder sb = new SAXBuilder();
		Document documento = null;

		documento = sb.build(archivoValidDublinCoreTags);
		Element elementoRaiz = documento.getRootElement();

		List<Element> elementosHijos = elementoRaiz.getChildren();
		Iterator<Element> iterador = elementosHijos.iterator();

		while (iterador.hasNext()) {
			Element elemento = iterador.next();
			String nombreElemento = (elemento.getChild("name").getText());

			Element vocabulario = elemento.getChild("vocabulary");
			if (vocabulario != null) {
				ArrayList<String> vocabularioValido = new ArrayList<String>();
				List contenidos = vocabulario.getChildren();
				Iterator i = contenidos.iterator();
				while (i.hasNext()) {
					Element contenido = (Element) i.next();
					vocabularioValido.add(contenido.getText().toLowerCase());
				}
				this.tagsDcValidation.put(nombreElemento, vocabularioValido);
			}
		}

		elementosHijos = null;

	}

	/**
	 * Carga el Vocabulario valido para LomCo
	 * 
	 * @throws JDOMException
	 * @throws IOException
	 * @author luis
	 */
	private void loadVocabularioLomco() throws JDOMException, IOException {

		SAXBuilder sb = new SAXBuilder();
		Document documento = null;

		documento = sb.build(archivoValidLomCoTags);
		Element elementoRaiz = documento.getRootElement();

		List<Element> elementosHijos = elementoRaiz.getChildren();
		Iterator<Element> iterador = elementosHijos.iterator();

		while (iterador.hasNext()) {
			Element elemento = iterador.next();
			String nombreElemento = (elemento.getChild("name").getText());

			Element vocabulario = elemento.getChild("vocabulary");
			if (vocabulario != null) {
				ArrayList<String> vocabularioValido = new ArrayList<String>();
				List contenidos = vocabulario.getChildren();
				Iterator i = contenidos.iterator();
				while (i.hasNext()) {
					Element contenido = (Element) i.next();
					vocabularioValido.add(contenido.getText().toLowerCase());
				}
				this.tagsLomCoValidation.put(nombreElemento, vocabularioValido);
			}
		}

		elementosHijos = null;

	}

	/**
	 * Carga el Vocabulario valido para DMP
	 * 
	 * @throws JDOMException
	 * @throws IOException
	 * @author Edwin
	 */
	private void loadVocabularioDmp() throws JDOMException, IOException {

		SAXBuilder sb = new SAXBuilder();
		Document documento = null;

		documento = sb.build(archivoValidDmpTags);
		Element elementoRaiz = documento.getRootElement();

		List<Element> elementosHijos = elementoRaiz.getChildren();
		Iterator<Element> iterador = elementosHijos.iterator();

		while (iterador.hasNext()) {
			Element elemento = iterador.next();
			String nombreElemento = (elemento.getChild("name").getText());

			Element vocabulario = elemento.getChild("vocabulary");
			if (vocabulario != null) {
				ArrayList<String> vocabularioValido = new ArrayList<String>();
				List contenidos = vocabulario.getChildren();
				Iterator i = contenidos.iterator();
				while (i.hasNext()) {
					Element contenido = (Element) i.next();
					vocabularioValido.add(contenido.getText().toLowerCase());
				}
				this.tagsDmpValidation.put(nombreElemento, vocabularioValido);
			}
		}

		elementosHijos = null;

	}

	/**
	 * Carga los patrones de texto de archivos XML
	 * 
	 * @throws Exception
	 *             en caso de error de carga
	 * @author sebastian
	 */
	private void loadTextPatterns() throws Exception {
		SAXBuilder sb = new SAXBuilder();
		Document doc1 = sb.build(archivoPatrones);
		Element e1 = doc1.getRootElement();
		List listaPatronesTexto = e1.getChildren();
		Iterator<Element> i = listaPatronesTexto.iterator();
		while (i.hasNext()) {
			Element patron = i.next();
			String shortname = (patron.getChild("apply_for")).getText();
			String tag = (patron.getChild("tag")).getText();
			List patronViejo = patron.getChildren("oldText");
			Iterator j = patronViejo.iterator();
			String viejo = "";
			while (j.hasNext()) {
				viejo += ((Element) j.next()).getText();
			}
			List patronNuevo = patron.getChildren("newText");
			Iterator k = patronNuevo.iterator();
			String nuevo = "";
			while (k.hasNext()) {
				nuevo += ((Element) k.next()).getText();
			}
			MyPattern p = new MyPattern(viejo, nuevo, shortname, tag);
			if (tag.equals(ALL)) {
				Vector<MyPattern> v = patterns.get(shortname);
				if (v == null) {
					v = new Vector<MyPattern>();
					v.add(p);
					patterns.put(shortname, v);

				} else {
					v.add(p);
				}
			} else {
				Vector<MyPattern> v = tagPatterns.get(shortname);
				if (v == null) {
					v = new Vector<MyPattern>();
					v.add(p);
					tagPatterns.put(shortname, v);
				} else {
					v.add(p);
				}
			}
		}

	}

	/**
	 * Carga los posibles tipos de documentos en el ArrayList de Strings
	 * bdngCols
	 * 
	 * @throws Exception
	 *             en caso de error de carga
	 * @author daniel
	 */
	@SuppressWarnings("unchecked")
	private void loadBdngCollections() throws Exception {
		SAXBuilder sb = new SAXBuilder();
		/* Crea documento con el archivo xml Archivo BdngCollections */
		Document documento = sb.build(archivoBdngCols);

		/*
		 * Obtiene elemento raiz con etiqueta <dl> y el elemento con la etiqueta
		 * <collections> Se realiza el metodo getChildren() en reemplazo de
		 * getChild(String name) por si se cambia la estructura del documento
		 * XML.
		 */
		Element elementoRaiz = documento.getRootElement();

		List<Element> elementosHijos = elementoRaiz.getChildren();
		Iterator<Element> iterador = elementosHijos.iterator();
		Element elemento1 = iterador.next();

		/* Obtiene lista de los elementos bajo la etiqueta <collections> */
		List<Element> listaColecciones = elemento1.getChildren();
		Iterator<Element> i = listaColecciones.iterator();

		elementosHijos = null;
		elemento1 = null;
		/*
		 * Recorre todos los hijos del root element, en este caso con etiqueta
		 * <collection> y los agrega al ArrayList bdngCols
		 */
		while (i.hasNext()) {
			Element coleccion = i.next();
			String tipoColeccion = (coleccion.getChildText("name"));
			this.bdngcols.add(tipoColeccion);
		}

	}

	/**
	 * Carga el archivo de caracteres limpios
	 * 
	 * @throws Exception
	 */
	private void loadCleanCharacters() throws Exception {
		String archivoCaracteres = readFile(archivoCleanCharacters);
		String[] caracteres = archivoCaracteres.split("\n");
		caracteresTildados = new String[caracteres.length];
		caracteresNoTildados = new String[caracteres.length];
		for (int i = 0; i < caracteres.length; i++) {
			String[] tmp = caracteres[i].split("\t");
			caracteresTildados[i] = tmp[0];
			caracteresNoTildados[i] = tmp[1];
		}
	}

	/**
	 * Busca si un String par_metro se encuentra en el ArrayList bdngcols, que
	 * contiene los tipos de documentos admitidos
	 * 
	 * @param type
	 * @return found booleano indicando si se encontro el tipo de documento
	 * @author daniel
	 */
	private boolean findCollection(String type) {
		boolean found = false;
		found = bdngcols.contains(type);
		return found;
	}

	/**
	 * Lee un archivo
	 * 
	 * @param NomArch
	 *            el path del archivo
	 * @return texto leido
	 */
	public static String readFile(String NomArch) {
		java.io.FileReader fstream = null;
		StringBuffer strBuf = new StringBuffer();
		FileInputStream fin = null;
		try {
			fin = new FileInputStream(NomArch);
			InputStreamReader ir = new InputStreamReader(fin, "UTF-8");
			int c;
			int cont = 0;
			while ((c = ir.read()) != -1) {
				strBuf.append((char) c);
				cont += 1;
			}
			return strBuf.toString();
		} catch (java.io.IOException ex) {
			return "";
		} finally {
			if (fstream != null) {
				try {
					fstream.close();
					fin.close();
				} catch (java.io.IOException ex) {
					return "";
				}
			}
		}
	}

	/**
	 * Carga el archivo de caracteres especiales CDATA
	 */
	private void loadCDataCharacters() {
		String archivoCaracteres = readFile(archivoCaracteresCDATA);
		String[] caracteres = archivoCaracteres.split("\n");
		caracteresInvalidosCData = new String[caracteres.length];
		caracteresValidosCData = new String[caracteres.length];
		for (int i = 0; i < caracteres.length; i++) {
			String[] tmp = caracteres[i].split("\t");
			caracteresInvalidosCData[i] = tmp[0];
			caracteresValidosCData[i] = tmp[2];
		}
	}

	/**
	 * Explora los archivos de manera recursiva para procesar cada uno
	 */
	public void explore_dir() {
		File c, r, f, ins;
		String[] cols, insts, reps, files;
		String col, inst, rep, file;
		c = new File(dir_in);
		cols = c.list();
		for (int i = 0; i < cols.length; i++) {
			col = dir_in + "/" + cols[i];
			c = new File(col);
			if (c.isDirectory()) {
				insts = c.list();
				for (int m = 0; m < insts.length; m++) {
					inst = col + "/" + insts[m];
					ins = new File(inst);
					if (ins.isDirectory()) {
						reps = ins.list();
						for (int j = 0; j < reps.length; j++) {
							rep = col + "/" + insts[m] + "/" + reps[j];
							log.info("REP=" + rep);
							r = new File(rep);
							if (r.isDirectory()) {
								files = r.list();
								for (int k = 0; k < files.length; k++) {
									file = rep + "/" + files[k];
									f = new File(file);
									if (f.isFile() && files[k].endsWith(".xml")) {
										// falta acomodar esto pues ya se recibe
										// el dp
										// procesarXmlOai(dlconfig.getString("dl_metadata")
										// +
										// "/"+ServerConfXml.DBXML+"temp/"+cols[i]+"/"+
										// reps[j], f.getName());
										// procesarTxt(dlconfig.getString("dl_metadata")
										// +
										// "/"+ServerConfXml.DBXML+"temp/"+cols[i]+"/"+reps[j],f.getName());
									} else
										log.info("Directorio: " + files[k]);
								}
							}
						}
					}
				}
			}
		}
	}

	/**
	 * Busca el registro en la lista blanca ubicada en /conf/whitelist.xml
	 * 
	 * @param identifier
	 *            del registro
	 * @return boolean value
	 * @author luis
	 */
	public boolean checkInWhiteList(String identifier) {
		try {
			SAXBuilder sb = new SAXBuilder();
			Document doc = sb.build(fileWhitelist);
			Element raiz = doc.getRootElement();
			List listMetadata = raiz.getChildren();
			Iterator it = listMetadata.iterator();
			Element eMetadata = null;
			Element eIdentifier = null;
			while (it.hasNext()) {
				eMetadata = (Element) it.next();
				eIdentifier = eMetadata.getChild("identifier");
				if (eIdentifier.getText().equals(identifier))
					return true;

			}
		} catch (JDOMException e) {
			log.error(e);
		} catch (IOException e) {
			log.error(e);
		}
		return false;

	}

	/**
	 * Busca el registro en la lista negra ubicada en /conf/blacklist.xml
	 * 
	 * @param identifier
	 *            del registro
	 * @return boolean value
	 * @author luis
	 */
	public boolean checkInBlackList(String identifier) {
		try {
			SAXBuilder sb = new SAXBuilder();
			Document doc = sb.build(fileBlacklist);
			Element raiz = doc.getRootElement();
			List listMetadata = raiz.getChildren();
			Iterator it = listMetadata.iterator();

			Element eMetadata = null;
			Element eIdentifier = null;
			while (it.hasNext()) {
				eMetadata = (Element) it.next();
				eIdentifier = eMetadata.getChild("identifier");
				if (eIdentifier.getText().equals(identifier))
					return true;

			}

			return false;
		} catch (JDOMException e) {
			log.error(e);
		} catch (IOException e) {
			log.error(e);
		}
		return false;

	}

	/**
	 * Realiza un procesamiento TXT, es decir, por texto
	 * 
	 * @param path
	 * @param filename
	 */
	private void procesarTxt(String path, String filename, String fecha) {
		// Sabemos que el path al final tiene la estructura:
		// /collection/shortName
		// De ah_ sacamos los nombres
		String[] temp = path.split("/");
		String myShortName = temp[temp.length - 1];
		// SE REEMPLAZA EL ARCHIVO DE SALIDA POR EL ARCHIVO DE ENTRADA
		String pathSalida = path.replace(ServerConfXml.DBXML + "temp" + fecha,
				ServerConfXml.DBXML);
		String xml = null;
		try {
			File f_in = new File(pathSalida + "/" + filename);
			if (f_in.exists()) {

				int ch;
				StringBuffer sb = new StringBuffer("");
				FileInputStream fin = null;
				fin = new FileInputStream(f_in);
				InputStreamReader ir = new InputStreamReader(fin, "UTF-8");
				while ((ch = ir.read()) != -1) {
					sb.append((char) ch);
				}
				fin.close();
				xml = new String(sb.toString());

				// Reemplazo los Patrones generales
				Vector<MyPattern> generalPatterns = patterns.get("all");
				if (generalPatterns != null) {
					for (int i = 0; i < generalPatterns.size(); i++) {
						xml = xml.replace(generalPatterns.get(i)
								.getOldPattern(), generalPatterns.get(i)
								.getNewPattern());
					}
				}
				// Reemplazo los Patrones especiales de este shortname
				Vector<MyPattern> myPatterns = patterns.get(myShortName);
				if (myPatterns != null) {
					for (int i = 0; i < myPatterns.size(); i++) {
						xml = xml.replace(myPatterns.get(i).getOldPattern(),
								myPatterns.get(i).getNewPattern());
					}
				}
				File f_out = new File(pathSalida + "/" + filename);
				FileOutputStream fos = new FileOutputStream(f_out);
				fos.write(xml.getBytes());
				fos.close();
			}
		} catch (Exception e) {
			log.error("Documento mal formado: " + filename);
			log.error(e);
		}
	}

	/**
	 * Realiza un procesamiento XML para archivos sin formato de metadatos
	 * especificado
	 * 
	 * @param path
	 * @param filename
	 * @param dp
	 * @param fecha
	 * 
	 * 
	 *            DESACTUALIZADO
	 * 
	 * 
	 */
	private void procesarXmlOaiDefault(String path, String filename,
			DataProvider dp, String fecha) {
		log.info(filename);
		// Sabemos que el path al final tiene la estructura:
		// /collection/shortName
		// De ah_ sacamos los nombres
		docType = null; // Add by Edwin, jan 30 2011
		sYear = null; // add by edwin, aug 25 2011
		String[] tmp = path.split("/");
		// String collection=tmp[tmp.length-3]; //revisar porque ya la
		// collection no es un directorio
		// String collection= dp.getCollection();
		String institution = tmp[tmp.length - 2];
		String shortName = tmp[tmp.length - 1];

		// log.info("nrorecs before ="+dp.getNrorecs());

		// Namespace ns_oai = Namespace.getNamespace("oai_dc",
		// "http://www.openarchives.org/OAI/2.0/oai_dc/");
		// Namespace ns_dc = Namespace.getNamespace("dc",
		// "http://purl.org/dc/elements/1.1/");

		try {
			Element rdf = null;
			SAXBuilder builder = new SAXBuilder();
			String pathSalida = path.replace(ServerConfXml.DBXML + "temp"
					+ fecha, ServerConfXml.DBXML);
			String pathEntrada = path;
			Document doc_in = builder.build(pathEntrada + "/" + filename);
			Document doc_out = new Document();
			// Document doc_out = builder.build(filename_out);
			// doc = builder.
			Element raiz = doc_in.getRootElement();
			Element listRecords = raiz.getChild("ListRecords", ns);
			List records = null;
			if (listRecords == null) {
				log.error(dp.getShortName() + ">" + "Documento sin registros");
			} else {
				records = listRecords.getChildren(); // si no llegan records
														// aqui vamos al final

				// rdf = new Element("RDF", ns_rdf);
				rdf = new Element("BDNG");
				Vector<MyPattern> tagsGenerales = tagPatterns.get("all");
				Vector<MyPattern> tagsEspeciales = tagPatterns.get(shortName);
				doc_out.setRootElement(rdf);
				Iterator i = records.iterator();
				while (i.hasNext()) {
					docType = null; // Add by Edwin
					//dp.incContRegisters();
					dp.incNrorecs();
					// Element recordsOutput=new Element("Description", ns_rdf);
					// rdf.addContent(recordsOutput);
					Element l = (Element) i.next();

					// add by edy - campos del header

					Element header = l.getChild("header", ns);
					String Sidentifier_oai = "";
					String Sdatestamp = "";
					String SsetSpec = "";

					Element eh = null;

					if (header != null) {

						eh = header.getChild("identifier", ns);
						if (eh != null)
							Sidentifier_oai = eh.getText();

						eh = header.getChild("datestamp", ns);
						if (eh != null)
							Sdatestamp = eh.getText();

						eh = header.getChild("setSpec", ns);
						if (eh != null)
							SsetSpec = eh.getText();

						/*
						 * List lH = header.getChildren(); Iterator iH =
						 * lH.iterator(); Element eH = (Element)iH.next();
						 * Sidentifier_oai = eH.getText(); eH =
						 * (Element)iH.next(); Sdatestamp = eH.getText(); eH =
						 * (Element)iH.next(); SsetSpec = eH.getText();
						 */
					}

					// end add edy

					Element metadata = l.getChild("metadata", ns);
					if (metadata != null) {

						dp.incNrorecs();
						List e4 = metadata.getChildren();
						Iterator i4 = e4.iterator();
						Namespace dc = null;
						Element e5 = (Element) i4.next();
						// Lista con todos los atributos del record. Se usa para
						// ver repetidos
						List allElements = e5.getChildren();
						if (allElements != null && allElements.size() > 0) {
							dc = (e5.getChildren().get(0)).getNamespace();
							Element salida = e5.clone();
							salida.removeContent();
							Iterator myIterator = allElements.iterator();
							Vector<String> singleElements = new Vector<String>();
							sYear = null;
							while (myIterator.hasNext()) {
								Element e6 = (Element) myIterator.next();
								String element = e6.getText();
								if (tagsEspeciales != null) {
									for (int cont = 0; cont < tagsEspeciales
											.size(); cont++) {
										if (tagsEspeciales.get(cont).tag
												.equalsIgnoreCase(e6.getName())) {
											if (e6.getText()
													.contains(
															tagsEspeciales
																	.get(cont).oldPattern)) {
												element = element
														.replace(
																tagsEspeciales
																		.get(cont)
																		.getOldPattern(),
																tagsEspeciales
																		.get(cont)
																		.getNewPattern());
												e6.setText(element);
												break;
											}
										}
									}
								}
							}

							// new for Edwin - Jan, 30, 2011
							Element eInstitution = new Element("institution",
									dc);
							eInstitution.setText(institution);
							// end for Edwin

							Element elementoCollection = new Element(
									"collection", dc);
							Element repname = new Element("repname", dc);
							repname.setText(shortName);
							salida.addContent(elementoCollection);
							salida.addContent(repname);

							// new for Edwin, Jan,30,2011
							salida.addContent(eInstitution);

							// end for Edwin

							// add edy - campos del header

							Element identifier_oai = new Element(
									"identifier_oai", dc);
							identifier_oai.setText(Sidentifier_oai);

							Element datestamp = new Element("datestamp", dc);
							datestamp.setText(Sdatestamp);

							Element setSpec = new Element("setSpec", dc);
							setSpec.setText(SsetSpec);

							salida.addContent(identifier_oai);
							salida.addContent(datestamp);
							salida.addContent(setSpec);

							if (certificacion) {
								// este es el proceso de certificacion de un
								// registro, se verifica en una lista negra y
								// una lista blanca, y dependiento de eso se
								// pone un tag para identificarlo
								Element certified = new Element("certified");
								if (checkInBlackList(identifier_oai.getText()))
									certified.setText("2").setNamespace(dc); // rechazado
								else if (checkInWhiteList(identifier_oai
										.getText()))
									certified.setText("0").setNamespace(dc); // aprobado
								else
									certified.setText("1").setNamespace(dc); // pendiente

								salida.addContent(certified);
							}
							//

							rdf.addContent(salida);
							// dp.incNrorecs();
						}
					}
				}
				File f_out = new File(pathSalida);
				f_out.mkdirs();
				f_out = new File(pathSalida + "/" + filename);
				// System.out.println("Path SALIDA"+
				// pathSalida+"/"+filename+" Exito? "+ f_out.createNewFile());
				f_out.createNewFile();
				XMLOutputter xml = new XMLOutputter(Format.getPrettyFormat());
				FileOutputStream out = new FileOutputStream(pathSalida + "/"
						+ filename);
				xml.output(doc_out, out);
				out.close();
			}// si no tiene records llega aqui
		} catch (Exception e) {
			log.error("Documento mal formado. Proveedor: " + dp.getShortName());
			log.error(e);
		}
		// System.out.println("nrorecs after ="+dp.getNrorecs());
	}

	/**
	 * Realiza un procesamiento XML para archivos con protocolo OAI
	 * 
	 * @param path
	 * @param filename
	 * @param dp
	 * @param fecha
	 * @param xmlFormat
	 */
	private void procesarXmlOai(String path, String filename, DataProvider dp,
			String fecha, String xmlFormat) {
		boolean resultValidation = true;
		boolean filterBadRecords = dp.filterBadRecords();
		log.info(filename);

		String institution=dp.getShortInst();
		String shortName=dp.getShortName();

		try {
			Element bdng = null;
			SAXBuilder builder = new SAXBuilder();
			String pathSalida = path.replace(ServerConfXml.DBXML + "temp"
					+ fecha, ServerConfXml.DBXML);
			String pathEntrada = path;
			Document doc_in = builder.build(pathEntrada + "/" + filename);
			Document doc_out = new Document();
			Element raiz = doc_in.getRootElement();

			isLom = false;
			isDmp = false;

			Element listRecords = raiz.getChild("ListRecords", ns);
			if (listRecords == null)
				listRecords = raiz.getChild("ListRecords");

			List records = null;
			if (listRecords == null) {
				log.error(dp.getShortName() + ">" + "Documento sin registros");
			} else {

				records = listRecords.getChildren(); 
				bdng = new Element("BDNG");
				Vector<MyPattern> tagsGenerales = tagPatterns.get("all");
				Vector<MyPattern> tagsEspeciales = tagPatterns.get(shortName);
				doc_out.setRootElement(bdng);
				Iterator i = records.iterator();
				
				while (i.hasNext()) {
					docType = null;
					sLinkIdentifier = null;
					sLearningObjectType = null;
					
					Element l = (Element) i.next();
					Element header = l.getChild("header", ns);
					if (header == null)
						header = l.getChild("header");
					String Sidentifier_oai = "";
					String Sdatestamp = "";
					String SsetSpec = "";

					Element eh = null;

					if (header != null) {

						eh = header.getChild("identifier", ns);
						if (eh == null)
							eh = header.getChild("identifier");
						if (eh != null)
							Sidentifier_oai = eh.getText();

						eh = null;
						eh = header.getChild("datestamp", ns);
						if (eh == null)
							eh = header.getChild("datestamp");
						if (eh != null)
							Sdatestamp = eh.getText();

						eh = header.getChild("setSpec", ns);
						if (eh == null)
							eh = header.getChild("setSpec");
						if (eh != null)
							SsetSpec = eh.getText();

					}

					Element metadata = l.getChild("metadata", ns);
					if (metadata == null)
						metadata = l.getChild("metadata");

					if (metadata != null) {

						
						List e4 = metadata.getChildren();
						Iterator i4 = e4.iterator();
						Namespace namespace = null;
						Element e5 = (Element) i4.next();
						// Lista con todos los atributos del record. Se usa para
						// ver repetidos
						List allElements = e5.getChildren();
						if (allElements != null && allElements.size() > 0) {
							Element outputElement = null;
							Iterator myIterator = allElements.iterator();
							Vector<String> singleElements = new Vector<String>();
							docType = null;
							sYear = null;

							if (xmlFormat.equalsIgnoreCase("lom")) {
								xmlFormat = "oai_lom";
							}

							if (xmlFormat.equalsIgnoreCase("dmp")) {
								xmlFormat = "oai_dmp";
							}

							if (xmlFormat.equalsIgnoreCase("oai_dc")
									|| xmlFormat.contains("dc")) {
								outputElement = new Element("record", ns_dc);
								outputElement = procesarDc(outputElement,
										myIterator, singleElements, ns_dc,
										tagsEspeciales, tagsGenerales);

							} else if (xmlFormat.equalsIgnoreCase("oai_lomco")) {
								outputElement = new Element("record");
								outputElement = procesarLomCo(outputElement,
										myIterator, singleElements, /* namespace */
										ns_oailom, tagsEspeciales,
										tagsGenerales);
							} else if (xmlFormat.equalsIgnoreCase("oai_lom")
									|| (xmlFormat.contains("oai_lom") && (!xmlFormat
											.contains("co")))) {
								outputElement = new Element("record", ns_oailom);
								outputElement = procesarLomIEEE(outputElement,
										myIterator, singleElements, /* namespace */
										ns_oailom, tagsEspeciales,
										tagsGenerales);
							} else if (xmlFormat.equalsIgnoreCase("oai_dmp")) {
								outputElement = new Element("record", ns_dc);
								outputElement.addContent(e5.clone());
								Document doctmp = new Document();
								doctmp.setRootElement(e5.clone());

								writeXml.writeXmlJdomFile(path,
										"filexmltemp.xml", doctmp);
								// boolean resultdoer =
								// valdoer.validar(path+"/filexmltemp.xml");
								resultValidation = valreda.validarSimpleReda(
										path + "/filexmltemp.xml",
										valreda.getDirLogs()
												+ dp.getShortName() + "-"
												+ valreda.getFechaActual());
								
								if (resultValidation==false && filterBadRecords==true)
									continue;
								// DmpDoer dpdmp = new DmpDoer(e5.clone());
								// System.out.println(dpdmp.getFirstId()+"/"+dpdmp.getTitle()+"/"+resultreda);

								// --- outputElement = new Element("record",
								// ns_dc);
								// e5.removeNamespaceDeclaration(ns_xsi); //
								// para pruebas.
								// --- outputElement.addContent(e5.clone());
								/*
								 * outputElement = procesarDmp(outputElement,
								 * myIterator, singleElements, ns_oaidmp,
								 * tagsEspeciales, tagsGenerales);
								 */
							}
							
							dp.incNrorecs();
							
							Element eDepto = null;	
							Element eInstitution = null;
							Element elementoCollection = null;
							Element repname = null;
							Element linkIdentifier = null;
							Element identifier_oai = null;
							Element datestamp = null;
							Element setSpec = null;

							eDepto = new Element("departamento", ns_dc);
							eInstitution = new Element("institution", ns_dc);
							elementoCollection = new Element("collection",
									ns_dc);
							repname = new Element("repname", ns_dc);
							linkIdentifier = new Element("linkidentifier",
									ns_dc);
							identifier_oai = new Element("identifier_oai",
									ns_dc);
							datestamp = new Element("datestamp", ns_dc);
							setSpec = new Element("setSpec", ns_dc);							
							eDepto.setText(dp.getTagDepartamento());
							eInstitution.setText(institution);
							boolean found = false;
							if (docType != null) {
								if (!xmlFormat.contains("lom")) {
									found = findCollection(docType
											.toUpperCase());
									if (found)
										elementoCollection.setText(docType
												.toUpperCase());
									else
										elementoCollection.setText("GENERAL");
								} else {
									elementoCollection.setText(docType
											.toUpperCase());
								}
							} else
								elementoCollection.setText("GENERAL");

							// end
							repname.setText(shortName);
							outputElement.addContent(elementoCollection);

							/*
							 * Crea nuevo elemento si el record tiene formato de
							 * learning object metadata
							 */
							if (elementoCollection.getText().equalsIgnoreCase(
									"LEARNING OBJECT")
									&& xmlFormat.equalsIgnoreCase("oai_lom")) {
								Element elementyear = new Element("year", ns_dc);
								if (sYear == null)
									elementyear.setText("");
								else {
									elementyear.setText(sYear);
								}
								outputElement.addContent(elementyear);
							}

							if (sLinkIdentifier == null)
								linkIdentifier.setText("");
							else
								linkIdentifier.setText(sLinkIdentifier);

							outputElement.addContent(linkIdentifier);
							outputElement.addContent(repname);
							outputElement.addContent(eInstitution);
							outputElement.addContent(eDepto);
							identifier_oai.setText(Sidentifier_oai);
							datestamp.setText(Sdatestamp);
							setSpec.setText(SsetSpec);
							outputElement.addContent(identifier_oai);
							outputElement.addContent(datestamp);
							outputElement.addContent(setSpec);

							if (certificacion) {
								// este es el proceso de certificacion de un
								// registro, se verifica en una lista negra y
								// una lista blanca, y dependiento de eso se
								// pone un tag para identificarlo

								Element certified = new Element("certified");
								if (checkInBlackList(linkIdentifier.getText()))
									certified.setText("2").setNamespace(ns_dc); // rechazado
								else if (checkInWhiteList(linkIdentifier
										.getText()))
									certified.setText("0").setNamespace(ns_dc); // aprobado
								else
									certified.setText("1").setNamespace(ns_dc); // pendiente

								outputElement.addContent(certified);
							}

							if (isLom) {
								recordsOutputDCQ = new Element("record", ns_dc);
								crearJerarquiaDublinCoreQ(outputElement, ns_dc,
										outputElement.getName());
								bdng.addContent(recordsOutputDCQ);
								recordsOutputDCQ = null;
							} else {
								bdng.addContent(outputElement.clone());
							}
						}
					}
				}
				
				writeXml.writeXmlJdomFile(pathSalida, filename, doc_out);
				
				

			}// si no tiene records llega aqui
		} catch (Exception e) {
			log.error("Documento mal formado. Proveedor: " + dp.getShortName());
			log.error(e);
		}

	}

	/**
	 * Realiza procesamiento XML para archivos con protocolo HTTP
	 * 
	 * @param path
	 * @param filename
	 * @param dp
	 * @param fecha
	 */
	private void procesarXmlHTTP(String path, String filename, DataProvider dp,
			String fecha) {
		// Sabemos que el path al final tiene la estructura:
		// /collection/shortName
		// De ahi sacamos los nombres

		// String collection="";
		String shortName = "";
		String institution = "";
		String metadataPrefix = dp.getMetadataPrefix();
		// Add by Edwin, jan 30 2011

		String[] cr = path.split("/");
		if (cr.length > 1) {
			institution = cr[cr.length - 2];
			shortName = cr[cr.length - 1];
		}

		List listdc = null;
		try {
			Element rdf = null;
			SAXBuilder builder = new SAXBuilder();
			String pathSalida = path.replace(ServerConfXml.DBXML + "temp"
					+ fecha, ServerConfXml.DBXML);
			String pathEntrada = path;
			Document doc_in = builder.build(pathEntrada + "/" + filename);
			Document doc_out = new Document();
			Element raiz = doc_in.getRootElement();

			Namespace ns = Namespace
					.getNamespace("http://www.openarchives.org/OAI/2.0/");
			Namespace ns_oai = Namespace.getNamespace("oai_dc",
					"http://www.openarchives.org/OAI/2.0/oai_dc/");
			Namespace ns_oailom = Namespace.getNamespace("oai_lom",
					"http://ltsc.ieee.org/xsd/LOM");
			Namespace ns_oaidmp = Namespace.getNamespace("oai_dmp", "http://");
			Namespace ns_dc = Namespace.getNamespace("dc",
					"http://purl.org/dc/elements/1.1/");
			Namespace ns_lom = Namespace.getNamespace("lom",
					"http://ltsc.ieee.org/xsd/LOM");
			Namespace ns_dmp = Namespace.getNamespace("dmp", "http://");
			Namespace ns_xsi = Namespace.getNamespace("xsi",
					"http://www.w3.org/2001/XMLSchema-instance");
			isLom = false;

			List records = raiz.getChildren();
			rdf = new Element("BDNG");
			doc_out.setRootElement(rdf);
			Vector<MyPattern> tagsGenerales = tagPatterns.get("all");
			Vector<MyPattern> tagsEspeciales = tagPatterns.get(shortName);
			Iterator i = records.iterator();

			while (i.hasNext()) {
				docType = null; // Add by Edwin
				sLinkIdentifier = null;
				sLearningObjectType = null;

				Element e = (Element) i.next();
				// Element recordsOutput = new Element("dc", ns_oai);
				// recordsOutput.addNamespaceDeclaration(ns_oai);
				listdc = e.getChildren();
				if (listdc != null && listdc.size() > 0) {
					// Namespace nsElementos = null;
					// ns_dc = ((Element)
					// (e.getChildren().get(0))).getNamespace();
					// recordsOutput.addNamespaceDeclaration(ns_dc);
					// rdf.addContent(recordsOutput);
					if (e != null) {
						dp.incNrorecs();
						List allElements = e.getChildren();
						Iterator myIterator = allElements.iterator();
						Element recordsOutput = null;
						Vector<String> singleElements = new Vector<String>();
						if (myIterator.hasNext()) {
							if (metadataPrefix.equalsIgnoreCase("oai_dc")
									|| metadataPrefix.contains("rdf")) {
								recordsOutput = new Element("record", ns_dc);
								recordsOutput = procesarDc(recordsOutput,
										myIterator, singleElements, ns_dc,
										tagsEspeciales, tagsGenerales);
							} else if (metadataPrefix
									.equalsIgnoreCase("oai_lomco")) {
								recordsOutput = new Element("record", ns_dc);
								recordsOutput = procesarLomCo(recordsOutput,
										myIterator, singleElements, ns_dc,
										tagsEspeciales, tagsGenerales);
							} else if (metadataPrefix
									.equalsIgnoreCase("oai_lom")) {
								recordsOutput = new Element("record");
								recordsOutput = procesarLomIEEE(recordsOutput,
										myIterator, singleElements, ns_oailom,
										tagsEspeciales, tagsGenerales);
							} else if (metadataPrefix
									.equalsIgnoreCase("oai_dmp")) {
								recordsOutput = new Element("record");
								recordsOutput = procesarDmp(recordsOutput,
										myIterator, singleElements, ns_oaidmp,
										tagsEspeciales, tagsGenerales);
							}
							// Element testingElement = (Element)
							// myIterator.next();
							// /* Resetear el iterador para obtener el primer
							// elemento evaluado */
							// myIterator = allElements.iterator();
							// if(testingElement.getChildren().size()==0){
							// //System.out.println("testing element"+testingElement.getName());
							// xmlEsquema="dc";
							// recordsOutput = new Element("dc", ns_oai);
							// recordsOutput.addNamespaceDeclaration(ns_dc);
							// recordsOutput=procesarDc(recordsOutput,
							// myIterator, singleElements, ns_dc ,
							// tagsEspeciales, tagsGenerales);
							// }else{
							// xmlEsquema="lom";
							// recordsOutput = new Element("lom", ns_oailom);
							// recordsOutput.addNamespaceDeclaration(ns_lom);
							// recordsOutput.addNamespaceDeclaration(ns_xsi);
							// recordsOutput.setAttribute(new
							// Attribute("schemaLocation","http://ltsc.ieee.org/xsd/LOM http://ltsc.ieee.org/xsd/lomv1.0/lom.xsd",
							// ns_xsi));
							// recordsOutput=procesarLomCo(recordsOutput,
							// myIterator, singleElements, ns_oailom,
							// tagsEspeciales, tagsGenerales);
							// }
						}

						// Element eInstitution = null;
						// Element elementoCollection = null;
						// Element repname = null;
						// Element linkIdentifier = null;
						Element eInstitution = new Element("institution", ns_dc);
						Element elementoCollection = new Element("collection",
								ns_dc);
						Element repname = new Element("repname", ns_dc);
						Element linkIdentifier = new Element("linkidentifier",
								ns_dc);

						// if (xmlEsquema.equalsIgnoreCase("dc")) {
						// /* Settear el namespace correcto para Dublin Core*/
						// eInstitution = new Element("institution",ns_dc);
						// elementoCollection = new Element("collection",ns_dc);
						// repname = new Element("repname", ns_dc);
						// linkIdentifier = new Element("linkidentifier",
						// ns_dc);
						// } else if (xmlEsquema.equalsIgnoreCase("oai_lom")) {
						// /* Settear el namespace correcto para LOM*/
						// eInstitution = new Element("institution",ns_oailom);
						// elementoCollection = new
						// Element("collection",ns_oailom);
						// repname = new Element("repname",ns_oailom);
						// linkIdentifier = new Element("linkidentifier",
						// ns_oailom);
						// }

						// dc = ((Element) (e.getChildren().get(0)))
						// .getNamespace();
						// eInstitution = new Element("institution", dc); // add
						// // by
						// // Edwin,
						// // jan
						// // 30,2011
						// sInstitution = null;
						// if(sInstitution!=null){
						// eInstitution.setText(sInstitution);
						// }else{
						eInstitution.setText(institution); // add by Edwin, jan
						// }
						// // 30, 2011
						// elementoCollection = new Element("collection",
						// dc);

						// Add by Edwin - Jan 30 2011
						// if (dp.getStaticCollection().equals("true")
						// || docType == null)
						// elementoCollection.setText(dp.getCollection().toUpperCase());
						// else
						// elementoCollection.setText(docType.toUpperCase());
						// // End
						// docType=null;
						// year=null;

						boolean found = false;
						if (docType != null) {
							// if(!metadataPrefix.contains("lom")){
							found = findCollection(docType.toUpperCase());
							if (found)
								elementoCollection.setText(docType
										.toUpperCase());
							else
								elementoCollection.setText("GENERAL");
							// }else{
							// elementoCollection.setText(docType.toUpperCase());
							// }
						} else
							elementoCollection.setText("GENERAL");

						docType = null;
						sYear = null;
						// end

						/*
						 * Crea nuevo elemento si el record tiene formato de
						 * learning object metadata
						 */
						// if(elementoCollection.getText().equalsIgnoreCase("LEARNING OBJECT")&&
						// metadataPrefix.equalsIgnoreCase("lom_ieee")){
						// Element learningObjectType= new
						// Element("objecttype"/*,ns_oailom*/);
						// if(sLearningObjectType==null)
						// learningObjectType.setText("");
						// else{
						// learningObjectType.setText(sLearningObjectType.toLowerCase());
						// }
						// recordsOutput.addContent(learningObjectType);
						//
						// Element creator= new
						// Element("creator"/*,ns_oailom*/);
						// if(sCreator==null)
						// creator.setText("");
						// else{
						// creator.setText(sCreator);
						// }
						// recordsOutput.addContent(creator);
						//
						// Element publisher= new
						// Element("publisher"/*,ns_oailom*/);
						// if(sPublisher==null)
						// publisher.setText("");
						// else{
						// publisher.setText(sPublisher);
						// }
						// recordsOutput.addContent(publisher);
						//
						// Element elementyear= new
						// Element("year"/*,ns_oailom*/);
						// if(sYear==null)
						// elementyear.setText("");
						// else{
						// elementyear.setText(sYear);
						// }
						// recordsOutput.addContent(elementyear);
						// }

						// repname = new Element("repname", dc);
						repname.setText(shortName);
						recordsOutput.addContent(elementoCollection);

						if (sLinkIdentifier == null)
							linkIdentifier.setText("");
						else
							linkIdentifier.setText(sLinkIdentifier);

						recordsOutput.addContent(linkIdentifier);
						recordsOutput.addContent(repname);
						recordsOutput.addContent(eInstitution);

						if (certificacion) {
							// este es el proceso de certificacion de un
							// registro, se verifica en una lista negra y una
							// lista blanca, y dependiento de eso se pone un tag
							// para identificarlo

							Element certified = new Element("certified");
							if (checkInBlackList(linkIdentifier.getText()))
								certified.setText("2").setNamespace(ns_dc); // rechazado
							else if (checkInWhiteList(linkIdentifier.getText()))
								certified.setText("0").setNamespace(ns_dc); // aprobado
							else
								certified.setText("1").setNamespace(ns_dc); // pendiente

							recordsOutput.addContent(certified);
						}

						// rdf.addContent((Element) recordsOutput.clone());

						/*
						 * Si los registros tienen estandar IEEE, primero se
						 * crea la jerarquia y despues se transforma a Dublin
						 * Core cualificado
						 */

						if (isLom) {
							recordsOutputDCQ = new Element("record", ns_dc);
							crearJerarquiaDublinCoreQ(recordsOutput, ns_dc,
									recordsOutput.getName());
							rdf.addContent(recordsOutputDCQ);
						} else {
							rdf.addContent(recordsOutput.clone());
						}
					}
				}

				writeXml.writeXmlJdomFile(pathSalida, filename, doc_out);

			}
		} catch (Exception e) {
			log.error(e);
		}
	}

	/**
	 * Procesa archivos con formato de metadatos Dublin Core
	 * 
	 * @param outputElement
	 * @param myIterator
	 * @param singleElements
	 * @param namespace
	 * @param tagsEspeciales
	 * @param tagsGenerales
	 * @return outputElement
	 * @author Daniel
	 */
	private Element procesarDc(Element outputElement, Iterator myIterator,
			Vector<String> singleElements, Namespace namespace,
			Vector<MyPattern> tagsEspeciales, Vector<MyPattern> tagsGenerales) {

		/* Mapeado DC a LOM, creacion de elementos */
		// Element general = new Element("general");
		// Element educational = new Element("educational");
		// Element lifeCycle = new Element("lifeCycle");
		// Element technical = new Element("technical");
		// Element rights = new Element("rights");
		// Element contributePublisher = new Element("contribute");
		// Element contributeAuthor = new Element("contribute");

		while (myIterator.hasNext()) {
			Element e6 = (Element) myIterator.next();
			Element elemento = new Element(e6.getName(), namespace);
			elemento.addContent(e6.getText());
			elemento = buscarTagsPredeterminados(tagsEspeciales, tagsGenerales,
					elemento, "oai_dc");
			outputElement = procesarTagsDc(outputElement, singleElements,
					myIterator, elemento, namespace);
		}
		/* Construcci_n de jerarquia documento salida */
		// if(general.getChildren().size()!=0)
		// outputElement.addContent(general);
		//
		// if(lifeCycle.getChildren().size()!=0)
		// outputElement.addContent(lifeCycle);
		//
		// if(technical.getChildren().size()!=0)
		// outputElement.addContent(technical);
		//
		// if(educational.getChildren().size()!=0)
		// outputElement.addContent(educational);
		//
		// if(rights.getChildren().size()!=0)
		// outputElement.addContent(rights);

		return outputElement;
	}

	/**
	 * Procesa archivos con formato de metadatos LOM_CO
	 * 
	 * @param outputElement
	 * @param myIterator
	 * @param singleElements
	 * @param namespace
	 * @param tagsEspeciales
	 * @param tagsGenerales
	 * @return outputElement
	 * @author Daniel
	 */
	private Element procesarLomCo(Element outputElement, Iterator myIterator,
			Vector<String> singleElements, Namespace namespace,
			Vector<MyPattern> tagsEspeciales, Vector<MyPattern> tagsGenerales) {
		docType = "LEARNING OBJECT";
		while (myIterator.hasNext()) {
			Element e6 = (Element) myIterator.next();
			Element elemento = new Element(e6.getName());
			elemento.addContent(e6.getText());
			elemento = buscarTagsPredeterminados(tagsEspeciales, tagsGenerales,
					elemento, "oai_lomco");
			outputElement = procesarTagsLomCo(outputElement, singleElements,
					myIterator, elemento, namespace);
		}

		return outputElement;
	}

	/**
	 * Procesa archivos con formato de metadatos LOM(Estandar IEEE)
	 * 
	 * @param outputElement
	 * @param AllClasificationElementsIterator
	 * @param singleElementsLom
	 * @param namespace
	 * @param tagsEspeciales
	 * @param tagsGenerales
	 * @return outputElement
	 * @author Daniel
	 */
	private Element procesarLomIEEE(Element outputElement,
			Iterator AllClasificationElementsIterator,
			Vector<String> singleElementsLom, Namespace namespace,
			Vector<MyPattern> tagsEspeciales, Vector<MyPattern> tagsGenerales) {

		docType = "LEARNING OBJECT";
		isLom = true;
		while (AllClasificationElementsIterator.hasNext()) {

			/* Obtiene un elemento de clasificaci_n, debajo del tag <metadata> */
			Element classificationElem = (Element) AllClasificationElementsIterator
					.next();
			Element classificationElemClone = classificationElem.clone();
			// Element classificationElemClone = new
			// Element(classificationElem.getName());

			// TODO: IMPLEMENT LOM
			/* Procesa elemento de clasificacion para agregarlo a la jerarquia */
			outputElement = procesarTagsLomIEEE(outputElement,
					singleElementsLom, AllClasificationElementsIterator,
					classificationElemClone, namespace);

			List allElements = classificationElem.getChildren();

			Iterator myElementIterator = allElements.iterator();

			/*
			 * Recorre todos los elementos de informaci_n debajo del elemento de
			 * clasificacion
			 */
			while (myElementIterator.hasNext()) {
				/*
				 * Crea un nuevo vector con los elementos de texto agregados, se
				 * debe sustituir para cada nivel
				 */
				singleElementsLom = new Vector<String>();

				/* Obtiene elemento de informacion */
				Element informationElement = (Element) myElementIterator.next();
				// Element informationElementClone = (Element)
				// informationElement.clone();
				Element informationElementClone = null;
				// informationElementClone.setNamespace(namespace);
				List allSubElements = informationElement.getChildren();
				// Element informationElementClone=null;
				// System.out.println("Information Element not cloned: "+informationElement.getName());
				// System.out.println("Children not cloned: "+allSubElements.size());
				// TODO: Do it!
				// if(allSubElements.size()==0){
				// // System.out.println("ENTRO ARRIBA >>>>>>>>");
				// informationElementClone = new
				// Element(informationElement.getName());
				// //
				// System.out.println("Information Element C L O N E D >>>>>: "+informationElementClone.getName());
				// informationElementClone.addContent(informationElement.getText());
				//
				// }else{
				// System.out.println("ENTRO ABAJO >>>>>>>>");
				informationElementClone = informationElement.clone();
				// }

				/*
				 * Verifica si este elemento contiene texto, puede o no que lo
				 * contenga, en caso de que si, busca tags predeterminados.
				 */
				if (informationElementClone.getText().length() != 0) {
					informationElementClone = buscarTagsPredeterminados(
							tagsEspeciales, tagsGenerales,
							informationElementClone, "oai_lomco");
				}

				/* Procesa elemento de informacion para agregarlo a la jerarquia */
				classificationElemClone = procesarTagsLomIEEE(
						classificationElemClone, singleElementsLom,
						myElementIterator, informationElementClone, namespace);
				// informationElementClone = new
				// Element(informationElementClone.getName());

				// List allSubElements = informationElement.getChildren();

				if (allSubElements != null && allSubElements.size() != 0) {

					Iterator allSubElementsIterator = allSubElements.iterator();

					/*
					 * Recorre todos los subelementos debajo del elemento de
					 * informacion
					 */
					while (allSubElementsIterator.hasNext()) {

						/* Crea un nuevo objeto vector en la referencia pasada */
						singleElementsLom = new Vector<String>();

						/*
						 * Obtiene subelemento, directamente debajo de elemento
						 * de informacion
						 */
						Element subElement = (Element) allSubElementsIterator
								.next();
						List allFinalElements = subElement.getChildren();
						// Element subElementClone=(Element) subElement.clone();
						// subElementClone.setNamespace(namespace);
						Element subElementClone = null;
						// if(allFinalElements.size()==0){
						// subElementClone = new Element(subElement.getName());
						// subElementClone.addContent(subElement.getText());
						// }else{
						// subElementClone = (Element) subElement.clone();
						// }
						subElementClone = subElement.clone();
						/*
						 * Verifica si este elemento contiene texto, puede o no
						 * que lo contenga, en caso de que si, busca tags
						 * predeterminados.
						 */
						if (subElementClone.getText().length() != 0) {
							subElementClone = buscarTagsPredeterminados(
									tagsEspeciales, tagsGenerales,
									subElementClone, "oai_lomco");
						}

						informationElementClone = procesarTagsLomIEEE(
								informationElementClone, singleElementsLom,
								allSubElementsIterator, subElementClone,
								namespace);
						// subElementClone = new
						// Element(subElementClone.getName());
						// List allFinalElements = subElement.getChildren();

						if (allFinalElements != null
								&& allFinalElements.size() != 0) {

							Iterator allFinalElementsIterator = allFinalElements
									.iterator();
							/*
							 * Recorre todos los elementos finales de bajo del
							 * subelemento de informacion
							 */
							while (allFinalElementsIterator.hasNext()) {
								/* Crea un nuevo vector en la referencia pasada */
								singleElementsLom = new Vector<String>();

								/*
								 * Obtiene elemento final, directamente debajo
								 * de subelemento
								 */
								Element finalElement = (Element) allFinalElementsIterator
										.next();
								Element finalElementClone = finalElement
										.clone();
								// finalElementClone.setNamespace(namespace);

								// Element finalElementClone = new
								// Element(finalElement.getName());
								// finalElementClone.addContent(finalElement.getText());

								/*
								 * Procesa texto del elemento y lo agrega a la
								 * jerarquia
								 */
								finalElementClone = buscarTagsPredeterminados(
										tagsEspeciales, tagsGenerales,
										finalElementClone, "oai_lomco");
								subElementClone = procesarTagsLomIEEE(
										subElementClone, singleElementsLom,
										allFinalElementsIterator,
										finalElementClone, namespace);

							}

						}

					}

				}

			}
		}

		return outputElement;
	}

	/**
	 * Procesa archivos con formato de metadatos DMP
	 * 
	 * @param outputElement
	 * @param AllClasificationElementsIterator
	 * @param singleElementsDmp
	 * @param namespace
	 * @param tagsEspeciales
	 * @param tagsGenerales
	 * @return outputElement
	 * @author Edwin
	 */
	private Element procesarDmp(Element outputElement,
			Iterator AllClasificationElementsIterator,
			Vector<String> singleElementsDmp, Namespace namespace,
			Vector<MyPattern> tagsEspeciales, Vector<MyPattern> tagsGenerales) {

		docType = "LEARNING OBJECT";
		isDmp = true;
		while (AllClasificationElementsIterator.hasNext()) {

			/* Obtiene un elemento de clasificaci_n, debajo del tag <metadata> */
			Element classificationElem = (Element) AllClasificationElementsIterator
					.next();
			Element classificationElemClone = classificationElem.clone();
			// Element classificationElemClone = new
			// Element(classificationElem.getName());

			// TODO: IMPLEMENT LOM
			/* Procesa elemento de clasificacion para agregarlo a la jerarquia */
			outputElement = procesarTagsDmp(outputElement, singleElementsDmp,
					AllClasificationElementsIterator, classificationElemClone,
					namespace);

			List allElements = classificationElem.getChildren();

			Iterator myElementIterator = allElements.iterator();

			/*
			 * Recorre todos los elementos de informaci_n debajo del elemento de
			 * clasificacion
			 */
			while (myElementIterator.hasNext()) {
				/*
				 * Crea un nuevo vector con los elementos de texto agregados, se
				 * debe sustituir para cada nivel
				 */
				singleElementsDmp = new Vector<String>();

				/* Obtiene elemento de informacion */
				Element informationElement = (Element) myElementIterator.next();
				// Element informationElementClone = (Element)
				// informationElement.clone();
				Element informationElementClone = null;
				// informationElementClone.setNamespace(namespace);
				List allSubElements = informationElement.getChildren();
				// Element informationElementClone=null;
				// System.out.println("Information Element not cloned: "+informationElement.getName());
				// System.out.println("Children not cloned: "+allSubElements.size());
				// TODO: Do it!
				// if(allSubElements.size()==0){
				// // System.out.println("ENTRO ARRIBA >>>>>>>>");
				// informationElementClone = new
				// Element(informationElement.getName());
				// //
				// System.out.println("Information Element C L O N E D >>>>>: "+informationElementClone.getName());
				// informationElementClone.addContent(informationElement.getText());
				//
				// }else{
				// System.out.println("ENTRO ABAJO >>>>>>>>");
				informationElementClone = informationElement.clone();
				// }

				/*
				 * Verifica si este elemento contiene texto, puede o no que lo
				 * contenga, en caso de que si, busca tags predeterminados.
				 */
				if (informationElementClone.getText().length() != 0) {
					informationElementClone = buscarTagsPredeterminados(
							tagsEspeciales, tagsGenerales,
							informationElementClone, "oai_dmp");
				}

				/* Procesa elemento de informacion para agregarlo a la jerarquia */
				classificationElemClone = procesarTagsDmp(
						classificationElemClone, singleElementsDmp,
						myElementIterator, informationElementClone, namespace);
				// informationElementClone = new
				// Element(informationElementClone.getName());

				// List allSubElements = informationElement.getChildren();

				if (allSubElements != null && allSubElements.size() != 0) {

					Iterator allSubElementsIterator = allSubElements.iterator();

					/*
					 * Recorre todos los subelementos debajo del elemento de
					 * informacion
					 */
					while (allSubElementsIterator.hasNext()) {

						/* Crea un nuevo objeto vector en la referencia pasada */
						singleElementsDmp = new Vector<String>();

						/*
						 * Obtiene subelemento, directamente debajo de elemento
						 * de informacion
						 */
						Element subElement = (Element) allSubElementsIterator
								.next();
						List allFinalElements = subElement.getChildren();
						// Element subElementClone=(Element) subElement.clone();
						// subElementClone.setNamespace(namespace);
						Element subElementClone = null;
						// if(allFinalElements.size()==0){
						// subElementClone = new Element(subElement.getName());
						// subElementClone.addContent(subElement.getText());
						// }else{
						// subElementClone = (Element) subElement.clone();
						// }
						subElementClone = subElement.clone();
						/*
						 * Verifica si este elemento contiene texto, puede o no
						 * que lo contenga, en caso de que si, busca tags
						 * predeterminados.
						 */
						if (subElementClone.getText().length() != 0) {
							subElementClone = buscarTagsPredeterminados(
									tagsEspeciales, tagsGenerales,
									subElementClone, "oai_dmp");
						}

						informationElementClone = procesarTagsDmp(
								informationElementClone, singleElementsDmp,
								allSubElementsIterator, subElementClone,
								namespace);
						// subElementClone = new
						// Element(subElementClone.getName());
						// List allFinalElements = subElement.getChildren();

						if (allFinalElements != null
								&& allFinalElements.size() != 0) {

							Iterator allFinalElementsIterator = allFinalElements
									.iterator();
							/*
							 * Recorre todos los elementos finales de bajo del
							 * subelemento de informacion
							 */
							while (allFinalElementsIterator.hasNext()) {
								/* Crea un nuevo vector en la referencia pasada */
								singleElementsDmp = new Vector<String>();

								/*
								 * Obtiene elemento final, directamente debajo
								 * de subelemento
								 */
								Element finalElement = (Element) allFinalElementsIterator
										.next();
								Element finalElementClone = finalElement
										.clone();
								// finalElementClone.setNamespace(namespace);

								// Element finalElementClone = new
								// Element(finalElement.getName());
								// finalElementClone.addContent(finalElement.getText());

								/*
								 * Procesa texto del elemento y lo agrega a la
								 * jerarquia
								 */
								finalElementClone = buscarTagsPredeterminados(
										tagsEspeciales, tagsGenerales,
										finalElementClone, "oai_dmp");
								subElementClone = procesarTagsDmp(
										subElementClone, singleElementsDmp,
										allFinalElementsIterator,
										finalElementClone, namespace);

							}

						}

					}

				}

			}
		}

		return outputElement;
	}

	/**
	 * Busca en cada tag similitud con alg_n tag predeterminado en el archivo
	 * xml para reemplazarlo por el texto est_ndar aceptado (Planteado en el
	 * archivo xml tambi_n)
	 * 
	 * @param tagsEspeciales
	 * @param tagsGenerales
	 * @param elementParameter
	 * @return elementParameter Element
	 * @author Daniel
	 */
	private Element buscarTagsPredeterminados(Vector<MyPattern> tagsEspeciales,
			Vector<MyPattern> tagsGenerales, Element elementParameter,
			String metadataPrefix) {

		/* Obtengo texto del elementoParametro para evaluar y settear de nuevo */
		String elementStr = elementParameter.getText();
		if (tagsEspeciales != null) {
			for (int cont = 0; cont < tagsEspeciales.size(); cont++) {
				if (tagsEspeciales.get(cont).tag
						.equalsIgnoreCase(elementParameter.getName())) {
					if (elementParameter.getText().contains(
							tagsEspeciales.get(cont).oldPattern)) {
						elementStr = elementStr.replace(tagsEspeciales
								.get(cont).getOldPattern(),
								tagsEspeciales.get(cont).getNewPattern());
						elementParameter.setText(elementStr);
						break;
					}
				}
			}
		}

		/* Verifica si se realizan reeemplazos de tags generales */
		if (tagsGenerales != null) {
			for (int cont = 0; cont < tagsGenerales.size(); cont++) {

				if (tagsGenerales.get(cont).tag
						.equalsIgnoreCase(elementParameter.getName())) {
					if (elementParameter.getText().contains(
							tagsGenerales.get(cont).oldPattern)) {
						elementStr = elementStr.replace(tagsGenerales.get(cont)
								.getOldPattern(), tagsGenerales.get(cont)
								.getNewPattern());
						elementParameter.setText(elementStr);
						break;
					}
				}
			}
		}

		return CheckVocabulary(elementParameter, metadataPrefix);
	}

	/**
	 * Esta funcion lo que hace es que hace una validacion de vocabulario
	 * mientras se recolecta
	 * 
	 * @param element
	 * @param metadataPrefix
	 * @return element
	 * @author luis
	 */
	private Element CheckVocabulary(Element element, String metadataPrefix) {

		element.setText(element.getText().toLowerCase()); // pasamos todo a
															// minuscula para
															// hacer mas facil
															// la comparacion

		HashMap<String, ArrayList<String>> Vocabulary;
		if (metadataPrefix.equalsIgnoreCase("oai_dc"))
			Vocabulary = tagsDcValidation;
		else if (metadataPrefix.equalsIgnoreCase("oai_lomco"))
			Vocabulary = tagsLomCoValidation;
		else
			return element; // prevent change

		if (Vocabulary.containsKey(element.getName())) { // tiene vocabulario
															// controlado
			ArrayList<String> vocabulario = Vocabulary.get(element.getName());

			if (element.getText().indexOf("-") != -1)
				element = element.setText(element.getText()
						.replaceAll("-", " ")); // esto lo realizamos para
												// eliminar los guiones

			/*
			 * version 1.4.3: element.getText().isEmpty() version 2.0:
			 * element.getText() == null
			 */
			if (!vocabulario.contains(element.getText().toLowerCase())
					|| element.getText() == null
					|| element.getText().length() == 0) {
				element.setText("General"); // con este text quedaran los
											// elementos que tienen vocabulario
											// controlado y que vienen con uno
											// diferente o viene vacio
			}

		}

		return element;
	}

	/**
	 * Realiza verificaciones en el elemento parametro para su adicion al
	 * documento salida, construyendo la jerarquia de niveles proveniente del
	 * modelo de metadatos de objetos de aprendizaje mientras procesa tags
	 * especiales del mismo.
	 * 
	 * @param outputElement
	 * @param singleElements
	 * @param lastIterator
	 * @param elementParameter
	 * @param lom
	 *            Namespace
	 * @return outputElement Element
	 * @author Daniel
	 */
	private Element procesarTagsLomIEEE(Element outputElement,
			Vector<String> singleElementsLom, Iterator lastIterator,
			Element elementParameter, Namespace lom) {

		boolean exists = false;
		/* Obtengo los hijos del elemento parametro */
		List<Element> children = elementParameter.getChildren();

		/*
		 * Verifica si no contiene hijos, debido a que si no tiene niveles por
		 * debajo, este ser_ un elemento final de la jerarquia, y contendr_
		 * texto.
		 */
		// System.out.println("Element parameter name >>> >> >> >> >> "+
		// elementParameter.getName());
		// System.out.println("children size:"+ children.size());
		if (children.size() == 0) {
			// System.out.println("Elemento con texto, ultimo en la jerarquia");
			/*
			 * Con tiene texto, analiza que no sea un elemento previamente
			 * agregado, y por ende repetido
			 */
			String elementStr = elementParameter.getText();

			for (int j = 0; j < singleElementsLom.size(); j++) {
				if (singleElementsLom.get(j).equalsIgnoreCase(elementStr)) {
					if (lastIterator.hasNext()) { // added
						lastIterator.remove();
						exists = true;
						break;
					}

				}
			}

			if (!exists) {
				/* Es un elemento con texto nuevo, agrega al documento */

				singleElementsLom.add(elementStr);

				// Element elementParameterNoNamespace = new
				// Element(elementParameter.getName());
				// elementParameterNoNamespace.addContent(elementStr);
				// outputElement.addContent(elementParameterNoNamespace);
				outputElement.addContent(elementParameter);

				// if
				// (contieneCaracteresEspeciales(elementParameter.getText())){
				// crearElementoLimpio(outputElement, elementParameter, lom);}

				Element parentElement = elementParameter.getParentElement();

				/* Busca el tipo de coleccion proveniente en los tags LOM */

				// if (parentElement.getName().equals("contribute")
				// && elementParameter.getName().equals("entity")) {
				// System.out.println(parentElement.getName());
				// /* Revisa que tipo de contribucion es y asigna la informaci_n
				// */
				// if(parentElement.getChild("role",
				// lom).getChild("value",lom).getText().equals("author")
				// &&(sCreator==null)){
				// sCreator=elementStr;
				//
				// }else if(parentElement.getChild("role"/*,
				// lom*/).getChild("value"/*,lom*/).getText().equals("publisher")
				// &&(sPublisher==null)){
				// sPublisher=elementStr;
				//
				// }
				// }else

				// if (parentElement.getName().equals("learningResourceType")
				// && elementParameter.getName().equals("value")) {
				// if (elementParameter.getText().length()!=0) {
				// sLearningObjectType = elementStr;
				// }
				// }else
				//
				if (parentElement.getName().equals("technical")
						&& elementParameter.getName().equals("location")) {
					if (elementParameter.getText().length() != 0
							&& elementParameter.getText().substring(0, 4)
									.equalsIgnoreCase("http")) {
						sLinkIdentifier = elementStr;
					}
				} else

				if (parentElement.getName().equals("date")
						&& elementParameter.getName().equalsIgnoreCase(
								"dateTime")) {
					sYear = elementStr;
					if (sYear.length() > 3) {
						String strYear = obtenerAnio(sYear).toString();
						if (!strYear.equalsIgnoreCase("error")) {
							sYear = strYear;

						} else {
							sYear = "";
						}

					}
				}

				// End

			}

		} else {
			/* El elemento no contiene texto, es un tag que contiene subniveles */
			List<Element> hijosTemp = outputElement
					.getChildren(elementParameter.getName());
			Iterator<Element> hijoTemp = hijosTemp.iterator();
			List<Element> elements = elementParameter.getChildren();
			Iterator<Element> elementsIterator = elements.iterator();
			boolean repeat = false;
			int contador = 0;
			/*
			 * Analiza si este tag(elemento) ha sido previamente agregado
			 * obteniendo los hijos de su padre con mismo tag
			 */
			if (hijosTemp.size() != 0) {
				while (hijoTemp.hasNext()) {

					/*
					 * Hijo con mismo nombre encontrado, revisa que no exista
					 * similitud en sus subelementos para evitar repetidos
					 */
					List<Element> tempElements = hijoTemp.next().getChildren();

					// String textoElemento=null;
					while (elementsIterator.hasNext()) {

						/*
						 * textoElemento = elementsIterator.next().getText();
						 * for (int j = 0; j < singleElementsLom.size(); j++) {
						 * if (singleElementsLom.get(j).equalsIgnoreCase(
						 * textoElemento)) { contador++; } }
						 */
						if (tempElements.contains(elementsIterator.next()
								.getText())) {
							contador++;
						}
					}

				}
				if (contador == elements.size() && lastIterator.hasNext()) { // added
					lastIterator.remove();
					repeat = true;
				}

				if (!repeat) {
					/*
					 * SubElementos no son iguales, se trata de otro tag con
					 * mismo nombre pero diferente contenido, agrega el elemento
					 * a la jerarquia eliminando el contenido para asi solo
					 * agregar el tag. Sus subelementos seran agregados en
					 * ciclos posteriores.
					 */
					elementParameter.removeContent();
					// Element elementParameterNoNamespace = new
					// Element(elementParameter.getName());
					// outputElement.addContent(elementParameterNoNamespace);
					outputElement.addContent(elementParameter);
				}

			} else {
				/*
				 * Hijo no agregado previamente, agrega el nuevo elemento a la
				 * jerarquia realizando el mismo borrado de contenido explicado
				 * anteriormente.
				 */
				elementParameter.removeContent();
				// Element elementParameterNoNamespace = new
				// Element(elementParameter.getName());
				// outputElement.addContent(elementParameterNoNamespace);
				outputElement.addContent(elementParameter);
			}

		}
		return outputElement;
	}

	/**
	 * Realiza verificaciones en el elemento parametro para su adicion al
	 * documento salida, construyendo la jerarquia de niveles proveniente del
	 * modelo de metadatos de objetos de aprendizaje mientras procesa tags
	 * especiales del mismo.
	 * 
	 * @param outputElement
	 * @param singleElements
	 * @param lastIterator
	 * @param elementParameter
	 * @param dmp
	 *            Namespace
	 * @return outputElement Element
	 * @author Edwin
	 */
	private Element procesarTagsDmp(Element outputElement,
			Vector<String> singleElementsDmp, Iterator lastIterator,
			Element elementParameter, Namespace dmp) {

		boolean exists = false;
		/* Obtengo los hijos del elemento parametro */
		List<Element> children = elementParameter.getChildren();

		/*
		 * Verifica si no contiene hijos, debido a que si no tiene niveles por
		 * debajo, este ser_ un elemento final de la jerarquia, y contendr_
		 * texto.
		 */
		// System.out.println("Element parameter name >>> >> >> >> >> "+
		// elementParameter.getName());
		// System.out.println("children size:"+ children.size());
		if (children.size() == 0) {
			// System.out.println("Elemento con texto, ultimo en la jerarquia");
			/*
			 * Con tiene texto, analiza que no sea un elemento previamente
			 * agregado, y por ende repetido
			 */
			String elementStr = elementParameter.getText();

			for (int j = 0; j < singleElementsDmp.size(); j++) {
				if (singleElementsDmp.get(j).equalsIgnoreCase(elementStr)) {
					if (lastIterator.hasNext()) { // added
						lastIterator.remove();
						exists = true;
						break;
					}

				}
			}

			if (!exists) {
				/* Es un elemento con texto nuevo, agrega al documento */

				singleElementsDmp.add(elementStr);

				// Element elementParameterNoNamespace = new
				// Element(elementParameter.getName());
				// elementParameterNoNamespace.addContent(elementStr);
				// outputElement.addContent(elementParameterNoNamespace);
				outputElement.addContent(elementParameter);

				// if
				// (contieneCaracteresEspeciales(elementParameter.getText())){
				// crearElementoLimpio(outputElement, elementParameter, lom);}

				Element parentElement = elementParameter.getParentElement();

				/* Busca el tipo de coleccion proveniente en los tags LOM */

				// if (parentElement.getName().equals("contribute")
				// && elementParameter.getName().equals("entity")) {
				// System.out.println(parentElement.getName());
				// /* Revisa que tipo de contribucion es y asigna la informaci_n
				// */
				// if(parentElement.getChild("role",
				// lom).getChild("value",lom).getText().equals("author")
				// &&(sCreator==null)){
				// sCreator=elementStr;
				//
				// }else if(parentElement.getChild("role"/*,
				// lom*/).getChild("value"/*,lom*/).getText().equals("publisher")
				// &&(sPublisher==null)){
				// sPublisher=elementStr;
				//
				// }
				// }else

				// if (parentElement.getName().equals("learningResourceType")
				// && elementParameter.getName().equals("value")) {
				// if (elementParameter.getText().length()!=0) {
				// sLearningObjectType = elementStr;
				// }
				// }else
				//
				if (parentElement.getName().equals("technical")
						&& elementParameter.getName().equals("location")) {
					if (elementParameter.getText().length() != 0
							&& elementParameter.getText().substring(0, 4)
									.equalsIgnoreCase("http")) {
						sLinkIdentifier = elementStr;
					}
				} else

				if (parentElement.getName().equals("date")
						&& elementParameter.getName().equalsIgnoreCase(
								"dateTime")) {
					sYear = elementStr;
					if (sYear.length() > 3) {
						String strYear = obtenerAnio(sYear).toString();
						if (!strYear.equalsIgnoreCase("error")) {
							sYear = strYear;

						} else {
							sYear = "";
						}

					}
				}

				// End

			}

		} else {
			/* El elemento no contiene texto, es un tag que contiene subniveles */
			List<Element> hijosTemp = outputElement
					.getChildren(elementParameter.getName());
			Iterator<Element> hijoTemp = hijosTemp.iterator();
			List<Element> elements = elementParameter.getChildren();
			Iterator<Element> elementsIterator = elements.iterator();
			boolean repeat = false;
			int contador = 0;
			/*
			 * Analiza si este tag(elemento) ha sido previamente agregado
			 * obteniendo los hijos de su padre con mismo tag
			 */
			if (hijosTemp.size() != 0) {
				while (hijoTemp.hasNext()) {

					/*
					 * Hijo con mismo nombre encontrado, revisa que no exista
					 * similitud en sus subelementos para evitar repetidos
					 */
					List<Element> tempElements = hijoTemp.next().getChildren();

					// String textoElemento=null;
					while (elementsIterator.hasNext()) {

						/*
						 * textoElemento = elementsIterator.next().getText();
						 * for (int j = 0; j < singleElementsLom.size(); j++) {
						 * if (singleElementsLom.get(j).equalsIgnoreCase(
						 * textoElemento)) { contador++; } }
						 */
						if (tempElements.contains(elementsIterator.next()
								.getText())) {
							contador++;
						}
					}

				}
				if (contador == elements.size() && lastIterator.hasNext()) { // added
					lastIterator.remove();
					repeat = true;
				}

				if (!repeat) {
					/*
					 * SubElementos no son iguales, se trata de otro tag con
					 * mismo nombre pero diferente contenido, agrega el elemento
					 * a la jerarquia eliminando el contenido para asi solo
					 * agregar el tag. Sus subelementos seran agregados en
					 * ciclos posteriores.
					 */
					elementParameter.removeContent();
					// Element elementParameterNoNamespace = new
					// Element(elementParameter.getName());
					// outputElement.addContent(elementParameterNoNamespace);
					outputElement.addContent(elementParameter);
				}

			} else {
				/*
				 * Hijo no agregado previamente, agrega el nuevo elemento a la
				 * jerarquia realizando el mismo borrado de contenido explicado
				 * anteriormente.
				 */
				elementParameter.removeContent();
				// Element elementParameterNoNamespace = new
				// Element(elementParameter.getName());
				// outputElement.addContent(elementParameterNoNamespace);
				outputElement.addContent(elementParameter);
			}

		}
		return outputElement;
	}

	/**
	 * Procesa tags especiales del modelo de metadatos Doublin Core
	 * 
	 * @param outputElement
	 * @param singleElements
	 * @param lastIterator
	 * @param elementParameter
	 * @param outputElement
	 * @param singleElements
	 * @param lastIterator
	 * @param elementParameter
	 * @param namespace
	 * @param general
	 * @param lifeCycle
	 * @param educational
	 * @param technical
	 * @param rights
	 * @param contributeAuthor
	 * @param contributePublisher
	 * @param dc
	 *            Namespace
	 * @return outputElement Element
	 * @author Daniel
	 */
	private Element procesarTagsDc(Element outputElement,
			Vector<String> singleElements, Iterator lastIterator,
			Element elementParameter, Namespace namespace/*
														 * , Element general,
														 * Element lifeCycle,
														 * Element educational,
														 * Element technical,
														 * Element rights,
														 * Element
														 * contributeAuthor,
														 * Element
														 * contributePublisher
														 */) {
		boolean exists = false;
		String nombreElemento = elementParameter.getName();
		String elementStr = elementParameter.getText();
		for (int j = 0; j < singleElements.size(); j++) {
			if (singleElements.get(j).equalsIgnoreCase(elementStr)) {
				lastIterator.remove();
				exists = true;
				break;
			}
		}
		if (!exists) {

			/* Verificacion de tag name para crear jerarquia DC Cualificado */
			if (nombreElemento.equalsIgnoreCase("identifier")) {
				outputElement.addContent(new Element("general.identifier",
						namespace).setText(elementStr));
				singleElements.add(elementStr);
				// general.addContent(new Element("identifier").addContent(new
				// Element("entry").setText(elementStr)));

				if (sLinkIdentifier == null) {
					if (elementStr.length() >= 4) {
						if (elementStr.substring(0, 4).equalsIgnoreCase("http")) {
							sLinkIdentifier = elementStr;
						}
					}
				}
			} else if (nombreElemento.equalsIgnoreCase("title")) {
				// general.addContent(new Element("title").setText(elementStr));
				outputElement
						.addContent(new Element("general.title", namespace)
								.setText(elementStr));
				singleElements.add(elementStr);
			} else if (nombreElemento.equalsIgnoreCase("language")) {
				outputElement.addContent(new Element("general.language",
						namespace).setText(elementStr));
				// general.addContent(new
				// Element("language").setText(elementStr));
				singleElements.add(elementStr);
			} else if (nombreElemento.equalsIgnoreCase("description")) {
				outputElement.addContent(new Element("general.description",
						namespace).setText(elementStr));
				// general.addContent(new
				// Element("description").setText(elementStr));
				singleElements.add(elementStr);
			} else if (nombreElemento.equalsIgnoreCase("subject")) {
				outputElement.addContent(new Element("general.keyword",
						namespace).setText(elementStr));
				// general.addContent(new
				// Element("keyword").setText(elementStr));
				singleElements.add(elementStr);
			} else if (nombreElemento.equalsIgnoreCase("coverage")) {
				outputElement.addContent(new Element("general.coverage",
						namespace).setText(elementStr));
				// general.addContent(new
				// Element("coverage").setText(elementStr));
				singleElements.add(elementStr);
			} else if (nombreElemento.equalsIgnoreCase("creator")) {
				outputElement.addContent(new Element(
						"lifecycle.contribute.role.author", namespace)
						.setText(elementStr));
				singleElements.add(elementStr);
				// contributeAuthor.addContent(new
				// Element("role").setText("author"));
				// contributeAuthor.addContent(new
				// Element("entity").setText(elementStr));
			} else if (nombreElemento.equalsIgnoreCase("publisher")) {
				outputElement.addContent(new Element(
						"lifecycle.contribute.role.publisher", namespace)
						.setText(elementStr));
				singleElements.add(elementStr);
				// contributePublisher.addContent(new
				// Element("entity").setText(elementStr));
			} else if (nombreElemento.equalsIgnoreCase("date")) {
				outputElement.addContent(new Element(
						"lifecycle.contribute.role.publisher.date.datetime",
						namespace).setText(elementStr));
				// contributePublisher.addContent(new
				// Element("role").setText("publisher"));
				// contributePublisher.addContent(new
				// Element("date").addContent(new
				// Element("datetime").setText(elementStr)));

				singleElements.add(elementStr);
				sYear = elementStr;
				Element eYear = new Element("year", namespace);
				if (sYear.length() >= 4) { // solucion
											// provisional.
											// Luis
											// Laverde
					String strYear = obtenerAnio(sYear).toString();
					if (!strYear.equalsIgnoreCase("error")) {
						eYear.setText(strYear);
					} else {
						eYear.setText("");
					}
					outputElement.addContent(eYear);
				}

			} else if (nombreElemento.equalsIgnoreCase("type")) {
				outputElement.addContent(new Element(
						"educational.learningResourceType", namespace)
						.setText(elementStr));
				// general.addContent(new
				// Element("learningResourceType").setText(elementStr));
				singleElements.add(elementStr);
				docType = elementStr;
			} else if (nombreElemento.equalsIgnoreCase("format")) {
				outputElement.addContent(new Element("technical.format",
						namespace).setText(elementStr));
				singleElements.add(elementStr);
				// technical.addContent(new
				// Element("format").setText(elementStr));
			} else if (nombreElemento.equalsIgnoreCase("rights")) {
				outputElement.addContent(new Element("rights.description",
						namespace).setText(elementStr));
				singleElements.add(elementStr);
				// technical.addContent(new Element("rights").addContent(new
				// Element("description").setText(elementStr)));
			} else

			/* Creacion de jerarquia lifeCycle */
			// if(contributeAuthor.getChildren().size()!=0)
			// lifeCycle.addContent(contributeAuthor);
			//
			// if(contributePublisher.getChildren().size()!=0)
			// lifeCycle.addContent(contributePublisher);

			/*
			 * Relation puede tener multiples entradas, se agrega al final y una
			 * por cada entrada
			 */
			if (nombreElemento.equalsIgnoreCase("relation")) {
				outputElement.addContent(new Element(
						"relation.resource.description", namespace)
						.setText(elementStr));
				singleElements.add(elementStr);
				// Element relation = new Element("relation").addContent(new
				// Element("resource").addContent(new
				// Element("description").setText(elementStr)));
				// outputElement.addContent(relation);
			} else if (nombreElemento.equalsIgnoreCase("source")) {
				outputElement.addContent(new Element(
						"relation.kind.isBasedOn.resource", namespace)
						.setText(elementStr));
				singleElements.add(elementStr);
				/*
				 * Source no es multiple, pero no se suele hallar comunmente,
				 * ahorro de creacion
				 */
				// Element relation = new Element("relation").addContent(new
				// Element("kind").setText("isBasedOn"));
				// relation.addContent(new
				// Element("resource").setText(elementStr));
				// outputElement.addContent(relation);
			}

			/*
			 * CODIGO PARA EJECUTAR UN PROCESAMIENTO DC PURO SIN MAPEADO A LOM
			 */

			// singleElements.add(elementStr);
			// Element elementParameterClone = (Element)
			// elementParameter.clone();
			// outputElement.addContent(elementParameterClone);

			// if (contieneCaracteresEspeciales(elementParameter
			// .getText()))
			// crearElementoLimpio(outputElement, elementParameter,namespace);
			// recordsOutput.addContent((Element)e6.clone());

			// Add by Edwin, Jan 30 2011
			// if (elementParameter.getName().equals("type")) {
			// docType = elementStr;
			// // System.out.println("campo="+e6.getName()+"val="+element);
			// }else
			// // End
			// // Add by Edwin, Aug 25 2011
			//
			// if (elementParameter.getName().equals("date")) {
			// sYear = elementStr;
			// Element eYear = new Element("year"/*, dc*/);
			// if (sYear.length() >= 4) { // solucion
			// // provisional.
			// // Luis
			// // Laverde
			// String strYear=obtenerAnio(sYear).toString();
			// if(!strYear.equalsIgnoreCase("error")){
			// eYear.setText(strYear);
			// }else{
			// eYear.setText("");
			// }
			// outputElement.addContent(eYear);
			// }
			// }
			// else
			//
			// if (elementParameter.getName().equals("identifier")&&
			// (sLinkIdentifier==null)) {
			// if (elementStr.length() >= 4){
			// if(elementStr.substring(0, 4).equalsIgnoreCase("http")){
			// sLinkIdentifier = elementStr;
			// }
			// }
			// }
			// End

		}
		return outputElement;
	}

	/**
	 * Procesa tags especiales del modelo de metadatos LOM_CO
	 * 
	 * @param outputElement
	 * @param singleElements
	 * @param lastIterator
	 * @param elementParameter
	 * @param dc
	 *            Namespace
	 * @return outputElement Element
	 * @author Daniel
	 */
	private Element procesarTagsLomCo(Element outputElement,
			Vector<String> singleElements, Iterator lastIterator,
			Element elementParameter, Namespace namespace) {
		boolean exists = false;
		String nombreElemento = elementParameter.getName();
		String elementStr = elementParameter.getText();
		for (int j = 0; j < singleElements.size(); j++) {
			if (singleElements.get(j).equalsIgnoreCase(elementStr)) {
				lastIterator.remove();
				exists = true;
				break;
			}
		}
		if (!exists) {
			/* Verificacion de tag name para crear jerarquia DC Cualificado */
			if (nombreElemento.equalsIgnoreCase("nid")) {
				outputElement.addContent(new Element("general.identifier",
						namespace).setText(elementStr));
				singleElements.add(elementStr);
			} else if (nombreElemento.equalsIgnoreCase("title")) {
				outputElement
						.addContent(new Element("general.title", namespace)
								.setText(elementStr));
				singleElements.add(elementStr);
			} else if (nombreElemento.equalsIgnoreCase("language")) {
				outputElement.addContent(new Element("general.language",
						namespace).setText(elementStr));
				singleElements.add(elementStr);
			} else if (nombreElemento.equalsIgnoreCase("description")) {
				outputElement.addContent(new Element("general.description",
						namespace).setText(elementStr));
				singleElements.add(elementStr);
			} else if (nombreElemento.equalsIgnoreCase("keywords")) {
				outputElement.addContent(new Element("general.keyword",
						namespace).setText(elementStr));
				singleElements.add(elementStr);
			} else if (nombreElemento.equalsIgnoreCase("coverage")) {
				outputElement.addContent(new Element("general.coverage",
						namespace).setText(elementStr));
				singleElements.add(elementStr);
			} else if (nombreElemento.equalsIgnoreCase("version")) {
				outputElement.addContent(new Element("lifecycle.version",
						namespace).setText(elementStr));
				singleElements.add(elementStr);
			} else if (nombreElemento.equalsIgnoreCase("author")) {
				outputElement.addContent(new Element(
						"lifecycle.contribute.role.author", namespace)
						.setText(elementStr));
				singleElements.add(elementStr);
			} else if (nombreElemento.equalsIgnoreCase("publisher")
					|| nombreElemento.equalsIgnoreCase("editor")
					|| nombreElemento.equalsIgnoreCase("compiler")) {
				outputElement.addContent(new Element(
						"lifecycle.contribute.role.publisher", namespace)
						.setText(elementStr));
				singleElements.add(elementStr);
			} else if (nombreElemento.equalsIgnoreCase("date")) {
				outputElement.addContent(new Element(
						"lifecycle.contribute.role.publisher.date.datetime",
						namespace).setText(elementStr));

				singleElements.add(elementStr);
				sYear = elementStr;
				Element eYear = new Element("year", namespace);
				if (sYear.length() >= 4) { // solucion
											// provisional.
											// Luis
											// Laverde
					String strYear = obtenerAnio(sYear).toString();
					if (!strYear.equalsIgnoreCase("error")) {
						eYear.setText(strYear);
					} else {
						eYear.setText("");
					}
					outputElement.addContent(eYear);
				}
			} else if (nombreElemento.equalsIgnoreCase("type_resource")) {
				outputElement.addContent(new Element(
						"educational.learningResourceType", namespace)
						.setText(elementStr));
				singleElements.add(elementStr);
				// docType = elementStr;
			} else if (nombreElemento.equalsIgnoreCase("level_int")) {
				outputElement.addContent(new Element(
						"educational.interactivityLevel", namespace)
						.setText(elementStr));
			} else if (nombreElemento.equalsIgnoreCase("type_int")) {
				outputElement.addContent(new Element(
						"educational.interactivityType", namespace)
						.setText(elementStr));
			} else if (nombreElemento.equalsIgnoreCase("focal_group")) {
				outputElement.addContent(new Element(
						"educational.intentedUserRole", namespace)
						.setText(elementStr));
			} else if (nombreElemento.equalsIgnoreCase("context")) {
				outputElement.addContent(new Element("educational.context",
						namespace).setText(elementStr));
			} else if (nombreElemento.equalsIgnoreCase("suggestions_for_use")) {
				outputElement.addContent(new Element("educational.description",
						namespace).setText(elementStr));
			} else if (nombreElemento.equalsIgnoreCase("format")) {
				outputElement.addContent(new Element("technical.format",
						namespace).setText(elementStr));
				singleElements.add(elementStr);
			} else if (nombreElemento.equalsIgnoreCase("size")) {
				outputElement.addContent(new Element("technical.size",
						namespace).setText(elementStr));
				singleElements.add(elementStr);
			} else if (nombreElemento.equalsIgnoreCase("location")) {
				outputElement.addContent(new Element("technical.location",
						namespace).setText(elementStr));
				singleElements.add(elementStr);
				if (sLinkIdentifier == null) {
					if (elementStr.length() >= 4) {
						if (elementStr.substring(0, 4).equalsIgnoreCase("http")) {
							sLinkIdentifier = elementStr;
						}
					}
				}
			} else if (nombreElemento.equalsIgnoreCase("requirements")) {
				outputElement.addContent(new Element("technical.requirement",
						namespace).setText(elementStr));
				singleElements.add(elementStr);

			} else if (nombreElemento.equalsIgnoreCase("installation")) {
				outputElement.addContent(new Element(
						"technical.installationremarks", namespace)
						.setText(elementStr));
				singleElements.add(elementStr);

			} else if (nombreElemento.equalsIgnoreCase("duration")) {
				outputElement.addContent(new Element("technical.duration",
						namespace).setText(elementStr));
				singleElements.add(elementStr);

			} else if (nombreElemento.equalsIgnoreCase("cost")) {
				outputElement.addContent(new Element("rights.cost", namespace)
						.setText(elementStr));
				singleElements.add(elementStr);

			} else if (nombreElemento
					.equalsIgnoreCase("copyright_and_other_restrictions")) {
				outputElement.addContent(new Element(
						"rights.copyrightandotherrestrictions", namespace)
						.setText(elementStr));
				singleElements.add(elementStr);

			} else if (nombreElemento.equalsIgnoreCase("rights")) {
				outputElement.addContent(new Element("rights.description",
						namespace).setText(elementStr));
				singleElements.add(elementStr);

			} else /*
					 * Relation puede tener multiples entradas, se agrega al
					 * final y una por cada entrada
					 */
			if (nombreElemento.equalsIgnoreCase("relation")) {
				outputElement.addContent(new Element(
						"relation.resource.description", namespace)
						.setText(elementStr));
				singleElements.add(elementStr);

			} else if (nombreElemento.equalsIgnoreCase("source")) {
				outputElement.addContent(new Element("classification.keyword",
						namespace).setText(elementStr));
				singleElements.add(elementStr);

			} else if (nombreElemento.equalsIgnoreCase("annotation")) {
				outputElement.addContent(new Element("annotation.description",
						namespace).setText(elementStr));
				singleElements.add(elementStr);

			} else if (nombreElemento.equalsIgnoreCase("path")) {
				outputElement.addContent(new Element(
						"classification.taxonpath.source", namespace)
						.setText(elementStr));
				singleElements.add(elementStr);

			} else if (nombreElemento.equalsIgnoreCase("category")) {
				outputElement.addContent(new Element(
						"classification.taxonpath.source", namespace)
						.setText(elementStr));
				singleElements.add(elementStr);
			}

			/*
			 * if (elementParameter.getName().equals("identifier")&&
			 * (sLinkIdentifier==null)) { if (elementStr.length() >= 4){
			 * if(elementStr.substring(0, 4).equalsIgnoreCase("http")){
			 * sLinkIdentifier = elementStr; } } }
			 */

			/*
			 * CODIGO PARA EJECUTAR UN PROCESAMIENTO DC PURO SIN MAPEADO A LOM
			 */

			// singleElements.add(elementStr);
			// outputElement.addContent((Element) elementParameter.clone());
			//
			// // if (contieneCaracteresEspeciales(elementParameter
			// // .getText()))
			// // crearElementoLimpio(outputElement, elementParameter,
			// namespace);
			// // recordsOutput.addContent((Element)e6.clone());
			//
			// // Add by Edwin, Jan 30 2011
			// // if (elementParameter.getName().equals("type")) {
			// // docType = elementStr;
			// // // System.out.println("campo="+e6.getName()+"val="+element);
			// // }else
			// // End
			// // Add by Edwin, Aug 25 2011
			//
			// if (elementParameter.getName().equals("date")) {
			// sYear = elementStr;
			// Element eYear = new Element("year"/*, dc*/);
			// if (sYear.length() >= 4) { // solucion
			// // provisional.
			// // Luis
			// // Laverde
			// String strYear=obtenerAnio(sYear).toString();
			// if(!strYear.equalsIgnoreCase("error")){
			// eYear.setText(strYear);
			// }else{
			// eYear.setText("");
			// }
			// outputElement.addContent(eYear);
			// }
			// }
			// else
			//
			// if (elementParameter.getName().equals("location")&&
			// (sLinkIdentifier==null)) {
			// if (elementStr.length() >= 4){
			// if(elementStr.substring(0, 4).equalsIgnoreCase("http")){
			// sLinkIdentifier = elementStr;
			// }
			// }
			// }
			// // End
			//
		}
		return outputElement;
	}

	/**
	 * Genera la transformacion hacia dublin core cualificado recursivamente
	 * 
	 * @param elemento
	 * @param namespace
	 * @param recordsOutputDCQ
	 * @return
	 */
	public Element crearJerarquiaDublinCoreQ(Element elemento,
			Namespace namespace, String topElementName) {

		if (elemento.getChildren().size() == 0) {
			Element parentElement = elemento.getParentElement();
			ArrayList<String> hierarchyArray = new ArrayList<String>();
			String hierarchy = "";
			while (parentElement != null
					&& (!parentElement.getName().equalsIgnoreCase(
							topElementName))) {
				if (first) {
					hierarchyArray.add(parentElement.getName());
					first = false;
				} else {
					hierarchyArray.add(parentElement.getName() + ".");
				}
				parentElement = parentElement.getParentElement();
			}
			Collections.reverse(hierarchyArray);
			// if(hierarchyArray.size()==0){
			// hierarchyArray.add("."+elemento.getName());
			// }else{
			hierarchyArray.add(elemento.getName());
			// }
			Iterator iteratorArray = hierarchyArray.iterator();

			while (iteratorArray.hasNext()) {
				hierarchy += iteratorArray.next();
			}
			return recordsOutputDCQ
					.addContent(new Element(hierarchy, namespace)
							.setText(elemento.getText()));
		} else {
			Iterator iteratorChildren = elemento.getChildren().iterator();
			while (iteratorChildren.hasNext()) {
				Element test = (Element) iteratorChildren.next();
				crearJerarquiaDublinCoreQ(test, namespace, topElementName);
			}
			return null;
		}

	}

	/**
	 * Inicia el procesamiento de archivos dependiendo de el tipo de protocolo y
	 * metadatos
	 * 
	 * @param xmlFormat
	 * @param path
	 * @param filename
	 * @param dp
	 * @param fecha
	 */
	public void procesarArchivo(String xmlFormat, String path, String filename,
			DataProvider dp, String fecha) {

		// System.out.println("Formato archivo xml="+xmlFormat+", cdata="+dp.getCdata());
		String pathXMLProcessing = path;
		if (dp.getCdata() == 1) {
			procesarCdata(path, filename, fecha);
			pathXMLProcessing = pathXMLProcessing.replace(ServerConfXml.DBXML
					+ "temp" + fecha, ServerConfXml.DBXML);
		}

		if (/*
			 * (xmlFormat.equalsIgnoreCase("oai_dc")||xmlFormat.equalsIgnoreCase(
			 * "oai_lom"))&&
			 */dp.getProtocol().equalsIgnoreCase("OAI")) {
			procesarXmlOai(pathXMLProcessing, filename, dp, fecha, xmlFormat);
		} else if (/* xmlFormat.equalsIgnoreCase("RDF")&& */dp.getProtocol()
				.equalsIgnoreCase("HTTP")) {
			procesarXmlHTTP(pathXMLProcessing, filename, dp, fecha);
		} else
			procesarXmlOaiDefault(pathXMLProcessing, filename, dp, fecha);

		procesarTxt(path, filename, fecha);
		/* Escribir el numero de registros en el stats manager */
		if (StatsManager.getInstance(dp) != null) {// por este if entra solo
													// cuando se hace harvester
													// + processor a la vez
			StatsManager.getInstance(dp).setNumber_registers(dp.getNrorecs()); // comentariada de prueba

			
		}
	}

	/**
	 * Es utilizado para obtener la parte del string que contenga el anio, de
	 * cualquier string obtendr_ el segmento con cuatro d_gitos consecutivos,
	 * verifica que no hayan 5 digitos consecutivos.
	 * 
	 * @param text
	 * @return StringBuilder
	 * @author Daniel
	 */
	private StringBuilder obtenerAnio(String date) {
		StringBuilder anio = new StringBuilder();
		int cont = 0;
		for (int i = 0; i < date.length(); i++) {
			Character ch = date.charAt(i);
			if (Character.isDigit(ch)) {
				cont++;
				anio.append(ch);
				if (cont == 4) {
					if ((i + 1) == date.length())
						return anio;
					else {
						ch = date.charAt(i + 1);
						if (!Character.isDigit(ch)) {
							return anio;
						} else
							break;
					}
				}
			} else {
				anio.delete(0, anio.length());
				cont = 0;
			}
		}
		anio.delete(0, anio.length());
		anio.append("error");
		return anio;
	}

	private boolean contieneCaracteresEspeciales(String text) {
		for (int i = 0; i < caracteresTildados.length; i++) {
			if (text.contains(caracteresTildados[i]))
				return true;
		}
		return false;
	}

	/**
	 * Crea un nuevo elemento eliminando caracteres especiales. Se agrega
	 * caracter x al final del nombre del tag.
	 * 
	 * @param padre
	 * @param hijo
	 * @param namespace
	 */
	private void crearElementoLimpio(Element padre, Element hijo,
			Namespace namespace) {
		Element nuevo = hijo.clone();
		nuevo.setName(nuevo.getName() + "x");
		String txt = ((Text) (nuevo.getContent()).get(0)).toString();
		if (txt.startsWith("[Text:"))
			txt = txt.substring(7, txt.length() - 1);
		nuevo.setText(limpiar(txt));
		padre.addContent(nuevo);
		return;
	}

	private String limpiar(String text) {
		for (int i = 0; i < caracteresTildados.length; i++) {
			text = text.replace(caracteresTildados[i], caracteresNoTildados[i]);
		}
		return text;
	}

	public void run() {
		System.out.println("INICIANDO EJECUCI_N DE HILO DE PROCESSOR");
		while (continuar) {
			try {
				signal.acquire();
			} catch (InterruptedException e) {
			}
			if (fileReadyQueue.size() != 0) {
				FileReady f = fileReadyQueue.poll();
				procesarArchivo(f.getXmlFormat(), f.getPath(), f.getFilename(),
						f.getDp(), "");
			}
		}
		while (fileReadyQueue.size() != 0) {
			if (fileReadyQueue.size() != 0) {
				try {
					FileReady f = fileReadyQueue.poll();
					procesarArchivo(f.getXmlFormat(), f.getPath(),
							f.getFilename(), f.getDp(), "");
				} catch (NullPointerException e) {
				}
			}
		}
		try {
			finalize();
			Thread t = Thread.currentThread();
			t.interrupt();
		} catch (Throwable e) {
			log.error(e);
		}
	}

	public void setContinuar(boolean continuar) {
		this.continuar = continuar;
	}

	private void procesarCdata(String path, String filename, String fecha) {
		// Sabemos que el path al final tiene la estructura:
		// /collection/shortName
		// De ah_ sacamos los nombres
		// SE REEMPLAZA EL ARCHIVO DE SALIDA POR EL ARCHIVO DE ENTRADA
		String pathSalida = path.replace(ServerConfXml.DBXML + "temp" + fecha,
				ServerConfXml.DBXML);
		// String pathEntrada=pathSalida;
		String xml = null;
		try {
			File f_in = new File(path + "/" + filename);
			int ch;
			StringBuffer sb = new StringBuffer("");
			FileInputStream fin = null;
			fin = new FileInputStream(f_in);
			InputStreamReader ir = new InputStreamReader(fin, "UTF-8");
			while ((ch = ir.read()) != -1) {
				sb.append((char) ch);
			}
			fin.close();
			xml = new String(sb.toString());
			// Ejecuto el reemplazo de expresion regular para el archivo
			xml = reemplazarExpresionRegularCData(xml);
			File f_out = new File(pathSalida + "/" + filename);
			FileOutputStream fos = new FileOutputStream(f_out);
			fos.write(xml.getBytes());
			fos.close();
		} catch (Exception e) {
			log.error(e);
		}
	}

	private String reemplazarExpresionRegularCData(String xml) {
		// Pattern p =
		// Pattern.compile("(?<!X)(<!\\[CDATA\\[(.*?)\\]\\]>|\\[CDATA:(.*?)\\])");
		Pattern p = Pattern.compile("(?<!X)(<!\\[CDATA\\[(.*?)\\]\\]>)");
		Matcher m = p.matcher(xml);
		StringBuffer sb = new StringBuffer();
		while (m.find()) {
			String string = m.group();
			String temp = limpiarCaracteresEspecialesCData(string);
			try {
				m.appendReplacement(sb, temp);
			} catch (IndexOutOfBoundsException e) {
			}
		}
		m.appendTail(sb);
		String rpta = sb.toString();
		return rpta;
	}

	private String limpiarCaracteresEspecialesCData(String text) {
		if (text.startsWith("<"))
			text = text.substring(9, text.length() - 3);
		else
			text = text.substring(8, text.length() - 1);
		for (int i = 0; i < caracteresInvalidosCData.length; i++) {
			text = text.replace(caracteresInvalidosCData[i],
					caracteresValidosCData[i]);
		}
		return text;
	}

	public void anunciarArchivoListo(String xmlFormat, String path,
			String filename, DataProvider dp) {
		FileReady f = new FileReady(xmlFormat, path, filename, dp);
		fileReadyQueue.add(f);
		signal.release();
	}

	public static void main(String[] args) { // uso.. dl-processor.bat
												// {Repostiorio/all} [fecha en
												// formato yyy-mm-dd]

		// long tiempoInicio = System.currentTimeMillis();
		String fecha = ""; // se inicializa vacia, para que en caso de no
							// haber fecha concatene ""
		String dl_home = null;
		String repositorio = "all";
		if (args.length > 2) { // dl_home + repositorio + fecha
			dl_home = args[0];
			repositorio = args[1];
			fecha = "-" + args[2];
		} else if (args.length > 1) { // dl_home + repositorio
			dl_home = args[0];
			repositorio = args[1];
		} else {
			System.out.println("DL_HOME no especificado");
			System.exit(0);
		}

		dlConfig properties = new dlConfig(dl_home);

		String path_temp = properties.getString("dl_metadata") + "/"
				+ ServerConfXml.DBXML + "temp" + fecha + "/";
		File dir = new File(path_temp);
		if (!dir.isDirectory() && !fecha.equals("")) {
			System.out.println("Fecha No Encontrada");
			System.exit(0);

		}
		Harvester har = new Harvester(dl_home, null);
		Collection<DataProvider> dataProviderCol = null;
		if (repositorio.equalsIgnoreCase("all"))
			dataProviderCol = har.getDataProvidersToProcces();
		else
			dataProviderCol = har
					.getDataProviderToProcess(new String[] { repositorio });

		if (dataProviderCol == null | dataProviderCol.size() == 0) {
			System.out.println("Repositorio no encontrado");
			System.exit(0);
		}

		Object[] dataProviders = dataProviderCol.toArray(); // objeto que
															// contiene
															// todos los
															// dataproviders

		// System.out.println("Cantidad de providers: "+dataProviders.length);
		for (int i = 0; i < dataProviders.length; i++) {
			DataProvider dp = (DataProvider) dataProviders[i];
			Processor p = new Processor(dl_home, i);
			String path = path_temp + dp.getShortInst() + "/"
					+ dp.getShortName();
			int doc = 0;

			while (true) {
				doc++; // esta variable se usa para recorrer los documentos
				String xmlFormat = dp.getMetadataPrefix();
				String filename = dp.getShortName() + "_" + doc + ".xml";
				// System.out.println(path+filename);
				File f = new File(path + "/" + filename);
				if (!f.exists()) {// si no existe fue porque ya termino el
									// ultimo documento, o el dir esta vacio
					// System.out.println(f+" NO EXISTE");
					break;
				}
				// System.out.println("procesando "+filename);
				p.procesarArchivo(xmlFormat, path, filename, dp, fecha);

			}
		}
		// long totalTiempo = System.currentTimeMillis() - tiempoInicio;
		// System.out.println("tiempo total de procesamiento :" +
		// totalTiempo/1000 + " segundos");
		System.out.println("PROCESAMIENTO DE ARCHIVOS FINALIZADO");

		// log.error(e);

	}

	public String getElementStr() {
		return elementStr;
	}

	public void setElementStr(String elementStr) {
		this.elementStr = elementStr;
	}
}