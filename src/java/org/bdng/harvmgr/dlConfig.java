package org.bdng.harvmgr;

import java.io.File;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.bdng.harvmgr.harvester.DataProvider;
import org.bdng.oai.db.DBExist;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;

public class dlConfig {

	Logger log = Logger.getLogger(dlConfig.class);

	String sysName = null;
	String sysNameFull = null;
	String dl_home = null;
	String dl_metadata = null;
	String dl_data = null;
	String dl_name = null;
	String dl_col = null;
	String dl_host = "localhost";
	String dl_port = "8080";
	String dl_url = null;
	String exist_uri = "xmldb:exist://" + dl_host + ":" + dl_port + "/"
			+ dl_name + "/xmlrpc";

	String exist_driver = null;
	String recordXML = null;

	String existUser = null;
	String existPass = null;

	String proxy_host = null;
	String proxy_port = null;
	String proxy = null;
	String file_server_config = null;

	String SERVER_CONFIG_TAG = "ServerConfig";

	// TODO OJO, ESTO NO DEBERIA ESTAR QUEMADO EN CODIGO
	String fileServerConfig = "conf/ServerConfig.xml";
	String oaiProviders = "conf/oai_providers.xml";
	String collections = "conf/collections.xml";
	String ListMetadataPrefix = "conf/ListMetadataPrefix.xml";
	String acceptedTags = "conf/acceptedtagsIdentify.xml";
	String validationCodesMapping = "conf/validationCodesMapping.xml";
	String validDublinCoreTags = "conf/validDublinCoreTags.xml";
	String validLomCoTags = "conf/validLomCoTags.xml";
	String schema_lomco = "conf/schemas/schema_lomco.xsd";
	String textPatterns = "conf/TextPatterns.xml";

	String identify = "conf/oai_identify.xml";
	String fileValidation = "validation/";
	String oaiProvidersStatus = "conf/oai_providers_status.xml";
	public static final String statsForHarvesterFile = "conf/statsforharvester.xml";
	public static final String statsForProviderPath = "stats";

	Document doc = null;

	/* Documento estadistica */
	private Document harvesterStats;

	public dlConfig() {
		loadProps(System.getProperty("user.dir") + "/../" + fileServerConfig);

		// System.out.println("***file:  " + System.getProperty("user.dir") +
		// "/../" + fileServerConfig);
	}

	public dlConfig(String path) {
		loadProps(path + "/" + fileServerConfig);
	}

	private void loadProps(String filename) {
		// System.out.println("--->ARCHIVO DE PROPIEDADES="+filename);
		file_server_config = filename;
		// Document doc = null;

		try {
			SAXBuilder builder = new SAXBuilder();
			doc = builder.build(filename);
		} catch (Exception e) {
			log.error(e);
		}

		dl_home = getString(doc, "dl_home");

		try {
			SAXBuilder builder = new SAXBuilder();
			doc = builder.build(dl_home + "/" + fileServerConfig); // Revisar
																	// Abril 30
																	// de 2009
		} catch (Exception e) {
			log.error(e);
		}

		dl_metadata = getString(doc, "dl_metadata");
		// System.out.println("dlConfig.loadProps:dl_metadata="+dl_metadata);
		dl_data = getString(doc, "dl_data");
		dl_name = getString(doc, "dl_name");
		dl_col = getString(doc, "dl_col");
		dl_host = getString(doc, "dl_host");
		dl_port = getString(doc, "dl_port");
		dl_url = "http://" + dl_host + ":" + dl_port + "/" + dl_name;

		// add luis- april 9 -2012
		sysName = getString(doc, "sysName");
		sysNameFull = getString(doc, "sysNameFull");

		existUser = getString(doc, "exist_user");
		existPass = getString(doc, "exist_pass");
		exist_driver = getString(doc, "exist_driver");
		// exist_uri = getString(doc,"exist_uri");

		recordXML = getString(doc, "recordXML");
		proxy_host = getString(doc, "proxy_host");
		proxy_port = getString(doc, "proxy_port");

		exist_uri = "xmldb:exist://" + dl_host + ":" + dl_port + "/" + dl_name
				+ "/xmlrpc";

		proxy = getString(doc, "proxy");
	}

	public String getString(String opcion) {
		String result = "";
		if (doc == null) {
			loadProps(fileServerConfig);
		}
		return getString(doc, opcion);
	}

	public String getString(Document doc, String option) {
		String result = "";
		try {
			Element dl = doc.getRootElement();
			Element sc = dl.getChild(SERVER_CONFIG_TAG);
			List options = sc.getChildren();
			Iterator i = options.iterator();
			while (i.hasNext()) {
				Element o = (Element) i.next();
				if (o.getName().equals(option)) {
					result = o.getText();
					break;
				}
			}
		} catch (Exception e) {
			log.error(e);
		}
		return result;
	}

	public String getSysName() {
		return sysName;
	}

	public String getSysNameFull() {
		return sysNameFull;
	}

	public String getExistUser() {
		return existUser;
	}

	public String getExistPass() {
		return existPass;
	}

	public String getfileServerConfig() {
		return file_server_config;
	}

	public String getOaiProviders() {
		// System.out.println("getOaiProviders = "+oaiProviders);
		return dl_home + "/" + oaiProviders;
	}

	public String getOaiProvidersStatus() {
		// System.out.println("getOaiProvidersStatus = "+oaiProvidersStatus);
		return dl_home + "/" + oaiProvidersStatus;
	}

	public String getFileValidation() {
		return fileValidation;
	}

	public String getStatsForHarvesterFile() {
		return dl_home + "/" + statsForHarvesterFile;
	}

	public String getStatsForProviderPath() {
		return dl_home + "/" + statsForProviderPath;
	}

	public static String getDlHome() {
		String dlhome = null;
		ResourceBundle rb = ResourceBundle.getBundle("dl");
		dlhome = rb.getString("dl_home");
		return dlhome;
	}

	public static String getDlHost() {
		String dlhost = null;
		ResourceBundle rb = ResourceBundle.getBundle("dl");
		dlhost = rb.getString("dl_host");
		return dlhost;
	}

	public static String getDlPort() {
		String dlport = null;
		ResourceBundle rb = ResourceBundle.getBundle("dl");
		dlport = rb.getString("dl_port");
		return dlport;
	}

	public String getDlName() {
		return dl_name;
	}

	public String getDlUrl() {
		/*
		 * String dlhost = null; String dlport = null; String dlname = null;
		 * 
		 * ResourceBundle rb = ResourceBundle.getBundle("dl"); dlhost =
		 * rb.getString("dl_host"); dlport = rb.getString("dl_port"); dlname =
		 * rb.getString("dl_name");
		 */

		String dlurl = null;

		dlurl = "http://" + dl_host + ":" + dl_port + "/" + dl_name;
		return dlurl;
	}

	public String getExist_uri() {
		return exist_uri;
	}

	public String getAcceptedTags() {
		return acceptedTags;
	}

	public String getValidationCodesMapping() {
		return validationCodesMapping;
	}

	public String getValidDublinCoreTags() {
		return validDublinCoreTags;
	}

	public String getValidLomCoTags() {
		return validLomCoTags;
	}

	public String getTextPatterns() {
		return textPatterns;
	}

	public String getSchema_lomco() {
		return schema_lomco;
	}

	// funcion que genera la lista del archivo statsForHarvester.xml para
	// imprimirla en el jsp
	public List GetVisualStatsForHarvester() {
		List l = null;
		try {
			SAXBuilder builder = new SAXBuilder();
			String path = (new StringBuilder(String.valueOf(dl_home)))
					.append("/").append(statsForHarvesterFile).toString();
			Document bd_xml = builder.build(path);
			Element raiz = bd_xml.getRootElement();
			// List lista = raiz.getChildren("stats");
			// Iterator i = lista.iterator();
			// Element elista = (Element)i.next();
			l = raiz.getChildren();

		} catch (Exception e) {
			log.error(e);
		}

		return l;
	}

	// funcion que genera la lista del archivo oai_providers_status.xml para
	// imprimirla en el jsp
	public List GetVisualProvidersStatus() {
		List l = null;
		try {
			SAXBuilder builder = new SAXBuilder();
			String path = (new StringBuilder(String.valueOf(dl_home)))
					.append("/").append(oaiProvidersStatus).toString();
			Document bd_xml = builder.build(path);
			Element raiz = bd_xml.getRootElement();
			List lista = raiz.getChildren("oai_providers");
			Iterator i = lista.iterator();
			Element elista = (Element) i.next();
			l = elista.getChildren();
		} catch (Exception e) {
			log.error(e);
		}
		return l;
	}

	// funcion que genera la lista del archivo oai_providers.xml para imprimirla
	// en el jsp
	public List GetVisualProviders() {
		List l = null;
		try {
			SAXBuilder builder = new SAXBuilder();
			String path = (new StringBuilder(String.valueOf(dl_home)))
					.append("/").append(oaiProviders).toString();
			Document bd_xml = builder.build(path);
			Element raiz = bd_xml.getRootElement();
			List lista = raiz.getChildren("oai_providers");
			Iterator i = lista.iterator();
			Element elista = (Element) i.next();
			l = elista.getChildren();
		} catch (Exception e) {
			System.out.println(e);
			log.error(e);
		}
		return l;
	}

	// funcion que genera la lista del archivo collections.xml para imprimirla
	// en el jsp como una lista tipo Select
	public List GetVisualCollections() {
		List l = null;
		try {
			SAXBuilder builder = new SAXBuilder();
			String path = (new StringBuilder(String.valueOf(dl_home)))
					.append("/").append(collections).toString();
			// System.out.println(path);
			Document bd_xml = builder.build(path);
			Element raiz = bd_xml.getRootElement();
			List lista = raiz.getChildren("collections");
			Iterator i = lista.iterator();
			Element elista = (Element) i.next();
			l = elista.getChildren();
		} catch (Exception e) {
			log.error(e);
		}
		return l;
	}

	/*
	 * funcion que genera la lista del archivo ListMetadataPrefix.xml para
	 * imprimirla en el jsp como una lista tipo Select que hara un JOIN con lo
	 * que se consulte por ListmedatadaFormats para el caso de OAI
	 */
	public List GetVisualListMetadataPrefix() {
		List l = null;
		try {
			SAXBuilder builder = new SAXBuilder();
			String path = (new StringBuilder(String.valueOf(dl_home)))
					.append("/").append(ListMetadataPrefix).toString();
			Document bd_xml = builder.build(path);
			Element raiz = bd_xml.getRootElement();
			List lista = raiz.getChildren("ListMetadataPrefix");
			Iterator i = lista.iterator();
			Element elista = (Element) i.next();
			l = elista.getChildren();
		} catch (Exception e) {
			log.error(e);
		}
		return l;
	}

	public List GetVisualIdentify() {
		List l = null;
		try {
			SAXBuilder builder = new SAXBuilder();
			String path = (new StringBuilder(String.valueOf(dl_home)))
					.append("/").append(identify).toString();
			Document bd_xml = builder.build(path);
			Element raiz = bd_xml.getRootElement();
			List lista = raiz.getChildren("Identify");

			l = lista;

		} catch (Exception e) {
			log.error(e);
		}
		return l;
	}

	public List GetVisualValidation(String shortname) {
		List l = null;
		try {
			SAXBuilder builder = new SAXBuilder();
			String path = dl_home + "/" + fileValidation + shortname + ".xml";
			// System.out.println("PATH Visual Validation "+path);
			Document bd_xml = builder.build(path);
			Element raiz = bd_xml.getRootElement();
			List lista = raiz.getChildren("results");
			Iterator i = lista.iterator();
			Element elista = (Element) i.next();
			l = elista.getChildren();

		} catch (Exception e) {
			log.error(e);
		}
		return l;
	}

	public String GetScoreValidation(String shortname) {
		String score = "0%";
		try {
			SAXBuilder builder = new SAXBuilder();
			String path = dl_home + "/" + fileValidation + shortname + ".xml";

			// System.out.println("cheking path..."+path);

			if (!new File(path).exists())
				return "N/A";
			// System.out.println("Found");

			Document bd_xml = builder.build(path);
			Element raiz = bd_xml.getRootElement();
			Element result = raiz.getChild("results");
			Element total = result.getChild("total");
			Element tscore = total.getChild("score");
			score = tscore.getValue();

		} catch (Exception e) {
			log.error(e);
			// return "N/A";
		}
		return score;
	}
}
