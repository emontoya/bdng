package org.bdng.oai.error;

import org.bdng.oai.error.OAIError;

public class BadGranularity extends OAIError {
	public BadGranularity(String text) {

		super(text);
		code = "badGranularity";
	}
}
