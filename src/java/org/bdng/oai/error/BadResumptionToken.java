package org.bdng.oai.error;

import org.bdng.oai.error.OAIError;

public class BadResumptionToken extends OAIError {
	public BadResumptionToken(String text) {

		super(text);
		code = "badResumptionToken";
	}
}
