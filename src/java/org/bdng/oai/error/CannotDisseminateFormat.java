package org.bdng.oai.error;

import org.bdng.oai.error.OAIError;

public class CannotDisseminateFormat extends OAIError {
	public CannotDisseminateFormat(String text) {

		super(text);
		code = "cannotDisseminateFormat";
	}
}
