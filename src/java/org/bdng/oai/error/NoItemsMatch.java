package org.bdng.oai.error;

import org.bdng.oai.error.OAIError;

public class NoItemsMatch extends OAIError {
	public NoItemsMatch(String text) {

		super(text);
		code = "noItemsMatch";
	}
}
