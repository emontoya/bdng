package org.bdng.oai.error;

import org.bdng.oai.error.OAIError;

public class BadArgument extends OAIError {
	public BadArgument(String text) {
		super(text);
		code = "badArgument";
	}
}
