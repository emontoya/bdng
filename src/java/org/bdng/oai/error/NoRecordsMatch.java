package org.bdng.oai.error;

import org.bdng.oai.error.OAIError;

public class NoRecordsMatch extends OAIError {
	public NoRecordsMatch(String text) {

		super(text);
		code = "noRecordsMatch";
	}
}
