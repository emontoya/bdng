package org.bdng.oai.error;

import org.bdng.oai.error.OAIError;

public class IdDoesNotExist extends OAIError {
	public IdDoesNotExist(String text) {

		super(text);
		code = "idDoesNotExist";
	}
}
