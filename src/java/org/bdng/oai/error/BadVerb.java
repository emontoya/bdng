package org.bdng.oai.error;

import org.bdng.oai.error.OAIError;

public class BadVerb extends OAIError {
	public BadVerb(String text) {

		super(text);
		code = "badVerb";
	}
}
