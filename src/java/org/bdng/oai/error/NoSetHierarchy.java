package org.bdng.oai.error;

import org.bdng.oai.error.OAIError;

public class NoSetHierarchy extends OAIError {
	public NoSetHierarchy(String text) {
		super(text);
		code = "noSetHierarchy";
	}
}
