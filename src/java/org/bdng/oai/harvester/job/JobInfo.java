package org.bdng.oai.harvester.job;

import java.io.Serializable;

import org.bdng.oai.harvester.job.HarvesterInfo;

public class JobInfo implements Serializable {

	// private Set days;
	// private Date time;
	private HarvesterInfo hInfo;

	/**
	 * @return Returns the days.
	 */
	/*
	 * public Set getDays() { return days; }
	 *//**
	 * @param days
	 *            The days to set.
	 */
	/*
	 * public void setDays(Set days) { this.days = days; }
	 */

	/**
	 * @return Returns the hInfo.
	 */
	public HarvesterInfo getHInfo() {
		return hInfo;
	}

	/**
	 * @param info
	 *            The hInfo to set.
	 */
	public void setHInfo(HarvesterInfo info) {
		hInfo = info;
	}

	/**
	 * @return Returns the time.
	 */
	/*
	 * public Date getTime() { return time; }
	 *//**
	 * @param time
	 *            The time to set.
	 */
	/*
	 * public void setTime(Date time) { this.time = time; }
	 */

}
