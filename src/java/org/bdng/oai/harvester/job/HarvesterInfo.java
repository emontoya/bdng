package org.bdng.oai.harvester.job;

import java.io.Serializable;

public class HarvesterInfo implements Serializable {

	private String harvestFrom;
	private String harvestTo;
	private String setSepc;
	private String metaDataPrefix;
	private String baseURL;
	private String respositoryID;

	/**
	 * @return Returns the baseURL.
	 */
	public String getBaseURL() {
		return baseURL;
	}

	/**
	 * @param baseURL
	 *            The baseURL to set.
	 */
	public void setBaseURL(String baseURL) {
		this.baseURL = baseURL;
	}

	/**
	 * @return Returns the harvestFrom.
	 */
	public String getHarvestFrom() {
		return harvestFrom;
	}

	/**
	 * @param harvestFrom
	 *            The harvestFrom to set.
	 */
	public void setHarvestFrom(String harvestFrom) {
		this.harvestFrom = harvestFrom;
	}

	/**
	 * @return Returns the harvestTo.
	 */
	public String getHarvestTo() {
		return harvestTo;
	}

	/**
	 * @param harvestTo
	 *            The harvestTo to set.
	 */
	public void setHarvestTo(String harvestTo) {
		this.harvestTo = harvestTo;
	}

	/**
	 * @return Returns the metaDataPrefix.
	 */
	public String getMetaDataPrefix() {
		return metaDataPrefix;
	}

	/**
	 * @param metaDataPrefix
	 *            The metaDataPrefix to set.
	 */
	public void setMetaDataPrefix(String metaDataPrefix) {
		this.metaDataPrefix = metaDataPrefix;
	}

	/**
	 * @return Returns the respositoryID.
	 */
	public String getRespositoryID() {
		return respositoryID;
	}

	/**
	 * @param respositoryID
	 *            The respositoryID to set.
	 */
	public void setRespositoryID(String respositoryID) {
		this.respositoryID = respositoryID;
	}

	/**
	 * @return Returns the setSepc.
	 */
	public String getSetSepc() {
		return setSepc;
	}

	/**
	 * @param setSepc
	 *            The setSepc to set.
	 */
	public void setSetSepc(String setSepc) {
		this.setSepc = setSepc;
	}

}
