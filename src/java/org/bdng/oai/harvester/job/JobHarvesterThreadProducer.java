package org.bdng.oai.harvester.job;

import java.util.ArrayList;
import java.util.Iterator;

import org.bdng.oai.harvester.job.HarvesterInfo;
import org.bdng.oai.harvester.job.HarvesterJob;

public class JobHarvesterThreadProducer extends Thread {

	HarvesterJob myHarvest;

	public JobHarvesterThreadProducer(HarvesterJob harvest) {
		this.myHarvest = harvest;
	}

	public void run() {

		// Metodo de lectura de archivo XML de Configuracion y retorno
		// de Jobs para ejecucion

		ArrayList jobsToExecute = new ArrayList();

		for (Iterator iter = jobsToExecute.iterator(); iter.hasNext();) {
			HarvesterInfo element = (HarvesterInfo) iter.next();
			// Se agrega un Job a la cola
			synchronized (this) {
				this.myHarvest.getJobs().add(element);

			}

		}

	}

}
