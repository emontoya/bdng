package org.bdng.oai.harvester.job;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import org.bdng.oai.harvester.job.HarvesterInfo;

public class ReadXMLForHarvest {

	private ResourceBundle properties = ResourceBundle.getBundle("oai");
	private boolean LOG = true;

	public Map getJobsToDo(String pathHarvestJobs) {
		// Variable a retirnar
		HashMap jobs = new HashMap();
		SimpleDateFormat formaTime = new SimpleDateFormat("h:mm");
		try {

			Log("inside it");
			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();

			String pathHarvest = pathHarvestJobs == null ? properties
					.getString("HARVEST_JOBS") : pathHarvestJobs;
			Document doc = docBuilder.parse(new File(pathHarvest));

			NodeList listOfJobs = doc.getElementsByTagName("task");

			// Se recorren los tasks y se agregan ala cola de Jobs/tasks
			for (int i = 0; i < listOfJobs.getLength(); i++) {
				Node task = listOfJobs.item(i);
				Log(((Element) task).getTextContent());
				Element elTask = (Element) task;
				String idTask = elTask.getElementsByTagName("id").item(0)
						.getTextContent();
				Log("idTaks: " + idTask);

				// Se traen los dias en los cuales se ejecuta los Jobs
				Log("Construyendo Dias");
				Log(elTask.getElementsByTagName("days").item(0)
						.getTextContent());
				Set days = getDaysFromElementJOBInfo(elTask
						.getElementsByTagName("days").item(0).getTextContent());

				// Se contruye la infromacion harvesting
				NodeList harvestNode = elTask.getElementsByTagName("harvest");
				Element elemHarvest = (Element) harvestNode.item(0);
				Log("HAVERST" + elemHarvest.getTextContent());
				HarvesterInfo hInfo = new HarvesterInfo();
				hInfo.setBaseURL(elemHarvest.getAttribute("url"));
				hInfo.setHarvestFrom(elemHarvest.getAttribute("harvestfrom"));
				hInfo.setHarvestTo(elemHarvest.getAttribute("harvestuntil"));
				hInfo.setMetaDataPrefix(elemHarvest
						.getAttribute("metadataprefix"));
				hInfo.setRespositoryID(elemHarvest.getAttribute("repositoryid"));
				Log(elemHarvest.getAttribute("repositoryid"));
				hInfo.setSetSepc(elemHarvest.getAttribute("setspec"));
				Log(hInfo.toString());

				Log("idTask:" + idTask);

				// antes de poner en cola para ejecutar, se verifica que se
				// peuda ejecutar en el d�a
				Date today = new Date();
				int day = today.getDay();
				if (days.contains(new Integer(day))) {
					jobs.put(idTask, hInfo);
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}

		return jobs;

	}

	private Set getDaysFromElementJOBInfo(String days) {

		Set daysAsMap = new HashSet();
		String[] daysArr = days.trim().split(",");

		for (int i = 0; i < daysArr.length; i++) {
			daysAsMap.add(new Integer(daysArr[i]));
		}
		return daysAsMap;
	}

	private void Log(String s) {
		if (LOG)
			System.out.println("XMLREAD:" + s);
	}

}
