package org.bdng.oai.harvester.job;

import org.bdng.oai.harvester.ListRecords;
import org.bdng.oai.harvester.job.HarvesterInfo;
import org.bdng.oai.harvester.job.HarvesterJob;

public class JobHarvesterThreadConsumer extends Thread {

	HarvesterJob myHarvest;

	public JobHarvesterThreadConsumer(HarvesterJob harvest) {
		this.myHarvest = harvest;
	}

	public void run() {

		// Esta permanentemente chequeando si hay jobs para ejecutar
		HarvesterInfo harvestToDo = new HarvesterInfo();
		while (!(this.myHarvest.getJobs().isEmpty())) {
			synchronized (this) {
				harvestToDo = (HarvesterInfo) this.myHarvest.getJobs().poll();
			}
			try {
				ListRecords list = new ListRecords(harvestToDo.getBaseURL(),
						harvestToDo.getHarvestFrom(),
						harvestToDo.getHarvestTo(), harvestToDo.getSetSepc(),
						harvestToDo.getMetaDataPrefix());

			} catch (Exception te) {

			}

		}
		try {
			sleep(1000000);
		} catch (InterruptedException ie) {
			resume();
		}

	}

}
