/**
 * 
 */
package org.bdng.oai.harvester;

import org.bdng.oai.dataprovider.DCRecord;

/**
 * @author Lucas
 * 
 */
public interface HarvesterInterface {

	/**
	 * Metodo que actualiza/crea un nuevo registro OAI en una biblioteca a
	 * partir de una recoleccion
	 * 
	 * @param record
	 */
	public void updateLibrary(DCRecord record);

}
