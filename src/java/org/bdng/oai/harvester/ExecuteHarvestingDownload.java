package org.bdng.oai.harvester;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import org.bdng.oai.dataprovider.DCRecord;
import org.bdng.oai.dataprovider.HarvesterUpdaterEAFIT;
import org.bdng.oai.harvester.HarvesterInterface;
import org.bdng.oai.harvester.ListRecords;
import org.bdng.oai.harvester.job.HarvesterInfo;
import org.bdng.oai.harvester.job.ReadXMLForHarvest;

public class ExecuteHarvestingDownload {

	protected static String URI = "xmldb:exist://localhost:8080/exist/xmlrpc/db/EAFIT/L";
	private static ResourceBundle properties = ResourceBundle
			.getBundle("xpathQ");

	public static void main(String args[]) throws Exception {

		Calendar timeStart = Calendar.getInstance();

		String pathJobHarvest = null;
		String folderOutputHarvest = "C:\\bdng\\outputHarvest\\";

		// para saber si el usuario paso el par�metro, si el paramtro de ruta de
		// Job
		// llega no se usa la ruta por DEfault
		if (args.length > 0) {
			pathJobHarvest = args[0];
			if (args.length > 1) {
				folderOutputHarvest = args[1];
			}
		}

		ReadXMLForHarvest rxml = new ReadXMLForHarvest();
		Map jobs = rxml.getJobsToDo(pathJobHarvest);
		Log("Total jobs:" + jobs.size());

		HarvesterInterface harvester = new HarvesterUpdaterEAFIT();
		int iters = 0;
		for (Iterator iter = jobs.values().iterator(); iter.hasNext();) {
			HarvesterInfo element = (HarvesterInfo) iter.next();

			// Here starts the Harvesting of one library
			boolean terminoHarvest = false;

			ListRecords list = new ListRecords(element.getBaseURL(),
					element.getHarvestFrom(), element.getHarvestTo(),
					element.getSetSepc(), element.getMetaDataPrefix());
			String token;
			while (!terminoHarvest) {

				Document doc = list.getDocument();
				token = getResumptionTokenFromOAIDoc(doc, "ListRecords");
				Log("Token:" + token);

				ArrayList dcRecords = (ArrayList) getDCRecordsFromOAIDoc(doc,
						"ListRecords");

				// Document bdeafitDocument = getDocFromOAIRecords(dcRecords);

				String nameFile = "BDEAFIT_" + element.getRespositoryID()
						+ "_f_" + element.getHarvestFrom() + "_u_"
						+ element.getHarvestTo() + "_s_" + element.getSetSepc()
						+ "_" + iters + ".xml";
				iters++;
				writeXmlFile(doc, folderOutputHarvest + nameFile);

				/*
				 * for (Iterator iter2 = dcRecords.iterator(); iter2.hasNext();)
				 * { DCRecord dcRec = (DCRecord) iter2.next();
				 * 
				 * //StringUtils.printDCRecord(dcRec);
				 * 
				 * ///////////////////////////// /////////////////////////////
				 * ///UPDATE EN BASE DE DATOS///
				 * 
				 * harvester.updateLibrary(dcRec);
				 * 
				 * }
				 */

				if (token != null) {
					list = new ListRecords(element.getBaseURL(), token);

				} else {
					terminoHarvest = true;
				}

			}

			// Element elem = list.getDocument().getDocumentElement()

		}

		Calendar timeStop = Calendar.getInstance();

		long diff = timeStop.getTimeInMillis() - timeStart.getTimeInMillis();
		Log("Total TIEMPO EJECUCION HARVEST:" + (diff / 1000) + " secs " + diff
				+ " millis");

	}

	private static Collection getDCRecordsFromOAIDoc(Document oaiDoc,
			String verb) throws Exception {

		ArrayList dcRecords = new ArrayList();
		NodeList listRecordsItems = oaiDoc.getElementsByTagName(verb);

		Log("Total LsitRecords:" + listRecordsItems.getLength());
		for (int i = 0; i < listRecordsItems.getLength(); i++) {
			Node listRecordItem = listRecordsItems.item(i);
			Document listRecordDoc = listRecordItem.getOwnerDocument();

			NodeList recordItems = listRecordDoc.getElementsByTagName("record");
			Log("Total Records:" + recordItems.getLength());

			for (int j = 0; j < recordItems.getLength(); j++) {
				Node recordItem = recordItems.item(j);
				// hijo 0 es el header
				NodeList inRecordItems = recordItem.getChildNodes();

				DCRecord record = new DCRecord();
				for (int k = 0; k < inRecordItems.getLength(); k++) {

					// Log("(((((((IN XML RESPONSE AFTER HARVESTING)))))))))");
					// Log(inRecordItems.item(k).getNodeName());
					Vector ids = new Vector();
					if (inRecordItems.item(k).getNodeName().equals("header")) {
						NodeList headerElements = inRecordItems.item(k)
								.getChildNodes();

						for (int index = 0; index < headerElements.getLength(); index++) {
							Node headerElement = headerElements.item(index);
							if ("identifier"
									.equals(headerElement.getNodeName())) {
								ids.add(headerElement.getTextContent());
								record.fullid = headerElement.getTextContent();
							} else if ("datestamp".equals(headerElement
									.getNodeName())) {
								record.datestamp = headerElement
										.getTextContent();
							}

						}

					}
					record.identifier = ids;
					Vector creators = new Vector();
					Vector subjects = new Vector();
					Vector identifiers = new Vector();

					if (inRecordItems.item(k).getNodeName().equals("metadata")) {
						Node metadataItem = inRecordItems.item(k);
						Document metadataDoc = metadataItem.getOwnerDocument();
						NodeList oai_dcElements = metadataDoc
								.getElementsByTagName("oai_dc:dc");

						// for (int index = 0; index <
						// oai_dcElements.getLength(); index++) {

						NodeList metadataElements = oai_dcElements.item(0)
								.getChildNodes();

						for (int indexChild = 0; indexChild < metadataElements
								.getLength(); indexChild++) {
							Node metadataElement = metadataElements
									.item(indexChild);
							/*
							 * if("dc:fullid".equals(metadataElement.getNodeName(
							 * ))) { record.fullid =
							 * metadataElement.getTextContent();
							 * //Log("Contenido nodo doc:"
							 * +nodes_j.item(j).getTextContent()); } else
							 */if ("dc:contributor".equals(metadataElement
									.getNodeName())) {
								record.contributor = metadataElement
										.getTextContent();
							} else if ("dc:coverage".equals(metadataElement
									.getNodeName())) {
								record.coverage = metadataElement
										.getTextContent();
							} else if ("dc:date".equals(metadataElement
									.getNodeName())) {
								record.date = metadataElement.getTextContent();
							} else if ("dc:datestamp".equals(metadataElement
									.getNodeName())) {
								record.datestamp = metadataElement
										.getTextContent();
							} else if ("dc:description".equals(metadataElement
									.getNodeName())) {
								record.description = metadataElement
										.getTextContent();
							} else if ("dc:format".equals(metadataElement
									.getNodeName())) {
								record.format = metadataElement
										.getTextContent();
							} else if ("dc:language".equals(metadataElement
									.getNodeName())) {
								record.language = metadataElement
										.getTextContent();
							} else if ("dc:publisher".equals(metadataElement
									.getNodeName())) {
								record.publisher = metadataElement
										.getTextContent();
							} else if ("dc:relation".equals(metadataElement
									.getNodeName())) {
								record.relation = metadataElement
										.getTextContent();
							} else if ("dc:rights".equals(metadataElement
									.getNodeName())) {
								record.rights = metadataElement
										.getTextContent();
							} else if ("dc:sets".equals(metadataElement
									.getNodeName())) {
								record.sets = metadataElement.getTextContent();
							} else if ("dc:source".equals(metadataElement
									.getNodeName())) {
								record.source = metadataElement
										.getTextContent();
							} else if ("dc:status".equals(metadataElement
									.getNodeName())) {
								record.status = metadataElement
										.getTextContent();
							} else if ("dc:title".equals(metadataElement
									.getNodeName())) {
								record.title = metadataElement.getTextContent();
							} else if ("dc:type".equals(metadataElement
									.getNodeName())) {
								record.type = metadataElement.getTextContent();
							} else if ("dc:creator".equals(metadataElement
									.getNodeName())) {
								creators.add(metadataElement.getTextContent());
							} else if ("dc:subject".equals(metadataElement
									.getNodeName())) {
								subjects.add(metadataElement.getTextContent());
							}
						}

						// }
						record.creator = creators;
						record.subject = subjects;

					}

				}
				dcRecords.add(record);
				// hijo 1 es
				// recordItem.
			}
		}

		return dcRecords;

	}

	public static Document getDocFromOAIRecords(Collection records) {
		try {
			DocumentBuilder builder = DocumentBuilderFactory.newInstance()
					.newDocumentBuilder();
			Document doc = builder.newDocument();

			Element header = doc.createElement("rdf:RDF");
			header.setAttribute("xmlns:bde",
					"http://www.eafit.edu.co/bdigital/bde/elements/1.0");
			header.setAttribute("xmlns:dc", "http://purl.org/dc/elements/1.1/");
			header.setAttribute("xmlns:rdf",
					"http://www.w3.org/1999/02/22-rdf-syntax-ns#");

			doc.appendChild(header);

			for (Iterator iter = records.iterator(); iter.hasNext();) {

				DCRecord record = (DCRecord) iter.next();
				Element eRecord = doc.createElement("rdf:Description");

				// Log("ITerando and creating document");
				// COntributor

				if (record.contributor != null) {
					Element eElement = doc.createElement("dc:contributor");
					eElement.setTextContent(record.contributor);
					eRecord.appendChild(eElement);
				}

				// Coverage

				if (record.coverage != null) {
					Element eElement = doc.createElement("dc:coverage");
					eElement.setTextContent(record.coverage);
					eRecord.appendChild(eElement);
				}

				// Date
				if (record.date != null) {
					Element eElement = doc.createElement("dc:date");
					eElement.setTextContent(record.date);
					eRecord.appendChild(eElement);
				}

				// Datestamp
				if (record.datestamp != null) {
					Element eElement = doc.createElement("dc:datestamp");
					eElement.setTextContent(record.datestamp);
					eRecord.appendChild(eElement);
				}

				// description
				if (record.description != null) {
					// Log("description");
					Element eElement = doc.createElement("dc:description");
					eElement.setTextContent(record.description);
					eRecord.appendChild(eElement);
				}

				// format
				if (record.format != null) {
					Element eElement = doc.createElement("dc:format");
					eElement.setTextContent(record.format);
					eRecord.appendChild(eElement);
				}

				// fullid
				if (record.fullid != null) {
					Element eElement = doc.createElement("dc:fullid");
					eElement.setTextContent(record.fullid);
					eRecord.appendChild(eElement);
				}

				// language
				if (record.language != null) {
					Element eElement = doc.createElement("dc:language");
					eElement.setTextContent(record.language);
					eRecord.appendChild(eElement);
				}

				// publisher
				if (record.publisher != null) {
					Element eElement = doc.createElement("dc:publisher");
					eElement.setTextContent(record.publisher);
					eRecord.appendChild(eElement);
				}

				// relation
				if (record.relation != null) {
					Element eElement = doc.createElement("dc:relation");
					eElement.setTextContent(record.relation);
					eRecord.appendChild(eElement);
				}

				// rights
				if (record.rights != null) {
					Element eElement = doc.createElement("dc:rights");
					eElement.setTextContent(record.rights);
					eRecord.appendChild(eElement);
				}

				// sets
				if (record.sets != null) {
					Element eElement = doc.createElement("dc:sets");
					eElement.setTextContent(record.sets);
					eRecord.appendChild(eElement);
				}

				// source
				if (record.source != null) {
					Element eElement = doc.createElement("dc:source");
					eElement.setTextContent(record.source);
					eRecord.appendChild(eElement);
				}

				// status
				if (record.status != null) {
					Element eElement = doc.createElement("dc:status");
					eElement.setTextContent(record.status);
					eRecord.appendChild(eElement);
				}

				// title
				if (record.title != null) {
					Element eElement = doc.createElement("dc:title");
					eElement.setTextContent(record.title);
					eRecord.appendChild(eElement);
				}

				// type
				if (record.type != null) {
					Element eElement = doc.createElement("dc:type");
					eElement.setTextContent(record.type);
					eRecord.appendChild(eElement);
				}

				// creator
				if (record.creator != null) {
					Element eElement = doc.createElement("dc:creator");

					for (Iterator iterator = record.creator.iterator(); iterator
							.hasNext();) {
						String creator = (String) iterator.next();
						eElement.setTextContent(creator);
						eRecord.appendChild(eElement);
					}

				}

				// identifier
				if (record.identifier != null) {
					Element eElement = doc.createElement("dc:identifier");

					for (Iterator iterator = record.identifier.iterator(); iterator
							.hasNext();) {
						String identifier = (String) iterator.next();
						eElement.setTextContent(identifier);
						eRecord.appendChild(eElement);
					}

				}

				// subject
				if (record.subject != null) {
					Element eElement = doc.createElement("dc:subject");

					for (Iterator iterator = record.subject.iterator(); iterator
							.hasNext();) {
						String subject = (String) iterator.next();
						eElement.setTextContent(subject);
						eRecord.appendChild(eElement);
					}

				}

				header.appendChild(eRecord);

			}

			// doc.appendChild(header);

			return doc;
		} catch (Exception e) {
			// TODO: handle exception
		}

		return null;

	}

	// This method writes a DOM document to a file
	public static void writeXmlFile(Document doc, String filename) {
		try {
			// Prepare the DOM document for writing
			Source source = new DOMSource(doc);

			// Prepare the output file
			File file = new File(filename);
			Result result = new StreamResult(file);

			// Write the DOM document to the file
			Transformer xformer = TransformerFactory.newInstance()
					.newTransformer();
			xformer.setOutputProperty(OutputKeys.INDENT, "yes");
			xformer.transform(source, result);
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		}
	}

	private static String getResumptionTokenFromOAIDoc(Document oaiDoc,
			String verb) {

		ArrayList dcRecords = new ArrayList();
		NodeList listRecordsItems = oaiDoc.getElementsByTagName(verb);
		Log("Total LsitRecords RESUMPTION TOKEN:"
				+ listRecordsItems.getLength());
		for (int i = 0; i < listRecordsItems.getLength(); i++) {
			Node listRecordItem = listRecordsItems.item(i);
			Document listRecordDoc = listRecordItem.getOwnerDocument();
			Log("getResumptionToken:" + listRecordDoc.getTextContent());

			NodeList recordItems = listRecordDoc
					.getElementsByTagName("resumptionToken");
			Log("getResumptionToken recordItems:" + recordItems.getLength());
			if (recordItems.getLength() == 1) {
				Log("NodeValue:" + recordItems.item(0).getTextContent());
				return recordItems.item(0).getTextContent();
			}
			Log("Wrong ResumptionToken");

		}
		return null;
	}

	private static void Log(String s) {
		System.out.println("main:" + s);
	}

}