package org.bdng.oai.harvester;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.ResourceBundle;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.bdng.oai.dataprovider.DCRecord;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class GenerateData {

	protected static String URI = "xmldb:exist://localhost:8080/exist/xmlrpc/db/bdeafit/L";
	private static ResourceBundle properties = ResourceBundle
			.getBundle("xpathQ");

	public static void main(String args[]) throws Exception {

		int totalInc = 10;
		int totalRegs = 10;
		Calendar timeStart = Calendar.getInstance();

		String pathJobHarvest = null;
		String folderOutputHarvest = "C:\\bdng\\outputFilesTest\\";

		// para saber si el usuario paso el par�metro, si el paramtro de ruta de
		// Job
		// llega no se usa la ruta por DEfault
		if (args.length > 0) {
			pathJobHarvest = args[0];
			if (args.length > 1) {
				folderOutputHarvest = args[1];
			}
		}

		for (int fileN = 0; fileN < totalRegs / totalInc; fileN++) {

			ArrayList dcRecords = new ArrayList();
			int i = 0; // para iterar

			for (int iterReg = 0; iterReg < totalInc; iterReg++) {
				DCRecord record = new DCRecord();
				record.contributor = "contributor" + i;
				record.coverage = "coverage" + i;
				Vector creators = new Vector();
				creators.add("creator1" + i);
				creators.add("creator2" + i);
				creators.add("creator3" + i);
				record.creator = creators;
				record.date = "date";
				record.datestamp = "19850101";
				record.description = "description" + i;
				record.format = "oai_dc";
				record.fullid = "EAFIT" + (new Date().getTime()) + "" + i;
				Vector identifiers = new Vector();
				identifiers.add(record.fullid);
				record.identifier = identifiers;
				record.language = "language" + i;
				record.publisher = "publisher" + i;
				record.relation = "relation" + i;
				record.rights = "rights" + i;
				record.sets = "sets" + i;
				record.source = "source" + i;
				record.status = "status" + i;
				Vector subjects = new Vector();
				subjects.add("subject1" + i);
				subjects.add("subject2" + i);
				subjects.add("subject3" + i);
				record.subject = subjects;
				record.title = "title" + i;
				record.type = "type" + i;
				i++;
				dcRecords.add(record);

			}

			Document bdeafitDocument = getDocFromOAIRecords(dcRecords);

			String nameFile = "BDEAFIT_" + totalRegs + "_" + fileN + ".xml";

			writeXmlFile(bdeafitDocument, folderOutputHarvest + nameFile);

		}

	}

	public static Document getDocFromOAIRecords(Collection records) {
		try {
			DocumentBuilder builder = DocumentBuilderFactory.newInstance()
					.newDocumentBuilder();
			Document doc = builder.newDocument();

			Element header = doc.createElement("rdf:RDF");
			header.setAttribute("xmlns:bde",
					"http://www.eafit.edu.co/bdigital/bde/elements/1.0");
			header.setAttribute("xmlns:dc", "http://purl.org/dc/elements/1.1/");
			header.setAttribute("xmlns:rdf",
					"http://www.w3.org/1999/02/22-rdf-syntax-ns#");

			doc.appendChild(header);

			for (Iterator iter = records.iterator(); iter.hasNext();) {

				DCRecord record = (DCRecord) iter.next();
				Element eRecord = doc.createElement("rdf:Description");

				// Log("ITerando and creating document");
				// COntributor

				if (record.contributor != null) {
					Element eElement = doc.createElement("dc:contributor");
					eElement.setTextContent(record.contributor);
					eRecord.appendChild(eElement);
				}

				// Coverage

				if (record.coverage != null) {
					Element eElement = doc.createElement("dc:coverage");
					eElement.setTextContent(record.coverage);
					eRecord.appendChild(eElement);
				}

				// Date
				if (record.date != null) {
					Element eElement = doc.createElement("dc:date");
					eElement.setTextContent(record.date);
					eRecord.appendChild(eElement);
				}

				// Datestamp
				if (record.datestamp != null) {
					Element eElement = doc.createElement("dc:datestamp");
					eElement.setTextContent(record.datestamp);
					eRecord.appendChild(eElement);
				}

				// description
				if (record.description != null) {
					// Log("description");
					Element eElement = doc.createElement("dc:description");
					eElement.setTextContent(record.description);
					eRecord.appendChild(eElement);
				}

				// format
				if (record.format != null) {
					Element eElement = doc.createElement("dc:format");
					eElement.setTextContent(record.format);
					eRecord.appendChild(eElement);
				}

				// fullid
				if (record.fullid != null) {
					Element eElement = doc.createElement("bde:id");
					eElement.setTextContent(record.fullid);
					eRecord.appendChild(eElement);
				}

				// language
				if (record.language != null) {
					Element eElement = doc.createElement("dc:language");
					eElement.setTextContent(record.language);
					eRecord.appendChild(eElement);
				}

				// publisher
				if (record.publisher != null) {
					Element eElement = doc.createElement("dc:publisher");
					eElement.setTextContent(record.publisher);
					eRecord.appendChild(eElement);
				}

				// relation
				if (record.relation != null) {
					Element eElement = doc.createElement("dc:relation");
					eElement.setTextContent(record.relation);
					eRecord.appendChild(eElement);
				}

				// rights
				if (record.rights != null) {
					Element eElement = doc.createElement("dc:rights");
					eElement.setTextContent(record.rights);
					eRecord.appendChild(eElement);
				}

				// sets
				if (record.sets != null) {
					Element eElement = doc.createElement("dc:sets");
					eElement.setTextContent(record.sets);
					eRecord.appendChild(eElement);
				}

				// source
				if (record.source != null) {
					Element eElement = doc.createElement("dc:source");
					eElement.setTextContent(record.source);
					eRecord.appendChild(eElement);
				}

				// status
				if (record.status != null) {
					Element eElement = doc.createElement("dc:status");
					eElement.setTextContent(record.status);
					eRecord.appendChild(eElement);
				}

				// title
				if (record.title != null) {
					Element eElement = doc.createElement("dc:title");
					eElement.setTextContent(record.title);
					eRecord.appendChild(eElement);
				}

				// type
				if (record.type != null) {
					Element eElement = doc.createElement("dc:type");
					eElement.setTextContent(record.type);
					eRecord.appendChild(eElement);
				}

				// creator
				if (record.creator != null) {

					for (Iterator iterator = record.creator.iterator(); iterator
							.hasNext();) {
						Element eElement = doc.createElement("dc:creator");
						String creator = new String((String) iterator.next());
						eElement.setTextContent(creator);
						eRecord.appendChild(eElement);
					}

				}

				// identifier
				if (record.identifier != null) {

					for (Iterator iterator = record.identifier.iterator(); iterator
							.hasNext();) {
						Element eElement = doc.createElement("dc:identifier");
						String identifier = new String((String) iterator.next());
						eElement.setTextContent(identifier);
						eRecord.appendChild(eElement);
					}

				}

				// subject
				if (record.subject != null) {
					// Element eElement = doc.createElement("dc:subject");

					for (Iterator iterator = record.subject.iterator(); iterator
							.hasNext();) {
						Element eElement = doc.createElement("dc:subject");
						String subject = new String((String) iterator.next());
						eElement.setTextContent(subject);
						eRecord.appendChild(eElement);
					}

				}

				header.appendChild(eRecord);

			}

			// doc.appendChild(header);

			return doc;
		} catch (Exception e) {
			// TODO: handle exception
		}

		return null;

	}

	// This method writes a DOM document to a file
	public static void writeXmlFile(Document doc, String filename) {
		try {
			// Prepare the DOM document for writing
			Source source = new DOMSource(doc);

			// Prepare the output file
			File file = new File(filename);
			// file.mkdirs();
			Result result = new StreamResult(file);

			// Write the DOM document to the file
			Transformer xformer = TransformerFactory.newInstance()
					.newTransformer();
			xformer.transform(source, result);
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		}
	}

	private static void Log(String s) {
		System.out.println("main:" + s);
	}

}