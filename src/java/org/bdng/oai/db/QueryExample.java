package org.bdng.oai.db;

import java.util.ResourceBundle;

import org.xmldb.api.DatabaseManager;
import org.xmldb.api.base.Collection;
import org.xmldb.api.base.Database;
import org.xmldb.api.base.Resource;
import org.xmldb.api.base.ResourceIterator;
import org.xmldb.api.base.ResourceSet;
import org.xmldb.api.modules.XPathQueryService;

public class QueryExample {

	protected static String URI = "xmldb:exist://localhost:8080/exist/xmlrpc/db/EAFIT";
	private static ResourceBundle properties = ResourceBundle
			.getBundle("xpathQ");

	public static void main(String args[]) throws Exception {
		String driver = "org.exist.xmldb.DatabaseImpl";
		Class cl = Class.forName(driver);
		Database database = (Database) cl.newInstance();
		DatabaseManager.registerDatabase(database);

		Collection col = DatabaseManager.getCollection(URI);

		/*
		 * Collection col = DatabaseManager.getCollection(
		 * "xmldb:exist://localhost:8080/exist/xmlrpc/db" );
		 */
		XPathQueryService service = (XPathQueryService) col.getService(
				"XPathQueryService", "1.0");
		service.setProperty("indent", "yes");

		// String qry = properties.getString("GET_IDS");
		// "declare namespace
		// rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\";declare namespace
		// dc=\"http://purl.org/dc/elements/1.1/\";declare namespace
		// bde=\"http://www.eafit.edu.co/bdigital/bde/elements/1.0\";collection(\'/db/bdeafit')//
		// rdf:Description[dc:title&='CONCEPCIONES
		// // EN
		// TORNO']"j

		String stSQL = "declare namespace rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\";declare namespace dc=\"http://purl.org/dc/elements/1.1/\";declare namespace bde=\"http://www.eafit.edu.co/bdigital/bde/elements/1.0\";collection(\'/db/EAFIT')//rdf:RDF/rdf:Description[bde:id = 'EAFITL302.2345C733']";
		ResourceSet result = service.query(stSQL);
		// service.query("").

		System.out.println("QueryExample.main() \n" + stSQL);
		ResourceIterator i = result.getIterator();
		while (i.hasMoreResources()) {
			Resource r = i.nextResource();
			System.out.println((String) r.getContent());

		}
	}
}