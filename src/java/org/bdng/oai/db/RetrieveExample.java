/*
 * Creado el 24-jun-2005
 *
 */
package org.bdng.oai.db;

import java.util.ResourceBundle;

import javax.xml.transform.OutputKeys;

import org.xmldb.api.DatabaseManager;
import org.xmldb.api.base.Collection;
import org.xmldb.api.base.Database;
import org.xmldb.api.modules.XMLResource;

public class RetrieveExample {
	protected static String URI = "xmldb:exist://localhost:8080/exist/xmlrpc/db/EAFIT";

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String args[]) throws Exception {
		String driver = "org.exist.xmldb.DatabaseImpl";
		ResourceBundle properties = ResourceBundle.getBundle("oai");
		// initialize database driver
		Class cl = Class.forName(driver);
		Database database = (Database) cl.newInstance();

		DatabaseManager.registerDatabase(database);
		System.out.println(properties.getString("URI"));
		// get the collection
		Collection col = DatabaseManager.getCollection(properties
				.getString("URI"));
		col.setProperty(OutputKeys.INDENT, "no");
		String t[] = col.listChildCollections();

		// /*
		// * LIST SETS
		// */
		// ListSets listSets = new ListSets(
		// "http://localhost:8090/OAI/servlet/org.bdcol.bdigital.oai.servlet.OAI");
		// //
		// listSets.harvest("http://localhost:8090/bdeafit/servlet/org.bdcol.bdigital.oai.servlet.OAI");
		// System.out.println(listSets.toString());
		//
		// ListRecords listRecords = new ListRecords(
		// "http://localhost:8090/OAI/servlet/org.bdcol.bdigital.oai.servlet.OAI",
		// null, null, "L", null);
		// System.out.println(listRecords.toString());
		/*
		 * DBExist db = new
		 * "http://localhost:8090/bdeafit/servlet/org.bdcol.bdigital.oai.servlet.OAI"
		 * DBExist(); Vector v= new Vector(db.loadSets());
		 * 
		 * for (int i = 0; i < v.size(); i++) { System.out.println("R");
		 * System.out.println(v.get(i)); }
		 */

		int total = col.getResourceCount();
		System.out.println(total);

		String[] resources = col.listResources();
		for (int i = 0; i < resources.length; i++) {
			System.out.println(resources[i]);
		}

		// XMLResource res = (XMLResource) col.getResource("EAFIT_L100.xml");
		XMLResource res = (XMLResource) col
				.getResource("eafit_libro7_2500.xml");
		if (res == null)
			System.out.println("document not found!");
		else
			System.out.println(res.getContent());

	}
}