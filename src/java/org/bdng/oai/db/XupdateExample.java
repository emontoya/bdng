/*
 * Creado el 24-jun-2005
 *
 */
package org.bdng.oai.db;

import java.util.ResourceBundle;

import org.xmldb.api.DatabaseManager;
import org.xmldb.api.base.Collection;
import org.xmldb.api.base.Database;
import org.xmldb.api.modules.XUpdateQueryService;

import org.bdng.oai.db.XupdateExample;

public class XupdateExample {
	protected static String URI = "xmldb:exist://localhost:8080/exist/xmlrpc/db/bdeafit/L";
	private static ResourceBundle properties = ResourceBundle
			.getBundle("xpathQ");

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String args[]) throws Exception {
		XupdateExample xu = new XupdateExample();
		String driver = "org.exist.xmldb.DatabaseImpl";
		Class cl = Class.forName(driver);
		Database database = (Database) cl.newInstance();
		DatabaseManager.registerDatabase(database);

		Collection col = DatabaseManager.getCollection(URI, "admin", "");

		String updateRemove = "<?xml version=\"1.0\"?><xupdate:modifications version=\"1.0\" xmlns:xupdate=\"http://www.xmldb.org/xupdate\">	<xupdate:remove select=\"/rdf:RDF/rdf:Description[bde:id = 'EAFITHAD0010460004200302']\"/></xupdate:modifications>";

		String update2 = "<?xml version=\"1.0\"?><xupdate:modifications version=\"1.0\" xmlns:xupdate=\"http://www.xmldb.org/xupdate\">	<xupdate:update select=\"/rdf:RDF/rdf:Description[bde:id = 'EAFITHAD0010460004200301']/dc:contributor\">ARTICULO</xupdate:update></xupdate:modifications>";

		String xupdate = "<?xml version=\"1.0\"?>"
				+ "<xupdate:modifications version=\"1.0\" xmlns:xupdate=\"http://www.xmldb.org/xupdate\">"
				+ "<xupdate:insert-after select=\"/nodes/level1[last()]\">"
				+ "<xupdate:element name=\"level1\">"
				+ "<xupdate:attribute name=\"id\">3</xupdate:attribute>"
				+ "<level2>LEVEL 222</level2>" + "<level2>LEVEL 222.1</level2>"
				+ "</xupdate:element>" + "</xupdate:insert-after>"
				+ "</xupdate:modifications>";

		XUpdateQueryService service = (XUpdateQueryService) col.getService(
				"XUpdateQueryService", "1.0");

		long update = service.update(updateRemove);

		xu.Log("total registros actualizados:" + update);

	}

	public void Log(String s) {
		System.out.println("XupdateExample:" + s);
	}

}