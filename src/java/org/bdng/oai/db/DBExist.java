/*
 o  * Creado el 24-jun-2005
 *
 */
package org.bdng.oai.db;

import java.util.Collection;
import java.util.HashSet;
import java.util.ResourceBundle;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;

import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xmldb.api.DatabaseManager;
import org.xmldb.api.base.Database;
import org.xmldb.api.base.Resource;
import org.xmldb.api.base.ResourceIterator;
import org.xmldb.api.base.ResourceSet;
import org.xmldb.api.base.XMLDBException;
import org.xmldb.api.modules.XMLResource;
import org.xmldb.api.modules.XPathQueryService;
import org.xmldb.api.modules.XUpdateQueryService;
import org.bdng.oai.dataprovider.DCRecord;
import org.bdng.oai.dataprovider.DMPRecord;
import org.bdng.oai.db.DBExist;
import org.bdng.oai.error.BadArgument;
import org.bdng.oai.error.NoRecordsMatch;
import org.bdng.oai.util.StringUtils;

/**
 * @author Lucas Fl�rez This class manages all Exist database "conectivity" and
 *         databse functions related
 * 
 * 
 */
public class DBExist {

	private String URI;
	private String driver;
	private Class cl;
	private Database database;
	private ResourceBundle properties = ResourceBundle.getBundle("oai");
	private ResourceBundle queries = ResourceBundle.getBundle("xpathQ");
	private DocumentBuilderFactory documentBuilderFactory;
	private DocumentBuilder documentBuilder;

	boolean LOG = false;

	private static DBExist dbexist = null;

	public static DBExist getInstance() {
		if (dbexist == null) {
			try {
				dbexist = new DBExist();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return dbexist;
	}

	/**
	 * Function that initializes all parameters for Exist connectivity
	 * 
	 * @throws ClassNotFoundException
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws XMLDBException
	 */
	private DBExist() throws ClassNotFoundException, InstantiationException,
			IllegalAccessException, XMLDBException,
			ParserConfigurationException {
		this.URI = properties.getString("URI");
		Log("URI:" + this.URI);
		this.driver = "org.exist.xmldb.DatabaseImpl";
		this.cl = Class.forName(this.driver);
		this.database = (Database) this.cl.newInstance();
		DatabaseManager.registerDatabase(this.database);
		// Para funcionalidad XML
		this.documentBuilderFactory = DocumentBuilderFactory.newInstance();
		this.documentBuilderFactory.setNamespaceAware(true);
		this.documentBuilder = this.documentBuilderFactory.newDocumentBuilder();
	}

	/**
	 * Carga los conjuntos Sets
	 * 
	 * @return
	 * @throws Exception
	 */
	public Collection loadSets() throws Exception {
		try {
			// Mayo 22 de 2008. DatabaseManager.registerDatabase(this.database);
			System.out.println("URI=======" + this.URI);
			org.xmldb.api.base.Collection col = DatabaseManager.getCollection(
					this.URI, "admin", "admin");
			System.out.println("col=" + col);
			col.setProperty(OutputKeys.INDENT, "no");

			XPathQueryService service = (XPathQueryService) col.getService(
					"XPathQueryService", "1.0");
			service.setProperty("indent", "yes");
			String query = "";
			query = queries.getString("GET_SETS");
			Log(query);
			ResourceSet result = (service.query(query));
			long totalRecords = result.getSize();
			Log(String.valueOf("totalRecords " + totalRecords));
			ResourceIterator it = result.getIterator();
			Vector sets = new Vector();
			for (int i = 0; i < totalRecords; i++) {
				Resource r = result.getResource(i);
				// Resource r = it.nextResource();
				XMLResource xmlr = (XMLResource) r;
				Node dom = xmlr.getContentAsDOM();

				NodeList nodes_i = dom.getChildNodes();
				// System.out.println("size of nodes_i"+nodes_i.getLength());
				Node node_i = nodes_i.item(0);
				if ("name".equals(node_i.getNodeName())
						& !nodes_i.item(0).getTextContent().equals("todos")) { // si
																				// el
																				// set
																				// es
																				// "todos"
																				// lo
																				// salta
					// System.out.println("NAME FOUND "+nodes_i.item(0).getTextContent());
					sets.add(nodes_i.item(0).getTextContent());

				}
			}

			/*
			 * 
			 * String[] resources = col.listChildCollections();
			 * 
			 * Vector sets = new Vector(resources.length);
			 * 
			 * for (int i = 0; i < resources.length; i++) {
			 * sets.add(resources[i]); }
			 */
			return sets;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;

		}
	}

	/**
	 * Retorn un Record Dublin Core
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public DCRecord getDCRecord(String handle) throws Exception {
		DCRecord DCRec = null;
		Log("getDCRecord");
		// DatabaseManager.registerDatabase(this.database); Abril 24 de 2008
		org.xmldb.api.base.Collection col = null;
		// Se traen primero las colecciones para verificar que la coleccion es
		// valida
		// HashSet sets = new HashSet(loadSets());
		Log("Cargando SETS identifiers");

		// Variable de retorno
		col = DatabaseManager.getCollection(this.URI, "admin", "admin");
		// col = DatabaseManager.getCollection(this.URI+"/"+set) ;
		col.setProperty(OutputKeys.INDENT, "no");
		Log("getDCRecord col");
		// String [] docs = col.listResources();
		Log("getDCRecord despues cols");

		XPathQueryService service = (XPathQueryService) col.getService(
				"XPathQueryService", "1.0");
		service.setProperty("indent", "yes");
		String query = "";

		Log("adentro GET RECORD ");

		query = queries.getString("GET_RECORD");

		// PAra reemplazr el parametro del id en le query
		// Log("Query:" + query);
		query = StringUtils.replaceWord(query, "?", handle);
		// query.replaceAll("//?",handle);
		// query.replaceFirst("",handle);
		Log("Query:" + query);
		// System.out.println("QUERY_OAI="+query);
		ResourceSet result = service.query(query);
		// Log("Result size:"+result.getSize()); //always 1
		// service.query("").
		ResourceIterator it = result.getIterator();

		while (it.hasMoreResources()) {

			// Resource r = it.nextResource();
			Resource r = result.getResource(0);
			XMLResource xmlr = (XMLResource) r;
			Node dom = xmlr.getContentAsDOM();
			// Log("node name:" + dom.getNodeName());

			Node nodo = dom.getFirstChild();
			// dom.getChildNodes()

			// NodeList nodes_j = elementTemp.getChildNodes();
			NodeList nodes_j = nodo.getChildNodes();
			// Creadores
			Vector creators = new Vector();
			// Subject
			Vector subjects = new Vector();
			// Identifiers
			Vector identifiers = new Vector();

			// Temp DCRecord
			DCRecord dcRecordTemp = new DCRecord();

			for (int j = 0; j < nodes_j.getLength(); j++) {

				Node node_j = nodes_j.item(j);

				// Log("Nombre nodo: " + node_j.getNodeName());
				if ("dc:general.identifier".equals(node_j.getNodeName())) {
					identifiers.add(nodes_j.item(j).getTextContent());
					dcRecordTemp.fullid = nodes_j.item(j).getTextContent();
					// Log("Contenido nodo doc
					// BDE:ID:"+nodes_j.item(j).getTextContent());
				} else if ("dc:contributor".equals(node_j.getNodeName())) {
					dcRecordTemp.contributor = nodes_j.item(j).getTextContent();
				} else if ("dc:general.coverage".equals(node_j.getNodeName())) {
					dcRecordTemp.coverage = nodes_j.item(j).getTextContent();
				} else if ("dc:lifecycle.contribute.role.publisher.date.datetime"
						.equals(node_j.getNodeName())) {
					dcRecordTemp.date = nodes_j.item(j).getTextContent();
				} else if ("dc:datestamp".equals(node_j.getNodeName())) {
					dcRecordTemp.datestamp = nodes_j.item(j).getTextContent();
				} else if ("dc:general.description"
						.equals(node_j.getNodeName())) {
					dcRecordTemp.description = nodes_j.item(j).getTextContent();
				} else if ("dc:technical.format".equals(node_j.getNodeName())) {
					dcRecordTemp.format = nodes_j.item(j).getTextContent();
				} else if ("dc:general.language".equals(node_j.getNodeName())) {
					dcRecordTemp.language = nodes_j.item(j).getTextContent();
				} else if ("dc:lifecycle.contribute.role.publisher"
						.equals(node_j.getNodeName())) {
					dcRecordTemp.publisher = nodes_j.item(j).getTextContent();
				} else if ("dc:relation.resource.description".equals(node_j
						.getNodeName())) {
					dcRecordTemp.relation = nodes_j.item(j).getTextContent();
				} else if ("dc:rights.description".equals(node_j.getNodeName())) {
					dcRecordTemp.rights = nodes_j.item(j).getTextContent();
				} else if ("dc:sets".equals(node_j.getNodeName())) {
					dcRecordTemp.sets = nodes_j.item(j).getTextContent();
				} else if ("dc:relation.kind.isBasedOn.resource".equals(node_j
						.getNodeName())) {
					dcRecordTemp.source = nodes_j.item(j).getTextContent();
				} else if ("dc:status".equals(node_j.getNodeName())) {
					dcRecordTemp.status = nodes_j.item(j).getTextContent();
				} else if ("dc:general.title".equals(node_j.getNodeName())) {
					dcRecordTemp.title = nodes_j.item(j).getTextContent();
				} else if ("dc:educational.learningResourceType".equals(node_j
						.getNodeName())) {
					dcRecordTemp.type = nodes_j.item(j).getTextContent();
				} else if ("dc:setSpec".equals(node_j.getNodeName())) {
					// System.out.println("FOUND dc:setSpec "+nodes_j.item(j).getTextContent());
					dcRecordTemp.setSpec = nodes_j.item(j).getTextContent();
				} else if ("dc:identifier_oai".equals(node_j.getNodeName())) {
					// System.out.println("FOUND identifier_oai "+nodes_j.item(j).getTextContent());
					dcRecordTemp.identifier_oai = nodes_j.item(j)
							.getTextContent();
				} else if ("dc:collection".equals(node_j.getNodeName())) {
					dcRecordTemp.collection = nodes_j.item(j).getTextContent();
				}
				/*
				 * else if("dc:fullid".equals(node_j.getNodeName())) {
				 * dcRecordTemp.fullid = nodes_j.item(j).getTextContent(); }
				 */
				else if ("dc:lifecycle.contribute.role.author".equals(node_j
						.getNodeName())) {
					creators.add(nodes_j.item(j).getTextContent());
				} else if ("dc:general.keyword".equals(node_j.getNodeName())) {
					subjects.add(nodes_j.item(j).getTextContent());
				}

			}

			dcRecordTemp.identifier = identifiers;
			dcRecordTemp.creator = creators;
			dcRecordTemp.subject = subjects;
			DCRec = dcRecordTemp;
			return DCRec;
		}

		// }

		// }
		throw (new NoRecordsMatch("no records match"));

	}

	/**
	 * Carga los conjuntos Sets
	 * 
	 * @return
	 * @throws Exception
	 */
	public Collection loadIdentfiers(String from, String until, String set,
			int startno, int size) throws Exception {
		try {
			Log("Loading identifiers");
			// DatabaseManager.registerDatabase(this.database); Abril 25 de 2008
			org.xmldb.api.base.Collection col = null;
			// Se traen primero las colecciones para verificar que la coleccion
			// es valida
			HashSet sets = new HashSet(loadSets());
			Log("Cargando SETS identifiers");

			if (set != null) {
				if (!sets.contains(set))
					throw (new BadArgument(
							"badArgument - Collection does not exist"));
			}

			Log("Loading identifiers");
			// Se traen todos los documentos para recorres un por uno
			Log("Loading identifiers col");

			// String uri = "";

			if ((set == null) || (set.trim().length() == 0)) {
				set = "";
				// uri = this.URI + "[dc:cd = \"Y\"]";
			} // else
				// {
				// uri = this.URI+"/"+set+ "[dc:cd = \"Y\"]";
				// }

			// col = DatabaseManager.getCollection(this.URI + "/" +
			// set,"admin","admin"); //ya el set no hace parte de la estructura
			col = DatabaseManager.getCollection(this.URI, "admin", "admin");

			// col = DatabaseManager.getCollection(uri);

			Log("Loading identifier despues cols");
			col.setProperty(OutputKeys.INDENT, "no");
			// String [] docs = col.listResources();

			// Variable de retorno
			Vector identifiers = new Vector();
			// col = DatabaseManager.getCollection(this.URI + "/" + set);

			XPathQueryService service = (XPathQueryService) col.getService(
					"XPathQueryService", "1.0");
			service.setProperty("indent", "yes");
			String query = "";

			Document docTem = this.documentBuilder.newDocument();

			// Cleaiing parameters from and until
			Log("FROM :" + from);
			Log("UNTIL :" + until);
			from = from == null ? "" : from.trim();
			until = until == null ? "" : until.trim();

			boolean hasFrom = from.equals("") ? false : true;
			boolean hasUntil = until.equals("") ? false : true;
			boolean hasSet = set.equals("") ? false : true;
			boolean addElement = false;

			// Log("total docs" + docs.length);
			// Se recorren todo los documentos
			// for (int i = 0; i < docs.length; i++) {

			// Log("adentro" + docs[i]);
			Log("adentro version sin docs");
			// OJO
			// query = "doc(\""+ docs[i]+"\")"+ queries.getString("GET_IDS");
			// Se arma el Query con o sin DateStamp
			// query = "doc(\""+ docs[i]+"\")"+ queries.getString("GET_RECS");
			query = queries.getString("GET_RECS"); // query default

			// mejora de codigo...
			if (hasFrom || hasUntil || hasSet) {

				query = queries.getString("GET_RECS_BASE");
				if (hasFrom) {

					query = query
							+ queries.getString("GET_RECS_DATESTAMP_FROM");
					query = StringUtils.replaceWord(query, "?", from);
				}
				if (hasUntil) {

					query = query
							+ queries.getString("GET_RECS_DATESTAMP_UNTIL");
					query = StringUtils.replaceWord(query, "?", until);

				}
				if (hasSet) {
					query = query + queries.getString("GET_RECS_SET");
					query = StringUtils.replaceWord(query, "?", set);
				}

			}

			/*
			 * if(from.equals("") && until.equals("")) { query = "doc(\""+
			 * docs[i]+"\")"+ queries.getString("GET_RECS"); }
			 */
			Log("Query:" + query);
			// ResourceSet result = service.query(query);
			ResourceSet result = (service.query(query));
			long totalRecords = result.getSize();

			Log("Resulset size:" + totalRecords);
			// service.query("").
			// ResourceIterator it = result.getIterator();

			// Se verifica con el cursor dos cosas:
			// Que exista un cojunto total de registros para devolver del tama�o
			// maximo
			// o solo una parte

			long startRec = startno;
			long endRec = 0;

			if (startno + size > totalRecords) {
				endRec = totalRecords;
			} else {
				endRec = startno + size;
			}

			for (long i = startno; i < endRec; i++) {

				Resource r = result.getResource(i);
				// Resource r = it.nextResource();
				XMLResource xmlr = (XMLResource) r;
				Node dom = xmlr.getContentAsDOM();

				String n1 = dom.getNodeName();
				String v1 = dom.getNodeValue();

				// NodeList nodes_j = elementTemp.getChildNodes();
				Node nodo = dom.getFirstChild();
				NodeList nodes_j = nodo.getChildNodes();

				// NodeList nodes_j = dom.getChildNodes();
				// Creadores
				Vector creators = new Vector();
				Vector subjects = new Vector();
				Vector ids = new Vector();

				// Temp DCRecord
				DCRecord dcRecordTemp = new DCRecord();
				// Log("Total nodes:" + nodes_j.getLength());
				int limit = nodes_j.getLength();

				for (int j = 0; j < nodes_j.getLength(); j++) {

					Node node_j = nodes_j.item(j);
					// Log("Loadindentifiers NodeName");
					// Log(node_j.getNodeName()+"1");
					// Log("Nombre nodo: " + node_j.getNodeName());

					// Log("Nombre nodo: " + node_j.getNodeName());
					String out = node_j.getNodeName();

					if ("dc:general.identifier".equals(node_j.getNodeName())) {
						ids.add(nodes_j.item(j).getTextContent());
						dcRecordTemp.fullid = nodes_j.item(j).getTextContent();
					} else if ("dc:contributor".equals(node_j.getNodeName())) {
						dcRecordTemp.contributor = nodes_j.item(j)
								.getTextContent();
					} else if ("dc:general.coverage".equals(node_j
							.getNodeName())) {
						dcRecordTemp.coverage = nodes_j.item(j)
								.getTextContent();
					} else if ("dc:lifecycle.contribute.role.publisher.date.datetime"
							.equals(node_j.getNodeName())) {
						dcRecordTemp.date = nodes_j.item(j).getTextContent();
					} else if ("dc:datestamp".equals(node_j.getNodeName())) {
						dcRecordTemp.datestamp = nodes_j.item(j)
								.getTextContent();
					} else if ("dc:general.description".equals(node_j
							.getNodeName())) {
						dcRecordTemp.description = nodes_j.item(j)
								.getTextContent();
					} else if ("dc:technical.format".equals(node_j
							.getNodeName())) {
						dcRecordTemp.format = nodes_j.item(j).getTextContent();
					} else if ("dc:general.language".equals(node_j
							.getNodeName())) {
						dcRecordTemp.language = nodes_j.item(j)
								.getTextContent();
					} else if ("dc:lifecycle.contribute.role.publisher"
							.equals(node_j.getNodeName())) {
						dcRecordTemp.publisher = nodes_j.item(j)
								.getTextContent();
					} else if ("dc:relation.resource.description".equals(node_j
							.getNodeName())) {
						dcRecordTemp.relation = nodes_j.item(j)
								.getTextContent();
					} else if ("dc:rights.description".equals(node_j
							.getNodeName())) {
						dcRecordTemp.rights = nodes_j.item(j).getTextContent();
					} else if ("dc:sets".equals(node_j.getNodeName())) {
						dcRecordTemp.sets = nodes_j.item(j).getTextContent();
					} else if ("dc:relation.kind.isBasedOn.resource"
							.equals(node_j.getNodeName())) {
						dcRecordTemp.source = nodes_j.item(j).getTextContent();
					} else if ("dc:status".equals(node_j.getNodeName())) {
						dcRecordTemp.status = nodes_j.item(j).getTextContent();
					} else if ("dc:general.title".equals(node_j.getNodeName())) {
						dcRecordTemp.title = nodes_j.item(j).getTextContent();
					} else if ("dc:educational.learningResourceType"
							.equals(node_j.getNodeName())) {
						dcRecordTemp.type = nodes_j.item(j).getTextContent();
					} else if ("dc:setSpec".equals(node_j.getNodeName())) {
						// System.out.println("FOUND dc:setSpec "+nodes_j.item(j).getTextContent());
						dcRecordTemp.setSpec = nodes_j.item(j).getTextContent();
					} else if ("dc:identifier_oai".equals(node_j.getNodeName())) {
						// System.out.println("FOUND identifier_oai "+nodes_j.item(j).getTextContent());
						dcRecordTemp.identifier_oai = nodes_j.item(j)
								.getTextContent();
					} else if ("dc:collection".equals(node_j.getNodeName())) {
						dcRecordTemp.collection = nodes_j.item(j)
								.getTextContent();
					}
					/*
					 * else if("dc:fullid".equals(node_j.getNodeName())) {
					 * dcRecordTemp.fullid = nodes_j.item(j).getTextContent(); }
					 */
					else if ("dc:lifecycle.contribute.role.author"
							.equals(node_j.getNodeName())) {
						creators.add(nodes_j.item(j).getTextContent());
					} else if ("dc:general.keyword"
							.equals(node_j.getNodeName())) {
						subjects.add(nodes_j.item(j).getTextContent());
					}
				}
				dcRecordTemp.creator = creators;
				dcRecordTemp.subject = subjects;
				dcRecordTemp.identifier = ids;
				// Comparaciones para poder comparar con Datestamps de cada uno
				// de los elementos
				// Busqueda secuencial - posible mejora
				//
				// MAYO 20 DE 2008.
				// addElement = false;
				// if (hasFrom && hasUntil) {
				// Log("Tiene until y From");
				// Log("DC:DATESTAMP:" + dcRecordTemp.datestamp + " both:");
				// if ((dcRecordTemp.datestamp).compareTo(from) >= 0
				// && (dcRecordTemp.datestamp).compareTo(until) <= 0) {
				// addElement = true;
				// }
				// } else if (hasFrom) {
				// Log("Tiene From");
				// Log("DC:DATESTAMP:" + dcRecordTemp.datestamp + " from:"
				// + from);
				// if ((dcRecordTemp.datestamp).compareTo(from) >= 0) {
				// addElement = true;
				// }
				// } else if (hasUntil) {
				// Log("Tiene until ");
				// Log("Fechas comparadas:");
				// Log("DC:DATESTAMP:" + dcRecordTemp.datestamp + " until:"
				// + until);
				// if ((dcRecordTemp.datestamp).compareTo(until) <= 0) {
				// addElement = true;
				// }
				// } else {
				// addElement = true;
				// }
				// if (addElement) { // Si cumple con los criterios se envia la
				// // busqueda {
				// // Log("Adiciona elmento DC-----------");
				// identifiers.add(dcRecordTemp);
				// }
				identifiers.add(dcRecordTemp);
			}

			// }

			if (identifiers.size() == 0)
				throw (new NoRecordsMatch("no records match"));

			return identifiers;

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * Realiza la consulta de los registros DMP para el protocolo OAI en la base
	 * de datos eXist
	 * 
	 * @return Collection
	 * @throws Exception
	 */
	public Collection loadIdentfiersDMP(String from, String until, String set,
			int startno, int size) throws Exception {
		Vector identifiers = new Vector();
		try {
			org.xmldb.api.base.Collection col = null;
			System.out.println("URL=" + this.URI);
			col = DatabaseManager.getCollection(this.URI, "admin", "admin");
			col.setProperty(OutputKeys.INDENT, "no");
			XPathQueryService service = (XPathQueryService) col.getService(
					"XPathQueryService", "1.0");
			service.setProperty("indent", "yes");
			String query = "";
			from = from == null ? "" : from.trim();
			until = until == null ? "" : until.trim();
			set = set == null ? "" : set.trim();
			boolean hasFrom = from.equals("") ? false : true;
			boolean hasUntil = until.equals("") ? false : true;
			boolean hasSet = set.equals("") ? false : true;
			boolean addElement = false;

			query = queries.getString("GET_RECS");

			if (hasFrom || hasUntil || hasSet) {
				query = queries.getString("GET_RECS_BASE");
				if (hasFrom) {
					query = query
							+ queries.getString("GET_RECS_DATESTAMP_FROM");
					query = StringUtils.replaceWord(query, "?", from);
				}
				if (hasUntil) {
					query = query
							+ queries.getString("GET_RECS_DATESTAMP_UNTIL");
					query = StringUtils.replaceWord(query, "?", until);
				}
				if (hasSet) {
					query = query + queries.getString("GET_RECS_SET");
					query = StringUtils.replaceWord(query, "?", set);
				}

			}

			ResourceSet result = (service.query(query));
			long totalRecords = result.getSize();
			ResourceIterator it = result.getIterator();

			long startRec = startno;
			long endRec = 0;

			if (startno + size > totalRecords) {
				endRec = totalRecords;
			} else {
				endRec = startno + size;
			}

			for (long i = startno; i < endRec; i++) {
				Resource r = result.getResource(i);
				XMLResource xmlr = (XMLResource) r;
				Node dom = xmlr.getContentAsDOM();
				DMPRecord dmprec = new DMPRecord();
				Node nodo = dom.getFirstChild();
				NodeList nodes_j = nodo.getChildNodes();
				for (int j = 0; j < nodes_j.getLength(); j++) {
					Node node_j = nodes_j.item(j);
					if (node_j.getNodeName().equals("dc:identifier_oai")) {
						dmprec.identifier_oai = node_j.getTextContent();
					} else if (node_j.getNodeName().equals("dc:datestamp"))
						dmprec.datestamp = node_j.getTextContent();
					else if (node_j.getNodeName().equals("dc:setSpec"))
						dmprec.setSpec = node_j.getTextContent();
					else if (node_j.getNodeName().equals("dmp:doer")) {
						dmprec.doer = node_j;
					}
				}
				identifiers.add(dmprec);
			}
			if (identifiers.size() == 0)
				throw (new NoRecordsMatch("no records match"));

			return identifiers;

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * Carga todos los REcords
	 * 
	 * @return
	 * @throws Exception
	 */
	public Collection loadRecords(String set) throws Exception {
		try {
			Log("Loading REcords");
			// Mayo 22 de 2008. DatabaseManager.registerDatabase(this.database);
			org.xmldb.api.base.Collection col = null;
			// Se traen primero las colecciones para verificar que la coleccion
			// es valida
			HashSet sets = new HashSet(loadSets());
			Log("Cargando SETS identifiers");
			if (!sets.contains(set))
				throw (new BadArgument(
						"badArgument - Collection does not exist"));
			Log("Loading identifiers");
			// Se traen todos los documentos para recorres un por uno
			Log("Loading identifiers col");
			col = DatabaseManager.getCollection(this.URI + "/" + set, "admin",
					"admin");
			Log("Loading identifier despues cols");
			col.setProperty(OutputKeys.INDENT, "no");
			String[] docs = col.listResources();

			// Variable de retorno
			Vector identifiers = new Vector();
			col = DatabaseManager.getCollection(this.URI + "/" + set, "admin",
					"admin");

			XPathQueryService service = (XPathQueryService) col.getService(
					"XPathQueryService", "1.0");
			service.setProperty("indent", "yes");
			String query = "";
			Document docTem = this.documentBuilder.newDocument();

			Log("total docs" + docs.length);
			// Se recorren todo los documentos
			for (int i = 0; i < docs.length; i++) {
				Log("adentro" + docs[i]);
				query = "doc(\"" + docs[i] + "\")"
						+ queries.getString("GET_RECS");
				ResourceSet result = service.query(query);
				// service.query("").
				ResourceIterator it = result.getIterator();
				while (it.hasMoreResources()) {
					Resource r = it.nextResource();
					Element elementTemp = docTem.createElement(r.getId());
					DCRecord dcRecordTemp = new DCRecord();
					Log(r.getContent().toString());
					elementTemp.setTextContent(r.getContent().toString());
					// dcRecordTemp.
					identifiers.add(r.getContent().toString());
				}
			}

			if (identifiers.size() == 0)
				throw (new NoRecordsMatch("no records match"));

			return identifiers;

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * 
	 * @param record
	 * @return
	 * @throws Exception
	 */
	public boolean updateDCinLibray(DCRecord record) throws Exception {
		DatabaseManager.registerDatabase(this.database);
		org.xmldb.api.base.Collection col = DatabaseManager.getCollection(
				this.URI, properties.getString("USER_BD_EXIST"),
				properties.getString("PASSWORD_BD_EXIST"));

		XUpdateQueryService service = (XUpdateQueryService) col.getService(
				"XUpdateQueryService", "1.0");

		String queryUpdate = "";
		String queryRemove = "";

		try {
			String idRec = "";
			Log("Longitud record.identifier:" + record.identifier.size());
			for (int i = 0; i < record.identifier.size(); i++) {
				Log((String) record.identifier.get(i));
				idRec = (String) record.identifier.get(i);

			}
			DCRecord currentDCRec = getDCRecord(idRec);
			StringUtils.printDCRecord(currentDCRec);

			queryRemove = queries.getString("UPDATE_REMOVE_DC_HARVEST");
			queryRemove = StringUtils
					.replaceXUpdateRemoveDC(queryRemove, idRec);
			service.update(queryRemove);
			Log("Hizo remove");
			queryUpdate = queries.getString("UPDATE_INSERT_DC_HARVEST");
			queryUpdate = StringUtils.replaceXUpdateDC(queryUpdate, record);

			/*
			 * queryUpdate = queries.getString("UPDATE_UPDATE_DC_HARVEST");
			 * queryUpdate =
			 * StringUtils.replaceXUpdateDC(queryUpdate,record,idRec);
			 */

		} catch (NoRecordsMatch nrExc) {
			String idRec = "";
			Log("Longitud record.identifier:" + record.identifier.size());
			for (int i = 0; i < record.identifier.size(); i++) {
				Log((String) record.identifier.get(i));
				idRec = (String) record.identifier.get(i);

			}

			queryUpdate = queries.getString("UPDATE_INSERT_DC_HARVEST");
			queryUpdate = StringUtils.replaceXUpdateDC(queryUpdate, record);

		}

		Log("QueryUpdate:" + queryUpdate);

		service.update(queryUpdate);

		return true;

	}

	private void Log(String s) {
		if (LOG && s != null)
			System.out.println(this.getClass().getName() + ": " + s);
	}

	/**
	 * Display one element
	 */
	public static String showElement(Element e) {
		String out;
		out = "<" + e.getNodeName() + " ";
		NamedNodeMap map = e.getAttributes();
		for (int i = 0; i < map.getLength(); i++)
			out += map.item(i).getNodeName() + "=\""
					+ map.item(i).getNodeValue() + "\" ";
		out += ">\n";

		NodeList nl = e.getChildNodes();
		for (int i = 0; i < nl.getLength(); i++) {
			if (nl.item(i).getNodeType() == Node.ELEMENT_NODE)
				out += showElement((Element) nl.item(i));
			else if (nl.item(i).getNodeType() == Node.TEXT_NODE) {
				// System.out.println(nl.item(i).getNodeValue());
				out += nl.item(i).getNodeValue();
			} else
				System.out.println("other node");

		}
		out = out + "</" + e.getNodeName() + ">";
		return out;
	}

}
