package org.bdng.oai.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.TimeZone;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpUtils;

import org.bdng.oai.dataprovider.DataProvider;
import org.bdng.oai.dataprovider.DataProviderInterface;
import org.bdng.oai.dataprovider.Identity;
import org.bdng.oai.dataprovider.IdentityBdng;
import org.bdng.oai.dataprovider.RecordFactory;
import org.bdng.oai.dataprovider.TRIRecordFactory;
import org.bdng.oai.error.BadArgument;
import org.bdng.oai.error.BadVerb;
import org.bdng.oai.error.OAIError;
import org.bdng.oai.util.xml.XMLTool;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * Servler principal del proveedor OAI de metadatos
 * 
 * @author Edwin Montoya - emontoya@eafit.edu.co - U. EAFIT
 * 
 */
public class OAI extends HttpServlet {

	protected Identity config = new IdentityBdng();
	protected RecordFactory rf = new TRIRecordFactory();

	DataProviderInterface dp = null;
	OAIError oaierror = null;

	boolean LOG = false;

	/**
	 * método GET del protocolo HTTP
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		work(request, response);
	}

	/**
	 * método POST del protocolo HTTP
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		work(request, response);
	}

	/**
	 * procesa el GET o el POST del servlet
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	public void work(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/xml;charset=UTF-8");
		PrintWriter out = response.getWriter();
		// BufferedReader in;
		String formname = request.getParameter("verb");
		String token = request.getParameter("resumptionToken");
		Element elem = null;
		XMLTool xtool = new XMLTool();
		Document doc = xtool.createDocumentRoot();
		dp = new DataProvider(config, rf, doc);
		oaierror = null;
		if ((token == null) || token.trim().equals(""))
			token = null;

		try {
			// check the number of parameters
			int numofparameters = 0;
			Hashtable params = new Hashtable();

			String queryString = request.getQueryString();
			if (queryString != null) {
				StringTokenizer st = new StringTokenizer(queryString, "&");
				while (st.hasMoreTokens()) {
					Object o = params.put(st.nextToken(), "anything");
					if (o != null) {
						throw new BadArgument("Duplicate Argument");
					}
					numofparameters++;
				}
			}
			if (numofparameters == 0)
				throw new BadVerb("No verb found");
			if ((token != null) && (numofparameters > 2))
				throw new BadArgument("The wrong argument with resumptionToken");

			if (formname.equals("Identify")) {
				if (numofparameters != 1)
					throw new BadArgument("bad argument for Identify");
				elem = dp.identify();
			}

			else if (formname.equals("ListIdentifiers")) {

				// if (request.getParameter("set")!= null) {
				Log("FROM :" + request.getParameter("from"));
				Log("UNTIL :" + request.getParameter("until"));
				Log("joeee");
				// elem=dp.listIdentifiers(clear(request.getParameter("from")),clear(request.getParameter("until")),clear(request.getParameter("set")),clear(request.getParameter("metadataPrefix")));
				// elem=dp.listRecords(clear(request.getParameter("from")),clear(request.getParameter("until")),clear(request.getParameter("set")),clear(request.getParameter("metadataPrefix")));
				log("despues consulta listIdentifiers");
				// }

				if (token == null) {

					elem = dp.listIdentifiers(
							clear(request.getParameter("from")),
							clear(request.getParameter("until")),
							clear(request.getParameter("set")),
							clear(request.getParameter("metadataPrefix")));
				} else {
					elem = dp.listIdentifiers(token);
				}
			}

			else if (formname.equals("GetRecord")) {
				// elem=dp.getRecord(clear(request.getParameter("identifier")),clear(request.getParameter("metadataPrefix")));
				elem = dp.getRecord(clear(request.getParameter("identifier")),
						clear(request.getParameter("metadataPrefix")));
			} else if (formname.equals("ListRecords")) {
				if (token == null) {
					Log("ListRecords");
					Log("FROM :" + request.getParameter("from"));
					Log("UNTIL :" + request.getParameter("until"));
					// elem=dp.listRecords(clear(request.getParameter("from")),clear(request.getParameter("until")),clear(request.getParameter("set")),clear(request.getParameter("metadataPrefix")));
					// OjolistIdentifiers
					// elem=dp.listIdentifiers(clear(request.getParameter("from")),clear(request.getParameter("until")),clear(request.getParameter("set")),clear(request.getParameter("metadataPrefix")));
					elem = dp.listRecords(clear(request.getParameter("from")),
							clear(request.getParameter("until")),
							clear(request.getParameter("set")),
							clear(request.getParameter("metadataPrefix")));
				} else
					elem = dp.listRecords(token);
			} else if (formname.equals("ListSets")) {
				if (token == null)
					elem = dp.listSets();
				else
					elem = dp.listSets(token);
			} else if (formname.equals("ListMetadataFormats")) { // se modifica
																	// la F por
																	// mayuscula
																	// para
																	// seguir
																	// con el
																	// estandar
																	// OAI-PMH
				elem = dp.listMetadataFormats(clear(request
						.getParameter("identifier")));
			} else
				throw (new BadVerb("badVerb"));
		} catch (OAIError err) {
			log("OAIError");
			oaierror = err;
			elem = doc.createElement("error");
			elem.setAttribute("code", err.getCode());
			elem.appendChild(doc.createTextNode(err.toString()));

		}

		Element root = doc.createElement("OAI-PMH");
		root.setAttribute("xmlns", "http://www.openarchives.org/OAI/2.0/");
		root.setAttribute("xmlns:xsi",
				"http://www.w3.org/2001/XMLSchema-instance");
		root.setAttribute(
				"xsi:schemaLocation",
				"http://www.openarchives.org/OAI/2.0/"
						+ "     http://www.openarchives.org/OAI/2.0/OAI-PMH.xsd");

		Element date = doc.createElement("responseDate");
		SimpleDateFormat simple = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
		simple.setTimeZone(TimeZone.getTimeZone("GMT"));

		String datestring = simple.format(new java.util.Date()) + "Z";
		date.appendChild(doc.createTextNode(datestring));
		root.appendChild(date);

		Element url = doc.createElement("request");
		if ((oaierror != null)
				&& (oaierror.getCode().equals("badVerb") || oaierror.getCode()
						.equals("badArgument"))) {
			url.appendChild(doc.createTextNode(HttpUtils.getRequestURL(request)
					.toString()));
		} else {
			for (Enumeration enum1 = request.getParameterNames(); enum1
					.hasMoreElements();) {
				String key = (String) enum1.nextElement();
				String value = (String) request.getParameter(key);
				url.setAttribute(key, value);
			}

			url.appendChild(doc.createTextNode(HttpUtils.getRequestURL(request)
					.toString()));

		}
		root.appendChild(url);
		root.appendChild(elem);
		doc.appendChild(root);
		XMLTool.Dom2Stream(doc, out);

		out.close();
		return;
	}

	private String clear(String in) {
		if (in == null)
			return null;
		else if (in.trim().equals(""))
			return null;
		else
			return in;
	}

	/**
	 * Special requirement in OAI for encoding & only in request URL
	 * 
	 * @param input
	 * @return
	 */

	private static String encodeAMP(String input) {
		String result = new String();
		StringTokenizer st = new StringTokenizer(input, "&");
		for (; st.hasMoreTokens();)
			result = result + st.nextToken() + "&amp;";
		result = result.substring(0, result.length() - 5);
		return result;
	}

	private void Log(String s) {
		if (LOG)
			System.out.println("OAI:" + s);
	}

}
