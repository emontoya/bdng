/**
 * 
 */
package org.bdng.oai.dataprovider;

import org.bdng.oai.dataprovider.DCRecord;
import org.bdng.oai.db.DBExist;
import org.bdng.oai.harvester.HarvesterInterface;

/**
 * @author Lucas, C�stulo Ramirez Londo�o.
 * 
 */
public class HarvesterUpdaterEAFIT implements HarvesterInterface {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.bdng.oai.harvester.HarvesterInterface#updateLibrary(org.bdng.oai.
	 * dataprovider.DCRecord)
	 */
	public void updateLibrary(DCRecord record) {
		// TODO Auto-generated method stub

		try {
			DBExist db = DBExist.getInstance();
			db.updateDCinLibray(record);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
