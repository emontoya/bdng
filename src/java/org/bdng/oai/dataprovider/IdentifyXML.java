package org.bdng.oai.dataprovider;

import java.util.List;
import org.bdng.harvmgr.dlConfig;
import org.jdom2.Document;
import java.util.List;
import org.jdom2.Element;

//esta clase se encarga de cargar los valores para el verbo Identify a travez del archivo oai_identify.xml

public class IdentifyXML {

	String repositoryNameSt = null;
	String baseURLSt = null;
	String protocolVersionSt = null;
	String adminEmailSt = null;
	String earliestDatestampSt = null;
	String deletedRecordSt = null;
	String granularitySt = null;
	String schemeSt = null;
	String repositoryIdentifierSt = null;
	String delimiterSt = null;
	String sampleIdentifierSt = null;

	void init() {

		dlConfig doa = new dlConfig(System.getProperty("user.dir") + "/"
				+ "../");
		List l = doa.GetVisualIdentify();

		Element Identify = (Element) l.get(0);// 0 pues por ahora solo hay un
												// Identify (el de bdcol)

		Element repositoryName = Identify.getChild("repositoryName");
		Element baseURL = Identify.getChild("baseURL");
		Element protocolVersion = Identify.getChild("protocolVersion");
		Element earliestDatestamp = Identify.getChild("earliestDatestamp");
		Element adminEmail = Identify.getChild("adminEmail");
		Element deletedRecord = Identify.getChild("deletedRecord");
		Element granularity = Identify.getChild("granularity");
		Element scheme = Identify.getChild("scheme");
		Element repositoryIdentifier = Identify
				.getChild("repositoryIdentifier");
		Element delimiter = Identify.getChild("delimiter");
		Element sampleIdentifier = Identify.getChild("sampleIdentifier");

		repositoryNameSt = repositoryName.getText();
		baseURLSt = baseURL.getText();
		protocolVersionSt = protocolVersion.getText();
		adminEmailSt = adminEmail.getText();
		earliestDatestampSt = earliestDatestamp.getText();
		deletedRecordSt = deletedRecord.getText();
		granularitySt = granularity.getText();
		schemeSt = scheme.getText();
		repositoryIdentifierSt = repositoryIdentifier.getText();
		delimiterSt = delimiter.getText();
		sampleIdentifierSt = sampleIdentifier.getText();

	}

	public String getOAIversion() {

		return protocolVersionSt;
	}

	public String getBaseURL() {

		return baseURLSt;
	}

	public String getName() {

		return repositoryNameSt;
	}

	public String getAdminemail() {

		return adminEmailSt;
	}

	public String getID() {

		return repositoryIdentifierSt;
	}

	public String getDelimiter() {

		return delimiterSt;
	}

	public String getSampleIdentifier() {

		return sampleIdentifierSt;
	}

	public String getSchema() {

		return schemeSt;
	}

	public String getEarliestDatestamp() {

		return earliestDatestampSt;
	}

	public String getDeletedItem() {

		return deletedRecordSt;
	}

	public String getGranularity() {

		return granularitySt;
	}

}
