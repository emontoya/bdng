package org.bdng.oai.dataprovider;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.StringTokenizer;
import java.util.Vector;

import org.bdng.oai.dataprovider.DCRecord;
import org.bdng.oai.dataprovider.RecordFactory;
import org.bdng.oai.db.DBExist;

public class TRIRecordFactory implements RecordFactory {

	public TRIRecordFactory() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.bdng.oai.dataprovider.RecordFactory#getSets()
	 */
	public java.util.Vector getSets() {
		try {
			DBExist db = DBExist.getInstance();
			Vector v = new Vector(db.loadSets());

			return v;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new Vector();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.bdng.oai.dataprovider.RecordFactory#getSets(int, int)
	 */
	public Vector getSets(int startno, int size) {
		Vector v = new Vector();
		v.add("Coleccion 1");
		v.add("Coleccion 2");
		v.add("Coleccion 3");
		v.add("Coleccion 4");

		Statement s = null;
		/*
		 * try{ connection=m_connectionPool.getConnection();
		 * s=connection.createStatement(); String query="select distinct archive
		 * from dc"; ResultSet rs= s.executeQuery(query); int count=0;
		 * while(rs.next()&&(count<(startno+size))){ count++; if
		 * (count>startno){ v.add(rs.getString("archive")); } } } catch
		 * (SQLException e){ e.printStackTrace();; } finally{ try{ s.close();
		 * closeConnection(); } catch(Exception e){ System.out.println(e); } }
		 */
		return v;
	}

	public Vector getRecords(String from, String until, String setspec,
			int startno, int size) {
		try {
			DBExist db = DBExist.getInstance();
			Vector v = new Vector(db.loadIdentfiers(from, until, setspec,
					startno, size));
			return v;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new Vector();
	}

	public Vector getRecordsDMP(String from, String until, String setspec,
			int startno, int size) {
		try {
			DBExist db = DBExist.getInstance();
			Vector v = new Vector(db.loadIdentfiersDMP(from, until, setspec,
					startno, size));
			return v;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new Vector();
	}

	/*
	 * public Vector getRecords(String fileafter,String filebefore,String
	 * setspec,int startno, int size){ Vector v=new Vector();
	 * 
	 * DCRecord record=null; Statement s=null; /*try{
	 */
	/*
	 * connection=m_connectionPool.getConnection();
	 * s=connection.createStatement(); String query="select id from dc "; String
	 * whereClause=null; if ((fileafter!=null)&&(!fileafter.equals("")))
	 * whereClause="datestamp >='"+fileafter+"' ";
	 * 
	 * if ((filebefore!=null) &&(!filebefore.equals(""))){ if
	 * (whereClause!=null)
	 * whereClause=whereClause+" and datestamp<='"+filebefore+"'"; else
	 * whereClause=" datestamp<='"+fileafter+"'"; }
	 * 
	 * if (setspec!=null){ if (whereClause!=null) whereClause=whereClause+" and
	 * archive='"+setspec+"'"; else whereClause=" archive='"+setspec+"'"; }
	 * 
	 * if (whereClause!=null) query=query+" where "+ whereClause+" order by id";
	 * 
	 * ResultSet rs= s.executeQuery(query);
	 * 
	 * 
	 * int count=0; while (rs.next()&&(count<(startno+size))){ count++; if
	 * (count>startno){ String id=rs.getString("id"); System.out.println(id);
	 * record=getRecord(connection,id); v.add(record); } } } catch (SQLException
	 * e){ e.printStackTrace();; } finally{ try{ s.close(); closeConnection(); }
	 * catch(Exception e){ e.printStackTrace(); } }
	 * 
	 * return v; }
	 */

	public DCRecord getRecord(Connection connection, String handle) {
		DCRecord record = null;
		Statement s = null;
		try {
			s = connection.createStatement();
			ResultSet rs = s.executeQuery("select * from dc where id='"
					+ handle + "'");
			while (rs.next()) {
				record = getRecord(rs);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				s.close();
			} catch (Exception e) {
				System.out.println(e);
			}
		}

		return record;
	}

	public DCRecord getRecord(String handle) {
		DCRecord record = null;
		try {
			DBExist db = DBExist.getInstance();
			record = db.getDCRecord(handle);
			return record;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
		/*
		 * Statement s=null; try{ connection=m_connectionPool.getConnection();
		 * s=connection.createStatement(); ResultSet rs= s.executeQuery("select
		 * * from dc where id='"+handle+"'"); while (rs.next()){
		 * record=getRecord(rs); } } catch (SQLException e){
		 * e.printStackTrace(); } finally{ try{ s.close(); closeConnection(); }
		 * catch(Exception e){ System.out.println(e); } }
		 */

	}

	private DCRecord getRecord(ResultSet rs) throws SQLException {
		DCRecord record = new DCRecord();
		record.fullid = rs.getString("id");
		record.datestamp = rs.getString("datestamp");
		record.status = rs.getString("status");
		record.sets = rs.getString("archive");
		record.title = rs.getString("title");

		record.creator = new Vector();
		String creator = rs.getString("creator");
		if (creator != null) {
			StringTokenizer st = new StringTokenizer(creator, "|");
			for (; st.hasMoreTokens();)
				record.creator.add(st.nextToken());
		}
		record.subject = new Vector();
		String subject = rs.getString("subject");
		if (subject != null) {
			StringTokenizer st = new StringTokenizer(subject, "|");
			for (; st.hasMoreTokens();)
				record.subject.add(st.nextToken());
		}

		record.identifier = new Vector();
		String identifier = rs.getString("identifier");
		if (identifier != null) {
			StringTokenizer st = new StringTokenizer(identifier, "|");
			for (; st.hasMoreTokens();)
				record.identifier.add(st.nextToken());
		}

		/*
		 * There is no long type in mysql for description, but the historical
		 * database of arc uses long, in the new versions "string" is used. for
		 * historical arc database, we need comment this line. but for new arc
		 * installation and mysql database, we could turn on this line. It's a
		 * pain. I have to find a way of converting long type to string in
		 * oracle, ugh!!!!
		 */
		record.description = rs.getString("description");
		record.publisher = rs.getString("publisher");
		record.contributor = rs.getString("contributor");
		record.date = rs.getString("discovery");
		record.type = rs.getString("type");
		record.format = rs.getString("format");
		record.source = rs.getString("source");
		record.language = rs.getString("language");
		record.relation = rs.getString("relation");
		record.coverage = rs.getString("coverage");
		record.rights = rs.getString("rights");
		return record;
	}

	public void closeConnection() {
		/*
		 * if (connection!=null) m_connectionPool.close(connection);
		 * connection=null;
		 */
	}

}
