package org.bdng.oai.dataprovider.servlet;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.bdng.oai.db.DBExist;

public class DataProviderConfig implements Filter {

	public void init(FilterConfig arg0) throws ServletException {
		try {
			System.out.println("DataProviderConfig.init(1)");
			System.out.println("DataProviderConfig.init(2)");
			System.out.println("DataProviderConfig.init(3)");
			DBExist.getInstance();
			System.out.println("DataProviderConfig.init(4)");
			System.out.println("DataProviderConfig.init(5)");
			System.out.println("DataProviderConfig.init(6)");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void destroy() {
		// TODO Auto-generated method stub

	}

	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		// TODO Auto-generated method stub
		chain.doFilter(request, response);
	}
}