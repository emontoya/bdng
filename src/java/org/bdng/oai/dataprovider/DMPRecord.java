package org.bdng.oai.dataprovider;

/**
 * @author Edwin Montoya
 *
 */
import java.util.Vector;

import org.w3c.dom.Node;

/**
 * DMP record
 */

public class DMPRecord {
	public Node doer;
	// oai
	public String datestamp;
	public String identifier_oai;
	public String setSpec;

	public String toString() {
		String result = null;
		result = result + "identifier_oai= " + identifier_oai + "/n";
		result = result + " datestamp= " + datestamp + "/n";
		result = result + " setSpec= " + setSpec + "/n";
		return result;
	}
}
