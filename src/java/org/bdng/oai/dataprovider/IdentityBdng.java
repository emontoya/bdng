package org.bdng.oai.dataprovider;

import org.bdng.oai.dataprovider.Identity;

public class IdentityBdng implements Identity {

	public String getOAIversion() {
		// TODO Auto-generated method stub
		return "2.0";
	}

	public String getBaseURL() {
		// TODO Auto-generated method stub
		return "http://test.bdcol.org:8080/admin/servlet/OAI";
	}

	public String getName() {
		// TODO Auto-generated method stub
		return "Biblioteca Digital Colombiana";
	}

	public String getAdminemail() {
		// TODO Auto-generated method stub
		return "contacto@bdcol.org";
	}

	public String getID() {
		// TODO Auto-generated method stub
		return "oai.bdcol.org";
	}

	public String getDelimiter() {
		// TODO Auto-generated method stub
		return ":";
	}

	public String getSampleIdentifier() {
		// TODO Auto-generated method stub
		return "oai:repository.bdcol.org:1040/1";
	}

	public String getSchema() {
		// TODO Auto-generated method stub
		return "oai";
	}

	public String getEarliestDatestamp() {
		// TODO Auto-generated method stub
		return "1999-01-01";
	}

	public String getDeletedItem() {
		// TODO Auto-generated method stub
		return "transient";
	}

	public String getGranularity() {
		// TODO Auto-generated method stubd
		// return "YYYY-MM-DD, YYYY-MM-DDThh:mm:ssZ";
		return "YYYY-MM-DD";
	}

}
