package org.bdng.oai.dataprovider;

/**
 * @author Lcuas Fl�rez
 *
 */
import java.util.Vector;

/**
 * Dublin Core record
 */

public class DCRecord {

	// For oai
	public String fullid;
	public String datestamp;
	public String status;
	public String identifier_oai;
	public String setSpec; // no usado
	public String collection;

	// For dc only
	public String title;
	public String description;
	public String publisher;
	public String contributor;
	public String date;
	public String sets;
	public String type;
	public String format;
	public Vector identifier;
	public String source;
	public String language;
	public String relation;
	public String coverage;
	public String rights;
	public Vector creator;
	public Vector subject;

	// public String collection;
	/** convert to string format */
	public String toString() {
		String result = null;
		result = result + "fullid= " + fullid + "/n";
		result = result + " datestamp= " + datestamp + "/n";
		result = result + " title= " + title + "/n";
		result = result + " description =" + description + "/n";
		return result;
	}

}
