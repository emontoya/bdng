package org.bdng.oai.dataprovider;

import java.text.SimpleDateFormat;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.ResourceBundle;
import java.util.Vector;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

import org.bdng.harvmgr.dlConfig;
import org.bdng.oai.dataprovider.DCRecord;
import org.bdng.oai.dataprovider.DataProviderInterface;
import org.bdng.oai.dataprovider.Identity;
import org.bdng.oai.dataprovider.RecordFactory;
import org.bdng.oai.dataprovider.token.IdentifierToken;
import org.bdng.oai.dataprovider.token.RecordsToken;
import org.bdng.oai.dataprovider.token.SetToken;
import org.bdng.oai.dataprovider.token.Token;
import org.bdng.oai.dataprovider.token.TokenModem;
import org.bdng.oai.error.BadArgument;
import org.bdng.oai.error.BadResumptionToken;
import org.bdng.oai.error.CannotDisseminateFormat;
import org.bdng.oai.error.IdDoesNotExist;
import org.bdng.oai.error.NoRecordsMatch;
import org.bdng.oai.error.OAIError;

import java.util.List;

/**
 * 
 * @author Edwin Montoya - emontoya@eafit.edu.co - U. EAFIT
 * 
 */
public class DataProvider implements DataProviderInterface {

	boolean LOG = true;

	Identity config;
	RecordFactory rf = null;
	int cursor = 0;
	Document doc = null;
	private ResourceBundle properties = ResourceBundle.getBundle("oai");
	HashMap<String, String> map;

	/**
	 * Constructor.
	 * 
	 * @param config
	 * @param rf
	 * @param doc
	 */

	public DataProvider(Identity config, RecordFactory rf, Document doc) {
		this.config = config;
		this.rf = rf;
		this.doc = doc;

		// mapeado de types contra setSpec - Pasar a un metodo que cargue esta
		// lista de un xml
		map = new HashMap<String, String>();
		map.put("article", "Driver");
		map.put("bachelorThesis", "Driver");
		map.put("masterThesis", "Driver");
		map.put("doctoralThesis", "Driver");
		map.put("book", "Driver");
		map.put("bookPart", "Driver");
		map.put("review", "Driver");
		map.put("conferenceObject", "Driver");
		map.put("lecture", "Driver");
		map.put("workingPaper", "Driver");
		map.put("preprint", "Driver");
		map.put("report", "Driver");
		map.put("annotation", "Driver");
		map.put("contributionToPeriodical", "Driver");
		map.put("patent", "Driver");
		map.put("other", "Driver");

		map.put("general", "Bdcol");
		map.put("learningObject", "Bdcol");
		map.put("dataset", "Bdcol");
		map.put("historicalDocument", "Bdcol");
		map.put("image", "Bdcol");
		map.put("journal", "Bdcol");
		map.put("map", "Bdcol");
		map.put("thesis", "Bdcol");
		map.put("presentation", "Bdcol");
		map.put("simulation", "Bdcol");
		map.put("sound", "Bdcol");
		map.put("video", "Bdcol");
		map.put("institutionalDocument", "Bdcol");
		map.put("manuscript", "Bdcol");
		map.put("newspaper", "Bdcol");
		map.put("software", "Bdcol");
		map.put("sheetMusic", "Bdcol");
	}

	/**
	 * Constructor
	 * 
	 * @param config
	 * @param rf
	 */

	public DataProvider(Identity config, RecordFactory rf) {
		this.config = config;
		this.rf = rf;
	}

	/**
	 * 
	 * @param doc
	 */

	public void setDocument(Document doc) {
		this.doc = doc;
	}

	/**
	 * 
	 * @return Element
	 */
	public Element identify() {

		IdentifyXML identify = new IdentifyXML();
		identify.init();

		Element root = doc.createElement("Identify");
		Element curr;
		Text text;
		doc.appendChild(root);

		curr = doc.createElement("repositoryName");
		curr.appendChild(doc.createTextNode(identify.getName()));
		root.appendChild(curr);

		curr = doc.createElement("baseURL");
		curr.appendChild(doc.createTextNode(identify.getBaseURL()));
		root.appendChild(curr);

		curr = doc.createElement("protocolVersion");
		curr.appendChild(doc.createTextNode(identify.getOAIversion()));
		root.appendChild(curr);

		curr = doc.createElement("adminEmail");
		curr.appendChild(doc.createTextNode(identify.getAdminemail()));
		root.appendChild(curr);

		curr = doc.createElement("earliestDatestamp");
		curr.appendChild(doc.createTextNode(identify.getEarliestDatestamp()));
		root.appendChild(curr);

		curr = doc.createElement("deletedRecord");
		curr.appendChild(doc.createTextNode(identify.getDeletedItem()));
		root.appendChild(curr);

		curr = doc.createElement("granularity");
		curr.appendChild(doc.createTextNode(identify.getGranularity()));
		root.appendChild(curr);

		curr = doc.createElement("description");
		root.appendChild(curr);
		Element oaiIdentifier = doc.createElement("oai-identifier");
		curr.appendChild(oaiIdentifier);

		oaiIdentifier.setAttribute("xmlns",
				"http://www.openarchives.org/OAI/2.0/oai-identifier");
		oaiIdentifier.setAttribute("xmlns:xsi",
				"http://www.w3.org/2001/XMLSchema-instance");
		oaiIdentifier
				.setAttribute(
						"xsi:schemaLocation",
						"http://www.openarchives.org/OAI/2.0/oai-identifier  http://www.openarchives.org/OAI/2.0/oai-identifier.xsd");

		curr = doc.createElement("scheme");
		curr.appendChild(doc.createTextNode(identify.getSchema()));
		oaiIdentifier.appendChild(curr);

		curr = doc.createElement("repositoryIdentifier");
		curr.appendChild(doc.createTextNode(identify.getID()));
		oaiIdentifier.appendChild(curr);

		curr = doc.createElement("delimiter");
		curr.appendChild(doc.createTextNode(identify.getDelimiter()));
		oaiIdentifier.appendChild(curr);

		curr = doc.createElement("sampleIdentifier");
		curr.appendChild(doc.createTextNode(identify.getSampleIdentifier()));
		oaiIdentifier.appendChild(curr);
		return root;

	}

	/**
	 * 
	 * @return Element
	 */
	public Element listSets() throws OAIError {

		Element root = doc.createElement("ListSets");
		Element curr, parent;
		Text text;

		// Codigo de pueba
		Vector v = new Vector();
		// v = rf.getSets();

		// Se adiciona de forma estática, modificarlo para generarlo de forma
		// dinámica.

		v.add("Driver");
		v.add("Bdcol");
		/*
		 * v.add("Coleccion 1"); v.add("Coleccion 2"); v.add("Coleccion 3");
		 * v.add("Coleccion 4");
		 */
		// Vector v=rf.getSets(cursor,Token.TOKENSIZE);
		doc.appendChild(root);

		for (Enumeration enum1 = v.elements(); enum1.hasMoreElements();) {
			curr = doc.createElement("set");
			root.appendChild(curr);
			parent = curr;

			String name = (String) enum1.nextElement();
			curr = doc.createElement("setSpec");
			curr.appendChild(doc.createTextNode(name));
			parent.appendChild(curr);

			curr = doc.createElement("setName");
			curr.appendChild(doc.createTextNode(name));
			parent.appendChild(curr);
		}

		if (v.size() == Token.TOKENSIZE) {
			SetToken token = new SetToken();
			token.setCursor(cursor + Token.TOKENSIZE);
			curr = doc.createElement("resumptionToken");
			curr.appendChild(doc.createTextNode(TokenModem.encode(token)));
			root.appendChild(curr);
		}
		// reset cursor;
		cursor = 0;
		return root;

	}

	/**
	 * 
	 * @param resumptionToken
	 * @return Element
	 */
	public Element listSets(String resumptionToken) throws OAIError {
		Token token = TokenModem.decode(resumptionToken);
		if (token.getType() == Token.SET_TOKEN) {
			cursor = token.getCursor();
			return listSets();
		} else
			throw new BadResumptionToken("malformat");

	}

	/**
	 * 
	 * @param id
	 * @return Element
	 */

	public Element listMetadataFormats(String id) throws OAIError {

		DCRecord record = null;
		if (id != null) {
			record = rf.getRecord(id);

			if (record == null)
				throw new IdDoesNotExist("id not exist");
		}

		Element root = doc.createElement("ListMetadataFormats");

		Element curr, parent;
		Text text;
		doc.appendChild(root);

		if ((record != null) || (id == null)) {
			curr = doc.createElement("metadataFormat");
			root.appendChild(curr);
			parent = curr;

			curr = doc.createElement("metadataPrefix");
			curr.appendChild(doc.createTextNode("oai_dc"));
			parent.appendChild(curr);

			curr = doc.createElement("schema");
			curr.appendChild(doc
					.createTextNode("http://www.openarchives.org/OAI/2.0/oai_dc.xsd"));
			parent.appendChild(curr);

			curr = doc.createElement("metadataNamespace");
			curr.appendChild(doc
					.createTextNode("http://www.openarchives.org/OAI/2.0/oai_dc"));
			parent.appendChild(curr);

		} else
			throw new IdDoesNotExist("id not exist");
		return root;
	}

	/**
	 * 
	 * @param from
	 * @param until
	 * @param setspec
	 * @param metaformat
	 * @return Element
	 */
	public Element listIdentifiers(String from, String until, String setspec,
			String metaformat) throws OAIError {

		Log("FROM :" + from);
		Log("UNTIL :" + until);
		if ((from != null) && (!from.trim().equals("")))
			from = checkValidaty(from);
		else
			from = null;

		if ((until != null) && (!until.trim().equals("")))
			until = checkValidaty(until);
		else
			until = null;

		if (metaformat == null) {
			// metaformat = "oai_dc";
			throw new BadArgument("Missing metadataPrefix parameter");

		}
		// throw new BadArgument("metadataPrefix error");

		if ((setspec == null) || (setspec.trim().equals("")))
			setspec = null;

		if ((metaformat != null) && (!metaformat.trim().equals(""))
				&& (!metaformat.equals("oai_dc")))
			throw new CannotDisseminateFormat("can not disseminate "
					+ metaformat);

		Vector v = rf.getRecords(from, until, setspec, cursor, Token.TOKENSIZE);
		// OJO
		// Vector
		// v=rf.getRecords(fileafter,filebefore,setspec,cursor,Token.TOKENSIZE
		// );
		if ((v == null) || (v.isEmpty()))
			throw new NoRecordsMatch("not items match");

		Element root = doc.createElement("ListIdentifiers");
		Element curr, parent;
		doc.appendChild(root);
		parent = root;
		Log("Cantidad Elementos listIdentifiers " + v.size());

		for (Enumeration e = v.elements(); e.hasMoreElements();) {
			DCRecord recordTemp = new DCRecord();
			// OJO recordTemp.fullid = (String)e.nextElement();
			DCRecord record = (DCRecord) (e.nextElement());
			// DCRecord record=recordTemp;

			curr = doc.createElement("header");
			Element header = curr;
			root.appendChild(header);

			if (record.identifier_oai != null) {
				curr = doc.createElement("identifier");
				curr.appendChild(doc.createTextNode(record.identifier_oai));
				header.appendChild(curr);
			}

			if (record.datestamp != null) {
				curr = doc.createElement("datestamp");
				curr.appendChild(doc.createTextNode(record.datestamp));
				header.appendChild(curr);
			}

			if (record.collection != null) {
				curr = doc.createElement("setSpec");
				curr.appendChild(doc.createTextNode(record.collection));
				header.appendChild(curr);
			}
		}

		if (Token.TOKENSIZE == v.size()) {
			IdentifierToken token = new IdentifierToken();
			token.setCursor(cursor + Token.TOKENSIZE);
			token.from = from;
			token.until = until;
			token.metadataPrefix = metaformat;
			token.set = setspec;
			curr = doc.createElement("resumptionToken");
			curr.appendChild(doc.createTextNode(TokenModem.encode(token)));
			root.appendChild(curr);
		}

		// reset cursor
		cursor = 0;
		return root;

	}

	/**
	 * 
	 * @param resumptionToken
	 * @return Element
	 */
	public Element listIdentifiers(String resumptionToken) throws OAIError {
		IdentifierToken token = (IdentifierToken) (TokenModem
				.decode(resumptionToken));
		if (token.getType() == Token.IDENTIFIER_TOKEN) {
			cursor = token.getCursor();
			return listIdentifiers(token.from, token.until, token.set,
					token.metadataPrefix);
		} else
			throw new BadResumptionToken("malformat");

	}

	/**
	 * 
	 * @param from
	 * @param until
	 * @param setspec
	 * @param metaformat
	 * @return Element
	 */
	public Element listRecords(String from, String until, String setspec,
			String metaformat) throws OAIError {

		if ((from != null) && (!from.trim().equals("")))
			from = checkValidaty(from);
		else
			from = null;

		if ((until != null) && (!until.trim().equals("")))
			until = checkValidaty(until);
		else
			until = null;

		if (metaformat == null)
			throw new BadArgument("Missing metadataPrefix parameter");

		// int totalRecords =
		// Integer.parseInt(properties.getString("RESUMPTION_TOKEN"));
		Vector v = null;
		if (metaformat.equals("oai_dc"))
			v = rf.getRecords(from, until, setspec, cursor, Token.TOKENSIZE);
		else if (metaformat.equals("oai_dmp"))
			v = rf.getRecordsDMP(from, until, setspec, cursor, Token.TOKENSIZE);
		// Vector
		// v=rf.getRecords(fileafter,filebefore,setspec,cursor,Token.TOKENSIZE);OJO
		if ((v == null) || (v.isEmpty()))
			throw new NoRecordsMatch("not items match");

		Element root = doc.createElement("ListRecords");

		Element curr;
		doc.appendChild(root);

		if (metaformat.equals("oai_dc")) {
			for (Enumeration e = v.elements(); e.hasMoreElements();) {
				createRecordElement(doc, root, (DCRecord) (e.nextElement()),
						metaformat);
			}
		} else if (metaformat.equals("oai_dmp")) {
			for (Enumeration e = v.elements(); e.hasMoreElements();) {
				createRecordElementDMP(doc, root,
						(DMPRecord) (e.nextElement()), metaformat);
			}

		}

		if (Token.TOKENSIZE == v.size()) {
			RecordsToken token = new RecordsToken();
			token.setCursor(cursor + Token.TOKENSIZE);
			token.from = from;
			token.until = until;
			token.set = setspec;
			token.metadataPrefix = metaformat;
			curr = doc.createElement("resumptionToken");
			curr.appendChild(doc.createTextNode(TokenModem.encode(token)));
			root.appendChild(curr);
		}

		cursor = 0;
		return root;
	}

	/**
	 * 
	 * @param resumptionToken
	 * @return Element
	 */

	public Element listRecords(String resumptionToken) throws OAIError {
		RecordsToken token = (RecordsToken) (TokenModem.decode(resumptionToken));

		if (token.getType() == Token.RECORDS_TOKEN) {
			cursor = token.getCursor();
			return listRecords(token.from, token.until, token.set,
					token.metadataPrefix);
		} else
			throw new BadResumptionToken("malformat");

	}

	/**
	 * 
	 * @param handle
	 * @param metaformat
	 * @return Element
	 */
	public Element getRecord(String handle, String metaformat) throws OAIError {
		if (metaformat == null) {
			throw new BadArgument("Missing metadataPrefix parameter");
		}

		if ((metaformat == null) || (handle == null))
			throw new BadArgument("Missing Mandatory Field");

		// handle=handle.substring(handle.lastIndexOf(":")+1);

		if (!metaformat.equals("oai_dc")) {
			throw new CannotDisseminateFormat("can not disseminate "
					+ metaformat);
		}

		DCRecord record = rf.getRecord(handle);
		if (record == null)
			throw new IdDoesNotExist("id not exist");
		Element root = doc.createElement("GetRecord");

		Element curr;
		doc.appendChild(root);

		createRecordElement(doc, root, record, metaformat);

		return root;
	}

	private void createRecordElement(Document doc, Element root,
			DCRecord record, String metaformat) {
		Element recordElmt, curr, header, metadata, dc;
		curr = doc.createElement("record");
		root.appendChild(curr);
		recordElmt = curr;

		curr = doc.createElement("header");
		header = curr;
		recordElmt.appendChild(header);

		if (record.identifier_oai != null) {
			curr = doc.createElement("identifier");
			curr.appendChild(doc.createTextNode(record.identifier_oai));
			header.appendChild(curr);
		}

		if (record.datestamp != null) {
			curr = doc.createElement("datestamp");
			curr.appendChild(doc.createTextNode(record.datestamp));
			header.appendChild(curr);
		}

		curr = doc.createElement("setSpec");
		String value = map.containsKey(record.type) ? map.get(record.type)
				: "other";
		curr.appendChild(doc.createTextNode(value));
		header.appendChild(curr);

		if (!metaformat.equals("oai_dc"))
			return;

		metadata = doc.createElement("metadata");
		recordElmt.appendChild(metadata);
		curr = doc.createElement("oai_dc:dc");
		addDCNameSpace(curr);
		dc = curr;
		metadata.appendChild(dc);

		if (record.title != null) {
			curr = doc.createElement("dc:title");
			curr.appendChild(doc.createTextNode(record.title));
			dc.appendChild(curr);
		}

		if (record.format != null) {
			curr = doc.createElement("dc:format");
			curr.appendChild(doc.createTextNode(record.format));
			dc.appendChild(curr);
		}

		if (record.creator != null) {
			for (Enumeration e = record.creator.elements(); e.hasMoreElements();) {
				curr = doc.createElement("dc:creator");
				curr.appendChild(doc.createTextNode((String) (e.nextElement())));
				dc.appendChild(curr);
			}
		}

		if (record.subject != null) {
			for (Enumeration e = record.subject.elements(); e.hasMoreElements();) {
				curr = doc.createElement("dc:subject");
				curr.appendChild(doc.createTextNode((String) (e.nextElement())));
				dc.appendChild(curr);
			}
		}

		if (record.description != null) {
			curr = doc.createElement("dc:description");
			curr.appendChild(doc.createTextNode(record.description));
			dc.appendChild(curr);
		}

		if (record.rights != null) {
			curr = doc.createElement("dc:rights");
			curr.appendChild(doc.createTextNode(record.rights));
			dc.appendChild(curr);
		}

		if (record.publisher != null) {
			curr = doc.createElement("dc:publisher");
			curr.appendChild(doc.createTextNode(record.publisher));
			dc.appendChild(curr);
		}

		if (record.contributor != null) {
			curr = doc.createElement("dc:contributor");
			curr.appendChild(doc.createTextNode(record.contributor));
			dc.appendChild(curr);
		}

		if (record.date != null) {
			curr = doc.createElement("dc:date");
			curr.appendChild(doc.createTextNode(record.date));
			dc.appendChild(curr);
		}

		if (record.type != null) {
			curr = doc.createElement("dc:type");
			curr.appendChild(doc.createTextNode(record.type));
			dc.appendChild(curr);
		}

		if (record.identifier != null) {
			for (Enumeration e = record.identifier.elements(); e
					.hasMoreElements();) {
				curr = doc.createElement("dc:identifier");
				curr.appendChild(doc.createTextNode((String) (e.nextElement())));
				dc.appendChild(curr);
			}
		}

		if (record.source != null) {
			curr = doc.createElement("dc:source");
			curr.appendChild(doc.createTextNode(record.source));
			dc.appendChild(curr);
		}

		if (record.language != null) {
			curr = doc.createElement("dc:language");
			curr.appendChild(doc.createTextNode(record.language));
			dc.appendChild(curr);
		}

		if (record.relation != null) {
			curr = doc.createElement("dc:relation");
			curr.appendChild(doc.createTextNode(record.relation));
			dc.appendChild(curr);
		}

	}

	/**
	 * 
	 * A partir de un objeto DMPRecord en el cual se encuentra información por
	 * registro relacionada con OAI y con un registro especifico REDA.
	 * 
	 * Es llamado internamente por el método "listRecords"
	 * 
	 * @param doc
	 *            usado para crear elementos
	 * @param root
	 *            nodo raiz ppal al que se le va adicionando uno a uno registros
	 *            oai
	 * @param record
	 *            Objeto DMP
	 * @param metaformat
	 *            formato de metadatos
	 */

	private void createRecordElementDMP(Document doc, Element root,
			DMPRecord record, String metaformat) {
		Element recordElmt, tmp, header, metadata;
		tmp = doc.createElement("record");
		root.appendChild(tmp);
		recordElmt = tmp;

		tmp = doc.createElement("header");
		header = tmp;
		recordElmt.appendChild(header);

		if (record.identifier_oai != null) {
			tmp = doc.createElement("identifier");
			tmp.appendChild(doc.createTextNode(record.identifier_oai));
			header.appendChild(tmp);
		}

		if (record.datestamp != null) {
			tmp = doc.createElement("datestamp");
			tmp.appendChild(doc.createTextNode(record.datestamp));
			header.appendChild(tmp);
		}

		if (record.setSpec != null) {
			tmp = doc.createElement("setSpec");
			tmp.appendChild(doc.createTextNode(record.setSpec));
			header.appendChild(tmp);
		}

		if (!metaformat.equals("oai_dmp"))
			return;

		metadata = doc.createElement("metadata");
		metadata.appendChild(doc.importNode(record.doer, true));
		recordElmt.appendChild(metadata);

	}

	private String checkValidaty(String day) throws OAIError {

		SimpleDateFormat formatter;

		// formatter= new SimpleDateFormat ("yyyy-MM-dd");

		formatter = new SimpleDateFormat("yyyy-MM-dd");
		java.util.Date date;

		formatter.setLenient(true);
		try {
			date = formatter.parse(day);
			// System.out.println("DATE:"+date);
		} catch (Exception e) {
			throw new BadArgument("date format error");
		}

		if (day.length() > 10)
			throw new BadArgument("bad Granularity for " + day);
		String time = "T00:00:00Z"; // se agrega el time para que se haga bien
									// la comparacion el en query de xpath

		return formatter.format(date) + time;

	}

	private void addDCNameSpace(Element e) {

		e.setAttribute("xmlns:dc", "http://purl.org/dc/elements/1.1/");
		e.setAttribute("xmlns:oai_dc",
				"http://www.openarchives.org/OAI/2.0/oai_dc/");
		e.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
		e.setAttribute(
				"xsi:schemaLocation",
				"http://www.openarchives.org/OAI/2.0/oai_dc/ http://www.openarchives.org/OAI/2.0/oai_dc.xsd");
	}

	private void Log(String s) {
		if (LOG)
			System.out.println("DataProvider:" + s);
	}

}