package org.bdng.oai.dataprovider.token;

import org.bdng.oai.dataprovider.token.Token;

public class SetToken extends Token {
	/**
	 * @return token type
	 */
	public int getType() {
		return Token.SET_TOKEN;
	}

}
