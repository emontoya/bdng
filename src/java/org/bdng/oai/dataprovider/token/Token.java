package org.bdng.oai.dataprovider.token;

import java.util.ResourceBundle;

public abstract class Token {
	public static int TOKENSIZE = Integer.parseInt(ResourceBundle.getBundle(
			"oai").getString("RESUMPTION_TOKEN"));

	public static final int SET_TOKEN = 1;
	public static final int IDENTIFIER_TOKEN = 2;
	public static final int RECORDS_TOKEN = 3;

	private int cursor;

	public void setCursor(int cursor) {
		this.cursor = cursor;
	}

	public int getCursor() {
		return cursor;
	}

	public abstract int getType();

}
