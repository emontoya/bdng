package org.bdng.oai.util;

import java.util.Iterator;

import org.bdng.oai.dataprovider.DCRecord;

public class StringUtils {

	private static boolean LOG = true;

	public static String replaceWord(String original, String find,
			String replacement) {
		if (replacement == null)
			replacement = "";

		int i = original.indexOf(find);
		if (i < 0) {
			return original; // return original if 'find' is not in it.
		}

		String partBefore = original.substring(0, i);
		String partAfter = original.substring(i + find.length());

		StringBuffer wholeString = new StringBuffer(partBefore.length()
				+ replacement.length() + partAfter.length());

		return (wholeString.append(partBefore).append(replacement)
				.append(partAfter)).toString();
	}

	public static void printDCRecord(DCRecord dcRecord) {
		Log("\n");
		Log("DCRecord************************************:");
		Log("contributor: " + dcRecord.contributor);
		Log("coverage: " + dcRecord.coverage);
		Log("date: " + dcRecord.date);
		Log("datestamp: " + dcRecord.datestamp);
		Log("description: " + dcRecord.description);
		Log("format: " + dcRecord.format);
		Log("fullid: " + dcRecord.fullid);
		Log("language: " + dcRecord.language);
		Log("publisher: " + dcRecord.publisher);
		Log("relation: " + dcRecord.relation);
		Log("rights: " + dcRecord.rights);
		Log("sets: " + dcRecord.sets);
		Log("source: " + dcRecord.source);
		Log("status: " + dcRecord.status);
		Log("title: " + dcRecord.title);
		Log("type: " + dcRecord.type);
		Log("creator: ");
		for (Iterator iter = dcRecord.creator.iterator(); iter.hasNext();) {
			String creator = (String) iter.next();
			Log(creator);
		}
		Log("identifier: ");
		for (Iterator iter = dcRecord.identifier.iterator(); iter.hasNext();) {
			String identifier = (String) iter.next();
			Log(identifier);
		}
		Log("subject: ");
		for (Iterator iter = dcRecord.subject.iterator(); iter.hasNext();) {
			String subject = (String) iter.next();
			Log(subject);
		}

		Log("********************************************\n");

	}

	public static String replaceXUpdateDC(String stmnt, DCRecord record) {

		StringBuffer xupdateStatement = new StringBuffer();

		if (record.contributor != null)
			xupdateStatement.append("<dc:contributor>")
					.append(record.contributor).append("</dc:contributor>");
		if (record.coverage != null)
			xupdateStatement.append("<dc:coverage>").append(record.coverage)
					.append("</dc:coverage>");
		if (record.date != null)
			xupdateStatement.append("<dc:date>").append(record.date)
					.append("</dc:date>");
		if (record.datestamp != null)
			xupdateStatement.append("<dc:datestamp>").append(record.datestamp)
					.append("</dc:datestamp>");
		if (record.description != null)
			xupdateStatement.append("<dc:description>")
					.append(record.description).append("</dc:description>");
		if (record.format != null)
			xupdateStatement.append("<dc:format>").append(record.format)
					.append("</dc:format>");
		if (record.fullid != null)
			xupdateStatement.append("<bde:id>").append(record.fullid)
					.append("</bde:id>");
		if (record.language != null)
			xupdateStatement.append("<dc:language>").append(record.language)
					.append("</dc:language>");
		if (record.publisher != null)
			xupdateStatement.append("<dc:publisher>").append(record.publisher)
					.append("</dc:publisher>");
		if (record.relation != null)
			xupdateStatement.append("<dc:relation>").append(record.relation)
					.append("</dc:relation>");
		if (record.rights != null)
			xupdateStatement.append("<dc:rights>").append(record.rights)
					.append("</dc:rights>");
		if (record.sets != null)
			xupdateStatement.append("<dc:sets>").append(record.sets)
					.append("</dc:sets>");
		if (record.source != null)
			xupdateStatement.append("<dc:source>").append(record.source)
					.append("</dc:source>");
		if (record.status != null)
			xupdateStatement.append("<dc:status>").append(record.status)
					.append("</dc:status>");
		if (record.title != null)
			xupdateStatement.append("<dc:title>").append(record.title)
					.append("</dc:title>");
		if (record.type != null)
			xupdateStatement.append("<dc:type>").append(record.type)
					.append("</dc:type>");
		if (record.language != null)
			xupdateStatement.append("<dc:language>").append(record.language)
					.append("</dc:language>");

		StringBuffer repCreator = new StringBuffer();
		for (int i = 0; i < record.creator.size(); i++) {
			repCreator.append("<dc:creator>")
					.append(record.creator.elementAt(i))
					.append("</dc:creator>");
		}
		xupdateStatement.append(repCreator);

		StringBuffer repIdentifier = new StringBuffer();
		for (int i = 0; i < record.identifier.size(); i++) {
			repIdentifier.append("<dc:identifier>")
					.append(record.identifier.elementAt(i))
					.append("</dc:identifier>");
		}
		xupdateStatement.append(repIdentifier);

		StringBuffer repSubejct = new StringBuffer();
		for (int i = 0; i < record.subject.size(); i++) {
			repSubejct.append("<dc:subject>")
					.append(record.subject.elementAt(i))
					.append("</dc:subject>");
		}
		xupdateStatement.append(repSubejct);

		stmnt = replaceWord(stmnt, "*?XUPDATE_STATEMENT?*",
				xupdateStatement.toString());

		return stmnt;
	}

	public static String replaceXUpdateRemoveDC(String stmnt, String recordId) {

		stmnt = replaceWord(stmnt, "*?DC:FULLID?*", recordId);
		return stmnt;
	}

	private static void Log(String s) {
		if (LOG && s != null)
			System.out.println("SringUtils" + ": " + s);
	}

}
