package org.bdng.webold;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.bdng.harvmgr.validator.ValidadorReda;
import org.bdng.harvmgr.validator.Validator;

/**
 * Servlet implementation class ValidatorRedaWeb
 */
public class ValidatorRedaWeb extends HttpServlet {
	ValidadorReda validar = null;
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ValidatorRedaWeb() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		this.ejecutar(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		this.ejecutar(request, response);
	}

	private void ejecutar(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		response.setContentType("text/xml");
		response.setHeader("Cache-Control", "no-cache");
		response.setHeader("Pragma", "no-cache");
		response.setDateHeader("Expires", 0);

		String dl_home = this.getServletContext().getRealPath("/");

		String valid_option = request.getParameter("option");
		if (valid_option.equalsIgnoreCase("validate")) {

			String stShortname = request.getParameter("shortname");
			String stUrlBase = request.getParameter("urlbase");
			String stProtocol = request.getParameter("protocol");
			String stMetadataPrefix = request.getParameter("metadataPrefix");
			/*
			 * System.out.println(stShortname); System.out.println(stUrlBase);
			 * System.out.println(stProtocol);
			 * System.out.println(stMetadataPrefix);
			 */
			// Validator validator = new Validator(dl_home);

			String myDl_home = System.getProperty("user.dir").replace("eXist",
					"");

			String defaultxsd = myDl_home + "/conf/xsd/bdng.xsd";
			String defaultlogs = myDl_home + "/logs/";

			// System.out.println(dl_home);

			// System.out.println(myDl_home);

			validar = new ValidadorReda(myDl_home, defaultxsd,
					defaultlogs);

			System.out
					.println("******************Iniciando Validacion******************");

			// validator.validarRepositorio(stUrlBase, stShortname, stProtocol,
			// stMetadataPrefix);
			validar.validarRepo(stShortname);

			System.out
					.println("******************Validador ha finalizado******************");

			response.sendRedirect("/"
					+ this.getServletContext().getServletContextName()
					+ "/ListValidationsReda.jsp");
			// response.sendRedirect("/"+this.getServletContext().getServletContextName()+"/validator.jsp?shortname="+stShortname);
		} else if (valid_option.equalsIgnoreCase("validateAll")) {
			String myDl_home = System.getProperty("user.dir").replace("eXist",
					"");

			String defaultxsd = myDl_home + "/conf/xsd/bdng.xsd";
			String defaultlogs = myDl_home + "/logs/";

			ValidadorReda validar = new ValidadorReda(myDl_home, defaultxsd,
					defaultlogs);

			System.out
					.println("******************Iniciando Validacion******************");

			// validator.validarRepositorio(stUrlBase, stShortname, stProtocol,
			// stMetadataPrefix);
			validar.validarTodos();

			System.out
					.println("******************Validador ha finalizado******************");

			response.sendRedirect("/"
					+ this.getServletContext().getServletContextName()
					+ "/ListValidationsReda.jsp");
		}
		// }else if(valid_option.equalsIgnoreCase("updateTextPatterns")){
		//
		// // Validator validator = new Validator(dl_home);
		//
		// String defaultxsd = dl_home+"/conf/xsd/bdng.xsd";
		// String defaultlogs = dl_home+"/logs/";
		//
		// ValidadorReda validar = new ValidadorReda(dl_home, defaultxsd,
		// defaultlogs);
		//
		//
		// String stShortname = request.getParameter("shortname");
		//
		// Enumeration e = request.getParameterNames();
		//
		// while (e.hasMoreElements()) {
		//
		// String select = (String)e.nextElement();
		//
		//
		// if(!select.equalsIgnoreCase("Grabar")&&!select.equalsIgnoreCase("option")&&!select.equalsIgnoreCase("shortname")){
		// //parametros que vienen y tenemos que filtrar
		// String newText = request.getParameter(select);
		//
		// select = select.replace("¿?", " "); //este par de caracteres se le
		// pusieron para que no hubiera espeacios en el name del select, aqui se
		// los quitamos
		//
		// //System.out.println(select + " --> "+newText);
		// try {
		// String [] partes = select.split("___");
		//
		// String tag = partes [0];
		// String oldText = partes [1];
		//
		//
		// System.out.println(stShortname+" "+ tag+" "+ oldText+ " "+newText);
		// validator.modificarTextPatterns(stShortname, tag, oldText, newText);
		//
		// } catch (Exception e1) {
		//
		// System.out.println("Error intentando modificar el archivo textPatterns.xml");
		// }
		//
		// }
		//
		//
		// }
		//
		//
		// response.sendRedirect("/"+this.getServletContext().getServletContextName()+"/validator.jsp?shortname="+stShortname);
		//
		// }
	}

}
