package org.bdng.webold;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.bdng.webold.ManejoXML;

public class AlmacenarDatos extends HttpServlet {

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.ejecutar(request, response);

	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.ejecutar(request, response);

	}

	/**
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	private void ejecutar(HttpServletRequest request,
			HttpServletResponse response) throws IOException {

		String stRepName = request.getParameter("repname");
		String stUrlBase = request.getParameter("urlbase");
		String stShortName = request.getParameter("shortname");
		String stDescripcion = request.getParameter("descripcion");
		String stMetadataPrefix = request.getParameter("metadataprefix");
		String stPrefijo = request.getParameter("prefijo");
		String stCollection = request.getParameter("collection");
		String stUrlhome = request.getParameter("urlhome");
		String stProtocol = request.getParameter("protocol");
		String stEmailadmin = request.getParameter("emailadmin");
		String stReptype = request.getParameter("reptype");
		boolean validarDatos = false;

		String stRuta = this.getServletContext().getRealPath("/")
				+ "/conf/oai_providers.xml";

		if ((stRepName == null) || (stRepName.trim().length() == 0)) {
			stRepName = "";
			validarDatos = true;
		}
		if ((stUrlBase == null) || (stUrlBase.trim().length() == 0)) {
			stUrlBase = "";
			validarDatos = true;
		}
		if ((stShortName == null) || (stShortName.trim().length() == 0)) {
			stShortName = "";
			validarDatos = true;
		}
		if (stDescripcion == null) {
			stDescripcion = "";
		}

		if ((stMetadataPrefix == null)
				|| (stMetadataPrefix.trim().length() == 0)) {
			stMetadataPrefix = "";
			validarDatos = true;
		}

		if ((stPrefijo == null) || (stPrefijo.trim().length() == 0)) {
			stPrefijo = "false";
		}

		if ((stCollection == null) || (stCollection.trim().length() == 0)) {
			stCollection = "";
			validarDatos = true;
		}

		if ((stProtocol == null) || (stProtocol.trim().length() == 0)) {
			stCollection = "";
			// validarDatos = true;
		}

		if (stUrlhome == null) {
			stUrlhome = "";
		}

		if (stReptype == null) {
			stReptype = "";
		}

		if (stEmailadmin == null) {
			stEmailadmin = "";
		}

		String stSalida = "";
		stSalida = "<dl><msg><valor>no</valor><mensaje>Faltan datos por llenar</mensaje></msg></dl>";

		PrintWriter out = response.getWriter();
		try {
			if (validarDatos) {
				out.println(stSalida);
				out.close();
				return;
			}

			ManejoXML manejoXML = new ManejoXML();
			manejoXML.almacenarDatos(stRuta, stRepName, stUrlBase, stShortName,
					stDescripcion, stMetadataPrefix, stPrefijo, stCollection,
					stUrlhome, stProtocol, stEmailadmin, stReptype);

			System.out.println("\nNombreRepositorio: " + stRepName
					+ " \nNombre Corto: " + stShortName + " Url: " + stUrlBase
					+ " \nMetadata prefix: " + stMetadataPrefix
					+ " \nprefijo: " + stPrefijo + " Descripcion: "
					+ stDescripcion + "\nProtocol:" + stProtocol + "\nReptype:"
					+ stReptype + "\nurlhome:" + stUrlhome + "\nemailAdmin:"
					+ stEmailadmin);

			// System.out.println(" Query String " + request.getQueryString());
			// stSalida = "<guardarsuccess>yes</guardarsuccess>";

			stSalida = "<dl><msg><valor>yes</valor>"
					+ "<mensaje>Datos Almacenados Correctamente</mensaje>"
					+ "</msg></dl>";

		} catch (Exception e) {
			stSalida = "<dl><msg><valor>no</valor><mensaje>" + e.getMessage()
					+ "</mensaje></msg></dl>";
		}
		System.out.println("AlmacenarDatos.ejecutar(stSalida) " + stSalida);
		out.println(stSalida);
		out.close();
	}
}