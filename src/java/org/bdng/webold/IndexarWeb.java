package org.bdng.webold;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.bdng.harvmgr.harvester.DataProvider;
import org.bdng.harvmgr.indexer.Indexer;

public class IndexarWeb extends HttpServlet {

	Indexer indexer = null;

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.ejecutar(request, response);

	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.ejecutar(request, response);

	}

	/**
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	private void ejecutar(HttpServletRequest request,
			HttpServletResponse response) throws IOException {

		response.setContentType("text/xml");
		response.setHeader("Cache-Control", "no-cache");
		response.setHeader("Pragma", "no-cache");
		response.setDateHeader("Expires", 0);
		String stSalida = "";
		PrintWriter out = response.getWriter();
		// String stRuta =
		// this.getServletContext().getRealPath("/")+"/conf/oai_providers.xml";
		String index_option = request.getParameter("index_opcion");

		String stRuta = this.getServletContext().getRealPath("/");
		String msg = "";
		// System.out.println(sched_option);
		// List<String> listaProviders = (List)
		// request.getAttribute("listProviders");

		if (index_option.equals("listar_prov")) {

			try {

				DataProvider dp = new DataProvider(this.getServletContext()
						.getRealPath("/"));
				out.println(dp.getXmlProvider());
				out.flush();
				out.close();

			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			if (index_option.equals("indexar_seleccionados")) {

				DataProvider dp = null;

				// String provs = request.getParameter("listProviders");
				String providers[] = request
						.getParameterValues("listProviders");
				dp = new DataProvider(this.getServletContext().getRealPath("/"));

				/*
				 * if (provs != null && !provs.equals("")) { dp = new
				 * DataProvider(this.getServletContext().getRealPath("/"));
				 * 
				 * String[] providers = provs.split(",");
				 */
				try {
					if (indexer == null) {
						indexer = new Indexer(stRuta, providers);
						indexer.start();
					}

					if (!indexer.isWorking()) {
						msg = "Iniciando Indexacion...";
						indexer = new Indexer(stRuta, providers);
						indexer.start();
					} else
						msg = "Indexaci�n en progreso...";

					// indexer.upload2exist();
					stSalida = "<dl><msg><valor>yes</valor>" + "<mensaje>"
							+ msg + "</mensaje>" + "</msg></dl>";

				} catch (Exception e) {
					e.printStackTrace();
					stSalida = "<dl><msg><valor>no</valor><mensaje>"
							+ e.getMessage() + "</mensaje></msg></dl>";
				}
				response.sendRedirect("/"
						+ this.getServletContext().getServletContextName()
						+ "/Indexador.jsp");

				/*
				 * out.println(dp.getXmlProvider()); out.flush(); out.close();
				 */

			} else {
				if (index_option.equals("indexar_todos")) {
					DataProvider dp = null;
					dp = new DataProvider(this.getServletContext().getRealPath(
							"/"));

					try {

						if (indexer == null) {
							indexer = new Indexer(stRuta);
							indexer.start();
						}

						if (!indexer.isWorking()) {
							msg = "Iniciando Indexacion...";
							indexer = new Indexer(stRuta);
							indexer.start();
						} else
							msg = "Indexaci�n en progreso...";

						// indexer.upload2exist();
						stSalida = "<dl><msg><valor>yes</valor>" + "<mensaje>"
								+ msg + "</mensaje>" + "</msg></dl>";

					} catch (Exception e) {
						e.printStackTrace();
						stSalida = "<dl><msg><valor>no</valor><mensaje>"
								+ e.getMessage() + "</mensaje></msg></dl>";
					}
					/*
					 * out.println(dp.getXmlProvider()); out.flush();
					 * out.close();
					 */
					response.sendRedirect("/"
							+ this.getServletContext().getServletContextName()
							+ "/Indexador.jsp");
				}
			}
		}
	}
}
