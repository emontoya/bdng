package org.bdng.webold;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.bdng.harvmgr.dlConfig;
import org.bdng.harvmgr.harvester.ServerConfXml;
import org.bdng.harvmgr.indexer.Indexer;

public class ServerConf extends HttpServlet {

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.ejecutar(request, response);

	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.ejecutar(request, response);

	}

	/**
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	private void ejecutar(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		// System.out.print("entro********************************************");
		HttpSession sesion = request.getSession();
		response.setContentType("text/xml");
		response.setHeader("Cache-Control", "no-cache");
		response.setHeader("Pragma", "no-cache");
		response.setDateHeader("Expires", 0);

		String stSysName = request.getParameter("sysName");
		String stSysNameFull = request.getParameter("sysNameFull");
		String stDl_home = request.getParameter("dl_home");
		String stDl_host = request.getParameter("dl_host");
		String stDl_port = request.getParameter("dl_port");
		String stDl_metadata = request.getParameter("dl_metadata");
		String stDl_data = request.getParameter("dl_data");
		String stDl_name = request.getParameter("dl_name");
		String stDl_col = request.getParameter("dl_col");
		String stAdmin_email = request.getParameter("admin_email");
		String stExist_user = request.getParameter("exist_user");
		String stExist_pass = request.getParameter("exist_pass");
		String stRecord_xml = request.getParameter("record_xml");
		String stProxy_host = request.getParameter("proxy_host");
		String stProxy_port = request.getParameter("proxy_port");
		String stSMTP_host = request.getParameter("smtp_host");
		String stHarvest_count_incremental = request
				.getParameter("harvest_count_incremental");
		String stHarvest_frecuency = request.getParameter("harvest_frecuency");
		String stProxy = request.getParameter("proxy");
		String number_threads = request.getParameter("number_threads");

		// **lineas agregadas

		if (stProxy == null) {
			stProxy = "false";
		}

		// /fin
		String stRuta = this.getServletContext().getRealPath("/");

		System.out.println("path abs =" + stRuta);

		ServerConfXml scxml = new ServerConfXml(stRuta);

		scxml.setOption("sysName", stSysName);
		scxml.setOption("sysNameFull", stSysNameFull);
		scxml.setOption("dl_home", stDl_home);
		scxml.setOption("dl_host", stDl_host);
		scxml.setOption("dl_port", stDl_port);
		scxml.setOption("dl_metadata", stDl_metadata);
		scxml.setOption("dl_data", stDl_data);
		scxml.setOption("dl_name", stDl_name);
		scxml.setOption("dl_col", stDl_col);
		scxml.setOption("record_xml", stRecord_xml);
		scxml.setOption("admin_email", stAdmin_email);
		scxml.setOption("exist_user", stExist_user);
		scxml.setOption("exist_pass", stExist_pass);
		scxml.setOption("proxy_host", stProxy_host);
		scxml.setOption("proxy_port", stProxy_port);
		scxml.setOption("smtp_host", stSMTP_host);
		scxml.setOption("harvest_count_incremental",
				stHarvest_count_incremental);
		scxml.setOption("harvest_frecuency", stHarvest_frecuency);
		scxml.setOption("proxy", stProxy);
		scxml.setOption("number_threads", number_threads);

		// upload Serverconfig.xml to /db/conf in eXist -luis
		Indexer indexer = new Indexer(this.getServletContext().getRealPath("/"));
		dlConfig dlconfig = new dlConfig(stDl_home);
		indexer.uploadConf(dlconfig.getfileServerConfig());

		response.sendRedirect("/"
				+ this.getServletContext().getServletContextName()
				+ "/ServerConf.jsp");
	}
}
