package org.bdng.webold;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.bdng.harvmgr.harvester.DataProvider;
import org.bdng.harvmgr.harvester.Harvester;
import org.bdng.harvmgr.harvester.HarvesterThread;

public class HarversterWeb extends HttpServlet {

	int HARV_NOT_WORKING = 0;
	int HARV_TODOS = 1;
	int HARV_REPOSEL = 2;
	int HARV_STOP = 4;

	int harv_state = HARV_NOT_WORKING;

	Harvester harvester = null;

	String stRuta = null;

	String msg = "";

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.ejecutar(request, response);

	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.ejecutar(request, response);
	}

	private void createHarvester() {
		String stRuta = this.getServletContext().getRealPath("/");
		if (harvester == null) {

			String all[] = { "all" };
			harvester = new Harvester(stRuta, all);
			harvester.setWorking(true);
			harvester.start();

		}
	}

	private void startHarvester(String repository[]) {
		System.out.println("iniciando harverster  para " + repository.length
				+ " repositorios");
		String stRuta = this.getServletContext().getRealPath("/");

		if (harvester == null) {
			harvester = new Harvester(stRuta, repository);
			harvester.setWorking(true);
			harvester.start();
		} else if (harvester != null && harvester.isWorking() == false) {
			harvester = null;
			harvester = new Harvester(stRuta, repository);
			harvester.setWorking(true);
			harvester.start();
		} else
			System.out.println("******* Recoleccion en curso **********");

	}

	private void startHarvesterAll() {

		System.out.println("iniciando harverster");
		createHarvester();
		// System.out.println("harvester.isWorking()="+harvester.isWorking());
		if (harvester != null && harvester.isWorking() == false) {
			// harvester.setWorking(true);
			// System.out.println("start-thread.isAlive()="+harvester.isAlive());
			// harvester.interrupt();
			harvester = null;
			createHarvester();

			// harvester.start();
		} else {
			System.out.println("******* Recoleccion en curso **********");
			// System.out.println("harvester.isWorking()="+harvester.isWorking());
		}
	}

	private void stopHarvesterAll() {
		// System.out.println("PARANDO RECOLECCION");
		msg = "Deteniendo...";
		if (harvester != null && harvester.isWorking() == true) {
			harvester.noContinuar();
			harvester.setWorking(false);

			/*
			 * try { System.out.println("ESPERANDO TERMINAR"); wait();
			 * System.out.println("YA TERMINO"); } catch (InterruptedException
			 * e) { // TODO Auto-generated catch block e.printStackTrace(); }
			 */

			// System.out.println("TTTTTERMINO"+harvester.isAlive());
		}
	}

	boolean h = false;

	private void ejecutar(HttpServletRequest request,
			HttpServletResponse response) throws IOException {

		PrintWriter out = response.getWriter();
		response.setContentType("text/xml");
		response.setHeader("Cache-Control", "no-cache");
		response.setHeader("Pragma", "no-cache");
		response.setDateHeader("Expires", 0);

		String stSalida = "";

		String harv_opcion = request.getParameter("harv_opcion");

		if (harv_opcion.equals("reset_providers")) {
			try {
				DataProvider dp = new DataProvider(this.getServletContext()
						.getRealPath("/"));
				out.println(dp.getXmlProviderStatus());
				out.flush();
				out.close();

			} catch (Exception e) {
				e.printStackTrace();
			}

		} else if (harv_opcion.equals("resetall_providers")) {

			try {
				DataProvider dp = new DataProvider(this.getServletContext()
						.getRealPath("/"));
				// dp.setRuta(this.getServletContext().getRealPath("/"));
				dp.resetAllProviders();
				response.sendRedirect("/"
						+ this.getServletContext().getServletContextName()
						+ "/recolector.jsp");
				/*
				 * out.println(dp.getXmlProviderStatus()); out.flush();
				 * out.close();
				 */
			} catch (Exception e) {
				/*
				 * stSalida = "<dl><msg><valor>no</valor><mensaje>" +
				 * e.getMessage() + "</mensaje></msg></dl>";
				 * out.println(stSalida); out.close();
				 */

				e.printStackTrace();
			}
		} else if (harv_opcion.equals("reset_seleccionados")) {

			// DataProvider dp = null;
			DataProvider dp = new DataProvider(this.getServletContext()
					.getRealPath("/"));

			// String provs = request.getParameter("listProviders");
			String[] providers = request.getParameterValues("listProviders");
			// System.out.println("cantidad de providers checkeados: "+providers.length);

			for (int i = 0; i < providers.length; i++) {
				try {
					// System.out.println("Va a resetear "+providers[i]);
					dp.resetProviderStatus(providers[i]);
					System.out
							.println("Proveedor a reseteado: " + providers[i]);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			response.sendRedirect("/"
					+ this.getServletContext().getServletContextName()
					+ "/recolector.jsp");

			/**
			 * if (provs != null && !provs.equals("")) { dp = new
			 * DataProvider(this.getServletContext().getRealPath("/"));
			 * 
			 * String[] providers = provs.split(",");
			 * 
			 * for (int i=0; i<providers.length; i++) { try {
			 * dp.resetProviderStatus(providers[i]);
			 * System.out.println("Proveedor a resetear="+providers[i]); } catch
			 * (Exception e) { } } out.println(dp.getXmlProviderStatus());
			 * out.flush(); out.close(); }
			 */
		} else if (harv_opcion.equals("parar_todos")) {
			// stopHarvesterAll(); //comentariada temporalmente

			if (harvester != null && harvester.isWorking() == true) {
				// aqui agregamos un mensaje y lo devolvemos por ajax,
				// "recoleccion en curso", lo mismo en los else de
				// starthaverster/all

			}

			// out.println("<dl><msg><valor>yes</valor><mensaje>"+msg+"</mensaje></msg></dl>");
			// out.close();
			response.sendRedirect("/"
					+ this.getServletContext().getServletContextName()
					+ "/recolector.jsp");

		} else if (harv_opcion.equals("iniciar_todos")) {
			startHarvesterAll();
			// out.println("<dl><msg><valor>yes</valor><mensaje>"+msg+"</mensaje></msg></dl>");
			// out.close();
			response.sendRedirect("/"
					+ this.getServletContext().getServletContextName()
					+ "/recolector.jsp");

			// response.sendRedirect("/"+this.getServletContext().getServletContextName()+"/recolector.jsp");

		} else if (harv_opcion.equals("init_seleccionados")) {
			String[] providers = request.getParameterValues("listProviders");
			// System.out.println("cantidad de providers checkeados: "+providers.length);

			try {
				startHarvester(providers);

			} catch (Exception e) {
				e.printStackTrace();
			}

			response.sendRedirect("/"
					+ this.getServletContext().getServletContextName()
					+ "/recolector.jsp");

		} else if (harv_opcion.equals("check_progress")) {

			int dataProvidersCompletos = HarvesterThread.dataProvidersCompletos;
			int dataProvidersSize = HarvesterThread.dataProvidersSize;

			String progress = "";
			progress += "<div id=\"data\">";
			progress += "<div id=\"dataProvidersCompletos\">";
			progress += dataProvidersCompletos;
			progress += "</div>";
			progress += "<div id=\"dataProvidersSize\">";
			progress += dataProvidersSize;
			progress += "</div>";

			progress += "</div>";

			// System.out.println("dataProvidersCompletos: "+dataProvidersCompletos+" de "+dataProvidersSize);

			out.println(progress);
			out.close();
			// response.sendRedirect("/"+this.getServletContext().getServletContextName()+"/recolector.jsp");

		}

	}
}
