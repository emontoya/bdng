package org.bdng.webold;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.bdng.harvmgr.indexer.Indexer;

public class IndexarTodos extends HttpServlet {

	Indexer indexer = null;

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.ejecutar(request, response);

	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.ejecutar(request, response);

	}

	/**
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	private void ejecutar(HttpServletRequest request,
			HttpServletResponse response) throws IOException {

		PrintWriter out = response.getWriter();
		response.setContentType("text/xml");
		response.setHeader("Cache-Control", "no-cache");
		response.setHeader("Pragma", "no-cache");
		response.setDateHeader("Expires", 0);
		String stSalida = "";
		String msg = "";
		try {
			String stRuta = this.getServletContext().getRealPath("/");
			if (indexer == null) {
				indexer = new Indexer(stRuta);
				indexer.start();
			}

			if (!indexer.isWorking()) {
				msg = "Iniciando Indexacion...";
				indexer = new Indexer(stRuta);
				indexer.start();
			} else
				msg = "Indexacion en progreso...";

			// indexer.upload2exist();
			stSalida = "<dl><msg><valor>yes</valor>" + "<mensaje>" + msg
					+ "</mensaje>" + "</msg></dl>";

		} catch (Exception e) {
			e.printStackTrace();
			stSalida = "<dl><msg><valor>no</valor><mensaje>" + e.getMessage()
					+ "</mensaje></msg></dl>";
		}
		System.out.println("AlmacenarDatos.ejecutar(stSalida) " + stSalida);
		out.println(stSalida);
		out.close();
	}

}
