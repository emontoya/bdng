package org.bdng.webold;

import java.io.IOException;

import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.bdng.harvmgr.harvester.DataProvider;
import org.bdng.harvmgr.harvester.Harvester;

public class RecolectarTodos extends HttpServlet {

	int HARV_NOT_WORKING = 0;
	int HARV_TODOS = 1;
	int HARV_REPOSEL = 2;
	int HARV_STOP = 4;

	int harv_state = HARV_NOT_WORKING;

	Harvester harvester = null;

	String stRuta = null;

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.ejecutar(request, response);

	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.ejecutar(request, response);

	}

	private void createHarvester() {
		String stRuta = this.getServletContext().getRealPath("/");
		if (harvester == null) {
			// System.out.println("Primera vez creando el objeto Harvester()="+stRuta);
			String all[] = { "all" };
			harvester = new Harvester(stRuta, all);
			// System.out.println("1harvester.isWorking()="+harvester.isWorking());
			// harvester.setWorking(true);
			// harvester.start();
			// System.out.println("2harvester.isWorking()="+harvester.isWorking());
		}
	}

	private void startHarvesterAll() {
		createHarvester();
		System.out.println("INICIANDO RECOLECCION");
		if (harvester != null && harvester.isWorking() == false) {
			harvester.setWorking(true);
			System.out.println("start-thread.isAlive()=" + harvester.isAlive());
			// harvester.interrupt();
			harvester = null;
			createHarvester();
			harvester.start();
		} else
			System.out.println("por el ELSE de startHarvester()");
	}

	private void stopHarvesterAll() {
		System.out.println("PARANDO RECOLECCION");
		if (harvester != null && harvester.isWorking() == true) {
			harvester.noContinuar();
			harvester.setWorking(false);
			/*
			 * try { System.out.println("ESPERANDO TERMINAR"); wait();
			 * System.out.println("YA TERMINO"); } catch (InterruptedException
			 * e) { // TODO Auto-generated catch block e.printStackTrace(); }
			 */

			System.out.println("TTTTTERMINO" + harvester.isAlive());
		}
	}

	boolean h = false;

	private void ejecutar(HttpServletRequest request,
			HttpServletResponse response) throws IOException {

		PrintWriter out = response.getWriter();
		response.setContentType("text/xml");
		response.setHeader("Cache-Control", "no-cache");
		response.setHeader("Pragma", "no-cache");
		response.setDateHeader("Expires", 0);

		String stSalida = "";

		String harv_opcion = request.getParameter("harv_opcion");

		try {

			String msg = null;

			if (harv_opcion.equals("parar_todos")) {
				msg = "PARANDO...";
				stopHarvesterAll();
			} else if (harv_opcion.equals("iniciar_todos")) {
				msg = "RECOLECTANDO...";
				startHarvesterAll();
			}

			System.out
					.println("harvester.isWorking()=" + harvester.isWorking());

			// if (harvester.isWorking()==false) {
			// msg = "VA A RECOLECTAR";
			// harvester.setWorking(true);
			// System.out.println("--->NO ESTA RECOLECTANDO");
			// harvester = new Harvester(stRuta);
			// harvester.start();
			// } else {
			// msg = "RECOLECCION EN PROGRESO";
			// System.out.println("--->ESTA RECOLECTANDO");
			// }

			stSalida = "<dl><msg><valor>yes</valor>" + "<mensaje>" + msg
					+ "</mensaje>" + "</msg></dl>";

		} catch (Exception e) {
			e.printStackTrace();
			stSalida = "<dl><msg><valor>no</valor><mensaje>" + e.getMessage()
					+ "</mensaje></msg></dl>";
		}
		// System.out.println("AlmacenarDatos.ejecutar(stSalida) " + stSalida);
		out.println(stSalida);
		out.close();
	}
}
