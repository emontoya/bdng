package org.bdng.webbeta;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.bdng.harvmgr.dlConfig;
import org.bdng.harvmgr.harvester.DataProvider;
import org.bdng.harvmgr.indexer.Indexer;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;

/**
 * Servlet implementation class FileUploadHandler
 */
public class FileUploadHandler extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private final String myDl_home = System.getProperty("user.dir").replace("eXist","");
	private final String UPLOAD_DIRECTORY = myDl_home + "metadata/";
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FileUploadHandler() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		System.out.println("Funciona");
//		System.out.println(UPLOAD_DIRECTORY);
		HttpSession sesion = request.getSession();
		if(ServletFileUpload.isMultipartContent(request)){
			try {
				boolean archivoCorrecto = true;
				String name = "";
				List<FileItem> multiparts = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
				for(FileItem item : multiparts){
					if(!item.isFormField()){
						name = new File(item.getName()).getName();
						if((getExtension(name)).equals("xml")){
							System.out.println(getExtension(name));
							item.write(new File(UPLOAD_DIRECTORY + File.separator + "temp-" + name));
						}else{
							archivoCorrecto = false;
							System.out.println("El archivo a importar debe tener un formato XML");
						}
					}
				}
				if(archivoCorrecto){
					//File uploaded successfully
					SAXBuilder builder = new SAXBuilder();
					Document doc = builder.build(UPLOAD_DIRECTORY + File.separator + "temp-" + name);
					Element raiz = doc.getRootElement();
					if((raiz.getName()).equals("oai_provider")){
						Element e = raiz.getChild("shortName");
						String stShortName = e.getText();
						
						e = raiz.getChild("repositoryName");
						String stRepName = e.getText();

						e = raiz.getChild("description");
						String stDescripcion = e.getText();

						e = raiz.getChild("urlhome");
						String stUrlhome = e.getText();

						e = raiz.getChild("protocol");
						String stProtocol = e.getText();

						e = raiz.getChild("baseURL");
						String stUrlBase = e.getText();

						e = raiz.getChild("nameAdmin");
						String stNameAdmin = e.getText();

						e = raiz.getChild("emailadmin");
						String stEmailadmin = e.getText();

						e = raiz.getChild("phoneAdmin");
						String stPhoneAdmin = e.getText();

						e = raiz.getChild("reptype");
						String stReptype = e.getText();

						e = raiz.getChild("collection");
						String stCollection = e.getText();
						
						e = raiz.getChild("metadataPrefix");
						String stMetadataPrefix = e.getText();
						
						e = raiz.getChild("institution");
						String stInstitution = e.getText();
						
						e = raiz.getChild("shortInst");
						String stShortInst = e.getText();
						
						e = raiz.getChild("staticCollection");
						String stStaticCollection = e.getText();
						
						e = raiz.getChild("departamento");
						String stDepartamento = e.getText();
						
						e = raiz.getChild("municipio");
						String stMunicipio = e.getText();
						
						e = raiz.getChild("bdcol");
						String stBdcol = e.getText();
						
						e = raiz.getChild("owner");
						String stOwner = e.getText();
						
						String stCdata = "2";
						
						String stPrefijo = "";
						
						System.out.println(this.getServletContext().getRealPath("/"));
						
						DataProvider dp = new DataProvider(this.getServletContext().getRealPath("/"));
						
						boolean result = dp.create(stOwner, stRepName, stUrlBase,
								stShortName, stDescripcion, stMetadataPrefix,
								stPrefijo, stCollection, stUrlhome, stProtocol,
								stEmailadmin, stNameAdmin, stPhoneAdmin, stReptype,
								stCdata, stInstitution, stShortInst,
								stStaticCollection, stDepartamento, stMunicipio,
								stBdcol);
						
						if(result){
							Indexer indexer = new Indexer(this.getServletContext().getRealPath("/"));
							dlConfig dlconfig = new dlConfig(this.getServletContext().getRealPath("/"));
							indexer.uploadConf(dlconfig.getOaiProviders());
							
							sesion.setAttribute("message", "El archivo se ha importado correctamente");
							sesion.setAttribute("pagina", "mensajeExito.jsp");
							System.out.println("El archivo se ha importado correctamente");
						}else{
							sesion.setAttribute("message", "El archivo no se ha podido importar");
							sesion.setAttribute("pagina", "mensajeError.jsp");
							System.out.println("El archivo no se ha podido importar");
						}
					}else{
						System.out.println(this.getServletContext().getRealPath("/"));
						sesion.setAttribute("message", "El archivo con el nuevo proveedor tiene errores en el formato, la raiz debe ser oai_provider");
						sesion.setAttribute("pagina", "mensajeError.jsp");
						System.out.println("El archivo con el nuevo proveedor tiene errores en el formato, la raiz debe ser oai_provider");
					}
					File fileTemporal = new File(UPLOAD_DIRECTORY + File.separator + "temp-" + name);
					if(fileTemporal.delete()){
		    			System.out.println(fileTemporal.getName() + " borrado");
		    		}else{
		    			System.out.println("No se pudo borrar el archivo temporal");
		    		}
				}else{
					sesion.setAttribute("message", "El archivo no se ha podido importar");
					sesion.setAttribute("pagina", "mensajeError.jsp");
					System.out.println("El archivo no se ha podido importar");
				}
			}catch(Exception ex){
				sesion.setAttribute("message", "Error al importar el archivo: " + ex);
				sesion.setAttribute("pagina", "mensajeError.jsp");
				System.out.println("Error al importar el archivo: " + ex);
			}
		}else{
			sesion.setAttribute("message","Este archivo no se puede importar");
			sesion.setAttribute("pagina", "mensajeError.jsp");
			System.out.println("Este archivo no se puede importar");
		}
		request.getRequestDispatcher("/index.jsp").forward(request, response);
	}
	
	public String getExtension(String f) {
		String ext = "";
		int dot = f.lastIndexOf(".");
		ext = f.substring(dot + 1);
		return ext;
	}

}
