package org.bdng.webbeta;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.bdng.harvmgr.dlConfig;
import org.bdng.web.UserEnt;

public class LoginUser extends HttpServlet {

	/**
	 * The doGet method of the servlet. <br>
	 * 
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.ejecutar(request, response);

	}

	/**
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	private void ejecutar(HttpServletRequest request,
			HttpServletResponse response) throws IOException {

		String stUser = request.getParameter("user");
		String stPwd = request.getParameter("pass");

		UserEnt userEnt = new UserEnt(this.getServletContext().getRealPath("/"));

		// System.out.println("BD"+
		// userEnt.getUsername()+" -"+userEnt.getPwd());
		String stSalida = "";
		stSalida += "<div id=\"data\">";
		stSalida += "<div id=\"loginsuccess\">";
		if ((userEnt.getUsername().equals(stUser))
				&& (userEnt.getPwd().equals(stPwd))) {
			stSalida += "yes";

			request.getSession().invalidate();// invalidamos alguna session que
												// exista

			request.getSession().setAttribute("userEnt", userEnt);
			request.getSession().setAttribute("type", userEnt.getType());
			request.getSession()
					.setAttribute("username", userEnt.getUsername());

		} else {
			stSalida += "no";
		}
		stSalida += "</div>";
		stSalida += "</div>";

		// response.sendRedirect("/"+this.getServletContext().getServletContextName()+"/index.jsp");

		PrintWriter out = response.getWriter();
		out.println(stSalida);
		out.close();

	}

	/**
	 * The doPost method of the servlet. <br>
	 * 
	 * This method is called when a form has its tag value method equals to
	 * post.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		this.ejecutar(request, response);
	}

	@Override
	public void init() throws ServletException {
		// TODO Auto-generated method stub
		super.init();
	}

}
