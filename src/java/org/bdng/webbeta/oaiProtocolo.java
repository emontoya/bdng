package org.bdng.webbeta;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.bdng.harvmgr.harvester.DataProvider;

public class oaiProtocolo extends HttpServlet {

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.ejecutar(request, response);

	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.ejecutar(request, response);

	}

	/**
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	private void ejecutar(HttpServletRequest request,
			HttpServletResponse response) throws IOException {

		String url = request.getParameter("verb");

		boolean validarDatos = false;

		String stRuta = this.getServletContext().getRealPath("/")
				+ "/conf/oai_providers.xml";

		if ((url == null) || (url.trim().length() == 0)) {
			url = "";
			validarDatos = true;
		}
		String stSalida = "";
		stSalida = "<dl><msg><valor>no</valor><mensaje>Faltan datos por llenar</mensaje></msg></dl>";

		PrintWriter out = response.getWriter();
		try {
			if (validarDatos) {
				out.println(stSalida);
				out.close();
				return;
			}

			DataProvider dp = new DataProvider();

			String iden = dp.Identify(url);

			System.out.println("Identificacion=\n" + iden);

			stSalida = "<dl><msg><valor>yes</valor>" + "<mensaje>" + iden
					+ "</mensaje>" + "</msg></dl>";

		} catch (Exception e) {
			stSalida = "<dl><msg><valor>no</valor><mensaje>" + e.getMessage()
					+ "</mensaje></msg></dl>";
		}
		System.out.println("AlmacenarDatos.ejecutar(stSalida) " + stSalida);
		out.println(stSalida);
		out.close();
	}
}