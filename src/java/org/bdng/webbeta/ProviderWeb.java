package org.bdng.webbeta;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.bdng.harvmgr.dlConfig;
import org.bdng.harvmgr.harvester.DataProvider;
import org.bdng.harvmgr.harvester.Scheduler;
import org.bdng.harvmgr.indexer.Indexer;

public class ProviderWeb extends HttpServlet {

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.ejecutar(request, response);

	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.ejecutar(request, response);

	}

	private void ejecutar(HttpServletRequest request,
			HttpServletResponse response) throws IOException {

		HttpSession sesion = request.getSession();
		response.setContentType("text/xml");
		response.setHeader("Cache-Control", "no-cache");
		response.setHeader("Pragma", "no-cache");
		response.setDateHeader("Expires", 0);
		sesion.setAttribute("pagina", "ListProviders.jsp");
		String stSalida = "";
		PrintWriter out = response.getWriter();
		String stRuta = this.getServletContext().getRealPath("/")
				+ "/conf/oai_providers.xml";

		String prov_opcion = request.getParameter("prov_opcion");
		// System.out.println(prov_opcion);

		String stRepName = request.getParameter("repname");
		String stUrlBase = request.getParameter("urlbase");
		String stShortName = request.getParameter("shortname");
		String stDescripcion = request.getParameter("descripcion");
		String stProtocol = request.getParameter("protocol");
		String stMetadataPrefix_oai = request
				.getParameter("metadataprefix_oai");
		String stMetadataPrefix_http = request
				.getParameter("metadataprefix_http");
		String stPrefijo = request.getParameter("prefijo");
		String stCollection = request.getParameter("collection");
		String stUrlhome = request.getParameter("urlhome");
		String stEmailadmin = request.getParameter("emailadmin");
		String stNameAdmin = request.getParameter("nameAdmin");
		String stPhoneAdmin = request.getParameter("phoneAdmin");
		String stReptype = request.getParameter("reptype");
		String stCdata = "2"; // OJO AKI
		String stInstitution = request.getParameter("institution");
		String stShortInst = request.getParameter("shortInst");
		String stStaticCollection = request.getParameter("staticCollection");
		String url = request.getParameter("verb");
		String stDepartamento = request.getParameter("Departamento");
		String stMunicipio = request.getParameter("Municipio");
		String stBdcol = request.getParameter("bdcol");
		String stOwner = request.getParameter("owner") == null ? "admin"
				: request.getParameter("owner"); // provisional AKI

		// System.out.println(request.getParameter("owner"));

		if (prov_opcion.equals("check_provider")) {

			dlConfig doa = new dlConfig(this.getServletContext().getRealPath(
					"/"));
			String FILE_OAI_PROVIDERS = doa.getOaiProviders();
			DataProvider dp = new DataProvider(this.getServletContext()
					.getRealPath("/"));
			boolean test = dp.isProvider(FILE_OAI_PROVIDERS, stShortName);

			if (test) {
				out.print("El Provider se actualizara");
				out.close();

			} else {
				out.print("Nombre Corto Disponible");
				out.close();

			}

		} else {

			if (prov_opcion.equals("crear_provider")) {
				try {
					DataProvider dp = new DataProvider(this.getServletContext()
							.getRealPath("/"));

					String stMetadataPrefix = stProtocol
							.equalsIgnoreCase("OAI") ? stMetadataPrefix_oai
							: stMetadataPrefix_http;

					boolean result = dp.create(stOwner, stRepName, stUrlBase,
							stShortName, stDescripcion, stMetadataPrefix,
							stPrefijo, stCollection, stUrlhome, stProtocol,
							stEmailadmin, stNameAdmin, stPhoneAdmin, stReptype,
							stCdata, stInstitution, stShortInst,
							stStaticCollection, stDepartamento, stMunicipio,
							stBdcol);
					String msg_out = "Operacion Correcta";
					if (result) {

						// upload oai_providers.xml to /db/conf in eXist -luis
						Indexer indexer = new Indexer(this.getServletContext()
								.getRealPath("/"));
						dlConfig dlconfig = new dlConfig(this
								.getServletContext().getRealPath("/"));
						indexer.uploadConf(dlconfig.getOaiProviders());

						response.sendRedirect("/"
								+ this.getServletContext()
										.getServletContextName() + "/index.jsp");

					} else
						msg_out = "<dl><msg><valor>no</valor><mensaje>Error</mensaje></msg></dl>";

					out.println(msg_out);
					out.flush();
					out.close();
				} catch (Exception e) {

				}

			} else {

				if (prov_opcion.equals("opt_borrar_rep")) {

					try {
						DataProvider dp = new DataProvider(this
								.getServletContext().getRealPath("/"));
						// dp.setRuta(this.getServletContext().getRealPath("/"));
						dp.delete(stShortName);
						System.out.println("Provider borrado: " + stShortName);
						sesion.setAttribute("pagina", "ListProviders.jsp");
						response.sendRedirect("/"
								+ this.getServletContext()
										.getServletContextName() + "/index.jsp");

					} catch (Exception e) {

					}

				} else {
					if (prov_opcion.equals("opt_actualizar_rep")) {
						sesion.setAttribute("stShortName", stShortName);
						sesion.setAttribute("pagina", "Provider.jsp");
						response.sendRedirect("/"
								+ this.getServletContext()
										.getServletContextName() + "/index.jsp");
					} else {
						if (prov_opcion.equals("listar_proveedores")) {// opcion
																		// no
																		// utilizada
							try {

								DataProvider dp = new DataProvider(this
										.getServletContext().getRealPath("/"));
								out.println(dp.getXmlProvider());
								out.flush();
								out.close();

							} catch (Exception e) {
								e.printStackTrace();
							}
						} else {
							if (prov_opcion.equals("listar_Scheduler_estatus")) {
								try {
									DataProvider dp = new DataProvider();
									String xml = dp.getXmlProviderStatus();
									// System.out.println(xml);

								} catch (Exception e) {
									e.printStackTrace();
								}

							} else {

								if (prov_opcion.equals("consultar_oaiprotocol")) {
									boolean validarDatos = false;
									url = stUrlBase; // linea agregada

									if ((url == null)
											|| (url.trim().length() == 0)) {
										url = "";
										validarDatos = true;
									}
									String stSalidas = "";
									stSalidas = "<dl><msg><valor>no</valor><mensaje>Faltan datos por llenar</mensaje></msg></dl>";

									PrintWriter outs = response.getWriter();
									try {
										if (validarDatos) {
											outs.println(stSalidas);
											outs.close();
											return;
										}

										DataProvider dp = new DataProvider(this
												.getServletContext()
												.getRealPath("/"));

										String iden = dp.Identify(url);

										System.out.println("Identificacion=\n"
												+ iden);

										stSalidas = "<dl><msg><valor>yes</valor>"
												+ "<mensaje>"
												+ iden
												+ "</mensaje>" + "</msg></dl>";

									} catch (Exception e) {
										stSalidas = "<dl><msg><valor>no</valor><mensaje>"
												+ e.getMessage()
												+ "</mensaje></msg></dl>";
									}
									// System.out.println("AlmacenarDatos.ejecutar(stSalida) "
									// + stSalidas);
									outs.println(stSalidas);
									outs.close();

								} else {
									if (prov_opcion
											.equals("ListMetadataFormats")) {
										boolean validarDatos = false;
										url = stUrlBase; // linea agregada
										// System.out.println("URL "+url);

										if ((url == null)
												|| (url.trim().length() == 0)) {
											url = "";
											validarDatos = true;
										}
										String stSalidas = "";
										stSalidas = "<dl><msg><valor>no</valor><mensaje>Faltan datos por llenar</mensaje></msg></dl>";

										PrintWriter outs = response.getWriter();
										try {
											if (validarDatos) {
												outs.println(stSalidas);
												outs.close();
												return;
											}

											DataProvider dp = new DataProvider(
													this.getServletContext()
															.getRealPath("/"));

											String iden = dp
													.ListMetadataFormats(url);
											// System.out.println(iden);
											// System.out.println("Identificacion=\n"
											// + iden);

											stSalidas = "<dl><msg><valor>yes</valor>"
													+ "<mensaje>"
													+ iden
													+ "</mensaje>"
													+ "</msg></dl>";
											// stSalidas= iden;

										} catch (Exception e) {
											stSalidas = "<dl><msg><valor>no</valor><mensaje>"
													+ e.getMessage()
													+ "</mensaje></msg></dl>";
										}
										// System.out.println("AlmacenarDatos.ejecutar(stSalida) "
										// + stSalidas);
										outs.println(stSalidas);
										outs.close();

									} else {

									}

								}
							}

						}
					}
				}
			}

		}

	}

}
