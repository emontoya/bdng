package org.bdng.webbeta;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class UtilXML {
	public static void main(String argv[]) {
		try {
			File file = new File(
					"C:/Java/apache-tomcat-6.0.14/apache-tomcat-6.0.14/webapps/bdcol//conf/dl_oai_providers.xml");
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document doc = db.parse(file);
			doc.getDocumentElement().normalize();
			System.out.println("Root element "
					+ doc.getDocumentElement().getNodeName());
			NodeList nodeLst = doc.getElementsByTagName("oai_providers");
			System.out.println("Information of all oai_provider");

			for (int s = 0; s < nodeLst.getLength(); s++) {
				Node fstNode = nodeLst.item(s);
				Element fstElmnt = (Element) fstNode;
				if (fstElmnt.getTagName().equals("oai_providers")) {
					System.out.println("XMLReader.main()");

					Element element2 = doc.createElement("oai_provider");
					fstElmnt.insertBefore(element2, fstElmnt.getLastChild()
							.getNextSibling());

					Element repositoryName = doc
							.createElement("repositoryName");
					element2.insertBefore(repositoryName, null);
					repositoryName
							.appendChild(doc
									.createTextNode("Universidad Nacional de Colombia"));

					Element shortName = doc.createElement("shortName");
					element2.insertBefore(shortName, null);
					shortName.appendChild(doc.createTextNode("unal"));

					Element baseURL = doc.createElement("baseURL");
					element2.insertBefore(baseURL, null);
					baseURL.appendChild(doc
							.createTextNode("http://digital.unal.edu.co/dspace-oai/request"));

					Element description = doc.createElement("description");
					element2.insertBefore(description, null);
					description
							.appendChild(doc
									.createTextNode("Esta es la Universidad Nacional de Colombia"));

					Element collection = doc.createElement("collection");
					element2.insertBefore(collection, null);
					collection.appendChild(doc.createTextNode("general"));

					Element metadataPrefix = doc
							.createElement("metadataPrefix");
					element2.insertBefore(metadataPrefix, null);
					metadataPrefix.appendChild(doc.createTextNode("oai_dc"));

					Element resumption = doc.createElement("resumption");
					element2.insertBefore(resumption, null);
					resumption.appendChild(doc.createTextNode("false"));

					Element harvest_status = doc
							.createElement("harvest_status");
					element2.insertBefore(harvest_status, null);
					harvest_status.appendChild(doc
							.createTextNode("NOT_STARTED"));

					Element harvest_count = doc.createElement("harvest_count");
					element2.insertBefore(harvest_count, null);
					harvest_count.appendChild(doc.createTextNode("1"));

					Element last_harvest_date = doc
							.createElement("last_harvest_date");
					element2.insertBefore(last_harvest_date, null);
					last_harvest_date.appendChild(doc
							.createTextNode("0000-00-00T00:00:00"));

					Element last_resumption_token = doc
							.createElement("last_resumption_token");
					element2.insertBefore(last_resumption_token, null);
					// <>
					Element last_id_file = doc.createElement("last_id_file");
					element2.insertBefore(last_id_file, null);
					last_id_file.appendChild(doc.createTextNode("1"));

					Element harvest_count_incremental = doc
							.createElement("harvest_count_incremental");
					element2.insertBefore(harvest_count_incremental, null);
					harvest_count_incremental.appendChild(doc
							.createTextNode("4"));

					Element harvest_frecuency = doc
							.createElement("harvest_frecuency");
					element2.insertBefore(harvest_frecuency, null);
					harvest_frecuency.appendChild(doc.createTextNode("8"));

					Element schedule_status = doc
							.createElement("schedule_status");
					element2.insertBefore(schedule_status, null);
					schedule_status
							.appendChild(doc.createTextNode("NOT_READY"));

				}
				try {
					// Prepare the DOM document for writing
					Source source = new DOMSource(doc);
					// Prepare the output file
					// file = new File(filename);
					Result result = new StreamResult(file);
					// Write the DOM document to the file
					Transformer xformer = TransformerFactory.newInstance()
							.newTransformer();
					xformer.setOutputProperty(OutputKeys.INDENT, "yes");
					// xformer.setOutputProperty(OutputKeys.METHOD, "xml");
					xformer.transform(source, result);
				} catch (TransformerConfigurationException e) {
					e.printStackTrace();
				} catch (TransformerException e) {
					e.printStackTrace();
				}
				// Element element2 = doc.createElement("item");
				// element.insertBefore(element2,
				// element.getFirstChild().getNextSibling());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @param file
	 * @return
	 * @throws Exception
	 */
	public Document leerXml(File file) throws Exception {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.parse(file);
		doc.getDocumentElement().normalize();
		return doc;

	}

	/**
	 * @param file
	 * @param doc
	 */
	public void escribirXml(File file, Document doc) {
		try {
			// Prepare the DOM document for writing
			Source source = new DOMSource(doc);
			// file = new File(filename);
			Result result = new StreamResult(file);
			// Write the DOM document to the file
			Transformer xformer = TransformerFactory.newInstance()
					.newTransformer();
			xformer.setOutputProperty(OutputKeys.INDENT, "yes");
			xformer.transform(source, result);
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @param stRuta
	 * @param stRepName
	 * @param stUrlBase
	 * @param stShortName
	 * @param stDescripcion
	 * @param stMetadataPrefix
	 * @param stPrefijo
	 * @param stCollection
	 */
	public void agregarElemetos(String stRuta, String stRepName,
			String stUrlBase, String stShortName, String stDescripcion,
			String stMetadataPrefix, String stPrefijo, String stCollection,
			String stUrlhome, String stProtocol, String stEmailadmin,
			String stReptype) {
		// TODO Auto-generated method stub
		File file = new File(stRuta);
		try {
			Document doc = this.leerXml(file);
			agregarCampos(doc, stRepName, stUrlBase, stShortName,
					stDescripcion, stMetadataPrefix, stPrefijo, stCollection,
					stUrlhome, stProtocol, stEmailadmin, stReptype);
			escribirXml(file, doc);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @param doc
	 * @param stRepName
	 * @param stUrlBase
	 * @param stShortName
	 * @param stDescripcion
	 * @param stMetadataPrefix
	 * @param stPrefijo
	 * @param stCollection
	 */
	private void agregarCampos(Document doc, String stRepName,
			String stUrlBase, String stShortName, String stDescripcion,
			String stMetadataPrefix, String stPrefijo, String stCollection,
			String stUrlhome, String stProtocol, String stEmailadmin,
			String stReptype) {

		NodeList nodeLst = doc.getElementsByTagName("oai_providers");
		System.out
				.println("Information of all oai_provider 1 2 3 4 5 6 7 8 9 0");

		for (int s = 0; s < nodeLst.getLength(); s++) {
			Node fstNode = nodeLst.item(s);
			Element fstElmnt = (Element) fstNode;
			if (fstElmnt.getTagName().equals("oai_providers")) {
				System.out.println("UtilXML.agregarCampos()");

				Element element2 = doc.createElement("oai_provider");
				fstElmnt.insertBefore(element2, fstElmnt.getLastChild()
						.getNextSibling());

				Element repositoryName = doc.createElement("repositoryName");
				element2.insertBefore(repositoryName, null);
				repositoryName.appendChild(doc.createTextNode(stRepName));

				Element shortName = doc.createElement("shortName");
				element2.insertBefore(shortName, null);
				shortName.appendChild(doc.createTextNode(stShortName));

				Element baseURL = doc.createElement("baseURL");
				element2.insertBefore(baseURL, null);
				baseURL.appendChild(doc.createTextNode(stUrlBase));

				Element description = doc.createElement("description");
				element2.insertBefore(description, null);
				description.appendChild(doc.createTextNode(stDescripcion));

				Element collection = doc.createElement("collection");
				element2.insertBefore(collection, null);
				collection.appendChild(doc.createTextNode(stCollection));

				Element metadataPrefix = doc.createElement("metadataPrefix");
				element2.insertBefore(metadataPrefix, null);
				metadataPrefix
						.appendChild(doc.createTextNode(stMetadataPrefix));

				Element resumption = doc.createElement("resumption");
				element2.insertBefore(resumption, null);
				resumption.appendChild(doc.createTextNode(stPrefijo));

				Element urlhome = doc.createElement("urlhome");
				element2.insertBefore(urlhome, null);
				urlhome.appendChild(doc.createTextNode(stUrlhome));

				Element protocol = doc.createElement("protocol");
				element2.insertBefore(protocol, null);
				protocol.appendChild(doc.createTextNode(stProtocol));

				Element emailadmin = doc.createElement("emailadmin");
				element2.insertBefore(emailadmin, null);
				emailadmin.appendChild(doc.createTextNode(stEmailadmin));

				Element reptype = doc.createElement("reptype");
				element2.insertBefore(reptype, null);
				reptype.appendChild(doc.createTextNode(stReptype));

				Element harvest_status = doc.createElement("harvest_status");
				element2.insertBefore(harvest_status, null);
				harvest_status.appendChild(doc.createTextNode("NOT_STARTED"));

				Element harvest_count = doc.createElement("harvest_count");
				element2.insertBefore(harvest_count, null);
				harvest_count.appendChild(doc.createTextNode("1"));

				Element last_harvest_date = doc
						.createElement("last_harvest_date");
				element2.insertBefore(last_harvest_date, null);
				last_harvest_date.appendChild(doc
						.createTextNode("0000-00-00T00:00:00"));

				Element last_resumption_token = doc
						.createElement("last_resumption_token");
				element2.insertBefore(last_resumption_token, null);
				// <>
				Element last_id_file = doc.createElement("last_id_file");
				element2.insertBefore(last_id_file, null);
				last_id_file.appendChild(doc.createTextNode("1"));

				Element harvest_count_incremental = doc
						.createElement("harvest_count_incremental");
				element2.insertBefore(harvest_count_incremental, null);
				harvest_count_incremental.appendChild(doc.createTextNode("4"));

				Element harvest_frecuency = doc
						.createElement("harvest_frecuency");
				element2.insertBefore(harvest_frecuency, null);
				harvest_frecuency.appendChild(doc.createTextNode("8"));

				Element schedule_status = doc.createElement("schedule_status");
				element2.insertBefore(schedule_status, null);
				schedule_status.appendChild(doc.createTextNode("NOT_READY"));

				Element printStackTrace = doc.createElement("printStackTrace");
				element2.insertBefore(printStackTrace, null);
				printStackTrace.appendChild(doc.createTextNode(""));
			}
		}
	}

}