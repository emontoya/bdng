package org.bdng.web;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.bdng.harvmgr.harvester.DataProvider;
import org.bdng.harvmgr.validator.Validator;

/**
 * Servlet implementation class PublishWeb
 */
public class PublishWeb extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.ejecutar(request, response);

	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.ejecutar(request, response);

	}

	private void ejecutar(HttpServletRequest request,
			HttpServletResponse response) throws IOException {

		response.setContentType("text/xml");
		response.setHeader("Cache-Control", "no-cache");
		response.setHeader("Pragma", "no-cache");
		response.setDateHeader("Expires", 0);

		String dl_home = this.getServletContext().getRealPath("/");

		String action = request.getParameter("action");
		String stShortname = request.getParameter("shortname");
		String stOwner = request.getParameter("owner");

		/*
		 * System.out.println(stShortname); System.out.println(stOwner);
		 */

		DataProvider dp = new DataProvider(dl_home);

		if (action.equals("publish")) {
			boolean result = dp.setActiveStatus(stShortname, "1");
			response.sendRedirect("/"
					+ this.getServletContext().getServletContextName()
					+ "/ListToPublishUser.jsp");
		} else if (action.equals("allow")) {
			boolean result = dp.setActiveStatus(stShortname, "0");
			if (result)
				dp.setOwner(stShortname, "admin");
			response.sendRedirect("/"
					+ this.getServletContext().getServletContextName()
					+ "/ListToPublish.jsp");
		} else if (action.equals("refuse")) {
			boolean result = dp.setActiveStatus(stShortname, "2");
			response.sendRedirect("/"
					+ this.getServletContext().getServletContextName()
					+ "/ListToPublish.jsp");
		}

	}

}
