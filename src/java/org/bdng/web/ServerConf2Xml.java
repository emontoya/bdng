package org.bdng.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.bdng.harvmgr.harvester.ServerConfXml;

public class ServerConf2Xml extends HttpServlet {

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.ejecutar(request, response);

	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.ejecutar(request, response);

	}

	/**
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	private void ejecutar(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		response.setContentType("text/xml");
		response.setHeader("Cache-Control", "no-cache");
		response.setHeader("Pragma", "no-cache");
		response.setDateHeader("Expires", 0);

		String stRuta = this.getServletContext().getRealPath("/");

		System.out.println("path abs =" + stRuta);

		ServerConfXml scxml = new ServerConfXml(stRuta);

		String stSalida = scxml.getXmlFile();

		PrintWriter out = response.getWriter();
		System.out.println("ServerConf.ejecutar(stSalida) " + stSalida);
		out.println(stSalida);
		out.close();
	}
}