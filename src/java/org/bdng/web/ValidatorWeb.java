package org.bdng.web;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.bdng.harvmgr.validator.Validator;

/**
 * Servlet implementation class ValidatorWeb
 */
public class ValidatorWeb extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.ejecutar(request, response);

	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.ejecutar(request, response);

	}

	private void ejecutar(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		
		HttpSession sesion = request.getSession();
		response.setContentType("text/xml");
		response.setHeader("Cache-Control", "no-cache");
		response.setHeader("Pragma", "no-cache");
		response.setDateHeader("Expires", 0);
		sesion.setAttribute("pagina", "validator.jsp");

		String dl_home = this.getServletContext().getRealPath("/");

		String valid_option = request.getParameter("option");
		if (valid_option.equalsIgnoreCase("validate")) {

			String stShortname = request.getParameter("shortname");
			String stUrlBase = request.getParameter("urlbase");
			String stProtocol = request.getParameter("protocol");
			String stMetadataPrefix = request.getParameter("metadataPrefix");
			/*
			 * System.out.println(stShortname); System.out.println(stUrlBase);
			 * System.out.println(stProtocol);
			 * System.out.println(stMetadataPrefix);
			 */
			Validator validator = new Validator(dl_home);
			System.out
					.println("******************Iniciando Validacion******************");

			validator.validarRepositorio(stUrlBase, stShortname, stProtocol,
					stMetadataPrefix);

			System.out
					.println("******************Validador ha finalizado******************");
			
			sesion.setAttribute("stShortName", stShortname);

			response.sendRedirect("/"
					+ this.getServletContext().getServletContextName()
					+ "/index.jsp");
		} else if (valid_option.equalsIgnoreCase("updateTextPatterns")) {

			Validator validator = new Validator(dl_home);

			String stShortname = request.getParameter("shortname");

			Enumeration e = request.getParameterNames();

			while (e.hasMoreElements()) {

				String select = (String) e.nextElement();

				if (!select.equalsIgnoreCase("Grabar")
						&& !select.equalsIgnoreCase("option")
						&& !select.equalsIgnoreCase("shortname")) { // parametros
																	// que
																	// vienen y
																	// tenemos
																	// que
																	// filtrar
					String newText = request.getParameter(select);

					select = select.replace("¿?", " "); // este par de
														// caracteres se le
														// pusieron para que no
														// hubiera espeacios en
														// el name del select,
														// aqui se los quitamos

					// System.out.println(select + " --> "+newText);
					try {
						String[] partes = select.split("___");

						String tag = partes[0];
						String oldText = partes[1];

						System.out.println(stShortname + " " + tag + " "
								+ oldText + " " + newText);
						validator.modificarTextPatterns(stShortname, tag,
								oldText, newText);

					} catch (Exception e1) {

						System.out
								.println("Error intentando modificar el archivo textPatterns.xml");
					}

				}

			}
			
			sesion.setAttribute("stShortName", stShortname);
			response.sendRedirect("/"
					+ this.getServletContext().getServletContextName()
					+ "/index.jsp");

		}

	}
}
