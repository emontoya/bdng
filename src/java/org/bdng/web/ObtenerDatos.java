package org.bdng.web;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ObtenerDatos extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.ejecutar(request, response);
	}

	private void ejecutar(HttpServletRequest request,
			HttpServletResponse response) throws IOException {

		response.setContentType("text/xml");
		response.setHeader("Cache-Control", "no-cache");
		response.setHeader("Pragma", "no-cache");
		response.setDateHeader("Expires", 0);

		PrintWriter out = response.getWriter();
		String stRuta = this.getServletContext().getRealPath("/")
				+ "/conf/oai_providers.xml";
		try {
			File f = new File(stRuta);
			// ///////////////////////////////////////
			int ch;
			StringBuffer strContent = new StringBuffer("");
			FileInputStream fin = null;

			fin = new FileInputStream(f);
			while ((ch = fin.read()) != -1) {
				strContent.append((char) ch);
			}
			fin.close();
			// System.out.println(strContent);
			String stSalida = strContent.toString();
			stSalida = stSalida.replace("<oai_providers>", "");
			stSalida = stSalida.replace("</oai_providers>", "");
			// System.out.println(stSalida);
			out.println(stSalida);
			out.flush();
			out.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest,
	 *      javax.servlet.http.HttpServletResponse)
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.ejecutar(request, response);
	}
}
