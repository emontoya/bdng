package org.bdng.web;

import java.io.Console;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.bdng.harvmgr.harvester.DataProvider;
import org.bdng.harvmgr.harvester.HarvesterThread;
import org.bdng.harvmgr.indexer.Indexer;

public class IndexarWeb extends HttpServlet {

	Indexer indexer = null;

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.ejecutar(request, response);

	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.ejecutar(request, response);

	}

	/**
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	private void ejecutar(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		
		HttpSession sesion = request.getSession();
		response.setContentType("text/xml");
		response.setHeader("Cache-Control", "no-cache");
		response.setHeader("Pragma", "no-cache");
		response.setDateHeader("Expires", 0);
		sesion.setAttribute("pagina", "Indexador.jsp");
		String stSalida = "";
		PrintWriter out = response.getWriter();
		// String stRuta =
		// this.getServletContext().getRealPath("/")+"/conf/oai_providers.xml";
		String index_option = request.getParameter("index_opcion");

		String stRuta = this.getServletContext().getRealPath("/");
		String msg = "";
		// System.out.println(sched_option);
		// List<String> listaProviders = (List)
		// request.getAttribute("listProviders");

		if (index_option.equals("listar_prov")) {

			try {

				DataProvider dp = new DataProvider(this.getServletContext()
						.getRealPath("/"));
				out.println(dp.getXmlProvider());
				out.flush();
				out.close();

			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			if(index_option.equals("refresh_providers")){
				response.sendRedirect("/"
						+ this.getServletContext().getServletContextName()
						+ "/index.jsp");
			}
			else {
			if (index_option.equals("indexar_seleccionados")) {
				System.out.println("Entro a indexar seleccionados");
				DataProvider dp = null;

				// String provs = request.getParameter("listProviders");
				String providers[] = request
						.getParameterValues("listProviders");
				System.out.println(providers[0]);
				System.out.println(providers.length);
				dp = new DataProvider(this.getServletContext().getRealPath("/"));
				

				/*
				 * if (provs != null && !provs.equals("")) { dp = new
				 * DataProvider(this.getServletContext().getRealPath("/"));
				 * 
				 * String[] providers = provs.split(",");
				 */
				try {
					if (indexer == null) {
						indexer = new Indexer(stRuta, providers);
						indexer.start();
					}

					if (!indexer.isWorking()) {
						msg = "Iniciando Indexacion...";
						indexer = new Indexer(stRuta, providers);
						indexer.start();
					} else
						msg = "Indexaci�n en progreso...";

					// indexer.upload2exist();
					stSalida = "<dl><msg><valor>yes</valor>" + "<mensaje>"
							+ msg + "</mensaje>" + "</msg></dl>";

				} catch (Exception e) {
					e.printStackTrace();
					stSalida = "<dl><msg><valor>no</valor><mensaje>"
							+ e.getMessage() + "</mensaje></msg></dl>";
				}
				
//				sesion.setAttribute("barraCargando", "true");
				response.sendRedirect("/"
						+ this.getServletContext().getServletContextName()
						+ "/index.jsp");

				/*
				 * out.println(dp.getXmlProvider()); out.flush(); out.close();
				 */

			} else {
				if (index_option.equals("indexar_todos")) {
					
					DataProvider dp = null;
					dp = new DataProvider(this.getServletContext().getRealPath(
							"/"));

					try {

						if (indexer == null) {
							indexer = new Indexer(stRuta);
							indexer.start();
						}

						if (!indexer.isWorking()) {
							msg = "Iniciando Indexacion...";
							indexer = new Indexer(stRuta);
							indexer.start();
						} else
							msg = "Indexaci�n en progreso...";

						// indexer.upload2exist();
						stSalida = "<dl><msg><valor>yes</valor>" + "<mensaje>"
								+ msg + "</mensaje>" + "</msg></dl>";

					} catch (Exception e) {
						e.printStackTrace();
						stSalida = "<dl><msg><valor>no</valor><mensaje>"
								+ e.getMessage() + "</mensaje></msg></dl>";
					}
					/*
					 * out.println(dp.getXmlProvider()); out.flush();
					 * out.close();
					 */
					response.sendRedirect("/"
							+ this.getServletContext().getServletContextName()
							+ "/index.jsp");
				}else{
					if (index_option.equals("check_progress")) {
//						System.out.println("Esta funcionando la barra");
						String providerActual = indexer.getCurrentFile();
						int archivosCompletos = indexer.getProgress();
//						int archivosSize = 3;
						
//						System.out.println("Esto viene de web " + indexer.getProgress());

						String progress = "";
						progress += "<div id=\"data\">";
						progress += "<div id=\"dataProvidersCompletos\">";
						progress += archivosCompletos;
						progress += "</div>";
						progress += "<div id=\"dataProviderActual\">";
						progress += providerActual;
						progress += "</div>";

						progress += "</div>";

						// System.out.println("dataProvidersCompletos: "+dataProvidersCompletos+" de "+dataProvidersSize);

						out.println(progress);
						out.close();
					}
				}
			}
		}
	}
	}
}
