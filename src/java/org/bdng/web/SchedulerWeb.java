package org.bdng.web;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.bdng.harvmgr.harvester.DataProvider;
import org.bdng.harvmgr.harvester.Scheduler;

public class SchedulerWeb extends HttpServlet {

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.ejecutar(request, response);

	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.ejecutar(request, response);

	}

	private void ejecutar(HttpServletRequest request,
			HttpServletResponse response) throws IOException {

		response.setContentType("text/xml");
		response.setHeader("Cache-Control", "no-cache");
		response.setHeader("Pragma", "no-cache");
		response.setDateHeader("Expires", 0);
		String stSalida = "";
		PrintWriter out = response.getWriter();
		// String stRuta =
		// this.getServletContext().getRealPath("/")+"/conf/oai_providers.xml";
		String sched_option = request.getParameter("sched_option");
		// System.out.println(sched_option);
		// List<String> listaProviders = (List)
		// request.getAttribute("listProviders");

		if (sched_option.equals("list_scheduler_status")) {

			try {

				response.sendRedirect("/"
						+ this.getServletContext().getServletContextName()
						+ "/Scheduler.jsp");
				/*
				 * DataProvider dp = new
				 * DataProvider(this.getServletContext().getRealPath("/"));
				 * out.println(dp.getXmlProviderStatus()); out.flush();
				 * out.close();
				 */

			} catch (Exception e) {
				e.printStackTrace();
			}

		} else {
			if (sched_option.equals("ejecutar_scheduler")) {

				try {

					Scheduler sc = new Scheduler(this.getServletContext()
							.getRealPath("/"));

					sc.validarDataProvidersOAI();
					/*
					 * DataProvider dp = new
					 * DataProvider(this.getServletContext().getRealPath("/"));
					 * out.println(dp.getXmlProviderStatus()); out.flush();
					 * out.close();
					 */
					response.sendRedirect("/"
							+ this.getServletContext().getServletContextName()
							+ "/Scheduler.jsp");

				} catch (Exception e) {
					stSalida = "<dl><msg><valor>no</valor><mensaje>"
							+ e.getMessage() + "</mensaje></msg></dl>";
				}
				// System.out.println("Scheduler.ejecutar(stSalida) " +
				// stSalida);
				// out.println(stSalida);
				// out.close();
			} else {

				if (sched_option.equals("reset_sc_status")) {

					try {
						DataProvider dp = new DataProvider(this
								.getServletContext().getRealPath("/"));
						// dp.setRuta(this.getServletContext().getRealPath("/")+
						// "/conf/oai_providers_status.xml");
						dp.resetAllSchedulerStatus();
						response.sendRedirect("/"
								+ this.getServletContext()
										.getServletContextName()
								+ "/Scheduler.jsp");

						/*
						 * out.println(dp.getXmlProviderStatus()); out.flush();
						 * out.close();
						 */

					} catch (Exception e) {
						stSalida = "<dl><msg><valor>no</valor><mensaje>"
								+ e.getMessage() + "</mensaje></msg></dl>";
						out.println(stSalida);
						out.close();
					}
				} else {

					if (sched_option.equals("reset_scheduler_selected")) {

						DataProvider dp = null;
						String providers[] = request
								.getParameterValues("listProviders");
						/*
						 * String provs = request.getParameter("listProviders");
						 * 
						 * if (provs != null && !provs.equals("")) { dp = new
						 * DataProvider
						 * (this.getServletContext().getRealPath("/"));
						 * 
						 * String[] providers = provs.split(",");
						 */
						for (int i = 0; i < providers.length; i++) {
							try {
								dp.resetScheduleStatus(providers[i]);
							} catch (Exception e) {
							}
						}
						response.sendRedirect("/"
								+ this.getServletContext()
										.getServletContextName()
								+ "/Scheduler.jsp");
						/*
						 * out.println(dp.getXmlProviderStatus()); out.flush();
						 * out.close();
						 */

					}
				}

			}
		}
	}
}
