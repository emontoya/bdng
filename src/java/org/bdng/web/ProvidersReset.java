package org.bdng.web;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.bdng.harvmgr.harvester.DataProvider;

public class ProvidersReset extends HttpServlet {

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.ejecutar(request, response);

	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.ejecutar(request, response);

	}

	/**
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	private void ejecutar(HttpServletRequest request,
			HttpServletResponse response) throws IOException {

		PrintWriter out = response.getWriter();
		response.setContentType("text/xml");
		response.setHeader("Cache-Control", "no-cache");
		response.setHeader("Pragma", "no-cache");
		response.setDateHeader("Expires", 0);
		String stSalida = "";
		String stRuta = this.getServletContext().getRealPath("/")
				+ "/conf/oai_providers_status.xml";
		try {
			DataProvider dp = new DataProvider();
			dp.setRuta(this.getServletContext().getRealPath("/"));
			dp.resetAllProviders();
			stSalida = "<dl><msg><valor>yes</valor>"
					+ "<mensaje>Datos Inicializados Correctamente</mensaje>"
					+ "</msg></dl>";

			File f = new File(stRuta);
			// ///////////////////////////////////////
			int ch;
			StringBuffer strContent = new StringBuffer("");
			FileInputStream fin = null;

			fin = new FileInputStream(f);
			while ((ch = fin.read()) != -1) {
				strContent.append((char) ch);
			}
			fin.close();
			stSalida = strContent.toString();
			out.println(stSalida);
			out.flush();
			out.close();

		} catch (Exception e) {
			stSalida = "<dl><msg><valor>no</valor><mensaje>" + e.getMessage()
					+ "</mensaje></msg></dl>";
			out.println(stSalida);
			out.close();
		}
	}

}
