package org.bdng.web;

import org.bdng.harvmgr.dlConfig;

public class UserEnt {

	private String username = "";
	private String pwd = "";
	private String type = "";

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public UserEnt(String dl_home) {

		dlConfig dlconfig = new dlConfig();
		this.username = dlconfig.getExistUser();
		this.pwd = dlconfig.getExistPass();
		if (username.equals("admin"))
			this.type = "admin";
		else
			this.type = "adminRepo";
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	@Override
	public String toString() {
		return "UserEnt [username=" + username + ", pwd=" + pwd + ", type="
				+ type + "]";
	}

}