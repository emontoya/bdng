package org.bdng.web;

import org.bdng.harvmgr.dlConfig;
import org.bdng.harvmgr.indexer.Indexer;

public class Utils {

	static String dl_home = null;

	public Utils(String dl_home) {
		this.dl_home = dl_home;

	}

	public static void uploadConf(String file) {

		String myDl_home = System.getProperty("user.dir").replace("eXist", "");
		// System.out.println("usinng= "+myDl_home);
		Indexer indexer = new Indexer(myDl_home);
		dlConfig dlconfig = new dlConfig(myDl_home);

		if (file.equals("serverconfig"))
			indexer.uploadConf(dlconfig.getfileServerConfig());
		else if (file.equals("oai_providers")) {
			indexer.uploadConf(dlconfig.getOaiProviders());
		}

	}

}
