package org.bdng.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.bdng.harvmgr.dlConfig;
import org.bdng.harvmgr.harvester.DataProvider;
import org.bdng.harvmgr.indexer.Indexer;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;

/**
 * Servlet implementation class FiltrarProviders
 */
public class FiltrarProviders extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private final String myDl_home = System.getProperty("user.dir").replace("eXist","");
	private final String FILE_OAI_PROVIDERS = myDl_home + "conf/oai_providers.xml";
	
	public static String OAI_PROVIDERS = "oai_providers";
	public static String OAI_PROVIDER = "oai_provider";
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FiltrarProviders() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		this.ejecutar(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		this.ejecutar(request, response);
	}
	
	private void ejecutar(HttpServletRequest request,HttpServletResponse response) throws IOException {
		HttpSession sesion = request.getSession();
		PrintWriter out = response.getWriter();
		response.setContentType("text/xml");
		response.setHeader("Cache-Control", "no-cache");
		response.setHeader("Pragma", "no-cache");
		response.setDateHeader("Expires", 0);
		try{
			
	//		sesion.setAttribute("pagina", "mensajeExito.jsp");
			
			String optionFiltrar = request.getParameter("optionFiltrar");
			String stReptype = "";
			
			if (optionFiltrar.equals("filtrartodos")) {
				stReptype = "1";
			}else if(optionFiltrar.equals("nofiltrartodos")){
				stReptype = "0";
			}
			
			if(!(stReptype.equals(""))){
				try {
					SAXBuilder builder = new SAXBuilder();
					Document doc = builder.build(FILE_OAI_PROVIDERS);
					Element raiz = doc.getRootElement();
					Element providers = raiz.getChild(OAI_PROVIDERS);
					List<Element> provider = providers.getChildren(OAI_PROVIDER);
					Iterator<Element> i = provider.iterator();
					
					while (i.hasNext()) {
						Element l = (Element) i.next();
						
						Element e = l.getChild("shortName");
						String stShortName = e.getText();
						
						e = l.getChild("repositoryName");
						String stRepName = e.getText();
	
						e = l.getChild("description");
						String stDescripcion = e.getText();
	
						e = l.getChild("urlhome");
						String stUrlhome = e.getText();
	
						e = l.getChild("protocol");
						String stProtocol = e.getText();
	
						e = l.getChild("baseURL");
						String stUrlBase = e.getText();
	
						e = l.getChild("nameAdmin");
						String stNameAdmin = e.getText();
	
						e = l.getChild("emailadmin");
						String stEmailadmin = e.getText();
	
						e = l.getChild("phoneAdmin");
						String stPhoneAdmin = e.getText();
	
						e = l.getChild("collection");
						String stCollection = e.getText();
						
						e = l.getChild("metadataPrefix");
						String stMetadataPrefix = e.getText();
						
						e = l.getChild("institution");
						String stInstitution = e.getText();
						
						e = l.getChild("shortInst");
						String stShortInst = e.getText();
						
						e = l.getChild("staticCollection");
						String stStaticCollection = e.getText();
						
						e = l.getChild("departamento");
						String stDepartamento = e.getText();
						
						e = l.getChild("municipio");
						String stMunicipio = e.getText();
						
						e = l.getChild("bdcol");
						String stBdcol = e.getText();
						
						e = l.getChild("owner");
						String stOwner = e.getText();
						
						String stCdata = "2";
						
						String stPrefijo = "";
						
						
						DataProvider dp = new DataProvider(this.getServletContext().getRealPath("/"));
						
						boolean result = dp.create(stOwner, stRepName, stUrlBase,
								stShortName, stDescripcion, stMetadataPrefix,
								stPrefijo, stCollection, stUrlhome, stProtocol,
								stEmailadmin, stNameAdmin, stPhoneAdmin, stReptype,
								stCdata, stInstitution, stShortInst,
								stStaticCollection, stDepartamento, stMunicipio,
								stBdcol);
						
						if(result){
							Indexer indexer = new Indexer(this.getServletContext().getRealPath("/"));
							dlConfig dlconfig = new dlConfig(this.getServletContext().getRealPath("/"));
							indexer.uploadConf(dlconfig.getOaiProviders());
							
							if(stReptype.equals("1")){
								System.out.println("El proveedor " + stShortName + " ha cambiado su estado para ser filtrado");
							}else if(stReptype.equals("0")){
								System.out.println("El proveedor " + stShortName + " ha cambiado su estado para no ser filtrado");
							}
						}else{
							if(stReptype.equals("1")){
								System.out.println("El proveedor " + stShortName + " no ha podido cambiar su estado para ser filtrado");
							}else if(stReptype.equals("0")){
								System.out.println("El proveedor " + stShortName + " no ha podido cambiar su estado para no ser filtrado");
							}
						}
					}
					
					if(stReptype.equals("1")){
						sesion.setAttribute("message", "Todos los proveedores han cambiado su estado para ser filtrados");
						System.out.println("Todos los proveedores han cambiado su estado para ser filtrados");
					}else if(stReptype.equals("0")){
						sesion.setAttribute("message", "Todos los proveedores han cambiado su estado para no ser filtrados");
						System.out.println("Todos los proveedores han cambiado su estado para no ser filtrados");
					}
					sesion.setAttribute("pagina", "mensajeExito.jsp");
					
				}catch(Exception ex){
					sesion.setAttribute("message", "Error al ejecutar la funcion para filtrar proveedores: " + ex);
					sesion.setAttribute("pagina", "mensajeError.jsp");
					System.out.println("Error al importar el archivo: " + ex);
				}
			}else{
				sesion.setAttribute("message", "Error en los parametros enviados al servidor");
				sesion.setAttribute("pagina", "mensajeError.jsp");
				System.out.println("Error en los parametros enviados al servidor");
			}
		}catch(Exception ex){
			sesion.setAttribute("message", "Error en los parametros enviados al servidor");
			sesion.setAttribute("pagina", "mensajeError.jsp");
			System.out.println("Error en los parametros enviados al servidor");
		}
		
		response.sendRedirect("/"+ this.getServletContext().getServletContextName()+ "/index.jsp");
	}

}
