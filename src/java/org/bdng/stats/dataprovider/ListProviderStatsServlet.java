package org.bdng.stats.dataprovider;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.bdng.stats.Util;

public class ListProviderStatsServlet extends HttpServlet {

	public ListProviderStatsServlet() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// String dataProvider = request.getParameter("dataprovider");
		// if (dataProvider == null) {
		// return;
		// } else {
		//
		// }
		// //init the controller
		// ProviderStatsController controller = new
		// ProviderStatsController(dataProvider,
		// getServletContext().getRealPath(""));
		// /*generate charts*/
		// controller.generateCharts();
		// /*redirect*/
		String path = getServletContext().getRealPath("");
		Util u = new Util();
		ArrayList<HashMap<String, String>> o = u.get_OAI_ProvidersHandler(path);
		response.sendRedirect("stats.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

}
