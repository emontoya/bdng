package org.bdng.stats.dataprovider;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.bdng.stats.common.CommonStatsController;

/**
 * This class defines the stats for one provider and send links to the page.
 * 
 */
public class ProviderStatsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * Default constructor.
	 */
	public ProviderStatsServlet() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String dataProvider = request.getParameter("dataprovider");
		if (dataProvider == null) {
			return;
		} else {

		}
		// init the controller
		ProviderStatsController controller = new ProviderStatsController(
				dataProvider, getServletContext().getRealPath(""));
		/* generate charts */
		controller.generateCharts();
		/* redirect */
		response.sendRedirect("providerStats.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

	}

}
