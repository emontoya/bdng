package org.bdng.stats.dataprovider;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.bdng.stats.Util;
import org.bdng.stats.record.ProviderStatRecord;
import org.bdng.stats.record.RecordUtil;

public class ProviderTableServlet extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String deployPath = getServletContext().getRealPath("");
		String dataProviderParam = request.getParameter("dataprovider");
		String[] dataProviderNames = new String[] { dataProviderParam };

		// Por ahora solo uno
		List<ProviderStatRecord> list = null;

		for (String dataProvider : dataProviderNames) {
			list = RecordUtil.generateProviderStatListFromXml(Util
					.getStatsPath(deployPath)
					+ "/stats/"
					+ dataProvider
					+ ".xml");
			request.setAttribute("table", list);
		}

		request.setAttribute("dataProviderName", dataProviderParam);
		RequestDispatcher rd = getServletContext().getRequestDispatcher(
				"/report.jsp");
		rd.forward(request, response);

		// response.sendRedirect("report.jsp");
	}
}