package org.bdng.stats.chartgenerator;

import java.awt.Color;
import java.util.Map;

import org.bdng.stats.ChartPositionEnum;

/**
 * This class allows to create one single image using google chart. each public
 * method of this class represents one possible diagram that can be generated.
 * 
 * @author sebastian
 * 
 */
public class GoogleChartUrlGenerator {

	/**
	 * Defines the base url of google chart.
	 */
	private static final String GOOGLE_CHART_BASE_URL = "http://chart.apis.google.com/chart?";

	private static final ChartPositionEnum DEFAULT_LEYEND_POSITION = ChartPositionEnum.LEFT;

	private static final Color[] DEFAULT_COLOR_ORDER = { Color.BLUE, Color.RED,
			Color.GREEN, Color.ORANGE, Color.CYAN, Color.GRAY };
	/**
	 * Constants that defines default size.
	 */
	public static final String CHART_NORMAL_SIZE = "400x325";
	public static final String CHART_BIG_SIZE = "600x425";

	/**
	 * Generates a google chart link to a line chart with the given parameters.
	 * 
	 * @param size
	 *            size of the chart. compulsory.
	 * @param lineTitles
	 *            titles of each of the lines.
	 * @param colors
	 *            colors of the lines.
	 * @param chartTitle
	 *            chart title
	 * @param yEnumeration
	 *            if true, a set of numbers giving orientation of the chart will
	 *            appear in vertical way.
	 * @param xEnumeration
	 *            if true, a set of numbers giving orientation of the chart will
	 *            appear in horizontal way.
	 * @param leyendPosition
	 *            position of the leyends.
	 * @param dataArray
	 *            array with the data. eah row represents a set of data. It is
	 *            possible to have different sizes.
	 * @return string with the complete url of the line chart.
	 */
	public static String lineChartDiagram(String size, String[] lineTitles,
			Color[] colors, String chartTitle, Boolean yEnumeration,
			Boolean xEnumeration, ChartPositionEnum leyendPosition,
			Double[][] dataArray, Map<String, String> pDataPointsAndAxisX) {
		StringBuffer url = new StringBuffer();
		url.append(generateBasicChartUrl("lc", size));
		url.append("&chdl=");
		String titlesString = "";
		for (String title : lineTitles) {
			titlesString += title + "|";
		}
		url.append(titlesString.substring(0, titlesString.length() - 1)); // remove
																			// the
																			// last
																			// "|"
		if (colors == null) {
			colors = getDefaultColors(lineTitles.length);
		}
		url.append("&chco="
				+ generateColorHexadecimalRepresentation(colors, ','));
		url.append("&chtt=" + chartTitle);
		url.append("&chdlp=l");
		url.append("&chls=1|1");
		// prints the enumerations
		if (yEnumeration || xEnumeration) {
			url.append("&chxt=");
			if (yEnumeration) {
				url.append("y");
				if (xEnumeration) {
					url.append(",");
				}
			}
			if (xEnumeration) {
				url.append("x");
			}
		}
		// defines the leyend position
		if (leyendPosition == null) {
			leyendPosition = DEFAULT_LEYEND_POSITION;
		}
		url.append("&chdlp=");
		String position = "";
		switch (leyendPosition) {
		case BOTTOM:
			position = "b";
			break;
		case TOP:
			position = "t";
			break;
		case LEFT:
			position = "l";
			break;
		case RIGHT:
			position = "r";
			break;
		default:
			// this never will happen;
			throw new RuntimeException("problem getting leyend position");
		}
		url.append(position);
		// defines the data
		url.append("&chd=t:");
		// iterates over the array in order to set the data of the line.
		String data = "";
		Double maxValue = -1.0;
		for (Double[] dataLine : dataArray) {
			for (Double cell : dataLine) {
				if (cell == null) {
					// means there is no more registers for this row.
					break;
				}
				data += String.valueOf(cell) + ",";
				if (maxValue < cell) {
					maxValue = cell;
				}
			}
			data = data.substring(0, data.length() - 1);
			data += "|";
		}
		url.append(data.substring(0, data.length() - 1));
		/* custom ranges */
		String ranges = "";
		url.append("&chds=");
		for (int i = 0; i < dataArray.length; i++) {
			ranges += "0," + String.valueOf(maxValue) + ",";
		}
		url.append(ranges.substring(0, ranges.length() - 1));
		// range of axes
		url.append("&chxr=0,0," + String.valueOf(maxValue));
		//

		if ((pDataPointsAndAxisX != null) && (pDataPointsAndAxisX.size() > 0)) {
			String sMonthsAxisX = pDataPointsAndAxisX.get("chl");
			String sPoints = pDataPointsAndAxisX.get("chm");

			if ((sMonthsAxisX != null) && (sMonthsAxisX.trim().length() > 0)) {
				if (sMonthsAxisX.trim().length() < 4)
					url.append("&chl=" + sMonthsAxisX);
			}

			if ((sPoints != null) && (sPoints.trim().length() > 0)) {
				url.append("&chm=" + sPoints);
			}
		}

		return url.toString();
	}

	/**
	 * Generates a google chart link to a pie chart with the given parameters.
	 * 
	 * @param size
	 *            size of the chart. compulsory.
	 * @param dataLeyend
	 *            titles of each of the options.
	 * @param colors
	 *            colors of the lines.
	 * @param chartTitle
	 *            chart title
	 * @param leyendPosition
	 *            position of the leyends.
	 * @param dataArray
	 *            array with the data. each row represents a
	 * @return string with the complete url of the line chart.
	 */
	public static String pieChartDiagram(String size, String[] dataLeyend,
			String[] dataTitle, Color colors, String chartTitle,
			ChartPositionEnum leyendPosition, Double[] dataArray) {
		StringBuffer url = new StringBuffer();
		url.append(generateBasicChartUrl("p3", size));
		url.append("&chdl=");
		String titlesString = "";
		for (String title : dataLeyend) {
			titlesString += title + "|";
		}
		url.append(titlesString.substring(0, titlesString.length() - 1)); // remove
																			// the
																			// last
																			// "|"
		if (colors != null) {
			url.append("&chco="
					+ generateColorHexadecimalRepresentation(
							new Color[] { colors }, ','));
		}
		url.append("&chtt=" + chartTitle);
		url.append("&chdlp=l");
		url.append("&chma=42,15,15,15");
		// defines the leyend position
		if (leyendPosition == null) {
			leyendPosition = DEFAULT_LEYEND_POSITION;
		}
		url.append("&chdlp=");
		String position = "";
		switch (leyendPosition) {
		case BOTTOM:
			position = "b";
			break;
		case TOP:
			position = "t";
			break;
		case LEFT:
			position = "l";
			break;
		case RIGHT:
			position = "r";
			break;
		default:
			// this never will happen;
			throw new RuntimeException("problem getting leyend position");
		}
		url.append(position);
		// &chd=t:40,60&chl=40%25|60%25
		// defines the data
		url.append("&chd=t:");
		// iterates over the array in order to set the data of the line.
		String data = "";
		for (Double dataUnit : dataArray) {
			data += String.valueOf(dataUnit) + ",";
		}
		url.append(data.substring(0, data.length() - 1));
		// data titles
		url.append("&chl=");
		String dataTitles = "";
		for (String dataUnit : dataTitle) {
			dataTitles += String.valueOf(dataUnit) + "|";
		}
		url.append(dataTitles.substring(0, dataTitles.length() - 1));
		return url.toString();
	}

	/**
	 * Generate a base chart url that all the charts may include.
	 * 
	 * @param type
	 *            type of chart to generate.
	 * @param size
	 *            size of the chart.
	 * @return base url.
	 */
	private static String generateBasicChartUrl(String type, String size) {
		return GOOGLE_CHART_BASE_URL + "cht=" + type + "&chs=" + size;
	}

	/**
	 * This method generates an hexadecimal representation of a list of colors
	 * in the order they come in the array, and generates and string separating
	 * the codes with the given split character.
	 * 
	 * @param colors
	 *            colors to generate
	 * @param splitCharacter
	 *            character to separate the colors.
	 * @return String with the code.
	 */
	private static String generateColorHexadecimalRepresentation(
			Color[] colors, Character splitCharacter) {
		String result = "";
		for (Color color : colors) {
			result += ((Integer.toHexString(color.getRed()).length() < 2) ? ("0" + Integer
					.toHexString(color.getRed())) : Integer.toHexString(color
					.getRed()));
			result += (Integer.toHexString(color.getGreen()).length() < 2 ? "0"
					+ Integer.toHexString(color.getGreen()) : Integer
					.toHexString(color.getGreen()));
			result += (Integer.toHexString(color.getBlue()).length() < 2 ? "0"
					+ Integer.toHexString(color.getBlue()) : Integer
					.toHexString(color.getBlue()));
			result += splitCharacter;
		}
		return result.substring(0, result.length() - 1);
	}

	/**
	 * Gets an array of colors with the default ones.
	 * 
	 * @param size
	 *            size of the array to generate.
	 * @return colors.
	 */
	private static Color[] getDefaultColors(int size) {
		Color[] response = new Color[size];
		for (int i = 0; i < size; i++) {
			response[i] = DEFAULT_COLOR_ORDER[i];
		}
		return response;
	}

}
