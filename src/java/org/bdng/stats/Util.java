package org.bdng.stats;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.bdng.harvmgr.harvester.DataProvider;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;

/**
 * Maneja utilidades.
 * 
 * @author sebastian
 * 
 */
public class Util {

	public static String getStatsPath(String deployPath) {
		SAXBuilder sb = new SAXBuilder(false);
		Document document = null;
		try {
			document = sb.build(deployPath + "/conf/statsConfig.xml");
		} catch (Exception e) {
			throw new RuntimeException("no config file!");
		}
		return document.getRootElement().getChild("path").getText();
	}

	public ArrayList<HashMap<String, String>> get_OAI_Providers(
			String harvesterPath) {
		ArrayList<HashMap<String, String>> response = new ArrayList<HashMap<String, String>>();
		SAXBuilder sb = new SAXBuilder(false);
		Document doc = null;
		try {
			doc = sb.build(harvesterPath);
		} catch (Exception e) {
			return new ArrayList<HashMap<String, String>>();
		}
		Element raiz = doc.getRootElement();
		Element providers = (Element) raiz.getChild(DataProvider.OAI_PROVIDERS);
		List provider = providers.getChildren(DataProvider.OAI_PROVIDER);
		Iterator i = provider.iterator();
		while (i.hasNext()) {
			// dataProvider.setRuta(path);
			Element l = (Element) i.next();
			Element e = (Element) l.getChild(DataProvider.SHORTNAME);

			HashMap<String, String> data = new HashMap<String, String>();
			data.put("shortName", e.getText());

			e = (Element) l.getChild(DataProvider.INSTITUTION);
			data.put("institution", e.getText());

			e = (Element) l.getChild(DataProvider.REPOSITORY_NAME);
			data.put("repository", e.getText());

			response.add(data);
		}

		return response;

	}

	public ArrayList<HashMap<String, String>> get_OAI_ProvidersHandler(
			String deployPath) {

		String sPathApp = Util.getStatsPath(deployPath);
		sPathApp = sPathApp + "/conf/oai_providers.xml";
		return this.get_OAI_Providers(sPathApp);
	}

	public static Map<String, String> getDataDates(List<List<Date>> pResultDates) {
		// Por Meses
		int sizeRepo = pResultDates.size();
		// Es porque son los datos de las fechas de todos los Repositorios.
		Map<String, String> dataPointsAndAxisX = new HashMap<String, String>();

		String sOut = "";
		String sOutPoint = "";
		String sDateFormat = "dd/MM/yyyy";
		SimpleDateFormat dayMonthYear = new SimpleDateFormat(sDateFormat);
		// String sMonth = "MMMM";
		String sMonth = "d MMM,yyyy";
		SimpleDateFormat monthYear = new SimpleDateFormat(sMonth);

		boolean isFirst = true;
		int i = 0;
		for (List<Date> list : pResultDates) {
			for (Date dateTem : list) {
				isFirst = false;
				sOut += monthYear.format(dateTem) + "|";
				isFirst = false;
				sOutPoint += "d,4d89f9,0," + i + ",12,0" + "|";
				i++;
			}
		}
		if (!isFirst) {
			sOut = sOut.substring(0, sOut.length() - 1);
			sOutPoint = sOutPoint.substring(0, sOutPoint.length() - 1);
		}

		dataPointsAndAxisX.put("chl", sOut);
		dataPointsAndAxisX.put("chm", sOutPoint);
		return dataPointsAndAxisX;
	}

	public static void main(String[] args) {
		Util u = new Util();
		ArrayList<HashMap<String, String>> h = u
				.get_OAI_Providers("/testing/bdcol/conf/oai_providers.xml");
		for (HashMap<String, String> hashMap : h) {
			String ss = hashMap.get("shortName");
			String in = hashMap.get("institution");
			System.out.println("shortName=" + ss + " institution" + in);
		}

		String s1 = "26/05/2011";
		String s2 = "07/06/2011";
		ArrayList<Date> datesCol = new ArrayList<Date>();
		String strDateFormat = "dd/MM/yyyy";
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(strDateFormat);
			Date date = sdf.parse(s1);
			datesCol.add(date);
			date = sdf.parse(s2);
			datesCol.add(date);
			List<List<Date>> resultDates = new ArrayList<List<Date>>();
			resultDates.add(datesCol);

			Map<String, String> pDataPointsAndAxisX = getDataDates(resultDates);
			String sMonthsAxisX = pDataPointsAndAxisX.get("chl");
			String sPoints = pDataPointsAndAxisX.get("chm");
			System.out.println("Util.main(Pu)" + sMonthsAxisX);
			System.out.println("Util.main(Pu)" + sPoints);

			// // Jan|Feb|March
			// String sOut = "";
			// boolean isFirst = true;
			// for (String s : datesCol) {
			// SimpleDateFormat sdf = new SimpleDateFormat(strDateFormat);
			// Date date = sdf.parse(s);
			// // System.out.println("Util.main() " + s);
			// // System.out.println("Month Name :"
			// // + new SimpleDateFormat("MMMM").format(date));
			// sOut += new SimpleDateFormat("MMMM").format(date) + "|";
			// isFirst = false;
			// }
			// if (!isFirst) {
			// System.out.println("Util.main(1 2 3 4 5 6 7 8 9) " + sOut);
			// sOut = sOut.substring(0, sOut.length() - 1);
			// System.out.println("Util.main(1 2 3 4 5 6 7 8) " + sOut);
			// }
			// System.out.println("Util.main(1234567890)" + datesCol.size());
			// System.out.println("Util.main(1234567890)" + datesCol.get(0));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
