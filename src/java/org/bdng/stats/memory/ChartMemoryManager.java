package org.bdng.stats.memory;

import java.util.HashMap;
import java.util.List;

import org.bdng.stats.exception.StatsException;

/**
 * Manages the memory of the charts generated. When we talk about memory, it is
 * referred to the lists of urls of a data provider. Each of the urls represents
 * one chart. This class, provides methods to access to urls, store them, etc.
 * 
 * @author sebastian
 * 
 */
public class ChartMemoryManager {

	/**
	 * The memory of charts its represented by a hashmap. The key is the data
	 * provider, and the data is a list of urls.
	 */
	private HashMap<String, List<String>> memoryHashMap;

	/**
	 * Last data provider updated.
	 */
	private String currentDataProvider;

	/**
	 * Singleton object.
	 */
	private static ChartMemoryManager chartMemoryManager;

	/**
	 * private constructor.
	 */
	private ChartMemoryManager() {
		memoryHashMap = new HashMap<String, List<String>>();
		currentDataProvider = "";
	}

	/**
	 * Singleton method. Provides the instance of the memory manager
	 * 
	 * @return memory manager instance.
	 */
	public static ChartMemoryManager getInstance() {
		if (chartMemoryManager == null) {
			chartMemoryManager = new ChartMemoryManager();
		}
		return chartMemoryManager;
	}

	/**
	 * Returns the url of a chart. The data provider is the current one, and the
	 * position in the list of charts is given as a parameter.
	 * 
	 * @param position
	 *            position of the chart in the array.
	 * @throws StatsException
	 *             in case of error when looking for the url.
	 * @return url of the chart
	 */
	public String getChartURL(int position) throws StatsException {
		List<String> list = memoryHashMap.get(currentDataProvider);
		if (list == null) {
			throw new StatsException("data provider url list not founded.");
		}
		if (list.size() <= position) {
			throw new StatsException("position not founded.");
		}
		return list.get(position);
	}

	/**
	 * Returns the url of a chart. The data provider and the position in the
	 * list of charts are given as parameters.
	 * 
	 * @param position
	 *            position of the chart in the array.
	 * @param dataProvider
	 *            data provider to look the url.
	 * @throws StatsException
	 *             in case of error when looking for the url.
	 * @return url of the chart
	 */
	public String getChartURL(int position, String dataProvider)
			throws StatsException {
		List<String> list = memoryHashMap.get(dataProvider);
		if (list == null) {
			throw new StatsException("data provider url list not founded.");
		}
		if (list.size() <= position) {
			throw new StatsException("position not founded.");
		}
		return list.get(position);
	}

	/**
	 * Puts a value in memory.
	 * 
	 * @param dataProvider
	 *            dataProvider.
	 * @param urlList
	 *            list of chart's urls.
	 */
	public void putChartList(String dataProvider, List<String> urlList) {
		memoryHashMap.put(dataProvider, urlList);
		currentDataProvider = dataProvider;
	}
}
