package org.bdng.stats.common;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This servlet generates some common charts for all the data providers, like
 * comparisons between all the number of registers for example.
 * 
 * @author sebastian
 * 
 */
public class CommonStatsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CommonStatsServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO! IF THERE ARE PARAMETERS, THEY COME HERE!
		// init the controller
		CommonStatsController controller = new CommonStatsController(
				getServletContext().getRealPath(""));
		/* generate charts */
		controller.generateCharts();
		/* redirect */
		response.sendRedirect("commonStats.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

	}

}
