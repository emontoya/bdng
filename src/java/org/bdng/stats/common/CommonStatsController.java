package org.bdng.stats.common;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bdng.stats.ChartPositionEnum;
import org.bdng.stats.Util;
import org.bdng.stats.chartgenerator.GoogleChartUrlGenerator;
import org.bdng.stats.memory.ChartMemoryManager;
import org.bdng.stats.record.ProviderStatRecord;
import org.bdng.stats.record.RecordUtil;

/**
 * This class controls the generation of charts for common properties.
 * @author sebastian
 *
 */
/**
 * @author sebastian
 * 
 */
public class CommonStatsController {

	String deployPath;

	public CommonStatsController(String deployPath) {
		this.deployPath = deployPath;
	}

	/**
	 * Generate the charts of common properties and puts the answer in memory.
	 */
	public void generateCharts() {

		List<String> urlList = new ArrayList<String>();

		/* Generates a line chart */
		String[] dataProviderNames = generateNamesOfDataProviders();
		// Generate Values line Y and X.
		Map<String, Object> paintMap = this
				.generateDataForLineChart(dataProviderNames);

		Double[][] dataArray = (Double[][]) paintMap.get("DataaAxisY");
		List<List<Date>> resultDates = (List<List<Date>>) paintMap
				.get("DataaAxisX");

		Map<String, String> dataPointsAndAxisX = Util.getDataDates(resultDates);

		String urlLineChart = GoogleChartUrlGenerator.lineChartDiagram(
				"500x250", dataProviderNames, null,
				"Comparación de registros recolectados", Boolean.TRUE,
				Boolean.FALSE, ChartPositionEnum.LEFT, dataArray,
				dataPointsAndAxisX);

		urlList.add(urlLineChart);

		/* Generates a pie chart */
		Double[] dataOfPieChart = generateDataForPieChartOfSuccessful(dataProviderNames);
		String[] titlesForPieChart = generateTitleForPieChartOfSuccessful(dataOfPieChart);
		String urlPieChart = GoogleChartUrlGenerator.pieChartDiagram("440x220",
				new String[] { "Fracasos", "Éxitos" }, titlesForPieChart, null,
				"Porcentaje de recolecciones exitosas", null, dataOfPieChart);
		urlList.add(urlPieChart);
		/* put the answer in memory. */
		ChartMemoryManager.getInstance().putChartList("common", urlList);
	}

	@SuppressWarnings("unchecked")
	// private Double[][] generateDataForLineChart(String[] dataProviderNames) {
	private Map<String, Object> generateDataForLineChart(
			String[] dataProviderNames) {
		/*
		 * Because we dont know in this moment which is the data provider with
		 * more results, we have to use a list of list, and after reading the
		 * xml, we can convert it.
		 */
		Map<String, Object> resultsNumbersAndDate = new HashMap<String, Object>();

		List<List<Double>> result = new ArrayList<List<Double>>();
		List<List<Date>> resultDates = new ArrayList<List<Date>>();

		for (String dataProvider : dataProviderNames) {
			List<Double> row = new ArrayList<Double>();
			List<Date> rowDate = new ArrayList<Date>();
			List<ProviderStatRecord> list = RecordUtil
					.generateProviderStatListFromXml(Util
							.getStatsPath(deployPath)
							+ "/stats/"
							+ dataProvider + ".xml");
			for (ProviderStatRecord record : list) {
				row.add(Double.parseDouble(String.valueOf(record
						.getNumberOfRegisters())));
				rowDate.add(record.getDate());
			}
			result.add(row);
			resultDates.add(rowDate);

		}
		@SuppressWarnings("unchecked")
		/* converting the list to an array */
		int maxLength = -1;
		for (List<Double> row : result) {
			if (maxLength < row.size()) {
				maxLength = row.size();
			}
		}
		Double[][] arrayResponse = new Double[result.size()][maxLength];
		for (int i = 0; i < result.size(); i++) {
			List<Double> row = result.get(i);
			for (int j = 0; j < row.size(); j++) {
				arrayResponse[i][j] = row.get(j);
			}
		}
		resultsNumbersAndDate.put("DataaAxisY", arrayResponse);
		resultsNumbersAndDate.put("DataaAxisX", resultDates);
		return resultsNumbersAndDate;
		// return arrayResponse;
	}

	/**
	 * Gets the data of successful for all providers.
	 * 
	 * @param dataProviderNames
	 *            names of the data providers to look.
	 * @return double stats
	 */
	private Double[] generateDataForPieChartOfSuccessful(
			String[] dataProviderNames) {

		/*
		 * Because we dont know in this moment which is the data provider with
		 * more results, we have to use a list of list, and after reading the
		 * xml, we can convert it.
		 */
		List<List<Double>> result = new ArrayList<List<Double>>();
		int successful = 0;
		int nonSuccessful = 0;
		for (String dataProvider : dataProviderNames) {
			List<ProviderStatRecord> list = RecordUtil
					.generateProviderStatListFromXml(Util
							.getStatsPath(deployPath)
							+ "/stats/"
							+ dataProvider + ".xml");
			for (ProviderStatRecord record : list) {
				if (record.getSuccessful()) {
					successful++;
				} else {
					nonSuccessful++;
				}
			}
		}
		double successfulPercentage = ((0.0 + successful) / (successful + nonSuccessful)) * 100.0;
		double nonSuccessfulPercentage = ((0.0 + nonSuccessful) / (successful + nonSuccessful)) * 100.0;
		return new Double[] { successfulPercentage, nonSuccessfulPercentage };
	}

	/**
	 * gets the name of all the data providers looking in oai_providers.xml.
	 * 
	 * @return list of data providers.
	 */
	private String[] generateNamesOfDataProviders() {
		// TODO Esta vaina esta incompleta!
		return new String[] { "usalle", "unal", "urosario" };

	}

	/**
	 * Gets the titles for the successful pie chart.
	 * 
	 * @param dataOfPieChart
	 *            doubles.
	 * @return titles
	 */
	private String[] generateTitleForPieChartOfSuccessful(
			Double[] dataOfPieChart) {
		String[] result = new String[dataOfPieChart.length];
		int i = 0;
		NumberFormat nf = NumberFormat.getInstance();
		nf.setMaximumFractionDigits(2);
		for (Double d : dataOfPieChart) {
			result[i] = nf.format(d) + "%";
			i++;
		}
		return result;
	}
}
