package org.bdng.stats.exception;

/**
 * Exception class.
 * 
 * @author sebastian
 * 
 */
public class StatsException extends Exception {

	public StatsException() {
		super();
	}

	public StatsException(String message, Throwable cause) {
		super(message, cause);
	}

	public StatsException(String message) {
		super(message);
	}

	public StatsException(Throwable cause) {
		super(cause);
	}
}