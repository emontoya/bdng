package org.bdng.stats.record;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;

/**
 * Class of utilities that allows to work with records stored (by harvester and
 * by provider).
 * 
 * @author sebastian
 * 
 */
public class RecordUtil {

	private static SimpleDateFormat dateformatter = new SimpleDateFormat(
			"dd/MM/yyyy");

	/**
	 * Generate a list of records for provider from a file, using jdom.
	 * 
	 * @param path
	 *            path where the xml file is located.
	 * @return list of records.
	 */
	public static List<ProviderStatRecord> generateProviderStatListFromXml(
			String path) {
		List<ProviderStatRecord> response = new ArrayList<ProviderStatRecord>();
		SAXBuilder sb = new SAXBuilder(false);
		Document document = null;
		try {
			document = sb.build(path);
		} catch (Exception e) {
			return new ArrayList<ProviderStatRecord>();
		}
		Element root = document.getRootElement().getChild("stats");
		for (Element harvesterRecord : (List<Element>) root
				.getChildren("harvester")) {
			ProviderStatRecord record = new ProviderStatRecord();
			Element date = harvesterRecord.getChild("date");
			try {
				record.setDate(dateformatter.parse(date.getText()));
			} catch (ParseException e) {
				throw new RuntimeException("invalid date format");
			}

			record.setHarvesterTime(Long.valueOf(harvesterRecord.getChild(
					"harvesterTime").getText()));
			record.setNumberOfFiles(Integer.valueOf(harvesterRecord.getChild(
					"number_files").getText()));
			record.setNumberOfRegisters(Integer.valueOf(harvesterRecord
					.getChild("number_registers").getText()));
			record.setSuccessful(Boolean.valueOf(harvesterRecord.getChild(
					"successfull").getText()));
			response.add(record);
		}
		return response;
	}

}
