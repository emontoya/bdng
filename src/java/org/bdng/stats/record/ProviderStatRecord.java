package org.bdng.stats.record;

import java.util.Date;

/**
 * Entity class that represents a record of a provider recolection.
 * 
 * @author sebastian
 * 
 */
public class ProviderStatRecord {

	private Date date;
	private Boolean successful;
	private int numberOfFiles;
	private int numberOfRegisters;
	private long harvesterTime;

	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * @param date
	 *            the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * @return the successful
	 */
	public Boolean getSuccessful() {
		return successful;
	}

	/**
	 * @param successful
	 *            the successful to set
	 */
	public void setSuccessful(Boolean successful) {
		this.successful = successful;
	}

	/**
	 * @return the numberOfFiles
	 */
	public int getNumberOfFiles() {
		return numberOfFiles;
	}

	/**
	 * @param numberOfFiles
	 *            the numberOfFiles to set
	 */
	public void setNumberOfFiles(int numberOfFiles) {
		this.numberOfFiles = numberOfFiles;
	}

	/**
	 * @return the numberOfRegisters
	 */
	public int getNumberOfRegisters() {
		return numberOfRegisters;
	}

	/**
	 * @param numberOfRegisters
	 *            the numberOfRegisters to set
	 */
	public void setNumberOfRegisters(int numberOfRegisters) {
		this.numberOfRegisters = numberOfRegisters;
	}

	/**
	 * @return the harvesterTime
	 */
	public long getHarvesterTime() {
		return harvesterTime;
	}

	/**
	 * @param harvesterTime
	 *            the harvesterTime to set
	 */
	public void setHarvesterTime(long harvesterTime) {
		this.harvesterTime = harvesterTime;
	}

}
