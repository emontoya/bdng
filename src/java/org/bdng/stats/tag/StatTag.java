package org.bdng.stats.tag;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import org.bdng.stats.exception.StatsException;
import org.bdng.stats.memory.ChartMemoryManager;

/**
 * Tag that allows to print a stat on the page.
 * 
 * @author sebastian
 * 
 */
public class StatTag extends TagSupport {

	public static final String ERROR_IMAGE_PATH = "images/error.png";

	/**
	 * data provider owner of the stats to get.
	 */
	private String dataProvider;

	/**
	 * current position in the array of urls.
	 */
	private int position;

	/**
	 * Id for serialization.
	 */
	private static final long serialVersionUID = 742679988728413173L;

	public int doStartTag() throws JspException {
		String url = "";
		if (dataProvider == null) {
			try {
				url = ChartMemoryManager.getInstance().getChartURL(position);
			} catch (StatsException e) {
				url = ERROR_IMAGE_PATH;
			}
		} else {
			try {
				url = ChartMemoryManager.getInstance().getChartURL(position,
						dataProvider);
			} catch (StatsException e) {
				url = ERROR_IMAGE_PATH;
			}
		}
		try {
			pageContext.getOut().print("<img src='" + url + "'/>");
		} catch (IOException e) {
			throw new JspException("Error: IOException" + e.getMessage());
		}
		return SKIP_BODY;
	}

	public int doEndTag() throws JspException {
		return EVAL_PAGE;
	}

	/**
	 * sets the data provider.
	 * 
	 * @param dataProvider
	 *            dataprovider to set.
	 */
	public void setDataProvider(String dataProvider) {
		this.dataProvider = dataProvider;
	}

	/**
	 * sets the position.
	 * 
	 * @param dataProvider
	 *            position to set.
	 */
	public void setPosition(int position) {
		this.position = position;
	}
}
