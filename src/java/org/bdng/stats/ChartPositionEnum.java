package org.bdng.stats;

/**
 * Defines the possible positions of a component in the screen.
 * 
 * @author sebastian
 * 
 */
public enum ChartPositionEnum {

	BOTTOM, TOP, LEFT, RIGHT;
}
