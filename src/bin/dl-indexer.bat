rem @echo off
set DL_HOME=%{INSTALL_PATH}
set EXIST_HOME=%DL_HOME%\eXist
set LIB=%EXIST_HOME%\lib\core
set CLASSPATH=.;%DL_HOME%\lib\bdng-2.0.jar;%DL_HOME%\lib\log4j-1.2.17.jar;%EXIST_HOME%\exist.jar;%LIB%\commons-collections-3.2.1.jar;%LIB%\commons-logging-1.1.1.jar;%LIB%\ws-commons-util-1.0.2.jar;%LIB%\xmldb.jar;%LIB%\xmlrpc-common-3.1.3.jar;%LIB%\xmlrpc-client-3.1.3.jar;%LIB%\commons-io-2.4.jar
java -cp %CLASSPATH% -Dfile.encoding=UTF-8 -Xms128m -Xmx512m org.bdng.harvmgr.indexer.Indexer %DL_HOME% %1
