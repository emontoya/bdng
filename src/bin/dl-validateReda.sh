DL_HOME=%{INSTALL_PATH}
EXIST_HOME=$DL_HOME/eXist
#echo $EXIST_HOME
LIB=$EXIST_HOME/lib/core
CLASSPATH=".:$DL_HOME/lib/jdom-2.0.4.jar:$DL_HOME/lib/htmlparser.jar:$DL_HOME/lib/bdng-2.0.jar:$DL_HOME/lib/log4j-1.2.17.jar"
java -cp $CLASSPATH -Dfile.encoding=UTF-8 -Xms512m -Xmx1024m org.bdng.harvmgr.validator.ValidadorReda $DL_HOME $1
