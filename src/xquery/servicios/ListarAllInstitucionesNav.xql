xquery version "3.0";

declare namespace request="http://exist-db.org/xquery/request";

declare namespace dc = "http://purl.org/dc/elements/1.1/";
declare namespace dmp = "http://reda.net.co/doer/ns/1.0/";

declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization";
declare namespace json="http://www.json.org";

declare option output:method "json";
declare option output:media-type "text/javascript";


declare function local:listarInstituciones() as element()*{
    let $data-collection := '/db/bdcol'
    let $hits :=
		for $i in distinct-values(collection($data-collection)//dc:record/dc:institution)
        return
            <institucion>
                <name>{$i}</name>
                <entity>
                {
                    for $m in distinct-values(collection($data-collection)//dc:record[dc:institution=$i]/dmp:doer/dmp:rights/dmp:copyrightStackHolder/dmp:entity/@entityForm)
                    return $m
                }
                </entity>
            </institucion>
        
    return
         <result>
            {
                $hits
            }
        </result>
};

local:listarInstituciones()