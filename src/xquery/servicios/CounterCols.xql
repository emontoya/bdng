xquery version "3.0";
declare namespace oai_lom="http://www.openarchives.org/OAI/2.0/oai_lom/";
declare namespace request="http://exist-db.org/xquery/request";
declare namespace lom="http://purl.org/lom/elements/1.1/";
declare namespace dc="http://purl.org/dc/elements/1.1/";
import module namespace t="http://exist-db.org/xquery/text";
import module namespace ft="http://exist-db.org/xquery/lucene";


declare function local:TotalInst($collection as xs:string) as element()+ {
for $inst in xmldb:get-child-collections($collection)
return (
<institution>
    <name>{$inst}</name>
    <size>{count(collection(concat($collection,"/",$inst))//dc:record)}</size>
    <!--collections>
            {local:TotalCollectionByInst($collection, $inst)}
    </collections--> 
</institution>,
for $child in xmldb:get-child-collections(concat($collection,"/",$inst))
order by $child 
return
local:TotalRep(concat($collection,"/",$inst,"/",$child),$inst, $child)
)
};

declare function local:TotalRep($collection as xs:string, $inst as xs:string, $rep as xs:string) as element()+ {
let $displayName := $collection
return (
<repname>
    <inst>{$inst}</inst>
    <name>{$rep}</name>
    <size>{count(collection($collection)//dc:record)}</size>
</repname>
)
};

declare function local:TotalCollection($dl_home as xs:string) as element()* {
    for $col in distinct-values(collection($dl_home)//dc:collection)
    return 
        <collection>
            <name>{$col}</name>
            <size>{count(collection($dl_home)//dc:collection[.=$col])}</size>
        </collection> 
};

declare function local:TotalCollectionByInst($dl_home as xs:string, $inst as xs:string) as element()* {
    for $col in distinct-values(collection(concat($dl_home,"/",$inst))//dc:collection)
    return 
        <collection>
            <name>{$col}</name>
            <size>{count(collection(concat($dl_home,"/",$inst))//dc:collection[.=$col])}</size>
        </collection> 
};

declare function local:store($dl_home as xs:string) as element()+ {
<collections>
<collection>
	<name>todos</name>
    <size>{count(collection($dl_home)//dc:record)}</size>
</collection>
    {local:TotalCollection($dl_home)}
    {local:TotalInst($dl_home)}
</collections>
};

let $dl_home := request:get-parameter("dl_home", ())
let $username := request:get-parameter("username", ())
let $password := request:get-parameter("password", ())
return
	(if (xmldb:collection-available("/db/stats") eq fn:false()) then
		system:as-user($username, $password, xmldb:create-collection("/db", "stats"))
	else 
		(),
	system:as-user($username, $password, xmldb:store("/db/stats","counter_cols.xml",local:store($dl_home)))
	)
