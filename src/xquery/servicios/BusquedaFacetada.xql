xquery version "3.0";

module namespace salidaFacetada="http://exist-db.org/modules/salidaFacetada";

declare namespace request="http://exist-db.org/xquery/request";
declare namespace session="http://exist-db.org/xquery/session";

declare namespace dc = "http://purl.org/dc/elements/1.1/";
declare namespace dmp = "http://reda.net.co/doer/ns/1.0/";

declare namespace functx = "http://www.functx.com";

declare function functx:capitalize-first ( $arg as xs:string? )  as xs:string? {
    concat(upper-case(substring($arg,1,1)),
    substring($arg,2))
};

declare function salidaFacetada:term-callback($term as xs:string, $data as xs:int+) as element() {
  <term freq="{$data[1]}" docs="{$data[2]}" n="{$data[3]}">{$term}</term>
};

(: Funcion utilizada para sacar las opciones de facetacion de un conjunto de resultados de una busqueda, ya sea basica o avanzada :)
declare function salidaFacetada:facetada($hits as element()*) as element()* {
    let $callback := util:function(xs:QName("salidaFacetada:term-callback"), 2)
    (: Si se desea agregar una nueva opcion de facetacion se debe de agregar en esta variable un nuevo elemento facet y ubicar el nodo deseado :)
    let $facets := 
        <facets>
            <facet label="general.language">$hits//dmp:general/dmp:language</facet>
            <facet label="general.titlePack.keyword.li">$hits//dmp:general/dmp:titlePack/dmp:keyword/dmp:li</facet>
            <facet label="lifeCycle.controlVersion.contribute.entity">$hits//dmp:controlVersion/dmp:contribute[dmp:role='AU']/dmp:entity</facet>
            <facet label="educational.resourceType.@main">$hits//dmp:resourceType/@main</facet>
            <facet label="rights.copyrightStackHolder.@year">$hits//dmp:copyrightStackHolder/@year</facet>
            <facet label="rights.copyrightStackHolder.entity">$hits//dmp:copyrightStackHolder/dmp:entity[@type='ORG']</facet>
            <facet label="general.coverage.hierarchical.@continent">$hits//dmp:general/dmp:coverage/dmp:hierarchical/@continent</facet>
            <facet label="general.coverage.hierarchical.@state">$hits//dmp:general/dmp:coverage/dmp:hierarchical/@state</facet>
            <facet label="general.coverage.hierarchical.@city">$hits//dmp:general/dmp:coverage/dmp:hierarchical/@city</facet>
            <facet label="technical.properties.file.@extension">$hits//dmp:technical/dmp:properties[1]/dmp:file/@extension</facet>
            <facet label="educational.eduProperties.specificContext.@hsKnowAreas">$hits//dmp:educational/dmp:eduProperties/dmp:specificContext/@hsKnowAreas</facet>
            <facet label="educational.eduProperties.specificContext.@hsBasicCore">$hits//dmp:educational/dmp:eduProperties/dmp:specificContext/@hsBasicCore</facet>
        </facets>
    return
    
        for $facet in $facets//facet 
        let $vals := util:eval($facet) 
        return 
            <facet>
            {
                $facet/@label,
                util:index-keys($vals, '', $callback, 10000)
            }
            </facet>
    
};

declare function salidaFacetada:refineBusqueda($query as xs:string*,$data-collection as xs:string*,$refineData as xs:string) as element()*{
    let $refine :=
(:        let $q := session:get-attribute("lastSearch"):)
(:        let $query := concat("[ft:query(.,'",$q,"')]"):)
(:        let $data-collection := '/db/bdcol':)
        let $scope := concat('collection($data-collection)','//dc:record')
(:        let $scope := util:eval($tempScope):)
        let $search-string := concat($scope,$query,$refineData)
        for $m in util:eval($search-string)
            let $score := ft:score($m)
            order by $score descending
            return $m
    let $q := session:set-attribute("lastSearch", concat($query,$refineData))
    let $q := session:set-attribute("lastSearchAvanzanda", concat($query,$refineData))
    return $refine
};

declare function salidaFacetada:navegacion($item as xs:string,$institution as xs:string) as element()*{

    let $condition := concat("//dc:",$item)
    
    let $data-collection := concat("/db/bdcol/",$institution)
    let $tempScope := concat('collection($data-collection)','//dc:record')(: $confVars:root tiene el nombre del root. Ej. dc_lom,lom; dc :)
    let $scope := util:eval($tempScope)  
    let $callback := util:function(xs:QName("salidaFacetada:term-callback"), 2)    

    let $faceteing := concat('util:index-keys($scope',$condition,",'', $callback, 10000)")
    let $result := <terms>{util:eval($faceteing)}</terms>
    
    let $show :=
                        
        for $term in $result/term
            let $ord := xs:integer($term//@freq)
            where $term != ""
            order by $ord descending
            return
            <facets>
                <cantidad>
                  {$ord}
                </cantidad>
                <termino>
                {
                    xs:string($term)
                }
                </termino>
            </facets>
                           
    
    return
        $show
        

};