xquery version "3.0";

module namespace usuario="http://exist-db.org/modules/usuarios";
declare namespace util="http://exist-db.org/xquery/util";
import module namespace configweb="http://exist-db.org/modules/configweb" at "ConfigWeb.xql";



(:funcion que nos devuelve el username dado un email:)
declare function usuario:ObtenerUsername($email as xs:string) as xs:string
{

for $record in collection($configweb:database-users )//metadata/user[email=$email]
    let $user := xs:string($record/username/text())
          return
              $user
              
};

(:funcion que nos devuelve el ID de un usuario es (1,2,3.....) luis :) 
declare function usuario:ObtenerIdUsuario($username as xs:string) as xs:integer
{

for $record in doc("/db/system/users.xml")//users/user[@name=$username]
    let $uid := xs:integer($record//user/@uid)
          return
              $uid
              
};

(:funcion que nos devuelve que tipo de usuario es (user adminRepo admin) luis :) 
declare function usuario:ObtenerTipoDeUsuario($username as xs:string) as xs:string
{
        
        if($username eq "admin") then 
            ( "admin")
            
        else( 
        
        let $nombreArchivo:=  concat($username,".xml")
        let $colleccion := concat($configweb:database-users, $nombreArchivo)
      
        let $type := doc($colleccion)/metadata
         return
             xs:string($type/user/type/text())
            ) 
             
};


(:funcion que nos devuelve la institucion  de un usuario -luis :) 
declare function usuario:ObtenerInstitucionUsuario($username as xs:string) as xs:string
{
        let $nombreArchivo:=  concat($username,".xml")
        let $colleccion := concat($configweb:database-users, $nombreArchivo)
      
        let $institucion := doc($colleccion)/metadata
        let $ret:=xs:string($institucion/user/institucion/text())
        return 
        if($ret) then( $ret)
                else("")
         
         
             
             
};


(:funcion que nos devuelve el telefono  de un usuario -luis :) 
declare function usuario:ObtenerTelefonoUsuario($username as xs:string) as xs:string
{
        let $nombreArchivo:=  concat($username,".xml")
        let $colleccion := concat($configweb:database-users, $nombreArchivo)
      
        let $telefono := doc($colleccion)/metadata
        let $ret:=xs:string($telefono/user/telefono/text())
        return 
        if($ret) then( $ret)
                else("")
             
};


(:funcion que nos devuelve el email  de un usuario -luis :) 
declare function usuario:ObtenerEmaileUsuario($username as xs:string) as xs:string
{
        let $nombreArchivo:=  concat($username,".xml")
        let $colleccion := concat($configweb:database-users, $nombreArchivo)
      
        let $email := doc($colleccion)/metadata
         return
             xs:string($email/user/email/text())
             
};

(:funcion que nos devuelve el Nombre  de un usuario -luis :) 
declare function usuario:ObtenerNombreUsuario($username as xs:string) as xs:string
{       
        
        if($username eq "admin") then 
            ( "admin")
        else( 
        
        
        let $nombreArchivo:=  concat($username,".xml")
        let $colleccion := concat($configweb:database-users, $nombreArchivo)
      
        let $nombre := doc($colleccion)/metadata
         
         return  xs:string($nombre/user/nombre/text())
             
        )
        
              
};



declare function usuario:ObtenerDatoConfirmacionMail($username as xs:string) as xs:string
{
        let $nombreArchivo:=  concat($username,".xml")
        let $colleccion := concat($configweb:database-users, $nombreArchivo)
        
        let $email := doc($colleccion)/metadata
         return
             xs:string($email/user/claveCorreo/text())
             
};
declare function usuario:EsUsuarioActivo($username as xs:string) as xs:boolean
{
        let $nombreArchivo:=  concat($username,".xml")
        let $colleccion := concat($configweb:database-users, $nombreArchivo)
        
        let $datoUsuario := doc($colleccion)/metadata
         return
             if($datoUsuario/user/activo/text() eq "0") then
             (
                 false()
             )else
             (
                 true()
             )
             
};
declare function usuario:activarUsuario($username as xs:string, $configwebirmacion as xs:string) as xs:string
{
        let $nombreArchivo:=  concat($username,".xml")
        let $archivo := concat($configweb:database-users, $nombreArchivo)
         
         
        let $colleccion := doc($archivo)/metadata/user
        let $claveConfirmacion := usuario:ObtenerDatoConfirmacionMail($username)
        let $loga := util:log("error", ("$username: ",$username))
        let $loga := util:log("error", ("confirmacion: ",$configwebirmacion))
        let $loga := util:log("error", ("$claveConfirmacion: ",$claveConfirmacion))
        return
        if($configwebirmacion eq $claveConfirmacion)then
        (
            let $loga := util:log("error", ("confirmacion: ", "1"))
            let $update := update replace $colleccion/activo with <activo>1</activo>
            
            return
                "1"
                
            )
        else
            (
                "0"
            )
        
        
             
};
declare function usuario:ObtenerEmailUsuario($username as xs:string) as xs:string
{
      
        let $nombreArchivo:= 
        if($username) then
        (
            concat($username,".xml")
        )
        else
        (
            ""
        )
        let $colleccion := concat($configweb:database-users, $nombreArchivo),
         $l := util:log("error", ("$email: ", doc($colleccion)/metadata))
        let $email := doc($colleccion)/metadata,
        $l := util:log("error", ("$email: ", $email//user/email/text()))
       return
             $email//user/email
             
};

(:funcion para cambiar password a un usuario (en el perfil, no en el sistema) -luis:)
declare function usuario:ChangePassword($username as xs:string, $newpassword as xs:string) as xs:string
{
        let $nombreArchivo:=  concat($username,".xml")
        let $archivo := concat($configweb:database-users, $nombreArchivo)
        let $colleccion := doc($archivo)/metadata/user
        
        let $update := update replace $colleccion/recuperacion with <recuperacion>{$newpassword}</recuperacion>
            return
        if($update) then(
                 "1"
                )else(
                    "0"
                )
             
};


(:funcion para actualizar los datos de un perfil de usuario -luis:)
declare function usuario:UpdateProfile($username as xs:string, $nombre as xs:string, $email as xs:string, $institucion as xs:string, $telefono as xs:string ) as xs:string
{
        let $nombreArchivo:=  concat($username,".xml")
        let $archivo := concat($configweb:database-users, $nombreArchivo)
        let $colleccion := doc($archivo)/metadata/user
        
        let $update := update replace $colleccion/nombre with <nombre>{$nombre}</nombre>
        let $update := update replace $colleccion/email with <email>{$email}</email>
        let $update := update replace $colleccion/institucion with <institucion>{$institucion}</institucion>
        let $update := update replace $colleccion/telefono with <telefono>{$telefono}</telefono>
                                       
            return
        if($update) then(
                 "1"
                )else(
                    "0"
                )
             
};



declare function usuario:ObtenerRecuperacionPassword($username as xs:string) as xs:string
{
        let $nombreArchivo:=  concat($username,".xml")
        let $colleccion := concat($configweb:database-users, $nombreArchivo)
        
        let $email := doc($colleccion)/metadata,
        $l := util:log("error", ("$email: ", $email))
         return
             $email/user/recuperacion/text()
             
};

declare function usuario:ObtenerImagenUsuario($username as xs:string) as xs:string?
{
        let $nombreArchivo:=  concat($username,".xml")
        let $colleccion := concat($configweb:database-users, $nombreArchivo)
        
        let $email := doc($colleccion)/metadata,
        $l := util:log("error", ("$email: ", $email))
         return
             xs:string($email/user/photo/text())
             
};