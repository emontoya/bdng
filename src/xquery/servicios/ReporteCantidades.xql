xquery version "3.0";

declare namespace request="http://exist-db.org/xquery/request";

declare namespace dc = "http://purl.org/dc/elements/1.1/";
declare namespace dmp = "http://reda.net.co/doer/ns/1.0/";

declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization";
declare namespace json="http://www.json.org";

declare option output:method "json";
declare option output:media-type "text/javascript";

declare function local:reporteCantidades($tipo as xs:string*) as element()*{
    
    let $tipo-predicate := if ($tipo) then concat("[dmp:doer/dmp:educational/dmp:resourceType[ft:query(@main,'", $tipo, "')]]") else ()
    
    let $hits :=
        let $data-collection := '/db/bdcol'
        let $tempScope := concat('collection($data-collection)','//dc:record')
        let $scope := util:eval($tempScope)
        let $search-string := concat('$scope',$tipo-predicate)
        for $m in util:eval($search-string)
        return $m
    
    let $total-result-count := count($hits)
    return
        <result>
            <cantidad>
            {
                $total-result-count
            }
            </cantidad>
        </result>
};

let $tipo := xs:string(request:get-parameter("tipo", ""))

return
    local:reporteCantidades($tipo)