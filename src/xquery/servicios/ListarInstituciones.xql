xquery version "3.0";

declare namespace request="http://exist-db.org/xquery/request";

declare namespace dc = "http://purl.org/dc/elements/1.1/";

declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization";
declare namespace json="http://www.json.org";

declare option output:method "json";
declare option output:media-type "text/javascript";

declare function local:ObtenerTooltip($inst as xs:string,$rep as xs:string) as xs:string*
{
    let $resp := 
        if($rep) then(
            (for $ttp in doc("/db/conf/oai_providers.xml")//oai_provider[contains(.//shortName,$rep)]
        	return
        	    $ttp//repositoryName/text())[1]
        )else(
            (for $ttp in doc("/db/conf/oai_providers.xml")//oai_provider[contains(.//shortInst,$inst)]
            return
                $ttp//institution/text())[1]
        )
    
    return
        $resp
};

declare function local:listarInstituciones() as element()*{
    let $collection:= "/db/bdcol"
    let $results :=
        for $child in doc("//db/stats/counter_cols.xml")//institution 
        order by number($child//size/text()) descending
        return 
            let $name := $child//name/text()
            let $size :=  $child//size/text()
            let $longname := local:ObtenerTooltip($name,"")
            return
                <institution>
                    <name>{$name}</name>
                    <size>{$size}</size>
                    <longname>{$longname}</longname>
                </institution>
    return
        <result>
        {
            $results
        }
        </result>
};

local:listarInstituciones()