xquery version "3.0";

declare namespace request="http://exist-db.org/xquery/request";
declare namespace session="http://exist-db.org/xquery/session";

declare namespace dc = "http://purl.org/dc/elements/1.1/";
declare namespace dmp = "http://reda.net.co/doer/ns/1.0/";

declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization";
declare namespace json="http://www.json.org";

declare option output:method "json";
declare option output:media-type "text/javascript";

import module namespace salidaFacetada="http://exist-db.org/modules/salidaFacetada"  at "BusquedaFacetada.xql";

import module namespace kwic="http://exist-db.org/xquery/kwic";

(: Funcion que realiza la búsqueda basica :)
declare function local:busquedaBasica($q as xs:string*, $start as xs:integer*, $perPage as xs:integer*, $refineBusqueda as xs:string*) as element()*{
    
    if ($q != '')then
        
        (: Variables que almacena el tiempo antes de la consulta para calcular cuanto tiempo se demora la busqueda :)
        let $timestart := util:system-time()
        
        (: En la variable options se declaran las opciones de la consulta, por ejemplo si es OR o AND. Para profundizar mirar la documentacion de eXist :)
        let $options := "<options><default-operator>and</default-operator><phrase-slop>1</phrase-slop><leading-wildcard>no</leading-wildcard><filter-rewrite>yes</filter-rewrite></options>"
        
        (: Variable que almacena la consulta realizada y con la cual se realiza el query:)
        let $query := concat("[ft:query(.,'",$q,"',",$options,")]")
        
        (: Variable que almacena la coleccion donde se realizara la consulta:)
        let $data-collection := '/db/bdcol'
                
        let $hits :=
            if($refineBusqueda) then (
                if(session:get-attribute("lastRefine") eq $refineBusqueda) then (
                    session:get-attribute("lastResult")
                )else(
                    salidaFacetada:refineBusqueda($query,$data-collection,$refineBusqueda)
                )
            )else(
                if(session:get-attribute("lastSearch") eq $q) then ( (: Si la búsqueda es igual a la anterior :)
                    session:get-attribute("lastResult")
                )else(
                    let $tempScope := concat('collection($data-collection)','//dc:record')
                    let $scope := util:eval($tempScope)
                    let $search-string := concat('$scope',$query)
                    
                    (: Organizar la busquedad :)
                    for $m in util:eval($search-string)
                    let $score := ft:score($m)
                    order by $score descending
            (:                order by $m/dc:general.title:)
                    return $m
                )
            )
            
        let $facets := 
            if(session:get-attribute("lastSearch") eq $q) then (
                session:get-attribute("lastFacets")
            )else(
                salidaFacetada:facetada($hits)
            )
        
        let $lastFacets := session:set-attribute("lastFacets", $facets)
        
        let $lastRefine := session:set-attribute("lastRefine", $refineBusqueda)
        let $q := session:set-attribute("lastSearch", concat($q,$refineBusqueda)),
            $lastResult := session:set-attribute("lastResult",$hits)
                    
        let $total-result-count := count($hits)
                    
        let $end := 
            if ($total-result-count lt $perPage) then 
                $total-result-count
            else 
                $perPage
                    
        return 
            <results>
                <facets>
                { 
                    $facets
                }
                </facets>
                <total>
                {
                    $total-result-count
                }
                </total>
                <start>
                {
                    $start
                }
                </start>
                <end>
                {
                    if ($total-result-count lt ($start + $perPage - 1)) then 
                        $total-result-count
                    else 
                        $start + $perPage - 1
                }
                </end>
                <time>
                {
                    seconds-from-duration(util:system-time()-$timestart)
                }
                </time>
                    
                {
                    subsequence($hits, $start, $end)
                }
                
            </results>
    else
        let $msg := 'Debe escribir algo en el campo de texto'
        return
            <result>
                {
                    $msg
                }
            </result>
            
};

let $q := replace(xs:string(request:get-parameter("query", "")), "[^0-9a-zA-ZäöüßÄÖÜáéíóúñÑ?\-,. ]", "")
let $start := xs:integer(request:get-parameter("start", "1"))
let $perPage := xs:integer(request:get-parameter("perPage", "9"))
let $refineBusqueda := xs:string(request:get-parameter("refineBusqueda", ""))
return
    local:busquedaBasica($q,$start,$perPage,$refineBusqueda)
