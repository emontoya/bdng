xquery version "3.0";

declare namespace request="http://exist-db.org/xquery/request";

declare namespace dc = "http://purl.org/dc/elements/1.1/";
declare namespace dmp = "http://reda.net.co/doer/ns/1.0/";

declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization";
declare namespace json="http://www.json.org";

declare option output:method "json";
declare option output:media-type "text/javascript";


declare function local:listarInstituciones($departamento as xs:string*) as element()*{
    let $data-collection := '/db/bdcol'
    let $hits :=
(:        for $m in distinct-values(collection($data-collection)//dc:record/dmp:doer/dmp:general/dmp:coverage/dmp:hierarchical/@state):)
(:        for $i in distinct-values(collection($data-collection)//dc:record[dmp:doer/dmp:general/dmp:coverage/dmp:hierarchical/@state=$departamento]/dc:institution):)
		for $i in distinct-values(collection($data-collection)//dc:record[dc:departamento=$departamento]/dc:institution)
        return
            <institucion>
                <name>{$i}</name>
                <entity>
                {
                    for $m in distinct-values(collection($data-collection)//dc:record[dc:institution=$i]/dmp:doer/dmp:rights/dmp:copyrightStackHolder/dmp:entity/@entityForm)
                    return $m
                }
                </entity>
            </institucion>
        
    return
         <result>
            {
                $hits
            }
        </result>
};

let $departamento := replace(xs:string(request:get-parameter("departamento", "")), "[^0-9a-zA-ZäöüßÄÖÜáéíóúñÑ?\-,. ]", "")
return
    local:listarInstituciones($departamento)