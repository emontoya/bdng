xquery version "3.0";

declare namespace request="http://exist-db.org/xquery/request";
declare namespace session="http://exist-db.org/xquery/session";

declare namespace dc = "http://purl.org/dc/elements/1.1/";
declare namespace dmp = "http://reda.net.co/doer/ns/1.0/";

declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization";
declare namespace json="http://www.json.org";

declare option output:method "json";
declare option output:media-type "text/javascript";

import module namespace salidaFacetada="http://exist-db.org/modules/salidaFacetada"  at "BusquedaFacetada.xql";

import module namespace kwic="http://exist-db.org/xquery/kwic";

(: Funcion que realiza la búsqueda avanzada. Tener en cuenta la modificación de los parametros si se requiere agregar un nuevo campo por el cual buscar :)
declare function local:busquedaAvanzada($title as xs:string*, $author as xs:string*, $nbc as xs:string*, $tipoRecurso as xs:string*, $tipo as xs:string*, $institution as xs:string*, $repositorio as xs:string*, $fechaPublicacion as xs:string*, $start as xs:integer*, $perPage as xs:integer*, $refineBusqueda as xs:string*) as element()*{
    
    (: Opciones con las cuales se pueden hacer los diferentes queries. Aca se puede definir si la consulta funciona como un AND o un OR :)
    let $options := "<options><default-operator>and</default-operator><phrase-slop>0</phrase-slop><leading-wildcard>no</leading-wildcard><filter-rewrite>yes</filter-rewrite></options>"
    
    (: Variables que almacenan los predicados de los queries. Para que las consultas con el indice funcionen se debe indicar antes del query el nodo padre del qname :)
    let $title-predicate := if ($title) then concat("[dmp:doer/dmp:general/dmp:titlePack[ft:query(dmp:title,'", $title, "')]]") else ()
    let $author-predicate := if ($author) then concat("[dmp:doer/dmp:lifeCycle/dmp:controlVersion/dmp:contribute[ft:query(dmp:entity,'", $author, "')]]") else ()
    let $nbc-predicate := if ($nbc) then concat("[dmp:doer/dmp:educational/dmp:eduProperties/dmp:specificContext[ft:query(@hsBasicCore,'", $nbc, "')]]") else ()
(:    let $tipoRecurso-predicate := if ($tipoRecurso) then concat("[dmp:doer/dmp:educational/dmp:resourceType[ft:query(@hsBasicCore,'", $subject, "')]]") else () :)
    let $tipo-predicate := if ($tipo) then concat("[dmp:doer/dmp:educational/dmp:resourceType[ft:query(@main,'", $tipo, "')]]") else ()
    let $fechaPublicacion-predicate := if ($fechaPublicacion) then concat('[dmp:doer/dmp:lifeCycle/dmp:grantDate/dmp:versionInfo[ft:query(@date,<query><near>',$fechaPublicacion,"</near></query>,",$options,")]]") else ()
    
    (: Variables que almacena el tiempo antes de la consulta para calcular cuanto tiempo se demora la busqueda :)
    let $timestart := util:system-time()
    
    (: Variables para controlar si se esta paginando y para realizar la busqueda refinada - facetacion :)
    let $query := concat($title,$author,$nbc,$tipo,$institution,$repositorio,$fechaPublicacion)
    let $query-predicate := concat($title-predicate,$author-predicate,$nbc-predicate,$tipo-predicate,$fechaPublicacion)
    
    (: Variables que almacenan la coleccion donde se realizara la busqueda. Puede cambiar dependiendo de en que institucion se quiere buscar segun la busqueda avanzada :)
    let $data-collection :=
        if ($repositorio and $institution) then
        (
            concat("/db/bdcol/",$institution,"/",$repositorio)   
        )else if($repositorio)then(
            let $instReep := 
            for $i in doc("//db/stats/counter_cols.xml")//repname[name = $repositorio]
                return(
                    $i//inst/text()
                )
            return    
                concat("/db/bdcol/",$instReep,"/",$repositorio)
        )else if($institution)then(
            concat("/db/bdcol/",$institution)   
        )else(
            '/db/bdcol'
        )
    
    (: Variables que almacena los resultados obtenidos por la busqueda :)
    let $hits :=
        if($refineBusqueda) then (
            if(session:get-attribute("lastRefineAvanzada") eq $refineBusqueda) then (
                session:get-attribute("lastResultAvanzanda")
            )else(
                salidaFacetada:refineBusqueda($query-predicate,$data-collection,$refineBusqueda)
            )
        )else(
            if(session:get-attribute("lastSearchAvanzanda") eq $query) then ( (: Si la búsqueda es igual a la anterior :)
                session:get-attribute("lastResultAvanzanda")
            )else(
    (:            let $data-collection := '/db/bdcol':)
                
        (:        let $tempScope := concat('collection($data-collection)','//dc:record'):)
                let $scope := concat('collection($data-collection)','//dc:record')
        (:        let $scope := util:eval($tempScope):)
                
                (: En esta variable se deben agregar los predicados que se agreguen nuevos en la busqueda avanzada :)
                let $search-string := concat($scope,$title-predicate,$author-predicate,$nbc-predicate,$tipo-predicate,$fechaPublicacion-predicate)
                (: Organizar la busquedad :)
            
                for $m in util:eval($search-string)
                let $score := ft:score($m)
                order by $score descending
                return $m
            )
        )
        
    let $facets := 
        if(session:get-attribute("lastSearchAvanzanda") eq $query) then (
            session:get-attribute("lastFacetsAvanzada")
        )else(
            salidaFacetada:facetada($hits)
        )
        
    let $lastFacetsAvanzada := session:set-attribute("lastFacetsAvanzada", $facets)
    
    let $lastRefineAvanzada := session:set-attribute("lastRefineAvanzada", $refineBusqueda)
    let $lastSearch := session:set-attribute("lastSearchAvanzanda",concat($query,$refineBusqueda)),
        $lastResult := session:set-attribute("lastResultAvanzanda",$hits)
        
    let $total-result-count := count($hits)
                    
    let $end := 
        if ($total-result-count lt $perPage) then 
            $total-result-count
        else 
            $perPage
    
    return 
        <results>
            <facets>
            { 
                $facets
            }
            </facets>
            <total>
            {
                $total-result-count
            }
            </total>
            <start>
            {
                $start
            }
            </start>
            <end>
            {
                if ($total-result-count lt ($start + $perPage - 1)) then 
                    $total-result-count
                else 
                    $start + $perPage - 1
            }
            </end>
            <time>
            {
                seconds-from-duration(util:system-time()-$timestart)
            }
            </time>
        {
            subsequence($hits, $start, $end)
        }
        </results>
            
};

(: Parametros capturados del request http al momento de hacer un llamado al servicio :)
(: Cuando se llama al servicio se ejecuta esta porcion de codigo donde se capturan los parametros y luego se llama la funcion local:busquedaAvanzada :)

let $title := replace(xs:string(request:get-parameter("title", "")), "[^0-9a-zA-ZäöüßÄÖÜáéíóúñÑ?\-,. ]", "")
let $author := replace(xs:string(request:get-parameter('author', '')), "[^0-9a-zA-ZäöüßÄÖÜáéíóúñÑ?\-,. ]", "")
let $nbc := replace(xs:string(request:get-parameter('nbc', '')), "[^0-9a-zA-ZäöüßÄÖÜáéíóúñÑ?\-,. ]", "")
let $tipoRecurso := replace(xs:string(request:get-parameter('tipoRecurso', '')), "[^0-9a-zA-ZäöüßÄÖÜáéíóúñÑ?\-,. ]", "")
let $tipo := replace(xs:string(request:get-parameter('tipo', '')), "[^0-9a-zA-ZäöüßÄÖÜáéíóúñÑ?\-,. ]", "")
let $institution := xs:string(request:get-parameter("institution", ""))
let $repositorio := xs:string(request:get-parameter("repositorio", ""))
let $fechaPublicacion := xs:string(request:get-parameter("fechaPublicacion", ""))
let $start := xs:integer(request:get-parameter("start", "1"))
let $perPage := xs:integer(request:get-parameter("perPage", "9"))
let $refineBusqueda := xs:string(request:get-parameter("refineBusqueda", ""))
return
    local:busquedaAvanzada($title,$author,$nbc,$tipoRecurso,$tipo,$institution,$repositorio,$fechaPublicacion,$start,$perPage,$refineBusqueda)
