xquery version "3.0";

declare namespace request="http://exist-db.org/xquery/request";

declare namespace dc = "http://purl.org/dc/elements/1.1/";
declare namespace dmp = "http://reda.net.co/doer/ns/1.0/";

declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization";
declare namespace json="http://www.json.org";

declare option output:method "json";
declare option output:media-type "text/javascript";

declare function local:ObtenerTooltip($inst as xs:string,$rep as xs:string) as xs:string*
{
    let $resp := 
        if($rep) then(
            (for $ttp in doc("/db/conf/oai_providers.xml")//oai_provider[contains(.//shortName,$rep)]
            return
        	    $ttp//repositoryName/text())[1]
        )else(
            (for $ttp in doc("/db/conf/oai_providers.xml")//oai_provider[contains(.//shortInst,$inst)]
            return
                $ttp//institution/text())[1]
        )
    
    return
        $resp
};

declare function local:listarDepartamentos() as element()*{
    let $data-collection := '/db/bdcol'
    let $hits :=
(:        for $m in distinct-values(collection($data-collection)//dc:record/dmp:doer/dmp:general/dmp:coverage/dmp:hierarchical/@state):)
        for $m in distinct-values(collection($data-collection)//dc:record/dc:departamento)
        return
            <departamento>
                <name>{$m}</name>
            </departamento>
        
    return
         <result>
            {
                $hits
            }
        </result>
};

local:listarDepartamentos()