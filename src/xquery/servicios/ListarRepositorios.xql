xquery version "3.0";

declare namespace request="http://exist-db.org/xquery/request";

declare namespace dc = "http://purl.org/dc/elements/1.1/";

declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization";
declare namespace json="http://www.json.org";

declare option output:method "json";
declare option output:media-type "text/javascript";

declare function local:listarRepositorios() as element()*{
    let $collection:= "/db/bdcol"
    let $results :=
        for $child in doc("//db/stats/counter_cols.xml")//repname 
        order by number($child//size/text()) descending
        return $child
            
    return
        <result>
        {
            $results
        }
        </result>
};

local:listarRepositorios()