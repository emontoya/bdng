xquery version "1.0";

module namespace configweb="http://exist-db.org/modules/configweb";
declare namespace request="http://exist-db.org/xquery/request";
declare namespace util="http://exist-db.org/xquery/util";
declare namespace system="http://exist-db.org/xquery/system";
declare namespace dc="http://purl.org/dc/elements/1.1/";
declare namespace oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/";

(: declaracion de variables de entorno de la configuracion del modulo Web 2.0 de BDCOL :)

declare variable $configweb:redirect-uri as xs:anyURI { xs:anyURI("index.xq") };
declare variable $configweb:ruta-web20  { concat("file://",system:get-exist-home(),"/webapp/query/")}; 
declare variable $configweb:ruta-bdngsetup  { concat("file://",system:get-exist-home(),"/webapp/bdng/")}; 
(:declare variable $configweb:ruta-web20  { concat("http://",request:get-server-name(),":", request:get-server-port(),request:get-context-path(),"/query/") }; :)
declare variable $configweb:database-perfiles  { "xmldb:exist:///db/users"};
declare variable $configweb:database-login { "xmldb:exist:///system" };


(:Path en eXist donde se guardan los archivos del perfil del usuario asi como las fotos de estos:)
declare variable $configweb:database-users as xs:string {  "/db/users/" };
declare variable $configweb:ruta-DirectorioImagen as xs:string {  "/images/users/" };

(:Ruta en el servidor donde se van a guardar las imagenes de los usuarios:)
declare function configweb:rutaLocalImagen() as xs:string 
{
   let $ruta:=system:get-module-load-path(),
   $ruta:= concat($ruta,$configweb:ruta-DirectorioImagen)
    return
    $ruta
   };

(:Estilos css para los comentarios y las etiquetas de la web 2.0 de BDCOL:)
declare variable $configweb:cssComentario { "styles/cssComentarios.css" };
declare variable $configweb:cssTags { "styles/cssTags.css" };


(:Path en eXist donde se guardan los comentarios:)
declare variable $configweb:database-com as xs:string { "/db/comments/" };



(:Ruta en web con el nombre del contexto bdcol donde estan las imagenes:)
declare variable $configweb:ruta_imagen as xs:string { "/bdcol/query/images/users/" };

(:Path para la validacion del capcha de registro de usuarios con www.recapcha.net de google:)
declare variable $configweb:VALIDATE_URI as xs:anyURI := xs:anyURI("http://api-verify.recaptcha.net/verify");

(: Archivo para la configuracion de los passwords :)
declare variable $configweb:recuperarPassword as xs:anyURI { xs:anyURI("recuperarPassword.xq") };

(:Path y archivo para activar el usuario cuando se manda el correo:)

declare function configweb:uri-emailValidation() as xs:string { 
let $ruta:=replace(request:get-url(),"/register.xq",""),
    $ruta:= concat($ruta,"/activarUsuario.xq?dato=")
    return
    $ruta
} ;

declare variable $configweb:ruta-raiz {"/db/bdcol"};

(:ATENCION DIRECCION  IP DEL SERVIDOR SMTP  - Actulizarla siempre que se cambia de maquina :)
declare variable $configweb:ip-smtp as xs:string { "200.12.180.67" } ;


(:Path en eXist donde se guardan los comentario y se define el maximo y el minimo de letras en los comentarios :)
declare variable $configweb:database-tags as xs:string { "/db/tags/" };
declare variable $configweb:tamanoMaximoLetraTags as xs:integer {50 };
declare variable $configweb:tamanoMinimoLetraTags as xs:integer {10 };

declare function configweb:get-namespace-decls() as xs:string? {
	"declare namespace bdcol='http://www.bdcol.org/v1';declare namespace dc='http://purl.org/dc/elements/1.1/';"
};

(: Get the XPath expression for the specified field :)
declare function configweb:queryField($field as xs:string) as xs:string
{
	if($field eq "au") then
	    "dc:creator"
	else if($field eq "ti") then
	    "dc:title"
	else if($field eq "ab") then
	    "dc:description"
	else if($field eq "su") then
	    "dc:subject"
	else if($field eq "ye") then
	    "dc:date"
	else
	    "."
};

declare function configweb:get-query-root($collection as xs:string) as xs:string {
    concat("collection('", $collection, "')//oai_dc:dc")
};

declare variable $configweb:cssUsuario { "styles/cssUsuarios.css" };
declare variable $configweb:cssRegister { "styles/register.css" };

declare variable $configweb:jsUsuario { "js/jsUsuarios.js" };
declare variable $configweb:homePage { "./index.xq" };

declare function configweb:ObtenerIdentificador($collection as element()*) as xs:string
{
	let $identificador:= configweb:ObtenerIdentificadorUrl($collection)
	let $retorno := 
		if($identificador and $identificador ne "") then
		(
			$identificador
		)
		else
		(
			if(count($collection) ne 0) then
            (
            	if($collection[0] and $collection[0] ne "") then
            	(
					$collection[0]
				)
				else
				(
					$collection[1]
				)
				
			)
			else
			(
				" "
			)
		)
	
	return
		if($retorno) then
		(
			$retorno
		)
		else
		(
			" "
		)
		
};


declare function configweb:ObtenerIdentificadorUrl($collection as element()*) as xs:string
{
    let $datos:= configweb:Identify($collection)
          return 
              let $conteo:= count($datos)
              return
                  if($conteo >1) then
                  (
                      xs:string($datos[1])
                  )
                  else
                  (
                      if($conteo eq 0) then
                      (
                          ""
                      )
                      else
                      (
                          xs:string($datos)
                       )
                  )
          
};
declare function configweb:Identify($identify as element()*) as element()* {
    if (exists($identify)) then
       (for $pub in $identify
                return
                   if (contains($pub, "http://")) then ($pub) else ()
        )                       
     else ()
};


(: Funcion para uso de la fecha en terminos de la fecha del dia actual :)
declare function configweb:ObtenerTiempoFechaFormateada($dt as xs:dateTime) as xs:string
{
    let $fechaActual:=current-dateTime(),
    $tiempoLlevado:="Hace",    
    $restaFechas:= $fechaActual - $dt,
    $anos:=years-from-duration($restaFechas),
    $meses:= months-from-duration($restaFechas),
    $dias:= days-from-duration($restaFechas),
    $horas:= hours-from-duration($restaFechas),
    $minutos:= minutes-from-duration($restaFechas),
    $medidaTiempo:=   
    if($anos gt 0) then
    (
        "Años"
    )
    else
    (
               if($meses gt 0) then
               (
                    "Meses"
               )else
               (
                           if($dias gt 0) then
                           (
                               "Días"
                           )
                           else
                           (
                                       if($horas gt 0) then
                                       (
                                               "Horas"
                                        )
                                        else
                                        (
                                                 if($minutos gt 0) then
                                               (
                                                   "Minutos"
                                                )
                                                else
                                                (
                                                    "Minuto"
                                                )
                                        )
                           )
                   )
         )
         
(:Hallar la cantidad de años, meses, dias o minutos desde la fecha que se paso por parametro:)
 let    $unidadTiempo:=   
    if($anos gt 0) then
    (
        $anos
    )
    else
    (
               if($meses gt 0) then
               (
                    $meses
               )else
               (
                           if($dias gt 0) then
                           (
                               $dias
                           )
                           else
                           (
                                           if($horas gt 0) then
                                       (
                                           $horas
                                        )
                                        else
                                        (
                                                      if($minutos gt 0) then
                                                       (
                                                          $minutos
                                                        )
                                                        else
                                                        (
                                                            1
                                                        )
                                        )
                           )
                   )
         )
         
         let $fechaFormateada:= concat("Hace ",$unidadTiempo," ",$medidaTiempo)
         return
         $fechaFormateada
    
};


(: Contenido del correo que se envia a los usuarios la recuperar su contraseña en BDCOL :)


declare function configweb:ContenidoCorreoRecuperacionPassword($usuario as xs:string, $password as xs:string, $email as xs:string) as element()*
{
let $mensaje := <mail>
    <from>Administrador BDCOL &lt;{$configweb:emailSender}&gt;</from>
    <to>{$email}</to>
    <subject>Recuperación de su contraseña</subject>
    <message>
        <text>Recuperación de su contrase&#241;a en BDCOL</text>
        <xhtml>
            <html>
					<head>
						<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
						<title>BDCOL- Biblioteca Digital Colombiana</title>
						<link type="text/css" href="styles/cssMail.css" rel="stylesheet" />
					</head>
					<body >
						<div class="topbar aligns" style="font-size: smaller; white-space: 
							nowrap;">

							</div>
							<div class="header margins" style="height: 40px; margin: 13px 15px 9px;">
								<a >
									<img src="{$configweb:imgHeader}" class="floats-normal" alt=""
									border="0" />
								</a>
							</div>
							<div style="clear: both;"></div>
							<br/>
							<div class="body">
								<b>Recuperaci&#243;n de su contrase&#241;a</b>
								<br/><br/>
								<p style="font-size: smaller;">El proceso de recuperación de contrase&#241;a se realiz&#243; con &#233;xito para el usuario <b>{$usuario}</b> - <b>{$email}</b> 
								email address.</p>
								<p style="font-size: smaller;">La contraseña del usuario <b>{$usuario}</b> es : <b>{$password}</b>.</p>
								
							</div>
					 </body>
			</html>

        </xhtml>
    </message>
</mail>
return $mensaje
};




(: Contenido del correo de activacion de que se le envia al usuario desde bdcol :)

declare function configweb:ContenidoCorreoActivacion($usuario as xs:string,$url as xs:string,$email as xs:string) as element()*
{
let $mensaje := <mail>
    <from>Administrador BDCOL  &lt;{$configweb:emailSender}&gt;</from>
    <to>{$email}</to>
    <subject>Activaci&#243;n de su cuenta en BDCOL</subject>
    <message>
        <text>Gracias por hacer parte de la Biblioteca Digital colombiana, este correo es para su activaci&#243;n como usuario en BDCOL</text>
        <xhtml>
            <html>
					<head>
						<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
						<title>BDCOL- Biblioteca Digital ColombianaL</title>
						<link type="text/css" href="styles/cssMail.css" rel="stylesheet" />
					</head>
					<body >
						<div class="topbar aligns" style="font-size: smaller; white-space: 
							nowrap;">

							</div>
							<div class="header margins" style="height: 140px; margin: 13px 15px 9px;">
								<a >
									<img src="{$configweb:imgHeader}" class="floats-normal" alt=""
									border="0" />
								</a>
							</div>
							<div style="clear: both;"></div>
							<br/>
							<div class="body">
								<b>Activacion de  Usuarios en BDCOL</b>
								<br/><br/>
								<p style="font-size: smaller;">El proceso de creación de su  usuario  <b>{$usuario}</b> - <b>{$email}</b> en BDCOL se realiz&#243;con &#233;xito.</p>
								<br/><br/>
								<p style="font-size: smaller;">Haga click en el siguiente enlace para activar su usuario.</p>
								<p style="font-size: smaller;"><a href="{$url}">Click para activar usuario</a></p>
								
							</div>
					 </body>
			</html>
        </xhtml>
    </message>
</mail>
return $mensaje
};


(: Contenido del correo de activacion de que se le envia adl admin de BDCOL para activar un Administrador de Repositorios- Luis :)

declare function configweb:ContenidoCorreoActivacionAdminRepo($usuario as xs:string,$url as xs:string,$email as xs:string, $institucion as xs:string, $telefono as xs:string) as element()*
{
let $mensaje := <mail>
    <from>Administrador BDCOL  &lt;{$configweb:emailSender}&gt;</from>
    <to>{$configweb:emailAdmin }</to>
    <subject>Activaci&#243;n de su cuenta en BDCOL</subject>
    <message>
        <text>Este correo es para la activaci&#243;n de un usuario Administrador de Repositorios  en BDCOL</text>
        <xhtml>
            <html>
					<head>
						<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
						<title>BDCOL- Biblioteca Digital ColombianaL</title>
						<link type="text/css" href="styles/cssMail.css" rel="stylesheet" />
					</head>
					<body >
						<div class="topbar aligns" style="font-size: smaller; white-space: 
							nowrap;">

							</div>
							<div class="header margins" style="height: 40px; margin: 13px 15px 9px;">
								<a >
									<img src="{$configweb:imgHeader}" class="floats-normal" alt=""
									border="0" />
								</a>
							</div>
							<div style="clear: both;"></div>
							<br/>
							<div class="body">
								<b>Activar de Usuarios en BDCOL</b>
								<br/><br/>
								<p style="font-size: smaller;">El usuario  <b>{$usuario}</b> ha solicitado darse de alta como Administrador de Repositorios en BDCOL.</p>
								
								<p style="font-size: smaller;">Institucion: <b>{$institucion}</b></p>
								<p style="font-size: smaller;">Telefono: <b>{$telefono}</b></p>
								<p style="font-size: smaller;">Correo electronico: <b>{$email}</b></p>
								<br/>
								<p style="font-size: smaller;">Haga click en el siguiente enlace para activarlo como Administrador de Repositorios.</p>
								<p style="font-size: smaller;"><a href="{$url}">Click para activar</a></p>
								
							</div>
					 </body>
			</html>
        </xhtml>
    </message>
</mail>
return $mensaje
};



(: contenido del correo de confirmacion que le llega al usuario tipo AdminRepo luego de la activacion -Luis:)
declare function configweb:ContenidoCorreoConfirmacionAdminRepo($user as xs:string,$email as xs:string) as element()*
{
let $mensaje := <mail>
    <from>Administrador BDCOL  &lt;{$configweb:emailSender}&gt;</from>
    <to>{$email}</to>
    <subject>Activaci&#243;n de  Administrador de Repositorios en BDCOL</subject>
    <message>
        <text>Ete correo es para confirmacion de  la activaci&#243;n de un usuario Administrador de Repositorios  en BDCOL</text>
        <xhtml>
            <html>
					<head>
						<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
						<title>BDCOL- Biblioteca Digital Colombiana</title>
						<link type="text/css" href="styles/cssMail.css" rel="stylesheet" />
					</head>
					<body >
						<div class="topbar aligns" style="font-size: smaller; white-space: 
							nowrap;">

							</div>
							<div class="header margins" style="height: 40px; margin: 13px 15px 9px;">
								<a >
									<img src="{$configweb:imgHeader}" class="floats-normal" alt=""
									border="0" />
								</a>
							</div>
							<div style="clear: both;"></div>
							<br/>
							<div class="body">
								<b>Felicitaciones</b>
								<br/>
								<p style="font-size: smaller;">El proceso de activacion del Administrador de Repositorios <b>{$user}</b> en BDCOL se realiz&#243; con &#233;xito.</p>
								<br/>
								<p style="font-size: smaller;">Haga click en el siguiente enlace para empezar a usar la plataforma como Administrador de Repositorios</p>
								<p style="font-size: smaller;"><a href="http://test.bdcol.org:7070/admin">Click para iniciar sesion como Administrador de Repositorios</a></p>
								
							</div>
					 </body>
			</html>
        </xhtml>
    </message>
</mail>
return $mensaje
};

(: Contenido de la pagina de exito que se muestra al realizarse una activacion de correo valida en  bdcol :)

declare function configweb:PaginaExito($operacion as xs:string,$mensaje as xs:string) as element()*
{
	<html>
		
				<head>
						<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
						<title>BDCOL - Biblioteca Digital Colombiana</title>
						<link type="text/css" href="styles/cssMail.css" rel="stylesheet" />
						<link type="text/css" href="styles/register.css" rel="stylesheet" />
					</head>
					<body >
							<center>
								<div  style="height: 140px; border:0; width:970px; align:center;background-image:url('{$configweb:imgHeader}');"  >
									<br/>
									
								</div>
							</center>	
							<BR/>
							<center>
								<div class="block" style="width:70%" align="left">
									<h3>{$operacion}</h3>
									<p class="s3" align="center">{$mensaje}
										
									</p>
									<a href="{$configweb:redirect-uri}">Ir a BDCOL</a>
								</div>
							</center>
					</body>
			</html>
};


(: Contenido de la pagina de error que se muestra al realizarse una activacion de correo no valida en  bdcol :)

declare function configweb:PaginaError($mensaje as xs:string) as element()*
{
	<html>
		
				<head>
						<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
						<title>BDCOL - Biblioteca Digital Colombiana</title>
						<link type="text/css" href="styles/cssMail.css" rel="stylesheet" />
						<link type="text/css" href="styles/register.css" rel="stylesheet" />
					</head>
					<body class="block" >
						 <center>
							<div  style="height: 140px; border:0; width:970px; align:center;background-image:url('{$configweb:imgHeader}');"  >
								<br/>
								
							</div>
						</center>	
						<BR/>
						<center>
							<div class="block" style="width:70%" align="left">
								<h3>Página errónea</h3>
								<p class="s3">Lo sentimos, han ocurrido los siguientes errores en el sistema :
									<ul>
										<li class="s2">{$mensaje}</li>
									</ul>
									<a href="{$configweb:redirect-uri}">Ir a BDCOL</a>
								</p>
							</div>
						</center>	
					</body>
			</html>
};

declare function configweb:ObtenerNodo($nombre as xs:string,$value as xs:string) as element()*
{
   let $elemento:= element { $nombre}
    {
        $value
    }
    let $loga := util:log("error", ("$elemento: ", $elemento))
    return
    $elemento
};

declare function configweb:ContainsNonCase
  ( $arg as element()* ,
    $substring as xs:string )  as xs:boolean? {
   let $loga := util:log("error", ("$conteo1: ", $arg))
   return
   if ($arg) then
   (
	for $obj in $arg
	return
		 let $loga := util:log("error", ("$conteo: ", count($obj)))
		 return
		if (count($obj) gt 1) then
		(
			let $loga := util:log("error", ("$conteo: ", "datos 1"))
			return
			for $obj1 in $obj
			where contains(upper-case(xs:string($obj)), fn:upper-case($substring))
			return
			true()
				
		)
		else
		(
			let $loga := util:log("error", ("$conteo2: ", upper-case($obj)))
			return
			let $datos:=contains(upper-case($obj), upper-case($substring)),
			 $loga := util:log("error", ("$conteo2: ", "datos1"))
			return
			$datos
		  
		)
		
	)
	else
	(
		false()
	)
};


(: Variable para el envio de correos electronicos de activacion desde BDCOL Web 2.0 al correo que el usuario registro :)
declare variable $configweb:emailSender { "sender@bdcol.org" };
declare variable $configweb:emailAdmin { "contacto.bdcol.org@gmail.com" };


(: Variables usadas en el Modulo de etiquetas :)
declare variable $configweb:tag-descripcion as xs:string {  "descripcion" };
declare variable $configweb:tag-username as xs:string {  "username" };
declare variable $configweb:tag-acceso as xs:string {  "acceso" };
declare variable $configweb:tag-identifier as xs:string {  "dc:identifier" };


(: Variables usadas en el Modulo de comentarios:)
declare variable $configweb:comment-id as xs:string {  "id" };
declare variable $configweb:comment-date as xs:string {  "date" };
declare variable $configweb:comment-user as xs:string {  "user" };
declare variable $configweb:comment-description as xs:string {  "description" };
declare variable $configweb:comment-identifier as xs:string {  "dc:identifier" };
declare variable $configweb:comment-nrocomments as xs:string {  "nrocomments" };
declare variable $configweb:comment-maximocomentarios as xs:integer { 30  };


(:Variables usadas en el Modulo de personalizacion:)

(: Variables usadas en Agregar documentos:)
declare variable $configweb:mybd-identifier as xs:string {  "dc:identifier" };

(:Variables usadas en  Consultas:)
(:conteo:)
declare variable $configweb:mybd-nrocomments as xs:string {  "nroconsultas" };
declare variable $configweb:mybd-id as xs:string {  "id" };
declare variable $configweb:mybd-url as xs:string {  "url" };
declare variable $configweb:mybd-texto as xs:string {  "texto" };
declare variable $configweb:mybd-descripcion as xs:string {  "descripcion" };


(:Variable usadas en el Modulo de usuarios:)
declare variable $configweb:usuarios-nombre as xs:string {  "nombre" };
declare variable $configweb:usuarios-username as xs:string {  "username" };
declare variable $configweb:usuarios-email as xs:string {  "email" };
declare variable $configweb:usuarios-photo as xs:string {  "photo" };
declare variable $configweb:usuarios-claveCorreo as xs:string {  "claveCorreo" };
declare variable $configweb:usuarios-activo as xs:string {  "activo" };
declare variable $configweb:usuarios-recuperacion as xs:string {  "recuperacion" };
declare variable $configweb:usuarios-subscripcion as xs:string {  "subscripcion" };
declare variable $configweb:usuarios-type as xs:string {  "type" };
declare variable $configweb:usuarios-institucion as xs:string {  "institucion" };
declare variable $configweb:usuarios-telefono as xs:string {  "telefono" };


(:Variable usada para la imagen del header de los correos que se envian -luis:)
declare variable $configweb:imgHeader as xs:string {  "http://test.bdcol.org:7070/admin/images/header.png" };


(:Variable usada para saber si es web o web 2.0:)
declare variable $configweb:esWeb2 as xs:string {  "0" };

(:Variable usada para la clave de la cuenta admin(AKI revisar si podemos modificarla con el instalador):)
declare variable $configweb:passwordAdmin as xs:string {  "admin" };


declare function configweb:get-query-root($collection as xs:string) as xs:string {
    concat("collection('", $collection, "')//oai_dc:dc")
};

declare function configweb:get-query-comentario($collection as xs:string) as xs:string {
    concat("collection('", $collection, "')//document")
};
