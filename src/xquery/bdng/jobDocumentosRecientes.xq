xquery version "1.0";
declare namespace dc="http://purl.org/dc/elements/1.1/";
declare namespace oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/";
declare namespace util="http://exist-db.org/xquery/util";
declare namespace xdb="http://exist-db.org/xquery/xmldb";
declare namespace request="http://exist-db.org/xquery/request";


declare function local:ObtenerIdentificadorUrl($datos as element()*) as xs:string
{
	let $conteo:= count($datos)
		  return
			  if($conteo >1) then
			  (
				  xs:string($datos[1])
			  )
			  else
			  (
				  if($conteo eq 0) then
				  (
					 ""
				  )
				  else
				  (
					  xs:string($datos)
				   )
			  )
          
};


declare function local:Identify($identify as element()*) as element()* {
    if (exists($identify)) then
       (for $pub in $identify
                return
                   if (contains($pub, "http://")) then ($pub) else ()
        )                       
     else ()
};

declare function local:ObtenerDatosColeccion($coleccion as xs:string) as element()*
{
	let $dl_home := "/db/bdcol"
	let $termCollection :=<query><near ordered='no'>{$coleccion}</near></query>
	let $datos:=	
		for $dato in collection($dl_home)//oai_dc:dc[dc:collection[ft:query(. , $termCollection)]] 
	   return 
			$dato
	let $documentos:= $datos
	let $count := count($documentos),
	$max := 20,
	$start := 1,
	$end := if ($start + $max - 1 < $count) then $start + $max - 1 else $count
	for $p in $start to $end
	return
		let $documento := $documentos[$p]
		return
		<documento>
			<identifier>{local:Identify($documento//dc:identifier)}</identifier>
			<titulo>{$documento//dc:title/text()}</titulo>
			<resumen>{$documento//dc:description/text()}</resumen>
		</documento>

			
};
declare function local:scan-collection() as element()* {
    for $coleccion in doc("//db/stats/counter_cols.xml")//collection  
    return (
        <collection>
          <name>{$coleccion//name/text()}</name>
		  <documentos>
			{local:ObtenerDatosColeccion($coleccion//name/text())}
		  </documentos>
		 </collection>
    )
};
declare function local:store() as element()* {
<registros>
{local:scan-collection()}
</registros>
};


let $nodo:= local:store()
let $datos:=system:as-user("admin", "admin",  xdb:store("/db/stats","consolidado_Datos.xml",<metadata></metadata>))
let $collection:= doc("/db/stats/consolidado_Datos.xml")//metadata
let $update := update insert
$nodo
into $collection  
return
$datos

 
