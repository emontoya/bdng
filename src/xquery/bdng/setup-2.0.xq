declare namespace dc="http://purl.org/dc/elements/1.1/";
declare namespace oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/";
declare namespace request="http://exist-db.org/xquery/request";
declare namespace xdb="http://exist-db.org/xquery/xmldb";
declare namespace exist = "http://exist.sourceforge.net/NS/exist";
import module namespace configweb="http://exist-db.org/modules/configweb" at "configWeb.xq";

declare option exist:output-size-limit "30000";


(: Funcion para crear los directorios de trabajo de web 2.0 josbs, comments, tags y users :)

declare function local:CrearDirectorios() 
{
	if (xdb:collection-available("/db/bdcol") eq fn:false()) then
	(
		system:as-user("admin", "admin",xdb:create-collection("/db", "bdcol")),
		<p>Se creo la coleccion bdcol</p>
	)
    else 
        (),
	if (xdb:collection-available("/db/jobs") eq fn:false()) then
	(
		system:as-user("admin", "admin",xdb:create-collection("/db", "jobs")),
		<p>Se creo la coleccion de jobs</p>
	)
    else 
        (),
    if (xdb:collection-available("/db/comments") eq fn:false()) then
    (
        system:as-user("admin", "admin",xdb:create-collection("/db", "comments")),
        <p>Se creo la coleccion de comentarios</p>
    )
    else 
        (),
	if (xdb:collection-available("/db/tags") eq fn:false()) then
	(
		system:as-user("admin", "admin",xdb:create-collection("/db", "tags")),
		<p>Se creo la coleccion de tags</p>
        )
    else 
        (),
	if (xdb:collection-available("/db/users") eq fn:false())
	 then
	(
		system:as-user("admin", "admin",xdb:create-collection("/db", "users")),
		<p>Se creo la coleccion de usuarios</p>
	)
    else 
        (),
    if (xdb:collection-available("/db/system/config/db/bdcol") eq fn:false()) then
	(
		system:as-user("admin", "admin",xdb:create-collection("/db/system/config", "db/bdcol")),
		<p>Se creo la coleccion de indexacion</p>
	)
    else 
        (),
    if (xdb:collection-available("/db/conf") eq fn:false()) then
	(
		system:as-user("admin", "admin",xdb:create-collection("/db", "conf")),
		<p>Se creo la coleccion: /db/conf</p>
	)
    else 
        ()
    
};


(: Funcion para crear jobs :)

declare function local:CrearJobs()
{
	let $collection := "xmldb:exist:///db/jobs"
	let $URI := xs:anyURI(concat($configweb:ruta-bdngsetup,"jobDocumentosRecientes.xq"))
	let $retCode :=  system:as-user("admin", "admin",xdb:store($collection, "jobDocumentosRecientes.xq", $URI))
	return <p>Se crearon los jobs</p>
};

declare function local:CrearArchivoIndice()
{
	let $collection := "xmldb:exist:////db/system/config/db/bdcol"
	let $URI := xs:anyURI(concat($configweb:ruta-bdngsetup,"collection.xconf"))
	let $retCode :=  system:as-user("admin", "admin",xdb:store($collection, "collection.xconf", $URI))
	return <p>Se crearon el archivo de indices</p>
};

declare function local:CrearBWlists()
{
	let $collection := "xmldb:exist:////db/conf"
	let $urib := xs:anyURI(concat($configweb:ruta-bdngsetup,"blacklist.xml"))
	let $uriw := xs:anyURI(concat($configweb:ruta-bdngsetup,"whitelist.xml"))
	let $retCode :=  system:as-user("admin", "admin",xdb:store($collection, "blacklist.xml", $urib))
	let $retCode :=  system:as-user("admin", "admin",xdb:store($collection, "whitelist.xml", $uriw))
	return <p>Se crearon el archivo de BW Lists</p>
};

local:CrearDirectorios(),
local:CrearJobs(),
local:CrearArchivoIndice(),
local:CrearBWlists()
