xquery version "1.0" ;

module namespace application_layout="http://exist-db.org/modules/application_layout";


import module namespace repositorio="http://exist-db.org/modules/repositorio" at "../libraryModules/repositorios.xq";
import module namespace loginF="http://exist-db.org/modules/loginF" at "../user/loginF.xq";
import module namespace configweb="http://exist-db.org/modules/configweb" at "../user/configWeb.xq";
 
declare namespace bib="http://exist-db.org/bibliography";
declare option exist:serialize "method=xhtml media-type=text/html";

declare function application_layout:default($content as element()*) as element()*{

    <html >
       <head>
            <title> {$configweb:sysName} - {$configweb:sysNameFull}</title>
            
            <!-- USER CSS -->
            <link href="./user/css/display.css" type="text/css" rel="stylesheet"/>
            <link href="./user/css/front.css" media="screen, projection" rel="stylesheet" type="text/css" />
            <link rel="stylesheet" href="./user/css/colorbox.css" />
            
            <link type="text/css" href="./resources/css/app/basic.css" rel="stylesheet"/>
            <link type="text/css" href="./resources/css/app/displayRecords.css" rel="stylesheet"/>
            <link type="text/css" href="./resources/css/general/nanoscroller.css" rel="stylesheet"/>
            <link type="text/css" href="./resources/css/general/jackedup.css" rel="stylesheet"/>
            <link type="text/css" href="./resources/css/general/chosen.css" rel="stylesheet"/>
            <link type="text/css" href="./resources/css/general/stylePaginate.css" rel="stylesheet"/>
            <link type="text/css" href="./resources/css/general/redmond/jquery-ui-1.8.17.custom.css" rel="stylesheet"/>
            <link type="text/css" href="./resources/css/general/tipTip.css" rel="stylesheet"/><!--tiptip -->
            <link type="text/css" href="./resources/css/general/jquery.reject.css" rel="stylesheet"/><!--Reject -->
            
            
            
            <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>-->
            <script type="text/javascript" src="./resources/js/general/jquery-1.7.1.min.js"></script>
            <script type="text/javascript" src="./resources/js/general/modernizr.js"></script>
            <script type="text/javascript" src="./resources/js/general/jquery-ui-1.8.17.custom.min.js"></script>
            <script type="text/javascript" src="./resources/js/general/jquery.nanoscroller.min.js"></script>
            <script type="text/javascript" src="./resources/js/general/spin.min.js"></script>
            <script type="text/javascript" src="./resources/js/general/humane.min.js"></script>
            <script type="text/javascript" src="./resources/js/general/chosen.jquery.min.js"></script>
            <script type="text/javascript" src="./resources/js/general/jquery.paginate.js"></script>
            <script type="text/javascript" src="./resources/js/general/jquery.tipTip.minified.js"></script>
            <script type="text/javascript" src="./resources/js/general/jquery.reject.min.js"></script>
            
            <script type="text/javascript" src="./resources/js/app/stopWords.js"></script>
            <!--<script type="text/javascript" src="./resources/js/general/jquery.jsonp.min.js"></script>-->
            <script type="text/javascript" src="./resources/js/app/commonFunctions.js"></script>
            <script type="text/javascript" src="./resources/js/app/admin.js"></script>
            <script type="text/javascript" src="./resources/js/app/app.js"></script>
            
            <!-- USER JS -->
            <script type="text/javascript" src="./user/js/jsLogin.js" charset="utf-8">a</script>
            <script type="text/javascript" src="./user/js/jquery.colorbox.js" charset="utf-8">a</script>
        
       </head>
       <style>
            
       </style>
       <body>
       <!-- CONTENEDOR TOTAL -->
       <div id="contenedor">
            <!-- LOADING DIV (div )-->
            <div id="cargando" class="loadingDiv"></div>
            <div style="width:900px;">
                <div class="adminLink"> <a  href="../../admin" >Admin</a></div>
                <!-- REGISTER AND LOGIN -->
                { loginF:show-login() }
            </div>
            <!-- HEADER TOP -->        
            <div class="header-top"></div>
            {$content}
            <div class="clear"></div>
            <!-- FOOTER -->
            <div class="footer_bottom"><img class="imgBack" src="resources/Images/layout/footer.jpg"></img></div>
       </div>
       <script type="text/javascript">
    
       </script>
       </body>
    </html>
};
