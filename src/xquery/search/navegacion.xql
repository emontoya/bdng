xquery version "1.0" ;
(:metadata: LOM:)
import module namespace repositorio="http://exist-db.org/modules/repositorio" at "./libraryModules/repositorios.xq";
import module namespace salidaFacetada="http://exist-db.org/modules/salidaFacetada" at "./libraryModules/salidaFacetada.xq";

declare namespace bib="http://exist-db.org/bibliography";
declare option exist:serialize "method=xhtml media-type=text/html";

<div>
    <!-- IZQUIERDA -->
    <div class="column-left">
        <div class="top-left">
            <h1 class="" >Resultados para:</h1>
            <div class="">
                <p style=" font-size:15px;" id="busco"></p>
            </div>
            <div class="dotted">
            </div>
        </div> 
        
        <div id="containerRecentSearch" class="dotted" style="display:none;">
            <h1 class="" >Historial busqueda:</h1>
            <ul class="RecentSearch">
        
            </ul>
        </div>
        <div style="width:180px;color:#ffffff">.</div>
           <!--{repositorio:cargarTabsRepositorios()}-->
         <!--<div style="padding-top:10px;" >  
             <input type="button" onCLick="consultaFacetar('collection')" title="funcionalidad temporal" value="Facetar Temp" />
         </div>-->   
    </div>
    <!-- CENTRO -->
    <div class="column-center dotted-left dotted-right ">
        <!-- BUSQUEDA AVANZADA -->
        <div id="panelBusquedaAvanzada" class="containerBusquedaAvanzada dotted" >
            <h3> Búsqueda Avanzada </h3>
            <form action="javascript:consultaAvanzada()">
                <div style="width: 524px; margin: 0 auto;" >
                    <div id="avanzadaInputs" class="dotted-right">
                        <table cellspacing="3">
                            <tr>
                                <td colspan="1">
                                   <label>Título:</label>
                                </td>
                                <td>
                                    <input style="width:180px" type="text" id="title" placeholder="Título" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="1">
                                   <label>Autor:</label>
                                </td>
                                <td>
                                    <input style="width:180px" type="text" id="author" placeholder="Autor" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="1">
                                   <label>Tema:</label>
                                </td>
                                <td>
                                    <input style="width:180px" type="text" id="subject" placeholder="Tema" />
                                </td>
                               
                            </tr>
                        </table>
                    </div>
                    <div id="avanzadaSelects" style="display:none;" > 
                        <label  class="tituloIzq">
                            Institución
                        </label>
                        <!--{repositorio:listadoInstitucionesList()}-->
                        <label  class="tituloIzq">
                            Repositorio
                        </label>          
                        {repositorio:listadoRepositoriosList()}
                    </div>
                    <div style="float:left; padding-left:10px; margin-top:78px;">
                        <input type="submit" value="Buscar" onClick="$('#start').val(0);pagStart=1;" />
                    </div>
                    <div class ="clear"></div>
                </div>
            </form>     
        </div>
        <!-- NAVEGACION MENU -->
        <div id="menu-navegacion">
            {repositorio:listadoInstitucionesListParaNavegacion()}
            <div class="browse-materials" >   
                <div class="contBrowse">
                    <table cellspacing="0" border="0">
                        <thead><tr>
                          <th width="50%">Tipo Documento</th>
                        </tr></thead>
                        <tbody><tr>
                          <td><div class="navegacion_cont_item" data-item="objecttype">{salidaFacetada:navegacion("objecttype","")}</div></td>
                        </tr></tbody>
                     </table>
                     <table width="50%" cellspacing="0" border="0">
                        <thead><tr>
                          <th width="50%">Contexto</th>
                        </tr></thead>
                        <tbody><tr>
                          <td><div class="navegacion_cont_item" data-item="context">{salidaFacetada:navegacion("context","")}</div></td>
                        </tr></tbody>
                     </table>
                </div>
                <div class="clear" ></div>
                <div class="contBrowse">
                    <table cellspacing="0" border="0">
                        <thead><tr>
                          <th width="50%">Lenguaje</th>
                        </tr></thead>
                        <tbody><tr>
                          <td><div class="navegacion_cont_item" data-item="language">{salidaFacetada:navegacion("language","")}</div></td>
                        </tr></tbody>
                     </table>
                     <table cellspacing="0" border="0">
                        <thead><tr>
                          <th width="50%">Año</th>
                        </tr></thead>
                        <tbody><tr>
                          <td><div class="navegacion_cont_item" data-item="year">{salidaFacetada:navegacion("year","")}</div></td>
                        </tr></tbody>
                     </table>
                </div>
                <div class="clear" ></div>
            </div>
        </div>
        <!-- PAGINATION DIV -->
        <div id="paginationContainer" class="dotted" style="width:590; margin-left:auto; margin-right:auto; min-height:25px;">
            <div class="left-Pagination" style="width:300px;">
                <div id="pagination" ></div>          
            </div>
            <!-- SORT BY -->
            <!--<div style=""  class="right-orderBy">
                <label>Orden</label>
                <select name="orderby" id="orderby" >
                  
                    <option value="" selected="selected" >Todos</option>
                    <option value="date">Fecha</option>                     
                    <option value="subject">Tema</option>                  
                    <option value="author">Autor</option>
                  
                </select>
            </div>-->
        </div>
        <div id="contenedorResultados" class="">     
             <div id="resultados" class="content">
             </div>   
        </div>
    </div>
    <!-- DERECHA -->
    <div class="column-right" style="display:none;">
        <h1  class="dotted" >Redefina su búsqueda:</h1> 
        <!-- FACETACION -->   
        <div id="facetDivs" style=" margin-top:10px; margin-bottom:10px;">
            <div id="institutionFacetContainer" style="display:none;" >
                <a class="expand-button expanded" data-id="institutionF"></a>
                <label class="tituloIzq">Institución</label>
                <div class="clear"></div>
                <div class="tabs" id="institutionF">
                        <div class="institutionF dotted"  id="ddlColeccionContainer">
                            <div class="content">

                            </div>
                        </div>
                </div>
            </div>
            <div id="objecttypeFacetContainer" style="display:none;">
                <a class="expand-button expanded" data-id="objecttypeF"></a>
                <label class="tituloIzq">Tipo</label>
                <div class="clear"></div>
                <div class="tabs" id="objecttypeF">
                        <div class="objecttypeF dotted" id="ddlColeccionContainer">
                            <div class="content">

                            </div>
                        </div>
                </div>
            </div>
            <div id="difficultyFacetContainer" style="display:none;">
                <a class="expand-button expanded" data-id="difficultyF"></a>
                <label class="tituloIzq">Dificultad</label>
                <div class="clear"></div>
                <div class="tabs" id="difficultyF">
                        <div class="difficultyF dotted" id="ddlColeccionContainer">
                            <div class="content">

                            </div>
                        </div>
                </div>
            </div>
            <div id="contextFacetContainer" style="display:none;" >
                <a class="expand-button expanded" data-id="contextF"></a>
                <label class="tituloIzq">Contexto</label>
                <div class="clear"></div>
                <div class="tabs" id="contextF">
                        <div class="contextF dotted" id="ddlColeccionContainer">
                            <div class="content">

                            </div>
                        </div>
                </div>
            </div>
            <div id="languageFacetContainer" style="display:none;" >
                <a class="expand-button expanded" data-id="languageF"></a>
                <label class="tituloIzq">Contexto</label>
                <div class="clear"></div>
                <div class="tabs" id="languageF">
                        <div class="languageF dotted" id="ddlColeccionContainer">
                            <div class="content">

                            </div>
                        </div>
                </div>
            </div>
            <div id="yearFacetContainer" style="display:none;" >
                <a class="expand-button expanded" data-id="yearF"></a>
                <label class="tituloIzq">Año</label>
                <div class="clear"></div>
                <div class="tabs" id="yearF">
                        <div class="yearF dotted" id="ddlColeccionContainer">
                            <div class="content">

                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="./resources/js/app/navegacion.js"></script>
</div>
