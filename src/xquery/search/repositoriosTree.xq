xquery version "1.0" ;
(:metadata: LOM:)

import module namespace application_layout="http://exist-db.org/modules/application_layout" at "./layout/application_layout.xq";
 
declare namespace bib="http://exist-db.org/bibliography";
declare option exist:serialize "method=xhtml media-type=text/html";

let $repositoriosTreeContent :=
    <div>
        <!-- NAVEGACION -->
        <div style="height: 38px;">
            <div id="botones-top">
                <ul class="nav">
                    <li>
                        <a href="/bdcol/search/repositoriosTree.xq" class="dotted-right" style=" padding-left:10px; padding-right:10px;" id="repositoriosLink">Repositorios</a>
                    </li>                
                    <li>
                        <a href=""class="dotted-right" style="padding-right:10px;  padding-left:10px;" id="navegacionLink">Navegación</a>
                    </li>
                     <li>
                        <a href="/bdcol/search/index.xq" style="padding-left:10px;"  id="">Búsqueda</a>
                    </li>
                </ul>
            </div>
        </div>
        <!--PANEL INFORMATION -->
        <div id="panelInformationSearch" class="panel">
             <ul class="left">
                    <li>
                    </li>
                    <li class="last">
                        <select name="batch_size" id="perpage">
                          
                            <option selected="selected" value="10">10</option>
                          
                            <option value="20">20</option>
                          
                            <option value="50">50</option>
                          
                            <option value="100">100</option>
                          
                        </select>
                        resultados por pagina
                    </li>
                </ul>
            
            <!-- INFORMACIÓN OCULTA -->
            <filedset style="display:none;">
                <input type="hidden" id="start" value="0"/>
                <input type="hidden" id="total" />
            </filedset>
        
        </div>
        <!-- CORE CONTAINER -->
        <div id="container-core" style="height:650px">
                <iframe width="100%" height="100%" frameborder="0" name="bdcol_header" src="../../admin/TreeRepositories.jsp"></iframe>
        </div>
    </div>
return
    application_layout:default($repositoriosTreeContent)

 