function parseScript(_source) {
	var source = _source;
	var scripts = new Array();
	//Strip out tags
	while(source.indexOf("<script") > -1 || source.indexOf("</script") > -1) {
		var s = source.indexOf("<script");
		var s_e = source.indexOf(">", s);
		var e = source.indexOf("</script", s);
		var e_e = source.indexOf(">", e);
		//Add to scripts array
		scripts.push(source.substring(s_e+1, e));
		//Strip from source
		source = source.substring(0, s) + source.substring(e_e+1);
	}
	//Loop through every script collected and eval it
	for(var i=0; i<scripts.length; i++) {
		try {
			eval(scripts[i]);
		}
		catch(ex) {
			//do what you want here when a script fails
		}
	}
	//Return the cleaned source
	return source;
} 

var loadingContent = null;
function loadContent(url, to)
{
    //Por si ya hay una petici�n previa al servidor
	if(loadingContent){
		loadingContent.abort();
	}
	loadingContent = $.ajax({
		type: "POST",
		url: url,
		dataType: "html",
		timeout: 8000, //Si se cumple este timepo ser� un error
		beforeSend: function(){
		},
		complete: function(){
		},
		success: function(data){
			$("#"+to).html(data);
			var output = data;
			parseScript(output);
		},
		error: function(result){
		}
	});
} 

function caseInsensitiveCompare(a, b) {
	var anew = a.toLowerCase();
	var bnew = b.toLowerCase();
	if (anew < bnew) return -1;
	if (anew > bnew) return 1;
	return 0;
}


function noChar(s){
	return s.replace(/\u00e1|\u00e9|\u00ed|\u00f3|\u00fa|\u00f1/i,'?'); // Para quitar las tildes y poner '?'
}
var crossDomain =null;
//Un lulito de funci�n, para traer cosas de otro dominio!!!
function requestCrossDomain( site, callback ) {
	
	if(crossDomain)
		crossDomain.abort();
	
	// If no url was passed, exit.
	if ( !site ) {
		alert('No site was passed.');
		return false;
	}

	// Take the provided url, and add it to a YQL query. Make sure you encode it!
	var yql = 'http://query.yahooapis.com/v1/public/yql?q=' + encodeURIComponent('select * from xml where url="' + site + '"') + '&format=xml&callback=?';

	// Request that YSQL string, and run a callback function.
	// Pass a defined function to prevent cache-busting.
	crossDomain = $.getJSON( yql, function (data) {
		// If we have something to work with...
		if ( data.results[0] ) {
			// Strip out all script tags, for security reasons.
			// BE VERY CAREFUL. This helps, but we should do more.
			data = data.results[0].replace(/<script[^>]*>[\s\S]*?<\/script>/gi, '');
	        
			// If the user passed a callback, and it
			// is a function, call it, and send through the data var.
			if ( typeof callback === 'function') {
				callback(data);
			}
		}
		// Else, Maybe we requested a site that doesn't exist, and nothing returned.
		else throw new Error('Nothing returned from getJSON.');
		});

	
}

