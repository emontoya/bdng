$(function() {
	
	$(document).on("click", ".adminActions", actionFromAdmin);
	
	
});

actionAdmin = null;
function actionFromAdmin(){
	
	obj = this;
	
	action=$(this).data("action");
	idIdentifier=$(this).parent().data("id");
	institucion=$(this).parent().data("institution");
	repname=$(this).parent().data("repname");
	
	console.log(action+" "+idIdentifier);
	
	var url = "procesarAdministracion.xq";
	
	if(actionAdmin){
		actionAdmin.abort();
	}
	
	var cadenaFormulario = "";

	cadenaFormulario += "action="+action;
	cadenaFormulario += "&idIdentifier="+escape(idIdentifier);
	cadenaFormulario += "&institucion="+escape(institucion);
	cadenaFormulario += "&repname="+escape(repname);
	
	
	
	actionAdmin  = $.ajax({
			type: "POST",
            data: cadenaFormulario,
            url: url,
            cache: false,
            timeout: 20000,
            dataType: "html",
            beforeSend: function(){
            	$("#cargando").show();
            	
            },
            complete: function(){
            	$("#cargando").hide();
            },
            success: function( data ) {
            	
            	console.log(data);
            	
            	$("#cargando").hide();
            	
            	$(obj).closest(".infoData").addClass("box"+action.toUpperCase().charAt(0) + action.substring(1));
            	


            },
            error: function(result){
            	if(result.statusText != "abort"){
            		console.log("Error: "+result.statusText);
            		humane.error("Problemas en el servidor, Intentelo otra vez en unos minutos.");
            	}
            }
        });
	
}