
$(function() {
	
	$(".contBrowse table tbody").each(function(){
		
		if ($(this).find(".content li").length == 0){
			$(this).parent().hide();
			$(this).parent().siblings("table").css("width","98%");
		}
	});
	
	//Seleccionan una institución en el checkbox 
	$("#institution").chosen({allow_single_deselect:true}).change( function(){   
		//Ocultaciones
		$("#resultados").slideUp();
		$(".column-right").hide(); //Escondo barra derecha
		$("#pagination").fadeOut();
		$(".browse-materials").slideDown();
		$("#panelInformationSearch ul.left").slideUp("slow");
		
		var institucion = $(this).val();
		$(".navegacion_cont_item").each(function(){
			var item = $(this).data("item");
			$(this).load("procesarNavegacion.xq",{typesearch:"navegacionFacetar", item: item, institution:institucion }, function(){
				//Pongo el scroller bonito
				$(this).find(".nano").each(function(){
					$(this).nanoScroller();
				});
			});
		});
	});

	$(".navegacion_cont_item").find(".nano").each(function(){
		$(this).nanoScroller();
	});
	
	$( document ).on( "click", ".navegacion_cont_item li a", function( e ) {
		asRefineData = new Object(); //Reseteo los seleccionados de la facetacion
		navegacion=true;
		consultaAvanzada(this);
		
		$(".browse-materials").slideUp();
		$("#resultados").slideDown();
	} ); 
});





