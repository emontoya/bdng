/*** GOOGLE ANALYTICS ***/
 var _gaq = _gaq || [];
 _gaq.push(['_setAccount', 'UA-32202558-1']);
 _gaq.push(['_trackPageview']);

 (function() {
   var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
   ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
   var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
 })();
/****/

/**********  GLOBAL VAIRABLES **********/

//Opciones del spinner (loading)
var opts = {
		 lines: 16, // The number of lines to draw
		  length: 1, // The length of each line
		  width: 5, // The line thickness
		  radius: 16, // The radius of the inner circle
		  color: '#fff', // #rgb or #rrggbb
		  speed: 1, // Rounds per second
		  trail: 73, // Afterglow percentage
		  shadow: false // Whether to render a shadow
};


//Mapeo de los Id's de las vistas con las del back-end
var mapFacetar = new Object();
mapFacetar["objecttype"] = "educational.learningResourceType";
mapFacetar["context"] = "educational.context";
mapFacetar["difficulty"] = "difficulty.context";
mapFacetar["language"] = "general.language";


/*//Live search vars
var livingSearch = null;
var deadKeys = [8,13,16,32,37,38,39,40]; // Keys like "down, up, delete etc..."
var alreadySearchedWords = new Array();
var itemsLoaded = new Array();
*/
/********** INITIALIZE **************/

var pagStart = 1;
var typeConsult = null;
var actualConsult = null;
var navegacion =false;

$(document).ready(function() {
	
	
	
	$.reject({  
	    reject: {
	        safari: false, // Apple Safari  
	        chrome: false, // Google Chrome  
	        firefox: false, // Mozilla Firefox  
	        msie5: true, msie6: true, msie7: true, msie8: true, // Microsoft Internet Explorer  
	        opera: false, // Opera  
	        konqueror: true, // Konqueror (Linux)  
	        unknown: false // Everything else  
	    }, // Reject all renderers for demo
	    display: ['chrome','safari','firefox','opera'],
	    close: true//, // Prevent closing of window  
	  /*  header: '��Este navegador NO SIRVE PARA NADA!!', // Header Text 
	    paragraph1: '�Respetate!', // Warning about closi 	ng 
	    paragraph2: 'Aqu� algunas sugerencias...' // Paragraph 2 
	    closeMessage: 'Close this window at your own demise!' // Message below close window link 	
	//paragraph2: 'To exit, you must <a href="javascript:window.location=window.location.pathname;">refresh the page</a>' // Display refresh link  */
	});  
	
	
	/******** YEAR SLIDER ********/
	var minYearRange=parseInt($("#minRangeYear").val());
	var maxYearRange=parseInt($("#maxRangeYear").val());
	$( "#slider-range" ).slider({
		range: true,
		min: minYearRange,
		max: maxYearRange,
		values: [ minYearRange, maxYearRange ],
		slide: function( event, ui ) {
			$( "#yearRange" ).val(ui.values[ 0 ] + " - " + ui.values[ 1 ] );
			$("#minRangeYear").val(ui.values[ 0 ]);
			$("#maxRangeYear").val(ui.values[ 1 ]);
			if(ui.values[ 0 ]!=minYearRange || ui.values[ 1 ]!=maxYearRange){
				requestCrossDomain('http://utilitymill.com/api/xml/utility/Regex_For_Range/45/run?MIN='+ui.values[ 1 ]+'&MAX='+ui.values[ 0 ]+'&WORDBOUNDARY=on&WHOLELINE=off&SUPPORT_LEADING_ZEROES=on',function(data){
					var respServXML =$(data).find("output").html();
					var expression = respServXML.substr(11,respServXML.length-11);
					expression = expression.substr(0,expression.length-6);
					$("#rangeYearRegExpr").val(expression);
				});
			}else{
				$("#rangeYearRegExpr").val("");
			}
			
		}
	});
	$( "#yearRange" ).val( $( "#slider-range" ).slider( "values", 0 ) +
		" - " + $( "#slider-range" ).slider( "values", 1 ) );
	
	
	
	/******** LIVE SEARCH ********/
	/*
	$( "#query" ).keyup(function(e) {
		
		var wordQuery = $(this).val();
		
		//Check the key that the user is typing and the content of the query
		if($(this).val().length > 2 && (jQuery.inArray(e.which, deadKeys) == -1) 
				&& (jQuery.inArray(wordQuery, alreadySearchedWords) == -1)){
			
			if(livingSearch != null)
				livingSearch.abort();

			livingSearch = $.ajax({
								url: "./libraryModules/features/liveSearch.xq",
								dataType: "xml",
								data: {query:wordQuery},
								beforeSend: function (){
									$( "#query" ).addClass("thinking");
								},
								complete: function(){
									$( "#query" ).removeClass("thinking");
								},
								success: function( xmlResponse ) {
						        	
						        	$(xmlResponse).find("term").each(function(i) {
						        		if($(this).text() != '' && (jQuery.inArray($(this).text(), itemsLoaded) == -1))
						        			itemsLoaded[itemsLoaded.length] = $(this).text();
						            });
						        	$( "#query" ).autocomplete({
										source: itemsLoaded
									});
						        	
						        	alreadySearchedWords[alreadySearchedWords.length] = wordQuery;
								}
							});
		}
	});
	*/
	/**************************/
	/*************************/
	
	//Tiptip
	$(".panelAvanzada").tipTip();
	
	// Spin.js, para el 'cargando...'
	var target = document.getElementById('cargando');
	var spinner = new Spinner(opts).spin(target);
	$("#cargando").hide();
	
	/******** SELECTS ADVANCE SEARCH ********/
	
	$("#institution").val('');
	$("#repositorio").val('');
	
	//Activo el CHOSEN de institucion
	 $("#institution").chosen({allow_single_deselect:true}).change( function(){
		 var institucion = $(this).val();
		 
		 $.post("procesarBusquedad.xq", { typesearch: "repInfo", institucion: institucion },
				   function(data) {
			 			$("#repositorio").html("<option selected='selected' value=''></option>"+data);
			 			$("#repositorio").trigger("liszt:updated");
			 			setTittles("repositorio");
			 			
			 			
				   });
		 
		 //console.log($(this).val());
	 });

	$("#repositorio").chosen({allow_single_deselect:true});
	
	setTittles("repositorio");
	setTittles("institution");
	/******** ********************* ********/
	
	
	$("#perpage").bind("change",function(){
			pagStart=1;
			$("#start").val(0);
			(typeConsult == "basica")?consulta():consultaAvanzada();		
		});
	
	$(".expand-button").click(function(){
		var idCont =  $(this).data("id");
		$("#"+idCont).slideToggle("slow");
		
		 $(this).toggleClass("expanded");
		 $(this).toggleClass("collapsed");
		
	});
	
	//Efecto panel avanzado de busqueda
	$(".panelAvanzada").click(function(){
			$("#panelBusquedaAvanzada").slideToggle("slow");
		}
	);
	
	
	/****** ACCIONES DE LOS  LINKS *********/
	//Link para ver mas informaci�n
	$(".bottomInfo .masDetalles").live("click",function(){
		$(this).closest('.infoData').fadeOut(400,function(){
			$(this).siblings().fadeIn();
		});
	});
	//Link para la NAVEGACION
	$("#navegacionLink").on( "click", function( e ) {
		e.preventDefault();
		
		$("#resultados").html("");
        $("#total-results").html("");
    	$("#cargando").show();
    	$("#panelInformationSearch ul.left").slideUp("slow");
    	$(".containerBusquedaBasica").hide("slow");
    	$(".panelAvanzada").hide()
    	$("#pagination").fadeOut();
    	$("#query").val("");
    	$("#busco").text("");
    	
    	//Cargo toda la navegacion
		$("#container-core").load('navegacion.xql',function(){
			$("#cargando").hide();
			$(".panelAvanzada").slideUp();
		});
	} );
	
	//Link para el admin set
	$( document ).on( "click", ".adminSetElementIcon", function(e){
		
		$(this).closest('table').siblings(".adminAdminElement").slideToggle();
	});
	
	
	$( "#navegacionBuscar" ).on( "click", function( e ) {
		e.preventDefault();
		location.reload(true);
	} );
	/****** FIN LINKS *********/
	
	//Ojo pal paginaitor que pone mucho problema
		pagination(2,1,1);
		$("#pagination").hide();	
	//Refine you search
	$( document ).on( "click", "#ddlColeccionContainer input:checkbox", refineBusqueda);
	
});

//Titles para los datos del select con el nombre bonito.
function setTittles(id){
	
	var tempLongNames = new Array();
	$("#"+id).find("option").each(function(index){
		if (index != 0) {
			tempLongNames[tempLongNames.length] = $(this).data("long");
		}
		
	});
	//Pego los Long names al tittle de cada li de las lista chosen
	$("#"+id+"_chzn").find("ul.chzn-results li").each(function(index){
		$(this).attr("title",tempLongNames[index]);
	});
	
}

function pagination(count,start,display){
	
	$(".left-Pagination").find("#pagination").remove();
	$(".left-Pagination").html("<div id='pagination'></div>");
	$("#pagination").paginate({
		count 		: count,
		start 		: start,
		display     : display,
		border					: false,
		text_color  			: '#666',
		background_color    	: '#fff',	
		text_hover_color  		: 'black',
		background_hover_color	: '#CFCFCF',
		onChange     			: function(page){
									$("#start").val(($("#perpage option:selected").val() * (page-1)));
									if(typeConsult == "basica"){
										consulta();
									}else if(typeConsult == "avanzada"){
										consultaAvanzada("pagination");    
									}else{
										refineBusqueda();
									}
									pagStart=page;
								  }
    });
	
}

//Prevenir dos llamados al sevidor al mismo tiempo
var consultado = null;
function consulta(queryVal){

	//Reseteo variabels para la facetacion
	asRefineData = new Object(); //{"query"=> parent}
	asRefineDataString = "";
	
	typeConsult="basica";
	actualConsult = "basica";
	if(consultado){
		consultando.abort();
	}

	var url = "procesarBusquedad.xq";
	
	//Verifico si llega algo por parametro
	if(queryVal === "" || queryVal === undefined){
		queryVal =  $("#query").val();
	}else{
		$("#query").val(queryVal);
	}
	var perpage = $("#perpage option:selected").val();
	var start = $("#start").val();

	
	$("#busco").text(queryVal);

	var cadenaFormulario = "";
	
	cadenaFormulario += "query="+noChar(queryVal).removeStopWords();;
	cadenaFormulario += "&typesearch=basica";
	cadenaFormulario += "&perpage="+perpage;
	cadenaFormulario += "&start="+start;
	
	/*var start = new Date().getTime();*/

	
	consultando  = $.ajax({
			type: "POST",
            data: cadenaFormulario,
            url: url,
            cache: false,
            timeout: 25000,
            dataType: "html",
            beforeSend: function(){
            	$("#resultados").html("");
                $("#total-results").html("");
            	$("#cargando").show();
            	$("#panelInformationSearch ul.left").slideUp("slow");
            	$("#pagination").fadeOut();
            	
            	
            },
            complete: function(){
            	$("#cargando").hide();
            	/*var duration = (new Date().getTime() - start) / 1000;
            	alert(duration);*/
            },
            success: function( data ) {

            	
            	$("#cargando").hide();
            	$(".top-left").show();
            	
            	var stringTotal  = parseInt($(data).find("#totalEncontrados").html());
            	var results = $(data).find("#searchresults");
            	var mostrandoTantos = $(data).find("#mostrandoTantos").html();
            	var numPages = $(data).find("#numberPages").html();
            	//var paginations = $(data).find("#search-pagination").html();

            	
            	//Informaci�n del centro (Datos encontrados)
            	$("#contenedorResultados").addClass("nano");
                $("#resultados").html(results);
                $("#contenedorResultados.nano").nanoScroller();
                
                if(stringTotal > 0){
                	
	                $("#panelInformationSearch").find("ul.left li").eq(0).html(mostrandoTantos);
	                
	                $("#panelInformationSearch ul.left").slideDown("slow");
	                
	                //Muestro la columna d ela derecha de facetacion
	                ($(".column-right").css("display") == "none")?$(".column-right").show("slide",{direction: 'left'}):null;
	                
	                //$("#pagination").html(paginations);
	                
	                (numPages > 100) ? numPages = 100 : numPages ;
	                
	                if(numPages > 1){
	                    $("#pagination").fadeIn();
	                    pagination(numPages,pagStart,5);
	                }else{
	                	$("#pagination").fadeOut();	
	                }
	                $("#paginationContainer").slideDown("slow");
	                
	                if($("#containerRecentSearch").css("display") == "none")
	                	$("#containerRecentSearch").show();
	                
	                //Agrego busqueda al historial
	                if($("#containerRecentSearch ul").text().indexOf(queryVal) == -1){
	                	var link = "<a onClick='consulta(\""+queryVal+"\")'>"+queryVal+" ("+stringTotal+")"+"</a>";
	                	$("#containerRecentSearch ul").prepend("<li>"+link+"</li>");
	                }
	                
	                /* TERMINOS CON LOS CUALES DESEO FACETAR */
	                consultaFacetar("institution");
	                consultaFacetar("objecttype");
	                consultaFacetar("difficulty");
	                consultaFacetar("context");
	                consultaFacetar("language");
	                consultaFacetar("year");
	                	
                }else{
                	$("#paginationContainer").slideUp("slow");
                }
                
                

            },
            error: function(result){
            	if(result.statusText != "abort"){
            		console.log("Error: "+result.statusText);
            		console.log("Problemas en el servidor, Intentelo otra vez en unos minutos.");
            		//humane.error("Problemas en el servidor, Intentelo otra vez en unos minutos.");
            	}
            }
        });
}

//Prevenir dos llamados al sevidor al mismo tiempo
var consultadoAvanzada = null;
function consultaAvanzada(obj){
	
	asRefineData = new Object(); 
	asRefineDataString ="";
	
	typeConsult="avanzada";
	actualConsult = "avanzada";
	//Limpio los inputs de la consulta b�sica
	$("#query").val("");
	$("#busco").text("");
	
	if(consultadoAvanzada){
		consultadoAvanzada.abort();
	}
	
	var url = "procesarBusquedad.xq";
	
	var title = $("#title").val();
	var author = $("#author").val();
	var subject = $("#subject").val();
	var institution = $("#institution").val();
	var repositorio = $("#repositorio").val();
	var perpage = $("#perpage option:selected").val();
	var start = $("#start").val();

	
	//Cadena con la informaci�n que se enviar� al servidor	
	var cadenaFormulario = "";

	cadenaFormulario += "title="+noChar(title);
	cadenaFormulario += "&author="+noChar(author);
	cadenaFormulario += "&subject="+noChar(subject);
	cadenaFormulario += "&institution="+noChar(institution);
	cadenaFormulario += "&repositorio="+noChar(repositorio);
	
	//El yearRange puede ser nulo en la vista de navegaci�n
	yearRange = ($("#rangeYearRegExpr").val() != undefined)?escape($("#rangeYearRegExpr").val()):"";
	
	cadenaFormulario += "&yearRange="+yearRange;
	cadenaFormulario += "&typesearch=avanzada";
	cadenaFormulario += "&perpage="+perpage;
	cadenaFormulario += "&start="+start;
	(obj != undefined && obj == "pagination")?cadenaFormulario += "&pagination=pagination":null;
	//Esto si el Request viene desde navegacion //REQUEST DE NAVEGACION
	var preSelected = new Object();
	if (navegacion){
		
		var parent = $(obj).closest(".navegacion_cont_item").data("item");
		var hijo = $(obj).data("value");
		
		//OJO AL CAMBIO
		parent = (mapFacetar[parent] != undefined)?mapFacetar[parent]:parent;
		
		(institution!="")?preSelected['institution']=institution:null;
		preSelected[parent] = hijo; // Para enviar como parametro en la facetada
		
		//Van a estar chequiados por defecto en la facetacion
		asRefineData[parent] = new Array();		
		asRefineData[parent].push(hijo);
		
		for (var i in asRefineData) {
			var parent = (mapFacetar[i] != undefined)?"dc:"+mapFacetar[i]:"dc:"+i;
			asRefineDataString +="[ft:query("+parent+",<query>";
            for(var j in asRefineData[i]){
                asRefineDataString +="<phrase>"+asRefineData[i][j]+"</phrase>";
            }
            asRefineDataString +="</query>)]";
		}
		cadenaFormulario += "&navegacion="+escape(asRefineDataString);
		
		start = 0; //Para que siempre la primera pagina sea la 1
		pagStart=1;
		
	}
	consultadoAvanzada  = $.ajax({
		type: "POST",
        data: cadenaFormulario,
        url: url,
        cache: false,
        timeout: 25000,
        dataType: "html",
        beforeSend: function(){
        	$("#resultados").html("");
            $("#total-results").html("");
        	$("#cargando").show();
        	$("#panelInformationSearch ul.left").slideUp("slow");
        	$("#pagination").fadeOut();
        	
        },
        complete: function(){
        	$("#cargando").hide();
        },
        success: function( data ) {
        	
        	$("#cargando").hide();
        	$(".top-left").show();
        	
        	var stringTotal  = parseInt($(data).find("#totalEncontrados").html());
        	var results = $(data).find("#searchresults");
        	var mostrandoTantos = $(data).find("#mostrandoTantos").html();
        	var numPages = $(data).find("#numberPages").html();
        	var paginations = $(data).find("#search-pagination").html();

        	
        	//Informaci�n del centro (Datos encontrados)
        	$("#contenedorResultados").addClass("nano");
            $("#resultados").html(results);
            $("#contenedorResultados.nano").nanoScroller();
            
            if(stringTotal > 0){
            	
                $("#panelInformationSearch").find("ul.left li").eq(0).html(mostrandoTantos);
                
                $("#panelInformationSearch ul.left").slideDown("slow");
                
                //Muestro la columna d ela derecha de facetacion
                ($(".column-right").css("display") == "none")?$(".column-right").show("slide",{direction: 'left'}):null;
                
                $("#pagination").html(paginations);
                
                if(numPages > 100)
                	numPages = 100;
                
                if(numPages > 1){
                	pagination(numPages,pagStart,5);
                	$("#pagination").fadeIn();
                }else{
                	$("#pagination").fadeOut();
                }
                $("#paginationContainer").slideDown("slow");
                
              //Chequeo para la navegacion
                if(navegacion){
                	state = "navegacion"
                	navegacion=false;
                }else{
                	state=""
                }
                //No faceto si viene de pagination
                if(!(obj != undefined && obj == "pagination")){
                	/* TERMINOS CON LOS CUALES DESEO FACETAR */
                    consultaFacetar("institution",state,preSelected);
                    consultaFacetar("objecttype",state,preSelected);
                    consultaFacetar("difficulty",state,preSelected);
                    consultaFacetar("context",state,preSelected);
                    consultaFacetar("language",state,preSelected);
                    consultaFacetar("year",state,preSelected);
                } 
                
                
            }else{
            	$("#paginationContainer").slideUp("slow");
            }
            
        },
        error: function(result){
        	if(result.statusText != "abort"){
        		console.log("Problemas en el servidor, Intentelo otra vez en unos minutos.")
        		//humane.error("Problemas en el servidor, Intentelo otra vez en unos minutos.");
        	}
        }
    });
}

var facetando = null;
/*Funcion para realizar el facetamiento despues de la consulta
 * (Parametros de entrada)
 * 		type: Tipo de facetaci�n, criterio por el cual se desea facetar
 * 		state: Est� parametro es recibido solo cuando se desea facetar al comienzo de la navegaci�n
 * 		preSelected: Hash map con los que se deben preSeleccionar. (SOlo se envia en la navegacion)
 * */

function consultaFacetar(type,state,preSelected){
	

	var url = "procesarBusquedad.xq";

	var cadenaFormulario = "";

	cadenaFormulario += "typesearch=facetada";
	cadenaFormulario += "&type="+type;
	//Para la facetada por navegacion
	if(state != undefined && state != ""){
		cadenaFormulario += "&state="+state;
	}
	

	facetando  = $.ajax({
		type: "GET",
		url: url,
		data: cadenaFormulario,
		dataType: "xml",
		contentType: "text/xml;",
		beforeSend: function(){

		},
		complete: function(){

		},
		success: function( xml ) {

			var items = new Array();

			$(xml).find("term").each(function(i) {
				if($(this).text() != ''){
					var node = "";
					var txt = $(this).text();
					var innerType =(mapFacetar[type] != undefined)?mapFacetar[type]:type;
					if(preSelected != undefined){
						if((preSelected[innerType] != undefined)  && preSelected[innerType] == txt){
							
							var chkd = (innerType=="institution")?"checked='checked' DISABLED":"checked='checked'";
						}
					}
					var dataParent = (mapFacetar[innerType] != undefined)?mapFacetar[innerType]:innerType; //Verificar ya que Jqueyr pone problema por los '.' en los nombres
					node += "<input type='checkbox' data-parent='"+dataParent+"' data-id='"+txt+"' "+chkd+"/>"+txt.toUpperCase();
					node += "("+$(this).attr("freq")+")";
					items[items.length] = node;
				}
			});

			var stringPrintArray = "";
			for(var i=0;i<items.length;i++){
				stringPrintArray += "<label defaultselection='false'>"+items[i]+"</label>";
			}

			$("#"+type+"FacetContainer").show();

			//console.log("ea el type: "+type+" Ealo ->"+$(xml).find("term").size());
			
			//Pongo el scroller
			$("."+type+"F").addClass("nano");
			$("#facetDivs").find("#"+type+"F"+" #ddlColeccionContainer .content").html(stringPrintArray);
			$("."+type+"F.nano").nanoScroller();
			//Si no me traj� ning�n resultado no lo muestro
			($(xml).find("term").size() == 0)?$("#"+type+"FacetContainer").css("display","none"):$("#"+type+"FacetContainer").css("display","");
		},
		error: function(result){
			if(result.statusText != "abort"){
				console.log("Problemas en el servidor, Intentelo otra vez en unos minutos.");
				//humane.error("Problemas en el servidor, Intentelo otra vez en unos minutos.");
			}
		}
	});

}


// show the values stored


var asRefineData = new Object(); //{"query"=> parent}
var asRefineDataString = "";
function refineBusqueda(){

	typeConsult = "refineBusqueda";

	var url = "procesarBusquedad.xq";

	var perpage = $("#perpage option:selected").val();
	var start = $("#start").val();
	asRefineDataString = "";
	var paginationRefine  = !(this == '[object HTMLInputElement]');
	//Si es paginacion no nesecito hacer esto
	if(!paginationRefine){
		if($(this).attr('checked')){
			//Si no tiene el array de los hijos creado se lo creo.
			if(asRefineData[$(this).data("parent")] == undefined){
				asRefineData[$(this).data("parent")] = new Array();
			}
			//Obtengo el contenedor de todos los inputs y escojo los que est�n chequiados
			$(this).closest(".content").find("input[type=checkbox]:checked").each(function(){
				if(jQuery.inArray($(this).data("id"), asRefineData[$(this).data("parent")] ) == -1 )// si no est� lo meto
					asRefineData[$(this).data("parent")].push($(this).data("id"));

			});
		}else{
			var position = jQuery.inArray($(this).data("id"), asRefineData[$(this).data("parent")] );
			asRefineData[$(this).data("parent")].splice(position,1);
			//Si el parent queda solo, lo elimino
			if(asRefineData[$(this).data("parent")].length == 0)
				delete asRefineData[$(this).data("parent")];
		}
		//((i == "language"|| i == "publisher")?i+"[1]":i)
		for (var i in asRefineData) {
			var parent ="";
			if(i == "difficulty"){
				parent = "dc:educational.difficulty";
			}else if(i == "context"){
				parent = "dc:educational.context";
			}else{
				parent = "dc:"+i;
			}
			asRefineDataString +="[ft:query("+parent+",<query>";
            for(var j in asRefineData[i]){
                asRefineDataString +="<phrase>"+asRefineData[i][j]+"</phrase>";
            }
            asRefineDataString +="</query>)]";
		}
		start = 0; //Para que siempre la primera pagina sea la 1
		pagStart=1;
	}
	
	var cadenaFormulario = "";


	cadenaFormulario += "typesearch=refineBusqueda";
	cadenaFormulario += "&pagination="+((paginationRefine)?'pagination':'');
	cadenaFormulario += "&perpage="+perpage;
	cadenaFormulario += "&start="+start;
	cadenaFormulario += "&actualConsult="+actualConsult;
	cadenaFormulario += "&asRefineData="+escape(asRefineDataString);

	consultando  = $.ajax({
		type: "POST",
        data: cadenaFormulario,
        url: url,
        cache: false,
        dataType: "html",
        beforeSend: function(){
        	$("#resultados").html("");
            $("#total-results").html("");
        	$("#cargando").show();
        	$("#panelInformationSearch ul.left").slideUp("slow");
        	$("#pagination").fadeOut();
        },
        complete: function(){
        	$("#cargando").hide();
        },
        success: function( data ) {

        	$("#cargando").hide();
        	$(".top-left").show();

        	var stringTotal  = parseInt($(data).find("#totalEncontrados").html());
        	var results = $(data).find("#searchresults");
        	var mostrandoTantos = $(data).find("#mostrandoTantos").html();
        	var numPages = $(data).find("#numberPages").html();

        	//Informaci�n del centro (Datos encontrados)
        	$("#contenedorResultados").addClass("nano");
            $("#resultados").html(results);
            $("#contenedorResultados.nano").nanoScroller();

            if(stringTotal > 0){

                $("#panelInformationSearch").find("ul.left li").eq(0).html(mostrandoTantos);

                $("#panelInformationSearch ul.left").slideDown("slow");


                (numPages > 100) ? numPages = 100 : numPages ;

                if(numPages > 1){
                    $("#pagination").fadeIn();
                    pagination(numPages,pagStart,5);
                }else{
                	$("#pagination").fadeOut();	
                }
                $("#paginationContainer").slideDown("slow");

                if($("#containerRecentSearch").css("display") == "none")
                	$("#containerRecentSearch").show();

            }else{
            	$("#paginationContainer").slideUp("slow");
            }

        },
        error: function(result){
        }
    });
}








