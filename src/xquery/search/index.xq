xquery version "1.0" ;
(:metadata: LOM:)
import module namespace repositorio="http://exist-db.org/modules/repositorio" at "./libraryModules/repositorios.xq";
import module namespace application_layout="http://exist-db.org/modules/application_layout" at "./layout/application_layout.xq";
 
declare namespace bib="http://exist-db.org/bibliography";
declare option exist:serialize "method=xhtml media-type=text/html";

let $indexContent :=
    <div>
        <!-- NAVEGACION -->
        <div style="height: 38px;">
            <div id="botones-top">
                <ul class="nav">
                    <li>
                        <a href="/bdcol/search/repositoriosTree.xq" class="dotted-right" style=" padding-left:10px; padding-right:10px;" id="repositoriosLink">Repositorios</a>
                    </li>
                    <li>
                        <a href=""class="dotted-right" style="padding-right:10px;  padding-left:10px;" id="navegacionLink">Navegación</a>
                    </li>
                     <li>
                        <a href="#" style="padding-left:10px;"  id="navegacionBuscar">Búsqueda</a>
                    </li>
                </ul>
            </div>
            <!-- LABEL BÚSQUEDA AVANZADA -->
            <a class="panelAvanzada" original-title="Advanced Search" title="Búsqueda Avanzada">Advanced Search</a>
            <!-- BUSQUEDA BÁSICA -->
            <!--<div id="panelBusquedaBasica" class="containerBusquedaBasica" > -->
            <form class="containerBusquedaBasica" action="javascript:consulta();">
                <input type="text" size="25" name="query" id="query" required="" placeholder="Buscar" />
                <button class="classy" type="submit"><span>Buscar</span></button>
                <!--<input type="submit" value="Buscar" onClick="$('#start').val(0);pagStart=1;"/>--> 
            </form>
            <!--</div>-->
        </div>
        <!--PANEL INFORMATION -->
        <div id="panelInformationSearch" class="panel">
            <ul class="left">
                <li>
                </li>
                <li class="last">
                    <select name="batch_size" id="perpage">
                      
                        <option selected="selected" value="10">10</option>
                      
                        <option value="20">20</option>
                      
                        <option value="50">50</option>
                      
                        <option value="100">100</option>
                      
                    </select>
                    resultados por pagina
                </li>
            </ul>
            
            <!-- INFORMACIÓN OCULTA -->
            <filedset style="display:none;">
                <input type="hidden" id="start" value="0"/>
                <input type="hidden" id="total" />
            </filedset>
        
        </div>
        
        <!-- CORE CONTAINER -->
        <div id="container-core">
            <!-- IZQUIERDA -->
            <div class="column-left">
                <div class="top-left">
                    <h1 class="" >Resultados para:</h1>
                    <div class="">
                        <p style=" font-size:15px;" id="busco"></p>
                    </div>
                    <div class="dotted">
                    </div>
                </div> 
                
                <div id="containerRecentSearch" class="dotted" style="display:none;">
                    <h1 class="" >Historial de búsqueda:</h1>
                    <ul class="RecentSearch">
                
                    </ul>
                </div>
                <div style="width:180px;color:#ffffff">.</div>
                   <!--{repositorio:cargarTabsRepositorios()}-->
                 <!--<div style="padding-top:10px;" >  
                     <input type="button" onCLick="consultaFacetar('collection')" title="funcionalidad temporal" value="Facetar Temp" />
                 </div>-->   
            </div>
            <!-- CENTRO -->
            <div class="column-center dotted-left dotted-right ">
                <!-- BUSQUEDA AVANZADA -->
                <div id="panelBusquedaAvanzada" class="containerBusquedaAvanzada dotted" >
                    <h3> Búsqueda Avanzada </h3>
                    <form action="javascript:consultaAvanzada()">
                        <div style="width: 524px; margin: 0 auto;" >
                            <div id="avanzadaInputs" class="dotted-right">
                                <table cellspacing="3">
                                    <tr>
                                        <td colspan="1">
                                           <label>Título:</label>
                                        </td>
                                        <td>
                                            <input style="width:180px" type="text" id="title" placeholder="Título" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="1">
                                           <label>Autor:</label>
                                        </td>
                                        <td>
                                            <input style="width:180px" type="text" id="author" placeholder="Autor" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="1">
                                           <label>Tema:</label>
                                        </td>
                                        <td>
                                            <input style="width:180px" type="text" id="subject" placeholder="Tema" />
                                        </td>
                                       
                                    </tr>
                                </table>
                            </div>
                            <div id="avanzadaSelects" > 
                                <label  class="tituloIzq">
                                    Institución
                                </label>
                                {repositorio:listadoInstitucionesList()}
                                <label  class="tituloIzq">
                                    Repositorio
                                </label>          
                                {repositorio:listadoRepositoriosList()}
                            </div>                            
                            <div class ="clear"></div>
                            <!-- YEAR PICKER -->
                            <div>
                                <div class="demo" style="float:left; padding-left:3px;">
                                    <p>
                                        <label for="yearRange">Año:</label>
                                        <input type="text" id="yearRange" style="border:0; color:#777777;" readonly="true"/>
                                    </p>
                                     <fieldset>
                                        <input type="hidden" id="rangeYearRegExpr"></input>
                                        <!--<div style="display:none;" id="maxAndMinYears">
                                            <input type="hidden" value="1" id="minRangeYear"></input>
                                            <input type="hidden" value="9999" id="maxRangeYear"></input>
                                        </div> -->
                                      {repositorio:minMaxYear()}
                                     </fieldset>
                                    <div id="slider-range" style="margin-left:8px; width:220px;"></div>
                                </div><!-- End demo -->
                                <div style="float:left; padding-left:70px; margin-top:12px;">
                                    <input type="submit" value="Buscar" onClick="$('#start').val(0);pagStart=1;" />
                                </div>
                            </div>
                        </div>
                    </form>     
                </div>
                <!-- PAGINATION DIV -->
                <div id="paginationContainer" class="dotted" style="width:590; margin-left:auto; margin-right:auto; min-height:25px;">
                    <div class="left-Pagination" style="width:300px;">
                        <div id="pagination" ></div>          
                    </div>
                    <!-- SORT BY 
                    <div style=""  class="right-orderBy">
                        <label>Orden</label>
                        <select name="orderby" id="orderby" >
                          
                            <option value="" selected="selected" >Todos</option>
                            <option value="date">Fecha</option>                     
                            <option value="subject">Tema</option>                  
                            <option value="author">Autor</option>
                          
                        </select>
                    </div>
                    -->
                </div>
                <div id="contenedorResultados" class="">     
                     <div id="resultados" class="content">
                     </div>   
                </div>
            </div>
            <!-- DERECHA -->
            <div class="column-right">
               <h1  class="dotted" >Filtre su búsqueda:</h1> 
                <!-- FACETACION -->   
                <div id="facetDivs" style=" margin-top:10px; margin-bottom:10px;">
                    <div id="institutionFacetContainer" style="display:none;" >
                        <a class="expand-button expanded" data-id="institutionF"></a>
                        <label class="tituloIzq">Institución</label>
                        <div class="clear"></div>
                        <div class="tabs" id="institutionF">
                                <div class="institutionF dotted"  id="ddlColeccionContainer">
                                    <div class="content">
        
                                    </div>
                                </div>
                        </div>
                    </div>
                    <div id="objecttypeFacetContainer" style="display:none;">
                        <a class="expand-button expanded" data-id="objecttypeF"></a>
                        <label class="tituloIzq">Tipo</label>
                        <div class="clear"></div>
                        <div class="tabs" id="objecttypeF">
                                <div class="objecttypeF dotted" id="ddlColeccionContainer">
                                    <div class="content">
        
                                    </div>
                                </div>
                        </div>
                    </div>
                    <div id="difficultyFacetContainer" style="display:none;">
                        <a class="expand-button expanded" data-id="difficultyF"></a>
                        <label class="tituloIzq">Dificultad</label>
                        <div class="clear"></div>
                        <div class="tabs" id="difficultyF">
                                <div class="difficultyF dotted" id="ddlColeccionContainer">
                                    <div class="content">
        
                                    </div>
                                </div>
                        </div>
                    </div>
                    <div id="contextFacetContainer" style="display:none;" >
                        <a class="expand-button expanded" data-id="contextF"></a>
                        <label class="tituloIzq">Contexto</label>
                        <div class="clear"></div>
                        <div class="tabs" id="contextF">
                                <div class="contextF dotted" id="ddlColeccionContainer">
                                    <div class="content">
        
                                    </div>
                                </div>
                        </div>
                    </div>
                    <div id="languageFacetContainer" style="display:none;" >
                        <a class="expand-button expanded" data-id="languageF"></a>
                        <label class="tituloIzq">Lenguaje</label>
                        <div class="clear"></div>
                        <div class="tabs" id="languageF">
                                <div class="languageF dotted" id="ddlColeccionContainer">
                                    <div class="content">
        
                                    </div>
                                </div>
                        </div>
                    </div>
                    <div id="yearFacetContainer" style="display:none;" >
                        <a class="expand-button expanded" data-id="yearF"></a>
                        <label class="tituloIzq">Año</label>
                        <div class="clear"></div>
                        <div class="tabs" id="yearF">
                                <div class="yearF dotted" id="ddlColeccionContainer">
                                    <div class="content">
        
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
return
    application_layout:default($indexContent)

 