xquery version "1.0" ;


import module namespace repositorio="http://exist-db.org/modules/repositorio" at "../libraryModules/repositorios.xq";
import module namespace loginF="http://exist-db.org/modules/loginF" at "../user/loginF.xq";
import module namespace configweb="http://exist-db.org/modules/configweb" at "../user/configWeb.xq";
 
declare namespace bib="http://exist-db.org/bibliography";
declare option exist:serialize "method=xhtml media-type=text/html";


    <html >
       <head>
            <title>TEST</title>
            <!-- USER JS -->
            <script type="text/javascript" src="test.js" charset="utf-8">a</script>
            <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
        
       </head>
       <style>
            
       </style>
       <body>
       <!-- CONTENEDOR TOTAL -->
       <div id="contenedor">
            <form class="containerBusquedaBasica" action="javascript:consulta();">
                <input type="text" size="25" name="query" id="query" required="" placeholder="Buscar" />
                <button class="classy" type="submit"><span>Buscar</span></button>
            </form>
            <div class="clear"></div>
            <div id="contenedorResultados" class="">    
                  <div id="info"></div>
                 <div id="resultados" class="content">
                 </div>   
            </div>
       </div>

       </body>
    </html>
