function noChar(s){
	return s.replace(/\u00e1|\u00e9|\u00ed|\u00f3|\u00fa|\u00f1/i,'?'); // Para quitar las tildes y poner '?'
}


function consulta(queryVal){


	var url = "test_procesarBusqueda.xq";

	var queryVal =  $("#query").val();
	
	$("#busco").text(queryVal);

	var cadenaFormulario = "";
	
	cadenaFormulario += "query="+noChar(queryVal);
	
	$.ajax({
			type: "POST",
            data: cadenaFormulario,
            url: url,
            cache: false,
            timeout: 35000,
            dataType: "html",
            success: function( data ) {
            	
            	var stringTotal  = parseInt($(data).find("#totalEncontrados").html());
            	var results = $(data).find("#searchresults");
            	var mostrandoTantos = $(data).find("#mostrandoTantos").html();

                $("#resultados").html(results);
                $("#info").html(mostrandoTantos);             

            },
            error: function(result){
            	alert(result);
            }
        });
}