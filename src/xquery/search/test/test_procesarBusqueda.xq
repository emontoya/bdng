xquery version "1.0";

declare namespace request="http://exist-db.org/xquery/request";
declare namespace session="http://exist-db.org/xquery/session";
declare namespace xs="http://www.w3.org/2001/XMLSchema";
declare namespace dc="http://purl.org/dc/elements/1.1/";

import module namespace kwic="http://exist-db.org/xquery/kwic";

declare option exist:serialize "method=xhtml media-type=text/html";
import module namespace confVars="http://exist-db.org/modules/confVars"  at "./../libraryModules/configVariables.xq"; (:Variables de configuración :)
import module namespace display="http://exist-db.org/modules/display"  at "./../libraryModules/display.xq";






let $q := xs:string(request:get-parameter("query",""))

let $q := replace($q, "[^0-9a-zA-ZäöüßÄÖÜáéíóúñÑ?\-,. ]", "")
(: let $query := if ($q) then concat("[ft:query(.,<query><near ordered='no'>",$q,"</near></query>)]") else () :)
let $query := if ($q) then concat("[ft:query(dc:general.title,'",$q,"')] or ") else ()
let $query2 := if ($q) then concat("[ft:query(dc:general.description,'",$q,"')]") else ()
let $query3 := if ($q) then concat("[ft:query(dc:lifecycle.contribute.role.author,'",$q,"')] or ") else ()
let $query4 := if ($q) then concat("[ft:query(dc:general.keyword,'",$q,"')]") else ()



let $data-collection := '/db/bdcol'
let $tempScope := concat('collection($data-collection)',$confVars:root)(: $confVars:root tiene el nombre del root. Ej. dc_lom,lom; dc :)
let $scope := util:eval($tempScope)
let $search-string := concat
   ('$scope', 
      $query,
      '$scope', 
      $query2
   ) 
   (:TIMES1 :)
  let $timestart := util:system-time(),
    $ts:=session:set-attribute("timestart",$timestart)
   
let $hits := 

     for $m in  util:eval($search-string)
              let $score := ft:score($m)
              order by $score descending
              return 
              $m
  
(:TIMES1-END!! :)
let $timeend := seconds-from-duration(util:system-time()-session:get-attribute("timestart")),
    $t := session:set-attribute("time", $timeend)
    
   (:TIMES2 :)
 let $timestart := util:system-time(),
    $ts:=session:set-attribute("timestart",$timestart)
     (: subsequence($sequence, $starting-item, $number-of-items) :)
     (: let $results := display:recordBasicTest($hits[position() = (0 to 100)])  :)
     
let $results := display:recordBasicTest(subsequence($hits,0, 10))
(:let $results := subsequence($hits,0, 10):)

let $timeend := seconds-from-duration(util:system-time()-session:get-attribute("timestart")),
    $t := session:set-attribute("timeDisplay", $timeend)
(:TIMES2-END!! :)
let $total-result-count := count($hits)


  let $how-many-on-this-page := 
        <label><strong>{$total-result-count}</strong> registros encontrados en <b>{session:get-attribute("time")}</b> segundos y procesó en <b>{session:get-attribute("timeDisplay")}</b></label>
        
     return
                <div>
                    <div id="data">
                     {
                        if (empty($hits)) then (
                            <div id="searchresults"><h3>No se encontraron resultados para esta consulta</h3></div>
                        )
                        else
                            (
                            <div>
                                <label id="totalEncontrados">{$total-result-count}</label>
                                <label id="mostrandoTantos">Resultados, {$how-many-on-this-page}</label>
                                <div id="searchresults">{$results}</div>,
                            </div>
                            )
                        }
                    </div>
                </div>

    
    
    
    
    
