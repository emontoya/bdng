module namespace busquedaBasica="http://exist-db.org/modules/busquedaBasica";

declare namespace request="http://exist-db.org/xquery/request";
declare namespace session="http://exist-db.org/xquery/session";
declare namespace xs="http://www.w3.org/2001/XMLSchema";
declare namespace dc="http://purl.org/dc/elements/1.1/";

import module namespace kwic="http://exist-db.org/xquery/kwic";
import module namespace display="http://exist-db.org/modules/display"  at "display.xq";
import module namespace salidaFacetada="http://exist-db.org/modules/salidaFacetada"  at "salidaFacetada.xq";
import module namespace confVars="http://exist-db.org/modules/confVars"  at "configVariables.xq"; (:Variables de configuración :)

declare option exist:serialize "method=xhtml media-type=text/html";

declare function busquedaBasica:basica() as element()*{

    let $q := xs:string(request:get-parameter("query",""))
    let $institution := replace(xs:string(request:get-parameter('institution', '')), "[^0-9a-zA-ZäöüßÄÖÜáéíóúñÑ?\-,. ]", "")
    (: In order to prevent XQuery injection attacks :)
    let $q := replace($q, "[^0-9a-zA-ZäöüßÄÖÜáéíóúñÑ?\-,. ]", "")
    (: ("[ft:query(dc:year,<query><bool><regex occur='must'>",$yearRange,"</regex></bool></query>)]") :)
    let $query := if ($q) then concat("[ft:query(.,'",$q,"')]") else ()
    
    (: Validar que el registro no esté 'rechazado' :)
    (: 0 => Aprobado :)
    (: 1 => Pendiente por verificar :)
    (: 2 => Rechazado :)
    let $notRejected := "[ft:query(dc:certified,<query><bool><term occur='should'>0</term><term occur='should'>1</term></bool></query>)]"
    
    let $hits :=
                if(session:get-attribute("basicSearchQuery") eq $q) then ( (: Si la bùsqueda es igual a la anterior :)
                    session:get-attribute("lastSearch")      
                )else(
                    let $data-collection := 
                        if($institution)then(
                            concat("/db/bdcol/",$institution)   
                        )else(
                            '/db/bdcol'
                        )
                    let $tempScope := concat('collection($data-collection)',$confVars:root)(: $confVars:root tiene el nombre del root. Ej. dc_lom,lom; dc :)
                    let $scope := util:eval($tempScope)
                    let $search-string := concat
                       ('$scope', 
                          $query,
                          $notRejected
                       ) 
                     
                         (: Organizar la busquedad :)
                     for $m in  util:eval($search-string)
                              let $score := ft:score($m)
                              order by $score descending
                              return 
                              $m
                )
    
    let $q := session:set-attribute("basicSearchQuery",$q),
        $lastSearch := session:set-attribute("lastSearch",$hits)

    let $total-result-count := count($hits)
    let $perpage := xs:integer(request:get-parameter("perpage", "10"))
    let $start := xs:integer(request:get-parameter("start", "0"))
    let $end := 
        if ($total-result-count lt $perpage) then 
            $total-result-count
        else 
            $start + $perpage
    
    let $results := display:recordBasic(subsequence($hits,$start, $end)),
        $timeend := seconds-from-duration(util:system-time()-session:get-attribute("timestart")),
        $t := session:set-attribute("time", $timeend)
        
        
    let $number-of-pages := 
        xs:integer(ceiling($total-result-count div $perpage))

    let $how-many-on-this-page := 
        (: provides textual explanation about how many results are on this page, 
         : i.e. 'all n results', or '10 of n results' :)
        if ($total-result-count lt $perpage) then (
             <label><strong>{$total-result-count}</strong> registros encontrados en <b>{session:get-attribute("time")}</b> segundos </label>
        )else(
                <label><strong>{$start + 1}</strong> - <strong>{$end}</strong> de <strong>{$total-result-count}</strong> registros encontrados en <b>{session:get-attribute("time")}</b> segundos </label>
        )
     return
                <div>
                    <div id="data">
                     {
                        if (empty($hits)) then (
                            <div id="searchresults"><h3>No se encontraron resultados para esta consulta</h3></div>
                        )
                        else
                            (
                            <div>
                                <label id="totalEncontrados">{$total-result-count}</label>
                                <label id="resultadosPara">Resultados para &quot;{$q}&quot;. </label>
                                <label id="mostrandoTantos">Resultados, {$how-many-on-this-page}</label>
                                <label id="numberPages">{$number-of-pages}</label>
                                <div id="searchresults">{$results}</div>,
                                <div id="search-pagination">(:$pagination-links:)</div>
                                <div id="salidaFacetada">
                                </div>
                            </div>
                            )
                        }
                    </div>
                </div>
        
};