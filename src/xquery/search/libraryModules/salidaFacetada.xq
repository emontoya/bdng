module namespace salidaFacetada="http://exist-db.org/modules/salidaFacetada";

declare namespace oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/";
declare namespace xdb="http://exist-db.org/xquery/xmldb";
declare namespace request="http://exist-db.org/xquery/request";
declare namespace dc="http://purl.org/dc/elements/1.1/";
declare namespace oai_lom="http://ltsc.ieee.org/xsd/LOM";
declare namespace functx = "http://www.functx.com";
import module namespace t="http://exist-db.org/xquery/text";
import module namespace ft="http://exist-db.org/xquery/lucene";
import module namespace display="http://exist-db.org/modules/display"  at "display.xq";
import module namespace confVars="http://exist-db.org/modules/confVars"  at "configVariables.xq";


 (:  UTILITY FUNCTIONS   :)
declare function functx:capitalize-first ( $arg as xs:string? )  as xs:string? {
       
   concat(upper-case(substring($arg,1,1)),
             substring($arg,2))
 } ;

declare function salidaFacetada:term-callback($term as xs:string, $data as xs:int+) as element() {
  <term freq="{$data[1]}" docs="{$data[2]}" n="{$data[3]}">{$term}</term>
}; 
(:----------------------:)


declare function salidaFacetada:simpleFacet( $query as xs:string)as element()*{

    let $data-collection := '/db/bdcol'
    let $tempScope := concat('collection($data-collection)',$confVars:root)(: $confVars:root tiene el nombre del root. Ej. dc_lom,lom; dc :)
    let $scope := util:eval($tempScope)
    let $callback := util:function(xs:QName("salidaFacetada:term-callback"), 2)

    let $faceteing := concat('util:index-keys($scope//dc:',$query,",'', $callback, 10000)")

    let $result := <terms>{util:eval($faceteing)}</terms>
    let $show :=
                    <terms>
                       {
                            for $term in $result/term
                                where $term != ""
                                return
                                    $term 
                       }
                    </terms>

    return 
        $show
};

declare function salidaFacetada:facetada($lastSearch as element()*, $query as xs:string, $type as xs:string) as element()* {

    let $scope := 
        if(not($lastSearch))then(
                let $data-collection := '/db/bdcol'
                let $tempScope := concat('collection($data-collection)',$confVars:root)(: $confVars:root tiene el nombre del root. Ej. dc_lom,lom; dc :)
                return
                    util:eval($tempScope)
        )else if($type eq "navegacion")then( (: Esta facetada es para la navegación, para que facete basado en  la elección y no en el resultado como tal :)     
                session:get-attribute("scopeNavegacion") 
        )else(
            $lastSearch
        )
    (: Validación para que coincida con los nomrbes de variables de la vista :)
    let $query := 
        if($query eq "objecttype")then(
            "educational.learningResourceType"
        )else if($query eq "context")then(
            "educational.context"
        )else if($query eq "language")then(
            "general.language"
        )else(
            $query
        )
        
    let $callback := util:function(xs:QName("salidaFacetada:term-callback"), 2)
    let $faceteing := concat('util:index-keys($scope//dc:',$query,",'', $callback, 10000)")
    let $result := <terms>{util:eval($faceteing)}</terms>
    let $show :=
                    <terms>
                       {
                            for $term in $result/term
                                let $ord := xs:integer($term//@freq)
                                where $term != ""
                                order by $ord descending
                                return
                                    $term 
                       }
                    </terms>
    return $show
}; 

declare function salidaFacetada:refineBusquedaBetter($asRefineData as xs:string,$typeSearch as xs:string) as element()*{

    let $refine :=
    if(request:get-parameter("pagination","") eq "pagination")then(
        session:get-attribute("refineLast")
    )else(  
        let $lastSearchQuery :=
            if($typeSearch eq "basica") then(
                
                let $preBasic := session:get-attribute("basicSearchQuery")
                return
                    concat('$scope',"[ft:query(.,'",$preBasic,"')]")
            )else if($typeSearch eq "avanzada")then(
                let $preAdvance := session:get-attribute("advanceSearchQuery")
                return 
                    $preAdvance
            )else(
            
            )
        let $data-collection := 
            if($typeSearch eq "basica") then(
                    '/db/bdcol'
            )else(
                    session:get-attribute("data-collection")
            )
        let $tempScope := concat('collection($data-collection)',$confVars:root)(: $confVars:root tiene el nombre del root. Ej. dc_lom,lom; dc :)
        let $scope := util:eval($tempScope)  
        let $search-string := concat($lastSearchQuery,$asRefineData)
        let $refine := 
                    for $x in util:eval($search-string)
                       let $score := ft:score($x)
                       order by $score descending
                         return 
                          $x 
       return 
            $refine
    )
                      
   let $refineLast := session:set-attribute("refineLast",$refine)
   let $total-result-count := count($refine)
   let $perpage := xs:integer(request:get-parameter("perpage", "10"))
   let $number-of-pages := 
        xs:integer(ceiling($total-result-count div $perpage))
   let $start := xs:integer(request:get-parameter("start", "0"))
   let $end := 
    if ($total-result-count lt $perpage) then 
        $total-result-count
    else 
        $start + $perpage
      
      (: let $results := display:recordBasic($refine,$start,$end), :)
   let $results := display:recordBasic($refine[position() = ($start to $end)]),
       $timeend := seconds-from-duration(util:system-time()-session:get-attribute("timestart")),
       $t := session:set-attribute("time", $timeend)
   
     let $how-many-on-this-page := 
        (: provides textual explanation about how many results are on this page, 
         : i.e. 'all n results', or '10 of n results' :)
        if ($total-result-count lt $perpage) then (
             <label><strong>{$total-result-count}</strong> registros encontrados en <b>{session:get-attribute("time")}</b> segundos </label>
        )else(
                <label><strong>{$start + 1}</strong> - <strong>{$end}</strong> de <strong>{$total-result-count}</strong> registros encontrados en <b>{session:get-attribute("time")}</b> segundos </label>
        )
     return
                <div>
                    <div id="data">
                        <div>
                            <label id="totalEncontrados">{$total-result-count}</label>
                            <label id="mostrandoTantos">Resultados, {$how-many-on-this-page}</label>
                            <label id="numberPages">{$number-of-pages}</label>
                            <div id="searchresults">{$results}</div>,
                        </div>
                    </div>
                </div>
};

(: Vista de la navegación :)
declare function salidaFacetada:navegacion($item as xs:string,$institution as xs:string) as element()*{

    let $condition := 
            if($item eq "institution")then(
                "//dc:institution"
            )else if($item eq "objecttype") then(
                "//dc:educational.learningResourceType"
            )else if($item  eq "difficulty") then(
                "//dc:educational.difficulty"
            )else if($item  eq "context") then(    (: context :)
                "//dc:educational.context"
            )else if($item  eq "language") then(    (: context :)
                "//dc:general.language"
            )else if($item eq "year") then (    (: context :)
                "//dc:year"
            )else(
                "//basura"
            )
    
    
    let $data-collection := concat("/db/bdcol/",$institution)
    let $tempScope := concat('collection($data-collection)',$confVars:root)(: $confVars:root tiene el nombre del root. Ej. dc_lom,lom; dc :)
    let $scope := util:eval($tempScope)  
    let $callback := util:function(xs:QName("salidaFacetada:term-callback"), 2)    

    let $faceteing := concat('util:index-keys($scope',$condition,",'', $callback, 10000)")
    let $result := <terms>{util:eval($faceteing)}</terms>
    
    let $show :=
                        <div style="height:200px;" class="nano">
                            <div class="content">
                               {
                                    for $term in $result/term
                                        let $ord := xs:integer($term//@freq)
                                        where $term != ""
                                        order by $ord descending
                                        return
                                          <li><span>({$ord})</span><a data-value="{$term}">{functx:capitalize-first ($term)}</a></li>   
                               }
                           </div>
                        </div> 
    
    return
        $show

};




    
       

