module namespace display="http://exist-db.org/modules/display";


declare namespace xs="http://www.w3.org/2001/XMLSchema";
declare namespace dc="http://purl.org/dc/elements/1.1/";
declare namespace oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/";
declare namespace oai_lom="http://ltsc.ieee.org/xsd/LOM";
declare namespace xmldb="http://exist-db.org/xquery/xmldb";

import module namespace kwic="http://exist-db.org/xquery/kwic";

import module namespace repositorio="http://exist-db.org/modules/repositorio" at "repositorios.xq";

declare option exist:serialize "method=xhtml media-type=text/html";



declare function display:recordBasic($hits as element()*) as element()*{

    let $config := <config xmlns="" width="100"/>
    let $userId := xmldb:get-current-user()
    let $adminDiv :=
    if(xmldb:is-admin-user($userId))then (
            <div class="adminSetElementIcon"><a class="icon" original-title="Admin Record">Admin Record</a></div>
    )else("")
    for $hit in $hits
        return
        (

            <div class="resulSetElement">
                <div  class="infoData">
                    <div class="adminAdminElement">
                        <div class="adminAdminElementContent" data-id="{$hit//dc:linkidentifier//text()}" data-institution="{$hit//dc:institution//text()}" data-repname="{$hit//dc:repname//text()}">
                            <div class="adminActions mostRight overCertify" data-action="certify"><a class="icon certify" original-title="Admin certify">Admin Certify</a>Aprobar</div>
                            <div class="adminActions overDelete" data-action="delete"><a class="icon delete" original-title="Admin Delete">Admin Delete</a>Rechazar</div>
                        </div>
                    </div>   
                    <div class="clear"></div>                        
                    <table style="width:590px;">
                        <tr>
                            <td colspan="2">
                                <p style="text-align:right; color: #616161; font-size:13px;">{repositorio:ObtenerTooltip($hit/dc:institution/text(),"")}/{repositorio:ObtenerTooltip("",$hit/dc:repname/text())}</p>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <label style="font-size:14px; color: #1F4D64" > { $hit//dc:general.title//text()}</label>   
                            </td>
                        </tr>
                        
                        <tr>
                            <td style=" width:45px; vertical-align: top;">
                                <label style=" font-size:13px; color: #616161; font-weight: bold; " >Autor:</label>
                            </td>
                            <td>
                                <p style=" font-size:13px;">{$hit//dc:lifecycle.contribute.role.author//text()}</p>
                            </td>
                        </tr> 
                        <tr>
                            <td>
                                <label style=" font-size:13px;  color: #616161; font-weight: bold;" >Tipo:</label>
                            </td>
                            <td>
                                <p style=" font-size:13px;">{$hit//dc:educational.learningResourceType//text()}</p>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="padding-left:5px;">
                                <label class="bottomInfo">
                                    <a class="masDetalles">[Más detalles]</a>
                                    <strong>|</strong>
                                    {
                                        if(contains($hit//dc:technical.format//text(),"application/pdf")) 
                                        then (
                                            <a  href="{$hit//dc:linkidentifier//text()}" title="Click aquí para ir al recurso" ><img width="20px" height="20px" align="absmiddle" src="./resources/Images/layout/pdf_icon.png"/> PDF</a>
                                        )else(
                                            <a  href="{$hit//dc:linkidentifier//text()}" title="Click aquí para ir al recurso" ><img width="20px" height="20px" align="absmiddle" src="./resources/Images/layout/html_icon.gif"/> HTML</a>
                                        )
                                    }
                                    <strong>|</strong>
                                    <p style="display:inline;padding-left:2px;font-size:13px;">{$hit//dc:year//text()}</p>
                                </label>
                                {$adminDiv}                                
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="dataFull" class="infoData" style="display:none;">
                    {display:recordFull($hit)}
                </div>
            </div>

        )

};

declare function display:recordFull($hit as element()) as element(){
            
        <table style="width:560px;">
            <tr>
                <td colspan="2">
                    <p style="text-align:right; color: #616161; font-size:13px;">{repositorio:ObtenerTooltip($hit/dc:institution/text(),"")}/{repositorio:ObtenerTooltip("",$hit/dc:repname/text())}</p>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <label style="font-size:14px; color: #1F4D64" > { $hit//dc:general.title//text()}</label>   
                </td>
            </tr>
            <tr>
                <td style=" width:45px; vertical-align: top;">
                    <label style=" font-size:13px; color: #616161; font-weight: bold; " >Autor:</label>
                </td>
                <td>
                    <p style=" font-size:13px;">{$hit//dc:lifecycle.contribute.role.author//text()}</p>
                </td>
            </tr>          
            <tr>
                <td style=" width:45px; vertical-align: top;">
                    <label style=" font-size:13px; color: #616161; font-weight: bold; " >Descripción:</label>
                </td>
                <td>
                    <p style=" font-size:13px;">{$hit//dc:general.description//text()}</p>
                </td>
            </tr>
            <tr>
                <td style=" width:45px; vertical-align: top;">
                    <label style=" font-size:13px; color: #616161; font-weight: bold; " >Tema:</label>
                </td>
                <td>
                    <p style=" font-size:13px;">{$hit//dc:general.keyword//text()}</p>
                </td>
            </tr>
            <tr>
                <td style=" width:45px;">
                    <label style=" font-size:13px; color: #616161; font-weight: bold; " >Colección:</label>
                </td>
                <td>
                    <p style=" font-size:13px;">{$hit//dc:collection//text()}</p>
                </td>
            </tr>
            <tr>
                <td>
                    <label style=" font-size:13px;  color: #616161; font-weight: bold;" >Tipo:</label>
                </td>
                <td>
                    <p style=" font-size:13px;">{$hit//dc:educational.learningResourceType//text()}</p>
                </td>
            </tr>
            <tr>
                <td style=" width:45px;">
                    <label style=" font-size:13px; color: #616161; font-weight: bold; " >Contexto:</label>
                </td>
                <td>
                    <p style=" font-size:13px;">{$hit//dc:educational.context//text()}</p>
                </td>
            </tr>
            {
                if($hit//dc:educational.difficulty//text() ne "") 
                then (
                    <tr>
                        <td style=" width:45px;">
                            <label style=" font-size:13px; color: #616161; font-weight: bold; " >Dificultad:</label>
                        </td>
                        <td>
                            <p style=" font-size:13px;">{$hit//dc:educational.difficulty//text()}</p>
                        </td>
                    </tr>
                )else(
                )
            }
            {
                if($hit//oai_lom:educational//oai_lom:typicalAgeRange//oai_lom:string[1]//text() ne "") 
                then (
                    <tr>
                        <td style=" width:45px;">
                            <label style=" font-size:13px; color: #616161; font-weight: bold; " >Rango Edad:</label>
                        </td>
                        <td>
                            <p style=" font-size:13px;">{$hit//oai_lom:educational//oai_lom:typicalAgeRange//oai_lom:string[1] //text()}</p>
                        </td>
                    </tr>
                )else(
                )
            }
            <tr>
                <td colspan="2" style="padding-left:5px;">
                    <label class="bottomInfo">
                        <a class="masDetalles">[Resumen]</a>
                        <strong>|</strong>
                        {
                            if(contains($hit//dc:technical.format//text(),"application/pdf")) 
                            then (
                                <a  href="{$hit//dc:linkidentifier//text()}" title="Click aquí para ir al recurso" ><img width="20px" height="20px" align="absmiddle" src="./resources/Images/layout/pdf_icon.png"/> PDF</a>
                            )else(
                                <a  href="{$hit//dc:linkidentifier//text()}" title="Click aquí para ir al recurso" ><img width="20px" height="20px" align="absmiddle" src="./resources/Images/layout/html_icon.gif"/> HTML</a>
                            )
                        }
                        <strong>|</strong>
                        <p style="display:inline;padding-left:2px;font-size:13px;">{$hit//dc:year//text()}</p>
                    </label>
                </td>
            </tr>
        </table>

};

declare function display:recordBasicTest($hits as element()*) as element()*{

    let $config := <config xmlns="" width="100"/>

    for $hit in $hits
        return
        (
            <div class="resulSetElement">
                <div  class="infoData">  
                    <div class="clear"></div>                        
                    <table style="width:590px;">
                        <tr>
                            <td colspan="2">
                                <p style="text-align:right; color: #616161; font-size:13px;">{repositorio:ObtenerTooltip($hit/dc:institution/text(),"")}/{repositorio:ObtenerTooltip("",$hit/dc:repname/text())}</p>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <label style="font-size:14px; color: #1F4D64" > { $hit//dc:general.title//text()}</label>   
                            </td>
                        </tr>
                        
                        <tr>
                            <td style=" width:45px; vertical-align: top;">
                                <label style=" font-size:13px; color: #616161; font-weight: bold; " >Autor:</label>
                            </td>
                            <td>
                                <p style=" font-size:13px;">{$hit//dc:lifecycle.contribute.role.author//text()}</p>
                            </td>
                        </tr> 
                        <tr>
                            <td>
                                <label style=" font-size:13px;  color: #616161; font-weight: bold;" >Tipo:</label>
                            </td>
                            <td>
                                <p style=" font-size:13px;">{$hit//dc:educational.learningResourceType//text()}</p>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="padding-left:5px;">
                                <label class="bottomInfo">
                                    <a class="masDetalles">[Más detalles]</a>
                                    <strong>|</strong>
                                    {
                                        if(contains($hit//dc:technical.format//text(),"application/pdf")) 
                                        then (
                                            <a  href="{$hit//dc:linkidentifier//text()}" title="Click aquí para ir al recurso" ><img width="20px" height="20px" align="absmiddle" src="./resources/Images/layout/pdf_icon.png"/> PDF</a>
                                        )else(
                                            <a  href="{$hit//dc:linkidentifier//text()}" title="Click aquí para ir al recurso" ><img width="20px" height="20px" align="absmiddle" src="./resources/Images/layout/html_icon.gif"/> HTML</a>
                                        )
                                    }
                                    <strong>|</strong>
                                    <p style="display:inline;padding-left:2px;font-size:13px;">{$hit//dc:year//text()}</p>
                                </label>
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="dataFull" class="infoData" style="display:none;">
                    {display:recordFull($hit)}
                </div>
            </div>
        )

};

(:
declare function display:recordBasic($hits as element()*, $start as xs:integer, $end as xs:integer) as element()*{

    let $config := <config xmlns="" width="100"/>

    for $hit in $hits[position() = ($start to $end)]
        return
        (
            <div class="resulSetElement">
                <div  class="infoData">
                    <div class="adminAdminElement">
                        <div class="adminAdminElementContent" data-id="{$hit//dc:linkidentifier//text()}">
                            <div class="adminActions mostRight overCertify" data-action="certify"><a class="icon certify" original-title="Admin certify">Admin Certify</a>Certificar</div>
                            <div class="adminActions overDelete" data-action="delete"><a class="icon delete" original-title="Admin Delete">Admin Delete</a>Eliminar</div>
                        </div>
                    </div>   
                    <div class="clear"></div>                        
                    <table style="width:590px;">
                        <tr>
                            <td colspan="2">
                                <p style="text-align:right; color: #616161; font-size:13px;">{repositorio:ObtenerTooltip($hit/dc:institution/text(),"")}/{repositorio:ObtenerTooltip("",$hit/dc:repname/text())}</p>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <label style="font-size:14px; color: #1F4D64" > { $hit//dc:general.title//text()}</label>   
                            </td>
                        </tr>
                        
                        <tr>
                            <td style=" width:45px; vertical-align: top;">
                                <label style=" font-size:13px; color: #616161; font-weight: bold; " >Autor:</label>
                            </td>
                            <td>
                                <p style=" font-size:13px;">{$hit//dc:lifecycle.contribute.role.author//text()}</p>
                            </td>
                        </tr> 
                        <tr>
                            <td>
                                <label style=" font-size:13px;  color: #616161; font-weight: bold;" >Tipo:</label>
                            </td>
                            <td>
                                <p style=" font-size:13px;">{$hit//dc:educational.learningResourceType//text()}</p>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="padding-left:5px;">
                                <label class="bottomInfo">
                                    <a class="masDetalles">[Más detalles]</a>
                                    <strong>|</strong>
                                    {
                                        if(contains($hit//dc:technical.format//text(),"application/pdf")) 
                                        then (
                                            <a  href="{$hit//dc:linkidentifier//text()}" title="Click aquí para ir al recurso" ><img width="20px" height="20px" align="absmiddle" src="./resources/Images/layout/pdf_icon.png"/> PDF</a>
                                        )else(
                                            <a  href="{$hit//dc:linkidentifier//text()}" title="Click aquí para ir al recurso" ><img width="20px" height="20px" align="absmiddle" src="./resources/Images/layout/html_icon.gif"/> HTML</a>
                                        )
                                    }
                                    <strong>|</strong>
                                    <p style="display:inline;padding-left:2px;font-size:13px;">{$hit//dc:year//text()}</p>
                                </label>
                                <div class="adminSetElementIcon"><a class="icon" original-title="Admin Record">Admin Record</a></div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="dataFull" class="infoData" style="display:none;">
                    {display:recordFull($hit)}
                </div>
            </div>
        )

};
:)

(: OAI_DC :)
(: 

declare function display:recordBasic($hits as element()*, $start as xs:integer, $end as xs:integer) as element()*{

    let $config := <config xmlns="" width="100"/>

    for $hit in $hits[position() = ($start to $end)]
        return
        (
            <div class="resulSetElement">
                <div  class="infoData">
                    <table style="width:590px;">
                        <tr>
                            <td colspan="2">
                                <p style="text-align:right; color: #616161; font-size:13px;">{repositorio:ObtenerTooltip($hit/dc:institution/text(),"")}/{repositorio:ObtenerTooltip("",$hit/dc:repname/text())}</p>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <label style="font-size:14px; color: #1F4D64" > { $hit/dc:title/text()}</label>   
                            </td>
                        </tr>
                        <tr>
                            <td style=" width:45px; vertical-align: top;">
                                <label style=" font-size:13px; color: #616161; font-weight: bold; " >Autor:</label>
                            </td>
                            <td>
                                <p style=" font-size:13px;">{$hit/dc:creator/text()}</p>
                            </td>
                        </tr>
                        <!--<tr>
                            <td>
                                <label style=" font-size:13px;  color: #616161; font-weight: bold;" >Editor:</label>
                            </td>
                            <td>
                                <p style=" font-size:13px;">{$hit/dc:publisher/text()}</p>
                            </td>
                        </tr>-->
                         <tr>
                            <td colspan="2">
                                <p style="padding-left:5px; font-size:13px;">{$hit/dc:year/text()}</p>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="padding-left:5px;">
                                <label class="bottomInfo">
                                    <a class="masDetalles">[Más detalles]</a>
                                    <strong>|</strong>
                                    {
                                        if(contains($hit/dc:format/text(),"application/pdf")) 
                                        then (
                                            <a  href="{$hit/dc:identifier/text()}" TARGET="_blank"><img width="20px" height="20px" align="absmiddle" src="./resources/Images/layout/pdf_icon.png"/> PDF</a>
                                        )else(
                                            <a  href="{$hit/dc:identifier/text()}" TARGET="_blank"><img width="20px" height="20px" align="absmiddle" src="./resources/Images/layout/html_icon.gif"/> HTML</a>
                                        )
                                    }
                                    <strong>|</strong>
                                </label>
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="dataFull" class="infoData" style="display:none;">
                    {display:recordFull($hit)}
                </div>
            </div>
        )

};


declare function display:recordFull($hit as element()) as element(){
            
        <table style="width:560px;">
            <tr>
                <td colspan="2">
                    <p style="text-align:right; color: #616161; font-size:13px;">{$hit/dc:publisher/text()}/{$hit/dc:repname/text()}</p>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <label style="font-size:14px; color: #1F4D64" > { $hit/dc:title/text()}</label>   
                </td>
            </tr>
            <tr>
                <td style=" width:45px; vertical-align: top;">
                    <label style=" font-size:13px; color: #616161; font-weight: bold; " >Autor:</label>
                </td>
                <td>
                    <p style=" font-size:13px;">{$hit/dc:creator/text()}</p>
                </td>
            </tr>
            <tr>
                <td style=" width:45px; vertical-align: top;">
                    <label style=" font-size:13px; color: #616161; font-weight: bold; " >Descripción:</label>
                </td>
                <td>
                    <p style=" font-size:13px;">{$hit/dc:description/text()}</p>
                </td>
            </tr>
            <tr>
                <td style=" width:45px; vertical-align: top;">
                    <label style=" font-size:13px; color: #616161; font-weight: bold; " >Tema:</label>
                </td>
                <td>
                    <p style=" font-size:13px;">{$hit/dc:subject/text()}</p>
                </td>
            </tr>
             <tr>
                <td style=" width:45px;">
                    <label style=" font-size:13px; color: #616161; font-weight: bold; " >Colección:</label>
                </td>
                <td>
                    <p style=" font-size:13px;">{$hit/dc:collection/text()}</p>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="padding-left:5px;">
                    <label class="bottomInfo">
                        <a class="masDetalles">[Resumen]</a>
                        <strong>|</strong>
                        {
                            if(contains($hit/dc:format/text(),"application/pdf")) 
                            then (
                                <a  href="{$hit/dc:identifier/text()}" TARGET="_blank"><img width="20px" height="20px" align="absmiddle" src="./resources/Images/layout/pdf_icon.png"/> PDF</a>
                            )else(
                                <a  href="{$hit/dc:identifier/text()}" TARGET="_blank"><img width="20px" height="20px" align="absmiddle" src="./resources/Images/layout/html_icon.gif"/> HTML</a>
                            )
                        }
                        <strong>|</strong>
                    </label>
                </td>
            </tr>
        </table>

};

 :)