xquery version "1.0";

module namespace feed="http://exist-db.org/modules/feed";
declare namespace oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/";
declare namespace dc="http://purl.org/dc/elements/1.1/";
declare namespace util="http://exist-db.org/xquery/util";
declare namespace session="http://exist-db.org/xquery/session";
import module namespace configweb="http://exist-db.org/modules/configweb" at "configWeb.xq";
declare namespace xdb="http://exist-db.org/xquery/xmldb";


declare function feed:ObtenerDocumentoPorIdentificador($identificador as xs:string) as element()*

{
   let $filtro:= concat("[contains(. , '",$identificador,"')]")   
    let $query:=  concat(configweb:get-namespace-decls(), 
            "for $m in  collection('/db/bdcol')//oai_dc:dc",$filtro," return $m") 
     return
        let $registros:=util:eval( $query)
        return 
        $registros[1]
};


declare function feed:ObtenerIdentificadorUrl($datos as element()*) as xs:string
{
	let $conteo:= count($datos)
		  return
			  if($conteo >1) then
			  (
				  xs:string($datos[1])
			  )
			  else
			  (
				  if($conteo eq 0) then
				  (
					 ""
				  )
				  else
				  (
					  xs:string($datos)
				   )
			  )
          
};


declare function feed:Identify($identify as element()*) as element()* {
    if (exists($identify)) then
       (for $pub in $identify
                return
                   if (contains($pub, "http://")) then ($pub) else ()
        )                       
     else ()
};



declare function feed:ObtenerDatosColeccion($coleccion as xs:string) as element()* {
    let $datos:= for $reg in doc("/db/stats/consolidado_Datos.xml")//collection
				 where $reg//name//text() eq $coleccion
				 return $reg
	return $datos
};


declare function feed:ObtenerDatosIdentificadores($datos as element()*) as element()* {
    let $registros := $datos//documentos
	for $documento in $registros//documento
	return
		$documento
			
};


declare function feed:ObtenerListadoColecciones() as element()* {
    for $coleccion in doc("//db/stats/counter_cols.xml")//collection  
    return 
    (
		  <collection>
            {$coleccion//name/text()}
          </collection>    
    )
};



(: Lista los feeds de BDCOL :)
declare function feed:ListarMisFeeds() as element()*
{
        let $colecciones:= feed:ObtenerListadoColecciones()
           for $coleccion in $colecciones
               return
                   let $titulo :=$coleccion
                   let $tituloCorto:=substring($titulo, 0, 20)
                   return
                    <li class=" ">
                       <a href="../atom/documentos?coleccion={$coleccion}" class="" title="{$coleccion}" id="lnkMiDocumento" >
                            <span class=""><p></p><img src="images/fed_rss.gif" border="0" cellpading="5" cellspacing="5" />&#160;&#160;{$tituloCorto}</span>
                       </a>
                    </li>
           
};

(: Genera el menu Mis feeds :)

declare function feed:VistaMenuMisFeeds() as element()*
{
    let $usuario:=session:get-attribute("user")
    return
        if($usuario and $usuario ne "guest")then
        (
		<div id="misfeeds" class="sidebar" >
			<div id="accordionFeed" class="sidebar-list" >
				<h3 class="expand ui-accordion-header ui-helper-reset ui-state-active ui-corner-top">
					Mis Feeds
				</h3>
				<div  class="collapse list ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom ui-accordion-content-active">
					<ul style="width:500px;height:100% ; " class="list">
						{feed:ListarMisFeeds()}
					</ul>
				</div>
			</div>
		</div>
	)
        else
        (
            ()
        )
};
