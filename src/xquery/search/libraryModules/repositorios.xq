xquery version "1.0";
module namespace repositorio="http://exist-db.org/modules/repositorio";

declare namespace dc="http://purl.org/dc/elements/1.1/";
declare namespace util="http://exist-db.org/xquery/util";
declare namespace xdb="http://exist-db.org/xquery/xmldb";
declare namespace request="http://exist-db.org/xquery/request";
declare namespace session="http://exist-db.org/xquery/session";
import module namespace confVars="http://exist-db.org/modules/confVars"  at "configVariables.xq";


(:  IMPORTANT FUNTIONS TO JUST FACET BY ONE QUERY :)

declare function repositorio:term-callback($term as xs:string, $data as xs:int+) as element() {
  <term freq="{$data[1]}" docs="{$data[2]}" n="{$data[3]}">{$term}</term>
}; 
(:----------------------:)


declare function repositorio:simpleFacet( $query as xs:string)as element()*{

    let $data-collection := '/db/bdcol'
    let $tempScope := concat('collection($data-collection)',$confVars:root)(: $confVars:root tiene el nombre del root. Ej. dc_lom,lom; dc :)
    let $scope := util:eval($tempScope)
    let $callback := util:function(xs:QName("repositorio:term-callback"), 2)

    let $faceteing := concat('util:index-keys($scope//dc:',$query,",'', $callback, 10000)")

    let $result := <terms>{util:eval($faceteing)}</terms>
    let $show :=
                    <terms>
                       {
                            for $term in $result/term
                                where $term != ""
                                return
                                    $term 
                       }
                    </terms>

    return 
        $show
};

declare function repositorio:ObtenerTooltip($inst as xs:string,$rep as xs:string) as element()*
{
    let $resp := 
        if($rep) then(
        	(for $ttp in doc("/db/conf/oai_providers.xml")//oai_provider[contains(.//shortName,$rep)]
        	return
        	    $ttp//repositoryName)[1]
        )else(
            (for $ttp in doc("/db/conf/oai_providers.xml")//oai_provider[contains(.//shortInst,$inst)]
            return
                $ttp//institution)[1]
        )
    
    return
        $resp
};

declare function repositorio:minMaxYear()as element()*
{
    let $collection:= "/db/bdcol"
    return
        <div id="maxAndMinYears" style="display:none;">
            <input type="hidden" id="minRangeYear" value="{min(repositorio:simpleFacet("year")//terms/term)}"></input>
            <input type="hidden" id="maxRangeYear" value="{year-from-date(current-date())}"></input>
        </div>
    
};

(: <input type="hidden" id="maxRangeYear" value="{max(repositorio:simpleFacet("year")//terms/term)}"></input> :)

declare function repositorio:listadoInstituciones() as element()*
{
	let $collection:= "/db/bdcol"
	return
		<div id="ddlInstitucionContainer" style="color: #666666; font-size: 14px;" >
		    
							{
								for $child in doc("//db/stats/counter_cols.xml")//institution 
								order by number($child//size/text()) descending
								return
									let $institucion := $child//name/text()
									let $conteo :=  $child//size/text()
									return
									(
										<div id="{$institucion}" title="{$institucion}" defaultSelection="false" state="0" data="{concat($institucion,"#")}">
											{$institucion}  ({$conteo})
										</div>
										
									)
							}
		</div>
};

(: Me devuelve un Select completo :)
declare function repositorio:listadoInstitucionesList() as element()*
{
    let $collection:= "/db/bdcol"
    return
        <div style="padding-right:7px; padding-bottom: 6px;">
            <select id="institution" class="putChosen" style="width:150px;" tabindex="1" data-placeholder="Todos">
                                <option value="" selected="selected"></option>
                                {
                                    for $child in doc("//db/stats/counter_cols.xml")//institution 
                                    order by number($child//size/text()) descending
                                    return
                                        let $institucion := $child//name/text()
                                        let $conteo :=  $child//size/text()
                                        let $longname := repositorio:ObtenerTooltip($institucion,"")
                                        return
                                        (
                                            <option data-long="{$longname}" value="{$institucion}" title="{$institucion}">
                                                {$institucion}  ({$conteo})
                                            </option>
                                            
                                        )
                                }
                        
            </select>
        </div>
};
(: Me devuelve un Select completo con nombre bien formado :)
declare function repositorio:listadoInstitucionesListParaNavegacion() as element()*
{
    let $collection:= "/db/bdcol"
    return
        <div style="padding-right:7px; padding-bottom: 16px;">
            <select id="institution" class="putChosen" style="width:auto;" tabindex="1" data-placeholder="Todos">
                                <option value="" selected="selected"></option>
                                {
                                    for $child in doc("//db/stats/counter_cols.xml")//institution 
                                    order by number($child//size/text()) descending
                                    return
                                        let $institucion := $child//name/text()
                                        let $conteo :=  $child//size/text()
                                        let $longname := repositorio:ObtenerTooltip($institucion,"")
                                        return
                                        (
                                            <option data-long="{$longname}" value="{$institucion}" title="{$institucion}">
                                                {$longname}
                                            </option>
                                            
                                        )
                                }
                        
            </select>
        </div>
};


declare function repositorio:listadoColecciones() as element()*
{
	let $collection := "/db/bdcol"
	return
	  	<div id="ddlColeccionContainer" style="color: #666666; font-size: 14px;">
				{
					for $child in doc("//db/stats/counter_cols.xml")//collection order by number($child//size/text()) descending
					return
						let $coleccion := $child//name/text()
						let $conteo := $child//size/text()
						return
							<div id="{replace($coleccion," ","_")}" title="{$coleccion}" defaultSelection="false" state="0" data="{concat($coleccion,"#")}">
								{$coleccion}  ({$conteo})
							</div>
					
				}
				
			</div>
		
	
};

(: --------- REPORSITORIOS --------- :) 

(: Listo todos los repositorios :)
declare function repositorio:listadoRepositoriosList() as element()*
{
    let $collection:= "/db/bdcol"
    return
        <div style="padding-right:7px;">
            <select id="repositorio" class="putChosen" style="width:150px;" tabindex="1" data-placeholder="Todos">
                                <option value="" selected="selected"></option>
                                {
                                
                                  for $child in doc("//db/stats/counter_cols.xml")//repname order by number($child//size/text()) descending
                                   return
                                        let $repositorio := $child//name/text()
                                        let $conteo := $child//size/text()
                                        let $longname := repositorio:ObtenerTooltip("",$repositorio)
                                        return
                                            <option data-long="{$longname}" value="{$repositorio}" title="{$repositorio}">
                                                {$repositorio} ({$conteo})
                                            </option>
                                }
                        
            </select>
            
        </div>
};

(: Listo todos los repositorios :)
declare function repositorio:listadoRepositoriosListAll() as element()*
{
    let $collection:= "/db/bdcol"
    return
          
          for $child in doc("//db/stats/counter_cols.xml")//repname order by number($child//size/text()) descending
           return
                let $repositorio := $child//name/text()
                let $conteo := $child//size/text()
                let $longname := repositorio:ObtenerTooltip("",$repositorio)
                return
                    <option data-long="{$longname}" value="{$repositorio}" title="{$repositorio}">
                        {$repositorio} ({$conteo})
                    </option>

};

(: Listo los repositorios de una institucion :)
declare function repositorio:listadoRepositoriosList($institucion as xs:string) as element()*
{
    let $collection:= "/db/bdcol"
    return

                                  for $child in doc("//db/stats/counter_cols.xml")//repname 
                                  where $child//inst/text() eq $institucion
                                  order by number($child//size/text()) descending
                                   return
                                        let $repositorio := $child//name/text()
                                        let $conteo := $child//size/text()
                                        let $longname := repositorio:ObtenerTooltip("",$repositorio)
                                        return
                                            <option data-long="{$longname}" value="{$repositorio}" title="{$repositorio}">
                                                {$repositorio} ({$conteo})
                                            </option>


};



declare function repositorio:listadoRepositorios() as element()*
{
	let $collection := "/db/bdcol"
	let $coleccion:= request:get-parameter("collection","")
	let $institucion:= request:get-parameter("institution","")
	let $repositorio:= request:get-parameter("repname","")
	let $tipo:=
		if($coleccion) then
		(
			"coleccion"
		)
		else if($institucion) then
		(
			"institucion"
		)else if($repositorio) then		
		(
			"repositorio"
		)else
		(
			"coleccion"
		)
	let $valorRepositorio:=
		if($coleccion ne "") then
		(
			$coleccion
		)
		else if($institucion ne "") then
		(
			$institucion
		)else if($repositorio ne "") then		
		(
			$repositorio
		)else
		(
			""
		)
	return
	(
		<div id="ddlRepositorioContainer" style="color: #666666; font-size: 14px;">
		
						{
								for $child in doc("//db/stats/counter_cols.xml")//repname order by number($child//size/text()) descending
								return
									let $repositorio := $child//name/text()
									let $conteo := $child//size/text()
								return
									<div id="{$repositorio}" title="{$repositorio}" defaultSelection="false" state="0" data="{concat($repositorio,"#")}">
										{$repositorio} ({$conteo})
									</div>
															
							}
							
						
		</div>
		,
		<input type="hidden" id="hdnColeccion" name="hdnColeccion" value="{$valorRepositorio}"  />
		,
		<input type="hidden" id="hdnTipo" name="hdnTipo" value="{$tipo}"  />
	)
	
};

declare function repositorio:buscarInstitucionesPorColeccion($coleccion as xs:string) as element()*
{
	if($coleccion eq "Todos") then
	(
		repositorio:listadoInstituciones2()
	)
	else
	(
		let $hijo:= concat("/db/bdcol","/",$coleccion)
		let $contador := 0
		return
		  for $child2 in xdb:get-child-collections($hijo)
			return
				let $contador := $contador + 1
				let $tooltip:= doc("/db/stats/oai_providers.xml")//oai_provider[contains(.//shortName,$child2)]//repositoryName/text()
				let $colection := concat($hijo,"/",$child2)
				return
					<div id="{$child2}" title="{$child2}" defaultSelection="false" state="0" data="{concat($colection,"#")}">
						{$child2}  ({collection("/db/stats")//collection[name = $colection]/size/text()})
					</div>
	)				
					
					

				
};

declare function repositorio:listadoInstituciones2() as element()*
{
	let $collection:= "/db/bdcol"
	return
	for $child in xdb:get-child-collections("/db/bdcol")
		return
			let $hijo:= concat("/db/bdcol","/",$child)
			return
				for $child2 in xdb:get-child-collections($hijo)
				return
					let $tooltip:= doc("/db/stats/oai_providers.xml")//oai_provider[contains(.//shortName,$child2)]//repositoryName/text()
					let $colection := concat($hijo,"/",$child2)
					return
						<div id="{$child2}" title="{$child2}" defaultSelection="false" state="0" data="{concat($colection,"#")}">
							{$child2}  ({collection("/db/stats")//collection[name = $colection]/size/text()})
						</div>
};



declare function repositorio:buscarRepositoriosPorColeccion($coleccion as xs:string) as element()*
{
	if($coleccion eq "Todos") then
	(
		repositorio:listadoRepositorios2()
	)
	else
	(
		let $hijo:= concat("/db/bdcol","/",$coleccion)
		return
			for $child2 in xdb:get-child-collections($hijo)
			return
				let $hijo2:= concat( $hijo,"/",$child2)
				return
					for $child3 in xdb:get-child-collections($hijo2)
					return
						let $collection := concat($hijo2,"/",$child3)
						return					
							<div id="{$child3}" title="{$child3}" defaultSelection="false" state="0" data="{concat($collection,"#")}">
								{$child3} ({collection("/db/stats")//collection[name = $collection]/size/text()})
							</div>
	)
					
};
declare function repositorio:buscarRepositoriosPorInstitucion($institucion as xs:string) as element()*
{
		let $raiz:=  "/db/bdcol"
		  return
			for $child in xdb:get-child-collections($raiz)
			  return
				let $hijo:= concat( $raiz,"/",$child)
				return
					for $child2 in xdb:get-child-collections($hijo)
						return
						if ($child2 eq $institucion) then
						(
							 let $hijo2:= concat( $hijo,"/",$child2)
								return
									for $child3 in xdb:get-child-collections($hijo2)
									return										
											let $collection := concat($hijo2,"/",$child3)
											return
											<div id="{$child3}" title="{$child3}" defaultSelection="false" state="0" data="{concat($collection,"#")}">
													{$child3} ({collection("/db/stats")//collection[name = $collection]/size/text()})
											</div>
						)
						else
						(
						)
					
					
};

declare function repositorio:listadoRepositorios2() as element()*
{
	let $collection := "/db/bdcol"
	return
	for $child in xdb:get-child-collections("/db/bdcol")
		return
			let $hijo:= concat("/db/bdcol","/",$child)
			return
				for $child2 in xdb:get-child-collections($hijo)
				return
					let $hijo2:= concat( $hijo,"/",$child2)
					return
						for $child3 in xdb:get-child-collections($hijo2)
						return
							let $collection := concat($hijo2,"/",$child3)
							return
							<div id="{$child3}" title="{$child3}" defaultSelection="false" state="0" data="{concat($collection,"#")}">
								{$child3} ({collection("/db/stats")//collection[name = $collection]/size/text()})
							</div>
			
};


declare function repositorio:AreaSeleccionListBox($tipo as xs:string) as element()*
{
	<ul style="overflow: visible; " id="{concat("sm",$tipo)}" class="selectlist">
    </ul>   

};

declare function repositorio:cargarSesionDatosColecciones($coleccion as xs:string,$colection as xs:string,$institucion as xs:string,$repositorio as xs:string) as element()*
{
	let $col:= session:set-attribute("coleccion", $coleccion)
	let $inst:= session:set-attribute("institucion", $institucion)
	let $rep:= session:set-attribute("repositorio", $repositorio)
	return
		 session:set-attribute("collection", $coleccion)
		
};

declare function repositorio:cargarTabsRepositorios()
{
        <div style="margin-top:10px; margin-bottom:10px;">
            <div class="dotted">
                <a data-id="coleccionButton" class="expand-button collapsed" ></a>
                <label class="tituloIzq">
                    Colección <label id="lblConteoColecciones" name="lblConteoColecciones" ></label>
                </label>  
                <div class="clear"></div>  
    			<div id="coleccionButton" class="tabs">
    				<ul  class="list">
    					{repositorio:listadoColecciones()}
    				 </ul>
    			</div>
			</div>
			<div class="dotted">
			    <a data-id="institucionButton" class="expand-button collapsed" ></a>
			    <label  class="tituloIzq">
                    Institución <label id="lblConteoInstituciones" name="lblConteoInstituciones" ></label>
                </label>
                <div class="clear"></div>  
    			<div id="institucionButton" class="tabs" >
    				<ul class="list">
    					{repositorio:listadoInstituciones()}
    				 </ul>
    			</div>
			</div>
			<div class="dotted">
			    <a data-id="repositorioButton" class="expand-button collapsed" ></a>
    			<label class="tituloIzq">
                    Repositorio <label id="lblConteoRepositorios" name="lblConteoRepositorios" ></label>
                </label>
                <div class="clear"></div>  
    			<div id="repositorioButton" class="tabs">
    				<ul  class="list">
    					{repositorio:listadoRepositorios()}
    				 </ul>
    			</div>
			</div>
		</div>

};
