declare namespace oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/";
declare namespace dc="http://purl.org/dc/elements/1.1/";
import module namespace ft="http://exist-db.org/xquery/lucene";


declare function local:term-callback($term as xs:string, $data as xs:int+) as element() {
  <term freq="{$data[1]}" docs="{$data[2]}" n="{$data[3]}">{$term}</term>
}; 

let $callback := util:function(xs:QName("local:term-callback"), 2)

let $coll := collection('/db/bdcol'),
    $t := request:get-parameter("query", ())
    
let $eval-string := concat
   ("util:index-keys($coll//oai_dc:dc,'",$t,"', $callback, 10, 'lucene-index')")

let $result := util:eval($eval-string) 

return
<terms>
 {$result}
</terms>