module namespace busquedaAvanzada="http://exist-db.org/modules/busquedaAvanzada";

declare namespace request="http://exist-db.org/xquery/request";
declare namespace xs="http://www.w3.org/2001/XMLSchema";
declare namespace dc="http://purl.org/dc/elements/1.1/"; (: 43152 madugirlsfirend :)
declare namespace oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/";
declare namespace oai_lom="http://ltsc.ieee.org/xsd/LOM";
declare namespace fn="http://www.w3.org/2005/xpath-functions";

import module namespace kwic="http://exist-db.org/xquery/kwic";
import module namespace display="http://exist-db.org/modules/display"  at "display.xq";
import module namespace confVars="http://exist-db.org/modules/confVars"  at "configVariables.xq"; (:Variables de configuración :)

declare option exist:serialize "method=xhtml media-type=text/html";

declare function busquedaAvanzada:avanzada() as element()*{
    let $pagination := request:get-parameter('pagination', '')
    
    let $hits := 
        if($pagination eq "pagination")then(
             session:get-attribute("lastSearch")
        )else(
            let $title := replace(xs:string(request:get-parameter('title', '')), "[^0-9a-zA-ZäöüßÄÖÜáéíóúñÑ?\-,. ]", "")
            let $author := replace(xs:string(request:get-parameter('author', '')), "[^0-9a-zA-ZäöüßÄÖÜáéíóúñÑ?\-,. ]", "")
            let $subject := replace(xs:string(request:get-parameter('subject', '')), "[^0-9a-zA-ZäöüßÄÖÜáéíóúñÑ?\-,. ]", "")
            let $institution := replace(xs:string(request:get-parameter('institution', '')), "[^0-9a-zA-ZäöüßÄÖÜáéíóúñÑ?\-,. ]", "")
            let $repositorio := replace(xs:string(request:get-parameter('repositorio', '')), "[^0-9a-zA-ZäöüßÄÖÜáéíóúñÑ?\-,. ]", "")
            let $yearRange := xs:string(request:get-parameter('yearRange', ''))
            (: Datos proveninetes de la vista de navegación :)
            let $navegacion := (xs:string(request:get-parameter('navegacion', '')))
            
            let $title-predicate := if ($title) then concat("[ft:query(dc:general.title,'", $title, "')]") else ()
            let $author-predicate := if ($author) then concat("[ft:query(dc:lifecycle.contribute.role.author,'", $author, "')]") else ()
            let $subject-predicate := if ($subject) then concat("[ft:query(dc:general.keyword,'", $subject, "')]") else ()
            let $yearRange-predicate := if ($yearRange) then concat("[ft:query(dc:year,<query><bool><regex occur='must'>",$yearRange,"</regex></bool></query>)]") else ()
        
            (: Validar que el registro no esté 'rechazado' :)
            (: 0 => Aprobado :)
            (: 1 => Pendiente por verificar :)
            (: 2 => Rechazado :)
            (:Atención, esta funcionalidad debe ser revisada, ya qeu en al búsqueda avanzada presentó problemas:)
            (:let $notRejected := "[ft:query(dc:certified,<query><bool><term occur='must'>0</term><term occur='should'>1</term></bool></query>)]":)
        
            
        
        
            let $data-collection := 
                if ($repositorio and $institution) then
                 (
                    concat("/db/bdcol/",$institution,"/",$repositorio)   
                 )else if($repositorio)then(
                    let $instReep := 
                    for $i in doc("//db/stats/counter_cols.xml")//repname[name = $repositorio]
                        return(
                            $i//inst/text()
                        )
                    return    
                        concat("/db/bdcol/",$instReep,"/",$repositorio)
                 )else if($institution)then(
                     concat("/db/bdcol/",$institution)   
                 )else(
                    '/db/bdcol'
                 )
                 
            let $tempScope := concat('collection($data-collection)',$confVars:root)(: $confVars:root tiene el nombre del root. Ej. dc_lom,lom; dc :)
            let $scope := util:eval($tempScope)
            (: 'Seteo' esta variable de session para utilizar en la facetación de la navegacion la primera vez:)
            let $scopeNavegacion := session:set-attribute("scopeNavegacion",$scope) 
        
        
            let $search-string := concat
               ('$scope', 
                $title-predicate,
                $author-predicate,
                $subject-predicate,
                $yearRange-predicate,
                (:$notRejected,:)
                $navegacion
               )
            let $search-string-clean-navegation := concat
               ('$scope', 
                $title-predicate,
                $author-predicate,
                $subject-predicate,
                $yearRange-predicate
                (:$notRejected:)
               )
               
            let $ojo := fn:trace($search-string-clean-navegation, 'The value of$search-string-clean-navegation is: ')
            
            let $data-collection := session:set-attribute("data-collection",$data-collection)
            let $advanceSearch := session:set-attribute("advanceSearchQuery",$search-string-clean-navegation )   
            let $hitsReturn := 
                    for $m in util:eval($search-string)
                              let $score := ft:score($m)
                              order by $score descending
                              return 
                              $m
            return $hitsReturn
        )
    
    
    
    
    let $lastSearch := session:set-attribute("lastSearch",$hits)
    
    let $config := <config xmlns="" width="100"/>
    
    let $total-result-count := count($hits)
    let $perpage := xs:integer(request:get-parameter("perpage", "10"))
    let $start := xs:integer(request:get-parameter("start", "0"))
    let $end := 
        if ($total-result-count lt $perpage) then 
            $total-result-count
        else 
            $start + $perpage
    
    (:[position() = ($start to $end)] :)
          (: let $results := display:recordBasic($refine,$start,$end), :)
    let $results := display:recordBasic($hits[position() = ($start to $end)]),
        $timeend := seconds-from-duration(util:system-time()-session:get-attribute("timestart")),
        $t := session:set-attribute("time", $timeend)
        
    let $number-of-pages := 
        xs:integer(ceiling($total-result-count div $perpage))
    let $current-page := xs:integer(($start + $perpage) div $perpage)
    let $url-params-without-start := replace(request:get-query-string(), '&amp;start=\d+', '')
    let $pagination-links := 
        if ($number-of-pages le 1) then ()
        else
            <ul>
                {
                (: Show 'Previous' for all but the 1st page of results :)
                    if ($current-page = 1) then ()
                    else
                        <li><a href="{concat('?', $url-params-without-start, '&amp;start=', $perpage * ($current-page - 2)) }">Previous</a></li>
                }
     
                {
                (: Show links to each page of results :)
                    let $max-pages-to-show := 20
                    let $padding := xs:integer(round($max-pages-to-show div 2))
                    let $start-page := 
                        if ($current-page le ($padding + 1)) then
                            1
                        else $current-page - $padding
                    let $end-page := 
                        if ($number-of-pages le ($current-page + $padding)) then
                            $number-of-pages
                        else $current-page + $padding - 1
                    for $page in ($start-page to $end-page)
                    let $newstart := $perpage * ($page - 1)
                    return
                        (
                        if ($newstart eq $start) then 
                            (<li>{$page}</li>)
                        else
                            <li><a href="{concat('?', $url-params-without-start, '&amp;start=', $newstart)}">{$page}</a></li>
                        )
                }
     
                {
                (: Shows 'Next' for all but the last page of results :)
                    if ($start + $perpage ge $total-result-count) then ()
                    else
                        <li><a href="{concat('?', $url-params-without-start, '&amp;start=', $start + $perpage)}">Next</a></li>
                }
            </ul>
    let $how-many-on-this-page := 
        (: provides textual explanation about how many results are on this page, 
         : i.e. 'all n results', or '10 of n results' :)
        if ($total-result-count lt $perpage) then (
             <label><strong>{$total-result-count}</strong> registros encontrados en <b>{session:get-attribute("time")}</b> segundos </label>
        )else(
                <label><strong>{$start + 1}</strong> - <strong>{$end}</strong> de <strong>{$total-result-count}</strong> registros encontrados en <b>{session:get-attribute("time")}</b> segundos </label>
        )
     return
                <div>
                    <div id="data">
                     {
                        if (empty($hits)) then (
                            <div id="searchresults"><h3>No se encontraron resultados para esta consulta</h3></div>
                        )
                        else
                            (
                            <div>
                                <label id="totalEncontrados">{$total-result-count}</label>
                                <label id="resultadosPara">Resultados busqueda avanzada. </label>
                                <label id="mostrandoTantos">Resultados, {$how-many-on-this-page}</label>
                                <label id="numberPages">{$number-of-pages}</label>
                                <div id="searchresults">{$results}</div>,
                                <div id="search-pagination">{$pagination-links}</div>
                            </div>
                            )
                        }
                    </div>
                </div>

};

