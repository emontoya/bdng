xquery version "1.0";
module namespace certificacionRegistros="http://exist-db.org/modules/certificacionRegistros";

declare namespace request="http://exist-db.org/xquery/request";
declare namespace file = "http://exist-db.org/xquery/file";
declare namespace xs="http://www.w3.org/2001/XMLSchema";
declare namespace dc="http://purl.org/dc/elements/1.1/"; (: 43152 madugirlsfirend :)

import module namespace kwic="http://exist-db.org/xquery/kwic";
import module namespace confVars="http://exist-db.org/modules/confVars"  at "configVariables.xq"; (:Variables de configuración :)
import module namespace configweb="http://exist-db.org/modules/configweb" at "/../user/configWeb.xq";


declare option exist:serialize "method=xhtml media-type=text/html";


declare function certificacionRegistros:actionCertificar ( $typeAction as xs:string? )  as xs:string? {


      let $idIdentifier := xs:string(request:get-parameter("idIdentifier",""))
      let $institucion := xs:string(request:get-parameter("institucion",""))
      let $repname := xs:string(request:get-parameter("repname",""))
      
      return
      
         if($typeAction eq "certify")then(
               update insert
                                    <metadata>
                                        <identifier>{$idIdentifier}</identifier>
                                        <date>{current-date()}</date>
                                    </metadata>
                                         into doc("/db/conf/whitelist.xml")//whitelist
         
         )else(
               update insert
                                    <metadata>
                                        <identifier>{$idIdentifier}</identifier>
                                        <date>{current-date()}</date>
                                    </metadata>
                                         into doc("/db/conf/blacklist.xml")//blacklist
         
         
         )

 };

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
    