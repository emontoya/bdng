xquery version "1.0";

declare namespace request="http://exist-db.org/xquery/request";
declare namespace session="http://exist-db.org/xquery/session";
declare namespace xs="http://www.w3.org/2001/XMLSchema";
declare namespace dc="http://purl.org/dc/elements/1.1/";
declare namespace oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/";
declare namespace oai_lom="http://ltsc.ieee.org/xsd/LOM";

import module namespace kwic="http://exist-db.org/xquery/kwic";

declare option exist:serialize "method=xhtml media-type=text/html";

import module namespace busquedaBasica="http://exist-db.org/modules/busquedaBasica"  at "./libraryModules/busquedaBasica.xq";
import module namespace busquedaAvanzada="http://exist-db.org/modules/busquedaAvanzada"  at "./libraryModules/busquedaAvanzada.xq";
import module namespace salidaFacetada="http://exist-db.org/modules/salidaFacetada"  at "./libraryModules/salidaFacetada.xq";
import module namespace repositorio="http://exist-db.org/modules/repositorio" at "./libraryModules/repositorios.xq";

(: Función llamada inmediatamente después de cada consulta (avanzada ó basica) para realizar la facetación de los datos :)
declare function local:facetarAfter() as element()*
{
   let $lastSearch := session:get-attribute("lastSearch") 
   let $type := xs:string(request:get-parameter("type",""))
   let $type := xs:string(request:get-parameter("state","")) (::)
   let $show :=
            salidaFacetada:facetada($lastSearch, $query, $type)
    return $show 
};


(: CONTROLLER :)
let $typeSearch := xs:string(request:get-parameter("typesearch","")),
    $timestart := util:system-time(),
    $ts:=session:set-attribute("timestart",$timestart)
 return
    if($typeSearch eq "navegacionFacetar")then(
        let $item := xs:string(request:get-parameter("item",""))
        let $institution := xs:string(request:get-parameter("institution",""))
        return
            salidaFacetada:navegacion($item,$institution)
    )else(

    )

    
    
    
    
    
    
    
    
    
