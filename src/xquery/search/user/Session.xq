xquery version "1.0";

declare namespace request="http://exist-db.org/xquery/request";
declare namespace response="http://exist-db.org/xquery/response";
declare namespace session="http://exist-db.org/xquery/session";
declare namespace xdb="http://exist-db.org/xquery/xmldb";
declare namespace xmldb="http://exist-db.org/xquery/xmldb";

import module namespace usuario="http://exist-db.org/modules/usuarios" at "User.xq";
import module namespace configweb="http://exist-db.org/modules/configweb" at "configWeb.xq";
import module namespace loginF="http://exist-db.org/modules/loginF" at "loginF.xq";

declare variable $database-uri as xs:string { "xmldb:exist:///db/users" };

 loginF:Login()
(:
 let $user := request:get-parameter("password", ("luis")) cast as xs:string
 let $pass := request:get-parameter("username", ("asd")) cast as xs:string
 

 


  let $login:= system:as-user("admin", $configweb:passwordAdmin, xdb:authenticate($database-uri, $user, $pass))
  

   (: let $login := xdb:authenticate($database-uri, $user, $pass):)
    return
        if ($login) then (
        let $activo:= usuario:EsUsuarioActivo($user)
        return
                if($activo) then
                (
                    
                  let $userSet := session:set-attribute("user", $user),
                    $passset := session:set-attribute("password", $pass),
                    $name := usuario:ObtenerNombreUsuario($user),
                    $type := usuario:ObtenerTipoDeUsuario($user)
                    return
                    <div id="data">
              
                        <div id="loginsuccess">yes</div>
                        <div id="name"> {$name} </div>
                        <div id="type"> {$type} </div>
                        <div id="username"> {$user} </div>
                    </div>
                    
                )
                else
                (   
                        <div id="data">
                            <div id="name">Inactivo</div>
                            <div id="loginsuccess">no</div>
                        </div>
                )
            
        ) else
        (
                <div id="data">
                        <div id="loginsuccess">no</div>
                        <div id="name">Wrong</div>
                </div>              
          )
:)