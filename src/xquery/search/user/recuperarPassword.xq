xquery version "1.0";


(: Demonstrates sending an email through Sendmail from eXist :)
import module namespace usuario="http://exist-db.org/modules/usuarios" at "User.xq";
declare namespace mail="http://exist-db.org/xquery/mail" ;
declare namespace request="http://exist-db.org/xquery/request";
import module namespace correo="http://exist-db.org/modules/mails" at "mail.xq";
declare namespace xmldb="http://exist-db.org/xquery/xmldb";
declare namespace util="http://exist-db.org/xquery/util";
import module namespace configweb="http://exist-db.org/modules/configweb" at "configWeb.xq";
declare variable $redirect-uri as xs:anyURI { xs:anyURI("../index.xq") };
util:catch(
	"java.lang.Exception",
(
	
    	(: let $usuario: = request:get-parameter("txtNombreUsuario", ()) :)
  
    	
    	let $fromJsp: = request:get-parameter("fromJsp", ())
    	let $email: = request:get-parameter("txtMail", ())
    	

    	
    	let $usuario:=
         
        if($email eq  configweb:get-admin-email()) then (
                "admin"
                
            ) else(
                usuario:ObtenerUsername($email)
            )
    	
    	return
    	 if(($fromJsp eq "si" and $email ne configweb:get-admin-email() and $usuario eq "admin" ) ) then(
    	 
    	 
    	   
                     <div id="data">
                        <div id="resp">no</div>
                       
                    </div>
                
            ) else(
            
            

    	
    	
    	let $url := session:encode-url($redirect-uri)
    	return
    	if($email) then 
    	(
    	
    	let $datos:=correo:EnviarCorreoRecuperarPassword($usuario ,$email )
    	return
        	if($datos eq "1") then
        	(
        		configweb:PaginaExito("Recuperación de la contraseña","El proceso de recuperación de contraseña se realizó con éxito")
        		
        	)
            	else
            	(
            		configweb:PaginaError("No se ha podido recuperar la contraseña, por favor contacte al administrador del sistema")
            	)
    	)else
    	(
    		configweb:PaginaError("No se ha podido recuperar la contraseña, por favor contacte al administrador del sistema")
    	)
    )),
    	configweb:PaginaError("El email ingresado no está registrado, por favor contacte al administrador del sistema.")
)
