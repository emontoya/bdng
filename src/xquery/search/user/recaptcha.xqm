xquery version "1.0";

module namespace recap="http://exist-db.org/recaptcha";
declare namespace request="http://exist-db.org/xquery/request";
declare namespace httpclient = "http://exist-db.org/xquery/httpclient";
declare namespace util="http://exist-db.org/xquery/util";
import module namespace configweb="http://exist-db.org/modules/configweb" at "configWeb.xq";
 
(:~
: Module for working with reCaptcha
:)
 
declare function recap:validate($private-key as xs:string, $recaptcha-challenge as xs:string?, $recaptcha-response as xs:string?) as xs:boolean
{
  let $datos:= $recaptcha-challenge
  return
  if($datos) then
  (
  
			  (: if behind webserver proxy :) 
			 (:<httpclient:field name="remoteip" value="{$client-ip}"/>:)
			let $client-ip := "200.12.180.127"
			let $l := util:log("error", ("$recaptcha-challenge: ", $recaptcha-challenge))
			let $l := util:log("error", ("$recaptcha-response: ", $recaptcha-response))
			   let  $post-fields := <httpclient:fields>
						<httpclient:field name="privatekey" value="{$private-key}"/>
						 <httpclient:field name="remoteip" value="{$client-ip}"/>
						<httpclient:field name="challenge" value="{$recaptcha-challenge}"/>
						<httpclient:field name="response" value="{$recaptcha-response}"/>
			   </httpclient:fields> 

			let $l := util:log("error", ("$post-fields: ", $post-fields))
				 let $response := httpclient:post-form($configweb:VALIDATE_URI, $post-fields, false(), ()) return
				 let $l := util:log("error", ("$response: ", $response))
						let $recapture-response := $response/httpclient:body/text() 
						return
							if(starts-with($recapture-response, "true"))then
							(
								true()
							)
							else
							(
								(: util:log("debug", concat("reCaptcha response='", $capture-response, "'")), :)    (: uncomment to debug reCaptcha response :)
								false()
							)
		)
		else
		(
			false()
		)
};