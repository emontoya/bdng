
/*
	Aqui se valida  los campos 
	*/
var mensajes = {
	txtNombre:{ required: "El nombre y apellido son obligatorios" , regex:"El nombre tiene caracteres invalidos"},
	txtUsername:{ required: "El nombre de usuario es obligatorio", regex:"El nombre de usuario tiene caracteres invalidos"},
	txtPassword:"La contraseña es obligatoria",
	txtConfirmarPassword: { required: "La confirmación de la contraseña es obligatoria", equalTo: "La contraseña y su confirmacion deben coincidir"},
	txtEmail: { required: "El email es obligatorio", email: "El email tiene caracteres inválidos"},
	txtFoto:  { accept:"Tipo de archivo no v´alido" },
	txtNombreUsuario:  "El nombre de usuario es obligatorio",
	user: "El usuario es obligatorio",
	pass: "La contraseña es obligatoria"
}

// Reglas que vamos a aplicar
var reglas = {
        txtNombre: { required: true,  regex:/^[A-Za-z\s\.]*$/  },
	txtUsername: { required: true,  regex: /^[A-Za-z](?=[A-Za-z0-9_.]{3,31}$)[a-zA-Z0-9_]*\.?[a-zA-Z0-9_]*$/ },
	txtPassword: "required",
	txtConfirmarPassword: { required: true,  equalTo: "#txtPassword"  },
	txtEmail: { required: true, email: true},
	txtFoto : { accept: "jpg|gif|png|bmp" },
	txtNombreUsuario: "required",
	user: "required",
	pass: "required"
}

jQuery.validator.addMethod(
        "regex",
        function(value, element, regexp) {
            if (regexp.constructor != RegExp)
                regexp = new RegExp(regexp);
            else if (regexp.global)
                regexp.lastIndex = 0;
            return this.optional(element) || regexp.test(value);
        },
        "Please check your input."
);


$(document).ready
        (
	
            function()
                {
					var RecaptchaOptions = {
						theme : 'red',
						lang : 'es'
					};
					$("#frmRegistro").validate
					  (
						  {
								event: "blur",
								rules:reglas,
								messages: mensajes,
								errorLabelContainer : "#messageBox ul",
								errorContainer:  "#messageBox",
								wrapper: "li"
							   
						  }
					  );
					 $("#frmRecuperar").validate
					  (
						    {
								event: "blur",
								rules:reglas,
								messages: mensajes,
								errorLabelContainer : "#messageBox2 ul",
								errorContainer:  "#messageBox2",
								wrapper: "li",
								submitHandler : function (form)
								{
									form.submit();
								}
							}
					);
					 $("#frmUpdate").validate
					  (
						    {
						    	event: "blur",
								rules:reglas,
								messages: mensajes,
								errorLabelContainer : "#messageBox ul",
								errorContainer:  "#messageBox",
								wrapper: "li"
							}
					); 
					 
					 
								  
				   $("#frmchangePassword").validate
				  (
					  {
							event: "blur",
							rules:reglas,
							messages: mensajes,
							errorLabelContainer : "#messageBox ul",
							errorContainer:  "#messageBox",
							wrapper: "li"
					 }
				  );
                  
               }
          ); 
		  
function RecuperarPassword()
{
     var trRecuperarPassword = document.getElementById("trRecuperarPassword");
     trRecuperarPassword.setAttribute("class", "visible");
}

function IrARecuperarPassword()
{
	 var nombreUsuario=document.getElementById("user").value;
	 var hdnRecuperar="1";
	 window.location.href="login.xql?hdnRecuperar="+hdnRecuperar;
}
		 
function irRegistro()
{
	location.href="register.xq";
}
