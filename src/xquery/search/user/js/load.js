var req;
var detailsDiv;
var persistclose=0;
var startX=0;
var startY=0;
var verticalpos="fromtop";

function loadXMLDoc(url) {
    // branch for native XMLHttpRequest object
    if (window.XMLHttpRequest) {
        req = new XMLHttpRequest();
        req.onreadystatechange = processReqChange;
        req.open("GET", url, true);
        req.send(null);
    // branch for IE/Windows ActiveX version
    } else if (window.ActiveXObject) {
        req = new ActiveXObject("Microsoft.XMLHTTP");
        if (req) {
            req.onreadystatechange = processReqChange;
            req.open("GET", url, true);
            req.send();
        }
    }

}

function processReqChange(data) {
    if (req.readyState == 4) {
        // only if "OK"
        if (req.status == 200) {
            details.innerHTML = req.responseText;
        } else {
            alert("There was a problem retrieving the XML data:\n" +
            req.statusText);
        }
    }
   
//	details.innerHTML = data;
    
      
}


function loadContenidoComentarios(identificador,id) {
    
    var dvc = document.getElementById("dvc_" + identificador+"_"+id);
	
	if(dvc){
		dvc.setAttribute("class", "visible");
		
	}
	var link = document.getElementById("acc_" + identificador);
	 if (link) {
        link.href = "javascript:hideContenidoComentarios('" + identificador + "');";
        
    }
   

}
function hideContenidoComentarios(identificador,id) {
	var link = document.getElementById("acc_" + identificador+"_"+id);
	 var dvc = document.getElementById("dvc_" + identificador+"_"+id);
    if (link) {
        link.href = "javascript:loadContenidoComentarios('" + identificador + "');";
        
    }

    
   // dvc.setAttribute("class", "hidden");
   
    var idDiv ="#"+dvc.id;
    $(idDiv).addClass("hidden");
 
}
function hideComentarios(identificador) {
	var link = document.getElementById("ac_" + identificador);
	details= document.getElementById("fc_" + identificador);
    if (link) {
        link.href = "javascript:loadComentarios('" + identificador + "');";
        
    }

    var idDiv ="#"+dvc.id;
   // $(idDiv).addClass("hidden");
    $(idDiv).css({ visibility:"hidden" }); 
 
}
function expandAll(start, end) {
    for (var i = start; i < end; i++) {
        loadDetails(i);
    }
}

function collapseAll(start, end) {
    for (var i = start; i < end; i++) {
        hideDetails(i);
    }
}

function toggleCheckboxes() {
    a = document.mainForm.elements;
    l = a.length;
    for (var i = 0; i < l; i++)
    if (a[i].type == "checkbox") a[i].checked = true;
}

function exportData() {
    document.mainForm.action = "export.xq";
    document.mainForm.target = "_new";
    document.mainForm.submit
}

function showError(message) {
    alert(message)
}

var divs_duplicado = new Array(10); 
/*
Permite abrir un popup de comentarios
*/
function abrirLightbox(windowID, borrarContenido, contenedorcampos,identificador){

            
   	
	this.id=windowID
	barheight=document.getElementById(this.id).offsetHeight
	var ns=(navigator.appName.indexOf("Netscape")!=-1)|| window.opera
	var d=document
	function ml(id){
		var el=d.getElementById(id)
		if (!persistclose || persistclose && get_cookie("remainclosed") == "") {
		    el.style.display = "block"
		 
		}
		if(d.layers)el.style=el
		el.sP=function(x,y){this.style.left=getLeftLightbox(el);this.style.top=y+"px";}
		el.x=startX
		if(verticalpos=="fromtop")
		el.y=startY
		else{
			el.y=ns ? pageYOffset+innerHeight : iecompattest().scrollTop+iecompattest().clientHeight
			el.y-=startY}
		return el}
	window.stayTopLeft=function(){
		if(verticalpos=="fromtop"){
			var pY=ns ? pageYOffset : iecompattest().scrollTop
			ftlObj.y+=(pY+startY-ftlObj.y)/8}
		else{
			var pY=ns ? pageYOffset+innerHeight-barheight: iecompattest().scrollTop+iecompattest().clientHeight-barheight
			ftlObj.y+=(pY-startY-ftlObj.y)/8
			}
		ftlObj.sP(ftlObj.x,ftlObj.y)
		setTimeout("stayTopLeft()",10)}
	ftlObj=ml(this.id)
	stayTopLeft()

	var datos = "#" + id;
	
	$user_dialog = new Boxy($(datos), { title: "Ingreso de Comentarios", closeable: false, modal: true });
	$user_dialog.show();
}
// Permite poner abajo en la pagina una mascara
function ponerVelo(){
   document.getElementById('vermask').className='layerlightver';
}
// cierra el popup de comentarios
function cerrarLightbox(windowID){
	quitarVelo();
	this.id=windowID
	if(persistclose)
	document.cookie="remainclosed=1"
	document.getElementById(this.id).style.display="none"
}



var fWidth;
function getLeftLightbox(divcito){
	//For old IE browsers
	if(document.all)
		fWidth = document.body.clientWidth;
	//For DOM1 browsers
	else if(document.getElementById &&!document.all)
		fWidth = innerWidth;
	else if(document.getElementById)
		fWidth = innerWidth;
	//For Opera
	else if (is.op)
		fWidth = innerWidth;
	//For old Netscape
	else if (document.layers)
		fWidth = window.innerWidth;
		
	var widthdivcito = obtenerEstilo(divcito,"width");
	return ((fWidth/2)-(widthdivcito/2))+"px";
}
function obtenerEstilo(elemento,estilo){
	if(elemento.width)
	   return elemento.width;
	
	style = estilo;
	value=elemento.style.estilo;
	if (!value) {
		  if (document.defaultView && document.defaultView.getComputedStyle) {
			var css = document.defaultView.getComputedStyle(elemento, null);
			value = css ? css[style] : null;
		  } else if (elemento.currentStyle) {
			value = elemento.currentStyle[style];
		  }
	}
	if(isNaN(value))
	   return elemento.offsetWidth;
	else if(value)
		return value.replace("px","");
	else
		return 0;
}


function iecompattest(){
	return(document.compatMode&&document.compatMode!="BackCompat")? document.documentElement : document.body
}


function checkLengthMultilineOnPress(obj,largo)
{
	
	if(obj.value.length >= largo)
		return false;
	else
		return true;
}

function checkLengthMultilineOnChange(obj,largo)
{
	
	if(obj.value.legnth >= largo)
		obj.value=obj.value.substring(0,largo);
}

function RealizarBusqueda()
{
	var query = document.forms.searchform.query.value;
	location.href= "./index.xq?query="+query;
}
function confirm(message, callback1,callback2) {
			$('#confirm').modal({
				closeHTML:"<a href='javascript:RealizarBusqueda();' title='Close' class='modal-close'>x</a>",
				position: ["20%",],
				overlayId:'confirm-overlay',
				containerId:'confirm-container', 
				onShow: function (dialog) {
					$('.message', dialog.data[0]).append(message);
		
					// if the user clicks "yes"
					$('#btnAceptarConfirmacion2', dialog.data[0]).click(function () {
						// call the callback
						alert(1);
						if ($.isFunction(callback1)) {
							callback1.apply();
						}
						// close the dialog
						$.modal.close();
					});
					$('#btnCancelarConfirmacion', dialog.data[0]).click(function () {
						// call the callback
						if ($.isFunction(callback2)) {
							callback2.apply();
						}
						// close the dialog
						$.modal.close();
					});
					
				}
			});
		}

$(function(){
			$('.live_search').focus(function() {
				var live_class = $(this).attr("id");
				$(this).liveUpdate("."+live_class,$(this),$('.live_search_clear'));
			});				
});


