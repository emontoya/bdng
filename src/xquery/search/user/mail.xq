xquery version "1.0";

module namespace correo="http://exist-db.org/modules/mails";

(: Demonstrates sending an email through Sendmail from eXist :)

import module namespace usuario="http://exist-db.org/modules/usuarios" at "User.xq";
declare namespace mail="http://exist-db.org/xquery/mail";
declare namespace request="http://exist-db.org/xquery/request";
declare namespace xmldb="http://exist-db.org/xquery/xmldb";
declare namespace util="http://exist-db.org/xquery/util";
import module namespace configweb="http://exist-db.org/modules/configweb" at "configWeb.xq";


declare function correo:CrearMensajeActivarUsuario($usuario as xs:string,$password as xs:string , $datoConfirmacion as xs:string, $email as xs:string ) as node()*
{

let $url : = xmldb:decode-uri(xs:anyURI(concat(configweb:uri-emailValidation(),$usuario,"__",$datoConfirmacion)))
let $mensaje := configweb:ContenidoCorreoActivacion($usuario,$url,$password, $email)
return $mensaje
};


(: Para enviar el correo de confirmacion por parte del usuario :)

declare function correo:EnviarCorreoConfirmacionUsuario($usuario as xs:string,$password as xs:string , $datoConfirmacion as xs:string, $email as xs:string) as element()*
{
       util:catch(
            "java.lang.Exception",
            (
                   let $l := util:log("error", ("$Envio mensaje: ", "Envio mensaje"))
                   let $mensaje:=correo:CrearMensajeActivarUsuario($usuario,$password,$datoConfirmacion, $email)
                            return
                              if(mail:send-email($mensaje,$configweb:ip-smtp, ()))then
                                (
                                    <h1>Se envio el mensaje con exito</h1>
                                )
                                else
                                (
                                        <h1>No se pudo enviar el mensaje</h1>
                                )
              ),
            <error>No se pudo enviar el mensaje: {$util:exception-message}</error>
        )
};
declare function correo:EnviarCorreoRecuperarPassword($usuario as xs:string,$email as xs:string ) as xs:string
{
       util:catch("java.lang.Exception",
                    (
                                  let $mensaje:=correo:CrearMensajeRecuperarPassword($usuario,$email)
				                  let $resultadoEnvio:=mail:send-email($mensaje,$configweb:ip-smtp, ())
                                  let $loga := util:log("error", ("$envio correo rec: ", $resultadoEnvio))
                                            return
                                              if($resultadoEnvio) then
                                                (
                                                    "1"
                                                )
                                                else
                                                (
                                                        "0"
                                                )
                      ),
                       configweb:PaginaError("No se ha podido recuperar la contraseña, no se envio el usuario")
                    )
};
declare function correo:CrearMensajeRecuperarPassword($usuario as xs:string,$email as xs:string) as node()*
{
            
    let $password :=
    
        if($usuario eq "admin") then (
                configweb:get-admin-password() 
            ) else(
                usuario:ObtenerRecuperacionPassword($usuario)
            )
        let $mensaje := configweb:ContenidoCorreoRecuperacionPassword($usuario,$password,$email)
        return $mensaje
};

(: Envio de mail para activacion de un usuario tipo AdminRepo - luis:)

declare function correo:EnviarCorreoActivacionAdminRepo($usuario as xs:string, $email as xs:string, $datoConfirmacion as xs:string, $institucion as xs:string, $telefono as xs:string ) as element()*
{
	 util:catch(
            "java.lang.Exception",
            (
                   let $l := util:log("error", ("$Envio mensaje: ", "Envio mensaje"))
                   let $mensaje:=correo:CrearMensajeActivarAdminRepo($usuario,$email,$datoConfirmacion,$institucion,$telefono)
                            return
                              if(mail:send-email($mensaje,$configweb:ip-smtp, ()))then
                                (
                                    <h1>Se envio el mensaje con exito</h1>
                                )
                                else
                                (
                                        <h1>No se pudo enviar el mensaje</h1>
                                )
              ),
            <error>No se pudo enviar el mensaje: {$util:exception-message}</error>
        )
};


declare function correo:CrearMensajeActivarAdminRepo($usuario as xs:string, $email as xs:string, $datoConfirmacion as xs:string, $institucion as xs:string, $telefono as xs:string) as node()*
{

let $url : = xmldb:decode-uri(xs:anyURI(concat(configweb:uri-emailValidation(),$usuario,"__",$datoConfirmacion)))
let $mensaje := configweb:ContenidoCorreoActivacionAdminRepo($usuario,$url,$email,$institucion,$telefono) 
return $mensaje
};

(: Envio de mail para la confirmacion de la activacion de un usuario tipo AdminRepo - luis:)

declare function correo:EnviarCorreoConfirmacionAdminRepo($usuario as xs:string,$email as xs:string ) as element()*
{
       util:catch(
            "java.lang.Exception",
            (
                   let $l := util:log("error", ("$Envio mensaje: ", "Envio mensaje"))
                   let $mensaje:=configweb:ContenidoCorreoConfirmacionAdminRepo($usuario, $email)
                            return
                              if(mail:send-email($mensaje,$configweb:ip-smtp, ()))then
                                (
                                    <h1>Se envio el mensaje con exito</h1>
                                )
                                else
                                (
                                        <h1>No se pudo enviar el mensaje</h1>
                                )
              ),
            <error>No se pudo enviar el mensaje: {$util:exception-message}</error>
        )
};



