xquery version "1.0";

declare namespace xpath="http://www.w3.org/2005/xpath-functions";
declare namespace system="http://exist-db.org/xquery/system";
declare namespace request="http://exist-db.org/xquery/request";
import module namespace usuario="http://exist-db.org/modules/usuarios" at "User.xq";
import module namespace configweb="http://exist-db.org/modules/configweb" at "configWeb.xq";
declare namespace response="http://exist-db.org/xquery/response";
declare variable $redirect-uri as xs:anyURI { xs:anyURI("index.xq") };
import module namespace correo="http://exist-db.org/modules/mails" at "mail.xq";

let $dato := request:get-parameter("dato", ())
let $tokenizador := xpath:tokenize($dato,"__")

let $usuario := $tokenizador[1]
let $confirmacion := $tokenizador[2]

return    
   let $datos:= usuario:activarUsuario($usuario,$confirmacion)
   let $url := session:encode-url($redirect-uri)
   let $type :=usuario:ObtenerTipoDeUsuario($usuario)
   let $email :=usuario:ObtenerEmaileUsuario($usuario)
   let $mensajeConfirmacion := 
                if($type eq "adminRepo" and $datos eq "1") then (
                    
                    	correo:EnviarCorreoConfirmacionAdminRepo($usuario,$email)
                )else()
   
       return
       if($datos eq "1") then
       (
   			
       
       configweb:PaginaExito("Activación de usuario","El usuario ha sido activado con éxito")
       
       
         )
       else
       (
         	configweb:PaginaError("No se ha podido activar el usuario")
       )
       

