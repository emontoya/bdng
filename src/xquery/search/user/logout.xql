xquery version "1.0";

declare namespace request="http://exist-db.org/xquery/request";
declare namespace session="http://exist-db.org/xquery/session";
declare namespace xdb="http://exist-db.org/xquery/xmldb";

declare variable $database-uri as xs:string { "xmldb:exist:///db" };
declare variable $redirect-uri as xs:anyURI { xs:anyURI("/bdcol/search/index.xq") };

session:invalidate(),
response:redirect-to(session:encode-url($redirect-uri))
