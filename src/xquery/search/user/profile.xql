xquery version "1.0";


declare namespace xdb="http://exist-db.org/xquery/xmldb";
import module namespace loginF="http://exist-db.org/modules/loginF" at "loginF.xq";
import module namespace configweb="http://exist-db.org/modules/configweb" at "configWeb.xq";
import module namespace usuario="http://exist-db.org/modules/usuarios" at "User.xq";



declare function local:Update() as xs:string{

let $username := session:get-attribute("user")

let $nombre:=request:get-parameter("txtNombre","0") cast as xs:string
let $email:=request:get-parameter("txtEmail","0") cast as xs:string
let $institucion:=request:get-parameter("txtInstitucion","0") cast as xs:string
let $telefono:=request:get-parameter("txtTelefono","u0er") cast as xs:string
let $update:= usuario:UpdateProfile($username, $nombre, $email, $institucion, $telefono)
    return
    if( $update) then ("1") 
        else ("2")
    
    
(:let $subscripcion:=request:get-parameter("chkSubscripcion","0") cast as xs:string:)


};



(:permite saber si mostrar el form de cambio de contraseña o si ya se realizo el action:) 
declare function local:show-form() as element()* {

   let $action := request:get-parameter("action", ())
        return   
           if($action eq "Actualizar") then(
                        let $result := local:Update()
                        
                        return 
                         if($result eq "1") then (
                           <div   id="messageBox" name="messageBox" class="sucessPrompt" style="display:block;" >
                              <ul>
                                      <li>
                                          <label class="error" generated="true" style="display: block;">Su perfil se actualizado exitosamente</label>
                                          <br/>
                                          <a href="{$configweb:redirect-uri}">Ir a BDCOL</a>
                                       </li>
                              </ul>
                          </div>,
                          local:displayFormChangePass()
                            )
                            else(
                            <div   id="messageBox" name="messageBox" class="sucessPrompt" style="display:block;" >
                              <ul>
                                      <li>
                                          <label class="error" generated="true" style="display: block;">Error en el proceso de actualizacion del perfil</label>
                                       </li>
                              </ul>
                             </div>,
                          local:displayFormChangePass()
                            
                            
                            )
                            
                        )else if (session:exists() and session:get-attribute("user")) then(
                        
                            local:displayFormChangePass()
                        )else(
                            <div   id="messageBox" name="messageBox" class="sucessPrompt" style="display:block;" >
                              <ul>
                                      <li>
                                          <label class="error" generated="true" style="display: block;">Debe haber iniciado sesion para visualizar su perfil</label>
                                          <br/>
                                          <br/>
                                          <a href="{$configweb:redirect-uri}">Ir a BDCOL</a>
                                       </li>
                              </ul>
                             </div>
                        
                        )

};


(:muestra el form de cambio de password:)
declare function local:displayFormChangePass() as element()*{

let $username := session:get-attribute("user")
let $email:= usuario:ObtenerEmaileUsuario($username)
let $nombre:= usuario:ObtenerNombreUsuario($username)
let $institucion:= usuario:ObtenerInstitucionUsuario($username)
let $telefono:= usuario:ObtenerTelefonoUsuario($username)
let $telefono:= usuario:ObtenerTelefonoUsuario($username)
let $foto := usuario:ObtenerImagenUsuario($username)

return

<div id="changePassword">
            <form name="frmUpdate" id="frmUpdate" method="post" enctype="multipart/form-data" action="profile.xql" style='padding:10px; background:#fff; text-align:center;'>
                        
                                    <p>Puede actualizar los datos de su cuenta mediante el siguiente formulario</p>
                                          <table class="block"  cellpadding="5" width="600px" align="center" >
                                          
                                                             
                                      
                                                              
                                                      <tr>
                                                                <th colspan="2" align="left"><h3>Mi Perfil</h3></th>
                                                      </tr>
                                                      <tr>
                                                                <td colspan="2" align="left"><img src="images/users/{$foto}" width="130px" height="130px"></img></td>
                                                      </tr>
                                                               
                                                      <tr>
                                                                <td align="left">Nombre de usuario:</td>
                                                                <td align="left">{session:get-attribute("user")}</td>
                                                      
                                                           
                                                      </tr>
                                                            
                                                       <tr>
                                                            <td>
                                                                <label>Nombres y Apellidos: *</label>
                                                            </td>
                                                            <td>
                                                            
                                                                <input type="text" size="60" id="txtNombre" name="txtNombre" maxlength="40" value="{$nombre}" />
                                                            </td>
                                                        </tr>
                                                        
                                                        <tr>
                                                        <td>
                                                            <label>Correo Electrónico: *</label>
                                                        </td>
                                                        <td>
                                                            <input type="text" size="60" id="txtEmail" name="txtEmail" maxlength="30" value="{$email}"/>
                                                            
                                                            
                                                            </td>
                                                        </tr>   
                                                        <tr >
                                                        <td>
                                                            <label>Institucion: </label>
                                                        </td>
                                                        <td>
                                                            <input type="text" size="60" id="txtInstitucion" name="txtInstitucion" maxlength="90" value="{$institucion}"/>
                                                            
                                                            
                                                        </td>
                                                        
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label>Telefono de Contacto: </label>
                                                        </td>
                                                        <td>
                                                            <input type="text" size="60" id="txtTelefono" name="txtTelefono" maxlength="20" value="{$telefono}"/>
                                                            
                                                            
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td coslpan="2">
                                                            <label><a  href="changePassword.xql">Cambiar Contraseña</a> </label>
                                                        </td>
                                                        
                                                    </tr>
                                                            <tr>
                                                                <td coslpan="2">
                                                                    <input name="action" id="btnUpdate" type="submit" text="Enviar" size="20" value="Actualizar"  />
                                                                        <input name="action" id="btnRegresar" type="button" text="Regresar" size="20" value="Regresar"    
                                                                        onClick="window.location.href='../index.xq'; " />
                                                                    
                                                                </td>
                                                               </tr>
                                                               
                                                                <tr>
                                                                        <td colspan="2">
                                                                                    <br/>                                                                        
                                                                        </td>
                                                                </tr>
                                                                   
                                        </table>
                                    
                            
             </form>
        </div>





};


<html>
    <head>
		 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>{$configweb:sysName} - {$configweb:sysNameFull}</title>
        <link href="css/display.css" type="text/css" rel="stylesheet"/>
        <link href="{$configweb:cssRegister}" type="text/css" rel="stylesheet"/>   
        <link href="css/front.css" media="screen, projection" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="css/colorbox.css" />  
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js">a</script>
        <script language="Javascript" type="text/javascript" src="js/jquery.validate.js"  charset="utf-8">a</script>   
        <script language="Javascript" type="text/javascript" src="{$configweb:jsUsuario}" charset="utf-8">a</script>
        <script language="Javascript" type="text/javascript" src="js/jquery.validate.js"  charset="utf-8">a</script>
        <script type="text/javascript" src="js/jsLogin.js" charset="utf-8">a</script>
        <script type="text/javascript" src="js/jquery.colorbox.js" charset="utf-8">a</script>
        
        
        
    </head>
    <body class="body2">
    
    
         <div class="header-ppal"></div>
         <div class="windows-content">
	
                          { loginF:show-login() }
		                  {local:show-form()}
		 </div>                                       
       
    </body>
</html>
