xquery version "1.0";

declare namespace request="http://exist-db.org/xquery/request";
declare namespace response="http://exist-db.org/xquery/response";
declare namespace session="http://exist-db.org/xquery/session";
declare namespace xdb="http://exist-db.org/xquery/xmldb";
declare namespace xmldb="http://exist-db.org/xquery/xmldb";

import module namespace usuario="http://exist-db.org/modules/usuarios" at "User.xq";
import module namespace configweb="http://exist-db.org/modules/configweb" at "configWeb.xq";


 let $password := request:get-parameter("password", ("admin")) cast as xs:string
 let $username := request:get-parameter("username", ("admin")) cast as xs:string

 let $update:= system:as-user("admin", $configweb:passwordAdmin, xdb:change-user($username, $password, "dba", $configweb:database-login))
 let $passset := session:set-attribute("password", $password) (:se hace el set del nuevo password para que quede en la session:) 
 let $change:= usuario:ChangePassword( $username, $password)

  
  
  let $userSet := session:set-attribute("user",$username )
  let $passset := session:set-attribute("password", $password)
  
  let $autenticacion := xmldb:authenticate($configweb:database-login, $username, $password)

 return <exito>{xmldb:get-current-user()}</exito>