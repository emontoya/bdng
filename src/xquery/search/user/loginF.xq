xquery version "1.0";

module namespace loginF="http://exist-db.org/modules/loginF";

declare namespace request="http://exist-db.org/xquery/request";
declare namespace response="http://exist-db.org/xquery/response";
declare namespace session="http://exist-db.org/xquery/session";
declare namespace xs="http://www.w3.org/2001/XMLSchema";
declare namespace xmldb="http://exist-db.org/xquery/xmldb";
declare namespace xdb="http://exist-db.org/xquery/xmldb";
import module namespace configweb="http://exist-db.org/modules/configweb" at "configWeb.xq";
import module namespace usuario="http://exist-db.org/modules/usuarios" at "User.xq";

declare option exist:serialize "method=xhtml media-type=text/html";

declare variable $loginF:database-uri as xs:string { "xmldb:exist:///db" };
declare variable $loginF:redirect-uri as xs:anyURI { xs:anyURI("index.xq") };



(:obtenemos el nombre del usuario de la session si es que esta existe -Luis:) 
declare function loginF:nombreSession() as xs:string
{
        let $sessionActiva :=  session:get-attribute("user")
        return
        if($sessionActiva eq "admin") then 
            ( "admin")
        else( 
        
        (:
        let $nombre := if (session:exists() and session:get-attribute("user")) then (
                            usuario:ObtenerNombreUsuario(session:get-attribute("user"))
                    ) else (
                            "admin"
                    )
            
            
        return $nombre
        :)
        "admin"
        
    )
        
};



declare function loginF:Recuperar() as xs:string
{
let $recuperar := request:get-parameter("hdnRecuperar", ())
return
if ($recuperar eq "1") then
    "display"
else (
    "none"
    )
};


(:funcion con la que controlamos si mostramos el dvi contenedor de logout para admin -Luis:)
declare function loginF:EstiloLoginAdmin() as xs:string
{

    let $sessionActiva :=  session:get-attribute("user")
    return
    if( $sessionActiva eq "admin") then
    (
        "display"
    )
    else(
        "none"
    )  
};



(:funcion con la que controlamos si el nombre del usuario logueado se muestra o no -Luis:)
declare function loginF:EstiloLogin() as xs:string
{

    let $sessionActiva :=  session:get-attribute("user")
    return
    if( $sessionActiva eq "admin") then
    (
        "none"
    )
    else(
        if (session:exists() and session:get-attribute("user")) then
            "display"
        else (
            "none"
        )
    )  
};


(:funcion con la que controlamos si el Form de Login se muestra o no -Luis:)
declare function loginF:EstiloForm() as xs:string
{
    if (session:exists() and session:get-attribute("user")) then
        "none"
        else (
        
        "display"
       )
};


(:Funcion utilizada mediante ajax para hacer login:)

declare function loginF:Login() as element()*
{
    let $user := request:get-parameter("user"," ")
    let $pass := request:get-parameter("pass"," ")
    let $login := xdb:authenticate($loginF:database-uri, $user, $pass)
    return
        if ($login) then (
                        let $userSet := session:set-attribute("user", $user),
                            $passset := session:set-attribute("password", $pass)
                            return
        
            if($user eq "admin") then (
                         <div id="data">
                    
                                <div id="isAdmin">true</div>
                                <div id="username"> {$user} </div>
                                <div id="loginsuccess">yes</div>
                            </div>
                
                
                )
                else(
        
                let $activo:= usuario:EsUsuarioActivo($user)
                return
                        if($activo) then
                        (
                            
                          let $userSet := session:set-attribute("user", $user),
                            $passset := session:set-attribute("password", $pass),
                            $name := usuario:ObtenerNombreUsuario($user),
                            $type := usuario:ObtenerTipoDeUsuario($user)
                            return
                            <div id="data">
                                <div id="name"> {$name} </div>
                                <div id="type"> {$type} </div>
                                <div id="username"> {$user} </div>
                                <div id="loginsuccess">yes</div>
                            </div>
                            
                        )
                        else
                        (   
                                <div id="data">
                                    <div id="name">Inactivo</div>
                                    <div id="loginsuccess">Inactivo</div>
                                </div>
                        )
                  )      
            
        ) else
        (
                        <div id="data">
                          
                            <div id="loginsuccess">no</div>
                        </div>
            (: esto se controla desde ajax jsLogin.js -Luis:)                
          )
};

(: muestra el form de login o el welcome msg con el nombre y apellidos del user activo:)
declare function loginF:show-login() as element()*
{
let $result:= <div>
              <div id="containerLogin">
            
               <div>
                    <div id ="welcome" style="display:{loginF:EstiloLogin()} "> <a id="nombreSession" href="/bdcol/search/user/profile.xql">{loginF:nombreSession()} </a>  <a href="/bdcol/search/user/logout.xql">  Logout</a></div>
                    <div id ="welcome_admin"  style="display:{loginF:EstiloLoginAdmin()}"> <a href="/bdcol/search/user/logout.xql">  Logout</a></div>
                    
                     
                </div>
                
                
                
        
        
        
            <div id ="formLogin" style="display:{loginF:EstiloForm()} ">
      <div id="topnav" class="topnav"><a href="/bdcol/search/user/register.xq" >Registrarse</a><a href="login" class="signin"><span>Entrar</span></a> </div>
       <fieldset id="signin_menu">
        <form method="post" id="signin">
        
          <label for="user">Usuario</label>
          <input required="required" id="user" name="Usuario" value="" title="user" tabindex="5" type="text" placeholder="Usuario" />
          
          <p>
            <label for="pass">Contraseña</label>
            <input required="required" id="pass" name="Contraseña" value="" title="pass" tabindex="6" type="password" placeholder="Contraseña" />
          </p>
          <p class="remember">
            <input id="signin_submit" value="Entrar" tabindex="7" type="submit" />
            
            
          </p>
          <div id="wrongLogin" >Usuario o contraseña invalida</div>
          <div id="inactive" >usuario inactivo</div>
          <p class="forgot"> <a href="#frmRecuperar" class='inline' >Olvidaste tu contraseña?</a> </p>
          
    
        </form>
      </fieldset>
      </div>
      
    </div>
             

        
        <div id="Recuperar" style='display:none'>
            <form name="frmRecuperar" id="frmRecuperar" method="post" enctype="multipart/form-data" action="/bdcol/search/user/recuperarPassword.xq" style='width:470px; margin:0 auto; padding:10px; background:#fff; text-align:center;'>
                        
                                    <p>Ingrese el email con el que se registro.</p>
                                          <table id="tableData" class="block"  cellpadding="5" width="70%" align="center" >
                                                             
                                                             <tr>
                                                                     <td colspan="2">
                                                                         <div   id="messageBox2" name="messageBox2" class="sucessPrompt" style="display:none;" >
                                                                             <ul></ul>
                                                                         </div>    
                                                                     </td>
                                                             </tr>
                                                            <tr >
                                                                <td align="left">
                                                                    <label>Email de usuario:</label>
                                                                    <input name="txtMail" id="txtMail" type="text" size="20"/>
                                                                </td>
                                                                </tr>
                                                               <tr>
                                                                <td coslpan="2">
                                                                    <input name="action" id="btnEnviarCorreo" type="submit" text="Enviar" size="20" value="Enviar"  />
                                                                        <input name="action" id="btnRegresar" type="button" text="Regresar" size="20" value="Regresar"    
                                                                        onClick="window.location.href='/bdcol/search/index.xq';" />
                                                                </td>
                                                                <td coslpan="2">
                                                                </td>
                                                               </tr>
                                                               
                                                                <tr>
                                                                        <td colspan="2">
                                                                                    <br/>                                                                        
                                                                        </td>
                                                                </tr>
                                                                   
                                        </table>
                                    
                            
             </form>
       </div>
   </div>
   return $result
};