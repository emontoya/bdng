xquery version "1.0";


declare namespace xdb="http://exist-db.org/xquery/xmldb";
import module namespace loginF="http://exist-db.org/modules/loginF" at "loginF.xq";
import module namespace configweb="http://exist-db.org/modules/configweb" at "configWeb.xq";
import module namespace usuario="http://exist-db.org/modules/usuarios" at "User.xq";



(:permite saber si mostrar el form de cambio de contraseña o si ya se realizo el action:) 
declare function local:show-form() as element()* {

   let $action := request:get-parameter("action", ())
        return   
           if($action eq "Cambiar Contraseña") then(
                        let $result := local:ChangePass()
                        return 
                         if($result eq "1") then (
                           <div   id="messageBox" name="messageBox" class="sucessPrompt" style="display:block;" >
                              <ul>
                                      <li>
                                          <label class="error" generated="true" style="display: block;">El cambio de contraseña se ha realizado exitosamente</label>
                                          <br/>
                                          <br/>
                                          <a href="{$configweb:redirect-uri}">Ir a BDCOL</a>
                                       </li>
                              </ul>
                          </div>,
                          local:displayFormChangePass()
                            )
                            else if($result eq "2") then (
                                <div   id="messageBox" name="messageBox" class="sucessPrompt" style="display:block;" >
                              <ul>
                                      <li>
                                          <label class="error" generated="true" style="display: block;">Contraseña actual Incorrecta</label>
                                       </li>
                              </ul>
                          </div>,
                          local:displayFormChangePass()
                            )
                            else(
                            <div   id="messageBox" name="messageBox" class="sucessPrompt" style="display:block;" >
                              <ul>
                                      <li>
                                          <label class="error" generated="true" style="display: block;">Error en el proceso de cambio de contraseña</label>
                                       </li>
                              </ul>
                             </div>,
                          local:displayFormChangePass()
                            
                            
                            )
                            
                        )else if (session:exists() and session:get-attribute("user")) then(
                        
                            local:displayFormChangePass()
                        )else(
                            <div   id="messageBox" name="messageBox" class="sucessPrompt" style="display:block;" >
                              <ul>
                                      <li>
                                          <label class="error" generated="true" style="display: block;">Debe estar logueado para poder cambiar la contraseña</label>
                                          <br/>
                                          <br/>
                                          <a href="{$configweb:redirect-uri}">Ir a BDCOL</a>
                                       </li>
                              </ul>
                             </div>
                        
                        )

};


declare function local:ChangePass() as xs:string{



let $username := session:get-attribute("user")
(:let $uid:= system:as-user("admin", $configweb:passwordAdmin, usuario:ObtenerIdUsuario($username)):) 
let $oldPassword:=request:get-parameter("txtOldPassword","0") cast as xs:string
let $passwordBD:=session:get-attribute("password")
return
if($oldPassword eq $passwordBD) then (
        let $password := request:request-parameter("txtPassword", "0") cast as xs:string
        let $type := usuario:ObtenerTipoDeUsuario($username)
        let $borrar:= system:as-user("admin", $configweb:passwordAdmin, xdb:change-user($username, $password, $type, $configweb:database-login))
        let $passset := session:set-attribute("password", $password) (:se hace el set del nuevo password para que quede en la session:)
        let $change:= usuario:ChangePassword($username, $password)
        

    return "1"
)else (
      "2"
        )
};

(:muestra el form de cambio de password:)
declare function local:displayFormChangePass() as element()*{


<div id="changePassword">
            <form name="frmchangePassword" id="frmchangePassword" method="post" enctype="multipart/form-data" action="changePassword.xql" style='padding:10px; background:#fff; text-align:center;'>
                        
                                    <p>Para cambiar su contraseña ingrese su usuario y contraseña actuales</p>
                                          <table class="block"  cellpadding="5" width="500px" align="center" >
                                          
                                                             
                                                             <tr>
                                                                     <td colspan="2">
                                                                         <div   id="messageBox2" name="messageBox2" class="sucessPrompt" style="display:none;" >
                                                                             <ul></ul>
                                                                         </div>    
                                                                     </td>
                                                             </tr>
                                                              
                                                            <tr>
                                                                <th colspan="2" align="left"><h3>Cambio de Contraseña</h3></th>
                                                            </tr>
                                                            <tr >
                                                                <td align="left">Nombre de usuario:</td>
                                                                <td align="left">{local:userSession()}</td>
                                                            </tr>
                                                            
                                                            <tr >
                                                                <td align="left">Contraseña actual:</td>
                                                                <td align="left"><input name="txtOldPassword" id="txtOldPassword" type="password" size="20"/></td>
                                                            </tr>
                                                            <tr>
                                                                                                                        
                                                                <td align="left">Contraseña nueva:</td>
                                                                <td align="left"><input name="txtPassword" id="txtPassword" type="password" size="20"/></td>
                                                            </tr>
                                                            <tr>
                                                                                                                        
                                                                <td align="left">Confirmar Contraseña nueva:</td>
                                                                <td align="left"><input name="txtConfirmarPassword" id="txtConfirmarPassword" type="password" size="20"/></td>
                                                            </tr>
                                                            <tr>
                                                                <td coslpan="2">
                                                                    <input name="action" id="btnChangePassword" type="submit" text="Enviar" size="20" value="Cambiar Contraseña"  />
                                                                        <input name="action" id="btnRegresar" type="button" text="Regresar" size="20" value="Regresar"    
                                                                        onClick="window.location.href='profile.xql'; " />
                                                                    
                                                                </td>
                                                               </tr>
                                                               
                                                                <tr>
                                                                        <td colspan="2">
                                                                                    <br/>                                                                        
                                                                        </td>
                                                                </tr>
                                                                   
                                        </table>
                                    
                            
             </form>
        </div>





};

(:obtenemos el nombre del usuario de la session si es que esta existe -Luis:) 
declare function local:userSession() as xs:string
{
    
    let $user := if (session:exists() and session:get-attribute("user")) then (
                        session:get-attribute("user")
                ) else (
                        ""
                )
        
        
    return $user
        
};


<html>
    <head>
		 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>{$configweb:sysName} - {$configweb:sysNameFull}</title>
        
        <link href="css/display.css" type="text/css" rel="stylesheet"/>
        <link href="{$configweb:cssRegister}" type="text/css" rel="stylesheet"/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js">a</script>
        <script language="Javascript" type="text/javascript" src="js/jquery.validate.js"  charset="utf-8">a</script>
        
        <script language="Javascript" type="text/javascript" src="{$configweb:jsUsuario}" charset="utf-8">a</script>
        <script language="Javascript" type="text/javascript" src="js/jquery.validate.js"  charset="utf-8">a</script>
        <link href="styles/front.css" media="screen, projection" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="styles/colorbox.css" />
        
        
        
    </head>
    <body class="body2">
    
    
         <div class="header-ppal"></div>
         <div class="windows-content">
		              <div   id="messageBox" name="messageBox" class="sucessPrompt" style="display:none;" >
                              <ul>
                                      
                              </ul>
                          </div>
		
		    {local:show-form()}   
		 </div>                                    
       
    </body>
</html>
