xquery version "1.0";

declare namespace request="http://exist-db.org/xquery/request";
declare namespace response="http://exist-db.org/xquery/response";
declare namespace session="http://exist-db.org/xquery/session";
declare namespace xdb="http://exist-db.org/xquery/xmldb";
declare namespace xmldb="http://exist-db.org/xquery/xmldb";

import module namespace usuario="http://exist-db.org/modules/usuarios" at "User.xq";
import module namespace configweb="http://exist-db.org/modules/configweb" at "configWeb.xq";

(:este xq no esta aun siendo usado- usar cuando se necesiten crear nuevos DBA :)


declare function local:CrearAdmin($nombre as xs:string,$username as xs:string,$email as xs:string, $password as xs:string) as element()*
{
        let $nombreArchivo:=  concat($username,".xml")
        let $perfil :=    system:as-user("admin", $configweb:passwordAdmin,  xmldb:store($configweb:database-perfiles, $nombreArchivo, <metadata/>))

   
        let $coleccion := doc(concat($configweb:database-perfiles,"/",$nombreArchivo))

        (: Actualizo la info del perfil del usuario :)
        
      let $update := update insert
                                    <user>
                                       {configweb:ObtenerNodo( $configweb:usuarios-nombre,$nombre)}
                                       {configweb:ObtenerNodo( $configweb:usuarios-username,$username)}
                                       {configweb:ObtenerNodo( $configweb:usuarios-email,$email)}
                                       {configweb:ObtenerNodo( $configweb:usuarios-activo,"1")}
                                       {configweb:ObtenerNodo( $configweb:usuarios-type,"dba")}
                                       {configweb:ObtenerNodo( $configweb:usuarios-recuperacion,$password)}

                                    </user>
                                         into $coleccion//metadata
    return   
                   <exito>exito</exito>        
                                        

};





let $nombre := request:get-parameter("nombre", ("administrador")) cast as xs:string
let $username := request:get-parameter("username", ("administrador")) cast as xs:string
let $email := request:get-parameter("email", ("0")) cast as xs:string
let $password := request:get-parameter("password", ("administrador")) cast as xs:string




let $usuario:= system:as-user("admin", $configweb:passwordAdmin, xdb:create-user($username,$password,"dba",$configweb:database-login))

return 
local:CrearAdmin($nombre, $username, $email, $password ) 




