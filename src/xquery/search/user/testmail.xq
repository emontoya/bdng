xquery version "1.0";
 
(: Demonstrates sending an email through Sendmail from eXist :)
 
declare namespace mail="http://exist-db.org/xquery/mail";
import module namespace configweb="http://exist-db.org/modules/configweb" at "configWeb.xq";
import module namespace usuario="http://exist-db.org/modules/usuarios" at "User.xq";
declare namespace request="http://exist-db.org/xquery/request";



    let $email: = request:get-parameter("txtMail", ())
    let $usuario := usuario:ObtenerUsername($email)
    return $usuario

(:
let $x :=replace(request:get-url(),"bdcol/search/configWeb.xq","/admin")
return $x



declare variable $xxx {
  <mail>
    <from>Administrador BDCOL  &lt;{$configweb:emailSender}&gt;</from>
    <to>edyhack@gmail.com</to>
    <subject>Activaci&#243;n de su cuenta en BDCOL</subject>
    <message>
        <text>Gracias por hacer parte de la Biblioteca Digital colombiana, este correo es para su activaci&#243;n como usuario en BDCOL</text>
        <xhtml>
            <html>
                    <head>
                        <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
                        <title>BDCOL- Biblioteca Digital ColombianaL</title>
                        <link type="text/css" href="css/cssMail.css" rel="stylesheet" />
                    </head>
                    <body >
                        <div class="topbar aligns" style="font-size: smaller; white-space: 
                            nowrap;">

                            </div>
                            <div class="header margins" style="height: 140px; margin: 13px 15px 9px;">
                                <a >
                                    <img src="{$configweb:imgHeader}" class="floats-normal" alt=""
                                    border="0" />
                                </a>
                            </div>
                            <div style="clear: both;"></div>
                            <br/>
                            <div class="body">
                                <b>Activacion de  Usuarios en BDCOL</b>
                                <br/><br/>
                                <p style="font-size: smaller;">El proceso de creación de su  usuario  <b></b>  en BDCOL se realiz&#243; con &#233;xito.</p>
                                <p style="font-size: smaller;"> Contraseña: <b> </b> </p>
                                <p style="font-size: smaller;"> Email: <b> </b> </p>
                                
                                <p style="font-size: smaller;">Haga click en el siguiente enlace para activar su usuario.</p>
                                <p style="font-size: smaller;"><a href="xx">Click para activar usuario</a></p>
                                
                            </div>
                     </body>
            </html>
        </xhtml>
    </message>
  </mail>
};



let $usuario := "edy"
let $url := "urlllll"
let $password := "aaaa"
let $email := "edyhack@gmail.com"
let $message := configweb:ContenidoCorreoActivacion($usuario,$url,$password, $email)

return

if ( mail:send-email($message, '200.12.180.67', ()) ) then
  <h1>Sent Message OK :-)</h1>
else
  <h1>Could not Send Message :-(</h1>
  
  :)