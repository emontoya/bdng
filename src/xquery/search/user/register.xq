xquery version "1.0";



declare namespace request="http://exist-db.org/xquery/request";
declare namespace response="http://exist-db.org/xquery/response";
declare namespace session="http://exist-db.org/xquery/session";
declare namespace xdb="http://exist-db.org/xquery/xmldb";
declare namespace image = "http://exist-db.org/xquery/image";
declare namespace xmldb="http://exist-db.org/xquery/xmldb";
declare namespace file="http://exist-db.org/xquery/file";
declare namespace text="http://exist-db.org/xquery/text";
declare namespace util="http://exist-db.org/xquery/util";
import module namespace configweb="http://exist-db.org/modules/configweb" at "configWeb.xq";
import module namespace correo="http://exist-db.org/modules/mails" at "mail.xq";
import module namespace recap = "http://exist-db.org/recaptcha" at "recaptcha.xqm";
import module namespace loginF="http://exist-db.org/modules/loginF" at "loginF.xq";



(: funcion mostrarMensaje() usada para mostrar el formulario de ingreso de datos para los usuarios de bdcol:)

declare function local:mostrarMensaje() as element()*
{
  let $exitoIngreso := session:get-attribute("exito")
      return
          if($exitoIngreso eq "0") then
          (
              <div   id="messageBox" name="messageBox" class="sucessPrompt" style="display:none;" >
                              <ul>
                                      
                              </ul>
                          </div>
          )
          else
          (
              if($exitoIngreso eq "1") then
              (
                   
                  <div   id="messageBox" name="messageBox" class="sucessPrompt" style="display:block;" >
                              <ul>
                                      <li>
                                          <label class="error" for="txtConfirmarPassword" generated="true" style="display: block;">Su usuario ha sido creado, verifique su correo electronico para activar su cuenta</label>
                                       </li>
                              </ul>
                          </div>
              )
              else
              
                  if($exitoIngreso eq "2") then
                  (
                      
                          <div   id="messageBox" name="messageBox" class="sucessPrompt" style="display:block;" >
                              <ul>
                                      <li>
                                          <label class="error" for="txtConfirmarPassword" generated="true" style="display: block;">El usuario ya existe</label>
                                       </li>
                              </ul>
                          </div>
                  )
                  else
                  (
                        ()
                  )
                   
              )
              
         
};


(: funcion mostrarValidacionImagen() valida que se ingresen los valores que se mostraron en la imagen del captcha y que estos sean los mismos :)

declare function local:mostrarValidacionImagen() as element()*
{
     let $exitoIngreso := session:get-attribute("exito")
       return
          if($exitoIngreso eq "3") then
          (
               <div   id="messageBox" name="messageBox" class="sucessPrompt" style="display:block;" >
                          <ul>
                                  <li>
                                      <label class="error" for="txtConfirmarPassword" generated="true" style="display: block;">Los caracteres ingresados no concuerdan con la imagen</label>
                                   </li>
                          </ul>
                  </div>
            
          )else
        (
              ()
          )
};

(: funcion mostrarValidacionNombre() valida que se ingrese un nombre en el txtNombre y ademas no tenga caracteres especiales ni numeros :)

declare function local:mostrarValidacionNombre() as xs:string
{
     let $nombre := request:get-parameter("txtNombre",())
      return
          if($nombre eq "") then
          (
              "El nombre es obligatorio"
          )else
          (
              let $validacionNombre:= text:filter($nombre,"/^[a-zA-Z0-9]+$/")
              return
              if($validacionNombre eq $nombre) then
              (
                  ""
              )
              else
              (
                  "El nombre tiene caracteres no validos"
              )
          )
};

(: funcion mostrarValidacionNombreUsuario() valida que se ingrese un usuario en el txtUsername y ademas no tenga caracteres especiales ni numeros :)
declare function local:mostrarValidacionNombreUsuario() as xs:string
{
     let $username := request:get-parameter("txtUsername",())
      return
           if($username eq ()) then
          (
              ""
          )
          else
          (
                  if($username eq "") then
                  (
                      "El nombre de usuario es obligatorio"
                  )else
                  (
                      let $validacionNombre:= text:filter($username,"/^[a-zA-Z0-9]+$/")
                      return
                      if($validacionNombre eq $username) then
                      (
                          ""
                      )
                      else
                      (
                          "El nombre de usuario tiene caracteres no validos"
                      )
                  )
            )
     };
     
(: funcion que valida que se ingrese un user diferente del password :)
declare function local:mostrarValidacionUserDiffPassword() as xs:string
{
     let $password := request:get-parameter("txtPassword",())
     let $username := request:get-parameter("txtUsername",())
      return
              if($password eq $username)  then
                   (
                          "El password y su nombre de usuario NO deben coincidir"
                  )
                   else
                     (
                       ""
                   )
                      
         
};


(: funcion mostrarValidacionPassword() valida que se ingrese un password en el txtPassword :)
declare function local:mostrarValidacionPassword() as xs:string
{
     let $password := request:get-parameter("txtPassword",())
      return
      if($password eq ()) then
          (
              ""
          )
          else
          (
                  if($password eq "") then
                  (
                      "El nombre de usuario es obligatorio"
                  )else
                  (
                     ""
                  )
          )
};


(: funcion mostrarValidacionConfirmacionPassword() valida que se ingrese un password en el txtConfirmarPassword igual al txtPasword ingresado en la funcion anterior :)
declare function local:mostrarValidacionConfirmacionPassword() as xs:string
{
     let $configwebirmacionPassword := request:get-parameter("txtConfirmarPassword",())
      return
       if($configwebirmacionPassword eq ()) then
          (
              ""
          )
          else
          (
                      if($configwebirmacionPassword eq "") then
                      (
                          "La confirmación del password es obligatoria"
                      )else
                      (
                         let $password := request:get-parameter("txtPassword","")
                         return
                             if($password eq $configwebirmacionPassword)  then
                             (
                                 ""
                             )
                             else
                             (
                                 "El password y su confirmación deben coincidir"
                             )
                      )
          )
};


(: funcion mostrarValidacionEmail() valida que se ingrese un email en txtEmail :)
declare function local:mostrarValidacionEmail() as xs:string
{
     let $email := request:get-parameter("txtEmail",())
      return
      if($email eq ()) then
          (
              ""
          )
          else
          (
                      if($email eq "") then
                      (
                          "El email es obligatorio"
                      )else
                      (
                           let $validacionEmail:= text:filter($email,"/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/")
                                      return
                                      if($validacionEmail eq $email) then
                                      (
                                          ""
                                      )
                                      else
                                      (
                                          "El email tiene caracteres no validos"
                                      )
                      )
           )
};


(: funcion displayFormSignup muestra el formulario de registro de usuarios :)

declare function local:displayFormSignup() as element()*
{
let $exitoIngreso := session:get-attribute("exito")
let $nombre:=request:get-parameter("txtNombre","0") cast as xs:string
let $txtUsername:=request:get-parameter("txtUsername","0") cast as xs:string
let $txtPassword := request:request-parameter("txtPassword", "0") cast as xs:string
let $email:=request:get-parameter("txtEmail","0") cast as xs:string
let $institucion:=request:get-parameter("txtInstitucion","0") cast as xs:string
let $telefono:=request:get-parameter("txtTelefono","0") cast as xs:string
return
    if($exitoIngreso eq "3") then
    (
				<div class="body">
						
							<form id="frmRegistro" method="post" enctype="multipart/form-data"  accept-charset="UTF-8">
									<table cellpadding="5" style="padding:5px 10px 7px;" width="80%">
										<tr>
											<td colspan="2">
														 
														  {local:mostrarValidacionImagen()}														  
                                                                  {local:mostrarMensaje()}
											</td>
										</tr>
									</table>
									<table id="registro" cellpadding="5" class="block" width="800px">
											
											 <tr>
                                                                    <th align="left" colspan="2"><h3>Registro de Usuarios</h3></th>
                                                      </tr>
                    	                         <tr>
                                    	                <td>
                                    	                    <label>Nombres y Apellidos: *</label>
                                    	                </td>
                                    	                <td>
                                    	                    
                                    	                    <input type="text" size="60" id="txtNombre" name="txtNombre" maxlength="30" value="{$nombre}" />
                                    	                    
                                    	                    
                                    	                </td>
                                    	            </tr>
                                    	            <tr>
                                    	                <td>
                                    	                    <label>Nombre de Usuario: *</label>
                                    	                </td>
                                    	                <td>
                                    	                    <input type="text" size="60" id="txtUsername" name="txtUsername" maxlength="30" value="{$txtUsername}" />
                                    	                    
                                    	                    
                                    	                </td>
                                    	            </tr>
                                    	            <tr>
                                    	                <td>
                                    	                    <label>Ingresar Password: *</label>
                                    	                </td>
                                    	                <td>
                                    	                    <input type="password" size="60" id="txtPassword" name="txtPassword" maxlength="30"  />
                                    	                    
                                    	                    
                                    	                </td>
                                    	            </tr>
                                    	              <tr>
                                    	                <td>
                                    	                    <label>Confirmar Password: *</label>
                                    	                </td>
                                    	                <td>
                                    	                    <input type="password" size="60" id="txtConfirmarPassword" name="txtConfirmarPassword" maxlength="30"  />
                                    	                    
                                    	                    
                                    	                </td>
                                    	            </tr>
                                    	            <tr>
                                    	                <td>
                                    	                    <label>Correo Electrónico: *</label>
                                    	                </td>
                                    	                <td>
                                    	                    <input type="text" size="60" id="txtEmail" name="txtEmail" maxlength="30" value="{$email}"/>
                                    	                    
                                    	                    
                                    	                </td>
                                    	            </tr>
													<tr>
																<td>
																	<label>Tipo de Usuario *</label>
																</td>
																<td>
																	<select id="selType" name="selType" >
																	   <option value="user" selected="selected" >Usuario de {$configweb:sysName}</option>
            														   <option value="adminRepo" >Administrador de Repositorios</option>
            														 </select>
																</td>
															</tr>
															
                                    	             <tr class="extendForm" style="display: none; ">
                                    	                <td>
                                                            <label>Institucion: *</label>
                                                        </td>
                                                        <td>
                                                            <input type="text" size="60" id="txtInstitucion" name="txtInstitucion" maxlength="90" value="{$institucion}"/>
                                                            
                                                            
                                                        </td>
                                                        
                                                    </tr>
                                                    <tr class="extendForm" style="display: none; ">
                                                        <td>
                                                            <label>Telefono de Contacto: *</label>
                                                        </td>
                                                        <td>
                                                            <input type="text" size="60" id="txtTelefono" name="txtTelefono" maxlength="20" value="{$telefono}"/>
                                                            
                                                            
                                                        </td>
                                                    </tr>
                                                  
                                                  <tr>
                                    	                <td>
                                    	                    <label>Foto:</label>
                                    	                </td>
                                    	                <td>
                                    	                       <input type="file" size="30" name="txtFoto" id="txtFoto"/>
                                    	                </td>
                                    	            </tr>
                                    	           <tr>
                                    	                <td >
                                    	                        Deseo subcribirme a los servicios,
                                    	                        <br/>
                                    	                        alertas y noticias de {$configweb:sysName}
                                    	                </td>
                                    	                <td>
                                    	                        <input type="checkbox" name="chkSubscripcion" id="chkSubscripcion" value="1"  />
                                    	                </td>
                                    	            </tr>
                                    	            <tr>
                                    	                 <td>
                                                                Ingrese en el campo de texto las dos palabras que aparecen en la imagen

                                                        </td>
                                    	            </tr>
                                    	            <tr>
                                    	                <td colspan="2" align="center" style="padding-top: 48px">
                                                                                                    	                        
                                                                             <!-- codigo para invocar el API del Captcha -->
                                                                                <script type="text/javascript" src="http://api.recaptcha.net/challenge?k=6Ld39QYAAAAAAFO0hEQpU6eTwOPvlkWKmTyiM3Ig'">
																					document.write("<div style='padding-top: 10px;'>\n");
																					document.write("<iframe src='http://api.recaptcha.net/noscript?k=6Ld39QYAAAAAAFO0hEQpU6eTwOPvlkWKmTyiM3Ig' height='300' width='500' frameborder='0'>No se encontro el servidor</iframe>\n");
                                                                                      document.write("<textarea name='recaptcha_challenge_field' rows='3' cols='40'></textarea>
                                                                                      document.write("<input type='hidden' name='recaptcha_response_field' value='manual_challenge'/>
                                                                                      document.write("</div>\n");

                                                                                  </script>
                                                                                 
                                                                   </td>
                                                             </tr>
                                                        <tr>
                                                        
                                    	            </tr>
                                    	           <tr>
                                    	                
                                    	                <td colspan="2" align="center">
                                    	                    <input type="submit"  id="btnEnviar" name="action" text="Enviar"  value="Crear Usuario"/>
                                    	                    
                                    	                </td>
                                    	            </tr>
									</table>
        	
							</form>
					</div>
            
    )
    else
    (
		  
			<div class="">
						<form id="frmRegistro" method="post" enctype="multipart/form-data" accept-charset="UTF-8" >
					
							<table cellpadding="5" style="padding:5px 10px 7px;" width="80%">
										<tr>
											<td colspan="2">
														 
													  {local:mostrarValidacionImagen()}
                                                                  {local:mostrarMensaje()}
											</td>
										</tr>
									</table>
							<table id="registro" cellpadding="5" class="block" width="80%">
											
											
											 <tr>
																			<th align="left" colspan="2"><h3>Registro de Usuarios</h3></th>
											</tr>
											<tr>
																<td>
																	<label>Nombres y Apellidos: *</label>
																</td>
																<td>
																	
																	<input type="text" size="60" id="txtNombre" name="txtNombre" maxlength="30" />
																	
																</td>
															</tr>
															<tr>
																<td>
																	<label>Nombre de Usuario: *</label>
																</td>
																<td>
																	<input type="text" size="60" id="txtUsername" name="txtUsername" maxlength="30" />
																</td>
															</tr>
															<tr>
																<td>
																	<label>Ingresar Password: *</label>
																</td>
																<td>
																	<input type="password" size="60" id="txtPassword" name="txtPassword" maxlength="30"/>
																</td>
															</tr>
															  <tr>
																<td>
																	<label>Confirmar Password: *</label>
																</td>
																<td>
																	<input type="password" size="60" id="txtConfirmarPassword" name="txtConfirmarPassword" maxlength="30"/>
																</td>
															</tr>
															<tr>
																<td>
																	<label>Correo Electrónico: *</label>
																</td>
																<td>
																	<input type="text" size="60" id="txtEmail" name="txtEmail" maxlength="30"/>
																</td>
															</tr>
															<tr>
																
                                                                <td>
                                                                    <label>Tipo de Usuario *</label>
                                                                </td>
                                                                <td>
                                                                    <select id="selType" name="selType" >
                                                                       <option value="user" selected="selected" >Usuario de {$configweb:sysName}</option>
                                                                       <option value="adminRepo" >Administrador de Repositorios</option>
                                                                     </select>
                                                                </td>
                                                            </tr>
                                                     <tr class="extendForm" style="display: none; ">
                                                        <td>
                                                            <label>Institucion: *</label>
                                                        </td>
                                                        <td>
                                                            <input type="text" size="60" id="txtInstitucion" name="txtInstitucion" maxlength="90"/>
                                                            
                                                            
                                                        </td>
                                                    </tr>
                                                    <tr class="extendForm" style="display: none; ">
                                                        <td>
                                                            <label>Telefono de Contacto: *</label>
                                                        </td>
                                                        <td>
                                                            <input type="text" size="60" id="txtTelefono" name="txtTelefono" maxlength="20"/>
                                                            
                                                            
                                                        </td>
                                                    </tr>
															 <tr>
																<td>
																	<label>Foto:</label>
																</td>
																<td>
																	   <input type="file" size="30" name="txtFoto" id="txtFoto"/>
																</td>
															</tr>
															 <tr>
																<td >
																		Desea subcribirse a los servicios y 
																		<br/>
																		alertas y noticias de {$configweb:sysName}
																</td>
																<td>
																		<input type="checkbox" name="chkSubscripcion" id="chkSubscripcion" value="1"  />
																</td>
															</tr>
														<tr>
                                                         <td>
                                                                Ingrese en el campo de texto las dos palabras que aparecen en la imagen

                                                        </td>
                                                    </tr>
															<tr>
																 <td colspan="2" align="center" style="padding-top: 48px">
                                                                                                    	                        
                                                                             <!-- codigo para invocar el API del Captcha -->
                                                                                <script type="text/javascript" src="http://api.recaptcha.net/challenge?k=6Ld39QYAAAAAAFO0hEQpU6eTwOPvlkWKmTyiM3Ig'">
																					document.write("<div style='padding-top: 10px;'>\n");
																					document.write("<iframe src='http://api.recaptcha.net/noscript?k=6Ld39QYAAAAAAFO0hEQpU6eTwOPvlkWKmTyiM3Ig' height='300' width='500' frameborder='0'>No se encontro el servidor</iframe>\n");
                                                                                      document.write("<textarea name='recaptcha_challenge_field' rows='3' cols='40'></textarea>
                                                                                      document.write("<input type='hidden' name='recaptcha_response_field' value='manual_challenge'/>
                                                                                      document.write("</div>\n");

                                                                                  </script>
                                                                                 
                                                                   </td>
															</tr>
														   <tr>
																
																<td colspan="2" align="center">
																	<input type="submit"  id="btnEnviar" name="action" text="Enviar"  value="Crear Usuario"/>
																	<input type="button"  id="btnCancelar" name="btnCancelar" text="Cancelar"  onClick="window.location.href='../index.xq'; " value="Cancelar"/>  
																	<input type="button"  id="btnVolver" name="btnVolver" text="Volver"  onClick="window.location.href='../index.xq'; " value="Volver"/>
																	
																</td>
															</tr>
										</table>
					
							</form>
					</div>
			
    )
};

(: Permite crear el usuario y luego asignarle un perfil 1 creo, 2 no creo , 3 invalida imagen :)

declare function local:crearUsuario() as xs:string 
{

                      let $validoImagen:=   local:ValidarImagen() (: AKI Solo para el test.bdcol.org por el captcha :)
                        (:let $validoImagen:=   true():)
                        return
                            if($validoImagen) then
                            (
                            
                                            let $nombre:=request:get-parameter("txtNombre","0") cast as xs:string
                                            let $username:=request:get-parameter("txtUsername","0") cast as xs:string
                                            let $subscripcion:=request:get-parameter("chkSubscripcion","0") cast as xs:string
                                            let $nombre:=request:get-parameter("txtNombre","0") cast as xs:string
                                            let $type:=request:get-parameter("selType","user") cast as xs:string
                                            let $institucion:=request:get-parameter("txtInstitucion","-") cast as xs:string
                                            let $telefono:=request:get-parameter("txtTelefono","-") cast as xs:string
                                            
                                                                                        
                                            let $usuarioEsta:=local:buscarUsuario($username)
                                                return 
                                                    if($usuarioEsta eq false()) then
                                                    (
                                                        let $password := request:request-parameter("txtPassword", "0") cast as
                                                            xs:string
                                                        let $email:=request:get-parameter("txtEmail","0") cast as xs:string
                                                        let $usuario:= system:as-user("admin", $configweb:passwordAdmin, xdb:create-user($username,$password,$type,$configweb:database-login))
                                                        let $datos:=local:CrearPerfilUsuario($nombre,$username,$email,$password,$subscripcion,$type,$institucion,$telefono)
                                                             return  
                                                                 "1"
                                                     )
                                                    else
                                                    (
                                                            "2"
                                                        
                                                    )
                             )
                             else
                            (
                                 "3"
                            )
      
};

declare function local:ValidarImagen() as xs:boolean 
{

        let $recapture-private-key := "6Ld39QYAAAAAAMGi9xDhZUvn0dZQcB6ooKEP4kNQ" 
        let $valido:= recap:validate($recapture-private-key, request:get-parameter("recaptcha_challenge_field", ()), request:get-parameter("recaptcha_response_field",()))
        return $valido

};
declare function local:ValidarDatos() as xs:boolean 
{
     let $validacionDatos:=local:ValidarDatos()
     
     return
         if($validacionDatos) then
         (
                 let $validacionNombre:= local:mostrarValidacionNombre()
                 return
                  if($validacionNombre eq "") then
                  (
                      let $validacionNombreUsuario:= local:mostrarValidacionNombreUsuario()
                      return
                                if($validacionNombreUsuario eq "") then
                                  (
                                      let $validacionPassword:= local:mostrarValidacionPassword()
                                         return
                                          if($validacionPassword eq "")then
                                          (
                                           let  $validacionUserDiffPassword:= local:mostrarValidacionUserDiffPassword()
                                           return
                                                if($validacionUserDiffPassword eq "") then 
                                                (
                                                    let $validacionConfirmacionPassword:= local:mostrarValidacionConfirmacionPassword()
                                                    return
                                                        if($validacionConfirmacionPassword eq "")then
                                                        (
                                                            let $validacionConfirmacionPassword:= local:mostrarValidacionEmail()
                                                                return
                                                                    if($validacionConfirmacionPassword eq "")then
                                                                        (
                                                                            true()
                                                                        )
                                                                        else
                                                                        (
                                                                            false()
                                                                        )
                                                                               )
                                                                               else
                                                      (
                                                          false()
                                                      )
                                                          
                                                      )
                                                      else
                                                      (
                                                          false()
                                                      )
                                          )
                                          else
                                          (
                                              false()
                                          )
                                  )
                                  else
                                  (
                                      false()
                                  )
                  )
                  else
                  (
                      false()
                  )
         )
         else
         (
             false()
         )
};

(: Guarda los archivos en el directorio local :)
declare function local:GuardarArchivo($docname as xs:string, $username as xs:string) as element()*
{
    let $validarArchivo:= $docname
    return
        if($validarArchivo eq "") then
        (
            ()
        )else
        (
            let $file := request:get-uploaded-file-data("txtFoto")
            let $docname :=  replace(request:get-uploaded-file-name("txtFoto")," ","_") (: este replace se hizo para que pudieran subirse fotos con espacios en el nombre- luis:)
            let $ruta:=configweb:rutaLocalImagen(),
             $nombreArchivo: = concat($ruta,$docname),
             $guardado:=system:as-user("admin", $configweb:passwordAdmin,   file:serialize-binary($file,$nombreArchivo) )
                return
                ()
        )
};
(: Permite crear el perfil del usuario asignando como nombre de archivo el mismo nombre de usuario :)
declare function local:CrearPerfilUsuario($nombre as xs:string,$username as xs:string,$email as xs:string, $password as xs:string, $subscripcion as xs:string, $type as xs:string, $institucion as xs:string, $telefono as xs:string) as element()*
{
        let $nombreArchivo:=  concat($username,".xml")
        let $perfil :=    system:as-user("admin", $configweb:passwordAdmin,  xmldb:store($configweb:database-perfiles, $nombreArchivo, <metadata/>))
        let $path := "/user"
        
        (: Guardo los archivos uploaded  :)
        let $docname :=  replace(request:get-uploaded-file-name("txtFoto")," ","_") (: este replace se hizo para que pudieran subirse fotos con espacios en el nombre- luis:)
        
        (:Guardo la imagen:)
        let $archivo :=local:GuardarArchivo($docname,$username)
        (: Obtengo la clave para verificacion luego de enviar correo :)
        let $claveCorreo:= util:md5($username)
 	let $l := util:log("error", ("Genero clave: ", "Trajo archivo"))
 	   let $rutarArchivo:=
 	   if($docname) then( $docname)
        else("noPicture.jpg")
        let $coleccion := doc(concat($configweb:database-perfiles,"/",$nombreArchivo))
         let $l := util:log("error", ("Busqueda error 1: ", $coleccion))
        (: Actualizo la info del perfil del usuario :)
        
      let $update := update insert
									<user>
                                       {configweb:ObtenerNodo( $configweb:usuarios-nombre,$nombre)}
                                       {configweb:ObtenerNodo( $configweb:usuarios-username,$username)}
                                       {configweb:ObtenerNodo( $configweb:usuarios-email,$email)}
                                       {configweb:ObtenerNodo( $configweb:usuarios-photo,$rutarArchivo)}
                                       {configweb:ObtenerNodo( $configweb:usuarios-claveCorreo,$claveCorreo)}
                                       {configweb:ObtenerNodo( $configweb:usuarios-activo,"0")}
                                       {configweb:ObtenerNodo( $configweb:usuarios-recuperacion,$password)}
                                       {configweb:ObtenerNodo( $configweb:usuarios-subscripcion,$subscripcion)}
                                       {configweb:ObtenerNodo( $configweb:usuarios-type,$type)}
                                       {configweb:ObtenerNodo( $configweb:usuarios-institucion,$institucion)}
                                       {configweb:ObtenerNodo( $configweb:usuarios-telefono,$telefono)}
                                    </user>
                                         into $coleccion//metadata
    return                               
                                        
       if($type eq "adminRepo") then(                                         
	           let $mensajeConfirmacion := correo:EnviarCorreoActivacionAdminRepo($username,$email,$claveCorreo,$institucion,$telefono)
	           
	           return ()
	   )else(
	           let $mensajeConfirmacion := correo:EnviarCorreoConfirmacionUsuario($username,$password,$claveCorreo,$email)
	           return ()
	   )
           
};

(: funcion para buscar si el usuario ya esta donde se guardan:)
declare function local:buscarUsuario($username as xs:string) as xs:boolean
{

    let $existe := xmldb:exists-user($username)
         return 
            $existe
        
};

declare function local:cambiarUsuario($user, $pass,$col) as element()*
{
let $login := xdb:authenticate($col, $user, $pass)
    return 
       ()
};

declare function local:upload() 
{
    let $file := request:get-uploaded-file("txtFoto"),
    $docname :=  request:get-uploaded-file-name("txtFoto")
    let $f := session:set-attribute("file", $file),
    $dn := session:set-attribute("docname", $docname)
    return
        local:displayFormSignup()
};


(: Funcion que permite ver si va a desplegar formulario de ingreso de datos o si va a crear los datos en si:)
declare function local:show-form() as element()* {

        	    		let $action := request:get-parameter("action", ())
        	    		        return
                		            if($action eq "upload") then
                		            (
                		                local:upload()
                		            )else if($action eq "Crear Usuario") then
                		            (
                		                
                		                let $bCreoUsuario:= local:crearUsuario()
                                		    return
                                		        

                                                                  
                                                    			if($bCreoUsuario eq "1") then
                                                    			(
                                                    			    let $exito := session:set-attribute("exito", "1")
                                                    			        return
                                                    			            local:displayFormSignup()
                                                    			    
                                                    			)
                                                    			else
                                                    			(
                                                            			    if($bCreoUsuario eq "2") then
                                                            			    (
                                                                                                                    let $exito := session:set-attribute("exito", "2")
                                                                                            		            return
                                                                                            			            local:displayFormSignup()
                                                                                                            )else
                                                                                                            (
                                                                                                                      let $exito := session:set-attribute("exito", "3")
                                                                                            		            return
                                                                                            			            local:displayFormSignup()
                                                                                                            )
                                                    			)
                                                    	
                		            )else
                		            (
                                		            let $exito := session:set-attribute("exito", "0")
                                		            
                                		                return
                                    					local:displayFormSignup()
                		            
                		            )

        		    
        	    
};

<html >
 <head>
    
 
        <title> {$configweb:sysName} - {$configweb:sysNameFull}</title>
       
       <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <link href="css/display.css" type="text/css" rel="stylesheet"/>
        <link href="css/register.css" type="text/css" rel="stylesheet"/>
        <link href="css/front.css" media="screen, projection" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="css/colorbox.css" />
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js">a</script>
        <script language="Javascript" type="text/javascript" src="js/jquery.validate.js"  charset="utf-8">a</script>
        <script language="Javascript" type="text/javascript" src="js/jsRegister.js" charset="utf-8">a</script>
        <script language="Javascript" type="text/javascript" src="{$configweb:jsUsuario}" charset="utf-8">a</script>
        <script language="Javascript" type="text/javascript" src="js/captcha.js" charset="utf-8">a</script>
	    <script type="text/javascript" src="js/jsLogin.js" charset="utf-8">a</script>
        <script type="text/javascript" src="js/jquery.colorbox.js" charset="utf-8">a</script>
        
         
        
     </head>
    <body class="body2">
         <div class="header-ppal"></div>
         <div class="windows-content">
 
        		<div align="center">

        		    { local:show-form() }
        		</div>		
		</div>
	</body>
</html>

